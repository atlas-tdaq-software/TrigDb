-- $Id: combined_schema.sql,v 1.83 2009-03-29 08:14:27 stelzer Exp $
-- $Revision: 1.83 $
---------------------------------------------------------
-- SQL script for the schema generation of the TriggerDB
--
-- This script simply combines scripts that have been
-- developed before (see comments below)
-- In this combination the names of tables and attributes 
-- have been changed in order to follow a consistent naming 
-- convention throughout the entire DB.
------------------------------------
-----------------------------
--                         --
-- clear the existing      --
-- tables                  --
--                         -- 
-----------------------------

-- In ORACLE tables can only be dropped 
-- if no foreign key points to them.
-- Hence we start droping the master tables
-- from the top, next we remove the M:N 
-- tables, next we remove all other tables
-- that hold a foreign key, and finally the
-- tables the have no foreign key.

-- Note that for the creation the reverse order
-- must be obeyed

-- We should use PURGE when on Oracle to make sure
-- that the database will get replicated without
-- problems


--
-----------------------------
--                         --
-- CREATE TABLEs           --
--                         -- 
-----------------------------

-------------------------------------------------
-- SQL script for the LVL1 trigger database
-- date  : 04.07.05
-- author: Johannes Haller (CERN)
-- author: Simon Head      (Manchester)
-- author: Paul Bell       (Manchester)
-- author: Joerg Stelzer   (DESY)
-- author: Tiago Perez 	   (DESY)
-- author: Alex Martyniuk  (UCL)
-------------------------------------------------

----------------------------
-- the LVL1 part


----------------------------
-- first the datatables
----------------------------

CREATE TABLE DBCOPY_SOURCE_DATABASE (
   NAME_OF_DB        VARCHAR2(200),
   CONSTRAINT	     DB_pk PRIMARY KEY (NAME_OF_DB)
);

-- Special table that describes the schema version
CREATE TABLE trigger_schema (
    ts_id                 NUMBER(10),
    ts_trigdb_tag         VARCHAR2(50),
    ts_trigtool_tag       VARCHAR2(50),
    CONSTRAINT            ts_id_NN         CHECK ( ts_id IS NOT NULL),
    CONSTRAINT            ts_trigdb_tag_NN     CHECK ( ts_trigdb_tag IS NOT NULL),
    CONSTRAINT            ts_trigtool_tag_NN     CHECK ( ts_trigtool_tag IS NOT NULL),
    CONSTRAINT            ts_pk            PRIMARY KEY(ts_id)
);
INSERT INTO trigger_schema VALUES (1,"TrigDB-02-00-06","TriggerTool-04-00-08");

-- The users and their access rights
CREATE TABLE tt_users (
    tt_level VARCHAR2(50),
    tt_user  VARCHAR2(50),
    tt_password VARCHAR2(50),
    CONSTRAINT		  tt_user_pk	PRIMARY KEY(tt_level,tt_user),
    CONSTRAINT            tt_level_NN         CHECK ( tt_level IS NOT NULL),
    CONSTRAINT            tt_user_NN             CHECK ( tt_user IS NOT NULL),
    CONSTRAINT            tt_password_NN        CHECK ( tt_password IS NOT NULL)
);

-- The write lock table for internal locking mechanism
-- Maximum one entry allowed
CREATE TABLE tt_writelock (
    tw_id             NUMBER(1)                  default 1,
    tw_process        VARCHAR2(256),
    tw_description    VARCHAR2(256),
    tw_username       VARCHAR2(50),
    tw_modified_time  TIMESTAMP,
    CONSTRAINT        tw_id_pk            PRIMARY KEY (tw_id),
    CONSTRAINT        tw_id_NN            CHECK(tw_id=1)

);

-- trigger alias presented to the shift user
CREATE TABLE trigger_alias (
    tal_id                          NUMBER(10),
    tal_super_master_table_id       NUMBER(10),
    tal_trigger_alias               VARCHAR2(50),
    tal_default                NUMBER(1)             default 0,
    tal_username                 VARCHAR2(50),
    tal_modified_time               TIMESTAMP,
    tal_used                        CHAR            default 0,       
    CONSTRAINT                    tal_pk               PRIMARY KEY(tal_id),
    CONSTRAINT            tal_id_NN         CHECK ( tal_id IS NOT NULL),
    CONSTRAINT            tal_smt_id_NN         CHECK ( tal_super_master_table_id IS NOT NULL),
    CONSTRAINT            tal_trigger_alias_NN     CHECK ( tal_trigger_alias IS NOT NULL),
    CONSTRAINT            tal_default_NN         CHECK ( tal_default IS NOT NULL),
    CONSTRAINT            tal_used_NN         CHECK ( tal_used IS NOT NULL)
);

-- A list of valid rates for the drop down box on the edit-rate tab.  The actual
-- rate is stored into the l1_random table
CREATE TABLE l1_random (
    l1r_id NUMBER(10), 
    l1r_cut0 VARCHAR2(20 BYTE), 
    l1r_cut1 VARCHAR2(20 BYTE), 
    l1r_cut2 VARCHAR2(20 BYTE), 
    l1r_cut3 VARCHAR2(20 BYTE), 
    l1r_version NUMBER(10),
    constraint l1r_pk      Primary key(l1r_id),
    CONSTRAINT l1r_cut0_NN     CHECK ( l1r_cut0 IS NOT NULL),
    CONSTRAINT l1r_cut1_NN     CHECK ( l1r_cut1 IS NOT NULL),
    CONSTRAINT l1r_cut2_NN     CHECK ( l1r_cut2 IS NOT NULL),
    CONSTRAINT l1r_cut3_NN     CHECK ( l1r_cut3 IS NOT NULL)
 );

-- Here the MUCPTI object is stored.
-- Please note that these are actually LVL2 cuts
CREATE TABLE l1_muctpi_info (
    l1mi_id             NUMBER(10),
    l1mi_name             VARCHAR2(50),
    l1mi_version             NUMBER(11),
    l1mi_low_pt             NUMBER(10),
    l1mi_high_pt             NUMBER(10),
    l1mi_max_cand             NUMBER(10),
    CONSTRAINT             muctpi_pk         PRIMARY KEY (l1mi_id),
    CONSTRAINT            muctpi_nmver        UNIQUE (l1mi_name, l1mi_version),
    CONSTRAINT            l1mi_id_NN         CHECK ( l1mi_id IS NOT NULL),
    CONSTRAINT            l1mi_name_NN         CHECK ( l1mi_name IS NOT NULL),
    CONSTRAINT            l1mi_version_NN     CHECK ( l1mi_version IS NOT NULL),
    CONSTRAINT            l1mi_low_pt_NN         CHECK ( l1mi_low_pt IS NOT NULL),
    CONSTRAINT            l1mi_high_pt_NN     CHECK ( l1mi_high_pt IS NOT NULL),
    CONSTRAINT            l1mi_max_cand_NN     CHECK ( l1mi_max_cand IS NOT NULL)
);

-- Here the deadtime information is stored
CREATE TABLE l1_dead_time ( 
    l1dt_id             NUMBER(10),
    l1dt_name             VARCHAR2(50),
    l1dt_version             NUMBER(11),
    l1dt_simple             NUMBER(10),
    l1dt_complex1_rate         NUMBER(10),
    l1dt_complex1_level        NUMBER(10),
    l1dt_complex2_rate         NUMBER(10),
    l1dt_complex2_level        NUMBER(10),
    CONSTRAINT            l1dt_pk            PRIMARY KEY (l1dt_id),
    CONSTRAINT             l1dt_nmver        UNIQUE (l1dt_name, l1dt_version),
    CONSTRAINT            l1dt_id_NN         CHECK ( l1dt_id IS NOT NULL),
    CONSTRAINT            l1dt_name_NN         CHECK ( l1dt_name IS NOT NULL),
    CONSTRAINT            l1dt_version_NN     CHECK ( l1dt_version IS NOT NULL),
    CONSTRAINT            l1dt_simple_NN         CHECK ( l1dt_simple IS NOT NULL),
    CONSTRAINT            l1dt_complex1_rate_NN     CHECK ( l1dt_complex1_rate IS NOT NULL),
    CONSTRAINT            l1dt_complex1_level_NN     CHECK ( l1dt_complex1_level IS NOT NULL),
    CONSTRAINT            l1dt_complex2_rate_NN     CHECK ( l1dt_complex2_rate IS NOT NULL),
    CONSTRAINT            l1dt_complex2_level_NN     CHECK ( l1dt_complex2_level IS NOT NULL)
);

-- Here the two prescaled clocks of the CTP are stored.
-- Please note the additional remarks on the random triggers
-- which also apply here.
CREATE TABLE l1_prescaled_clock (
    l1pc_id             NUMBER(10),
    l1pc_name             VARCHAR2(50),
    l1pc_version             NUMBER(11),
    l1pc_clock1             NUMBER(10),
    l1pc_clock2             NUMBER(10),
    CONSTRAINT            psc_pk            PRIMARY KEY (l1pc_id),
    CONSTRAINT            psc_nmver        UNIQUE (l1pc_name, l1pc_version),
    CONSTRAINT            l1pc_id_NN         CHECK ( l1pc_id IS NOT NULL),
    CONSTRAINT            l1pc_name_NN         CHECK ( l1pc_name IS NOT NULL),
    CONSTRAINT            l1pc_version_NN     CHECK ( l1pc_version IS NOT NULL),
    CONSTRAINT            l1pc_clock1_NN         CHECK ( l1pc_clock1 IS NOT NULL),
    CONSTRAINT            l1pc_clock2_NN         CHECK ( l1pc_clock2 IS NOT NULL)
);

-- two additional tables requested by L1Calo
CREATE TABLE l1_jet_input (
    l1ji_id             NUMBER(10),
    l1ji_name             VARCHAR2(50),
    l1ji_version             NUMBER(11),
    l1ji_type            VARCHAR2(6),
    l1ji_value             NUMBER(10),
    l1ji_eta_min             NUMBER(10),
    l1ji_eta_max             NUMBER(10),
    l1ji_phi_min             NUMBER(10),
    l1ji_phi_max             NUMBER(10),
    CONSTRAINT            l1ji_pk            PRIMARY KEY (l1ji_id),
    CONSTRAINT             l1ji_nmver        UNIQUE (l1ji_name, l1ji_version),
    CONSTRAINT            l1ji_id_NN         CHECK ( l1ji_id IS NOT NULL),
    CONSTRAINT            l1ji_name_NN         CHECK ( l1ji_name IS NOT NULL),
    CONSTRAINT            l1ji_version_NN     CHECK ( l1ji_version IS NOT NULL),
    CONSTRAINT            l1ji_type_NN         CHECK ( l1ji_type IS NOT NULL),
    CONSTRAINT            l1ji_value_NN         CHECK ( l1ji_value IS NOT NULL),
    CONSTRAINT            l1ji_eta_min_NN     CHECK ( l1ji_eta_min IS NOT NULL),
    CONSTRAINT            l1ji_eta_max_NN     CHECK ( l1ji_eta_max IS NOT NULL),
    CONSTRAINT            l1ji_phi_min_NN     CHECK ( l1ji_phi_min IS NOT NULL),
    CONSTRAINT            l1ji_phi_max_NN     CHECK ( l1ji_phi_max IS NOT NULL)
);

-- note the values for val1 to val8 should eb 8-bit integer
CREATE TABLE l1_calo_sin_cos (
    l1csc_id             NUMBER(10),
    l1csc_name             VARCHAR2(50),
    l1csc_version             NUMBER(11),
    l1csc_val1             NUMBER(10),
    l1csc_val2             NUMBER(10),
    l1csc_val3             NUMBER(10),
    l1csc_val4             NUMBER(10),
    l1csc_val5             NUMBER(10),
    l1csc_val6             NUMBER(10),
    l1csc_val7             NUMBER(10),
    l1csc_val8             NUMBER(10),
    l1csc_eta_min             NUMBER(10),
    l1csc_eta_max             NUMBER(10),
    l1csc_phi_min             NUMBER(10),
    l1csc_phi_max             NUMBER(10),
    CONSTRAINT            l1csc_pk        PRIMARY KEY (l1csc_id),
    CONSTRAINT            l1csc_nmver        UNIQUE (l1csc_name, l1csc_version),
    CONSTRAINT            l1csc_id_NN         CHECK ( l1csc_id IS NOT NULL),
    CONSTRAINT            l1csc_name_NN         CHECK ( l1csc_name IS NOT NULL),
    CONSTRAINT            l1csc_version_NN     CHECK ( l1csc_version IS NOT NULL),
    CONSTRAINT            l1csc_val1_NN         CHECK ( l1csc_val1 IS NOT NULL),
    CONSTRAINT            l1csc_val2_NN         CHECK ( l1csc_val2 IS NOT NULL),
    CONSTRAINT            l1csc_val3_NN         CHECK ( l1csc_val3 IS NOT NULL),
    CONSTRAINT            l1csc_val4_NN         CHECK ( l1csc_val4 IS NOT NULL),
    CONSTRAINT            l1csc_val5_NN         CHECK ( l1csc_val5 IS NOT NULL),
    CONSTRAINT            l1csc_val6_NN         CHECK ( l1csc_val6 IS NOT NULL),
    CONSTRAINT            l1csc_val7_NN         CHECK ( l1csc_val7 IS NOT NULL),
    CONSTRAINT            l1csc_val8_NN         CHECK ( l1csc_val8 IS NOT NULL),
    CONSTRAINT            l1csc_eta_min_NN     CHECK ( l1csc_eta_min IS NOT NULL),
    CONSTRAINT            l1csc_eta_max_NN     CHECK ( l1csc_eta_max IS NOT NULL),
    CONSTRAINT            l1csc_phi_min_NN     CHECK ( l1csc_phi_min IS NOT NULL),
    CONSTRAINT            l1csc_phi_max_NN     CHECK ( l1csc_phi_max IS NOT NULL)
);

-- Here the bunchgroup sets are defined. These sets are
-- maximum 8 bunch groups. Here again we give a direct link
-- from the trigger menu in order to change the definition
-- of bunchgroups without changing the definition of the
-- trigger items. In their definition only the BG is specified
-- from the set of BGs associated to the trigger menu.
CREATE TABLE l1_bunch_group_set (
    l1bgs_id              NUMBER(10),
    l1bgs_name            VARCHAR2(50),
    l1bgs_version         NUMBER(11),
    l1bgs_comment         VARCHAR2(200),
    l1bgs_username        VARCHAR2(50),
    l1bgs_modified_time   TIMESTAMP,
    --l1bgs_used            CHAR
    l1bgs_partition           NUMBER(1),     -- use as distinguishing parameter
    CONSTRAINT            l1bgs_pk        PRIMARY KEY (l1bgs_id),
    CONSTRAINT            l1bgs_nmver        UNIQUE (l1bgs_name, l1bgs_version),
    CONSTRAINT            l1bgs_id_NN         CHECK ( l1bgs_id IS NOT NULL),
    CONSTRAINT            l1bgs_name_NN         CHECK ( l1bgs_name IS NOT NULL),
    CONSTRAINT            l1bgs_version_NN     CHECK ( l1bgs_version IS NOT NULL),
    --CONSTRAINT            l1bgs2bg_used_NN     CHECK ( l1bgs_used IS NOT NULL)
    CONSTRAINT            l1bgs_partition_NN     CHECK ( l1bgs_partition IS NOT NULL)
);        

-- defines the bunch groups used in the bunch group sets above
CREATE TABLE l1_bunch_group (
    l1bg_id                NUMBER(10),
    --l1bg_name             VARCHAR2(50),
    --l1bg_version            NUMBER(11),
    --l1bg_comment            VARCHAR2(200),
    l1bg_size               NUMBER(4),
    CONSTRAINT            l1bg_pk            PRIMARY KEY (l1bg_id),
    --CONSTRAINT             l1bg_nmver        UNIQUE (l1bg_name, l1bg_version),
    CONSTRAINT            l1bg_id_NN         CHECK ( l1bg_id IS NOT NULL),
    --CONSTRAINT            l1bg_name_NN         CHECK ( l1bg_name IS NOT NULL),
    --CONSTRAINT            l1bg_version_NN     CHECK ( l1bg_version IS NOT NULL),
    CONSTRAINT            l1bg_size_NN     CHECK ( l1bg_size IS NOT NULL)
);

-- In this table the CTP HW files are stored. THis table is referenced
-- by the trigger_menu. There is one entry of ctp_hw_files for each L1_menu;
CREATE TABLE l1_ctp_files ( 
    l1cf_id             NUMBER(10),
    l1cf_name             VARCHAR2(50),
    l1cf_version             NUMBER(11),
    l1cf_lut            CLOB,
    l1cf_cam            CLOB,    
    l1cf_mon_sel_slot7        CLOB,
    l1cf_mon_sel_slot8        CLOB,
    l1cf_mon_sel_slot9        CLOB,
    l1cf_mon_sel_ctpmon        CLOB,
    l1cf_mon_dec_slot7        CLOB,
    l1cf_mon_dec_slot8        CLOB,
    l1cf_mon_dec_slot9        CLOB,
    l1cf_mon_dec_ctpmon        CLOB,
    CONSTRAINT             l1cf_pk            PRIMARY KEY (l1cf_id),
    CONSTRAINT             l1cf_nmver        UNIQUE (l1cf_name, l1cf_version),
    CONSTRAINT            l1cf_id_NN         CHECK ( l1cf_id IS NOT NULL),
    CONSTRAINT            l1cf_name_NN         CHECK ( l1cf_name IS NOT NULL),
    CONSTRAINT            l1cf_version_NN     CHECK ( l1cf_version IS NOT NULL)
);

-- In this table the switch matrix input files are stored, as well as their 
-- associated vhdl and binary files. This table is referenced by the trigger_menu.
CREATE TABLE l1_ctp_smx (
    l1smx_id            NUMBER(10),
    l1smx_name             VARCHAR2(50),
    l1smx_version            NUMBER(11),
    l1smx_output            CLOB,
    l1smx_vhdl_slot7        CLOB,
    l1smx_vhdl_slot8        CLOB,
    l1smx_vhdl_slot9        CLOB,
    l1smx_svfi_slot7        CLOB,
    l1smx_svfi_slot8        CLOB,
    l1smx_svfi_slot9        CLOB,        
    CONSTRAINT            l1smx_pk        PRIMARY KEY (l1smx_id),
    CONSTRAINT            l1smx_nmver        UNIQUE (l1smx_name, l1smx_version),
    CONSTRAINT            l1smx_id_NN         CHECK ( l1smx_id IS NOT NULL),
    CONSTRAINT            l1smx_name_NN         CHECK ( l1smx_name IS NOT NULL),
    CONSTRAINT            l1smx_version_NN     CHECK ( l1smx_version IS NOT NULL)
); 

-- Surprise! Its a wild topo table!
-- A link between the L1Topo and L1CTP configurations. This way the L1 will know
-- what bits are coming from the menu. Currently the topo and L1 menus point to this
-- table might have to point this table back to the topo menu?
-- The output bits will not necessarily change if the algos do, so this might be more stable
-- in this configuration.

CREATE TABLE topo_output_list (
    ol_id 		NUMBER(10),
    ol_name		VARCHAR2(50),
    CONSTRAINT          ol_pk          PRIMARY KEY (ol_id),    
    CONSTRAINT		ol_id_NN	  CHECK ( ol_id IS NOT NULL),
    CONSTRAINT		ol_name_NN	  CHECK ( ol_name IS NOT NULL)
);


-- The trigger menu. There are refrences to information stored
-- in other tables. Each change of this information requires a 
-- version update of the trigger menu. Please note that the 
-- random trigger rates, the prescaled clocks, and the bunch group
-- sets are defined here.     
-- Note that the reference to the prescale set only gives the default
-- prescale of this trigger menu. The actual used prescales are stored
-- in the master table
-- There is a foreign key to the ctp_hw_files attributed to this menu.
-- There is a foreign key to the ctp_smx_files attributed to this menu.
CREATE TABLE l1_trigger_menu (
    l1tm_id             NUMBER(10),
    l1tm_name             VARCHAR2(50),
    l1tm_version             NUMBER(11),
    l1tm_phase             VARCHAR2(50),
    l1tm_ctp_safe            NUMBER(1),
    l1tm_ctp_files_id        NUMBER(10),
    l1tm_ctp_smx_id            NUMBER(10),
    CONSTRAINT            l1tm_fk_cf        FOREIGN KEY (l1tm_ctp_files_id)
                                REFERENCES l1_ctp_files(l1cf_id),
    CONSTRAINT            l1tm_fk_smx        FOREIGN KEY (l1tm_ctp_smx_id)
                                REFERENCES l1_ctp_smx(l1smx_id),
    CONSTRAINT             l1tm_pk            PRIMARY KEY (l1tm_id),
    CONSTRAINT             l1tm_nmver        UNIQUE (l1tm_name, l1tm_version),
    CONSTRAINT            l1tm_id_NN         CHECK ( l1tm_id IS NOT NULL),
    CONSTRAINT            l1tm_name_NN         CHECK ( l1tm_name IS NOT NULL),
    CONSTRAINT            l1tm_version_NN     CHECK ( l1tm_version IS NOT NULL),
    CONSTRAINT            l1tm_phase_NN         CHECK ( l1tm_phase IS NOT NULL),
    CONSTRAINT            l1tm_ctp_safe_NN     CHECK ( l1tm_ctp_safe IS NOT NULL)
);
CREATE INDEX l1tm_ctp_files_id_ind ON l1_trigger_menu(l1tm_ctp_files_id) COMPRESS 1;
CREATE INDEX l1tm_ctp_smx_id_ind   ON l1_trigger_menu(l1tm_ctp_smx_id) COMPRESS 1;



--This table links the topo_output_list to the L1_menu
CREATE TABLE topo_l1_link (
    t2l1_id            	NUMBER(10),
    t2l1_topo_id	NUMBER(10),
    t2l1_l1_id          NUMBER(10),
    CONSTRAINT          t2l1_pk          PRIMARY KEY (t2l1_id),
    CONSTRAINT          olid_fk_tm        FOREIGN KEY (t2l1_topo_id)
                                REFERENCES topo_output_list(ol_id),
    CONSTRAINT          t2l1_topo_NN     CHECK ( t2l1_topo_id IS NOT NULL),
    CONSTRAINT          l1tm_fk_tm        FOREIGN KEY (t2l1_l1_id)
                                REFERENCES l1_trigger_menu(l1tm_id),
    CONSTRAINT          t2l1_l1_NN     CHECK ( t2l1_l1_id IS NOT NULL)
);
CREATE INDEX t2l1_topo_id_ind ON topo_l1_link (t2l1_topo_id) COMPRESS 1;
CREATE INDEX t2l1_l1_id_ind ON topo_l1_link (t2l1_l1_id) COMPRESS 1;


-- This is a list of trigger items. The ti_definiton is a encoded
-- logical expression. The amount of logical expression in LVL1 trigger
-- menus will be limited. 95 percents of the cases are simple AND
-- connections. No need to install something more sophisticated. Can
-- be changed easily. ti_group is just a variable to bring an 
-- overview into the many trigger-items.
CREATE TABLE l1_trigger_item (
    l1ti_id             NUMBER(10),     
    l1ti_name             VARCHAR2(250),     
    l1ti_version             NUMBER(11),    
    l1ti_comment            VARCHAR2(50),    
    l1ti_ctp_id            NUMBER(10),     
    l1ti_priority            VARCHAR2(6),    
    l1ti_definition            VARCHAR2(128),    
    l1ti_group            NUMBER(10),    
    l1ti_trigger_type        NUMBER(4),     
    l1ti_partition           NUMBER(1),     
    l1ti_monitor	     VARCHAR2(15) default "~",
    CONSTRAINT            l1ti_pk            PRIMARY KEY (l1ti_id),
    CONSTRAINT            l1ti_nmver        UNIQUE (l1ti_name, l1ti_version),
    CONSTRAINT            l1ti_id_NN         CHECK ( l1ti_id IS NOT NULL),
    CONSTRAINT            l1ti_name_NN         CHECK ( l1ti_name IS NOT NULL),
    CONSTRAINT            l1ti_version_NN     CHECK ( l1ti_version IS NOT NULL),
    CONSTRAINT            l1ti_ctp_id_NN         CHECK ( l1ti_ctp_id IS NOT NULL),
    CONSTRAINT            l1ti_priority_NN     CHECK ( l1ti_priority IS NOT NULL),
    CONSTRAINT            l1ti_definition_NN     CHECK ( l1ti_definition IS NOT NULL),
    CONSTRAINT            l1ti_group_NN         CHECK ( l1ti_group IS NOT NULL),
    CONSTRAINT            l1ti_monitor_NN         CHECK ( l1ti_monitor IS NOT NULL)	
--    CONSTRAINT            l1ti__NN         CHECK ( l1ti_partition IS NOT NULL)
);

-- This table gives a list a trigger thresholds. Please note that
-- also the RNDs, PSC and BG are listed here.  
CREATE TABLE l1_trigger_threshold (
    l1tt_id                 NUMBER(10),
    l1tt_name               VARCHAR2(100),
    l1tt_version            NUMBER(11),
    l1tt_type               VARCHAR2(6),
    l1tt_bitnum             NUMBER(10),
    l1tt_active             NUMBER(1),
    l1tt_mapping            NUMBER(10),
    l1tt_bcdelay            NUMBER(10),
    l1tt_seed               VARCHAR2(50),
    l1tt_seed_multi         NUMBER(10),
    CONSTRAINT            l1tt_pk             PRIMARY KEY (l1tt_id),
    CONSTRAINT            l1tt_nmver          UNIQUE (l1tt_name, l1tt_version),
    CONSTRAINT            l1tt_id_NN          CHECK ( l1tt_id IS NOT NULL),
    CONSTRAINT            l1tt_name_NN        CHECK ( l1tt_name IS NOT NULL),
    CONSTRAINT            l1tt_version_NN     CHECK ( l1tt_version IS NOT NULL),
    CONSTRAINT            l1tt_type_NN        CHECK ( l1tt_type IS NOT NULL),
    CONSTRAINT            l1tt_bitnum_NN      CHECK ( l1tt_bitnum IS NOT NULL),
    CONSTRAINT            l1tt_mapping_NN     CHECK ( l1tt_mapping IS NOT NULL),   
    CONSTRAINT            l1tt_bcdelay_NN     CHECK ( l1tt_bcdelay IS NOT NULL),
    CONSTRAINT            l1tt_seed_NN        CHECK ( l1tt_seed IS NOT NULL),
    CONSTRAINT            l1tt_seed_multi_NN  CHECK ( l1tt_seed_multi IS NOT NULL)
);

-- This table holds a list of all trigger threshold values. This
-- is relevant for all calo thresholds which could be angular dependent.
-- For muon thresholds we re-use the same table because of simplicity.
-- Aslo the RND, PSC and BG are listed here. The pt_cut value of thes
-- thresholds are the reference to the list attched to the trigger menu.
-- e.g. a BG with ptcut=4, means the BG that is attached to the trigger
-- menu via the BG set and has an internal number of 4.
CREATE TABLE l1_trigger_threshold_value (
    l1ttv_id             NUMBER(10),
    l1ttv_name             VARCHAR2(50),
    l1ttv_version             NUMBER(11),
    l1ttv_type            VARCHAR2(10),
    l1ttv_pt_cut             VARCHAR2(10),
    l1ttv_eta_min             NUMBER(10),
    l1ttv_eta_max             NUMBER(10),
    l1ttv_phi_min             NUMBER(10),
    l1ttv_phi_max             NUMBER(10),
    l1ttv_em_isolation         VARCHAR2(10),
    l1ttv_had_isolation         VARCHAR2(10),
    l1ttv_had_veto             VARCHAR2(10),
    l1ttv_window             NUMBER(10),
    l1ttv_priority            VARCHAR2(10),
    CONSTRAINT            l1ttv_pk        PRIMARY KEY (l1ttv_id),
    CONSTRAINT            l1ttv_nmver        UNIQUE (l1ttv_name, l1ttv_version),
    CONSTRAINT            l1ttv_id_NN         CHECK ( l1ttv_id IS NOT NULL),
    CONSTRAINT            l1ttv_name_NN         CHECK ( l1ttv_name IS NOT NULL),
    CONSTRAINT            l1ttv_version_NN     CHECK ( l1ttv_version IS NOT NULL),
    CONSTRAINT            l1ttv_type_NN         CHECK ( l1ttv_type IS NOT NULL),
    CONSTRAINT            l1ttv_pt_cut_NN     CHECK ( l1ttv_pt_cut IS NOT NULL)
);

-- This table is referenced by the table l1_calo_info.
-- There are 4 entires of l1_calo_min_tob.
CREATE TABLE l1_calo_min_tob (
    l1cmt_id                 NUMBER(10),
    l1cmt_thr_type           VARCHAR2(20),
    l1cmt_window             NUMBER(3) default 0,
    l1cmt_pt_min             NUMBER(6),
    l1cmt_eta_min            NUMBER(3),
    l1cmt_eta_max            NUMBER(3),
    l1cmt_priority           NUMBER(1),
    CONSTRAINT               l1cmt_pk                   PRIMARY KEY (l1cmt_id),
    CONSTRAINT               l1cmt_id_NN                CHECK (l1cmt_id       IS NOT NULL),
    CONSTRAINT               l1cmt_thr_type_NN          CHECK (l1cmt_thr_type IS NOT NULL),
    CONSTRAINT               l1cmt_window_NN            CHECK (l1cmt_window   IS NOT NULL),
    CONSTRAINT               l1cmt_pt_min_NN            CHECK (l1cmt_pt_min   IS NOT NULL),
    CONSTRAINT               l1cmt_eta_min_NN           CHECK (l1cmt_eta_min  IS NOT NULL),
    CONSTRAINT               l1cmt_eta_max_NN           CHECK (l1cmt_eta_max  IS NOT NULL),
    CONSTRAINT               l1cmt_priority_NN          CHECK (l1cmt_priority IS NOT NULL)
);

-- This table is referenced by the table l1_calo_isolation.
-- There are up to 5 entires of l1_calo_isoparam per l1_calo_isolation.
CREATE TABLE l1_calo_isoparam (
    l1cip_id                 NUMBER(10),
    l1cip_iso_bit            NUMBER(1) default 0,
    l1cip_offset             NUMBER(6) default 0,
    l1cip_slope              NUMBER(6) default 0,
    l1cip_min_cut            NUMBER(6) default 0,
    l1cip_upper_limit        NUMBER(6) default 0,
    l1cip_eta_min            NUMBER(3) default 0,
    l1cip_eta_max            NUMBER(3) default 0,
    l1cip_priority           NUMBER(1) default 0,
    CONSTRAINT               l1cip_pk                   PRIMARY KEY (l1cip_id),
    CONSTRAINT               l1cip_id_NN                CHECK (l1cip_id          IS NOT NULL),
    CONSTRAINT               l1cip_iso_bit_NN           CHECK (l1cip_iso_bit     IS NOT NULL),
    CONSTRAINT               l1cip_offset_NN            CHECK (l1cip_offset      IS NOT NULL),
    CONSTRAINT               l1cip_slope_NN             CHECK (l1cip_slope       IS NOT NULL),
    CONSTRAINT               l1cip_min_cut_NN           CHECK (l1cip_min_cut     IS NOT NULL),
    CONSTRAINT               l1cip_upper_limit_NN       CHECK (l1cip_upper_limit IS NOT NULL),
    CONSTRAINT               l1cip_eta_min_NN           CHECK (l1cip_eta_min     IS NOT NULL),
    CONSTRAINT               l1cip_eta_max_NN           CHECK (l1cip_eta_max     IS NOT NULL),
    CONSTRAINT               l1cip_priority_NN          CHECK (l1cip_priority    IS NOT NULL)
);

-- This table is referenced by the table l1_calo_info.
-- There are 3 entires of l1_calo_isolation.
CREATE TABLE l1_calo_isolation (
    l1cis_id                 NUMBER(10),
    l1cis_thr_type           VARCHAR2(20),
    l1cis_par1_id            NUMBER(10),
    l1cis_par2_id            NUMBER(10),
    l1cis_par3_id            NUMBER(10),
    l1cis_par4_id            NUMBER(10),
    l1cis_par5_id            NUMBER(10),
    CONSTRAINT               l1cis_pk                   PRIMARY KEY (l1cis_id),
    CONSTRAINT               l1cis_fk_par1              FOREIGN KEY (l1cis_par1_id) REFERENCES l1_calo_isoparam(l1cip_id),
    CONSTRAINT               l1cis_fk_par2              FOREIGN KEY (l1cis_par2_id) REFERENCES l1_calo_isoparam(l1cip_id),
    CONSTRAINT               l1cis_fk_par3              FOREIGN KEY (l1cis_par3_id) REFERENCES l1_calo_isoparam(l1cip_id),
    CONSTRAINT               l1cis_fk_par4              FOREIGN KEY (l1cis_par4_id) REFERENCES l1_calo_isoparam(l1cip_id),
    CONSTRAINT               l1cis_fk_par5              FOREIGN KEY (l1cis_par5_id) REFERENCES l1_calo_isoparam(l1cip_id),
    CONSTRAINT               l1cis_id_NN                CHECK (l1cis_id       IS NOT NULL),
    CONSTRAINT               l1cis_thr_type_NN          CHECK (l1cis_thr_type IS NOT NULL),
    CONSTRAINT               l1cis_par1_id_NN           CHECK (l1cis_par1_id  IS NOT NULL),
    CONSTRAINT               l1cis_par2_id_NN           CHECK (l1cis_par2_id  IS NOT NULL),
    CONSTRAINT               l1cis_par3_id_NN           CHECK (l1cis_par3_id  IS NOT NULL),
    CONSTRAINT               l1cis_par4_id_NN           CHECK (l1cis_par4_id  IS NOT NULL),
    CONSTRAINT               l1cis_par5_id_NN           CHECK (l1cis_par5_id  IS NOT NULL)
);
CREATE INDEX l1cis_par1_id_ind ON l1_calo_isolation(l1cis_par1_id) COMPRESS 1;
CREATE INDEX l1cis_par2_id_ind ON l1_calo_isolation(l1cis_par2_id) COMPRESS 1;
CREATE INDEX l1cis_par3_id_ind ON l1_calo_isolation(l1cis_par3_id) COMPRESS 1;
CREATE INDEX l1cis_par4_id_ind ON l1_calo_isolation(l1cis_par4_id) COMPRESS 1;
CREATE INDEX l1cis_par5_id_ind ON l1_calo_isolation(l1cis_par5_id) COMPRESS 1;


CREATE TABLE l1_calo_info (
    l1ci_id                  NUMBER(10),  
    l1ci_name                VARCHAR2(50),
    l1ci_version             NUMBER(11),
    l1ci_min_tob_em          NUMBER(10),
    l1ci_min_tob_tau         NUMBER(10),
    l1ci_min_tob_jets        NUMBER(10),
    l1ci_min_tob_jetl        NUMBER(10),
    l1ci_iso_ha_em           NUMBER(10),
    l1ci_iso_em_em           NUMBER(10),
    l1ci_iso_em_tau          NUMBER(10),
    l1ci_global_jet_scale    NUMBER(4,2) default 1,
    l1ci_global_em_scale     NUMBER(4,2) default 1,
    l1ci_xs_sigma_scale      NUMBER(10) default 0,
    l1ci_xs_sigma_offset     NUMBER(10) default 0,
    l1ci_xs_xe_min           NUMBER(3) default 0,
    l1ci_xs_xe_max           NUMBER(3) default 0,
    l1ci_xs_tesqrt_min       NUMBER(3) default 0,
    l1ci_xs_tesqrt_max       NUMBER(3) default 0,
    l1ci_xe_red_eta_min      NUMBER(4) default 0,
    l1ci_xe_red_eta_max      NUMBER(4) default 0,
    CONSTRAINT               l1ci_pk                  PRIMARY KEY (l1ci_id),
    CONSTRAINT               l1ci_fk_min_tob_em       FOREIGN KEY (l1ci_min_tob_em)   REFERENCES l1_calo_min_tob(l1cmt_id),
    CONSTRAINT               l1ci_fk_min_tob_tau      FOREIGN KEY (l1ci_min_tob_tau)  REFERENCES l1_calo_min_tob(l1cmt_id),
    CONSTRAINT               l1ci_fk_min_tob_jets     FOREIGN KEY (l1ci_min_tob_jets) REFERENCES l1_calo_min_tob(l1cmt_id),
    CONSTRAINT               l1ci_fk_min_tob_jetsl    FOREIGN KEY (l1ci_min_tob_jetl) REFERENCES l1_calo_min_tob(l1cmt_id),
    CONSTRAINT               l1ci_fk_iso_ha_em        FOREIGN KEY (l1ci_iso_ha_em)  REFERENCES l1_calo_isolation(l1cis_id),
    CONSTRAINT               l1ci_fk_iso_em_em        FOREIGN KEY (l1ci_iso_em_em)  REFERENCES l1_calo_isolation(l1cis_id),
    CONSTRAINT               l1ci_fk_iso_em_tau       FOREIGN KEY (l1ci_iso_em_tau) REFERENCES l1_calo_isolation(l1cis_id),
    CONSTRAINT               l1ci_id_NN               CHECK (l1ci_id IS NOT NULL),
    CONSTRAINT               l1ci_name_NN             CHECK (l1ci_name IS NOT NULL),
    CONSTRAINT               l1ci_version_NN          CHECK (l1ci_version IS NOT NULL),
    CONSTRAINT               l1ci_min_tob_em_NN       CHECK (l1ci_min_tob_em   IS NOT NULL),
    CONSTRAINT               l1ci_min_tob_tau_NN      CHECK (l1ci_min_tob_tau  IS NOT NULL),
    CONSTRAINT               l1ci_min_tob_jets_NN     CHECK (l1ci_min_tob_jets IS NOT NULL),
    CONSTRAINT               l1ci_min_tob_jetl_NN     CHECK (l1ci_min_tob_jetl IS NOT NULL),
    CONSTRAINT               l1ci_iso_ha_em_NN        CHECK (l1ci_iso_ha_em  IS NOT NULL),
    CONSTRAINT               l1ci_iso_em_em_NN        CHECK (l1ci_iso_em_em  IS NOT NULL),
    CONSTRAINT               l1ci_iso_em_tau_NN       CHECK (l1ci_iso_em_tau IS NOT NULL),
    CONSTRAINT               l1ci_global_jet_scale_NN CHECK (l1ci_global_jet_scale IS NOT NULL),
    CONSTRAINT               l1ci_global_em_scale_NN  CHECK (l1ci_global_em_scale  IS NOT NULL),
    CONSTRAINT               l1ci_xs_sigma_scale_NN   CHECK (l1ci_xs_sigma_scale  IS NOT NULL),
    CONSTRAINT               l1ci_xs_sigma_offset_NN  CHECK (l1ci_xs_sigma_offset IS NOT NULL),
    CONSTRAINT               l1ci_xs_xe_min_NN        CHECK (l1ci_xs_xe_min       IS NOT NULL),
    CONSTRAINT               l1ci_xs_xe_max_NN        CHECK (l1ci_xs_xe_max       IS NOT NULL),
    CONSTRAINT               l1ci_xs_tesqrt_min_NN    CHECK (l1ci_xs_tesqrt_min   IS NOT NULL),
    CONSTRAINT               l1ci_xs_tesqrt_max_NN    CHECK (l1ci_xs_tesqrt_max   IS NOT NULL),
    CONSTRAINT               l1ci_xe_red_eta_min_NN   CHECK (l1ci_xe_red_eta_min  IS NOT NULL),
    CONSTRAINT               l1ci_xe_red_eta_max_NN   CHECK (l1ci_xe_red_eta_max  IS NOT NULL)
);    

CREATE INDEX l1ci_min_tob_em_ind ON l1_calo_info(l1ci_min_tob_em) COMPRESS 1;
CREATE INDEX l1ci_min_tob_tau_ind ON l1_calo_info(l1ci_min_tob_tau) COMPRESS 1;
CREATE INDEX l1ci_min_tob_jets_ind ON l1_calo_info(l1ci_min_tob_jets) COMPRESS 1;
CREATE INDEX l1ci_min_tob_jetl_ind ON l1_calo_info(l1ci_min_tob_jetl) COMPRESS 1;
CREATE INDEX l1ci_iso_ha_em_ind ON l1_calo_info(l1ci_iso_ha_em) COMPRESS 1;
CREATE INDEX l1ci_iso_em_em_ind ON l1_calo_info(l1ci_iso_em_em) COMPRESS 1;
CREATE INDEX l1ci_iso_em_tau_ind ON l1_calo_info(l1ci_iso_em_tau) COMPRESS 1;

-- This table holds the list of available threshold sets for the
-- Lvl1 muon trigger. The configured thresholds must map on to the 
-- the values and the position of one of the available threshold sets
-- or the corresponding configuration is not viable. This table
-- should not be referenced directly, as it may move to the muon trigger
-- world. It is used by the trigger tool to verify configurations.

CREATE TABLE l1_muon_threshold_set (
    l1mts_id                    NUMBER(10), 
    l1mts_name                    VARCHAR2(200),
    l1mts_version                    NUMBER(11),    
    l1mts_rpc_available               NUMBER(1) default 0,    
    l1mts_rpc_available_online         NUMBER(1) default 0,    
    l1mts_rpc_set_ext_id         NUMBER(10) default 0,     
    l1mts_rpc_set_name         VARCHAR2(200) default "",     
    l1mts_rpc_pt1_ext_id         NUMBER(10) default 0,     
    l1mts_rpc_pt2_ext_id         NUMBER(10) default 0,     
    l1mts_rpc_pt3_ext_id         NUMBER(10) default 0,     
    l1mts_rpc_pt4_ext_id         NUMBER(10) default 0,     
    l1mts_rpc_pt5_ext_id         NUMBER(10) default 0,     
    l1mts_rpc_pt6_ext_id         NUMBER(10) default 0,     
    l1mts_tgc_available          NUMBER(1) default 0,    
    l1mts_tgc_available_online     NUMBER(1) default 0,      
        l1mts_tgc_set_ext_id         NUMBER(10) default 0,     
        l1mts_tgc_set_name         VARCHAR2(200) default "",     
    CONSTRAINT             l1mts_pk        PRIMARY KEY (l1mts_id),
    CONSTRAINT              l1mts_nmver        UNIQUE (l1mts_name, l1mts_version),
    CONSTRAINT            l1mts_id_NN         CHECK ( l1mts_id IS NOT NULL),
    CONSTRAINT            l1mts_name_NN         CHECK ( l1mts_name IS NOT NULL),
    CONSTRAINT            l1mts_version_NN     CHECK ( l1mts_version IS NOT NULL),
    CONSTRAINT            l1mts_rpc_avail_NN     CHECK ( l1mts_rpc_available IS NOT NULL),
    CONSTRAINT            l1mts_rpc_avail_onl_NN     CHECK ( l1mts_rpc_available_online IS NOT NULL),
    CONSTRAINT            l1mts_rpc_set_ext_id_NN CHECK ( l1mts_rpc_set_ext_id IS NOT NULL),
    CONSTRAINT            l1mts_rpc_set_name_NN     CHECK ( l1mts_rpc_set_name IS NOT NULL),
    CONSTRAINT            l1mts_rpc_pt1_ext_id_NN CHECK ( l1mts_rpc_pt1_ext_id IS NOT NULL),
    CONSTRAINT            l1mts_rpc_pt2_ext_id_NN CHECK ( l1mts_rpc_pt2_ext_id IS NOT NULL),
    CONSTRAINT            l1mts_rpc_pt3_ext_id_NN CHECK ( l1mts_rpc_pt3_ext_id IS NOT NULL),
    CONSTRAINT            l1mts_rpc_pt4_ext_id_NN CHECK ( l1mts_rpc_pt4_ext_id IS NOT NULL),
    CONSTRAINT            l1mts_rpc_pt5_ext_id_NN CHECK ( l1mts_rpc_pt5_ext_id IS NOT NULL),
    CONSTRAINT            l1mts_rpc_pt6_ext_id_NN CHECK ( l1mts_rpc_pt6_ext_id IS NOT NULL),
    CONSTRAINT            l1mts_tgc_avail_NN     CHECK ( l1mts_tgc_available IS NOT NULL),
    CONSTRAINT            l1mts_tgc_avail_onl_NN     CHECK ( l1mts_tgc_available_online IS NOT NULL),
    CONSTRAINT            l1mts_tgc_set_ext_id_NN CHECK ( l1mts_tgc_set_ext_id IS NOT NULL),
    CONSTRAINT            l1mts_tgc_set_name_NN     CHECK ( l1mts_tgc_set_name IS NOT NULL)
);


-- In this table the prescale factors for the
-- trigger items are stored
CREATE TABLE l1_prescale_set (
    l1ps_id             NUMBER(10),
    l1ps_name           VARCHAR2(65),
    l1ps_version        NUMBER(11),
    l1ps_comment        VARCHAR2(200),
    l1ps_lumi           VARCHAR2(10),
    l1ps_shift_safe     NUMBER(1),
    l1ps_default        NUMBER(1),
    l1ps_partition      NUMBER(1),     
    l1ps_type           VARCHAR2(200),     
    l1ps_val1           NUMBER(12),
    l1ps_val2           NUMBER(12),
    l1ps_val3           NUMBER(12),
    l1ps_val4           NUMBER(12),
    l1ps_val5           NUMBER(12),
    l1ps_val6           NUMBER(12),
    l1ps_val7           NUMBER(12),
    l1ps_val8           NUMBER(12),
    l1ps_val9           NUMBER(12),
    l1ps_val10          NUMBER(12),
    l1ps_val11          NUMBER(12),
    l1ps_val12          NUMBER(12),
    l1ps_val13          NUMBER(12),
    l1ps_val14          NUMBER(12),
    l1ps_val15          NUMBER(12),
    l1ps_val16          NUMBER(12),
    l1ps_val17          NUMBER(12),
    l1ps_val18          NUMBER(12),
    l1ps_val19          NUMBER(12),
    l1ps_val20          NUMBER(12),
    l1ps_val21          NUMBER(12),
    l1ps_val22          NUMBER(12),
    l1ps_val23          NUMBER(12),
    l1ps_val24          NUMBER(12),
    l1ps_val25            NUMBER(12),
    l1ps_val26            NUMBER(12),
    l1ps_val27            NUMBER(12),
    l1ps_val28            NUMBER(12),
    l1ps_val29            NUMBER(12),
    l1ps_val30            NUMBER(12),
    l1ps_val31            NUMBER(12),
    l1ps_val32            NUMBER(12),
    l1ps_val33            NUMBER(12),
    l1ps_val34            NUMBER(12),
    l1ps_val35            NUMBER(12),
    l1ps_val36            NUMBER(12),
    l1ps_val37            NUMBER(12),
    l1ps_val38            NUMBER(12),
    l1ps_val39            NUMBER(12),
    l1ps_val40            NUMBER(12),
    l1ps_val41            NUMBER(12),
    l1ps_val42            NUMBER(12),
    l1ps_val43            NUMBER(12),
    l1ps_val44            NUMBER(12),
    l1ps_val45            NUMBER(12),
    l1ps_val46            NUMBER(12),
    l1ps_val47            NUMBER(12),
    l1ps_val48            NUMBER(12),
    l1ps_val49            NUMBER(12),
    l1ps_val50            NUMBER(12),
    l1ps_val51            NUMBER(12),
    l1ps_val52            NUMBER(12),
    l1ps_val53            NUMBER(12),
    l1ps_val54            NUMBER(12),
    l1ps_val55            NUMBER(12),
    l1ps_val56            NUMBER(12),
    l1ps_val57            NUMBER(12),
    l1ps_val58            NUMBER(12),
    l1ps_val59            NUMBER(12),
    l1ps_val60            NUMBER(12),
    l1ps_val61            NUMBER(12),
    l1ps_val62            NUMBER(12),
    l1ps_val63            NUMBER(12),
    l1ps_val64            NUMBER(12),
    l1ps_val65            NUMBER(12),
    l1ps_val66            NUMBER(12),
    l1ps_val67            NUMBER(12),
    l1ps_val68            NUMBER(12),
    l1ps_val69            NUMBER(12),
    l1ps_val70            NUMBER(12),
    l1ps_val71            NUMBER(12),
    l1ps_val72            NUMBER(12),
    l1ps_val73            NUMBER(12),
    l1ps_val74            NUMBER(12),
    l1ps_val75            NUMBER(12),
    l1ps_val76            NUMBER(12),
    l1ps_val77            NUMBER(12),
    l1ps_val78            NUMBER(12),
    l1ps_val79            NUMBER(12),
    l1ps_val80            NUMBER(12),
    l1ps_val81            NUMBER(12),
    l1ps_val82            NUMBER(12),
    l1ps_val83            NUMBER(12),
    l1ps_val84            NUMBER(12),
    l1ps_val85            NUMBER(12),
    l1ps_val86            NUMBER(12),
    l1ps_val87            NUMBER(12),
    l1ps_val88            NUMBER(12),
    l1ps_val89            NUMBER(12),
    l1ps_val90            NUMBER(12),
    l1ps_val91            NUMBER(12),
    l1ps_val92            NUMBER(12),
    l1ps_val93            NUMBER(12),
    l1ps_val94            NUMBER(12),
    l1ps_val95            NUMBER(12),
    l1ps_val96            NUMBER(12),
    l1ps_val97            NUMBER(12),
    l1ps_val98            NUMBER(12),
    l1ps_val99            NUMBER(12),
    l1ps_val100            NUMBER(12),
    l1ps_val101            NUMBER(12),
    l1ps_val102            NUMBER(12),
    l1ps_val103            NUMBER(12),
    l1ps_val104            NUMBER(12),
    l1ps_val105            NUMBER(12),
    l1ps_val106            NUMBER(12),
    l1ps_val107            NUMBER(12),
    l1ps_val108            NUMBER(12),
    l1ps_val109            NUMBER(12),
    l1ps_val110            NUMBER(12),
    l1ps_val111            NUMBER(12),
    l1ps_val112            NUMBER(12),
    l1ps_val113            NUMBER(12),
    l1ps_val114            NUMBER(12),
    l1ps_val115            NUMBER(12),
    l1ps_val116            NUMBER(12),
    l1ps_val117            NUMBER(12),
    l1ps_val118            NUMBER(12),
    l1ps_val119            NUMBER(12),
    l1ps_val120            NUMBER(12),
    l1ps_val121            NUMBER(12),
    l1ps_val122            NUMBER(12),
    l1ps_val123            NUMBER(12),
    l1ps_val124            NUMBER(12),
    l1ps_val125            NUMBER(12),
    l1ps_val126            NUMBER(12),
    l1ps_val127            NUMBER(12),
    l1ps_val128            NUMBER(12),
    l1ps_val129            NUMBER(12),
    l1ps_val130            NUMBER(12),
    l1ps_val131            NUMBER(12),
    l1ps_val132            NUMBER(12),
    l1ps_val133            NUMBER(12),
    l1ps_val134            NUMBER(12),
    l1ps_val135            NUMBER(12),
    l1ps_val136            NUMBER(12),
    l1ps_val137            NUMBER(12),
    l1ps_val138            NUMBER(12),
    l1ps_val139            NUMBER(12),
    l1ps_val140            NUMBER(12),
    l1ps_val141            NUMBER(12),
    l1ps_val142            NUMBER(12),
    l1ps_val143            NUMBER(12),
    l1ps_val144            NUMBER(12),
    l1ps_val145            NUMBER(12),
    l1ps_val146            NUMBER(12),
    l1ps_val147            NUMBER(12),
    l1ps_val148            NUMBER(12),
    l1ps_val149            NUMBER(12),
    l1ps_val150            NUMBER(12),    
    l1ps_val151            NUMBER(12),
    l1ps_val152            NUMBER(12),    
    l1ps_val153            NUMBER(12),    
    l1ps_val154            NUMBER(12),    
    l1ps_val155            NUMBER(12),    
    l1ps_val156            NUMBER(12),    
    l1ps_val157            NUMBER(12),    
    l1ps_val158            NUMBER(12),    
    l1ps_val159            NUMBER(12),    
    l1ps_val160            NUMBER(12),    
    l1ps_val161            NUMBER(12),    
    l1ps_val162            NUMBER(12),    
    l1ps_val163            NUMBER(12),    
    l1ps_val164            NUMBER(12),    
    l1ps_val165            NUMBER(12),    
    l1ps_val166            NUMBER(12),    
    l1ps_val167            NUMBER(12),    
    l1ps_val168            NUMBER(12),    
    l1ps_val169            NUMBER(12),    
    l1ps_val170            NUMBER(12),    
    l1ps_val171            NUMBER(12),    
    l1ps_val172            NUMBER(12),    
    l1ps_val173            NUMBER(12),    
    l1ps_val174            NUMBER(12),    
    l1ps_val175            NUMBER(12),    
    l1ps_val176            NUMBER(12),    
    l1ps_val177            NUMBER(12),    
    l1ps_val178            NUMBER(12),    
    l1ps_val179            NUMBER(12),    
    l1ps_val180            NUMBER(12),    
    l1ps_val181            NUMBER(12),    
    l1ps_val182            NUMBER(12),    
    l1ps_val183            NUMBER(12),    
    l1ps_val184            NUMBER(12),    
    l1ps_val185            NUMBER(12),    
    l1ps_val186            NUMBER(12),    
    l1ps_val187            NUMBER(12),    
    l1ps_val188            NUMBER(12),    
    l1ps_val189            NUMBER(12),    
    l1ps_val190            NUMBER(12),    
    l1ps_val191            NUMBER(12),    
    l1ps_val192            NUMBER(12),    
    l1ps_val193            NUMBER(12),    
    l1ps_val194            NUMBER(12),    
    l1ps_val195            NUMBER(12),    
    l1ps_val196            NUMBER(12),    
    l1ps_val197            NUMBER(12),    
    l1ps_val198            NUMBER(12),    
    l1ps_val199            NUMBER(12),
    l1ps_val200            NUMBER(12),
    l1ps_val201            NUMBER(12),
    l1ps_val202            NUMBER(12),
    l1ps_val203            NUMBER(12),
    l1ps_val204            NUMBER(12),
    l1ps_val205            NUMBER(12),
    l1ps_val206            NUMBER(12),
    l1ps_val207            NUMBER(12),
    l1ps_val208            NUMBER(12),
    l1ps_val209            NUMBER(12),
    l1ps_val210            NUMBER(12),
    l1ps_val211            NUMBER(12),
    l1ps_val212            NUMBER(12),
    l1ps_val213            NUMBER(12),
    l1ps_val214            NUMBER(12),
    l1ps_val215            NUMBER(12),
    l1ps_val216            NUMBER(12),
    l1ps_val217            NUMBER(12),
    l1ps_val218            NUMBER(12),
    l1ps_val219            NUMBER(12),
    l1ps_val220            NUMBER(12),
    l1ps_val221            NUMBER(12),
    l1ps_val222            NUMBER(12),
    l1ps_val223            NUMBER(12),
    l1ps_val224            NUMBER(12),
    l1ps_val225            NUMBER(12),
    l1ps_val226            NUMBER(12),
    l1ps_val227            NUMBER(12),
    l1ps_val228            NUMBER(12),
    l1ps_val229            NUMBER(12),
    l1ps_val230            NUMBER(12),    
    l1ps_val231            NUMBER(12),    
    l1ps_val232            NUMBER(12),    
    l1ps_val233            NUMBER(12),    
    l1ps_val234            NUMBER(12),    
    l1ps_val235            NUMBER(12),    
    l1ps_val236            NUMBER(12),    
    l1ps_val237            NUMBER(12),    
    l1ps_val238            NUMBER(12),    
    l1ps_val239            NUMBER(12),    
    l1ps_val240            NUMBER(12),    
    l1ps_val241            NUMBER(12),    
    l1ps_val242            NUMBER(12),    
    l1ps_val243            NUMBER(12),    
    l1ps_val244            NUMBER(12),    
    l1ps_val245            NUMBER(12),    
    l1ps_val246            NUMBER(12),    
    l1ps_val247            NUMBER(12),    
    l1ps_val248            NUMBER(12),    
    l1ps_val249            NUMBER(12),    
    l1ps_val250            NUMBER(12),    
    l1ps_val251            NUMBER(12),    
    l1ps_val252            NUMBER(12),    
    l1ps_val253            NUMBER(12),    
    l1ps_val254            NUMBER(12),    
    l1ps_val255            NUMBER(12),    
    l1ps_val256            NUMBER(12),
    L1PS_VAL257 NUMBER(12), 
    L1PS_VAL258 NUMBER(12), 
    L1PS_VAL259 NUMBER(12), 
    L1PS_VAL260 NUMBER(12), 
    L1PS_VAL261 NUMBER(12), 
    L1PS_VAL262 NUMBER(12), 
    L1PS_VAL263 NUMBER(12), 
    L1PS_VAL264 NUMBER(12), 
    L1PS_VAL265 NUMBER(12), 
    L1PS_VAL266 NUMBER(12), 
    L1PS_VAL267 NUMBER(12), 
    L1PS_VAL268 NUMBER(12), 
    L1PS_VAL269 NUMBER(12), 
    L1PS_VAL270 NUMBER(12), 
    L1PS_VAL271 NUMBER(12), 
    L1PS_VAL272 NUMBER(12), 
    L1PS_VAL273 NUMBER(12), 
    L1PS_VAL274 NUMBER(12), 
    L1PS_VAL275 NUMBER(12), 
    L1PS_VAL276 NUMBER(12), 
    L1PS_VAL277 NUMBER(12), 
    L1PS_VAL278 NUMBER(12), 
    L1PS_VAL279 NUMBER(12), 
    L1PS_VAL280 NUMBER(12), 
    L1PS_VAL281 NUMBER(12), 
    L1PS_VAL282 NUMBER(12), 
    L1PS_VAL283 NUMBER(12), 
    L1PS_VAL284 NUMBER(12), 
    L1PS_VAL285 NUMBER(12), 
    L1PS_VAL286 NUMBER(12), 
    L1PS_VAL287 NUMBER(12), 
    L1PS_VAL288 NUMBER(12), 
    L1PS_VAL289 NUMBER(12), 
    L1PS_VAL290 NUMBER(12), 
    L1PS_VAL291 NUMBER(12), 
    L1PS_VAL292 NUMBER(12), 
    L1PS_VAL293 NUMBER(12), 
    L1PS_VAL294 NUMBER(12), 
    L1PS_VAL295 NUMBER(12), 
    L1PS_VAL296 NUMBER(12), 
    L1PS_VAL297 NUMBER(12), 
    L1PS_VAL298 NUMBER(12), 
    L1PS_VAL299 NUMBER(12), 
    L1PS_VAL300 NUMBER(12), 
    L1PS_VAL301 NUMBER(12), 
    L1PS_VAL302 NUMBER(12), 
    L1PS_VAL303 NUMBER(12), 
    L1PS_VAL304 NUMBER(12), 
    L1PS_VAL305 NUMBER(12), 
    L1PS_VAL306 NUMBER(12), 
    L1PS_VAL307 NUMBER(12), 
    L1PS_VAL308 NUMBER(12), 
    L1PS_VAL309 NUMBER(12), 
    L1PS_VAL310 NUMBER(12), 
    L1PS_VAL311 NUMBER(12), 
    L1PS_VAL312 NUMBER(12), 
    L1PS_VAL313 NUMBER(12), 
    L1PS_VAL314 NUMBER(12), 
    L1PS_VAL315 NUMBER(12), 
    L1PS_VAL316 NUMBER(12), 
    L1PS_VAL317 NUMBER(12), 
    L1PS_VAL318 NUMBER(12), 
    L1PS_VAL319 NUMBER(12), 
    L1PS_VAL320 NUMBER(12), 
    L1PS_VAL321 NUMBER(12), 
    L1PS_VAL322 NUMBER(12), 
    L1PS_VAL323 NUMBER(12), 
    L1PS_VAL324 NUMBER(12), 
    L1PS_VAL325 NUMBER(12), 
    L1PS_VAL326 NUMBER(12), 
    L1PS_VAL327 NUMBER(12), 
    L1PS_VAL328 NUMBER(12), 
    L1PS_VAL329 NUMBER(12), 
    L1PS_VAL330 NUMBER(12), 
    L1PS_VAL331 NUMBER(12), 
    L1PS_VAL332 NUMBER(12), 
    L1PS_VAL333 NUMBER(12), 
    L1PS_VAL334 NUMBER(12), 
    L1PS_VAL335 NUMBER(12), 
    L1PS_VAL336 NUMBER(12), 
    L1PS_VAL337 NUMBER(12), 
    L1PS_VAL338 NUMBER(12), 
    L1PS_VAL339 NUMBER(12), 
    L1PS_VAL340 NUMBER(12), 
    L1PS_VAL341 NUMBER(12), 
    L1PS_VAL342 NUMBER(12), 
    L1PS_VAL343 NUMBER(12), 
    L1PS_VAL344 NUMBER(12), 
    L1PS_VAL345 NUMBER(12), 
    L1PS_VAL346 NUMBER(12), 
    L1PS_VAL347 NUMBER(12), 
    L1PS_VAL348 NUMBER(12), 
    L1PS_VAL349 NUMBER(12), 
    L1PS_VAL350 NUMBER(12), 
    L1PS_VAL351 NUMBER(12), 
    L1PS_VAL352 NUMBER(12), 
    L1PS_VAL353 NUMBER(12), 
    L1PS_VAL354 NUMBER(12), 
    L1PS_VAL355 NUMBER(12), 
    L1PS_VAL356 NUMBER(12), 
    L1PS_VAL357 NUMBER(12), 
    L1PS_VAL358 NUMBER(12), 
    L1PS_VAL359 NUMBER(12), 
    L1PS_VAL360 NUMBER(12), 
    L1PS_VAL361 NUMBER(12), 
    L1PS_VAL362 NUMBER(12), 
    L1PS_VAL363 NUMBER(12), 
    L1PS_VAL364 NUMBER(12), 
    L1PS_VAL365 NUMBER(12), 
    L1PS_VAL366 NUMBER(12), 
    L1PS_VAL367 NUMBER(12), 
    L1PS_VAL368 NUMBER(12), 
    L1PS_VAL369 NUMBER(12), 
    L1PS_VAL370 NUMBER(12), 
    L1PS_VAL371 NUMBER(12), 
    L1PS_VAL372 NUMBER(12), 
    L1PS_VAL373 NUMBER(12), 
    L1PS_VAL374 NUMBER(12), 
    L1PS_VAL375 NUMBER(12), 
    L1PS_VAL376 NUMBER(12), 
    L1PS_VAL377 NUMBER(12), 
    L1PS_VAL378 NUMBER(12), 
    L1PS_VAL379 NUMBER(12), 
    L1PS_VAL380 NUMBER(12), 
    L1PS_VAL381 NUMBER(12), 
    L1PS_VAL382 NUMBER(12), 
    L1PS_VAL383 NUMBER(12), 
    L1PS_VAL384 NUMBER(12), 
    L1PS_VAL385 NUMBER(12), 
    L1PS_VAL386 NUMBER(12), 
    L1PS_VAL387 NUMBER(12), 
    L1PS_VAL388 NUMBER(12), 
    L1PS_VAL389 NUMBER(12), 
    L1PS_VAL390 NUMBER(12), 
    L1PS_VAL391 NUMBER(12), 
    L1PS_VAL392 NUMBER(12), 
    L1PS_VAL393 NUMBER(12), 
    L1PS_VAL394 NUMBER(12), 
    L1PS_VAL395 NUMBER(12), 
    L1PS_VAL396 NUMBER(12), 
    L1PS_VAL397 NUMBER(12), 
    L1PS_VAL398 NUMBER(12), 
    L1PS_VAL399 NUMBER(12), 
    L1PS_VAL400 NUMBER(12), 
    L1PS_VAL401 NUMBER(12), 
    L1PS_VAL402 NUMBER(12), 
    L1PS_VAL403 NUMBER(12), 
    L1PS_VAL404 NUMBER(12), 
    L1PS_VAL405 NUMBER(12), 
    L1PS_VAL406 NUMBER(12), 
    L1PS_VAL407 NUMBER(12), 
    L1PS_VAL408 NUMBER(12), 
    L1PS_VAL409 NUMBER(12), 
    L1PS_VAL410 NUMBER(12), 
    L1PS_VAL411 NUMBER(12), 
    L1PS_VAL412 NUMBER(12), 
    L1PS_VAL413 NUMBER(12), 
    L1PS_VAL414 NUMBER(12), 
    L1PS_VAL415 NUMBER(12), 
    L1PS_VAL416 NUMBER(12), 
    L1PS_VAL417 NUMBER(12), 
    L1PS_VAL418 NUMBER(12), 
    L1PS_VAL419 NUMBER(12), 
    L1PS_VAL420 NUMBER(12), 
    L1PS_VAL421 NUMBER(12), 
    L1PS_VAL422 NUMBER(12), 
    L1PS_VAL423 NUMBER(12), 
    L1PS_VAL424 NUMBER(12), 
    L1PS_VAL425 NUMBER(12), 
    L1PS_VAL426 NUMBER(12), 
    L1PS_VAL427 NUMBER(12), 
    L1PS_VAL428 NUMBER(12), 
    L1PS_VAL429 NUMBER(12), 
    L1PS_VAL430 NUMBER(12), 
    L1PS_VAL431 NUMBER(12), 
    L1PS_VAL432 NUMBER(12), 
    L1PS_VAL433 NUMBER(12), 
    L1PS_VAL434 NUMBER(12), 
    L1PS_VAL435 NUMBER(12), 
    L1PS_VAL436 NUMBER(12), 
    L1PS_VAL437 NUMBER(12), 
    L1PS_VAL438 NUMBER(12), 
    L1PS_VAL439 NUMBER(12), 
    L1PS_VAL440 NUMBER(12), 
    L1PS_VAL441 NUMBER(12), 
    L1PS_VAL442 NUMBER(12), 
    L1PS_VAL443 NUMBER(12), 
    L1PS_VAL444 NUMBER(12), 
    L1PS_VAL445 NUMBER(12), 
    L1PS_VAL446 NUMBER(12), 
    L1PS_VAL447 NUMBER(12), 
    L1PS_VAL448 NUMBER(12), 
    L1PS_VAL449 NUMBER(12), 
    L1PS_VAL450 NUMBER(12), 
    L1PS_VAL451 NUMBER(12), 
    L1PS_VAL452 NUMBER(12), 
    L1PS_VAL453 NUMBER(12), 
    L1PS_VAL454 NUMBER(12), 
    L1PS_VAL455 NUMBER(12), 
    L1PS_VAL456 NUMBER(12), 
    L1PS_VAL457 NUMBER(12), 
    L1PS_VAL458 NUMBER(12), 
    L1PS_VAL459 NUMBER(12), 
    L1PS_VAL460 NUMBER(12), 
    L1PS_VAL461 NUMBER(12), 
    L1PS_VAL462 NUMBER(12), 
    L1PS_VAL463 NUMBER(12), 
    L1PS_VAL464 NUMBER(12), 
    L1PS_VAL465 NUMBER(12), 
    L1PS_VAL466 NUMBER(12), 
    L1PS_VAL467 NUMBER(12), 
    L1PS_VAL468 NUMBER(12), 
    L1PS_VAL469 NUMBER(12), 
    L1PS_VAL470 NUMBER(12), 
    L1PS_VAL471 NUMBER(12), 
    L1PS_VAL472 NUMBER(12), 
    L1PS_VAL473 NUMBER(12), 
    L1PS_VAL474 NUMBER(12), 
    L1PS_VAL475 NUMBER(12), 
    L1PS_VAL476 NUMBER(12), 
    L1PS_VAL477 NUMBER(12), 
    L1PS_VAL478 NUMBER(12), 
    L1PS_VAL479 NUMBER(12), 
    L1PS_VAL480 NUMBER(12), 
    L1PS_VAL481 NUMBER(12), 
    L1PS_VAL482 NUMBER(12), 
    L1PS_VAL483 NUMBER(12), 
    L1PS_VAL484 NUMBER(12), 
    L1PS_VAL485 NUMBER(12), 
    L1PS_VAL486 NUMBER(12), 
    L1PS_VAL487 NUMBER(12), 
    L1PS_VAL488 NUMBER(12), 
    L1PS_VAL489 NUMBER(12), 
    L1PS_VAL490 NUMBER(12), 
    L1PS_VAL491 NUMBER(12), 
    L1PS_VAL492 NUMBER(12), 
    L1PS_VAL493 NUMBER(12), 
    L1PS_VAL494 NUMBER(12), 
    L1PS_VAL495 NUMBER(12), 
    L1PS_VAL496 NUMBER(12), 
    L1PS_VAL497 NUMBER(12), 
    L1PS_VAL498 NUMBER(12), 
    L1PS_VAL499 NUMBER(12), 
    L1PS_VAL500 NUMBER(12), 
    L1PS_VAL501 NUMBER(12), 
    L1PS_VAL502 NUMBER(12), 
    L1PS_VAL503 NUMBER(12), 
    L1PS_VAL504 NUMBER(12), 
    L1PS_VAL505 NUMBER(12), 
    L1PS_VAL506 NUMBER(12), 
    L1PS_VAL507 NUMBER(12), 
    L1PS_VAL508 NUMBER(12), 
    L1PS_VAL509 NUMBER(12), 
    L1PS_VAL510 NUMBER(12), 
    L1PS_VAL511 NUMBER(12), 
    L1PS_VAL512 NUMBER(12),
    l1ps_username            VARCHAR2(50),     
    l1ps_modified_time        TIMESTAMP,
    l1ps_used            CHAR            default 0, 
    CONSTRAINT            l1ps_pk            PRIMARY KEY (l1ps_id),
    CONSTRAINT             l1ps_nmver        UNIQUE (l1ps_name, l1ps_version),
    CONSTRAINT            l1ps_id_NN         CHECK ( l1ps_id IS NOT NULL),
    CONSTRAINT            l1ps_name_NN         CHECK ( l1ps_name IS NOT NULL),
    CONSTRAINT            l1ps_version_NN     CHECK ( l1ps_version IS NOT NULL),
    CONSTRAINT            l1ps_lumi_NN         CHECK ( l1ps_lumi IS NOT NULL),
    CONSTRAINT            l1ps_shift_safe_NN     CHECK ( l1ps_shift_safe IS NOT NULL),
    CONSTRAINT            l1ps_default_NN     CHECK ( l1ps_default IS NOT NULL),
--    CONSTRAINT            l1ps_partition_NN     CHECK ( l1ps_partition IS NOT NULL),
    CONSTRAINT            l1ps_type_NN         CHECK ( l1ps_type IS NOT NULL),
    CONSTRAINT            l1ps_val1_NN         CHECK ( l1ps_val1 IS NOT NULL),
    CONSTRAINT            l1ps_val2_NN         CHECK ( l1ps_val2 IS NOT NULL),
    CONSTRAINT            l1ps_val3_NN         CHECK ( l1ps_val3 IS NOT NULL),
    CONSTRAINT            l1ps_val4_NN         CHECK ( l1ps_val4 IS NOT NULL),
    CONSTRAINT            l1ps_val5_NN         CHECK ( l1ps_val5 IS NOT NULL),
    CONSTRAINT            l1ps_val6_NN         CHECK ( l1ps_val6 IS NOT NULL),
    CONSTRAINT            l1ps_val7_NN         CHECK ( l1ps_val7 IS NOT NULL),
    CONSTRAINT            l1ps_val8_NN         CHECK ( l1ps_val8 IS NOT NULL),
    CONSTRAINT            l1ps_val9_NN         CHECK ( l1ps_val9 IS NOT NULL),
    CONSTRAINT            l1ps_val10_NN         CHECK ( l1ps_val10 IS NOT NULL),
    CONSTRAINT            l1ps_val11_NN         CHECK ( l1ps_val11 IS NOT NULL),
    CONSTRAINT            l1ps_val12_NN         CHECK ( l1ps_val12 IS NOT NULL),
    CONSTRAINT            l1ps_val13_NN         CHECK ( l1ps_val13 IS NOT NULL),
    CONSTRAINT            l1ps_val14_NN         CHECK ( l1ps_val14 IS NOT NULL),
    CONSTRAINT            l1ps_val15_NN         CHECK ( l1ps_val15 IS NOT NULL),
    CONSTRAINT            l1ps_val16_NN         CHECK ( l1ps_val16 IS NOT NULL),
    CONSTRAINT            l1ps_val17_NN         CHECK ( l1ps_val17 IS NOT NULL),
    CONSTRAINT            l1ps_val18_NN         CHECK ( l1ps_val18 IS NOT NULL),
    CONSTRAINT            l1ps_val19_NN         CHECK ( l1ps_val19 IS NOT NULL),
    CONSTRAINT            l1ps_val20_NN         CHECK ( l1ps_val20 IS NOT NULL),
    CONSTRAINT            l1ps_val21_NN         CHECK ( l1ps_val21 IS NOT NULL),
    CONSTRAINT            l1ps_val22_NN         CHECK ( l1ps_val22 IS NOT NULL),
    CONSTRAINT            l1ps_val23_NN         CHECK ( l1ps_val23 IS NOT NULL),
    CONSTRAINT            l1ps_val24_NN         CHECK ( l1ps_val24 IS NOT NULL),
    CONSTRAINT            l1ps_val25_NN         CHECK ( l1ps_val25 IS NOT NULL),
    CONSTRAINT            l1ps_val26_NN         CHECK ( l1ps_val26 IS NOT NULL),
    CONSTRAINT            l1ps_val27_NN         CHECK ( l1ps_val27 IS NOT NULL),
    CONSTRAINT            l1ps_val28_NN         CHECK ( l1ps_val28 IS NOT NULL),
    CONSTRAINT            l1ps_val29_NN         CHECK ( l1ps_val29 IS NOT NULL),
    CONSTRAINT            l1ps_val30_NN         CHECK ( l1ps_val30 IS NOT NULL),
    CONSTRAINT            l1ps_val31_NN         CHECK ( l1ps_val31 IS NOT NULL),
    CONSTRAINT            l1ps_val32_NN         CHECK ( l1ps_val32 IS NOT NULL),
    CONSTRAINT            l1ps_val33_NN         CHECK ( l1ps_val33 IS NOT NULL),
    CONSTRAINT            l1ps_val34_NN         CHECK ( l1ps_val34 IS NOT NULL),
    CONSTRAINT            l1ps_val35_NN         CHECK ( l1ps_val35 IS NOT NULL),
    CONSTRAINT            l1ps_val36_NN         CHECK ( l1ps_val36 IS NOT NULL),
    CONSTRAINT            l1ps_val37_NN         CHECK ( l1ps_val37 IS NOT NULL),
    CONSTRAINT            l1ps_val38_NN         CHECK ( l1ps_val38 IS NOT NULL),
    CONSTRAINT            l1ps_val39_NN         CHECK ( l1ps_val39 IS NOT NULL),
    CONSTRAINT            l1ps_val40_NN         CHECK ( l1ps_val40 IS NOT NULL),
    CONSTRAINT            l1ps_val41_NN         CHECK ( l1ps_val14 IS NOT NULL),
    CONSTRAINT            l1ps_val42_NN         CHECK ( l1ps_val42 IS NOT NULL),
    CONSTRAINT            l1ps_val43_NN         CHECK ( l1ps_val43 IS NOT NULL),
    CONSTRAINT            l1ps_val44_NN         CHECK ( l1ps_val44 IS NOT NULL),
    CONSTRAINT            l1ps_val45_NN         CHECK ( l1ps_val45 IS NOT NULL),
    CONSTRAINT            l1ps_val46_NN         CHECK ( l1ps_val46 IS NOT NULL),
    CONSTRAINT            l1ps_val47_NN         CHECK ( l1ps_val47 IS NOT NULL),
    CONSTRAINT            l1ps_val48_NN         CHECK ( l1ps_val48 IS NOT NULL),
    CONSTRAINT            l1ps_val49_NN         CHECK ( l1ps_val49 IS NOT NULL),
    CONSTRAINT            l1ps_val50_NN         CHECK ( l1ps_val50 IS NOT NULL),
    CONSTRAINT            l1ps_val51_NN         CHECK ( l1ps_val51 IS NOT NULL),
    CONSTRAINT            l1ps_val52_NN         CHECK ( l1ps_val52 IS NOT NULL),
    CONSTRAINT            l1ps_val53_NN         CHECK ( l1ps_val53 IS NOT NULL),
    CONSTRAINT            l1ps_val54_NN         CHECK ( l1ps_val54 IS NOT NULL),
    CONSTRAINT            l1ps_val55_NN         CHECK ( l1ps_val55 IS NOT NULL),
    CONSTRAINT            l1ps_val56_NN         CHECK ( l1ps_val56 IS NOT NULL),
    CONSTRAINT            l1ps_val57_NN         CHECK ( l1ps_val57 IS NOT NULL),
    CONSTRAINT            l1ps_val58_NN         CHECK ( l1ps_val58 IS NOT NULL),
    CONSTRAINT            l1ps_val59_NN         CHECK ( l1ps_val59 IS NOT NULL),
    CONSTRAINT            l1ps_val60_NN         CHECK ( l1ps_val60 IS NOT NULL),
    CONSTRAINT            l1ps_val61_NN         CHECK ( l1ps_val61 IS NOT NULL),
    CONSTRAINT            l1ps_val62_NN         CHECK ( l1ps_val62 IS NOT NULL),
    CONSTRAINT            l1ps_val63_NN         CHECK ( l1ps_val63 IS NOT NULL),
    CONSTRAINT            l1ps_val64_NN         CHECK ( l1ps_val64 IS NOT NULL),
    CONSTRAINT            l1ps_val65_NN         CHECK ( l1ps_val65 IS NOT NULL),
    CONSTRAINT            l1ps_val66_NN         CHECK ( l1ps_val66 IS NOT NULL),
    CONSTRAINT            l1ps_val67_NN         CHECK ( l1ps_val67 IS NOT NULL),
    CONSTRAINT            l1ps_val68_NN         CHECK ( l1ps_val68 IS NOT NULL),
    CONSTRAINT            l1ps_val69_NN         CHECK ( l1ps_val69 IS NOT NULL),
    CONSTRAINT            l1ps_val70_NN         CHECK ( l1ps_val70 IS NOT NULL),
    CONSTRAINT            l1ps_val71_NN         CHECK ( l1ps_val71 IS NOT NULL),
    CONSTRAINT            l1ps_val72_NN         CHECK ( l1ps_val72 IS NOT NULL),
    CONSTRAINT            l1ps_val73_NN         CHECK ( l1ps_val73 IS NOT NULL),
    CONSTRAINT            l1ps_val74_NN         CHECK ( l1ps_val74 IS NOT NULL),
    CONSTRAINT            l1ps_val75_NN         CHECK ( l1ps_val75 IS NOT NULL),
    CONSTRAINT            l1ps_val76_NN         CHECK ( l1ps_val76 IS NOT NULL),
    CONSTRAINT            l1ps_val77_NN         CHECK ( l1ps_val77 IS NOT NULL),
    CONSTRAINT            l1ps_val78_NN         CHECK ( l1ps_val78 IS NOT NULL),
    CONSTRAINT            l1ps_val79_NN         CHECK ( l1ps_val79 IS NOT NULL),
    CONSTRAINT            l1ps_val80_NN         CHECK ( l1ps_val80 IS NOT NULL),
    CONSTRAINT            l1ps_val81_NN         CHECK ( l1ps_val81 IS NOT NULL),
    CONSTRAINT            l1ps_val82_NN         CHECK ( l1ps_val82 IS NOT NULL),
    CONSTRAINT            l1ps_val83_NN         CHECK ( l1ps_val83 IS NOT NULL),
    CONSTRAINT            l1ps_val84_NN         CHECK ( l1ps_val84 IS NOT NULL),
    CONSTRAINT            l1ps_val85_NN         CHECK ( l1ps_val85 IS NOT NULL),
    CONSTRAINT            l1ps_val86_NN         CHECK ( l1ps_val86 IS NOT NULL),
    CONSTRAINT            l1ps_val87_NN         CHECK ( l1ps_val87 IS NOT NULL),
    CONSTRAINT            l1ps_val88_NN         CHECK ( l1ps_val88 IS NOT NULL),
    CONSTRAINT            l1ps_val89_NN         CHECK ( l1ps_val89 IS NOT NULL),
    CONSTRAINT            l1ps_val90_NN         CHECK ( l1ps_val90 IS NOT NULL),
    CONSTRAINT            l1ps_val91_NN         CHECK ( l1ps_val91 IS NOT NULL),
    CONSTRAINT            l1ps_val92_NN         CHECK ( l1ps_val92 IS NOT NULL),
    CONSTRAINT            l1ps_val93_NN         CHECK ( l1ps_val93 IS NOT NULL),
    CONSTRAINT            l1ps_val94_NN         CHECK ( l1ps_val94 IS NOT NULL),
    CONSTRAINT            l1ps_val95_NN         CHECK ( l1ps_val95 IS NOT NULL),
    CONSTRAINT            l1ps_val96_NN         CHECK ( l1ps_val96 IS NOT NULL),
    CONSTRAINT            l1ps_val97_NN         CHECK ( l1ps_val97 IS NOT NULL),
    CONSTRAINT            l1ps_val98_NN         CHECK ( l1ps_val98 IS NOT NULL),
    CONSTRAINT            l1ps_val99_NN         CHECK ( l1ps_val99 IS NOT NULL),
    CONSTRAINT            l1ps_val100_NN         CHECK ( l1ps_val100 IS NOT NULL),
    CONSTRAINT            l1ps_val101_NN         CHECK ( l1ps_val101 IS NOT NULL),
    CONSTRAINT            l1ps_val102_NN         CHECK ( l1ps_val102 IS NOT NULL),
    CONSTRAINT            l1ps_val103_NN         CHECK ( l1ps_val103 IS NOT NULL),
    CONSTRAINT            l1ps_val104_NN         CHECK ( l1ps_val104 IS NOT NULL),
    CONSTRAINT            l1ps_val105_NN         CHECK ( l1ps_val105 IS NOT NULL),
    CONSTRAINT            l1ps_val106_NN         CHECK ( l1ps_val106 IS NOT NULL),
    CONSTRAINT            l1ps_val107_NN         CHECK ( l1ps_val107 IS NOT NULL),
    CONSTRAINT            l1ps_val108_NN         CHECK ( l1ps_val108 IS NOT NULL),
    CONSTRAINT            l1ps_val109_NN         CHECK ( l1ps_val109 IS NOT NULL),
    CONSTRAINT            l1ps_val110_NN         CHECK ( l1ps_val110 IS NOT NULL),
    CONSTRAINT            l1ps_val111_NN         CHECK ( l1ps_val111 IS NOT NULL),
    CONSTRAINT            l1ps_val112_NN         CHECK ( l1ps_val112 IS NOT NULL),
    CONSTRAINT            l1ps_val113_NN         CHECK ( l1ps_val113 IS NOT NULL),
    CONSTRAINT            l1ps_val114_NN         CHECK ( l1ps_val114 IS NOT NULL),
    CONSTRAINT            l1ps_val115_NN         CHECK ( l1ps_val115 IS NOT NULL),
    CONSTRAINT            l1ps_val116_NN         CHECK ( l1ps_val116 IS NOT NULL),
    CONSTRAINT            l1ps_val117_NN         CHECK ( l1ps_val117 IS NOT NULL),
    CONSTRAINT            l1ps_val118_NN         CHECK ( l1ps_val118 IS NOT NULL),
    CONSTRAINT            l1ps_val119_NN         CHECK ( l1ps_val119 IS NOT NULL),
    CONSTRAINT            l1ps_val120_NN         CHECK ( l1ps_val120 IS NOT NULL),
    CONSTRAINT            l1ps_val121_NN         CHECK ( l1ps_val121 IS NOT NULL),
    CONSTRAINT            l1ps_val122_NN         CHECK ( l1ps_val122 IS NOT NULL),
    CONSTRAINT            l1ps_val123_NN         CHECK ( l1ps_val123 IS NOT NULL),
    CONSTRAINT            l1ps_val124_NN         CHECK ( l1ps_val124 IS NOT NULL),
    CONSTRAINT            l1ps_val125_NN         CHECK ( l1ps_val125 IS NOT NULL),
    CONSTRAINT            l1ps_val126_NN         CHECK ( l1ps_val126 IS NOT NULL),
    CONSTRAINT            l1ps_val127_NN         CHECK ( l1ps_val127 IS NOT NULL),
    CONSTRAINT            l1ps_val128_NN         CHECK ( l1ps_val128 IS NOT NULL),
    CONSTRAINT            l1ps_val129_NN         CHECK ( l1ps_val129 IS NOT NULL),
    CONSTRAINT            l1ps_val130_NN         CHECK ( l1ps_val130 IS NOT NULL),
    CONSTRAINT            l1ps_val131_NN         CHECK ( l1ps_val131 IS NOT NULL),
    CONSTRAINT            l1ps_val132_NN         CHECK ( l1ps_val132 IS NOT NULL),
    CONSTRAINT            l1ps_val133_NN         CHECK ( l1ps_val133 IS NOT NULL),
    CONSTRAINT            l1ps_val134_NN         CHECK ( l1ps_val134 IS NOT NULL),
    CONSTRAINT            l1ps_val135_NN         CHECK ( l1ps_val135 IS NOT NULL),
    CONSTRAINT            l1ps_val136_NN         CHECK ( l1ps_val136 IS NOT NULL),
    CONSTRAINT            l1ps_val137_NN         CHECK ( l1ps_val137 IS NOT NULL),
    CONSTRAINT            l1ps_val138_NN         CHECK ( l1ps_val138 IS NOT NULL),
    CONSTRAINT            l1ps_val139_NN         CHECK ( l1ps_val139 IS NOT NULL),
    CONSTRAINT            l1ps_val140_NN         CHECK ( l1ps_val140 IS NOT NULL),
    CONSTRAINT            l1ps_val141_NN         CHECK ( l1ps_val114 IS NOT NULL),
    CONSTRAINT            l1ps_val142_NN         CHECK ( l1ps_val142 IS NOT NULL),
    CONSTRAINT            l1ps_val143_NN         CHECK ( l1ps_val143 IS NOT NULL),
    CONSTRAINT            l1ps_val144_NN         CHECK ( l1ps_val144 IS NOT NULL),
    CONSTRAINT            l1ps_val145_NN         CHECK ( l1ps_val145 IS NOT NULL),
    CONSTRAINT            l1ps_val146_NN         CHECK ( l1ps_val146 IS NOT NULL),
    CONSTRAINT            l1ps_val147_NN         CHECK ( l1ps_val147 IS NOT NULL),
    CONSTRAINT            l1ps_val148_NN         CHECK ( l1ps_val148 IS NOT NULL),
    CONSTRAINT            l1ps_val149_NN         CHECK ( l1ps_val149 IS NOT NULL),
    CONSTRAINT            l1ps_val150_NN         CHECK ( l1ps_val150 IS NOT NULL),
    CONSTRAINT            l1ps_val151_NN         CHECK ( l1ps_val151 IS NOT NULL),
    CONSTRAINT            l1ps_val152_NN         CHECK ( l1ps_val152 IS NOT NULL),
    CONSTRAINT            l1ps_val153_NN         CHECK ( l1ps_val153 IS NOT NULL),
    CONSTRAINT            l1ps_val154_NN         CHECK ( l1ps_val154 IS NOT NULL),
    CONSTRAINT            l1ps_val155_NN         CHECK ( l1ps_val155 IS NOT NULL),
    CONSTRAINT            l1ps_val156_NN         CHECK ( l1ps_val156 IS NOT NULL),
    CONSTRAINT            l1ps_val157_NN         CHECK ( l1ps_val157 IS NOT NULL),
    CONSTRAINT            l1ps_val158_NN         CHECK ( l1ps_val158 IS NOT NULL),
    CONSTRAINT            l1ps_val159_NN         CHECK ( l1ps_val159 IS NOT NULL),
    CONSTRAINT            l1ps_val160_NN         CHECK ( l1ps_val160 IS NOT NULL),
    CONSTRAINT            l1ps_val161_NN         CHECK ( l1ps_val161 IS NOT NULL),
    CONSTRAINT            l1ps_val162_NN         CHECK ( l1ps_val162 IS NOT NULL),
    CONSTRAINT            l1ps_val163_NN         CHECK ( l1ps_val163 IS NOT NULL),
    CONSTRAINT            l1ps_val164_NN         CHECK ( l1ps_val164 IS NOT NULL),
    CONSTRAINT            l1ps_val165_NN         CHECK ( l1ps_val165 IS NOT NULL),
    CONSTRAINT            l1ps_val166_NN         CHECK ( l1ps_val166 IS NOT NULL),
    CONSTRAINT            l1ps_val167_NN         CHECK ( l1ps_val167 IS NOT NULL),
    CONSTRAINT            l1ps_val168_NN         CHECK ( l1ps_val168 IS NOT NULL),
    CONSTRAINT            l1ps_val169_NN         CHECK ( l1ps_val169 IS NOT NULL),
    CONSTRAINT            l1ps_val170_NN         CHECK ( l1ps_val170 IS NOT NULL),
    CONSTRAINT            l1ps_val171_NN         CHECK ( l1ps_val171 IS NOT NULL),
    CONSTRAINT            l1ps_val172_NN         CHECK ( l1ps_val172 IS NOT NULL),
    CONSTRAINT            l1ps_val173_NN         CHECK ( l1ps_val173 IS NOT NULL),
    CONSTRAINT            l1ps_val174_NN         CHECK ( l1ps_val174 IS NOT NULL),
    CONSTRAINT            l1ps_val175_NN         CHECK ( l1ps_val175 IS NOT NULL),
    CONSTRAINT            l1ps_val176_NN         CHECK ( l1ps_val176 IS NOT NULL),
    CONSTRAINT            l1ps_val177_NN         CHECK ( l1ps_val177 IS NOT NULL),
    CONSTRAINT            l1ps_val178_NN         CHECK ( l1ps_val178 IS NOT NULL),
    CONSTRAINT            l1ps_val179_NN         CHECK ( l1ps_val179 IS NOT NULL),
    CONSTRAINT            l1ps_val180_NN         CHECK ( l1ps_val180 IS NOT NULL),
    CONSTRAINT            l1ps_val181_NN         CHECK ( l1ps_val181 IS NOT NULL),
    CONSTRAINT            l1ps_val182_NN         CHECK ( l1ps_val182 IS NOT NULL),
    CONSTRAINT            l1ps_val183_NN         CHECK ( l1ps_val183 IS NOT NULL),
    CONSTRAINT            l1ps_val184_NN         CHECK ( l1ps_val184 IS NOT NULL),
    CONSTRAINT            l1ps_val185_NN         CHECK ( l1ps_val185 IS NOT NULL),
    CONSTRAINT            l1ps_val186_NN         CHECK ( l1ps_val186 IS NOT NULL),
    CONSTRAINT            l1ps_val187_NN         CHECK ( l1ps_val187 IS NOT NULL),
    CONSTRAINT            l1ps_val188_NN         CHECK ( l1ps_val188 IS NOT NULL),
    CONSTRAINT            l1ps_val189_NN         CHECK ( l1ps_val189 IS NOT NULL),
    CONSTRAINT            l1ps_val190_NN         CHECK ( l1ps_val190 IS NOT NULL),
    CONSTRAINT            l1ps_val191_NN         CHECK ( l1ps_val191 IS NOT NULL),
    CONSTRAINT            l1ps_val192_NN         CHECK ( l1ps_val192 IS NOT NULL),
    CONSTRAINT            l1ps_val193_NN         CHECK ( l1ps_val193 IS NOT NULL),
    CONSTRAINT            l1ps_val194_NN         CHECK ( l1ps_val194 IS NOT NULL),
    CONSTRAINT            l1ps_val195_NN         CHECK ( l1ps_val195 IS NOT NULL),
    CONSTRAINT            l1ps_val196_NN         CHECK ( l1ps_val196 IS NOT NULL),
    CONSTRAINT            l1ps_val197_NN         CHECK ( l1ps_val197 IS NOT NULL),
    CONSTRAINT            l1ps_val198_NN         CHECK ( l1ps_val198 IS NOT NULL),
    CONSTRAINT            l1ps_val199_NN         CHECK ( l1ps_val199 IS NOT NULL),
    CONSTRAINT            l1ps_val200_NN         CHECK ( l1ps_val200 IS NOT NULL),
    CONSTRAINT            l1ps_val201_NN         CHECK ( l1ps_val201 IS NOT NULL),
    CONSTRAINT            l1ps_val202_NN         CHECK ( l1ps_val202 IS NOT NULL),
    CONSTRAINT            l1ps_val203_NN         CHECK ( l1ps_val203 IS NOT NULL),
    CONSTRAINT            l1ps_val204_NN         CHECK ( l1ps_val204 IS NOT NULL),
    CONSTRAINT            l1ps_val205_NN         CHECK ( l1ps_val205 IS NOT NULL),
    CONSTRAINT            l1ps_val206_NN         CHECK ( l1ps_val206 IS NOT NULL),
    CONSTRAINT            l1ps_val207_NN         CHECK ( l1ps_val207 IS NOT NULL),
    CONSTRAINT            l1ps_val208_NN         CHECK ( l1ps_val208 IS NOT NULL),
    CONSTRAINT            l1ps_val209_NN         CHECK ( l1ps_val209 IS NOT NULL),
    CONSTRAINT            l1ps_val210_NN         CHECK ( l1ps_val210 IS NOT NULL),
    CONSTRAINT            l1ps_val211_NN         CHECK ( l1ps_val211 IS NOT NULL),
    CONSTRAINT            l1ps_val212_NN         CHECK ( l1ps_val212 IS NOT NULL),
    CONSTRAINT            l1ps_val213_NN         CHECK ( l1ps_val213 IS NOT NULL),
    CONSTRAINT            l1ps_val214_NN         CHECK ( l1ps_val214 IS NOT NULL),
    CONSTRAINT            l1ps_val215_NN         CHECK ( l1ps_val215 IS NOT NULL),
    CONSTRAINT            l1ps_val216_NN         CHECK ( l1ps_val216 IS NOT NULL),
    CONSTRAINT            l1ps_val217_NN         CHECK ( l1ps_val217 IS NOT NULL),
    CONSTRAINT            l1ps_val218_NN         CHECK ( l1ps_val218 IS NOT NULL),
    CONSTRAINT            l1ps_val219_NN         CHECK ( l1ps_val219 IS NOT NULL),
    CONSTRAINT            l1ps_val220_NN         CHECK ( l1ps_val220 IS NOT NULL),
    CONSTRAINT            l1ps_val221_NN         CHECK ( l1ps_val221 IS NOT NULL),
    CONSTRAINT            l1ps_val222_NN         CHECK ( l1ps_val222 IS NOT NULL),
    CONSTRAINT            l1ps_val223_NN         CHECK ( l1ps_val223 IS NOT NULL),
    CONSTRAINT            l1ps_val224_NN         CHECK ( l1ps_val224 IS NOT NULL),
    CONSTRAINT            l1ps_val225_NN         CHECK ( l1ps_val225 IS NOT NULL),
    CONSTRAINT            l1ps_val226_NN         CHECK ( l1ps_val226 IS NOT NULL),
    CONSTRAINT            l1ps_val227_NN         CHECK ( l1ps_val227 IS NOT NULL),
    CONSTRAINT            l1ps_val228_NN         CHECK ( l1ps_val228 IS NOT NULL),
    CONSTRAINT            l1ps_val229_NN         CHECK ( l1ps_val229 IS NOT NULL),
    CONSTRAINT            l1ps_val230_NN         CHECK ( l1ps_val230 IS NOT NULL),
    CONSTRAINT            l1ps_val231_NN         CHECK ( l1ps_val231 IS NOT NULL),
    CONSTRAINT            l1ps_val232_NN         CHECK ( l1ps_val232 IS NOT NULL),
    CONSTRAINT            l1ps_val233_NN         CHECK ( l1ps_val233 IS NOT NULL),
    CONSTRAINT            l1ps_val234_NN         CHECK ( l1ps_val234 IS NOT NULL),
    CONSTRAINT            l1ps_val235_NN         CHECK ( l1ps_val235 IS NOT NULL),
    CONSTRAINT            l1ps_val236_NN         CHECK ( l1ps_val236 IS NOT NULL),
    CONSTRAINT            l1ps_val237_NN         CHECK ( l1ps_val237 IS NOT NULL),
    CONSTRAINT            l1ps_val238_NN         CHECK ( l1ps_val238 IS NOT NULL),
    CONSTRAINT            l1ps_val239_NN         CHECK ( l1ps_val239 IS NOT NULL),
    CONSTRAINT            l1ps_val240_NN         CHECK ( l1ps_val240 IS NOT NULL),
    CONSTRAINT            l1ps_val241_NN         CHECK ( l1ps_val241 IS NOT NULL),
    CONSTRAINT            l1ps_val242_NN         CHECK ( l1ps_val242 IS NOT NULL),
    CONSTRAINT            l1ps_val243_NN         CHECK ( l1ps_val243 IS NOT NULL),
    CONSTRAINT            l1ps_val244_NN         CHECK ( l1ps_val244 IS NOT NULL),
    CONSTRAINT            l1ps_val245_NN         CHECK ( l1ps_val245 IS NOT NULL),
    CONSTRAINT            l1ps_val246_NN         CHECK ( l1ps_val246 IS NOT NULL),
    CONSTRAINT            l1ps_val247_NN         CHECK ( l1ps_val247 IS NOT NULL),
    CONSTRAINT            l1ps_val248_NN         CHECK ( l1ps_val248 IS NOT NULL),
    CONSTRAINT            l1ps_val249_NN         CHECK ( l1ps_val249 IS NOT NULL),
    CONSTRAINT            l1ps_val250_NN         CHECK ( l1ps_val250 IS NOT NULL),
    CONSTRAINT            l1ps_val251_NN         CHECK ( l1ps_val251 IS NOT NULL),
    CONSTRAINT            l1ps_val252_NN         CHECK ( l1ps_val252 IS NOT NULL),
    CONSTRAINT            l1ps_val253_NN         CHECK ( l1ps_val253 IS NOT NULL),
    CONSTRAINT            l1ps_val254_NN         CHECK ( l1ps_val254 IS NOT NULL),
    CONSTRAINT            l1ps_val255_NN         CHECK ( l1ps_val255 IS NOT NULL),
    CONSTRAINT            l1ps_val256_NN         CHECK ( l1ps_val256 IS NOT NULL),
    CONSTRAINT            l1ps_val257_NN         CHECK ( l1ps_val257 IS NOT NULL),
    CONSTRAINT            l1ps_val258_NN         CHECK ( l1ps_val258 IS NOT NULL),
    CONSTRAINT            l1ps_val259_NN         CHECK ( l1ps_val259 IS NOT NULL),
    CONSTRAINT            l1ps_val260_NN         CHECK ( l1ps_val260 IS NOT NULL),
    CONSTRAINT            l1ps_val261_NN         CHECK ( l1ps_val261 IS NOT NULL),
    CONSTRAINT            l1ps_val262_NN         CHECK ( l1ps_val262 IS NOT NULL),
    CONSTRAINT            l1ps_val263_NN         CHECK ( l1ps_val263 IS NOT NULL),
    CONSTRAINT            l1ps_val264_NN         CHECK ( l1ps_val264 IS NOT NULL),
    CONSTRAINT            l1ps_val265_NN         CHECK ( l1ps_val265 IS NOT NULL),
    CONSTRAINT            l1ps_val266_NN         CHECK ( l1ps_val266 IS NOT NULL),
    CONSTRAINT            l1ps_val267_NN         CHECK ( l1ps_val267 IS NOT NULL),
    CONSTRAINT            l1ps_val268_NN         CHECK ( l1ps_val268 IS NOT NULL),
    CONSTRAINT            l1ps_val269_NN         CHECK ( l1ps_val269 IS NOT NULL),
    CONSTRAINT            l1ps_val270_NN         CHECK ( l1ps_val270 IS NOT NULL),
    CONSTRAINT            l1ps_val271_NN         CHECK ( l1ps_val271 IS NOT NULL),
    CONSTRAINT            l1ps_val272_NN         CHECK ( l1ps_val272 IS NOT NULL),
    CONSTRAINT            l1ps_val273_NN         CHECK ( l1ps_val273 IS NOT NULL),
    CONSTRAINT            l1ps_val274_NN         CHECK ( l1ps_val274 IS NOT NULL),
    CONSTRAINT            l1ps_val275_NN         CHECK ( l1ps_val275 IS NOT NULL),
    CONSTRAINT            l1ps_val276_NN         CHECK ( l1ps_val276 IS NOT NULL),
    CONSTRAINT            l1ps_val277_NN         CHECK ( l1ps_val277 IS NOT NULL),
    CONSTRAINT            l1ps_val278_NN         CHECK ( l1ps_val278 IS NOT NULL),
    CONSTRAINT            l1ps_val279_NN         CHECK ( l1ps_val279 IS NOT NULL),
    CONSTRAINT            l1ps_val280_NN         CHECK ( l1ps_val280 IS NOT NULL),
    CONSTRAINT            l1ps_val281_NN         CHECK ( l1ps_val281 IS NOT NULL),
    CONSTRAINT            l1ps_val282_NN         CHECK ( l1ps_val282 IS NOT NULL),
    CONSTRAINT            l1ps_val283_NN         CHECK ( l1ps_val283 IS NOT NULL),
    CONSTRAINT            l1ps_val284_NN         CHECK ( l1ps_val284 IS NOT NULL),
    CONSTRAINT            l1ps_val285_NN         CHECK ( l1ps_val285 IS NOT NULL),
    CONSTRAINT            l1ps_val286_NN         CHECK ( l1ps_val286 IS NOT NULL),
    CONSTRAINT            l1ps_val287_NN         CHECK ( l1ps_val287 IS NOT NULL),
    CONSTRAINT            l1ps_val288_NN         CHECK ( l1ps_val288 IS NOT NULL),
    CONSTRAINT            l1ps_val289_NN         CHECK ( l1ps_val289 IS NOT NULL),
    CONSTRAINT            l1ps_val290_NN         CHECK ( l1ps_val290 IS NOT NULL),
    CONSTRAINT            l1ps_val291_NN         CHECK ( l1ps_val291 IS NOT NULL),
    CONSTRAINT            l1ps_val292_NN         CHECK ( l1ps_val292 IS NOT NULL),
    CONSTRAINT            l1ps_val293_NN         CHECK ( l1ps_val293 IS NOT NULL),
    CONSTRAINT            l1ps_val294_NN         CHECK ( l1ps_val294 IS NOT NULL),
    CONSTRAINT            l1ps_val295_NN         CHECK ( l1ps_val295 IS NOT NULL),
    CONSTRAINT            l1ps_val296_NN         CHECK ( l1ps_val296 IS NOT NULL),
    CONSTRAINT            l1ps_val297_NN         CHECK ( l1ps_val297 IS NOT NULL),
    CONSTRAINT            l1ps_val298_NN         CHECK ( l1ps_val298 IS NOT NULL),
    CONSTRAINT            l1ps_val299_NN         CHECK ( l1ps_val299 IS NOT NULL),
    CONSTRAINT            l1ps_val300_NN         CHECK ( l1ps_val300 IS NOT NULL),
    CONSTRAINT            l1ps_val301_NN         CHECK ( l1ps_val301 IS NOT NULL),
    CONSTRAINT            l1ps_val302_NN         CHECK ( l1ps_val302 IS NOT NULL),
    CONSTRAINT            l1ps_val303_NN         CHECK ( l1ps_val303 IS NOT NULL),
    CONSTRAINT            l1ps_val304_NN         CHECK ( l1ps_val304 IS NOT NULL),
    CONSTRAINT            l1ps_val305_NN         CHECK ( l1ps_val305 IS NOT NULL),
    CONSTRAINT            l1ps_val306_NN         CHECK ( l1ps_val306 IS NOT NULL),
    CONSTRAINT            l1ps_val307_NN         CHECK ( l1ps_val307 IS NOT NULL),
    CONSTRAINT            l1ps_val308_NN         CHECK ( l1ps_val308 IS NOT NULL),
    CONSTRAINT            l1ps_val309_NN         CHECK ( l1ps_val309 IS NOT NULL),
    CONSTRAINT            l1ps_val310_NN         CHECK ( l1ps_val310 IS NOT NULL),
    CONSTRAINT            l1ps_val311_NN         CHECK ( l1ps_val311 IS NOT NULL),
    CONSTRAINT            l1ps_val312_NN         CHECK ( l1ps_val312 IS NOT NULL),
    CONSTRAINT            l1ps_val313_NN         CHECK ( l1ps_val313 IS NOT NULL),
    CONSTRAINT            l1ps_val314_NN         CHECK ( l1ps_val314 IS NOT NULL),
    CONSTRAINT            l1ps_val315_NN         CHECK ( l1ps_val315 IS NOT NULL),
    CONSTRAINT            l1ps_val316_NN         CHECK ( l1ps_val316 IS NOT NULL),
    CONSTRAINT            l1ps_val317_NN         CHECK ( l1ps_val317 IS NOT NULL),
    CONSTRAINT            l1ps_val318_NN         CHECK ( l1ps_val318 IS NOT NULL),
    CONSTRAINT            l1ps_val319_NN         CHECK ( l1ps_val319 IS NOT NULL),
    CONSTRAINT            l1ps_val320_NN         CHECK ( l1ps_val320 IS NOT NULL),
    CONSTRAINT            l1ps_val321_NN         CHECK ( l1ps_val321 IS NOT NULL),
    CONSTRAINT            l1ps_val322_NN         CHECK ( l1ps_val322 IS NOT NULL),
    CONSTRAINT            l1ps_val323_NN         CHECK ( l1ps_val323 IS NOT NULL),
    CONSTRAINT            l1ps_val324_NN         CHECK ( l1ps_val324 IS NOT NULL),
    CONSTRAINT            l1ps_val325_NN         CHECK ( l1ps_val325 IS NOT NULL),
    CONSTRAINT            l1ps_val326_NN         CHECK ( l1ps_val326 IS NOT NULL),
    CONSTRAINT            l1ps_val327_NN         CHECK ( l1ps_val327 IS NOT NULL),
    CONSTRAINT            l1ps_val328_NN         CHECK ( l1ps_val328 IS NOT NULL),
    CONSTRAINT            l1ps_val329_NN         CHECK ( l1ps_val329 IS NOT NULL),
    CONSTRAINT            l1ps_val330_NN         CHECK ( l1ps_val330 IS NOT NULL),
    CONSTRAINT            l1ps_val331_NN         CHECK ( l1ps_val331 IS NOT NULL),
    CONSTRAINT            l1ps_val332_NN         CHECK ( l1ps_val332 IS NOT NULL),
    CONSTRAINT            l1ps_val333_NN         CHECK ( l1ps_val333 IS NOT NULL),
    CONSTRAINT            l1ps_val334_NN         CHECK ( l1ps_val334 IS NOT NULL),
    CONSTRAINT            l1ps_val335_NN         CHECK ( l1ps_val335 IS NOT NULL),
    CONSTRAINT            l1ps_val336_NN         CHECK ( l1ps_val336 IS NOT NULL),
    CONSTRAINT            l1ps_val337_NN         CHECK ( l1ps_val337 IS NOT NULL),
    CONSTRAINT            l1ps_val338_NN         CHECK ( l1ps_val338 IS NOT NULL),
    CONSTRAINT            l1ps_val339_NN         CHECK ( l1ps_val339 IS NOT NULL),
    CONSTRAINT            l1ps_val340_NN         CHECK ( l1ps_val340 IS NOT NULL),
    CONSTRAINT            l1ps_val341_NN         CHECK ( l1ps_val341 IS NOT NULL),
    CONSTRAINT            l1ps_val342_NN         CHECK ( l1ps_val342 IS NOT NULL),
    CONSTRAINT            l1ps_val343_NN         CHECK ( l1ps_val343 IS NOT NULL),
    CONSTRAINT            l1ps_val344_NN         CHECK ( l1ps_val344 IS NOT NULL),
    CONSTRAINT            l1ps_val345_NN         CHECK ( l1ps_val345 IS NOT NULL),
    CONSTRAINT            l1ps_val346_NN         CHECK ( l1ps_val346 IS NOT NULL),
    CONSTRAINT            l1ps_val347_NN         CHECK ( l1ps_val347 IS NOT NULL),
    CONSTRAINT            l1ps_val348_NN         CHECK ( l1ps_val348 IS NOT NULL),
    CONSTRAINT            l1ps_val349_NN         CHECK ( l1ps_val349 IS NOT NULL),
    CONSTRAINT            l1ps_val350_NN         CHECK ( l1ps_val350 IS NOT NULL),
    CONSTRAINT            l1ps_val351_NN         CHECK ( l1ps_val351 IS NOT NULL),
    CONSTRAINT            l1ps_val352_NN         CHECK ( l1ps_val352 IS NOT NULL),
    CONSTRAINT            l1ps_val353_NN         CHECK ( l1ps_val353 IS NOT NULL),
    CONSTRAINT            l1ps_val354_NN         CHECK ( l1ps_val354 IS NOT NULL),
    CONSTRAINT            l1ps_val355_NN         CHECK ( l1ps_val355 IS NOT NULL),
    CONSTRAINT            l1ps_val356_NN         CHECK ( l1ps_val356 IS NOT NULL),
    CONSTRAINT            l1ps_val357_NN         CHECK ( l1ps_val357 IS NOT NULL),
    CONSTRAINT            l1ps_val358_NN         CHECK ( l1ps_val358 IS NOT NULL),
    CONSTRAINT            l1ps_val359_NN         CHECK ( l1ps_val359 IS NOT NULL),
    CONSTRAINT            l1ps_val360_NN         CHECK ( l1ps_val360 IS NOT NULL),
    CONSTRAINT            l1ps_val361_NN         CHECK ( l1ps_val361 IS NOT NULL),
    CONSTRAINT            l1ps_val362_NN         CHECK ( l1ps_val362 IS NOT NULL),
    CONSTRAINT            l1ps_val363_NN         CHECK ( l1ps_val363 IS NOT NULL),
    CONSTRAINT            l1ps_val364_NN         CHECK ( l1ps_val364 IS NOT NULL),
    CONSTRAINT            l1ps_val365_NN         CHECK ( l1ps_val365 IS NOT NULL),
    CONSTRAINT            l1ps_val366_NN         CHECK ( l1ps_val366 IS NOT NULL),
    CONSTRAINT            l1ps_val367_NN         CHECK ( l1ps_val367 IS NOT NULL),
    CONSTRAINT            l1ps_val368_NN         CHECK ( l1ps_val368 IS NOT NULL),
    CONSTRAINT            l1ps_val369_NN         CHECK ( l1ps_val369 IS NOT NULL),
    CONSTRAINT            l1ps_val370_NN         CHECK ( l1ps_val370 IS NOT NULL),
    CONSTRAINT            l1ps_val371_NN         CHECK ( l1ps_val371 IS NOT NULL),
    CONSTRAINT            l1ps_val372_NN         CHECK ( l1ps_val372 IS NOT NULL),
    CONSTRAINT            l1ps_val373_NN         CHECK ( l1ps_val373 IS NOT NULL),
    CONSTRAINT            l1ps_val374_NN         CHECK ( l1ps_val374 IS NOT NULL),
    CONSTRAINT            l1ps_val375_NN         CHECK ( l1ps_val375 IS NOT NULL),
    CONSTRAINT            l1ps_val376_NN         CHECK ( l1ps_val376 IS NOT NULL),
    CONSTRAINT            l1ps_val377_NN         CHECK ( l1ps_val377 IS NOT NULL),
    CONSTRAINT            l1ps_val378_NN         CHECK ( l1ps_val378 IS NOT NULL),
    CONSTRAINT            l1ps_val379_NN         CHECK ( l1ps_val379 IS NOT NULL),
    CONSTRAINT            l1ps_val380_NN         CHECK ( l1ps_val380 IS NOT NULL),
    CONSTRAINT            l1ps_val381_NN         CHECK ( l1ps_val381 IS NOT NULL),
    CONSTRAINT            l1ps_val382_NN         CHECK ( l1ps_val382 IS NOT NULL),
    CONSTRAINT            l1ps_val383_NN         CHECK ( l1ps_val383 IS NOT NULL),
    CONSTRAINT            l1ps_val384_NN         CHECK ( l1ps_val384 IS NOT NULL),
    CONSTRAINT            l1ps_val385_NN         CHECK ( l1ps_val385 IS NOT NULL),
    CONSTRAINT            l1ps_val386_NN         CHECK ( l1ps_val386 IS NOT NULL),
    CONSTRAINT            l1ps_val387_NN         CHECK ( l1ps_val387 IS NOT NULL),
    CONSTRAINT            l1ps_val388_NN         CHECK ( l1ps_val388 IS NOT NULL),
    CONSTRAINT            l1ps_val389_NN         CHECK ( l1ps_val389 IS NOT NULL),
    CONSTRAINT            l1ps_val390_NN         CHECK ( l1ps_val390 IS NOT NULL),
    CONSTRAINT            l1ps_val391_NN         CHECK ( l1ps_val391 IS NOT NULL),
    CONSTRAINT            l1ps_val392_NN         CHECK ( l1ps_val392 IS NOT NULL),
    CONSTRAINT            l1ps_val393_NN         CHECK ( l1ps_val393 IS NOT NULL),
    CONSTRAINT            l1ps_val394_NN         CHECK ( l1ps_val394 IS NOT NULL),
    CONSTRAINT            l1ps_val395_NN         CHECK ( l1ps_val395 IS NOT NULL),
    CONSTRAINT            l1ps_val396_NN         CHECK ( l1ps_val396 IS NOT NULL),
    CONSTRAINT            l1ps_val397_NN         CHECK ( l1ps_val397 IS NOT NULL),
    CONSTRAINT            l1ps_val398_NN         CHECK ( l1ps_val398 IS NOT NULL),
    CONSTRAINT            l1ps_val399_NN         CHECK ( l1ps_val399 IS NOT NULL),
    CONSTRAINT            l1ps_val400_NN         CHECK ( l1ps_val400 IS NOT NULL),
    CONSTRAINT            l1ps_val401_NN         CHECK ( l1ps_val401 IS NOT NULL),
    CONSTRAINT            l1ps_val402_NN         CHECK ( l1ps_val402 IS NOT NULL),
    CONSTRAINT            l1ps_val403_NN         CHECK ( l1ps_val403 IS NOT NULL),
    CONSTRAINT            l1ps_val404_NN         CHECK ( l1ps_val404 IS NOT NULL),
    CONSTRAINT            l1ps_val405_NN         CHECK ( l1ps_val405 IS NOT NULL),
    CONSTRAINT            l1ps_val406_NN         CHECK ( l1ps_val406 IS NOT NULL),
    CONSTRAINT            l1ps_val407_NN         CHECK ( l1ps_val407 IS NOT NULL),
    CONSTRAINT            l1ps_val408_NN         CHECK ( l1ps_val408 IS NOT NULL),
    CONSTRAINT            l1ps_val409_NN         CHECK ( l1ps_val409 IS NOT NULL),
    CONSTRAINT            l1ps_val410_NN         CHECK ( l1ps_val410 IS NOT NULL),
    CONSTRAINT            l1ps_val411_NN         CHECK ( l1ps_val411 IS NOT NULL),
    CONSTRAINT            l1ps_val412_NN         CHECK ( l1ps_val412 IS NOT NULL),
    CONSTRAINT            l1ps_val413_NN         CHECK ( l1ps_val413 IS NOT NULL),
    CONSTRAINT            l1ps_val414_NN         CHECK ( l1ps_val414 IS NOT NULL),
    CONSTRAINT            l1ps_val415_NN         CHECK ( l1ps_val415 IS NOT NULL),
    CONSTRAINT            l1ps_val416_NN         CHECK ( l1ps_val416 IS NOT NULL),
    CONSTRAINT            l1ps_val417_NN         CHECK ( l1ps_val417 IS NOT NULL),
    CONSTRAINT            l1ps_val418_NN         CHECK ( l1ps_val418 IS NOT NULL),
    CONSTRAINT            l1ps_val419_NN         CHECK ( l1ps_val419 IS NOT NULL),
    CONSTRAINT            l1ps_val420_NN         CHECK ( l1ps_val420 IS NOT NULL),
    CONSTRAINT            l1ps_val421_NN         CHECK ( l1ps_val421 IS NOT NULL),
    CONSTRAINT            l1ps_val422_NN         CHECK ( l1ps_val422 IS NOT NULL),
    CONSTRAINT            l1ps_val423_NN         CHECK ( l1ps_val423 IS NOT NULL),
    CONSTRAINT            l1ps_val424_NN         CHECK ( l1ps_val424 IS NOT NULL),
    CONSTRAINT            l1ps_val425_NN         CHECK ( l1ps_val425 IS NOT NULL),
    CONSTRAINT            l1ps_val426_NN         CHECK ( l1ps_val426 IS NOT NULL),
    CONSTRAINT            l1ps_val427_NN         CHECK ( l1ps_val427 IS NOT NULL),
    CONSTRAINT            l1ps_val428_NN         CHECK ( l1ps_val428 IS NOT NULL),
    CONSTRAINT            l1ps_val429_NN         CHECK ( l1ps_val429 IS NOT NULL),
    CONSTRAINT            l1ps_val430_NN         CHECK ( l1ps_val430 IS NOT NULL),
    CONSTRAINT            l1ps_val431_NN         CHECK ( l1ps_val431 IS NOT NULL),
    CONSTRAINT            l1ps_val432_NN         CHECK ( l1ps_val432 IS NOT NULL),
    CONSTRAINT            l1ps_val433_NN         CHECK ( l1ps_val433 IS NOT NULL),
    CONSTRAINT            l1ps_val434_NN         CHECK ( l1ps_val434 IS NOT NULL),
    CONSTRAINT            l1ps_val435_NN         CHECK ( l1ps_val435 IS NOT NULL),
    CONSTRAINT            l1ps_val436_NN         CHECK ( l1ps_val436 IS NOT NULL),
    CONSTRAINT            l1ps_val437_NN         CHECK ( l1ps_val437 IS NOT NULL),
    CONSTRAINT            l1ps_val438_NN         CHECK ( l1ps_val438 IS NOT NULL),
    CONSTRAINT            l1ps_val439_NN         CHECK ( l1ps_val439 IS NOT NULL),
    CONSTRAINT            l1ps_val440_NN         CHECK ( l1ps_val440 IS NOT NULL),
    CONSTRAINT            l1ps_val441_NN         CHECK ( l1ps_val441 IS NOT NULL),
    CONSTRAINT            l1ps_val442_NN         CHECK ( l1ps_val442 IS NOT NULL),
    CONSTRAINT            l1ps_val443_NN         CHECK ( l1ps_val443 IS NOT NULL),
    CONSTRAINT            l1ps_val444_NN         CHECK ( l1ps_val444 IS NOT NULL),
    CONSTRAINT            l1ps_val445_NN         CHECK ( l1ps_val445 IS NOT NULL),
    CONSTRAINT            l1ps_val446_NN         CHECK ( l1ps_val446 IS NOT NULL),
    CONSTRAINT            l1ps_val447_NN         CHECK ( l1ps_val447 IS NOT NULL),
    CONSTRAINT            l1ps_val448_NN         CHECK ( l1ps_val448 IS NOT NULL),
    CONSTRAINT            l1ps_val449_NN         CHECK ( l1ps_val449 IS NOT NULL),
    CONSTRAINT            l1ps_val450_NN         CHECK ( l1ps_val450 IS NOT NULL),
    CONSTRAINT            l1ps_val451_NN         CHECK ( l1ps_val451 IS NOT NULL),
    CONSTRAINT            l1ps_val452_NN         CHECK ( l1ps_val452 IS NOT NULL),
    CONSTRAINT            l1ps_val453_NN         CHECK ( l1ps_val453 IS NOT NULL),
    CONSTRAINT            l1ps_val454_NN         CHECK ( l1ps_val454 IS NOT NULL),
    CONSTRAINT            l1ps_val455_NN         CHECK ( l1ps_val455 IS NOT NULL),
    CONSTRAINT            l1ps_val456_NN         CHECK ( l1ps_val456 IS NOT NULL),
    CONSTRAINT            l1ps_val457_NN         CHECK ( l1ps_val457 IS NOT NULL),
    CONSTRAINT            l1ps_val458_NN         CHECK ( l1ps_val458 IS NOT NULL),
    CONSTRAINT            l1ps_val459_NN         CHECK ( l1ps_val459 IS NOT NULL),
    CONSTRAINT            l1ps_val460_NN         CHECK ( l1ps_val460 IS NOT NULL),
    CONSTRAINT            l1ps_val461_NN         CHECK ( l1ps_val461 IS NOT NULL),
    CONSTRAINT            l1ps_val462_NN         CHECK ( l1ps_val462 IS NOT NULL),
    CONSTRAINT            l1ps_val463_NN         CHECK ( l1ps_val463 IS NOT NULL),
    CONSTRAINT            l1ps_val464_NN         CHECK ( l1ps_val464 IS NOT NULL),
    CONSTRAINT            l1ps_val465_NN         CHECK ( l1ps_val465 IS NOT NULL),
    CONSTRAINT            l1ps_val466_NN         CHECK ( l1ps_val466 IS NOT NULL),
    CONSTRAINT            l1ps_val467_NN         CHECK ( l1ps_val467 IS NOT NULL),
    CONSTRAINT            l1ps_val468_NN         CHECK ( l1ps_val468 IS NOT NULL),
    CONSTRAINT            l1ps_val469_NN         CHECK ( l1ps_val469 IS NOT NULL),
    CONSTRAINT            l1ps_val470_NN         CHECK ( l1ps_val470 IS NOT NULL),
    CONSTRAINT            l1ps_val471_NN         CHECK ( l1ps_val471 IS NOT NULL),
    CONSTRAINT            l1ps_val472_NN         CHECK ( l1ps_val472 IS NOT NULL),
    CONSTRAINT            l1ps_val473_NN         CHECK ( l1ps_val473 IS NOT NULL),
    CONSTRAINT            l1ps_val474_NN         CHECK ( l1ps_val474 IS NOT NULL),
    CONSTRAINT            l1ps_val475_NN         CHECK ( l1ps_val475 IS NOT NULL),
    CONSTRAINT            l1ps_val476_NN         CHECK ( l1ps_val476 IS NOT NULL),
    CONSTRAINT            l1ps_val477_NN         CHECK ( l1ps_val477 IS NOT NULL),
    CONSTRAINT            l1ps_val478_NN         CHECK ( l1ps_val478 IS NOT NULL),
    CONSTRAINT            l1ps_val479_NN         CHECK ( l1ps_val479 IS NOT NULL),
    CONSTRAINT            l1ps_val480_NN         CHECK ( l1ps_val480 IS NOT NULL),
    CONSTRAINT            l1ps_val481_NN         CHECK ( l1ps_val481 IS NOT NULL),
    CONSTRAINT            l1ps_val482_NN         CHECK ( l1ps_val482 IS NOT NULL),
    CONSTRAINT            l1ps_val483_NN         CHECK ( l1ps_val483 IS NOT NULL),
    CONSTRAINT            l1ps_val484_NN         CHECK ( l1ps_val484 IS NOT NULL),
    CONSTRAINT            l1ps_val485_NN         CHECK ( l1ps_val485 IS NOT NULL),
    CONSTRAINT            l1ps_val486_NN         CHECK ( l1ps_val486 IS NOT NULL),
    CONSTRAINT            l1ps_val487_NN         CHECK ( l1ps_val487 IS NOT NULL),
    CONSTRAINT            l1ps_val488_NN         CHECK ( l1ps_val488 IS NOT NULL),
    CONSTRAINT            l1ps_val489_NN         CHECK ( l1ps_val489 IS NOT NULL),
    CONSTRAINT            l1ps_val490_NN         CHECK ( l1ps_val490 IS NOT NULL),
    CONSTRAINT            l1ps_val491_NN         CHECK ( l1ps_val491 IS NOT NULL),
    CONSTRAINT            l1ps_val492_NN         CHECK ( l1ps_val492 IS NOT NULL),
    CONSTRAINT            l1ps_val493_NN         CHECK ( l1ps_val493 IS NOT NULL),
    CONSTRAINT            l1ps_val494_NN         CHECK ( l1ps_val494 IS NOT NULL),
    CONSTRAINT            l1ps_val495_NN         CHECK ( l1ps_val495 IS NOT NULL),
    CONSTRAINT            l1ps_val496_NN         CHECK ( l1ps_val496 IS NOT NULL),
    CONSTRAINT            l1ps_val497_NN         CHECK ( l1ps_val497 IS NOT NULL),
    CONSTRAINT            l1ps_val498_NN         CHECK ( l1ps_val498 IS NOT NULL),
    CONSTRAINT            l1ps_val499_NN         CHECK ( l1ps_val499 IS NOT NULL),
    CONSTRAINT            l1ps_val500_NN         CHECK ( l1ps_val500 IS NOT NULL),
    CONSTRAINT            l1ps_val501_NN         CHECK ( l1ps_val501 IS NOT NULL),
    CONSTRAINT            l1ps_val502_NN         CHECK ( l1ps_val502 IS NOT NULL),
    CONSTRAINT            l1ps_val503_NN         CHECK ( l1ps_val503 IS NOT NULL),
    CONSTRAINT            l1ps_val504_NN         CHECK ( l1ps_val504 IS NOT NULL),
    CONSTRAINT            l1ps_val505_NN         CHECK ( l1ps_val505 IS NOT NULL),
    CONSTRAINT            l1ps_val506_NN         CHECK ( l1ps_val506 IS NOT NULL),
    CONSTRAINT            l1ps_val507_NN         CHECK ( l1ps_val507 IS NOT NULL),
    CONSTRAINT            l1ps_val508_NN         CHECK ( l1ps_val508 IS NOT NULL),
    CONSTRAINT            l1ps_val509_NN         CHECK ( l1ps_val509 IS NOT NULL),
    CONSTRAINT            l1ps_val510_NN         CHECK ( l1ps_val510 IS NOT NULL),
    CONSTRAINT            l1ps_val511_NN         CHECK ( l1ps_val511 IS NOT NULL),
    CONSTRAINT            l1ps_val512_NN         CHECK ( l1ps_val512 IS NOT NULL),
CONSTRAINT            l1ps_used_NN        CHECK ( l1ps_used IS NOT NULL)
);


---------------------------
-- N to N relationships --
--------------------------

-- Menu and prescale relation
CREATE TABLE l1_tm_to_ps ( 
    l1tm2ps_id             NUMBER(10),    
    l1tm2ps_trigger_menu_id     NUMBER(10),    
    l1tm2ps_prescale_set_id     NUMBER(10),    
    l1tm2ps_used            CHAR            default 0,
    CONSTRAINT            l1tm2ps_pk        PRIMARY KEY (l1tm2ps_id),
    CONSTRAINT            l1tm2ps_fk_tm        FOREIGN KEY (l1tm2ps_trigger_menu_id)
                                REFERENCES l1_trigger_menu(l1tm_id),
    CONSTRAINT            l1tm2ps_fk_ps         FOREIGN KEY (l1tm2ps_prescale_set_id)
                                REFERENCES l1_prescale_set(l1ps_id),
    CONSTRAINT            l1tm2ps_id_NN         CHECK ( l1tm2ps_id IS NOT NULL),
    CONSTRAINT            l1tm2ps_menu_id_NN     CHECK ( l1tm2ps_trigger_menu_id IS NOT NULL),
    CONSTRAINT            l1tm2ps_pss_id_NN     CHECK ( l1tm2ps_prescale_set_id IS NOT NULL),
    CONSTRAINT            l1tm2ps_used_NN     CHECK ( l1tm2ps_used IS NOT NULL)
);
CREATE INDEX l1tm2ps_trigger_menu_id_ind ON l1_tm_to_ps(l1tm2ps_trigger_menu_id) COMPRESS 1;
CREATE INDEX l1tm2ps_prescale_set_id_ind ON l1_tm_to_ps(l1tm2ps_prescale_set_id) COMPRESS 1;

-- this table encodes the n-n relationship between trigger
-- menu and trigger items. The ti_ctp_id is needed to associate
-- the correct prescale factor from the presclae factor set with the trigger
-- item.
CREATE TABLE l1_tm_to_ti ( 
    l1tm2ti_id             NUMBER(10),
    l1tm2ti_trigger_menu_id     NUMBER(10),
    l1tm2ti_trigger_item_id     NUMBER(10),
    CONSTRAINT            l1tm2ti_pk        PRIMARY KEY (l1tm2ti_id),
    CONSTRAINT            l1tm2ti_fk_tm        FOREIGN KEY (l1tm2ti_trigger_menu_id) 
                                REFERENCES l1_trigger_menu(l1tm_id),
    CONSTRAINT            l1tm2ti_fk_ti         FOREIGN KEY (l1tm2ti_trigger_item_id) 
                                REFERENCES l1_trigger_item(l1ti_id),
    CONSTRAINT            l1tm2ti_id_NN         CHECK ( l1tm2ti_id IS NOT NULL),
    CONSTRAINT            l1tm2ti_trigger_menu_id_NN         CHECK ( l1tm2ti_trigger_menu_id IS NOT NULL),
    CONSTRAINT            l1tm2ti_trigger_item_id_NN         CHECK ( l1tm2ti_trigger_item_id IS NOT NULL)
);
CREATE INDEX l1tm2ti_trigger_menu_id_ind ON l1_tm_to_ti(l1tm2ti_trigger_menu_id) COMPRESS 1;
CREATE INDEX l1tm2ti_trigger_item_id_ind ON l1_tm_to_ti(l1tm2ti_trigger_item_id) COMPRESS 1;

-- here the n-n relationship between trigger item and trigger threshold is
-- encoded. the ti_tt_position and ti_tt_multiplicity are needed for
-- complicated logic structures. ti_tt_position gives the position of the
-- threshold inside of the logical expression defined in the ti_definition
-- of the references trigger item. ti_tt_multiplicity is just the requied 
-- multiplicity.
CREATE TABLE l1_ti_to_tt ( 
    l1ti2tt_id             NUMBER(10),
    l1ti2tt_trigger_item_id     NUMBER(10),
    l1ti2tt_trigger_threshold_id     NUMBER(10),
    l1ti2tt_position        NUMBER(10),
    l1ti2tt_multiplicity        NUMBER(10),
    CONSTRAINT            l1ti2tt_pk        PRIMARY KEY (l1ti2tt_id),
    CONSTRAINT            l1ti2tt_fk_ti        FOREIGN KEY (l1ti2tt_trigger_item_id) 
                                REFERENCES l1_trigger_item(l1ti_id),
    CONSTRAINT            l1ti2tt_fk_tt        FOREIGN KEY (l1ti2tt_trigger_threshold_id)
                                REFERENCES l1_trigger_threshold(l1tt_id),
    CONSTRAINT            l1ti2tt_id_NN         CHECK ( l1ti2tt_id IS NOT NULL),
    CONSTRAINT            l1ti2tt_item_id_NN     CHECK ( l1ti2tt_trigger_item_id IS NOT NULL),
    CONSTRAINT            l1ti2tt_thres_id_NN     CHECK ( l1ti2tt_trigger_threshold_id IS NOT NULL),
    CONSTRAINT            l1ti2tt_position_NN     CHECK ( l1ti2tt_position IS NOT NULL),
    CONSTRAINT            l1ti2tt_multiplicity_NN CHECK ( l1ti2tt_multiplicity IS NOT NULL)
);
CREATE INDEX l1ti2tt_trigger_item_id_ind  ON l1_ti_to_tt(l1ti2tt_trigger_item_id) COMPRESS 1;
CREATE INDEX l1ti2tt_trigger_thres_id_ind ON l1_ti_to_tt(l1ti2tt_trigger_threshold_id) COMPRESS 1;

-- n-n relationship between trigger threshold and trigger threshold value.
CREATE TABLE l1_tt_to_ttv (
    l1tt2ttv_id             NUMBER(10),     
    l1tt2ttv_trigger_threshold_id     NUMBER(10),     
    l1tt2ttv_trig_thres_value_id     NUMBER(10),     
    CONSTRAINT            l1tt2ttv_pk        PRIMARY KEY (l1tt2ttv_id),
    CONSTRAINT            l1tt2ttv_fk_tt        FOREIGN KEY (l1tt2ttv_trigger_threshold_id)
                                REFERENCES l1_trigger_threshold(l1tt_id),
    CONSTRAINT            l1tt2ttv_fk_ttv        FOREIGN KEY (l1tt2ttv_trig_thres_value_id)
                                REFERENCES l1_trigger_threshold_value(l1ttv_id),
    CONSTRAINT            l1tt2ttv_id_NN         CHECK ( l1tt2ttv_id IS NOT NULL),
    CONSTRAINT            l1tt2ttv_thres_id_NN    CHECK ( l1tt2ttv_trigger_threshold_id IS NOT NULL),
    CONSTRAINT            l1tt2ttv_thres_va_id_NN CHECK ( l1tt2ttv_trig_thres_value_id IS NOT NULL)
);
CREATE INDEX l1tt2ttv_trigger_thres_id_ind  ON l1_tt_to_ttv(l1tt2ttv_trigger_threshold_id) COMPRESS 1;
CREATE INDEX l1tt2ttv_trig_thres_val_id_ind ON l1_tt_to_ttv(l1tt2ttv_trig_thres_value_id) COMPRESS 1;

-- This table list all thresholds of a menu that is included in the menu
-- by hand, ie. which are not part of the menu via items. 
CREATE TABLE l1_tm_to_tt_forced ( 
    l1tm2ttf_id             NUMBER(10), 
    l1tm2ttf_trigger_menu_id     NUMBER(10), 
    l1tm2ttf_trigger_threshold_id     NUMBER(10), 
    CONSTRAINT            l1tm2ttf_pk        PRIMARY KEY (l1tm2ttf_id),
    CONSTRAINT            l1tm2ttf_fk_tm        FOREIGN KEY (l1tm2ttf_trigger_menu_id) 
                                REFERENCES l1_trigger_menu(l1tm_id),
    CONSTRAINT            l1tm2ttf_fk_tt        FOREIGN KEY (l1tm2ttf_trigger_threshold_id)
                                REFERENCES l1_trigger_threshold(l1tt_id),
    CONSTRAINT            l1tm2ttf_id_NN         CHECK ( l1tm2ttf_id IS NOT NULL),
    CONSTRAINT            l1tm2ttf_menu_id_NN    CHECK ( l1tm2ttf_trigger_menu_id IS NOT NULL),
    CONSTRAINT            l1tm2ttf_thres_id_NN     CHECK ( l1tm2ttf_trigger_threshold_id IS NOT NULL)
);
CREATE INDEX l1tm2ttf_trigger_menu_id_ind ON l1_tm_to_tt_forced(l1tm2ttf_trigger_menu_id) COMPRESS 1;
CREATE INDEX l1tm2ttf_trigger_thresid_ind ON l1_tm_to_tt_forced(l1tm2ttf_trigger_threshold_id) COMPRESS 1;

-- This table is needed to store the information on which cable
-- the thresholds are delivered to the CTP. This information depends
-- on the thresholds and on the trigger menu (i.e. which other thresholds
-- are present in that trigger menu). The thresholds of a menu can be found via the
-- l1_tm_to_ti and via the l1_tm_to_tt_forced tables. Therefore the information in this table 
-- (i.e. l1_tm_to_tt) must be compiled from l1_tm_to_ti and from the
-- l1_tm_to_tt_forced table. It introduces some data redundancy. 
CREATE TABLE l1_tm_to_tt ( 
    l1tm2tt_id                   NUMBER(10),     
    l1tm2tt_trigger_menu_id      NUMBER(10),     
    l1tm2tt_trigger_threshold_id NUMBER(10),     
    l1tm2tt_cable_name           VARCHAR2(12),    
    l1tm2tt_cable_ctpin          VARCHAR2(10),    
    l1tm2tt_cable_connector      VARCHAR2(10),    
    l1tm2tt_cable_start          NUMBER(10),    
    l1tm2tt_cable_end            NUMBER(10),    
    l1tm2tt_cable_clock          NUMBER(2),    
    CONSTRAINT            l1tm2tt_pk        PRIMARY KEY (l1tm2tt_id),
    CONSTRAINT            l1tm2tt_fk_tm        FOREIGN KEY (l1tm2tt_trigger_menu_id) 
                                REFERENCES l1_trigger_menu(l1tm_id),
    CONSTRAINT            l1tm2tt_fk_tt        FOREIGN KEY (l1tm2tt_trigger_threshold_id)
                                REFERENCES l1_trigger_threshold(l1tt_id),
    CONSTRAINT            l1tm2tt_id_NN              CHECK ( l1tm2tt_id IS NOT NULL),
    CONSTRAINT            l1tm2tt_menu_id_NN         CHECK ( l1tm2tt_trigger_menu_id IS NOT NULL),
    CONSTRAINT            l1tm2tt_thres_id_NN        CHECK ( l1tm2tt_trigger_threshold_id IS NOT NULL),
    CONSTRAINT            l1tm2tt_cable_name_NN      CHECK ( l1tm2tt_cable_name IS NOT NULL),
    CONSTRAINT            l1tm2tt_cable_ctpin_NN     CHECK ( l1tm2tt_trigger_threshold_id IS NOT NULL),
    CONSTRAINT            l1tm2tt_cable_conn_NN      CHECK ( l1tm2tt_cable_connector IS NOT NULL),
    CONSTRAINT            l1tm2tt_cable_start_NN     CHECK ( l1tm2tt_cable_start IS NOT NULL),
    CONSTRAINT            l1tm2tt_cable_end_NN       CHECK ( l1tm2tt_cable_end IS NOT NULL),
    CONSTRAINT            l1tm2tt_cable_clock_NN     CHECK ( l1tm2tt_cable_clock IS NOT NULL)
);
CREATE INDEX l1tm2tt_trigger_menu_id_ind  ON l1_tm_to_tt(l1tm2tt_trigger_menu_id) COMPRESS 1;
CREATE INDEX l1tm2tt_trigger_thres_id_ind ON l1_tm_to_tt(l1tm2tt_trigger_threshold_id) COMPRESS 1;

-- hold the l1 pit information
CREATE TABLE l1_pits (
    l1pit_id            NUMBER(10),     
        l1pit_tm_to_tt_id            NUMBER(10), 
        l1pit_pit_number             NUMBER(10), 
        l1pit_threshold_bit          NUMBER(2),  
    CONSTRAINT            l1pit_pk        PRIMARY KEY (l1pit_id),
        CONSTRAINT                l1pit_tm_to_tt_id_fk    FOREIGN KEY (l1pit_tm_to_tt_id)
                                                REFERENCES  l1_tm_to_tt(l1tm2tt_id),
    CONSTRAINT            l1pit_id_NN         CHECK ( l1pit_id IS NOT NULL),
    CONSTRAINT            l1pit_tm_to_tt_id_NN     CHECK ( l1pit_tm_to_tt_id IS NOT NULL),
    CONSTRAINT            l1pit_pit_number_NN     CHECK ( l1pit_pit_number IS NOT NULL),
    CONSTRAINT            l1pit_threshold_bit_NN     CHECK ( l1pit_threshold_bit IS NOT NULL)
);
CREATE INDEX l1pit_tm_to_tt_id_ind ON l1_pits(l1pit_tm_to_tt_id) COMPRESS 1;

--Monitoring
CREATE TABLE l1_tm_to_tt_mon (
    l1tm2ttm_id             NUMBER(10),
    l1tm2ttm_trigger_menu_id     NUMBER(10),
    l1tm2ttm_trigger_threshold_id     NUMBER(10),
    l1tm2ttm_name            VARCHAR2(50),
    l1tm2ttm_internal_counter    NUMBER(3),
    l1tm2ttm_bunch_group_id        NUMBER(10),
    l1tm2ttm_counter_type        VARCHAR2(10),
    l1tm2ttm_multiplicity        NUMBER(3),
    CONSTRAINT            l1tm2ttm_pk        PRIMARY KEY (l1tm2ttm_id),
    CONSTRAINT            l1tm2ttm_fk_tm        FOREIGN KEY (l1tm2ttm_trigger_menu_id) 
                                REFERENCES l1_trigger_menu(l1tm_id),
    CONSTRAINT            l1tm2ttm_fk_tt        FOREIGN KEY (l1tm2ttm_trigger_threshold_id)
                                REFERENCES l1_trigger_threshold(l1tt_id),
    CONSTRAINT            l1tm2ttm_id_NN         CHECK ( l1tm2ttm_id IS NOT NULL),
    CONSTRAINT            l1tm2ttm_menu_id_NN    CHECK ( l1tm2ttm_trigger_menu_id IS NOT NULL),
    CONSTRAINT            l1tm2ttm_thres_id_NN     CHECK ( l1tm2ttm_trigger_threshold_id IS NOT NULL),
    CONSTRAINT            l1tm2ttm_name_NN     CHECK ( l1tm2ttm_name IS NOT NULL),
    CONSTRAINT            l1tm2ttm_int_counter_NN CHECK ( l1tm2ttm_internal_counter IS NOT NULL),
    CONSTRAINT            l1tm2ttm_bch_grp_id_NN     CHECK ( l1tm2ttm_bunch_group_id IS NOT NULL),
    CONSTRAINT            l1tm2ttm_cter_type_NN     CHECK ( l1tm2ttm_counter_type IS NOT NULL),
    CONSTRAINT            l1tm2ttm_mplicity_NN     CHECK ( l1tm2ttm_multiplicity IS NOT NULL)
);
CREATE INDEX l1tm2ttm_trigger_menu_id_ind  ON l1_tm_to_tt_mon(l1tm2ttm_trigger_menu_id) COMPRESS 1;
CREATE INDEX l1tm2ttm_trigger_thres_id_ind ON l1_tm_to_tt_mon(l1tm2ttm_trigger_threshold_id) COMPRESS 1;

-- n-n relationship between the bunch group set and the bunch groups. The
-- l1_bgs_bg_internal_number is needed to label the bunch group within the 
-- bunch group set that is associated to a trigger menu. Inside the trigger
-- threshold defintion these internal numbers are referenced.
-- WPV add name field, varchar(20/25), only meaningful for particular BG in this BGS
-- add partition field (just in case) - number(1), default 0
CREATE TABLE l1_bgs_to_bg (
    l1bgs2bg_id             NUMBER(10),
    l1bgs2bg_bunch_group_set_id     NUMBER(10),
    l1bgs2bg_bunch_group_id     NUMBER(10),
    l1bgs2bg_internal_number    NUMBER(2),
    l1bgs2bg_bunch_group_name    VARCHAR(25),
    l1bgs2bg_partition    NUMBER(1),
    CONSTRAINT            l1bgs2bg_pk        PRIMARY KEY (l1bgs2bg_id),
    CONSTRAINT            l1bgs2bg_fk_bgs        FOREIGN KEY (l1bgs2bg_bunch_group_set_id)
                                REFERENCES l1_bunch_group_set(l1bgs_id),
    CONSTRAINT            l1bgs2bg_fk_bg        FOREIGN KEY (l1bgs2bg_bunch_group_id)
                                REFERENCES l1_bunch_group(l1bg_id),
    CONSTRAINT            l1bgs2bg_id_NN         CHECK ( l1bgs2bg_id IS NOT NULL),
    CONSTRAINT            l1bgs2bg_bgrp_set_id_NN CHECK ( l1bgs2bg_bunch_group_set_id IS NOT NULL),
    CONSTRAINT            l1bgs2bg_bgrp_id_NN     CHECK ( l1bgs2bg_bunch_group_id IS NOT NULL),
    CONSTRAINT            l1bgs2bg_int_number_NN     CHECK ( l1bgs2bg_internal_number IS NOT NULL),
    CONSTRAINT            l1bgs2bg_bunch_group_name     CHECK ( l1bgs2bg_bunch_group_name IS NOT NULL),
    CONSTRAINT            l1bgs2bg_partition_NN     CHECK ( l1bgs2bg_partition IS NOT NULL)
);
CREATE INDEX l1bgs2bg_bgroup_set_id_ind ON l1_bgs_to_bg(l1bgs2bg_bunch_group_set_id) COMPRESS 1;
CREATE INDEX l1bgs2bg_bgroup_id_ind     ON l1_bgs_to_bg(l1bgs2bg_bunch_group_id) COMPRESS 1;

-- this tables associates the various bunches to the bunch groups
CREATE TABLE l1_bg_to_b (
    l1bg2b_id             NUMBER(10),     
    l1bg2b_bunch_group_id         NUMBER(10),     
    l1bg2b_bunch_number        NUMBER(10),    
    CONSTRAINT            l1bg2b_pk        PRIMARY KEY (l1bg2b_id),
    CONSTRAINT            l1bg2b_fk_bg        FOREIGN KEY (l1bg2b_bunch_group_id)
                                REFERENCES l1_bunch_group(l1bg_id),
    CONSTRAINT            l1bg2b_id_NN         CHECK ( l1bg2b_id IS NOT NULL),
    CONSTRAINT            l1bg2b_bch_group_id_NN     CHECK ( l1bg2b_bunch_group_id IS NOT NULL),
    CONSTRAINT            l1bg2b_bch_number_NN     CHECK ( l1bg2b_bunch_number IS NOT NULL)
);
CREATE INDEX l1bg2b_bunch_group_id_ind ON l1_bg_to_b(l1bg2b_bunch_group_id) COMPRESS 1;

CREATE TABLE l1_ci_to_csc (
    l1ci2csc_id                     NUMBER(10), 
    l1ci2csc_calo_info_id           NUMBER(10), 
    l1ci2csc_calo_sin_cos_id        NUMBER(10), 
    CONSTRAINT                      l1ci2csc_pk         PRIMARY KEY (l1ci2csc_id),
    CONSTRAINT                      l1ci2csc_fk_ci      FOREIGN KEY (l1ci2csc_calo_info_id)
                                                        REFERENCES l1_calo_info(l1ci_id),
    CONSTRAINT                      l1ci2csc_fk_csc     FOREIGN KEY (l1ci2csc_calo_sin_cos_id)
                                                        REFERENCES l1_calo_sin_cos(l1csc_id),
    CONSTRAINT            l1ci2csc_id_NN          CHECK ( l1ci2csc_id IS NOT NULL),
    CONSTRAINT            l1ci2csc_calo_info_NN   CHECK ( l1ci2csc_calo_info_id IS NOT NULL),
    CONSTRAINT            l1ci2csc_calo_sincos_NN CHECK ( l1ci2csc_calo_sin_cos_id IS NOT NULL)
);
CREATE INDEX l1ci2csc_calo_info_id_ind    ON l1_ci_to_csc(l1ci2csc_calo_info_id) COMPRESS 1;
CREATE INDEX l1ci2csc_calo_sin_cos_id_ind ON l1_ci_to_csc(l1ci2csc_calo_sin_cos_id) COMPRESS 1;

-- This table binds together all parts of the LVL1 configuration. This
-- mechanism allows to separately change the various parts separately.
-- E.g. prescale sets can be changed more often than menus.

CREATE TABLE l1_master_table (
    l1mt_id                 NUMBER(10),
    l1mt_name            VARCHAR2(50),
    l1mt_version            NUMBER(11),
    l1mt_comment            VARCHAR2(200),
    l1mt_trigger_menu_id        NUMBER(10),
    l1mt_muctpi_info_id        NUMBER(10),
    l1mt_random_id            NUMBER(10),
    l1mt_prescaled_clock_id        NUMBER(10),
    l1mt_calo_info_id        NUMBER(10),
    l1mt_muon_threshold_set_id       NUMBER(10),
    l1mt_ctpversion       NUMBER(10),
    l1mt_l1version       NUMBER(10),
    l1mt_username            VARCHAR2(50),
    l1mt_modified_time        TIMESTAMP,
    l1mt_status            NUMBER(2),
    l1mt_used            CHAR            default 0,
    CONSTRAINT l1mt_pk      PRIMARY KEY (l1mt_id),
    CONSTRAINT l1mt_fk_tm   FOREIGN KEY (l1mt_trigger_menu_id)          REFERENCES l1_trigger_menu(l1tm_id),
    CONSTRAINT l1mt_fk_mi   FOREIGN KEY (l1mt_muctpi_info_id)           REFERENCES l1_muctpi_info(l1mi_id),
    CONSTRAINT l1mt_fk_r    FOREIGN KEY (l1mt_random_id)                REFERENCES l1_random(l1r_id),
    CONSTRAINT l1mt_fk_psc  FOREIGN KEY (l1mt_prescaled_clock_id)       REFERENCES l1_prescaled_clock(l1pc_id),
    CONSTRAINT l1mt_fk_ci   FOREIGN KEY (l1mt_calo_info_id)             REFERENCES l1_calo_info(l1ci_id),
    CONSTRAINT l1mt_fk_mt   FOREIGN KEY (l1mt_muon_threshold_set_id)    REFERENCES l1_muon_threshold_set(l1mts_id),
    CONSTRAINT l1mt_nmver               UNIQUE (l1mt_name, l1mt_version),
    CONSTRAINT l1mt_id_NN               CHECK ( l1mt_id IS NOT NULL),
    CONSTRAINT l1mt_name_NN             CHECK ( l1mt_name IS NOT NULL),
    CONSTRAINT l1mt_version_NN          CHECK ( l1mt_version IS NOT NULL),
    CONSTRAINT l1mt_trigger_menu_id_NN  CHECK ( l1mt_trigger_menu_id IS NOT NULL),
    CONSTRAINT l1mt_muctpi_info_id_NN   CHECK ( l1mt_muctpi_info_id IS NOT NULL),
    CONSTRAINT l1mt_random_id_NN        CHECK ( l1mt_random_id IS NOT NULL),
    CONSTRAINT l1mt_pscale_clock_id_NN  CHECK ( l1mt_prescaled_clock_id IS NOT NULL),
    CONSTRAINT l1mt_calo_info_id_NN     CHECK ( l1mt_calo_info_id IS NOT NULL),
    CONSTRAINT l1mt_muon_thres_set_NN   CHECK ( l1mt_muon_threshold_set_id IS NOT NULL),
    CONSTRAINT l1mt_status_NN           CHECK ( l1mt_status IS NOT NULL),
    CONSTRAINT l1mt_used_NN             CHECK ( l1mt_used IS NOT NULL)
);
CREATE INDEX l1mt_trigger_menu_id_ind       ON l1_master_table(l1mt_trigger_menu_id) COMPRESS 1;
CREATE INDEX l1mt_muctpi_info_id_ind        ON l1_master_table(l1mt_muctpi_info_id) COMPRESS 1;
CREATE INDEX l1mt_random_id_ind             ON l1_master_table(l1mt_random_id) COMPRESS 1;
CREATE INDEX l1mt_prescaled_clock_id_ind    ON l1_master_table(l1mt_prescaled_clock_id) COMPRESS 1;
CREATE INDEX l1mt_calo_info_id_ind          ON l1_master_table(l1mt_calo_info_id) COMPRESS 1;
CREATE INDEX l1mt_muon_threshold_set_id_ind ON l1_master_table(l1mt_muon_threshold_set_id) COMPRESS 1;

-- Here lies the tables which describe the L1Topo config. May they rest in peace.
-- Starting with creating the tables with no foreign keys

-- A table to hold the top level of the topo trigger menu.
-- This will be the main table that described the configuration and output to be used.
CREATE TABLE topo_trigger_menu (
    ttm_id   		NUMBER(10),
    ttm_name		VARCHAR2(50),
    ttm_version 	NUMBER(10),
    ttm_ctplink_id 	NUMBER(10),
    CONSTRAINT 		ttm_pk		PRIMARY KEY (ttm_id),
    CONSTRAINT 		ttm_fk_ctplink  FOREIGN KEY (ttm_ctplink_id)          REFERENCES topo_output_list(ol_id),
    CONSTRAINT          ttm_nmver     	UNIQUE (ttm_name, ttm_version),
    CONSTRAINT		ttm_id_NN	CHECK ( ttm_id IS NOT NULL),
    CONSTRAINT		ttm_name_NN	CHECK ( ttm_name IS NOT NULL),
    CONSTRAINT 		ttm_version_NN	CHECK ( ttm_version IS NOT NULL)
);
CREATE INDEX ttm_ctplink_id_ind ON topo_trigger_menu (ttm_ctplink_id) COMPRESS 1;


CREATE TABLE topo_algo (
    ta_id    	       NUMBER(10),
    ta_name	       VARCHAR2(50),
    ta_output	       VARCHAR2(50),
    ta_type	       VARCHAR2(50),
    -- The number of output bits for this algo always 0 for sort algos
    ta_bits	       NUMBER(10,0),
    -- best thing to store this type as?
    ta_sort_deci       VARCHAR2(50),
    ta_algo_id	       NUMBER(10),
    CONSTRAINT	       ta_pk		PRIMARY KEY (ta_id),
    CONSTRAINT	       ta_id_NN		CHECK( ta_id IS NOT NULL),
    CONSTRAINT	       ta_name_NN	CHECK( ta_name IS NOT NULL),
    CONSTRAINT	       ta_output_NN	CHECK( ta_output IS NOT NULL),	
    CONSTRAINT	       ta_type_NN	CHECK( ta_type IS NOT NULL),
    CONSTRAINT	       ta_sort_deci_NN	CHECK( ta_sort_deci IS NOT NULL)
);
	
-- These tables contain foreign keys, either to those created above or to other FK tables.
-- Create them in a sensible order please, dont try to link to a table that doesnt yet exist.

-- This table stores the inputs to the sorting and decision algos. Only
-- decision algos have a position info.
CREATE TABLE topo_algo_input (
    tai_id   		NUMBER(10),
    tai_name		VARCHAR2(50),
    tai_value		VARCHAR2(50),
    tai_position	NUMBER(10),
    CONSTRAINT		tai_pk		PRIMARY KEY (tai_id),
    CONSTRAINT		tai_id_NN	CHECK( tai_id IS NOT NULL),
    CONSTRAINT		tai_name_NN	CHECK( tai_name IS NOT NULL),
    CONSTRAINT		tai_value_NN	CHECK( tai_value IS NOT NULL)
);

-- Table to hold the topo algorithm outputs for both the sorting and decision algos.
-- Be careful there is no NOT NULL requirement for the bits/value as these are unique to the
-- decision/sorting tables respectively. 
CREATE TABLE topo_algo_output (
    tao_id   		NUMBER(10),
    tao_name		VARCHAR2(50),
    tao_value		VARCHAR2(50),
    tao_selection	NUMBER(10),
    tao_bitname		VARCHAR2(50),
    CONSTRAINT		tao_pk		PRIMARY KEY (tao_id),
    CONSTRAINT		tao_id_NN	CHECK( tao_id IS NOT NULL ),
    CONSTRAINT		tao_name_NN	CHECK( tao_name IS NOT NULL ),	
    CONSTRAINT		tao_value_NN	CHECK( tao_value IS NOT NULL ),	
    CONSTRAINT		tao_selection_NN	CHECK( tao_selection IS NOT NULL ),	
    CONSTRAINT		tao_bitname_NN	CHECK( tao_bitname IS NOT NULL )	
);

-- Table to hold the parameters for both the sorting and decision algorithms.
-- Be careful as there is no NOT NULL constraint on the position and selection as they
-- only appear in the decision algorithms!
CREATE TABLE topo_parameter (
    tp_id    		NUMBER(10),
    tp_name		VARCHAR2(50),
    tp_position		NUMBER(10),
    tp_value		NUMBER(10),
    tp_selection	NUMBER(10),
    CONSTRAINT		tp_pk		PRIMARY KEY (tp_id),
    CONSTRAINT		tp_id_NN	CHECK( tp_id IS NOT NULL),
    CONSTRAINT		tp_name_NN	CHECK( tp_name IS NOT NULL),
    CONSTRAINT		tp_value_NN	CHECK( tp_value IS NOT NULL)
);

-- A table to describe the generic parameters for the sorting and decision algorithms.
CREATE TABLE topo_generic (
    tg_id    		NUMBER(10),
    tg_name		VARCHAR2(50),
    tg_value		VARCHAR2(50),
    CONSTRAINT		tg_pk		PRIMARY KEY (tg_id),
    CONSTRAINT 		tg_id_NN 	CHECK( tg_id IS NOT NULL ),		
    CONSTRAINT 		tg_name_NN 	CHECK( tg_name IS NOT NULL ),		
    CONSTRAINT 		tg_value_NN 	CHECK( tg_value IS NOT NULL )
);

-- A table to describe the topo configuration. Not sure if this should be linked by a
-- link table to the menu yet?
CREATE TABLE topo_config (
    tc_id              NUMBER(10),
    tc_name	       VARCHAR2(50),
    tc_value	       VARCHAR2(50),
    CONSTRAINT	       tc_pk		PRIMARY KEY (tc_id),
    CONSTRAINT	       tc_id_NN		CHECK( tc_id IS NOT NULL),
    CONSTRAINT	       tc_name_NN	CHECK( tc_name IS NOT NULL),
    CONSTRAINT	       tc_value_NN	CHECK( tc_value IS NOT NULL)
);

-- A table to describe the topo output. Not sure if this should be linked by a
-- link table to the menu yet?
CREATE TABLE topo_output_line (
    tol_id    	       NUMBER(10),
    tol_algo_name      VARCHAR2(50),
    tol_triggerline      VARCHAR2(200),
    tol_algo_id	       NUMBER(10),
    tol_module 	       NUMBER(2),
    tol_fpga	       NUMBER(2),
    tol_first_bit       NUMBER(10),
    tol_clock	       NUMBER(1),
    CONSTRAINT	       tol_pk		PRIMARY KEY (tol_id),
    CONSTRAINT	       tol_id_NN	CHECK( tol_id IS NOT NULL),
    CONSTRAINT	       tol_algo_name_NN	CHECK( tol_algo_name IS NOT NULL),
    CONSTRAINT	       tol_triggerline_NN	CHECK( tol_triggerline IS NOT NULL),
    CONSTRAINT	       tol_algo_id_NN	CHECK( tol_algo_id IS NOT NULL),
    CONSTRAINT	       tol_fmodule_NN	CHECK( tol_module IS NOT NULL),
    CONSTRAINT	       tol_fpga_NN	CHECK( tol_fpga IS NOT NULL),
    CONSTRAINT	       tol_first_bit_NN	CHECK( tol_first_bit IS NOT NULL),
    CONSTRAINT	       tol_clock_NN	CHECK( tol_clock IS NOT NULL)
);

-- N-M tables

-- A link table between the menu tables and the algorithms.
-- There is likely to be many occasions when the same algo is needed in
-- different menus so this seems a sensible table to add
CREATE TABLE ttm_to_ta (
    ttm2ta_id		NUMBER(10),
    ttm2ta_menu_id	NUMBER(10),
    ttm2ta_algo_id	NUMBER(10),
    CONSTRAINT          ttm2ta_pk	    PRIMARY KEY (ttm2ta_id),
    CONSTRAINT          ttm2ta_fk_out        FOREIGN KEY (ttm2ta_menu_id)
                                REFERENCES topo_trigger_menu(ttm_id),
    CONSTRAINT          ttm2ta_fk_algo        FOREIGN KEY (ttm2ta_algo_id)
                                REFERENCES topo_algo(ta_id),
    CONSTRAINT 		ttm2ta_id_NN 	   CHECK( ttm2ta_id IS NOT NULL ),
    CONSTRAINT 		ttm2ta_menu_id_NN 	   CHECK( ttm2ta_menu_id IS NOT NULL ),
    CONSTRAINT 		ttm2ta_algo_id_NN 	   CHECK( ttm2ta_algo_id IS NOT NULL )
);
CREATE INDEX ttm2ta_menu_id_ind       ON ttm_to_ta(ttm2ta_menu_id) COMPRESS 1;
CREATE INDEX ttm2ta_algo_id_ind       ON ttm_to_ta(ttm2ta_algo_id) COMPRESS 1;

CREATE TABLE ttm_to_tc (
    ttm2tc_id		NUMBER(10),
    ttm2tc_menu_id	NUMBER(10),
    ttm2tc_config_id	NUMBER(10),
    CONSTRAINT          ttm2tc_pk	    PRIMARY KEY (ttm2tc_id),
    CONSTRAINT          ttm2tc_fk_out        FOREIGN KEY (ttm2tc_menu_id)
                                REFERENCES topo_trigger_menu(ttm_id),
    CONSTRAINT          ttm2tc_fk_config        FOREIGN KEY (ttm2tc_config_id)
                                REFERENCES topo_config(tc_id),
    CONSTRAINT 		ttm2tc_id_NN 	   CHECK( ttm2tc_id IS NOT NULL ),
    CONSTRAINT 		ttm2tc_menu_id_NN 	   CHECK( ttm2tc_menu_id IS NOT NULL ),
    CONSTRAINT 		ttm2tc_config_id_NN 	   CHECK( ttm2tc_config_id IS NOT NULL )
);
CREATE INDEX ttm2tc_menu_id_ind       ON ttm_to_tc(ttm2tc_menu_id) COMPRESS 1;
CREATE INDEX ttm2tc_config_id_ind     ON ttm_to_tc(ttm2tc_config_id) COMPRESS 1;

CREATE TABLE ta_to_ti (
    ta2ti_id		NUMBER(10),
    ta2ti_algo_id	NUMBER(10),
    ta2ti_input_id	NUMBER(10),
    CONSTRAINT          ta2ti_pk	    PRIMARY KEY (ta2ti_id),
    CONSTRAINT          ta2ti_fk_out        FOREIGN KEY (ta2ti_algo_id)
                                REFERENCES topo_algo(ta_id),
    CONSTRAINT          ta2ti_fk_input        FOREIGN KEY (ta2ti_input_id)
                                REFERENCES topo_algo_input(tai_id),
    CONSTRAINT 		ta2ti_id_NN 	   CHECK( ta2ti_id IS NOT NULL ),
    CONSTRAINT 		ta2ti_algo_id_NN 	   CHECK( ta2ti_algo_id IS NOT NULL ),
    CONSTRAINT 		ta2ti_input_id_NN 	   CHECK( ta2ti_input_id IS NOT NULL )
);
CREATE INDEX ta2ti_algo_id_ind       ON ta_to_ti(ta2ti_algo_id) COMPRESS 1;
CREATE INDEX ta2ti_input_id_ind      ON ta_to_ti(ta2ti_input_id) COMPRESS 1;

CREATE TABLE ta_to_tp (
    ta2tp_id            NUMBER(10),
    ta2tp_algo_id       NUMBER(10),
    ta2tp_param_id      NUMBER(10),
    CONSTRAINT          ta2tp_pk            PRIMARY KEY (ta2tp_id),
    CONSTRAINT          ta2tp_fk_out        FOREIGN KEY (ta2tp_algo_id)
                                REFERENCES topo_algo(ta_id),
    CONSTRAINT          ta2tp_fk_param        FOREIGN KEY (ta2tp_param_id)
                                REFERENCES topo_parameter(tp_id),
    CONSTRAINT          ta2tp_id_NN        CHECK( ta2tp_id IS NOT NULL ),
    CONSTRAINT          ta2tp_algo_id_NN           CHECK( ta2tp_algo_id IS NOT NULL ),
    CONSTRAINT          ta2tp_param_id_NN          CHECK( ta2tp_param_id IS NOT NULL )
);
CREATE INDEX ta2tp_algo_id_ind       ON ta_to_tp(ta2tp_algo_id) COMPRESS 1;
CREATE INDEX ta2tp_param_id_ind      ON ta_to_tp(ta2tp_param_id) COMPRESS 1;

CREATE TABLE ta_to_tg (
    ta2tg_id            NUMBER(10),
    ta2tg_algo_id       NUMBER(10),
    ta2tg_generic_id      NUMBER(10),
    CONSTRAINT          ta2tg_pk            PRIMARY KEY (ta2tg_id),
    CONSTRAINT          ta2tg_fk_out        FOREIGN KEY (ta2tg_algo_id)
                                REFERENCES topo_algo(ta_id),
    CONSTRAINT          ta2tg_fk_generic        FOREIGN KEY (ta2tg_generic_id)
                                REFERENCES topo_generic(tg_id),
    CONSTRAINT          ta2tg_id_NN        CHECK( ta2tg_id IS NOT NULL ),
    CONSTRAINT          ta2tg_algo_id_NN           CHECK( ta2tg_algo_id IS NOT NULL ),
    CONSTRAINT          ta2tg_generic_id_NN          CHECK( ta2tg_generic_id IS NOT NULL )
);
CREATE INDEX ta2tg_algo_id_ind       ON ta_to_tg(ta2tg_algo_id) COMPRESS 1;
CREATE INDEX ta2tg_generic_id_ind      ON ta_to_tg(ta2tg_generic_id) COMPRESS 1;

CREATE TABLE ta_to_to (
    ta2to_id		NUMBER(10),
    ta2to_algo_id	NUMBER(10),
    ta2to_output_id	NUMBER(10),
    CONSTRAINT          ta2to_pk	    PRIMARY KEY (ta2to_id),
    CONSTRAINT          ta2to_fk_out        FOREIGN KEY (ta2to_algo_id)
                                REFERENCES topo_algo(ta_id),
    CONSTRAINT          ta2to_fk_output        FOREIGN KEY (ta2to_output_id)
                                REFERENCES topo_algo_output(tao_id),
    CONSTRAINT 		ta2to_id_NN 	   CHECK( ta2to_id IS NOT NULL ),
    CONSTRAINT 		ta2to_algo_id_NN 	   CHECK( ta2to_algo_id IS NOT NULL ),
    CONSTRAINT 		ta2to_output_id_NN 	   CHECK( ta2to_output_id IS NOT NULL )
);
CREATE INDEX ta2to_algo_id_ind       ON ta_to_to(ta2to_algo_id) COMPRESS 1;
CREATE INDEX ta2to_output_id_ind      ON ta_to_to(ta2to_output_id) COMPRESS 1;

CREATE TABLE topo_output_link (
    tl_id		NUMBER(10),
    tl_link_id	NUMBER(10),
    tl_output_id	NUMBER(10),
    CONSTRAINT          tl_pk	    PRIMARY KEY (tl_id),
    CONSTRAINT          tl_fk_link        FOREIGN KEY (tl_link_id)
                                REFERENCES topo_output_list(ol_id),
    CONSTRAINT          tl_fk_output        FOREIGN KEY (tl_output_id)
                                REFERENCES topo_output_line(tol_id),
    CONSTRAINT 		tl_id_NN 	   CHECK( tl_id IS NOT NULL ),
    CONSTRAINT 		tl_link_id_NN 	   CHECK( tl_link_id IS NOT NULL ),
    CONSTRAINT 		tl_output_id_NN 	   CHECK( tl_output_id IS NOT NULL )
);
CREATE INDEX tl_link_id_ind       ON topo_output_link(tl_link_id) COMPRESS 1;
CREATE INDEX tl_output_id_ind       ON topo_output_link(tl_output_id) COMPRESS 1;

-- This table will be the link between the SMT and the L1 topo menu.
-- It binds together a complete L1Topo description and will be used as a key along side the SMK, L1, HLT
-- and bunch gorup keys to describe the currently loaded setup of the trigger. 
CREATE TABLE topo_master_table (
    tmt_id                 NUMBER(10),
    tmt_hash		   NUMBER(10),
    tmt_name               VARCHAR2(50),
    tmt_version            NUMBER(11),
    tmt_comment            VARCHAR2(200),
    tmt_trigger_menu_id    NUMBER(10),
    CONSTRAINT            tmt_pk            PRIMARY KEY (tmt_id),
    CONSTRAINT            tmt_fk_tm        FOREIGN KEY (tmt_trigger_menu_id)
                                REFERENCES topo_trigger_menu(ttm_id),
    CONSTRAINT            tmt_nmver        UNIQUE (tmt_name, tmt_version),
    CONSTRAINT            tmt_id_NN         CHECK ( tmt_id IS NOT NULL),
    CONSTRAINT            tmt_hash_NN         CHECK ( tmt_hash IS NOT NULL),
    CONSTRAINT            tmt_name_NN         CHECK ( tmt_name IS NOT NULL),
    CONSTRAINT            tmt_version_NN     CHECK ( tmt_version IS NOT NULL),
    CONSTRAINT            tmt_trigger_menu_id_NN CHECK ( tmt_trigger_menu_id IS NOT NULL)
);
CREATE INDEX tmt_trigger_menu_id_ind       ON topo_master_table(tmt_trigger_menu_id) COMPRESS 1;

--- HLT setup and release

-- Please emacs, make this buffer -*- sql -*-
-- Initially contributed by Johannes Haller
-- Modifications and usage Andre dos Anjos and Werner Wiedenmann
-- 18.11.05:     Modified by Johannes Haller, to allow setup for each algorithm
--        as agreed on in meeting on 17.11.05
--
-- This schema describes the ATLAS Trigger HLT database schema. 
--
-- The schema was designed based on a separation between logical and physical
-- software infrastructure. Every Trigger release will be composed of a set of 
-- libraries with given versions that are supposed to be run by the HLT
-- processors. This set of library releases represent the "physical setup".
-- The other part of the setup is represented by the options that need to be 
-- in place for a particular logical work to happen, for instance, to run 
-- T2Calo or a combined setup. The options for running T2Calo change less 
-- frequently than the library releases and therefore it makes no sense to 
-- bind the two together.
--
-- The union of a software release (physical) and a trigger configuration 
-- setup (logical) makes up a runnable environment which is defined by 
-- a master table that correlates the two parameters. This semantic separation
-- imposes the need to build a consistency checker that assures that all
-- components in a logical setup can be run in a given (physical) Trigger 
-- software release.
--
-- Versioning:
-- Versioning information is handled by a UNIQUE constraint. A combination of
-- a particular feature on the table + a version number should be always 
-- unique to guarantee that we dont have the same versions of particular
-- parameter or setup, for instance.
-- N-to-N relationship tables hardly need versioning information.

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- TABLES REPRESENTING THE LOGICAL SOFTWARE SETUP FOR A JOB
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- This table defines all parameters for all possible components I can run.
-- Different components can share the same set of paramters and every 
-- component has a set of parameters what defines a M:N relationship. This
-- unfortunately can only be addressed in SQL through a "human-unreadable" 
-- relationship table (named in this schema "comp_to_param"). To make sense
-- out of this relationship table, use the full power of SQL to make a JOIN
-- the way you prefer.
-- The hash for the table is used for quicker saving of the table.
-- Currently the NN requirement here is turned off to allow comparison.
CREATE TABLE hlt_parameter (
      hpa_id                         NUMBER(10),
      hpa_hash                      NUMBER(10),	
      hpa_name                   VARCHAR2(50),
      hpa_op                     VARCHAR2(30),
      hpa_value                  VARCHAR2(4000),
      hpa_chain_user_version         CHAR,
      CONSTRAINT                 hpa_pk                PRIMARY KEY (hpa_id),
    CONSTRAINT            hpa_id_NN         CHECK ( hpa_id IS NOT NULL),
    CONSTRAINT            hpa_hash_NN         CHECK ( hpa_hash IS NOT NULL),
    CONSTRAINT            hpa_name_NN         CHECK ( hpa_name IS NOT NULL),
    CONSTRAINT            hpa_op_NN         CHECK ( hpa_op IS NOT NULL),
    CONSTRAINT            hpa_value_NN         CHECK ( hpa_value IS NOT NULL),
    CONSTRAINT            hpa_chain_user_v_NN     CHECK ( hpa_chain_user_version IS NOT NULL)
);

CREATE INDEX hpa_parameter_hash_ind ON hlt_parameter(hpa_hash) COMPRESS 1;

-- This table contains all components that can run for all possible
-- setups. It defines the components name, also known as Alias in the
-- Gaudi world, that can be used for a given setup. The combination of
-- those is *not* free, you have to be a trigger expert to define what can run
-- in the same setup. This table is linked to a "source" table that
-- defines the originating component name and the library (DLL) it belongs to.
-- The topalg column defines if the current component should be in the top
-- algorithm lis tor not. The order of their execution is independent.
-- The hash for the table is used for quicker saving of the table.
-- Currently the NN requirement here is turned off to allow comparison.

CREATE TABLE hlt_component (
    hcp_id                   NUMBER(10),
    hcp_hash                 NUMBER(10),
    hcp_hashfull             NUMBER(10),
    hcp_name                 VARCHAR2(200),
    hcp_version              NUMBER(11),
    hcp_alias                VARCHAR2(200),
    hcp_type                 VARCHAR2(200),
    hcp_py_name              VARCHAR2(200),
    hcp_py_package           VARCHAR2(200),
    CONSTRAINT            hcp_pk            PRIMARY KEY (hcp_id),
    CONSTRAINT            hcp_nmver         UNIQUE (hcp_name, hcp_alias, hcp_version),
    CONSTRAINT            hcp_id_NN         CHECK ( hcp_id IS NOT NULL),
    CONSTRAINT            hcp_hash_NN         CHECK ( hcp_hash IS NOT NULL),
    CONSTRAINT            hcp_hashfull_NN         CHECK ( hcp_hashfull IS NOT NULL),
    CONSTRAINT            hcp_name_NN         CHECK ( hcp_name IS NOT NULL),
    CONSTRAINT            hcp_version_NN         CHECK ( hcp_version IS NOT NULL),
    CONSTRAINT            hcp_alias_NN         CHECK ( hcp_alias IS NOT NULL),
    CONSTRAINT            hcp_type_NN         CHECK ( hcp_type IS NOT NULL),
    CONSTRAINT            hcp_py_name_NN         CHECK ( hcp_py_name IS NOT NULL),
    CONSTRAINT            hcp_py_package_NN     CHECK ( hcp_py_package IS NOT NULL)
);
CREATE INDEX hcp_component_hash_ind ON hlt_component(hcp_hash) COMPRESS 1;
CREATE INDEX hcp_component_hashfull_ind ON hlt_component(hcp_hashfull) COMPRESS 1;

-- This table defines the relation between one component and another
-- Used to assign private tools to algorithms and more generally
-- any child component (tool) to a parent component.
-- The tools can only be reached via the cp-cp link, and the "parent" cp if an algo 
-- will be linked to a TE via the TE-CP link. 
CREATE TABLE hlt_cp_to_cp (
     hcp2cp_id            NUMBER(10),
     hcp2cp_parent_comp_id        NUMBER(10),
     hcp2cp_child_comp_id        NUMBER(10),
    CONSTRAINT              hcp2cp_pk        PRIMARY KEY (hcp2cp_id),
     CONSTRAINT              hcp2cp_fk_acp        FOREIGN KEY (hcp2cp_parent_comp_id)
                                   REFERENCES hlt_component (hcp_id),
     CONSTRAINT              hcp2cp_fk_tcp        FOREIGN KEY (hcp2cp_child_comp_id)
                                   REFERENCES hlt_component (hcp_id),
    CONSTRAINT            hcp2cp_id_NN         CHECK ( hcp2cp_id IS NOT NULL),
    CONSTRAINT            hcp2cp_parent_id_NN     CHECK ( hcp2cp_parent_comp_id IS NOT NULL),
    CONSTRAINT            hcp2cp_child_id_NN     CHECK ( hcp2cp_child_comp_id IS NOT NULL)
);
CREATE INDEX hcp2cp_parent_comp_id_ind ON hlt_cp_to_cp(hcp2cp_parent_comp_id) COMPRESS 1;
CREATE INDEX hcp2cp_child_comp_id_ind  ON hlt_cp_to_cp(hcp2cp_child_comp_id) COMPRESS 1;

-- This table defines the relation between the component (comp) and the
-- parameters (param) that it has to set. It will define what you see as
-- jobOptions in the form of ComponentName.ProperValue = MyValue. It is
-- a relationship table thefore it defines only foreign keys to the components
-- and parameters table
CREATE TABLE hlt_cp_to_pa (
     hcp2pa_id            NUMBER(10),
     hcp2pa_component_id        NUMBER(10),
     hcp2pa_parameter_id        NUMBER(10),
    CONSTRAINT              hcp2pa_pk        PRIMARY KEY (hcp2pa_id),
     CONSTRAINT              hcp2pa_fk_cp        FOREIGN KEY (hcp2pa_component_id)
                                   REFERENCES hlt_component (hcp_id),
     CONSTRAINT              hcp2pa_fk_pa        FOREIGN KEY (hcp2pa_parameter_id)
                                   REFERENCES hlt_parameter (hpa_id),
    CONSTRAINT            hcp2pa_id_NN         CHECK ( hcp2pa_id IS NOT NULL),
    CONSTRAINT            hcp2pa_component_id_NN     CHECK ( hcp2pa_component_id IS NOT NULL),
    CONSTRAINT            hcp2pa_parameter_id_NN     CHECK ( hcp2pa_parameter_id IS NOT NULL)
);
CREATE INDEX hcp2pa_component_id_ind ON hlt_cp_to_pa(hcp2pa_component_id) COMPRESS 1;
CREATE INDEX hcp2pa_parameter_id_ind ON hlt_cp_to_pa(hcp2pa_parameter_id) COMPRESS 1;

-- This table defines all possible running setups one can enjoy. Since
-- a particular component can belong to a number of setups,
-- and a setup may contain many components, we need another N:N
-- relationship table. Please note that the hlt_setup table
-- is referenced by the hlt_algorithm table, ie. for each algorithm
-- in the menu we define a setup with several componennts that need to be
-- setup. Hence we can derive the components that need to run form the
-- menu. We will start with a single setup, the base setup. This means
-- the setup for all algorithms is the same. Later on , this may change
-- and we have several setups.
CREATE TABLE hlt_setup (
    hst_id                NUMBER(10),
    hst_hash              NUMBER(10),
    hst_name              VARCHAR2(50),
    hst_version		  NUMBER(11),
    CONSTRAINT            hst_pk            PRIMARY KEY (hst_id),
    CONSTRAINT            hst_nmver         UNIQUE (hst_name, hst_version),
    CONSTRAINT            hst_id_NN         CHECK ( hst_id IS NOT NULL),
--    CONSTRAINT            hst_hash_NN         CHECK ( hst_hash IS NOT NULL),
    CONSTRAINT            hst_name_NN         CHECK ( hst_name IS NOT NULL),
    CONSTRAINT            hst_version_NN         CHECK ( hst_version IS NOT NULL)
);

CREATE INDEX hst_setup_hash_ind ON hlt_setup(hst_hash) COMPRESS 1;

-- This table defines the N:N relations between setups and components.
CREATE TABLE hlt_st_to_cp (
    hst2cp_id            NUMBER(10),
    hst2cp_setup_id         NUMBER(10),
    hst2cp_component_id        NUMBER(10),
    CONSTRAINT            hst2cp_pk        PRIMARY KEY (hst2cp_id),
     CONSTRAINT            hst2cp_fk_st        FOREIGN KEY (hst2cp_setup_id) 
                                REFERENCES hlt_setup(hst_id),
    CONSTRAINT            hst2cp_fk_cp        FOREIGN KEY (hst2cp_component_id) 
                                REFERENCES hlt_component(hcp_id),
    CONSTRAINT            hst2cp_id_NN         CHECK ( hst2cp_id IS NOT NULL),
    CONSTRAINT            hst2cp_setup_id_NN     CHECK ( hst2cp_setup_id IS NOT NULL),
    CONSTRAINT            hst2cp_component_id_NN     CHECK ( hst2cp_component_id IS NOT NULL)
);
CREATE INDEX hst2cp_setup_id_ind     ON hlt_st_to_cp(hst2cp_setup_id) COMPRESS 1;
CREATE INDEX hst2cp_component_id_ind ON hlt_st_to_cp(hst2cp_component_id) COMPRESS 1;

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- TABLES REPRESENTING THE PHYSICAL SOFTWARE SETUP FOR A JOB
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- This table stores information about all available software releases.
CREATE TABLE hlt_release (
    hre_id                NUMBER(10),
    hre_hash		  NUMBER(10),
    hre_name             VARCHAR2(200),
    hre_base             VARCHAR2(200),
    hre_patch_1             VARCHAR2(200),
    hre_patch_2             VARCHAR2(200),
    CONSTRAINT            hre_pk            PRIMARY KEY (hre_id),
    CONSTRAINT            hre_id_NN         CHECK ( hre_id IS NOT NULL),
--    CONSTRAINT            hre_hash_NN         CHECK ( hre_hash IS NOT NULL),
    CONSTRAINT            hre_base_NN         CHECK ( hre_base IS NOT NULL),
    CONSTRAINT            hre_patch_1_NN         CHECK ( hre_patch_1 IS NOT NULL),
    CONSTRAINT            hre_patch_2_NN         CHECK ( hre_patch_2 IS NOT NULL)
);

CREATE INDEX hre_release_hash_ind ON hlt_release(hre_hash) COMPRESS 1;

-------------------------------------------------
-- SQL script for the HLT menu part in the TriggerDB
-- date  : 24.10.05
-- author: Johannes Haller (CERN)
-------------------------------------------------

----------------------------
-- the data tables
----------------------------

-- This table describes the HLT trigger menus
-- In a given menu, the algorithms are specified that need to be configured in order to process the event
-- and to derive the trigger decision. These algorihtms need certain other algorithms or services to 
-- be running (and configured) in order to guarantee a correct event processing. For e.g. byte-stream converter
-- services. For this reason, for each menu a certain infrastructure is needed. This infrastructure is given in
-- the setup table. htm_setup_id is a foreign key to this table. When inserting a new menu it must be 
-- guaranteed that menu and setup are consistent. For the time being we will only use one setup (the base setup).
-- The relationship between menu and setup could either be stored in the hlt_menu table (ergo: here) or
-- in a higher master_key table. But consistency is important. Later we could use several setups: e.g. one
-- for calorimeter algorithms, one for the muons algorihtms, a combined setup, and so on.  
CREATE TABLE hlt_trigger_menu ( 
    htm_id                 NUMBER(10),
    htm_hash		   NUMBER(10),
    htm_name             VARCHAR2(50),
    htm_version             NUMBER(11),
    htm_phase             VARCHAR2(50),
    htm_consistent            CHAR,
    CONSTRAINT             htm_pk            PRIMARY KEY (htm_id),
    CONSTRAINT             htm_nmver        UNIQUE (htm_name, htm_version),
    CONSTRAINT            htm_id_NN         CHECK ( htm_id IS NOT NULL),
--    CONSTRAINT            htm_hash_NN         CHECK ( htm_hash IS NOT NULL),
    CONSTRAINT            htm_name_NN         CHECK ( htm_name IS NOT NULL),
    CONSTRAINT            htm_version_NN         CHECK ( htm_version IS NOT NULL),
    CONSTRAINT            htm_phase_NN         CHECK ( htm_phase IS NOT NULL),
    CONSTRAINT            htm_consistent_NN     CHECK ( htm_consistent IS NOT NULL)
);

CREATE INDEX htm_trigger_menu_hash_ind ON hlt_trigger_menu(htm_hash) COMPRESS 1;

-- This table describes hlt_chains which are a trigger in L2 or EF, i.e. a chain of 
-- signatures. For each processing step, there is one signature.
-- The attirbute htcc_lower_chain_name gives the name of the signatures this chain is based on.
-- In case of a L2 chain, this column gives the LVL1 trigger item. For EF chains
-- this string is the name of a L2 chain. When combining the LVL1 menu and the HLT part,
-- it must be made sure that the lower signatures that are expected by L2 are also configured
-- in the LVL1 part. The same for EF and L2. We dont put foreign keys here to the actual signature
-- but rahter strings of the name. This allows to change the LVL1 (LVL2) trigger menu items without
-- necessarily changing the HLT trigger menu.
CREATE TABLE hlt_trigger_chain (
    htc_id                         NUMBER(10),
    htc_hash			   NUMBER(10),
    htc_name                       VARCHAR2(4000),
    htc_version                    NUMBER(11),
    htc_user_version               NUMBER(11),
    htc_comment                    VARCHAR2(4000),
    htc_chain_counter              NUMBER(10),
    htc_lower_chain_name           VARCHAR2(4000),
    htc_eb_point		   NUMBER(2),
    CONSTRAINT            htc_pk                  PRIMARY KEY (htc_id),
    CONSTRAINT            htc_nmver               UNIQUE (htc_name, htc_version),
    CONSTRAINT            htc_id_NN               CHECK ( htc_id IS NOT NULL),
    CONSTRAINT            htc_name_NN             CHECK ( htc_name IS NOT NULL),
    CONSTRAINT            htc_version_NN          CHECK ( htc_version IS NOT NULL),
    CONSTRAINT            htc_user_version_NN     CHECK ( htc_user_version IS NOT NULL),
    CONSTRAINT            htc_chain_counter_NN    CHECK ( htc_chain_counter IS NOT NULL),
    CONSTRAINT            htc_lower_chain_name_NN CHECK ( htc_lower_chain_name IS NOT NULL)
    --CONSTRAINT 		  htc_eb_point_NN	  CHECK ( htc_eb_point IS NOT NULL)
);

CREATE INDEX htc_trigger_chain_hash_ind ON hlt_trigger_chain(htc_hash) COMPRESS 1;

-- this table describes the signatures at each step of a chain.
-- The attribute hs_logic identifies the logic expression with which trigger_elements are combined
-- in this step. At the moment we use an integer to encode this logic. e.g. hs_logic=1 means
-- that all trigger_element are used in an AND expression. We expect that the number
-- of different logical experssions will be rather small. Then this encoded logic makes sense.
-- LAter one could change it to a string and parser code to encode the logic. The implementation of a
-- proper logical tree with AND and OR nodes seems to be an over-kill.
CREATE TABLE hlt_trigger_signature ( 
    hts_id                 NUMBER(10),
    hts_hash                      NUMBER(10),
    hts_logic            NUMBER(2),
    CONSTRAINT             hts_pk            PRIMARY KEY (hts_id),
    CONSTRAINT            hts_id_NN         CHECK ( hts_id IS NOT NULL),
    CONSTRAINT            hts_logic_NN         CHECK ( hts_logic IS NOT NULL)
--    CONSTRAINT            hts_hash_NN         CHECK ( hts_hash IS NOT NULL),
);

CREATE INDEX hts_trigger_signature_hash_ind ON hlt_trigger_signature(hts_hash) COMPRESS 1;

-- Steps are logical combination of trigger_elements. These trigger_elements are defined in 
-- this table. 
CREATE TABLE hlt_trigger_element ( 
    hte_id                    NUMBER(10),
    hte_hash		      NUMBER(10),
    hte_name                  VARCHAR2(100),
    hte_topo_start_from	      VARCHAR2(100) default "~",
    hte_version               NUMBER(11),
    CONSTRAINT             hte_pk            PRIMARY KEY (hte_id),
    CONSTRAINT            hte_id_NN         CHECK ( hte_id IS NOT NULL),
    CONSTRAINT            hte_name_NN         CHECK ( hte_name IS NOT NULL),
    CONSTRAINT            hte_version_NN         CHECK ( hte_version IS NOT NULL),
    CONSTRAINT            hte_topo_start_from_NN         CHECK ( hte_topo_start_from IS NOT NULL),
    CONSTRAINT            hte_hash_NN         CHECK ( hte_hash IS NOT NULL)
 );

CREATE INDEX hts_trigger_element_hash_ind ON hlt_trigger_element(hte_hash) COMPRESS 1;

-- Prescale sets are supposed to be changed more often than the menu logic. 
-- Therefore this separate table is introduced. It binds all 
-- prescale values. There will be several Prescale Sets for a certain menu. 
CREATE TABLE hlt_prescale_set ( 
    hps_id                        NUMBER(10),
    hps_name                      VARCHAR2(65),
    hps_version                   NUMBER(11),
    hps_comment                   VARCHAR2(200),
    hps_username                  VARCHAR2(50),
    hps_modified_time             TIMESTAMP, 
    hps_used                      CHAR default 0, 
    hps_type                      VARCHAR2(200),     
    CONSTRAINT            hps_pk                PRIMARY KEY (hps_id),
    CONSTRAINT            hps_nmver             UNIQUE (hps_name, hps_version),
    CONSTRAINT            hps_id_NN         CHECK ( hps_id IS NOT NULL),
    CONSTRAINT            hps_name_NN         CHECK ( hps_name IS NOT NULL),
    CONSTRAINT            hps_version_NN         CHECK ( hps_version IS NOT NULL),
    CONSTRAINT            hps_used_NN         CHECK ( hps_used IS NOT NULL),
    CONSTRAINT            hps_type_NN         CHECK ( hps_type IS NOT NULL)
);

-- Here the various prescale factors are listed for a certain set.
-- The hash for the table is used for quicker saving of the table.
-- Currently the NN requirement here is turned off to allow comparison.
CREATE TABLE hlt_prescale (
      hpr_id                        NUMBER(10),
      hpr_hash                      NUMBER(10),
      hpr_prescale_set_id           NUMBER(10),
      hpr_chain_counter             NUMBER(10),
      hpr_value                     NUMBER(10,2),
      hpr_type                      VARCHAR2(50),
      hpr_condition                 VARCHAR2(200),
      CONSTRAINT     hpr_pk                     PRIMARY KEY (hpr_id),
      CONSTRAINT     hpr_fk_ps                  FOREIGN KEY (hpr_prescale_set_id) REFERENCES hlt_prescale_set(hps_id),
      CONSTRAINT     hpr_id_NN                  CHECK ( hpr_id IS NOT NULL),
      CONSTRAINT     hpr_prescale_set_id_NN     CHECK ( hpr_prescale_set_id IS NOT NULL),
      CONSTRAINT     hpr_chain_counter_NN       CHECK ( hpr_chain_counter IS NOT NULL),
      CONSTRAINT     hpr_value_NN               CHECK ( hpr_value IS NOT NULL),
      CONSTRAINT     hpr_type_NN                CHECK ( hpr_type IS NOT NULL)
);

CREATE INDEX hpr_prescale_set_id_ind ON hlt_prescale(hpr_prescale_set_id) COMPRESS 1;
CREATE INDEX hpr_prescale_hash_ind ON hlt_prescale(hpr_hash) COMPRESS 1;

----------------------
-- N-N relations
----------------------

-- Relates the HLT prescale sets to the HLT menu. Hence if the menu changes
-- the same set may still be applied
CREATE TABLE hlt_tm_to_ps ( 
    htm2ps_id             NUMBER(10),
    htm2ps_trigger_menu_id         NUMBER(10),
    htm2ps_prescale_set_id      NUMBER(10),
    htm2ps_used            CHAR            default 0,
    CONSTRAINT            htm2ps_pk        PRIMARY KEY (htm2ps_id),
    CONSTRAINT            htm2ps_fk_tm        FOREIGN KEY (htm2ps_trigger_menu_id) 
                                REFERENCES hlt_trigger_menu(htm_id),
    CONSTRAINT            htm2ps_fk_ps        FOREIGN KEY (htm2ps_prescale_set_id)
                                REFERENCES hlt_prescale_set(hps_id),
    CONSTRAINT            htm2ps_id_NN         CHECK ( htm2ps_id IS NOT NULL),
    CONSTRAINT            htm2ps_menu_id_NN     CHECK ( htm2ps_trigger_menu_id IS NOT NULL),
    CONSTRAINT            htm2ps_set_id_NN     CHECK ( htm2ps_prescale_set_id IS NOT NULL),
    CONSTRAINT            htm2ps_used_NN         CHECK ( htm2ps_used IS NOT NULL)
);
CREATE INDEX htm2ps_trigger_menu_id_ind ON hlt_tm_to_ps(htm2ps_trigger_menu_id) COMPRESS 1;
CREATE INDEX htm2ps_prescale_set_id_ind ON hlt_tm_to_ps(htm2ps_prescale_set_id) COMPRESS 1;

-- In this table it is specified which chains are included in a certain menu.
-- For each menu the chains are counted and they are attributed a certain counter value in this menu.
-- This id can then be used e.g. for trigger_bits in a trigger_bit_pattern and also for
-- the prescale_set table in which the prescale for the various chains are stored. Please note
-- that these prescales are supposed to be changed more frequently than full menus. Thats why a 
-- separate table is introduced.
CREATE TABLE hlt_tm_to_tc ( 
    htm2tc_id             NUMBER(10),
    htm2tc_trigger_menu_id         NUMBER(10),
    htm2tc_trigger_chain_id     NUMBER(10),
    CONSTRAINT            htm2tc_pk        PRIMARY KEY (htm2tc_id),
    CONSTRAINT            htm2tc_fk_tm        FOREIGN KEY (htm2tc_trigger_menu_id) 
                                REFERENCES hlt_trigger_menu(htm_id),
    CONSTRAINT            htm2tc_fk_tc        FOREIGN KEY (htm2tc_trigger_chain_id)
                                REFERENCES hlt_trigger_chain(htc_id),
    CONSTRAINT            htm2tc_id_NN         CHECK ( htm2tc_id IS NOT NULL),
    CONSTRAINT            htm2tc_trig_menu_id_NN     CHECK ( htm2tc_trigger_menu_id IS NOT NULL),
    CONSTRAINT            htm2tc_trig_chain_id_NN CHECK ( htm2tc_trigger_chain_id IS NOT NULL)
);
CREATE INDEX htm2tc_trigger_menu_id_ind  ON hlt_tm_to_tc(htm2tc_trigger_menu_id) COMPRESS 1;
CREATE INDEX htm2tc_trigger_chain_id_ind ON hlt_tm_to_tc(htm2tc_trigger_chain_id) COMPRESS 1;

-- one chain is composed of several steps/signatures.
-- one step/signature can appear in several trigger chains.
-- The attribute htc2ts_step_counter indicates the order of steps in a chain. For a chain with 4 steps
-- there should be 4 steps with counter= 1,2,3,4
CREATE TABLE hlt_tc_to_ts ( 
    htc2ts_id             NUMBER(10),
    htc2ts_trigger_chain_id     NUMBER(10),
    htc2ts_trigger_signature_id     NUMBER(10),
    htc2ts_signature_counter     NUMBER(10),
    CONSTRAINT            htc2ts_pk        PRIMARY KEY (htc2ts_id),
    CONSTRAINT            htc2ts_fk_tc        FOREIGN KEY (htc2ts_trigger_chain_id) 
                                 REFERENCES hlt_trigger_chain(htc_id),
    CONSTRAINT            htc2ts_fk_ts        FOREIGN KEY (htc2ts_trigger_signature_id)
                                REFERENCES hlt_trigger_signature(hts_id),
    CONSTRAINT            htc2ts_id_NN         CHECK ( htc2ts_id IS NOT NULL),
    CONSTRAINT            htc2ts_trig_chain_id_NN CHECK ( htc2ts_trigger_chain_id IS NOT NULL),
    CONSTRAINT            htc2ts_trig_sig_id_NN     CHECK ( htc2ts_trigger_signature_id IS NOT NULL),
    CONSTRAINT            htc2ts_sig_counter_NN     CHECK ( htc2ts_signature_counter IS NOT NULL)
);
CREATE INDEX htc2ts_trigr_chain_id_ind ON hlt_tc_to_ts(htc2ts_trigger_chain_id) COMPRESS 1;
CREATE INDEX htc2ts_trigr_sig_id_ind   ON hlt_tc_to_ts(htc2ts_trigger_signature_id) COMPRESS 1;

-- this table constains the group info
--   for monitoring purposes trigger chains 
--   can be grouped together, one can think
--   of electron, muon, tau groups, etc.
CREATE TABLE hlt_trigger_group (
      htg_id                         NUMBER(10),
      htg_trigger_chain_id           NUMBER(10),
      htg_name                       VARCHAR2(50),
      CONSTRAINT             htg_pk              PRIMARY KEY (htg_id),
      CONSTRAINT             htg_fk_tc           FOREIGN KEY (htg_trigger_chain_id)  
                                REFERENCES hlt_trigger_chain(htc_id),
    CONSTRAINT            htg_id_NN         CHECK ( htg_id IS NOT NULL),
    CONSTRAINT            htg_trigger_chain_id_NN CHECK ( htg_trigger_chain_id IS NOT NULL),
    CONSTRAINT            htg_name_NN         CHECK ( htg_name IS NOT NULL)
);
CREATE INDEX htg_trigger_chain_id_ind ON hlt_trigger_group(htg_trigger_chain_id) COMPRESS 1;

-- this table constains the trigger type info
CREATE TABLE hlt_trigger_type (
      htt_id                         NUMBER(10),
      htt_trigger_chain_id           NUMBER(10),
      htt_typebit                    NUMBER(10),
      CONSTRAINT             htt_pk              PRIMARY KEY (htt_id),
      CONSTRAINT             htt_fk_tc           FOREIGN KEY (htt_trigger_chain_id)      
                                REFERENCES hlt_trigger_chain(htc_id),
    CONSTRAINT            htt_id_NN         CHECK ( htt_id IS NOT NULL),
    CONSTRAINT            htt_trigger_chain_id_NN CHECK ( htt_trigger_chain_id IS NOT NULL)
);
CREATE INDEX htt_trigger_chain_id_ind ON hlt_trigger_type(htt_trigger_chain_id) COMPRESS 1;

-- this table constains the stream info
CREATE TABLE hlt_trigger_stream (
      htr_id                         NUMBER(10),
      htr_hash			     NUMBER(10),
      htr_name                       VARCHAR2(50),
      htr_description                VARCHAR2(200),
      htr_type                       VARCHAR2(50),
      htr_obeyLB                     NUMBER(1),
      CONSTRAINT             htr_pk              PRIMARY KEY (htr_id),
    CONSTRAINT            htr_id_NN         CHECK ( htr_id IS NOT NULL),
--    CONSTRAINT            htr_hash_NN         CHECK ( htr_hash IS NOT NULL),
    CONSTRAINT            htr_name_NN         CHECK ( htr_name IS NOT NULL),
    CONSTRAINT            htr_description_NN     CHECK ( htr_description IS NOT NULL),
    CONSTRAINT            htr_type_NN         CHECK ( htr_type IS NOT NULL),
    CONSTRAINT            htr_obeyLB_NN         CHECK ( htr_obeyLB IS NOT NULL)
);

CREATE INDEX htr_trigger_stream_hash_ind ON hlt_trigger_stream(htr_hash) COMPRESS 1;
-- this table connects the trigger chains with the
-- data stream definitions
-- an M:N connection is needed, since a chain can
-- feet multiple data streams, while a data stream
-- can be fed by multiple chains

CREATE TABLE hlt_tc_to_tr (
      htc2tr_id                           NUMBER(10),
      htc2tr_trigger_chain_id             NUMBER(10),
      htc2tr_trigger_stream_id            NUMBER(10),
      htc2tr_trigger_stream_prescale      VARCHAR2(50),
      CONSTRAINT  htc2tr_pk               PRIMARY KEY (htc2tr_id),
      CONSTRAINT  htc2tr_fk_tc            FOREIGN KEY (htc2tr_trigger_chain_id)  REFERENCES hlt_trigger_chain(htc_id),
      CONSTRAINT  htc2tr_fk_tm            FOREIGN KEY (htc2tr_trigger_stream_id) REFERENCES hlt_trigger_stream(htr_id),
      CONSTRAINT  htc2tr_id_NN            CHECK ( htc2tr_id IS NOT NULL),
      CONSTRAINT  htc2tr_trig_chain_NN    CHECK ( htc2tr_trigger_chain_id IS NOT NULL),
      CONSTRAINT  htc2tr_trig_str_NN      CHECK ( htc2tr_trigger_stream_id IS NOT NULL),
      CONSTRAINT  htc2tr_trig_str_ps_NN   CHECK ( htc2tr_trigger_stream_prescale IS NOT NULL)
);
CREATE INDEX htc2tr_trigger_chain_id_ind  ON hlt_tc_to_tr(htc2tr_trigger_chain_id) COMPRESS 1;
CREATE INDEX htc2tr_trigger_stream_id_ind ON hlt_tc_to_tr(htc2tr_trigger_stream_id) COMPRESS 1;

-- one signature has several trigger_elements.
CREATE TABLE hlt_ts_to_te ( 
      hts2te_id                         NUMBER(10),
      hts2te_trigger_signature_id       NUMBER(10),
      hts2te_trigger_element_id         NUMBER(10),
      hts2te_element_counter            NUMBER(10),
      CONSTRAINT              hts2te_pk             PRIMARY KEY (hts2te_id),
      CONSTRAINT              hts2te_fk_ts          FOREIGN KEY (hts2te_trigger_signature_id) 
                                                  REFERENCES hlt_trigger_signature(hts_id),
      CONSTRAINT              hts2te_fk_te         FOREIGN KEY (hts2te_trigger_element_id)
                                                  REFERENCES hlt_trigger_element(hte_id),
    CONSTRAINT            hts2te_id_NN         CHECK ( hts2te_id IS NOT NULL),
    CONSTRAINT            hts2te_trig_sig_id_NN     CHECK ( hts2te_trigger_signature_id IS NOT NULL),
    CONSTRAINT            hts2te_trig_ele_id_NN     CHECK ( hts2te_trigger_element_id IS NOT NULL),
    CONSTRAINT            hts2te_ele_counter_NN     CHECK ( hts2te_element_counter IS NOT NULL)
);
CREATE INDEX hts2te_trigr_sig__id_ind ON hlt_ts_to_te(hts2te_trigger_signature_id) COMPRESS 1;
CREATE INDEX hts2te_trigr_ele_id_ind  ON hlt_ts_to_te(hts2te_trigger_element_id) COMPRESS 1;

-- This table describes the N-N relation between trigger_elements and algorithm component.
CREATE TABLE hlt_te_to_cp (
      hte2cp_id                     NUMBER(10),
      hte2cp_trigger_element_id    NUMBER(10),
      hte2cp_component_id           NUMBER(10),
      hte2cp_algorithm_counter      NUMBER(10),
      CONSTRAINT              hte2cp_pk             PRIMARY KEY (hte2cp_id),
      CONSTRAINT              hte2cp_fk_te          FOREIGN KEY (hte2cp_trigger_element_id) 
                                                REFERENCES hlt_trigger_element(hte_id),
      CONSTRAINT              hte2cp_fk_cp          FOREIGN KEY (hte2cp_component_id)
                                                REFERENCES hlt_component(hcp_id),
    CONSTRAINT            hte2cp_id_NN         CHECK ( hte2cp_id IS NOT NULL),
    CONSTRAINT            hte2cp_trig_ele_id_NN     CHECK ( hte2cp_trigger_element_id IS NOT NULL),
    CONSTRAINT            hte2cp_comp_id_NN     CHECK ( hte2cp_component_id IS NOT NULL),
    CONSTRAINT            hte2cp_algo_counter_NN     CHECK ( hte2cp_algorithm_counter IS NOT NULL)
);
CREATE INDEX hte2cp_trigger_element_id_ind ON hlt_te_to_cp(hte2cp_trigger_element_id) COMPRESS 1;
CREATE INDEX hte2cp_component_id_ind       ON hlt_te_to_cp(hte2cp_component_id) COMPRESS 1;

-- This table allows to specify several input trigger elements for an output trigger
-- element produced by a certain algorithm. hte2te_trigger_element_id gives the
-- trigger_element to be produced and hte2te_trigger_element_inp_id gives the input
-- trigger elements. The property of an algorithm to produce multiple trigger_elements
-- can be indicated in the DB by multiple links from one hlt_trigger_signature to the 
-- hlt_trigger_element table. In order to distinguish multiple separate trigger_elements
-- produced in multiple runnings of a algorithm from multiple trigger elements produced
-- by an algorithm in just one running, the hcp_flag attribute in the hlt_component table 
-- must be used (This is a property of the algorithm!).

CREATE TABLE hlt_te_to_te (
      hte2te_id                      NUMBER(10),
      hte2te_te_id                   NUMBER(10),
      hte2te_te_inp_id               VARCHAR2(100),
      hte2te_te_inp_type             VARCHAR2(10),
      hte2te_te_counter              NUMBER(3),
    CONSTRAINT             hte2te_pk           PRIMARY KEY (hte2te_id),
      CONSTRAINT             hte2cte_fk_te       FOREIGN KEY (hte2te_te_id) 
                                                   REFERENCES hlt_trigger_element(hte_id),
    CONSTRAINT            hte2te_id_NN         CHECK ( hte2te_id IS NOT NULL),
    CONSTRAINT            hte2te_te_id_NN     CHECK ( hte2te_te_id IS NOT NULL),
    CONSTRAINT            hte2te_te_inp_id_NN     CHECK ( hte2te_te_inp_id IS NOT NULL),
    CONSTRAINT            hte2te_te_inp_type_NN     CHECK ( hte2te_te_inp_type IS NOT NULL),
    CONSTRAINT            hte2te_te_counter_NN     CHECK ( hte2te_te_counter IS NOT NULL)
 );
CREATE INDEX hte2te_te_id_ind ON hlt_te_to_te(hte2te_te_id) COMPRESS 1;

-- Tables presenting the rules to convert the online setup into an offline
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- HLT RULES
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- this table contains the rule set info
CREATE TABLE HLT_RULE_SET (
      hrs_id                         NUMBER(10),
      hrs_name                       VARCHAR2(50),
      hrs_version                    NUMBER(11),
      CONSTRAINT             hrs_pk              PRIMARY KEY (hrs_id),
      CONSTRAINT             hrs_nmver           UNIQUE (hrs_name, hrs_version),
    CONSTRAINT            hrs_id_NN         CHECK ( hrs_id IS NOT NULL),
    CONSTRAINT            hrs_name_NN         CHECK ( hrs_name IS NOT NULL),
    CONSTRAINT            hrs_version_NN         CHECK ( hrs_version IS NOT NULL)
);


-- This table contains the rule info
-- hru_type:
--     1 - Replace
--     2 - Rename
--     3 - Modify
--     4 - Merge
--     5 - Sort
--     6 - Copy

CREATE TABLE HLT_RULE (
      hru_id                         NUMBER(10),
      hru_name                       VARCHAR2(50),
      hru_version                    NUMBER(11),
      hru_type                       NUMBER(1),
      CONSTRAINT            hru_pk              PRIMARY KEY (hru_id),
      CONSTRAINT             hru_nmvertp         UNIQUE (hru_name, hru_version, hru_type),
    CONSTRAINT            hru_id_NN         CHECK ( hru_id IS NOT NULL),
    CONSTRAINT            hru_name_NN         CHECK ( hru_name IS NOT NULL),
    CONSTRAINT            hru_version_NN         CHECK ( hru_version IS NOT NULL),
    CONSTRAINT            hru_type_NN         CHECK ( hru_type IS NOT NULL)
);

-- this table contains the rule component
-- the structure of the table is identical to the hlt_component
CREATE TABLE HLT_RULE_COMPONENT (
    hrc_id                        NUMBER(10),
    hrc_name                      VARCHAR2(200),
    hrc_version                   NUMBER(11),
    hrc_alias                     VARCHAR2(200),
    hrc_type                      VARCHAR2(50),
    hrc_py_name                   VARCHAR2(200),
    hrc_py_package                VARCHAR2(200),
    CONSTRAINT            hrc_pk              PRIMARY KEY (hrc_id),
    CONSTRAINT            hrc_nmver           UNIQUE (hrc_name, hrc_alias, hrc_version),
    CONSTRAINT            hrc_id_NN           CHECK ( hrc_id IS NOT NULL),
    CONSTRAINT            hrc_name_NN         CHECK ( hrc_name IS NOT NULL),
    CONSTRAINT            hrc_version_NN      CHECK ( hrc_version IS NOT NULL),
    CONSTRAINT            hrc_alias_NN        CHECK ( hrc_alias IS NOT NULL),
    CONSTRAINT            hrc_type_NN         CHECK ( hrc_type IS NOT NULL),
    CONSTRAINT            hrc_py_name_NN      CHECK ( hrc_py_name IS NOT NULL),
    CONSTRAINT            hrc_py_package_NN   CHECK ( hrc_py_package IS NOT NULL)
);

-- This table defines rule parameters
-- its structure is identical to the hlt_parameter
CREATE TABLE HLT_RULE_PARAMETER (
    hrp_id                      NUMBER(10),
    hrp_name                    VARCHAR2(50),
    hrp_op                      VARCHAR2(30),
    hrp_value                   VARCHAR2(4000),
    CONSTRAINT                  hrp_pk              PRIMARY KEY (hrp_id),
    CONSTRAINT            hrp_id_NN         CHECK ( hrp_id IS NOT NULL),
    CONSTRAINT            hrp_name_NN         CHECK ( hrp_name IS NOT NULL),
    CONSTRAINT            hrp_op_NN         CHECK ( hrp_op IS NOT NULL),
    CONSTRAINT            hrp_value_NN         CHECK ( hrp_value IS NOT NULL)
);

-- ________________________________________________________________________
-- HLT RULES - N:M tables
-- ________________________________________________________________________

-- This table describes N:M relation between HLT_RELEASE and HLT_RULE_SET
CREATE TABLE HLT_HRE_TO_HRS (
      hre2rs_id                          NUMBER(10),
      hre2rs_release_id                  NUMBER(10),
      hre2rs_rule_set_id                 NUMBER(10),
      CONSTRAINT             hre2rs_pk               PRIMARY KEY (hre2rs_id),
      CONSTRAINT             hre2rs_fk_ru            FOREIGN KEY (hre2rs_release_id)   
                                REFERENCES HLT_RELEASE(hre_id),
      CONSTRAINT             hre2rs_fk_rs            FOREIGN KEY (hre2rs_rule_set_id)  
                                REFERENCES HLT_RULE_SET(hrs_id),
    CONSTRAINT            hre2rs_id_NN         CHECK ( hre2rs_id IS NOT NULL)
);
CREATE INDEX hre2rs_release_id_ind  ON HLT_HRE_TO_HRS(hre2rs_release_id) COMPRESS 1;
CREATE INDEX hre2rs_rule_set_id_ind ON HLT_HRE_TO_HRS(hre2rs_rule_set_id) COMPRESS 1;


-- This table describes N:M relation between HLT_RULE_SET and HLT_RULE
-- The Rule order matters - hrs2ru_rule_counter states the position of the rule in the XML file
CREATE TABLE HLT_HRS_TO_HRU (
      hrs2ru_id                          NUMBER(10),
      hrs2ru_rule_set_id                 NUMBER(10),
      hrs2ru_rule_id                     NUMBER(10),
      hrs2ru_rule_counter                NUMBER(10),
      CONSTRAINT            hrs2ru_pk               PRIMARY KEY (hrs2ru_id),
      CONSTRAINT             hrs2ru_fk_rs            FOREIGN KEY (hrs2ru_rule_set_id)  
                                REFERENCES HLT_RULE_SET(hrs_id),
      CONSTRAINT             hrs2ru_fk_ru            FOREIGN KEY (hrs2ru_rule_id) 
                                REFERENCES HLT_RULE(hru_id),
    CONSTRAINT            hrs2ru_id_NN         CHECK ( hrs2ru_id IS NOT NULL),
    CONSTRAINT            hrs2ru_rule_counter_NN     CHECK ( hrs2ru_rule_counter IS NOT NULL)
);
CREATE INDEX hrs2ru_rule_id_ind     ON HLT_HRS_TO_HRU(hrs2ru_rule_id) COMPRESS 1;
CREATE INDEX hrs2ru_rule_set_id_ind ON HLT_HRS_TO_HRU(hrs2ru_rule_set_id) COMPRESS 1;


-- This table describes N:M relation between HLT_RULE and HLT_RULE_COMPONENT
-- hrs2rc_component_type: 0 - unasigned (used for Merge and Sort rules), 1 - online component, 2 - offline component
CREATE TABLE HLT_HRU_TO_HRC (
      hru2rc_id                          NUMBER(10),   
      hru2rc_rule_id                     NUMBER(10),
      hru2rc_component_id                NUMBER(10),
      hru2rc_component_type              NUMBER(1),
      CONSTRAINT             hru2rc_pk               PRIMARY KEY (hru2rc_id),
      CONSTRAINT             hru2rc_fk_ru            FOREIGN KEY (hru2rc_rule_id)
                                REFERENCES HLT_RULE(hru_id),
      CONSTRAINT             hru2rc_fk_rc            FOREIGN KEY (hru2rc_component_id) 
                                REFERENCES HLT_RULE_COMPONENT(hrc_id),
      CONSTRAINT             hru2rc_rurcct           UNIQUE (hru2rc_rule_id, hru2rc_component_id, hru2rc_component_type),
    CONSTRAINT            hru2rc_id_NN         CHECK ( hru2rc_id IS NOT NULL)
);
CREATE INDEX hru2rc_rule_id_ind ON HLT_HRU_TO_HRC(hru2rc_rule_id) COMPRESS 1;
CREATE INDEX hru2rc_component_id_ind ON HLT_HRU_TO_HRC(hru2rc_component_id) COMPRESS 1;


-- This table describes N:M relation between HLT_RULE_COMPONENT and HLT_RULE_PARAMETER
-- identical structure to HLT_CP_TO_PA
CREATE TABLE HLT_HRC_TO_HRP (
      hrc2rp_id                          NUMBER(10),
      hrc2rp_component_id                NUMBER(10),
      hrc2rp_parameter_id                NUMBER(10),
      CONSTRAINT             hrc2rp_pk               PRIMARY KEY (hrc2rp_id),
      CONSTRAINT             hrc2rp_fk_rc            FOREIGN KEY (hrc2rp_component_id) 
                                REFERENCES HLT_RULE_COMPONENT(hrc_id),
      CONSTRAINT             hrc2rp_fk_rp            FOREIGN KEY (hrc2rp_parameter_id) 
                                REFERENCES HLT_RULE_PARAMETER(hrp_id),
    CONSTRAINT            hrc2rp_id_NN         CHECK ( hrc2rp_id IS NOT NULL)
);
CREATE INDEX hrc2rp_component_id_ind ON HLT_HRC_TO_HRP(hrc2rp_component_id) COMPRESS 1;
CREATE INDEX hrc2rp_parameter_id_ind ON HLT_HRC_TO_HRP(hrc2rp_parameter_id) COMPRESS 1;

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- PRESCALE ALIAS TABLES
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
CREATE TABLE prescale_set_alias (
    psa_id                NUMBER(10),
    psa_name              VARCHAR2(100), -- ENUM Basic beam types: "Physics", "Stand By", "Other"
    psa_name_free         VARCHAR2(250), -- FREE description : 	   "HI v1", "1300 bunches_25ns"...
    psa_default           CHAR		default 0, -- Denotes the default alias      
    psa_comment		  VARCHAR2(200),
    psa_username          VARCHAR2(50),
    psa_modified_time     TIMESTAMP,

    --
    CONSTRAINT             psa_pk            PRIMARY KEY(psa_id),
    CONSTRAINT             psa_name_NN	  CHECK ( psa_name IS NOT NULL)
);

-- l1 prescale table link table
CREATE TABLE l1_prescale_set_alias(
    l1psa_id                NUMBER(10),
    l1psa_name              VARCHAR2(50),
--    l1psa_version           NUMBER(11), -- No versioning
--    l1psa_used              CHAR		default 0, -- Use "DEFAULT" instead.
    l1psa_default             CHAR		default 0,       
    --
    l1psa_alias_id		NUMBER(10),
    l1psa_l1tm2ps_id		NUMBER(10),
    l1psa_lmin             	VARCHAR2(50)	default 0,
    l1psa_lmax           	VARCHAR2(50),
    l1psa_comment		VARCHAR2(200),
    --
    CONSTRAINT             l1psa_pk		PRIMARY KEY(l1psa_id),
    CONSTRAINT             l1psa_alias_id_NN	CHECK ( l1psa_alias_id IS NOT NULL),
    CONSTRAINT             l1psa_lmax_NN	CHECK ( l1psa_lmax     IS NOT NULL),
    CONSTRAINT		   l1psa_alias_fk    	FOREIGN KEY (l1psa_alias_id)
                                REFERENCES prescale_set_alias(psa_id),
    CONSTRAINT		   l1psa_l1tm2ps_fk        FOREIGN KEY (l1psa_l1tm2ps_id)
                                REFERENCES l1_tm_to_ps(l1tm2ps_id)
);
CREATE INDEX l1psa_alias_id_ind ON l1_prescale_set_alias(l1psa_alias_id) COMPRESS 1;
CREATE INDEX l1psa_l1tm2ps_id_ind ON l1_prescale_set_alias(l1psa_l1tm2ps_id) COMPRESS 1;

-- hlt prescale table link table
CREATE TABLE hlt_prescale_set_alias(
    hpsa_id                NUMBER(10),
    hpsa_name              VARCHAR2(50),
--    hpsa_version           NUMBER(11),
--    hpsa_used              CHAR		default 0,       
    hpsa_default           CHAR		default 0,       
    --
    hpsa_alias_id		NUMBER(10),
    hpsa_htm2ps_id		NUMBER(10),
    hpsa_lmin             	VARCHAR2(50)	default 0,
    hpsa_lmax           	VARCHAR2(50),
    hpsa_comment		VARCHAR2(200),
    --
    CONSTRAINT             hpsa_pk		PRIMARY KEY(hpsa_id),
    CONSTRAINT             hpsa_alias_id_NN	CHECK ( hpsa_alias_id IS NOT NULL),
    CONSTRAINT             hpsa_lmax_NN	CHECK ( hpsa_lmax     IS NOT NULL),
    CONSTRAINT		   hpsa_alias_fk    	FOREIGN KEY (hpsa_alias_id)
                                REFERENCES prescale_set_alias(psa_id),
    CONSTRAINT		   hpsa_htm2ps_fk        FOREIGN KEY (hpsa_htm2ps_id)
                                REFERENCES hlt_tm_to_ps(htm2ps_id)
);
CREATE INDEX hpsa_alias_id_ind ON hlt_prescale_set_alias(hpsa_alias_id) COMPRESS 1;
CREATE INDEX hpsa_htm2ps_id_ind ON hlt_prescale_set_alias(hpsa_htm2ps_id) COMPRESS 1;

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- MASTER TABLE
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- This table is the HLT mastertable, which defines via a masterkey (hmt_id)
-- which binds all component to have a complete HLT configuration
CREATE TABLE hlt_master_table (
    hmt_id                   NUMBER(10),
    hmt_name                 VARCHAR2(50),
    hmt_version              NUMBER(11),
    hmt_comment              VARCHAR2(200),
    hmt_trigger_menu_id      NUMBER(10),
    hmt_setup_id             NUMBER(10),
    hmt_forced_setup_id      NUMBER(10),
    hmt_username             VARCHAR2(50),
    hmt_modified_time        TIMESTAMP,
    hmt_status               NUMBER(2),
    hmt_used                 CHAR              default 0,
    CONSTRAINT            hmt_pk               PRIMARY KEY (hmt_id),
    CONSTRAINT            hmt_fk_tm            FOREIGN KEY (hmt_trigger_menu_id)    REFERENCES hlt_trigger_menu(htm_id),
    CONSTRAINT            hmt_fk_st            FOREIGN KEY (hmt_setup_id)           REFERENCES hlt_setup(hst_id),
    CONSTRAINT            hmt_nmver            UNIQUE (hmt_name, hmt_version),
    CONSTRAINT            hmt_id_NN            CHECK ( hmt_id IS NOT NULL),
    CONSTRAINT            hmt_name_NN          CHECK ( hmt_name IS NOT NULL),
    CONSTRAINT            hmt_version_NN       CHECK ( hmt_version IS NOT NULL),
    CONSTRAINT            hmt_status_NN        CHECK ( hmt_status IS NOT NULL),
    CONSTRAINT            hmt_used_NN          CHECK ( hmt_used IS NOT NULL)
);
CREATE INDEX hmt_trigger_menu_id_ind    ON hlt_master_table(hmt_trigger_menu_id) COMPRESS 1;
CREATE INDEX hmt_setup_id_ind           ON hlt_master_table(hmt_setup_id) COMPRESS 1;


--This table binds the LVL1 configuration and the HLT configuration to a single key.

CREATE TABLE super_master_table (
    smt_id                      NUMBER(10),
    smt_name                    VARCHAR2(50),
    smt_version                 NUMBER(11),
    smt_comment                 VARCHAR2(200),
    smt_origin                  VARCHAR2(50),
    smt_parent_history_key      NUMBER(10),
    smt_l1_master_table_id      NUMBER(10),
    smt_topo_master_table_id    NUMBER(10),
    smt_hlt_master_table_id     NUMBER(10),
    smt_username                VARCHAR2(50),
    smt_modified_time           TIMESTAMP,
    smt_status                  NUMBER(2),
    smt_used                    CHAR           default 0,
    CONSTRAINT            smt_pk               PRIMARY KEY (smt_id),
    CONSTRAINT            smt_fk_l1mt          FOREIGN KEY (smt_l1_master_table_id)
                                               REFERENCES l1_master_table(l1mt_id),
    CONSTRAINT            smt_fk_tmt           FOREIGN KEY (smt_topo_master_table_id)
                                               REFERENCES topo_master_table(tmt_id),
    CONSTRAINT            smt_fk_hmt           FOREIGN KEY (smt_hlt_master_table_id)
                                               REFERENCES hlt_master_table(hmt_id),
    CONSTRAINT            smt_nmver            UNIQUE (smt_name, smt_version),
    CONSTRAINT            smt_id_NN            CHECK ( smt_id IS NOT NULL),
    CONSTRAINT            smt_name_NN          CHECK ( smt_name IS NOT NULL),
    CONSTRAINT            smt_version_NN       CHECK ( smt_version IS NOT NULL),
    CONSTRAINT            smt_l1_master_id_NN  CHECK ( smt_l1_master_table_id IS NOT NULL),
    CONSTRAINT            smt_topo_master_id_NN CHECK ( smt_topo_master_table_id IS NOT NULL),
    CONSTRAINT            smt_hlt_master_id_NN CHECK ( smt_hlt_master_table_id IS NOT NULL),
    CONSTRAINT            smt_status_NN        CHECK ( smt_status IS NOT NULL),
    CONSTRAINT            smt_used_NN          CHECK ( smt_used IS NOT NULL)
);
CREATE INDEX smt_l1_master_id_ind  ON super_master_table(smt_l1_master_table_id) COMPRESS 1;
CREATE INDEX smt_topo_master_id_ind  ON super_master_table(smt_topo_master_table_id) COMPRESS 1;
CREATE INDEX smt_hlt_master_id_ind ON super_master_table(smt_hlt_master_table_id) COMPRESS 1;


-- Extra link table to relate release table to the supermaster table (one SMkey can work with many releases) 
CREATE TABLE HLT_SMT_TO_HRE (
    smt2re_id                          NUMBER(10),
    smt2re_super_master_table_id       NUMBER(10),
    smt2re_release_id                  NUMBER(10),
    CONSTRAINT            smt2re_pk                PRIMARY KEY (smt2re_id),
    CONSTRAINT            smt2re_fk_smt            FOREIGN KEY (smt2re_super_master_table_id)  
                                                   REFERENCES SUPER_MASTER_TABLE(smt_id),
    CONSTRAINT            smt2re_fk_re             FOREIGN KEY (smt2re_release_id)             
                                                   REFERENCES HLT_RELEASE(hre_id),
    CONSTRAINT            smt2re_id_NN             CHECK ( smt2re_id IS NOT NULL)
);
CREATE INDEX smt2re_smt_id_ind ON HLT_SMT_TO_HRE(smt2re_super_master_table_id) COMPRESS 1;
CREATE INDEX smt2re_rel_id_ind ON HLT_SMT_TO_HRE(smt2re_release_id) COMPRESS 1;

----------------------------
-- extra table to associate
-- the run/lb with the HLT
-- prescale key
-- 
-- this table is not accessed
-- by the triggertool, but
-- filled by the RootController
----------------------------
CREATE TABLE hlt_prescale_set_coll (
    hpsc_partition   VARCHAR2(255),
    hpsc_run         NUMBER(11),
    hpsc_lb          NUMBER(5),
    hpsc_hltpsk      NUMBER(10),
    CONSTRAINT       hpsc_pk            PRIMARY KEY (hpsc_partition, hpsc_run, hpsc_lb),
    CONSTRAINT       hpsc_run_nn        CHECK ( hpsc_run IS NOT NULL),
    CONSTRAINT       hpsc_lb_nn         CHECK ( hpsc_lb IS NOT NULL),
    CONSTRAINT       hpsc_fk_hltpsk     FOREIGN KEY (hpsc_hltpsk)
                                        REFERENCES hlt_prescale_set(hps_id)
);  
CREATE INDEX hpsc_hltpsk_ind ON hlt_prescale_set_coll(hpsc_hltpsk) COMPRESS 1;

CREATE TABLE active_masters (
    am_active_topomaster_id     NUMBER(11),
    am_active_l1master_id       NUMBER(11),
    CONSTRAINT am_pk		PRIMARY KEY (am_active_topomaster_id,am_active_l1master_id),
    CONSTRAINT am_fk_topo       FOREIGN KEY (am_active_topomaster_id)
                                REFERENCES topo_master_table(tmt_id),
    CONSTRAINT am_fk_l1         FOREIGN KEY (am_active_l1master_id)
                                REFERENCES l1_master_table(l1mt_id)
);
CREATE INDEX am_active_topomaster_id_ind ON active_masters(am_active_topomaster_id) COMPRESS 1;
CREATE INDEX am_active_l1master_id_ind ON active_masters(am_active_l1master_id) COMPRESS 1;

CREATE TABLE trigger_log (
    tlog_username             VARCHAR2(50),
    tlog_short                VARCHAR2(200),
    tlog_message              VARCHAR2(2000),
    tlog_modified_time        TIMESTAMP,
    CONSTRAINT tlog_pk	      PRIMARY KEY(tlog_username,tlog_modified_time)
);

create global temporary table TEMP (TEMPVAR NUMBER) on commit delete rows;

commit;
