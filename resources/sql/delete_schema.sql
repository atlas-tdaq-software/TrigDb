-- $Id: combined_schema.sql,v 1.83 2009-03-29 08:14:27 stelzer Exp $
-- $Revision: 1.83 $
---------------------------------------------------------
-- SQL script for the schema generation of the TriggerDB
--
-- This script simply combines scripts that have been
-- developed before (see comments below)
-- In this combination the names of tables and attributes 
-- have been changed in order to follow a consistent naming 
-- convention throughout the entire DB.
------------------------------------
-----------------------------
--                         --
-- clear the existing      --
-- tables                  --
--                         -- 
-----------------------------

-- In ORACLE tables can only be dropped 
-- if no foreign key points to them.
-- Hence we start droping the master tables
-- from the top, next we remove the M:N 
-- tables, next we remove all other tables
-- that hold a foreign key, and finally the
-- tables the have no foreign key.

-- Note that for the creation the reverse order
-- must be obeyed

-- We should use PURGE when on Oracle to make sure
-- that the database will get replicated without
-- problems

DROP TABLE trigger_log PURGE;
DROP TABLE trigger_schema PURGE;
DROP TABLE tt_users PURGE;
DROP TABLE tt_writelock PURGE;

-- drop trigger alias table
DROP TABLE trigger_alias PURGE;

-- drop the LB-HLTPSK association table
DROP TABLE hlt_prescale_set_coll PURGE;

-- specific order:
DROP TABLE HLT_SMT_TO_HRE PURGE;

-- drop master table first
DROP TABLE super_master_table PURGE;

-- drop PRESCALE ALIAS
-- DROP TABLE prescale_set_alias_to_tm2ps PURGE;
DROP TABLE l1_prescale_set_alias PURGE;
DROP TABLE hlt_prescale_set_alias PURGE;
DROP TABLE prescale_set_alias PURGE;

---------------------------
DROP TABLE topo_l1_link PURGE;

DROP TABLE active_masters PURGE;

-- drop the LVL1 tables

-- drop the master table first
DROP TABLE l1_master_table PURGE;

-- drop N-N tables
DROP TABLE l1_tm_to_ps PURGE;
DROP TABLE l1_tm_to_ti PURGE;
DROP TABLE l1_ti_to_tt PURGE;
DROP TABLE l1_tt_to_ttv PURGE;
DROP TABLE l1_pits PURGE;
DROP TABLE l1_tm_to_tt_mon PURGE;
DROP TABLE l1_tm_to_tt PURGE;
DROP TABLE l1_tm_to_tt_forced PURGE;
DROP TABLE l1_bgs_to_bg PURGE;
DROP TABLE l1_bg_to_b PURGE;
DROP TABLE l1_ci_to_csc PURGE;

-- drop tables with FK
DROP TABLE l1_prescale_set PURGE;
DROP TABLE l1_trigger_menu PURGE;

-- drop the rest
DROP TABLE l1_bunch_group_set PURGE;
DROP TABLE l1_bunch_group PURGE;
DROP TABLE l1_muctpi_info PURGE;
DROP TABLE l1_dead_time PURGE;
DROP TABLE l1_random PURGE;
DROP TABLE l1_prescaled_clock PURGE;
DROP TABLE l1_jet_input PURGE;
DROP TABLE l1_calo_sin_cos PURGE; 
DROP TABLE l1_ctp_files PURGE;
DROP TABLE l1_ctp_smx PURGE;
DROP TABLE l1_trigger_item PURGE;
DROP TABLE l1_trigger_threshold PURGE;
DROP TABLE l1_trigger_threshold_value PURGE;
DROP TABLE l1_calo_info PURGE;
DROP TABLE l1_calo_min_tob PURGE;
DROP TABLE l1_calo_isolation PURGE;
DROP TABLE l1_calo_isoparam PURGE;
DROP TABLE l1_muon_threshold_set PURGE;

---------------------------
-- drop the LVL1 topo tables

-- drop the master table first
DROP TABLE topo_master_table PURGE;

-- drop N-N tables
DROP TABLE ttm_to_ta PURGE;
DROP TABLE ttm_to_tc PURGE;
DROP TABLE ta_to_ti PURGE;
DROP TABLE ta_to_to PURGE;
DROP TABLE ta_to_tp PURGE;
DROP TABLE ta_to_tg PURGE;
DROP TABLE topo_output_link PURGE;

--drop tables with foreign keys
DROP TABLE topo_algo_input PURGE;
DROP TABLE topo_parameter PURGE;
DROP TABLE topo_generic PURGE;
DROP TABLE topo_algo_output PURGE;

--drop the rest
DROP TABLE topo_algo   PURGE;
DROP TABLE topo_output_line PURGE;
DROP TABLE topo_config PURGE;
DROP TABLE topo_trigger_menu PURGE;

DROP TABLE topo_output_list PURGE;

------------------------------
-- drop the HLT tables

-- drop the master table first
DROP TABLE hlt_master_table PURGE;

-- drop the M-N tables
DROP TABLE hlt_cp_to_pa PURGE;
DROP TABLE hlt_cp_to_cp PURGE;
DROP TABLE hlt_st_to_cp PURGE;
DROP TABLE hlt_tm_to_tc PURGE;
DROP TABLE hlt_ts_to_te PURGE;
DROP TABLE hlt_te_to_te PURGE;
DROP TABLE hlt_tc_to_ts PURGE;
DROP TABLE hlt_te_to_cp PURGE;
DROP TABLE hlt_tc_to_tr PURGE;
DROP TABLE hlt_tm_to_ps PURGE;

-- drop M-N rule tables
DROP TABLE HLT_HRE_TO_HRS PURGE;
DROP TABLE HLT_HRS_TO_HRU PURGE;
DROP TABLE HLT_HRU_TO_HRC PURGE;
DROP TABLE HLT_HRC_TO_HRP PURGE;

-- drop the tables with FK
--DROP TABLE hlt_force_dll PURGE;
DROP TABLE hlt_prescale PURGE;
DROP TABLE hlt_prescale_set PURGE;
DROP TABLE hlt_trigger_group PURGE;
DROP TABLE hlt_trigger_type PURGE;
DROP TABLE hlt_trigger_signature PURGE;
DROP TABLE hlt_trigger_menu PURGE;
DROP TABLE hlt_trigger_chain PURGE;

-- drop the rest
DROP TABLE hlt_release PURGE;
DROP TABLE hlt_parameter PURGE;
DROP TABLE hlt_trigger_element PURGE;
DROP TABLE hlt_setup PURGE;
DROP TABLE hlt_component PURGE;
DROP TABLE hlt_trigger_stream PURGE;

-- drop rule tables
DROP TABLE HLT_RULE_SET PURGE;
DROP TABLE HLT_RULE PURGE;
DROP TABLE HLT_RULE_COMPONENT PURGE;
DROP TABLE HLT_RULE_PARAMETER PURGE;

-- drop table DBCOPY_SOURCE_DATABASE
DROP TABLE DBCOPY_SOURCE_DATABASE PURGE;

-- drop global temporary table TEMP
DROP TABLE TEMP PURGE;

COMMIT;
