/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author davethomas
 */
public class DiffMutableTreeNodeTest {
    
    public DiffMutableTreeNodeTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetKey() {
        System.out.println("getKey");
        DiffMutableTreeNode instance = new DiffMutableTreeNode("inkeyStr", "inval1Str", "inval2Str");
        String expResult = "inkeyStr";
        Object result = instance.getKey();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetVal1() {
        System.out.println("getVal1");
        DiffMutableTreeNode instance = new DiffMutableTreeNode("inkeyStr", "inval1Str", "inval2Str");
        String expResult = "inval1Str";
        Object result = instance.getVal1();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetVal2() {
        System.out.println("getVal2");
        DiffMutableTreeNode instance = new DiffMutableTreeNode("inkeyStr", "inval1Str", "inval2Str");
        String expResult = "inval2Str";
        Object result = instance.getVal2();
        assertEquals(expResult, result);
    }
    
}
