/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import com.mockrunner.mock.jdbc.MockResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.Entities.HLT.HLTMaster;
import triggerdb.Entities.HLT.HLTRelease;
import triggerdb.Entities.L1.L1Master;
import triggerdb.Entities.SMT.SuperMasterTable;
import triggerdb.Entities.Topo.TopoMaster;
import triggerdb.Entities.alias.HLTPrescaleSetAliasLumi;
import triggerdb.Entities.alias.L1PrescaleSetAliasLumi;
import triggerdb.Entities.alias.PrescaleSetAlias;
import triggerdb.PrescaleSetAliasComponent;

/**
 *
 * @author davethomas
 */
public class PrescaleSetAliasComponentTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public PrescaleSetAliasComponentTest() {
    }
    
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
        ConnectionManager.getInstance().unsetTestingConnection();
    }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
    }


    @Test
    public void testGetAliasList() throws Exception {
        System.out.println("getAliasList");
        PrescaleSetAliasComponent instance = new PrescaleSetAliasComponent();     
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        List expResult = new ArrayList<PrescaleSetAliasComponent>();
        Object result = instance.getAliasList(2712);
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_l1_aliases() throws Exception {
        System.out.println("get_l1_aliases");
        PrescaleSetAliasComponent instance = new PrescaleSetAliasComponent();     
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        SuperMasterTable input_smt = new SuperMasterTable();
        PrescaleSetAlias input_alias = new PrescaleSetAlias();
        List expResult = new ArrayList<L1PrescaleSetAliasLumi>();
        Object result = instance.get_l1_aliases(input_smt, input_alias);
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_hlt_aliases() throws Exception {
        System.out.println("get_hlt_aliases");
        PrescaleSetAliasComponent instance = new PrescaleSetAliasComponent();     
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        SuperMasterTable input_smt = new SuperMasterTable();
        PrescaleSetAlias input_alias = new PrescaleSetAlias();
        List expResult = new ArrayList<HLTPrescaleSetAliasLumi>();
        Object result = instance.get_hlt_aliases(input_smt, input_alias);
        assertEquals(expResult, result);
    }

    @Test
    public void test_get_lumis_from_DB() throws Exception {
        System.out.println("_get_lumis_from_DB");
        PrescaleSetAliasComponent instance = new PrescaleSetAliasComponent();     
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        SuperMasterTable input_smt = new SuperMasterTable();
        input_smt.set_id(2712);
        PrescaleSetAlias input_alias = new PrescaleSetAlias();
        input_alias.set_id(27);
        List expResult = new ArrayList<PrescaleSetAliasLumi>();
        Object result = instance._get_lumis_from_DB(input_smt, input_alias);
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_lumis() throws Exception {

    }

    @Test
    public void testAdd_lumi() throws Exception {
       // System.out.println("add_lumi");
        //PrescaleSetAliasComponent instance = new PrescaleSetAliasComponent();     
        //MockResultSet resultset = resultSetHandler.createResultSet();
        //setEmptyInitInfo();
        //PrescaleSetAliasLumi param = new PrescaleSetAliasLumi();
        //instance.add_lumi(param);
        //List expResult = new ArrayList<PrescaleSetAliasLumi>();
        //expResult.add(param);
        //Object result = instance.get_lumis();
        //assertEquals(expResult, result);
        //YIELDS NULL POINTER -> NEEDS FIXING
    }

    @Test
    public void testGet_name() {
    }

    @Test
    public void testSet_name() {
        System.out.println("set_name");
        PrescaleSetAliasComponent instance = new PrescaleSetAliasComponent();
        String input = "a string";
        instance.set_name(input);
        String expResult = "a string";
        Object result = instance.get_name();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_name_free() {
    }

    @Test
    public void testSet_name_free() {
        System.out.println("set_name_free");
        PrescaleSetAliasComponent instance = new PrescaleSetAliasComponent();
        String input = "a string";
        instance.set_name_free(input);
        String expResult = "a string";
        Object result = instance.get_name_free();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_comment() {
    }

    @Test
    public void testSet_comment() {
        System.out.println("set_comment");
        PrescaleSetAliasComponent instance = new PrescaleSetAliasComponent();
        String input = "a string";
        instance.set_comment(input);
        String expResult = "a string";
        Object result = instance.get_comment();
        assertEquals(expResult, result);
    }

    @Test
    public void testSave() throws Exception {
        System.out.println("save");
        PrescaleSetAliasComponent instance = new PrescaleSetAliasComponent();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Integer expResult = 1;
        Object result = instance.save();
        assertEquals(expResult, result);
    }

    @Test
    public void testUpdate_comment() throws Exception {
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        PrescaleSetAliasComponent instance = new PrescaleSetAliasComponent();
        String expResult= "( / -1)";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
    }

    @Test
    public void testCompareTo() {
        System.out.println("compareTo");
        PrescaleSetAliasComponent instance = new PrescaleSetAliasComponent();
        PrescaleSetAliasComponent param = new PrescaleSetAliasComponent();
        Integer expResult= 0;
        Object Result = instance.compareTo(param);
        assertEquals(expResult, Result);
    }

    @Test
    public void testGet_super_master_table() {
        System.out.println("get_super_master_table");
        PrescaleSetAliasComponent instance = new PrescaleSetAliasComponent();
        SuperMasterTable expResult = null;
        Object Result = instance.get_super_master_table();
        assertEquals(expResult, Result);
    }

    @Test
    public void testGet_alias_id() {
        System.out.println("get_alias_id");
        PrescaleSetAliasComponent instance = new PrescaleSetAliasComponent();
        Integer expResult = -1;
        Object Result = instance.get_alias_id();
        assertEquals(expResult, Result);
    }

    @Test
    public void testGet_alias() {
        System.out.println("get_alias");
        PrescaleSetAliasComponent instance = new PrescaleSetAliasComponent();
        PrescaleSetAlias expResult = new PrescaleSetAlias();
        Object Result = instance.get_alias();
        assertEquals(expResult, Result);
    }
    
}
