/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Connections;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import com.mockrunner.mock.jdbc.MockResultSet;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.DBConnections.AliasEntry;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.Entities.HLT.HLTMaster;
import triggerdb.Entities.HLT.HLTRelease;
import triggerdb.Entities.L1.L1Item;
import triggerdb.Entities.L1.L1Master;
import triggerdb.Entities.SMT.SuperMasterTable;
import triggerdb.Entities.Topo.TopoMaster;
import triggerdb.Entities.alias.HLTPrescaleSetAliasLumi;
import triggerdb.Entities.alias.L1PrescaleSetAliasLumi;
import triggerdb.Entities.alias.PrescaleSetAlias;
import triggerdb.PrescaleSetAliasComponent;

/**
 *
 * @author davethomas
 */
public class DBConnectionsTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public DBConnectionsTest() {
    }
    
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
        ConnectionManager.getInstance().unsetTestingConnection();
    }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
    }


    @Test
    public void testSetOnline() {
        System.out.println("setOnline");
        DBConnections instance = new DBConnections(); 
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Boolean expResult = false;
        instance.setOnline(false);
        Object result = instance.getOnline();
        assertEquals(expResult, result);
    }

    @Test
    public void testIsOnlineExpert() {
        //static method
    }

    @Test
    public void testSetUpConnection() {
        //null pointer
    }

    @Test
    public void testGetDBstring() {
        System.out.println("getDBstring");
        DBConnections instance = new DBConnections(); 
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        instance.setDBAliases("","");
        String expResult = "";
        String input = "";
        Object result = instance.getDBstring(input);
        assertEquals(expResult, result);
        
        String input2 = "a string";
        String expResult2 = null;
        Object result2 = instance.getDBstring(input2);
        assertEquals(expResult2, result2);
        
    }

    @Test
    public void testCreateAtlrReader() {
        //Why does this fail?
        //System.out.println("createAtlrReader");
        //DBConnections instance = new DBConnections(); 
        //MockResultSet resultset = resultSetHandler.createResultSet();
        //setEmptyInitInfo();
        //InitInfo expResult = new InitInfo();
        //instance.setAtlrReader(expResult);
        //Object result = instance.createAtlrReader();
        //assertEquals(expResult, result);
    }

    @Test
    public void testSetAtlrReader() {
        //Why does this fail?
        //System.out.println("setAtlrReader");
        //DBConnections instance = new DBConnections(); 
        //MockResultSet resultset = resultSetHandler.createResultSet();
        //setEmptyInitInfo();
        //InitInfo expResult = new InitInfo();
        //instance.setAtlrReader(expResult);
        //Object result = instance.getAtlrReader();
        //assertEquals(expResult, result);
    }

    @Test
    public void testSetAtonrWriter() {
    }

    @Test
    public void testSetAtonrReader() {
    }

    @Test
    public void testSetRHULReader() {
    }

    @Test
    public void testSetLocalReader() {
    }

    @Test
    public void testCreateAtonrWriter() {
    }

    @Test
    public void testCreateAtonrReader() {
    }

    @Test
    public void testReadLinesFromDotTriggerTool() {
    }

    @Test
    public void testCreateDropDownEntriesFromDBLookup() {
    }

    @Test
    public void testCreateDropDownEntriesFromDotTriggerTool() {
    }

    @Test
    public void testGetConnUserPwFromAlias() throws Exception {
        System.out.println("getConnUserPwFromAlias");
        DBConnections instance = new DBConnections(); 
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        String[] expResult = {"",""};
        String input = "";
        String[] result = instance.getConnUserPwFromAlias(input);
        assertArrayEquals(expResult, result);
        
        AliasEntry entry = new AliasEntry("a string", true);
        instance.setDBLookup("a string", entry);
        String[] expResult2 = {"",""};
        String input2 = "a string";
        String[] result2 = instance.getConnUserPwFromAlias(input2);
        assertArrayEquals(expResult2, result2);
    }

    @Test
    public void testGetSvcNameFromDbConn() throws Exception {
        System.out.println("getSvcNameFromDbConn");
        DBConnections instance = new DBConnections(); 
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        String expResult = "";
        String input = "";
        String result = instance.getSvcNameFromDbConn(input);
        assertEquals(expResult, result);
        
        String expResult2 = "a string";
        String input2 = "a string";
        AliasEntry ae = new AliasEntry(input2, true);
        instance.setDBLookup(input2, ae);
        String result2 = instance.getSvcNameFromDbConn(input2);
        assertEquals(expResult2, result2);
    }

    @Test
    public void testGetConnUserPwFromSvcName() throws Exception {
    //Exception
    }

    @Test
    public void testGetDBLookupPath() {
    }

    @Test
    public void testGetDBAuthPath() {
        System.out.println("getDBAuthPath");
        DBConnections instance = new DBConnections(); 
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        ArrayList<Object> expResults = new ArrayList<>();
        expResults.add(null);
        expResults.add("/eos/user/a/atdaqeos/adm/db");
        expResults.add("/afs/cern.ch/atlas/project/tdaq/databases/.coral");
        String result = instance.getDBAuthPath();
        assertTrue(expResults.contains(result));
    }
}
