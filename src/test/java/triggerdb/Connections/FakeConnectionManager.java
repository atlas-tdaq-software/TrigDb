package triggerdb.Connections;

import triggerdb.Entities.AbstractTable;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import com.mockrunner.jdbc.*;
import com.mockrunner.mock.jdbc.*;

/**
 * Created by adambozson on 23/11/2016.
 */
public class FakeConnectionManager extends ConnectionManager {

    public FakeConnectionManager(Connection connection) {
        super(null);
        this.connection_ = connection;
    }
    
//    @Override
//    public boolean connect(InitInfo initInfo) throws SQLException {
//        return true;
//    }
//
//    @Override
//    public void disconnect() throws SQLException {
//
//    }
//
//    @Override
//    public void forceLoad(AbstractTable aThis) throws SQLException {
//
//    }
//
//    @Override
//    public InitInfo getInitInfo() {
//        return null;
//    }
//
//    @Override
//    public Connection getConnection() {
//        return null;
//    }
//
//    @Override
//    public boolean isDBCopy() {
//        return false;
//    }
//
//    @Override
//    public boolean isConnectionOpen() {
//        return true;
//    }
//
//    @Override
//    public String fix_schema_name(String query) {
//        return null;
//    }
//
//    @Override
//    public void setSetter(Object onj, int i, PreparedStatement ps) throws SQLException {
//
//    }
//
//    @Override
//    public String get_clob(String tableName, String tablePrefix, int id, String field) {
//        return null;
//    }
//
//    @Override
//    public void set_clob(String tableName, String fieldName, String filename, String tablePrefix, int id) {
//
//    }
//
//    @Override
//    public void set_clob_from_str(String tableName, String fieldName, String text, String tablePrefix, int id) {
//
//    }
//
//    @Override
//    public ArrayList<Integer> get_IDs(String table_name, String table_prefix, TreeMap<String, Object> fields_values, String order, String ignore) throws SQLException {
//        return null;
//    }
//
//    @Override
//    public int save(String tableName, String tablePrefix, Integer oldId, TreeMap<String, Object> fields_values) throws SQLException {
//        return 0;
//    }
//
//    @Override
//    public TreeMap<Integer, Integer> saveBatch(String tableName, String tablePrefix, Integer id, ArrayList<TreeMap<String, Object>> table) throws SQLException {
//        return null;
//    }
//
//    @Override
//    public ArrayList<AbstractTable> forceLoadVector(AbstractTable child, String whereClause, List filters) throws SQLException {
//        return null;
//    }
//
//    @Override
//    public ArrayList<AbstractTable> forceLoadVector(AbstractTable child, String query, String where, List filters) throws SQLException {
//        return null;
//    }
//
//    @Override
//    public ArrayList<AbstractTable> forceLoadVector(AbstractTable child, String queryClause, String whereClause, List filters, int extraColumnIdx, List<Integer> extraColumn) throws SQLException {
//        return null;
//    }
//
//    @Override
//    public ArrayList<AbstractTable> LoadVectorOf(Class<? extends AbstractTable> classType, String queryClause, String whereClause, List filters, int extraColumnIdx, List<Integer> extraColumn) throws SQLException, InstantiationException, InvocationTargetException, IllegalAccessException, NoSuchMethodException {
//        return null;
//    }
//
//    @Override
//    public TreeMap<Integer, Integer> getConstrainedItemList(String query, int id) {
//        return null;
//    }
//
//    @Override
//    public int getMaxVersion(String tableName, String tablePrefix, String name) {
//        return 0;
//    }
//
//    @Override
//    public int getMaxCompVersion(String name, String alias) {
//        return 0;
//    }
//
//    @Override
//    public ArrayList<Integer> matchingConnectionSignatureIDs(ArrayList<Integer> id_vec, ArrayList<Integer> counters_vec, ArrayList<Integer> id2_vec) throws SQLException {
//        return null;
//    }
//
//    @Override
//    public ArrayList<Integer> get_ItemList(String table_name, String table_prefix, String base_name, String reference_name, int baseID) {
//        return null;
//    }
//
//    @Override
//    public ArrayList<int[]> get_CompactItemList(String table_name, String table_prefix, String base_name, String reference_name, ArrayList<Integer> BaseIds) {
//        return null;
//    }
//
//    @Override
//    public ArrayList<Integer> get_hashIDs(String table_name, String table_prefix, Integer hash, boolean full) throws SQLException {
//        return null;
//    }
//
//    @Override
//    public void Update(String tableName, String tablePrefix, Integer id, TreeMap<String, Object> tableValues) throws SQLException {
//
//    }
//
//    @Override
//    public int loadSchemaVersion(Connection connection) {
//        return 0;
//    }
//
//    @Override
//    public int executeUpdateStatement(String query) throws SQLException {
//        return 0;
//    }
//
//    @Override
//    public ArrayList<Integer> getLinkedTable(ArrayList<String> Objects, String linkTableName, String tableName, String linkUpID, String linkDownID, String targetID, String Ordering) {
//        return null;
//    }
//
//    @Override
//    public ArrayList<Integer> get_allIDs(String table_name, String table_prefix) throws SQLException {
//        return null;
//    }
//
//    @Override
//    public int checkDBVersionCompatible() {
//        return 0;
//    }
//
//    @Override
//    public int isDBVersionCompatible(int dbversion, String dbdesc) {
//        return 0;
//    }
//
//    @Override
//    public String getDBVersionWarningMsg(int type) {
//        return null;
//    }
//
//    @Override
//    public void changeUser(String user, String pwd, UserMode mode) {
//
//    }
//
//    @Override
//    public ArrayList<AbstractMap.SimpleEntry<Integer, Integer>> checkTopoExisting(ArrayList<String> Objects, String Names, String tablePrefix, String tableName) {
//        return null;
//    }
//
//    @Override
//    public ArrayList<Integer> checkLinkExisting(ArrayList<String> Objects, String ID, String tableName, String list_id) {
//        return null;
//    }
//
//    @Override
//    public TreeMap<Integer, ArrayList<Integer>> checkTopoExistingMulti(ArrayList<String> Objects, String Names, String tablePrefix, String tableName) {
//        return null;
//    }
}
