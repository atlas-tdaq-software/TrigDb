/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Connections;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import com.mockrunner.mock.jdbc.MockResultSet;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.Entities.HLT.HLTMaster;
import triggerdb.Entities.HLT.HLTRelease;
import triggerdb.Entities.L1.L1Item;
import triggerdb.Entities.L1.L1Master;
import triggerdb.Entities.SMT.SuperMasterTable;
import triggerdb.Entities.Topo.TopoMaster;
import triggerdb.Entities.alias.HLTPrescaleSetAliasLumi;
import triggerdb.Entities.alias.L1PrescaleSetAliasLumi;
import triggerdb.Entities.alias.PrescaleSetAlias;
import triggerdb.PrescaleSetAliasComponent;

/**
 *
 * @author davethomas
 */
public class DotTriggerToolEntryTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public DotTriggerToolEntryTest() {
    }
    
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
        ConnectionManager.getInstance().unsetTestingConnection();
    }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
    }

    @Test
    public void testSetReadOnly() {
        System.out.println("setReadOnly");
        DBTechnology tech = DBTechnology.ORACLE;
        DotTriggerToolEntry instance = new DotTriggerToolEntry(tech, "","","","");
        instance.setReadOnly(true);
        Boolean expResult = true;
        Object result = instance.getReadOnly();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetInitInfo() throws Exception {
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        DBTechnology tech = null;
        DotTriggerToolEntry instance = new DotTriggerToolEntry(tech, "","","","");
        String expResult = "";
        Object result = instance.toString();
        assertEquals(expResult, result);
        
        DBTechnology tech2 = DBTechnology.DBLOOKUP;
        DotTriggerToolEntry instance2 = new DotTriggerToolEntry(tech2, "","","","");
        String expResult2 = "  r   [] *";
        Object result2 = instance2.toString();
        assertEquals(expResult2, result2);
        
        DBTechnology tech3 = DBTechnology.UNKNOWN;
        DotTriggerToolEntry instance3 = new DotTriggerToolEntry(tech3, "","","","");
        String expResult3 = "";
        Object result3 = instance3.toString();
        assertEquals(expResult3, result3);
    }

    @Test
    public void testCompareTo() {
        System.out.println("compareTo");
        DBTechnology tech = DBTechnology.ORACLE;
        DotTriggerToolEntry instance = new DotTriggerToolEntry(tech, "","","","");
        DBTechnology tech2 = DBTechnology.DBLOOKUP;
        DotTriggerToolEntry param = new DotTriggerToolEntry(tech2, "","","","");
        Integer expResult = -1;
        Object result = instance.compareTo(param);
        assertEquals(expResult, result);
        
        DotTriggerToolEntry param2 = new DotTriggerToolEntry(tech,"a string","","",""); 
        Integer expResult2 = -8;
        Object result2 = instance.compareTo(param2);
        assertEquals(expResult2, result2);
       
        DotTriggerToolEntry param3 = new DotTriggerToolEntry(tech,"","a string","",""); 
        Integer expResult3 = -8;
        Object result3 = instance.compareTo(param3);
        assertEquals(expResult3, result3);
    }

    @Test
    public void testGetDbaccount() {
    }

    @Test
    public void testGetDbhost() {
    }

    @Test
    public void testGetDbpasswd() {
        System.out.println("getDbpasswd");
        DBTechnology tech = DBTechnology.ORACLE;
        DotTriggerToolEntry instance = new DotTriggerToolEntry(tech, "","","","");
        String expResult = "";
        Object result = instance.getDbpasswd();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetDbschema() {
        System.out.println("getDbschema");
        DBTechnology tech = DBTechnology.ORACLE;
        DotTriggerToolEntry instance = new DotTriggerToolEntry(tech, "","","","");
        String expResult = "";
        Object result = instance.getDbschema();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetAlias() {
        System.out.println("getDbalias");
        DBTechnology tech = DBTechnology.ORACLE;
        DotTriggerToolEntry instance = new DotTriggerToolEntry(tech, "","","","");
        String expResult = "";
        Object result = instance.getAlias();
        assertEquals(expResult, result);
    }

    @Test
    public void testIsReadonly() {
        System.out.println("isReadonly");
        DBTechnology tech = DBTechnology.ORACLE;
        DotTriggerToolEntry instance = new DotTriggerToolEntry(tech, "","","","");
        Boolean expResult = true;
        Object result = instance.isReadonly();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetTechnology() {
    }
    
    
}
