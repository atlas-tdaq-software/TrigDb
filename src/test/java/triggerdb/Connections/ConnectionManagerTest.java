/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Connections;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import com.mockrunner.mock.jdbc.MockResultSet;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.Entities.HLT.HLTMaster;
import triggerdb.Entities.HLT.HLTRelease;
import triggerdb.Entities.L1.L1Item;
import triggerdb.Entities.L1.L1Master;
import triggerdb.Entities.SMT.SuperMasterTable;
import triggerdb.Entities.Topo.TopoMaster;
import triggerdb.Entities.alias.HLTPrescaleSetAliasLumi;
import triggerdb.Entities.alias.L1PrescaleSetAliasLumi;
import triggerdb.Entities.alias.PrescaleSetAlias;
import triggerdb.PrescaleSetAliasComponent;

/**
 *
 * @author davethomas
 */
public class ConnectionManagerTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public ConnectionManagerTest() {
    }
    
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
        ConnectionManager.getInstance().unsetTestingConnection();
    }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
    }


    @Test
    public void testLoadSchemaVersionStatic() throws SQLException {
        //System.out.println("loadSchemaVersionStatic");
        //Logger logger= Logger.getLogger("TriggerDb");
        //ConnectionManager instance = new ConnectionManager(logger); 
        //Connection param = new Connection();
        //Integer expResult = 0;
        //InitInfo init = new InitInfo();
        //instance.connect(init);
        //Object result = instance.loadSchemaVersionStatic(param);
        //assertEquals(expResult, result);
    }

    @Test
    public void testIsConnectionOpen() {
        System.out.println("isConnectionOpen");
        Logger logger= Logger.getLogger("TriggerDb");
        ConnectionManager instance = new ConnectionManager(logger); 
        Boolean expResult = false;
        Object result = instance.isConnectionOpen();
        assertEquals(expResult, result);
    }

    @Test
    public void testConnect() throws Exception {
        System.out.println("connect");
        Logger logger= Logger.getLogger("TriggerDb");
        ConnectionManager instance = new ConnectionManager(logger);     
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        InitInfo init = new InitInfo();
        init.setUserName("atlas_conf_trigger_v2_w");
        Boolean expResult = true;
        Object result = instance.connect(init);
        assertEquals(expResult, result);
    }

    @Test
    public void testDisconnect() throws Exception {
    }

    @Test
    public void testForceLoad() throws Exception {
    }
    
    @Test
    public void testCheckPassword() throws Exception {
        System.out.println("checkPassword");
        Logger logger= Logger.getLogger("TriggerDb");
        ConnectionManager instance = new ConnectionManager(logger); 
        InitInfo param = new InitInfo();
        Boolean expResult = false;
        instance.connect(param);
        boolean result = instance.getPasswordOK();
        assertEquals(expResult, result);
        
    }
    
    @Test
    public void testExists(){ 
    }

    @Test
    public void testForceLoadVector_3args() throws Exception {
    }

    @Test
    public void testForceLoadVector_4args() throws Exception {
    }

    @Test
    public void testForceLoadVector_6args() throws Exception {
    }

    @Test
    public void testLoadVectorOf() throws Exception {
        System.out.println("loadVectorOf");
        Logger logger= Logger.getLogger("TriggerDb");
        ConnectionManager instance = new ConnectionManager(logger); 
        ArrayList<AbstractTable> expResult = new ArrayList<>();
        Class<? extends AbstractTable> param_class = null;
        String param_query = "query";
        String param_where = "where";
        List param_list = new ArrayList<>();
        int param_id = 3;
        List<Integer> param_column = new ArrayList<>();
        InitInfo init = new InitInfo();
        instance.connect(init);
        Object result = instance.LoadVectorOf(param_class,param_query,param_where,param_list,param_id,param_column);
        assertEquals(expResult, result);
    }

    @Test
    public void testPreparePlaceHolders() throws SQLException {
        System.out.println("preparePlaceHolders");
        Logger logger= Logger.getLogger("TriggerDb");
        ConnectionManager instance = new ConnectionManager(logger); 
        InitInfo param = new InitInfo();
        String expResult = "?,?,?,?,?";
        instance.connect(param);
        String result = instance.preparePlaceHolders(5);
        assertEquals(expResult, result);
        
    }

    @Test
    public void testSetValues() throws Exception {
    }

    @Test
    public void testGetInitInfo() {
    }

    @Test
    public void testSetInitInfo() {
    }

    @Test
    public void testGetConnection() {
    }

    @Test
    public void testIsDBCopy() {
       System.out.println("isDBcopy");
        Logger logger= Logger.getLogger("TriggerDb");
        ConnectionManager instance = new ConnectionManager(logger); 
        Boolean expResult = false;
        Object result = instance.isDBCopy();
        assertEquals(expResult, result);
    }

    @Test
    public void testDisconnectFromCopyDB() throws Exception {
    }

    @Test
    public void testConnectToCopyDB() throws Exception {
        System.out.println("connectToCopyDB");
        Logger logger= Logger.getLogger("TriggerDb");
        ConnectionManager instance = new ConnectionManager(logger); 
        InitInfo param = new InitInfo();
        Boolean expResult = true;
        instance.connect(param);
        boolean result = instance.connectToCopyDB(param);
        assertEquals(expResult, result);
    }

    @Test
    public void testSwitchToCopyDB() {
        System.out.println("switchToCopyDB");
        Logger logger= Logger.getLogger("TriggerDb");
        ConnectionManager instance = new ConnectionManager(logger); 
        Boolean expResult = false;
        Object result = instance.switchToCopyDB();
        assertEquals(expResult, result);
    }

    @Test
    public void testSwitchToMainDB() {
        System.out.println("switchToMainDB");
        Logger logger= Logger.getLogger("TriggerDb");
        ConnectionManager instance = new ConnectionManager(logger); 
        Boolean expResult = true;
        Object result = instance.switchToMainDB();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_clob_from_str() throws SQLException {
    }

    //@Test
    public void testSet_clob() throws SQLException {
        System.out.println("set_clob");
        Logger logger= Logger.getLogger("TriggerDb");
        ConnectionManager instance = new ConnectionManager(logger); 
        InitInfo param = new InitInfo();
        String param1 = "";
        String param2 = "";
        String param3 = "";
        String param4 = "";
        Integer param5 = 7;
        instance.connect(param);
        String expResult = "";
        instance.set_clob(param1, param2, param3, param4, param5);
        String result = instance.get_clob(param1, param4, param5, param2);
        assertEquals(expResult, result);
        
    }

    @Test
    public void testGet_clob() {
    }

    @Test
    public void testGet_log_path() {
        System.out.println("get_log_path");
        Logger logger= Logger.getLogger("TriggerDb");
        ConnectionManager instance = new ConnectionManager(logger); 
        String expResult = "";
        Object result = instance.get_log_path();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetInstance() {
    }

    @Test
    public void testSetTestingConnection() {
    }

    @Test
    public void testUnsetTestingConnection() {
    }

    @Test
    public void testExecuteDeleteTable() throws Exception {
        System.out.println("executeDeleteTable");
        Logger logger= Logger.getLogger("TriggerDb");
        ConnectionManager instance = new ConnectionManager(logger); 
        InitInfo init = new InitInfo();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        String input = "name";
        Integer expResult = 0;
        instance.connect(init);
        Object result = instance.executeDeleteTable(input);
        assertEquals(expResult, result);
    }

    @Test
    public void testExecuteUpdateStatement() throws Exception {
        System.out.println("executeUpdateStatement");
        Logger logger= Logger.getLogger("TriggerDb");
        ConnectionManager instance = new ConnectionManager(logger); 
        InitInfo init = new InitInfo();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        String input = "name";
        Integer expResult = 0;
        instance.connect(init);
        Object result = instance.executeUpdateStatement(input);
        assertEquals(expResult, result);
    }

    @Test
    public void testSave() throws Exception {
    }

    @Test
    public void testSaveBatch() throws Exception {
        System.out.println("set_clob");
        Logger logger= Logger.getLogger("TriggerDb");
        ConnectionManager instance = new ConnectionManager(logger); 
        InitInfo param = new InitInfo();
        String param1 = "";
        String param2 = "";
        Integer param3 = 7;
        ArrayList<TreeMap<String,Object>> param4 = new ArrayList<>();
        instance.connect(param);
        TreeMap<Integer, Integer> expResult1 = new TreeMap<>();
        expResult1.put(0,7);
        TreeMap<Integer, Integer> result1 = instance.saveBatch(param1, param2, param3, param4);
        assertEquals(expResult1, result1);
        
        Logger logger2= Logger.getLogger("TriggerDb");
        ConnectionManager instance2 = new ConnectionManager(logger); 
        InitInfo paramII = new InitInfo();
        Integer param3II = -7;
        instance.connect(paramII);
        TreeMap<Integer, Integer> expResult2 = new TreeMap<>();
        expResult2.put(0,0);
        TreeMap<Integer, Integer> result2 = instance.saveBatch(param1, param2, param3II, param4);
        assertEquals(expResult2, result2);
    }

    @Test
    public void testUpdate() throws Exception {
    }

    @Test
    public void testSetSetter() throws Exception {
    }

    @Test
    public void testPrintSaveStatement() {
    }

    @Test
    public void testGetMaxVersion() throws SQLException {
        System.out.println("getMaxVersion");
        Logger logger= Logger.getLogger("TriggerDb");
        ConnectionManager instance = new ConnectionManager(logger); 
        InitInfo param = new InitInfo();
        String param1 = "HLTTriggerSignature";
        String param2 = "";
        String param3 = "";
        instance.connect(param);
        Integer expResult = 0;
        Integer result = instance.getMaxVersion(param1, param2, param3);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetMaxCompVersion() throws SQLException {
        System.out.println("getMaxCompVersion");
        Logger logger= Logger.getLogger("TriggerDb");
        ConnectionManager instance = new ConnectionManager(logger); 
        InitInfo param = new InitInfo();
        String param1 = "";
        String param2 = "";
        instance.connect(param);
        Integer expResult = 0;
        Integer result = instance.getMaxCompVersion(param1, param2);
        assertEquals(expResult, result);
    }

    @Test
    public void testFindComponent_int_String() throws SQLException {
        System.out.println("findComponent");
        Logger logger= Logger.getLogger("TriggerDb");
        ConnectionManager instance = new ConnectionManager(logger); 
        InitInfo param = new InitInfo();
        Integer param1 = 0;
        String param2 = "";
        instance.connect(param);
        Integer expResult = -1;
        Integer result = instance.findComponent(param1, param2);
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_IDs_ArrayList() throws SQLException {
        System.out.println("get_IDs");
        Logger logger= Logger.getLogger("TriggerDb");
        ConnectionManager instance = new ConnectionManager(logger); 
        InitInfo param = new InitInfo();
        ArrayList input = new ArrayList<>();
        instance.connect(param);
        ArrayList<Integer> expResult = new ArrayList<>();
        ArrayList<Integer> result = instance.get_IDs(input);
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_IDs_5args_1() throws Exception {
    }

    @Test
    public void testGet_IDs_5args_2() throws Exception {
    }

    @Test
    public void testGet_hashIDs() throws Exception {
        System.out.println("get_hashIDs");
        Logger logger = Logger.getLogger("TriggerDb");
        ConnectionManager instance = new ConnectionManager(logger); 
        InitInfo param = new InitInfo();
        String param1 = "";
        String param2 = "";
        Integer param3 = 7;
        Boolean param4 = true;
        instance.connect(param);
        ArrayList<Integer> expResult = new ArrayList<>();
        ArrayList<Integer> result = instance.get_hashIDs(param1, param2, param3, param4);
        assertEquals(expResult, result);
        
        Logger logger2 = Logger.getLogger("TriggerDb");
        ConnectionManager instance2 = new ConnectionManager(logger2); 
        InitInfo paramII = new InitInfo();
        Boolean param4II = false;
        instance2.connect(paramII);
        ArrayList<Integer> expResult2 = new ArrayList<>();
        ArrayList<Integer> result2 = instance2.get_hashIDs(param1, param2, param3, param4II);
        assertEquals(expResult2, result2);
    }

    @Test
    public void testGet_allIDs() throws Exception {
        System.out.println("get_allIDs");
        Logger logger = Logger.getLogger("TriggerDb");
        ConnectionManager instance = new ConnectionManager(logger); 
        InitInfo param = new InitInfo();
        String param1 = "";
        String param2 = "";
        instance.connect(param);
        ArrayList<Integer> expResult = new ArrayList<>();
        ArrayList<Integer> result = instance.get_allIDs(param1, param2);
        assertEquals(expResult, result);
    }

    @Test
    public void testFindComponent_3args() throws SQLException {
        System.out.println("findComponent_3args");
        Logger logger= Logger.getLogger("TriggerDb");
        ConnectionManager instance = new ConnectionManager(logger); 
        InitInfo param = new InitInfo();
        Integer param1 = 0;
        String param2 = "a string";
        String param3 = "a different string";
        instance.connect(param);
        Integer expResult = -1;
        Integer result = instance.findComponent(param1, param2, param3);
        assertEquals(expResult, result);
    }

    @Test
    public void testFindComponents() throws Exception {
        System.out.println("findComponents");
        Logger logger= Logger.getLogger("TriggerDb");
        ConnectionManager instance = new ConnectionManager(logger); 
        InitInfo param = new InitInfo();
        Integer param1 = 0;
        ArrayList<String> param2 = new ArrayList<>();
        param2.add("a string");
        ArrayList<String> param3 = new ArrayList<>();
        param3.add("a different string");
        instance.connect(param);
        ArrayList<Integer> expResult = new ArrayList<>();
        expResult.add(-1);
        ArrayList<Integer> result = instance.findComponents(param1, param2, param3);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetConstrainedItemList() throws SQLException {
        System.out.println("getConstrainedItemList");
        Logger logger= Logger.getLogger("TriggerDb");
        ConnectionManager instance = new ConnectionManager(logger); 
        InitInfo param = new InitInfo();
        String param1 = "";
        String param2 = "";
        String param3 = "";
        String param4 = "";
        Integer param5 = 0;
        instance.connect(param);
        TreeMap<Integer,Integer> expResult = new TreeMap<>();
        TreeMap<Integer,Integer> result = instance.getConstrainedItemList(param1,param2,param3,param4,param5);
        assertEquals(expResult, result);
        
    }

    @Test
    public void testGet_ItemList() throws SQLException {
        System.out.println("get_ItemList");
        Logger logger= Logger.getLogger("TriggerDb");
        ConnectionManager instance = new ConnectionManager(logger); 
        InitInfo param = new InitInfo();
        String param1 = "";
        String param2 = "";
        String param3 = "";
        String param4 = "";
        Integer param5 = 0;
        instance.connect(param);
        ArrayList<Integer> expResult = new ArrayList<>();
        ArrayList<Integer> result = instance.get_ItemList(param1,param2,param3,param4,param5);
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_CompactItemList() throws SQLException {
        System.out.println("get_CompactItemList");
        Logger logger= Logger.getLogger("TriggerDb");
        ConnectionManager instance = new ConnectionManager(logger); 
        InitInfo param = new InitInfo();
        String param1 = "";
        String param2 = "";
        String param3 = "";
        String param4 = "";
        ArrayList<Integer> param5 = new ArrayList<>();
        param5.add(3);
        param5.add(4);
        instance.connect(param);
        ArrayList<int[]> expResult = new ArrayList<>();
        ArrayList<int[]> result = instance.get_CompactItemList(param1,param2,param3,param4,param5);
        assertEquals(expResult, result);
    }

    @Test
    public void testDblog() {
    }

    @Test
    public void testMatchingConnectionSignatureIDs() throws Exception {
        System.out.println("matchingConnectionSignatureIDs");
        Logger logger= Logger.getLogger("TriggerDb");
        ConnectionManager instance = new ConnectionManager(logger); 
        InitInfo param = new InitInfo();
        ArrayList<Integer> param1 = new ArrayList<>();
        ArrayList<Integer> param2 = new ArrayList<>();
        ArrayList<Integer> param3 = new ArrayList<>();
        param1.add(3);
        param1.add(4);
        param2.add(3);
        param2.add(4);
        param3.add(3);
        param3.add(4);
        instance.connect(param);
        ArrayList<Integer> expResult = new ArrayList<>();
        ArrayList<Integer> result = instance.matchingConnectionSignatureIDs(param1,param2,param3);
        assertEquals(expResult, result);
        
        Logger logger2 = Logger.getLogger("TriggerDb");
        ConnectionManager instance2 = new ConnectionManager(logger2); 
        InitInfo paramII = new InitInfo();
        ArrayList<Integer> param1II = new ArrayList<>();
        ArrayList<Integer> param2II = new ArrayList<>();
        ArrayList<Integer> param3II = new ArrayList<>();
        instance.connect(paramII);
        ArrayList<Integer> expResult2 = new ArrayList<>();
        ArrayList<Integer> result2 = instance2.matchingConnectionSignatureIDs(param1II,param2II,param3II);
        assertEquals(expResult2, result2);
    }

    @Test
    public void testMatchingElementIDs() throws SQLException {
        System.out.println("matchingElementIDs");
        Logger logger= Logger.getLogger("TriggerDb");
        ConnectionManager instance = new ConnectionManager(logger); 
        InitInfo param = new InitInfo();
        ArrayList<Integer> param1 = new ArrayList<>();
        param1.add(3);
        param1.add(4);
        instance.connect(param);
        ArrayList<Integer> expResult = new ArrayList<>();
        ArrayList<Integer> result = instance.matchingElementIDs(param1);
        assertEquals(expResult, result);
        
        Logger logger2 = Logger.getLogger("TriggerDb");
        ConnectionManager instance2 = new ConnectionManager(logger2); 
        InitInfo paramII = new InitInfo();
        ArrayList<Integer> param1II = new ArrayList<>();
        instance.connect(paramII);
        ArrayList<Integer> expResult2 = new ArrayList<>();
        ArrayList<Integer> result2 = instance2.matchingElementIDs(param1II);
        assertEquals(expResult2, result2);
        
    }

    @Test
    public void testFix_schema_name() throws SQLException {
        System.out.println("fix_schema_name");
        Logger logger= Logger.getLogger("TriggerDb");
        ConnectionManager instance = new ConnectionManager(logger); 
        InitInfo param = new InitInfo();
        String input = "a string";
        instance.connect(param);
        String expResult = "a string";
        String result = instance.fix_schema_name(input);
        assertEquals(expResult, result);
       
        param.setSchemaName("a name");
        instance.connect(param); 
        String result2 = instance.fix_schema_name(input);
        assertEquals(expResult, result2);
       
        
    }

    @Test
    public void testLoadSchemaVersion() throws SQLException {
        
    }

    @Test
    public void testCheckDBVersionCompatible() {
    }

    //@Test
    public void testIsDBVersionCompatible() throws SQLException {
        System.out.println("isDBVersionCompatible");
        Logger logger= Logger.getLogger("TriggerDb");
        ConnectionManager instance = new ConnectionManager(logger); 
        InitInfo param = new InitInfo();
        Integer input = 25;
        String input2 = " a string";
        instance.connect(param);
        instance.setDBSchemaversion(27);
        Integer expResult = 0;
        Integer result = instance.isDBVersionCompatible(input, input2);
        assertEquals(expResult, result);   
    }

    @Test
    public void testGetDBVersionWarningMsg() throws SQLException {
        System.out.println("getDBVersionWarningMsg");
        Logger logger= Logger.getLogger("TriggerDb");
        ConnectionManager instance = new ConnectionManager(logger); 
        InitInfo param = new InitInfo();
        instance.connect(param);
        String expResult = "";
        String result = instance.getDBVersionWarningMsg(1);
        assertEquals(expResult, result); 
        
        String expResult2 = "Trigger DB is incompatible with the TriggerTool";
        String result2 = instance.getDBVersionWarningMsg(0);
        assertEquals(expResult2, result2); 
        
        String expResult3 = "TriggerDB does not support L1 MET significance parameters";
        String result3 = instance.getDBVersionWarningMsg(-1);
        assertEquals(expResult3, result3); 
        
        String expResult4 = "TriggerDB does not support Prescale Set Alias";
        String result4 = instance.getDBVersionWarningMsg(-2);
        assertEquals(expResult4, result4); 
        
        String expResult5 = "Unknown warning - contact trigger tool authors";
        String result5 = instance.getDBVersionWarningMsg(-3);
        assertEquals(expResult5, result5); 
    }

    @Test
    public void testClobToString() throws SQLException {
        //Dereferencing null pointer; to revisit
    }

    @Test
    public void testCheckTopoExisting() throws SQLException {
        System.out.println("checkTopoExisting");
        Logger logger= Logger.getLogger("TriggerDb");
        ConnectionManager instance = new ConnectionManager(logger); 
        InitInfo param = new InitInfo();
        ArrayList<String> param1 = new ArrayList<>();
        String param2 = "a string";
        String param3 = "a different string";
        String param4 = "a different different string";
        instance.connect(param);
        ArrayList<AbstractMap.SimpleEntry<Integer, Integer>> expResult = null;
        ArrayList<AbstractMap.SimpleEntry<Integer, Integer>> result = instance.checkTopoExisting(param1,param2,param3,param4);
        assertEquals(expResult, result);
        
        //Null pointer :/
        //Logger logger2= Logger.getLogger("TriggerDb");
        //ConnectionManager instance2 = new ConnectionManager(logger2); 
       // MockResultSet resultset = resultSetHandler.createResultSet();
       /// setEmptyInitInfo();
        //InitInfo paramII= new InitInfo();
       // ArrayList<String> param1II = new ArrayList<>();
       // param1II.add("");
       // param1II.add("");
       // String param2II = "a string";
        //String param3II = "a different string";
       // String param4II = "a different different string";
       // instance.connect(paramII);
       // ArrayList<AbstractMap.SimpleEntry<Integer, Integer>> expResultII = new ArrayList();
       // ArrayList<AbstractMap.SimpleEntry<Integer, Integer>> resultII = instance2.checkTopoExisting(param1II,param2II,param3II,param4II);
       // assertEquals(expResultII, resultII);
    }

    @Test
    public void testCheckTopoExistingMulti() throws SQLException {
        System.out.println("checkTopoExistingMulti");
        Logger logger= Logger.getLogger("TriggerDb");
        ConnectionManager instance = new ConnectionManager(logger); 
        InitInfo param = new InitInfo();
        ArrayList<String> param1 = new ArrayList<>();
        String param2 = "a string";
        String param3 = "a different string";
        String param4 = "a different different string";
        instance.connect(param);
        TreeMap<Integer, ArrayList<Integer>> expResult = null;
        TreeMap<Integer, ArrayList<Integer>> result = instance.checkTopoExistingMulti(param1,param2,param3,param4);
        assertEquals(expResult, result);
       
    }

    @Test
    public void testCheckLinkExisting() throws SQLException {
        System.out.println("checkLinkExisting");
        Logger logger= Logger.getLogger("TriggerDb");
        ConnectionManager instance = new ConnectionManager(logger); 
        InitInfo param = new InitInfo();
        ArrayList<String> param1 = new ArrayList<>();
        String param2 = "a string";
        String param3 = "a different string";
        String param4 = "a different different string";
        instance.connect(param);
        ArrayList<Integer> expResult = new ArrayList();
        ArrayList<Integer> result = instance.checkLinkExisting(param1,param2,param3,param4);
        assertEquals(expResult, result);
        
        Logger logger2= Logger.getLogger("TriggerDb");
        ConnectionManager instance2 = new ConnectionManager(logger2); 
        InitInfo paramII = new InitInfo();
        ArrayList<String> param1II = new ArrayList<>();
        param1II.add("a string");
        String param2II = "a string";
        String param3II = "a different string";
        String param4II = "a different different string";
        instance2.connect(paramII);
        ArrayList<Integer> expResult2 = new ArrayList();
        ArrayList<Integer> result2 = instance.checkLinkExisting(param1II,param2II,param3II,param4II);
        assertEquals(expResult2, result2);
    }

    @Test
    public void testGetLinkedTable() {
    }

    @Test
    public void testExecuteSelect() throws Exception {
    }

    @Test
    public void testExecuteSelectCheck() throws Exception {
    //need initialisation
    }

    @Test
    public void testExecuteInsert() throws Exception {
    }
    
}
