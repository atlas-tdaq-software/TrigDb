/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Connections;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import com.mockrunner.mock.jdbc.MockResultSet;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.Entities.HLT.HLTMaster;
import triggerdb.Entities.HLT.HLTRelease;
import triggerdb.Entities.L1.L1Item;
import triggerdb.Entities.L1.L1Master;
import triggerdb.Entities.SMT.SuperMasterTable;
import triggerdb.Entities.Topo.TopoMaster;
import triggerdb.Entities.alias.HLTPrescaleSetAliasLumi;
import triggerdb.Entities.alias.L1PrescaleSetAliasLumi;
import triggerdb.Entities.alias.PrescaleSetAlias;
import triggerdb.PrescaleSetAliasComponent;
/**
 *
 * @author davethomas
 */
public class InitInfoTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public InitInfoTest() {
    }
    
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
        ConnectionManager.getInstance().unsetTestingConnection();
    }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
    }
    @Test
    public void testGetUserMode() {
    }

    @Test
    public void testSetUserMode() {
        System.out.println("setUserMode");
        InitInfo instance = new InitInfo(); 
        UserMode param = UserMode.EXPERT;
        instance.setUserMode(param);
        UserMode expResult = param;
        Object result = instance.getUserMode();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetTechnology() {
    }

    @Test
    public void testSetTechnology() {
        System.out.println("setTechnology");
        InitInfo instance = new InitInfo(); 
        DBTechnology param = DBTechnology.ORACLE;
        instance.setTechnology(param);
        DBTechnology expResult = param;
        Object result = instance.getTechnology();
        assertEquals(expResult, result);
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        InitInfo instance = new InitInfo();
        ArrayList<Object> expResults = new ArrayList<>();
        expResults.add("technology: unknown\nschema    : \nuser name : \nserver    : \nhost      : \nmode      : Unknown\ndblookup  : null\ndbauth    : null\nonline    : false\nonlineDB  : false\n");
        expResults.add("technology: unknown\nschema    : \nuser name : \nserver    : \nhost      : \nmode      : Unknown\ndblookup  : /eos/user/a/atdaqeos/adm/db\ndbauth    : /eos/user/a/atdaqeos/adm/db\nonline    : false\nonlineDB  : false\n");
        expResults.add("technology: unknown\nschema    : \nuser name : \nserver    : \nhost      : \nmode      : Unknown\ndblookup  : /afs/cern.ch/atlas/project/tdaq/databases/.coral\ndbauth    : /afs/cern.ch/atlas/project/tdaq/databases/.coral\nonline    : false\nonlineDB  : false\n");

        Object result = instance.toString();
        assertTrue(expResults.contains(result));
    }

    @Test
    public void testSetOnline() {
        System.out.println("setOnline");
        InitInfo instance = new InitInfo(); 
        Boolean param = true;
        instance.setOnline(param);
        Boolean expResult = param;
        Object result = instance.getOnline();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetOnline() {
    }

    @Test
    public void testSetOnlineDB() {
        System.out.println("setOnlineDB");
        InitInfo instance = new InitInfo(); 
        Boolean param = true;
        instance.setOnlineDB(param);
        Boolean expResult = param;
        Object result = instance.getOnlineDB();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetOnlineDB() {
    }

    @Test
    public void testSetgoodInfo() {
        System.out.println("setgoodInfo");
        InitInfo instance = new InitInfo(); 
        Boolean param = true;
        instance.setgoodInfo(param);
        Boolean expResult = param;
        Object result = instance.getgoodInfo();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetUserName() {
        System.out.println("setUserName");
        InitInfo instance = new InitInfo(); 
        String param = "a string";
        instance.setUserName(param);
        String expResult = "a string";
        Object result = instance.getUserName();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetPassWord() {
        System.out.println("setPassWord");
        InitInfo instance = new InitInfo(); 
        String param = "a string";
        instance.setPassWord(param);
        String expResult = "a string";
        Object result = instance.getPassWord();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetTechnologyFromString() {
        System.out.println("setTechnologyFromString");
        InitInfo instance = new InitInfo(); 
        String param = "Oracle";
        instance.setTechnologyFromString(param);
        DBTechnology expResult = DBTechnology.ORACLE;
        Object result = instance.getTechnology();
        assertEquals(expResult, result);
        
        InitInfo instance2 = new InitInfo(); 
        String param2 = "Nonsense";
        instance2.setTechnologyFromString(param2);
        DBTechnology expResult2 = DBTechnology.UNKNOWN;
        Object result2 = instance2.getTechnology();
        assertEquals(expResult2, result2);
    }

    @Test
    public void testSetdbHost() {
        System.out.println("setdbHost");
        InitInfo instance = new InitInfo(); 
        String param = "a string";
        instance.setdbHost(param);
        String expResult = "a string";
        Object result = instance.getdbHost();
        assertEquals(expResult, result); 
    }

    @Test
    public void testSetdbServer() {
    }

    @Test
    public void testSetSchemaName() {
        System.out.println("setSchemaName");
        InitInfo instance = new InitInfo(); 
        String param = "a string";
        instance.setSchemaName(param);
        String expResult = "a string";
        Object result = instance.getSchemaName();
        assertEquals(expResult, result); 
    }

    @Test
    public void testGetConnUserPwFromSvcName() throws Exception {
    }

    @Test
    public void testSetConnectionFromCommandLine() throws Exception {
    }

    @Test
    public void testGetURL() {
    }

    @Test
    public void testGetgoodInfo() {
    }

    @Test
    public void testGetUserName() {
    }

    @Test
    public void testGetPassWord() {
    }

    @Test
    public void testGetdbHost() {
    }

    @Test
    public void testGetdbServer() {
    }

    @Test
    public void testGetSchemaName() {
    }

    @Test
    public void testGetSchemaModifier() {
    }

    @Test
    public void testResetAll() {
        System.out.println("resetAll");
        InitInfo instance = new InitInfo(); 
        instance.resetAll();
        UserMode expResult1 = UserMode.UNKNOWN;
        String expResult2 = "";
        String expResult3 = "";
        DBTechnology expResult4 = DBTechnology.UNKNOWN;
        String expResult5 = "";
        Object result1 = instance.getUserMode();
        Object result2 = instance.getUserName();
        Object result3 = instance.getPassWord();
        Object result4 = instance.getTechnology();
        Object result5 = instance.getdbServer();
        assertEquals(expResult1, result1); 
        assertEquals(expResult2, result2); 
        assertEquals(expResult3, result3); 
        assertEquals(expResult4, result4); 
        assertEquals(expResult5, result5); 
    }

    @Test
    public void testSetSystemUsername() {
        System.out.println("setSystemUsername");
        InitInfo instance = new InitInfo(); 
        String param = "a string";
        instance.setSystemUsername(param);
        String expResult = "a string";
        Object result = instance.getSystemUsername();
        assertEquals(expResult, result); 
    }

    @Test
    public void testGetSystemUsername() {
    }

    //@Test
    public void testGetSystemHome() {
        System.out.println("getSystemHome");
        InitInfo instance = new InitInfo(); 
        String expResult = "/Users/davethomas";
        Object result = instance.getSystemHome();
        assertEquals(expResult, result); 
    }

    @Test
    public void testSetOnlineUserPassword() {
        System.out.println("setOnlineUserPassword");
        InitInfo instance = new InitInfo(); 
        String param = "a string";
        instance.setOnlineUserPassword(param);
        String expResult = "a string";
        Object result = instance.getOnlineUserPassword();
        assertEquals(expResult, result); 
    }

    @Test
    public void testGetOnlineUserPassword() {
    }

    @Test
    public void testSetAtlrRPassword() {
        System.out.println("setAtlrRPassword");
        InitInfo instance = new InitInfo(); 
        String param = "a string";
        instance.setAtlrRPassword(param);
        String expResult = "a string";
        Object result = instance.getAtlrRPassword();
        assertEquals(expResult, result); 
    }

    @Test
    public void testGetAtlrRPassword() {
    }

    @Test
    public void testSetAtonrRPassword() {
        System.out.println("setAtonrRPassword");
        InitInfo instance = new InitInfo(); 
        String param = "a string";
        instance.setAtonrRPassword(param);
        String expResult = "a string";
        Object result = instance.getAtonrRPassword();
        assertEquals(expResult, result); 
    }

    @Test
    public void testGetAtonrRPassword() {
    }

    @Test
    public void testSetAtonrWPassword() {
        System.out.println("setAtonrWPassword");
        InitInfo instance = new InitInfo(); 
        String param = "a string";
        instance.setAtonrWPassword(param);
        String expResult = "a string";
        Object result = instance.getAtonrWPassword();
        assertEquals(expResult, result); 
    }

    @Test
    public void testGetAtonrWPassword() {
    }

    @Test
    public void testEncryptAtonrPassword() {
        System.out.println("encryptAtonrPassword");
        InitInfo instance = new InitInfo(); 
        String expResult = "2jmj7l5rSw0yVb/vlWAYkK/YBwk=";
        Object result = instance.encryptAtonrPassword();
        assertEquals(expResult, result); 
    }

    @Test
    public void testEncrypt() {
        System.out.println("encrypt");
        InitInfo instance = new InitInfo(); 
        String input = "a string";
        String expResult = "VV0B5sgyZrPp+SvYEZBTcMr2J3A=";
        Object result = instance.encrypt(input);
        assertEquals(expResult, result); 
    }

    @Test
    public void testIsOnlineExpert() {
        //null pointer
    }

    @Test
    public void testSetModeString() {
        System.out.println("setModeString");
        InitInfo instance = new InitInfo(); 
        String input1 = "UsEr";
        String input2 = "sHIfTEr";
        String input3 = "ExPeRt";
        String input4 = "dbA";
        String input5 = "l1EXPERT";
        String input6 = "hltexpert";
        UserMode expResult1 = UserMode.USER;
        UserMode expResult2 = UserMode.SHIFTER;
        UserMode expResult3 = UserMode.EXPERT;
        UserMode expResult4 = UserMode.DBA;
        UserMode expResult5 = UserMode.L1EXPERT;
        UserMode expResult6 = UserMode.HLTEXPERT;
        instance.setModeString(input1);
        Object result1 = instance.getUserMode();
        assertEquals(expResult1, result1); 
        instance.setModeString(input2);
        Object result2 = instance.getUserMode();
        assertEquals(expResult2, result2); 
        instance.setModeString(input3);
        Object result3 = instance.getUserMode();
        assertEquals(expResult3, result3); 
        instance.setModeString(input4);
        Object result4 = instance.getUserMode();
        assertEquals(expResult4, result4); 
        instance.setModeString(input5);
        Object result5 = instance.getUserMode();
        assertEquals(expResult5, result5); 
        instance.setModeString(input6);
        Object result6 = instance.getUserMode();
        assertEquals(expResult6, result6); 
    }

    @Test
    public void testIsCommandLine() {
    }

    @Test
    public void testSetIsCommandLine() {
        System.out.println("setIsCommandLine");
        InitInfo instance = new InitInfo(); 
        Boolean param = true;
        instance.SetIsCommandLine(param);
        Boolean expResult = true;
        Object result = instance.IsCommandLine();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetDbMinimunSchema() {
    }

    @Test
    public void testSetDbMinimunSchema() {
        System.out.println("setDbMinimunSchema");
        InitInfo instance = new InitInfo(); 
        Integer param = 2712;
        instance.setDbMinimunSchema(param);
        Integer expResult = 2712;
        Object result = instance.getDbMinimunSchema();
        assertEquals(expResult, result);
    }
    
}
