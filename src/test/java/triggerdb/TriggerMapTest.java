/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import com.mockrunner.mock.jdbc.MockResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.Entities.HLT.HLTMaster;
import triggerdb.Entities.HLT.HLTRelease;
import triggerdb.Entities.L1.L1Item;
import triggerdb.Entities.L1.L1Master;
import triggerdb.Entities.SMT.SuperMasterTable;
import triggerdb.Entities.Topo.TopoMaster;
import triggerdb.Entities.alias.HLTPrescaleSetAliasLumi;
import triggerdb.Entities.alias.L1PrescaleSetAliasLumi;
import triggerdb.Entities.alias.PrescaleSetAlias;
import triggerdb.PrescaleSetAliasComponent;

/**
 *
 * @author davethomas
 */
public class TriggerMapTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public TriggerMapTest() {
    }
    
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
        ConnectionManager.getInstance().unsetTestingConnection();
    }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
    }

    @Test
    public void testPutFirst() {
    }

    @Test
    public void testGetSearchData() {
    }


    @Test
    public void testGetRowData() {
        System.out.println("getRowData");
        TriggerMap instance = new TriggerMap();     
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        TreeMap<String, Object> expResult = new TreeMap<>();
        instance.setHuman("key1", "name1");
        instance.setHuman("key2", "name2");
        TreeMap<String, Object> result = instance.getRowData();
        instance.setRowData(result, "key", "name");
        expResult.put("key","name");
        assertEquals(expResult, result);
        
        TriggerMap instance2 = new TriggerMap();     
        TreeMap<String, Object> expResult2 = new TreeMap<>();
        instance.setHuman("key1", "name1");
        instance.setHuman("key2", "name2");
        TreeMap<String, Object> result2 = instance2.getRowData();
        instance.setRowData(result2, "key1", "name1");
        expResult2.put("key1","name1");
        assertEquals(expResult2, result2);
    }

    


    @Test
    public void testGetHuman() {
        System.out.println("getHuman");
        TriggerMap instance = new TriggerMap();
        String input_key = "a key";
        String input_str = "a str";
        instance.setHuman(input_key, input_str);
        String expResult = "a str";
        Object result = instance.getHuman(input_key);
        assertEquals(expResult, result);
    }
    
}
