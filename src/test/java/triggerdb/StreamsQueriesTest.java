/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import com.mockrunner.mock.jdbc.MockResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.Entities.HLT.HLTMaster;
import triggerdb.Entities.HLT.HLTRelease;
import triggerdb.Entities.L1.L1Item;
import triggerdb.Entities.L1.L1Master;
import triggerdb.Entities.SMT.SuperMasterTable;
import triggerdb.Entities.Topo.TopoMaster;
import triggerdb.Entities.alias.HLTPrescaleSetAliasLumi;
import triggerdb.Entities.alias.L1PrescaleSetAliasLumi;
import triggerdb.Entities.alias.PrescaleSetAlias;
import triggerdb.PrescaleSetAliasComponent;

/**
 *
 * @author davethomas
 */
public class StreamsQueriesTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public StreamsQueriesTest() {
    }
    
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
        ConnectionManager.getInstance().unsetTestingConnection();
    }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
    }

    @Test
    public void testGetIds() throws Exception {
        System.out.println("getIds");
        StreamsQueries instance = new StreamsQueries();     
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        resultset.addColumn("Column 1");
        resultset.addColumn("Column 2");
        List<Object> intlist = new ArrayList<>();
        intlist.add(2);
        intlist.add(10);
        intlist.add(25);
        intlist.add(100);
        resultset.addRow(intlist);
        List expResult = new ArrayList<Integer>();
        Integer param1 = 255;
        L1Item param2 = new L1Item();
        L1Item param3 = new L1Item();
        param3.set_id(511);
        List<L1Item> paramlist = new ArrayList<L1Item>();
        paramlist.add(param2);
        paramlist.add(param3);
        Object result = instance.GetIds(param1, paramlist);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetTypeBit() throws Exception {
        System.out.println("getTypeBit");
        StreamsQueries instance = new StreamsQueries();     
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        String expResult = "";
        Integer param = 255;
        Object result = instance.getTypeBit(param);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetTypeBitMask() throws Exception {
        System.out.println("getTypeBitMask");
        StreamsQueries instance = new StreamsQueries();     
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        String expResult = "";
        Integer param = 255;
        Object result = instance.getTypeBitMask(param);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetInvertMask() throws Exception {
        System.out.println("getInvertMask");
        StreamsQueries instance = new StreamsQueries();     
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        String expResult = "";
        Integer param = 255;
        Object result = instance.getInvertMask(param);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetAlgoIds() throws Exception {
        System.out.println("getAlgoIds");
        StreamsQueries instance = new StreamsQueries();     
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        List expResult = new ArrayList<Integer>();
        Integer param = 255;
        Object result = instance.GetAlgoIds(param);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetL1TriggerItems() throws Exception {
        System.out.println("getL1TriggerItems");
        StreamsQueries instance = new StreamsQueries();     
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        List expResult = new ArrayList<L1Item>();
        Integer param = 255;
        Object result = instance.GetL1TriggerItems(param);
        assertEquals(expResult, result);
    }
    
}
