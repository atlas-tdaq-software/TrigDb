/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import com.mockrunner.mock.jdbc.MockResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.Entities.HLT.HLTMaster;
import triggerdb.Entities.HLT.HLTRelease;
import triggerdb.Entities.L1.L1Master;
import triggerdb.Entities.L1.L1Prescale;
import triggerdb.Entities.SMT.SuperMasterTable;
import triggerdb.Entities.Topo.TopoMaster;
import triggerdb.Entities.alias.HLTPrescaleSetAliasLumi;
import triggerdb.Entities.alias.L1PrescaleSetAliasLumi;
import triggerdb.Entities.alias.PrescaleSetAlias;
import triggerdb.PrescaleSetAliasComponent;

/**
 *
 * @author davethomas
 */
public class PrescaleSetAliasLumiTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public PrescaleSetAliasLumiTest() {
    }
    
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
        ConnectionManager.getInstance().unsetTestingConnection();
    }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
    }


    @Test
    public void testGet_l1_prescale_set_alias() {
        System.out.println("get_l1_prescale_set_alias");
        PrescaleSetAliasLumi instance = new PrescaleSetAliasLumi();
        L1PrescaleSetAliasLumi expResult = new L1PrescaleSetAliasLumi();
        Object Result = instance.get_l1_prescale_set_alias();
        assertEquals(expResult, Result);
    }

    @Test
    public void testGet_hlt_prescale_set_alias() {
        System.out.println("get_hlt_prescale_set_alias");
        PrescaleSetAliasLumi instance = new PrescaleSetAliasLumi();
        HLTPrescaleSetAliasLumi expResult = new HLTPrescaleSetAliasLumi();
        Object Result = instance.get_hlt_prescale_set_alias();
        assertEquals(expResult, Result);
    }

    @Test
    public void testGet_alias_id() {
        System.out.println("get_alias_id");
        PrescaleSetAliasLumi instance = new PrescaleSetAliasLumi();
        Integer expResult = -1;
        Object Result = instance.get_alias_id();
        assertEquals(expResult, Result);
    }

    @Test
    public void testSet_alias_id() {
    }

    @Test
    public void testGet_alias() {
        System.out.println("get_alias");
        PrescaleSetAliasLumi instance = new PrescaleSetAliasLumi();
        PrescaleSetAlias expResult = null;
        Object Result = instance.get_alias();
        assertEquals(expResult, Result);
        
    }

    @Test
    public void testSet_alias_PrescaleSetAliasComponent() {
        System.out.println("set_alias_PrescaleSetAliasComponent");
        PrescaleSetAliasLumi instance = new PrescaleSetAliasLumi();
        PrescaleSetAliasComponent input = new PrescaleSetAliasComponent();
        PrescaleSetAlias expResult = new PrescaleSetAlias();
        instance.set_alias(input);
        Object Result = instance.get_alias();
        assertEquals(expResult, Result);
    }

    @Test
    public void testSet_alias_PrescaleSetAlias() {
        System.out.println("set_alias_PrescaleSetAlias");
        PrescaleSetAliasLumi instance = new PrescaleSetAliasLumi();
        PrescaleSetAlias input = new PrescaleSetAlias();
        PrescaleSetAlias expResult = new PrescaleSetAlias();
        instance.set_alias(input);
        Object Result = instance.get_alias();
        assertEquals(expResult, Result);
    }

    @Test
    public void testIsDdefault() {
        System.out.println("isDdefault");
        PrescaleSetAliasLumi instance = new PrescaleSetAliasLumi();
        Boolean expResult = false;
        Object Result = instance.isDdefault();
        assertEquals(expResult, Result);
    }

    @Test
    public void testGet_default() {
        System.out.println("get_default");
        PrescaleSetAliasLumi instance = new PrescaleSetAliasLumi();
        Boolean expResult = false;
        Object Result = instance.get_default();
        assertEquals(expResult, Result);
    }

    @Test
    public void testSet_default() {
        System.out.println("set_default");
        PrescaleSetAliasLumi instance = new PrescaleSetAliasLumi();
        instance.set_default(true);
        Boolean expResult = true;
        Object Result = instance.get_default();
        assertEquals(expResult, Result);
    }

    @Test
    public void testGet_lum_max() {
        System.out.println("get_lum_max");
        PrescaleSetAliasLumi instance = new PrescaleSetAliasLumi();
        String expResult = "~";
        Object Result = instance.get_lum_max();
        assertEquals(expResult, Result);
    }

    @Test
    public void testSet_lum_max() {
        System.out.println("set_lum_max");
        PrescaleSetAliasLumi instance = new PrescaleSetAliasLumi();
        String expResult = "1.0";
        instance.set_lum_max(expResult);
        Object Result = instance.get_lum_max();
        assertEquals(expResult, Result);
    }

    @Test
    public void testGet_lum_min() {
        System.out.println("get_lum_min");
        PrescaleSetAliasLumi instance = new PrescaleSetAliasLumi();
        String expResult = "~";
        Object Result = instance.get_lum_min();
        assertEquals(expResult, Result);
    }

    @Test
    public void testSet_lum_min() {
        System.out.println("set_lum_mim");
        PrescaleSetAliasLumi instance = new PrescaleSetAliasLumi();
        String expResult = "1.0";
        instance.set_lum_min(expResult);
        Object Result = instance.get_lum_min();
        assertEquals(expResult, Result);
    }

    @Test
    public void testGet_comment() {
        System.out.println("get_comment");
        PrescaleSetAliasLumi instance = new PrescaleSetAliasLumi();
        String expResult = "~";
        Object Result = instance.get_comment();
        assertEquals(expResult, Result);
    }

    @Test
    public void testSet_comment() {
        System.out.println("set_comment");
        PrescaleSetAliasLumi instance = new PrescaleSetAliasLumi();
        String expResult = "a string";
        instance.set_comment(expResult);
        Object Result = instance.get_comment();
        assertEquals(expResult, Result);
    }

    @Test
    public void testSave() throws SQLException {
    //null pointer
    }

    @Test
    public void testCompareTo() {
        //null pointer
    }

    @Test
    public void testToString() {
       //null pointer
    }

    @Test
    public void testSet_l1_prescale_set() throws Exception {
        //null pointer
    }

    @Test
    public void testSet_hlt_prescale_set() {
    }

    @Test
    public void testUpdate() {
    }

    @Test
    public void testDelete() throws Exception {
    }
    
}
