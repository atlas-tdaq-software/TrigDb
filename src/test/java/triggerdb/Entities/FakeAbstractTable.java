package triggerdb.Entities;

import java.sql.SQLException;

/**
 * Created by adambozson on 04/07/2016.
 *
 * Since AbstractTable is abstract, we have to subclass it to be able to test an instance.
 *
 */
public class FakeAbstractTable extends AbstractTable {

    public FakeAbstractTable() {
        this("");
    }

    public FakeAbstractTable(String prefix) {
        super(prefix);
        this.set_name("fake_table");
    }
    
    @Override
    public int save() throws SQLException {
        return 0;
    }

    @Override
    public FakeAbstractTable clone() {
        return new FakeAbstractTable();
    }

    @Override
    public String getTableName() {
        return "FAKE_TABLE";
    }
    
    public void setKeyValue(String key, Object value) {
        this.keyValue.putFirst(key, value, key);
    }
    
    public Object getValueFor(String key) {
        return keyValue.get(key);
    }

}
