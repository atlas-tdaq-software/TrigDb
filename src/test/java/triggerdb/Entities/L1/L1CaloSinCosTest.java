/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.L1;

import java.sql.SQLException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Entities.HLT.HLTPrescaleSet;
import triggerdb.Entities.HLT.HLTTriggerGroup;

/**
 *
 * @author davethomas
 */
public class L1CaloSinCosTest {
    
    public L1CaloSinCosTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGet_val() {
        System.out.println("get_val");
        L1CaloSinCos instance = new L1CaloSinCos();
        Integer expResult = -1;
        Object Result = instance.get_val(1);
        assertEquals(expResult,Result);
    }

    @Test
    public void testGet_eta_min() {
        System.out.println("get_eta_min");
        L1CaloSinCos instance = new L1CaloSinCos();
        Integer expResult = -1;
        Object Result = instance.get_eta_min();
        assertEquals(expResult,Result);
    }

    @Test
    public void testGet_eta_max() {
        System.out.println("get_eta_max");
        L1CaloSinCos instance = new L1CaloSinCos();
        Integer expResult = -1;
        Object Result = instance.get_eta_max();
        assertEquals(expResult,Result);
    }

    @Test
    public void testGet_phi_min() {
        System.out.println("get_eta_min");
        L1CaloSinCos instance = new L1CaloSinCos();
        Integer expResult = -1;
        Object Result = instance.get_phi_min();
        assertEquals(expResult,Result);
    }

    @Test
    public void testGet_phi_max() {
        System.out.println("get_phi_max");
        L1CaloSinCos instance = new L1CaloSinCos();
        Integer expResult = -1;
        Object Result = instance.get_phi_max();
        assertEquals(expResult,Result);
    }

    @Test
    public void testSet_eta_min() {
        System.out.println("set_eta_min");
        L1CaloSinCos instance = new L1CaloSinCos();
        Integer input = -2712;
        Integer expResult = -2712;
        instance.set_eta_min(input);
        Object Result = instance.get_eta_min();
        assertEquals(expResult,Result);
    }

    @Test
    public void testSet_eta_max() {
        System.out.println("set_eta_max");
        L1CaloSinCos instance = new L1CaloSinCos();
        Integer input = -2712;
        Integer expResult = -2712;
        instance.set_eta_max(input);
        Object Result = instance.get_eta_max();
        assertEquals(expResult,Result);
    }

    @Test
    public void testSet_phi_min() {
        System.out.println("set_phi_min");
        L1CaloSinCos instance = new L1CaloSinCos();
        Integer input = -2712;
        Integer expResult = -2712;
        instance.set_phi_min(input);
        Object Result = instance.get_phi_min();
        assertEquals(expResult,Result);
    }

    @Test
    public void testSet_phi_max() {
        System.out.println("set_phi_max");
        L1CaloSinCos instance = new L1CaloSinCos();
        Integer input = -2712;
        Integer expResult = -2712;
        instance.set_phi_max(input);
        Object Result = instance.get_phi_max();
        assertEquals(expResult,Result);
    }

    @Test
    public void testSet_val() {
        System.out.println("set_val");
        L1CaloSinCos instance = new L1CaloSinCos();
        Integer input = -2712;
        Integer expResult = -2712;
        instance.set_val(1,input);
        Object Result = instance.get_val(1);
        assertEquals(expResult,Result);
    }

    @Test
    public void testSave() throws Exception {
    }

@Test
    public void testToString() {
        System.out.println("toString");
        L1CaloSinCos instance = new L1CaloSinCos();
        String expResult= "L1 Calo Sin Cos";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
        
        L1CaloSinCos instance2 = getL1CaloSinCosWithID(-2712);
        String expResult2 = "L1 CALO SIN COS: id=-2712, name=, ver=1";
        Object Result2 = instance2.toString();
        assertEquals(expResult2, Result2);
    }

          /**
     * Helper function to make an HLTTE_TE and handle
     * SQLExceptions.
     * @param id
     * @return 
     */
    public L1CaloSinCos getL1CaloSinCosWithID(int id) {
        L1CaloSinCos result = null;
        try {
            result = new L1CaloSinCos(id);
        } catch (SQLException e) {
            fail(e.toString());
        }
        return result;
    }

    
    
    
    
    @Test
    public void testClone() {
        System.out.println("clone");
        L1CaloSinCos instance = new L1CaloSinCos();
        Object expResult = instance;
        Object result = instance.clone();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetTableName() {
        System.out.println("getTableName");
        L1CaloSinCos instance = new L1CaloSinCos();
        String expResult = "L1_CALO_SIN_COS";
        String result = instance.getTableName();
        assertEquals(expResult, result);
    }
    
}
