/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.L1;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import com.mockrunner.mock.jdbc.MockResultSet;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.Entities.HLT.HLTPrescaleSet;
import triggerdb.Entities.L1Links.L1TM_TTM;
import triggerdb.Entities.alias.PrescaleSetAlias;

/**
 *
 * @author dwthomas
 */
public class L1MenuTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public L1MenuTest() {
    }
    
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @After
    public void tearDown() {
    }
    
    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
    }

    @Test
    public void testForceLoad() throws Exception {
    }

    @Test
    public void testLoadThresholdValues() throws Exception {
    }

    @Test
    public void testSet_ctp_files() {
        System.out.println("set_ctp_files");
        L1Menu instance = new L1Menu();
        L1CtpFiles param = new L1CtpFiles();
        instance.set_ctp_files(param);
        Object Result = instance.get_ctp_files();
        Object expResult = param;
        assertEquals(expResult, Result);
    }

    @Test
    public void testSet_ctp_smx() {
        System.out.println("set_ctp_smx");
        L1Menu instance = new L1Menu();
        L1CtpSmx param = new L1CtpSmx();
        instance.set_ctp_smx(param);
        Object Result = instance.get_ctp_smx();
        Object expResult = param;
        assertEquals(expResult, Result);
    }

    @Test
    public void testToString() {        
        System.out.println("toString");
        L1Menu instance = new L1Menu();
        String expResult= "L1 Menu";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
      
        instance.set_id(2712);
        String expResult2 = "L1 MENU: ID=2712, Name=, Version=1";
        Object Result2 = instance.toString();
        assertEquals(expResult2, Result2);
    }

    @Test
    public void testGet_phase() {
        System.out.println("get_phase");
        L1Menu instance = new L1Menu();
        String expResult = "";
        String result = instance.get_phase();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_ctp_safe() {
        System.out.println("get_ctp_safe");
        L1Menu instance = new L1Menu();
        Integer expResult = 0;
        Object result = instance.get_ctp_safe();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_ctp_files_id() {
        System.out.println("get_ctp_files_id");
        L1Menu instance = new L1Menu();
        Integer expResult = -1;
        Object result = instance.get_ctp_files_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_ctp_smx_id() {
        System.out.println("get_ctp_smx_id");
        L1Menu instance = new L1Menu();
        Integer expResult = -1;
        Object result = instance.get_ctp_smx_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_phase() {
        System.out.println("set_phase");
        L1Menu instance = new L1Menu();
        instance.set_phase("a string");
        String expResult = "a string";
        String result = instance.get_phase();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_ctp_safe() {
        System.out.println("set_ctp_safe");
        L1Menu instance = new L1Menu();
        Integer input = 3;
        instance.set_ctp_safe(input);
        Integer expResult = 3;
        Object result = instance.get_ctp_safe();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_ctp_smx_id() {
        System.out.println("set_ctp_smx_id");
        L1Menu instance = new L1Menu();
        Integer input = 3;
        instance.set_ctp_smx_id(input);
        Integer expResult = 3;
        Object result = instance.get_ctp_smx_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_ctp_files_id() {
        System.out.println("set_ctp_files_id");
        L1Menu instance = new L1Menu();
        Integer input = 3;
        instance.set_ctp_files_id(input);
        Integer expResult = 3;
        Object result = instance.get_ctp_files_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testLoadThresholds() throws Exception {
    }

    @Test
    public void testGetItemIds() throws Exception {
        System.out.println("getItemIds");
        L1Menu instance = new L1Menu();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        ArrayList<Integer> expResult = new ArrayList<>();           
        Object Result = instance.getItemIds();
        assertEquals(expResult, Result);
        
        L1Menu instance2 = new L1Menu();
        instance2.set_id(2712);
        ArrayList<Integer> expResult2 = new ArrayList<>(); 
        Object Result2 = instance2.getItemIds();
        assertEquals(expResult2, Result2);
    }

   
    @Test
    public void testGetSetItems() {
        System.out.println("setItems");
        L1Menu instance = new L1Menu();
        L1Item param = new L1Item();
        ArrayList<L1Item> input = new ArrayList();
        input.add(param);
        instance.setItems(input);
        ArrayList<L1Item> expResult = new ArrayList();
        expResult.add(param);
        Object result = instance.getItems();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetForcedThresholds() throws Exception {
        System.out.println("getForcedThresholds");
        L1Menu instance = new L1Menu();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        L1Threshold param = new L1Threshold();
        ArrayList<L1Threshold> expResult = new ArrayList();
        Object result = instance.getForcedThresholds();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetMonitors() throws Exception {
        System.out.println("getMonitors");
        L1Menu instance = new L1Menu();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        L1TM_TTM param = new L1TM_TTM(-1);
        ArrayList<L1TM_TTM> expResult = new ArrayList();
        Object result = instance.getMonitors();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetAliases() throws Exception {
        System.out.println("getAliases");
        L1Menu instance = new L1Menu();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        PrescaleSetAlias param = new PrescaleSetAlias();
        ArrayList<PrescaleSetAlias> expResult = new ArrayList();
        Object result = instance.getAliases();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetPrescaleSets() throws Exception {
        System.out.println("getPrescaleSets");
        L1Menu instance = new L1Menu();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        L1Prescale param = new L1Prescale();
        ArrayList<L1Prescale> expResult = new ArrayList();
        Object result = instance.getPrescaleSets();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetPrescalesSetsNonHidden() throws Exception {
        System.out.println("getPrescaleSetsNonHidden");
        L1Menu instance = new L1Menu();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        L1Prescale param = new L1Prescale();
        ArrayList<L1Prescale> expResult = new ArrayList();
        Object result = instance.getPrescalesSetsNonHidden();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetTMTT() throws Exception {
    }

    @Test
    public void testGet_files() throws Exception {
        System.out.println("get_files");
        L1Menu instance = new L1Menu();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Object result = instance.get_files();
        Object expResult = null;
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_smx() throws Exception {
        System.out.println("get_smx");
        L1Menu instance = new L1Menu();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Object result = instance.get_smx();
        Object expResult = null;
        assertEquals(expResult, result);
    }

    @Test
    public void testAddToTree() throws Exception {
    }

    @Test
    public void testClone() {
        System.out.println("clone");
        L1Menu instance = new L1Menu();
        Object expResult = instance;
        Object result = instance.clone();
        assertEquals(expResult, result);
    }

    @Test
    public void testDoDiff() throws Exception {
    }

    @Test
    public void testSave() throws Exception {
        System.out.println("save");
        L1Menu instance = new L1Menu();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Integer expResult = 1;
        Object result = instance.save();
        assertEquals(expResult, result);
    }

    @Test
    public void testReplace_ctp_links() throws Exception {
    }

    @Test
    public void testGetTableName() {
        System.out.println("getTableName");
        L1Menu instance = new L1Menu();
        String expResult = "L1_TRIGGER_MENU";
        String result = instance.getTableName();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetPrescalesSets() throws Exception {
        System.out.println("getPrescalesSets");
        L1Menu instance = new L1Menu();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        List<L1Prescale> expResult = new ArrayList();
        Object Result = instance.getPrescalesSets(true);
        assertEquals(expResult, Result);
        Object Result2 = instance.getPrescalesSets(false);
        assertEquals(expResult, Result2);
    }

    @Test
    public void testGetPrescalesSetsInfo() throws Exception {
        System.out.println("getPrescalesSetsInfo");
        L1Menu instance = new L1Menu();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        List<String> expResult = new ArrayList();
        Object Result = instance.getPrescalesSetsInfo(true);
        assertEquals(expResult, Result);
        Object Result2 = instance.getPrescalesSetsInfo(false);
        assertEquals(expResult, Result2);
    }

    @Test
    public void testGetPrescaleSetInfoPartition() throws Exception {
        //REVISIT THIS
        System.out.println("getPrescaleSetInfoPartition");
        L1Menu instance = new L1Menu();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        List<String> expResult = new ArrayList();
        Boolean input_bool = true;
        ArrayList<Integer> input_int = new ArrayList();
        input_int.add(2712);
        input_int.add(-1);
        Object Result = instance.getPrescaleSetInfoPartition(input_bool,input_int);
        assertEquals(expResult, Result);
    }

    @Test
    public void testAddPrescaleSet() throws Exception {
        System.out.println("addPrescaleSet");
        L1Menu instance = new L1Menu();
        instance.set_id(2712);
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Boolean expResult = false;
        L1Prescale input = new L1Prescale();
        input.set_id(2712);
        Boolean input_bool = true;
        Object Result = instance.addPrescaleSet(input, input_bool);
        assertEquals(expResult, Result);
        input.set_id(-1);
        Boolean expResult2 = false;
        Object Result2 = instance.addPrescaleSet(input, input_bool);
        assertEquals(expResult2, Result2);
    }
    
}
