/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.L1;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import com.mockrunner.mock.jdbc.MockResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.Entities.HLT.HLTTriggerGroup;

/**
 *
 * @author davethomas
 */
public class L1ThresholdTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public  L1ThresholdTest() {
    }
    
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
        ConnectionManager.getInstance().unsetTestingConnection();
    }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
    }

    @Test
    public void testGet_multiplicity() {
        System.out.println("get_multiplicity");
        L1Threshold instance = new L1Threshold();
        Integer expResult = 1;
        Object result = instance.get_multiplicity();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_position() {
        System.out.println("get_position");
        L1Threshold instance = new L1Threshold();
        Integer expResult = -1;
        Object result = instance.get_position();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_position() {
        System.out.println("set_position");
        L1Threshold instance = new L1Threshold();
        Integer input = 3;
        instance.set_position(input);
        Integer expResult = 3;
        Object result = instance.get_position();
        assertEquals(expResult, result);
    }

    @Test
    public void testIsInternalThreshold() {
        System.out.println("isInternalThreshold");
        L1Threshold instance = new L1Threshold();
        L1Threshold input = new L1Threshold();
        input.set_name("RNDM0");
        boolean expResult = true;
        Object result = instance.isInternalThreshold(input);
        assertEquals(expResult, result);
        
        L1Threshold instance2 = new L1Threshold();
        L1Threshold input2 = new L1Threshold();
        boolean expResult2 = false;
        Object result2 = instance2.isInternalThreshold(input2);
        assertEquals(expResult2, result2);
    }

    @Test
    public void testGetThresholdValues() {
        System.out.println("getThresholdValues");
        L1Threshold instance = new L1Threshold();
        ArrayList<L1ThresholdValue> expResult = new ArrayList();
        Object result = instance.getThresholdValues();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetThresholdValues() {
        System.out.println("getThresholdValues");
        L1Threshold instance = new L1Threshold();
        L1ThresholdValue param = new L1ThresholdValue();
        ArrayList<L1ThresholdValue> input = new ArrayList();
        input.add(param);
        instance.setThresholdValues(input);
        Object result = instance.getThresholdValues();
        Object expResult = input;
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_l1_master() {
        System.out.println("get_l1_master");
        L1Threshold instance = new L1Threshold();
        Object expResult = null;
        Object result = instance.get_l1_master();
        assertEquals(expResult, result);
        
    }

    @Test
    public void testForceLoad() throws Exception {
    }

    @Test
    public void testGet_bitnum() {
        System.out.println("get_bitnum");
        L1Threshold instance = new L1Threshold();
        Integer expResult = 1;
        Object result = instance.get_bitnum();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_type() {
        System.out.println("get_type");
        L1Threshold instance = new L1Threshold();
        String expResult = "";
        Object result = instance.get_type();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_active() {
        System.out.println("get_active");
        L1Threshold instance = new L1Threshold();
        Integer expResult = 0;
        Object result = instance.get_active();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_mapping() {
        System.out.println("get_mapping");
        L1Threshold instance = new L1Threshold();
        Integer expResult = 0;
        Object result = instance.get_mapping();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_bcdelay() {
        System.out.println("get_bcdelay");
        L1Threshold instance = new L1Threshold();
        Integer expResult = -1;
        Object result = instance.get_bcdelay();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_seed() {
        System.out.println("get_seed");
        L1Threshold instance = new L1Threshold();
        String expResult = "";
        Object result = instance.get_seed();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_seed_multi() {
        System.out.println("get_seed_multi");
        L1Threshold instance = new L1Threshold();
        Integer expResult = -1;
        Object result = instance.get_seed_multi();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_active() {
        System.out.println("set_active");
        L1Threshold instance = new L1Threshold();
        Integer input = 3;
        instance.set_active(input);
        Integer expResult = 3;
        Object result = instance.get_active();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_mapping() {
        System.out.println("set_mapping");
        L1Threshold instance = new L1Threshold();
        Integer input = 3;
        instance.set_mapping(input);
        Integer expResult = 3;
        Object result = instance.get_mapping();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_bitnum() {
        System.out.println("set_bitnum");
        L1Threshold instance = new L1Threshold();
        Integer input = 3;
        instance.set_bitnum(input);
        Integer expResult = 3;
        Object result = instance.get_bitnum();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_bcdelay() {
        System.out.println("set_bcdelay");
        L1Threshold instance = new L1Threshold();
        Integer input = 3;
        instance.set_bcdelay(input);
        Integer expResult = 3;
        Object result = instance.get_bcdelay();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_seed() {
        System.out.println("set_seed");
        L1Threshold instance = new L1Threshold();
        String input = "a string";
        instance.set_seed(input);
        String expResult = "a string";
        Object result = instance.get_seed();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_seed_multi() {
        System.out.println("set_seed_multi");
        L1Threshold instance = new L1Threshold();
        Integer input = 3;
        instance.set_seed_multi(input);
        Integer expResult = 3;
        Object result = instance.get_seed_multi();
        assertEquals(expResult, result);
    }

    @Test
    public void testAddToTree() throws Exception {
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        L1Threshold instance = new L1Threshold();
        String expResult= "L1 Threshold";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
                
        L1Threshold instance2 = new L1Threshold();
        instance2.set_id(2712);
        String expResult2 = "THRESHOLD:  (DBid=2712/V.1)";
        Object Result2 = instance2.toString();
        assertEquals(expResult2, Result2);
    }

    @Test
    public void testClone() {
        System.out.println("clone");
        L1Threshold instance = new L1Threshold();
        Object expResult = instance;
        Object result = instance.clone();
        assertEquals(expResult, result);
    }

    @Test
    public void testDoDiff() throws Exception {
    }

    @Test
    public void testSave() throws Exception {
        System.out.println("save");
        L1Threshold instance = new L1Threshold();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Integer expResult = 1;
        Object result = instance.save();
        assertEquals(expResult, result);
    }

    @Test
    public void testCompactsave() throws Exception {
        System.out.println("compactsave");
        L1Threshold instance = new L1Threshold();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Integer expResult = 1;
        Object result = instance.compactsave();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_l1_master() {
        System.out.println("set_l1_master");
        L1Threshold instance = new L1Threshold();
        L1Master input = new L1Master();
        Object expResult = input;
        instance.set_l1_master(input);
        Object result = instance.get_l1_master();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_multiplicity() {
        System.out.println("set_multiplicity");
        L1Threshold instance = new L1Threshold();
        Integer input = 3;
        instance.set_multiplicity(input);
        Integer expResult = 3;
        Object result = instance.get_multiplicity();
        assertEquals(expResult, result);
    }

    @Test
    public void testEquals() {
        System.out.println("equals");
        
        L1Threshold instance = new L1Threshold();
        L1Threshold param1 = new L1Threshold();
        L1Threshold param2 = new L1Threshold();
        Boolean expTrueResult = true;
        Boolean expFalseResult = false;
            
        String IncorrectType = "";
        Object Result1 = param1.equals(IncorrectType);
        assertEquals(Result1,expFalseResult);
        assertNotEquals(Result1,expTrueResult);
        
        instance.set_name("right name"); 
        param1.set_name("right name");
        param2.set_name("wrong name");
        Object TrueResult = param1.equals(instance);
        Object FalseResult = param2.equals(instance);
        assertEquals(TrueResult,expTrueResult);
        assertEquals(FalseResult,expFalseResult);
        
       //L1ThresholdValue instance_value = (L1ThresholdValue) v1;
        //instance_value= L1ThresholdValue v1;
       // ArrayList<L1ThresholdValue>;
       // instance.setThresholdValues();
       // param1.set_trigger_chain_id(-2);
       // param2.set_trigger_chain_id(-3);
       // instance.set_trigger_chain_id(-2);
       // instance.set_name("");
       // param1.set_name("");
       // param2.set_name("");
       // System.out.println(instance.get_name());
       // System.out.println(param1.get_name());
       // System.out.println(param2.get_name());
       // Object Result = param1.equals(instance);
       // Object Result2 = param2.equals(instance);
       // assertEquals(Result,expTrueResult);
       // assertEquals(Result2,expFalseResult);
       
       // instance.set_trigger_chain_id(-1);
       // param1.set_trigger_chain_id(-1);
       // instance.set_name("goodName");
       // param1.set_name("goodName");
       // param2.set_name("badName");
       // System.out.println(instance.get_name());
       // System.out.println(param1.get_name());
       // System.out.println(param2.get_name());
       // Object Result3 = param1.equals(instance);
       // Object Result4 = param2.equals(instance);
       // assertEquals(Result3,expTrueResult);
       // assertEquals(Result4,expFalseResult);
        
       // instance.set_name("notempty");
       // param1.set_name("alsonotempty");
       // System.out.println(instance.get_name());
        //System.out.println(param1.get_name());
       // Object Result5 = param1.equals(instance);
       // assertEquals(Result5,expFalseResult);
        
       // param1.set_name("stillnotempty");
       // instance.set_name("stillnotempty");
       // Object Result6 = param1.equals(instance);
       // assertEquals(Result6,expTrueResult);
        
       // param1.set_name("");
       // instance.set_name("blah");
       // Object Result7 = param1.equals(instance);
       // assertEquals(Result7,expTrueResult);
    }

    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        L1Threshold instance = new L1Threshold();
        Integer expResult = 469;
        Object result = instance.hashCode();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetTableName() {
        System.out.println("getTableName");
        L1Threshold instance = new L1Threshold();
        String expResult = "L1_TRIGGER_THRESHOLD";
        String result = instance.getTableName();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_type() {
        System.out.println("set_type");
        L1Threshold instance = new L1Threshold();
        String input = "a string";
        instance.set_type(input);
        String expResult = "a string";
        Object result = instance.get_type();
        assertEquals(expResult, result);
    }
    
}