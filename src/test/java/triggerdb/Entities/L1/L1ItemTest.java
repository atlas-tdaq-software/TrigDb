/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.L1;

import java.util.ArrayList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;

/**
 *
 * @author davethomas
 */
public class L1ItemTest {
    
    public L1ItemTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testEditComment() throws Exception {
    }

    @Test
    public void testSet_l1_master() {
    }

    @Test
    public void testSet_hlt_master() {
    }

    @Test
    public void testForceLoad() throws Exception {
    }

    @Test
    public void testGetThresholds() {
    }

    @Test
    public void testSetThresholds() {
    }

    @Test
    public void testRenumberThresholds() throws Exception {
    }

     @Test
    public void testSet_comment() {
        System.out.println("set_comment");
        L1Item instance1 = new L1Item();
        instance1.set_comment(null);
        String expResult = "null comment";
        String result = instance1.get_comment();
        assertEquals(expResult, result);
        
        L1Item instance2 = new L1Item();
        instance2.set_comment("something helpful");
        String expResult2 = "something helpful";
        String result2 = instance2.get_comment();
        assertEquals(expResult2, result2);
    }

    @Test
    public void testGet_trigger_type() {
        System.out.println("get_trigger_type");
        L1Item instance = new L1Item();
        String expResult = "00000000";
        String result = instance.get_trigger_type();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_trigger_type_i() {
        System.out.println("get_trigger_type_i");
        L1Item instance = new L1Item();
        Integer expResult = 0;
        Object result = instance.get_trigger_type_i();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_trigger_type() {
        System.out.println("set_trigger_type");
        L1Item instance1 = new L1Item();
        String input = "1001001";
        Integer expResult = 73;
        instance1.set_trigger_type(input);
        Object result = instance1.get_trigger_type_i();
        assertEquals(expResult, result);
        
        L1Item instance2 = new L1Item();
        String input2 = "73";
        Integer expResult2 = 73;
        instance2.set_trigger_type(input2);
        Object result2 = instance2.get_trigger_type_i();
        assertEquals(expResult2, result2);
    }

    @Test
    public void testGet_ctp_id() {
        System.out.println("get_ctp_id");
        L1Item instance = new L1Item();
        Integer expResult = 1;
        Object result = instance.get_ctp_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_partition() {
        System.out.println("get_partition");
        L1Item instance = new L1Item();
        Integer expResult = 1;
        Object result = instance.get_partition();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_definition() {
        System.out.println("get_definition");
        L1Item instance = new L1Item();
        String expResult = "(1)";
        String result = instance.get_definition();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_priority() {
        System.out.println("get_priority");
        L1Item instance = new L1Item();
        String expResult = "LOW";
        String result = instance.get_priority();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_group() {
        System.out.println("get_group");
        L1Item instance = new L1Item();
        Integer expResult = 1;
        Object result = instance.get_group();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_monitor() {
        System.out.println("get_monitor");
        L1Item instance = new L1Item();
        ConnectionManager.L1ItemMonitorColumnExists = 0;
        instance.initialize_monitor();
        String expResult = "";
        String result = instance.get_monitor();
        assertEquals(expResult, result);
        
        L1Item instance2 = new L1Item();
        ConnectionManager.L1ItemMonitorColumnExists = 1;
        instance2.initialize_monitor();
        String expResult2 = "None";
        String result2 = instance2.get_monitor();
        assertEquals(expResult2, result2);
    }

    @Test
    public void testSet_ctp_id() {
        System.out.println("set_ctp_id");
        L1Item instance = new L1Item();
        Integer input = 3;
        instance.set_ctp_id(input);
        Object Result = instance.get_ctp_id();
        Integer expResult = 3;
        assertEquals(Result,expResult);
        
    }

    @Test
    public void testSet_partition() {
        System.out.println("set_partition");
        L1Item instance = new L1Item();
        Integer input = 3;
        instance.set_partition(input);
        Object Result = instance.get_partition();
        Integer expResult = 3;
        assertEquals(Result,expResult);
    }

    @Test
    public void testSet_definition() {
        System.out.println("set_definition");
        L1Item instance = new L1Item();
        instance.set_definition("definition");
        String expResult = "definition";
        String result = instance.get_definition();
        assertEquals(expResult, result);
        
    }

    @Test
    public void testSet_priority() {
        System.out.println("set_priority");
        L1Item instance = new L1Item();
        instance.set_priority("a string");
        String expResult = "a string";
        String result = instance.get_priority();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_group() {
        System.out.println("set_group");
        L1Item instance = new L1Item();
        Integer input = 3;
        instance.set_group(input);
        Object Result = instance.get_group();
        Integer expResult = 3;
        assertEquals(Result,expResult);
    }

    @Test
    public void testSet_monitor() {
        System.out.println("get_monitor");
        L1Item instance = new L1Item();
        ConnectionManager.L1ItemMonitorColumnExists = 0;
        instance.set_monitor("a string");
        String expResult = "";
        String result = instance.get_monitor();
        assertEquals(expResult, result);
        
        L1Item instance2 = new L1Item();
        ConnectionManager.L1ItemMonitorColumnExists = 1;
        instance2.set_monitor("a string");
        String expResult2 = "a string";
        String result2 = instance2.get_monitor();
        assertEquals(expResult2, result2);
    }

    @Test
    public void testAddToTree() throws Exception {
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        L1Item instance = new L1Item();
        String expResult= "ITEM: , def: (1) (DBid=-1/V.1)";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
    }

    @Test
    public void testClone() {
        System.out.println("clone");
        L1Item instance = new L1Item();
        Object expResult = instance;
        Object result = instance.clone();
        assertEquals(expResult, result);
    }

    @Test
    public void testDoDiff() throws Exception {
    }

    @Test
    public void testSave() throws Exception {
    }

    @Test
    public void testGet_min_names() {
        System.out.println("get_min_names");
        L1Item instance = new L1Item();
        
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("ID");
        expResult.add("Name");
        expResult.add("Version");
        expResult.add("Comment");                
        Object Result = instance.get_min_names();
        assertEquals(expResult, Result);
    }

    @Test
    public void testGet_min_info() {
        System.out.println("get_min_info");
        L1Item instance = new L1Item();
        
        ArrayList<Object> expResult = new ArrayList<>();
        expResult.add(-1);
        expResult.add("");
        expResult.add(1);
        expResult.add("Comment");                
        Object Result = instance.get_min_info();
        assertEquals(expResult, Result);
    }

    @Test
    public void testGetTableName() {
        System.out.println("getTableName");
        L1Item instance = new L1Item();
        String expResult = "L1_TRIGGER_ITEM";
        String result = instance.getTableName();
        assertEquals(expResult, result);
    }
    
}
