package triggerdb.Entities.L1;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import static triggerdb.Entities.HLT.HLTPrescaleType.*;
import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.*;
import com.mockrunner.mock.jdbc.*;
import java.sql.PreparedStatement;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Logger;
import javax.swing.tree.DefaultMutableTreeNode;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.TriggerMap;
import triggerdb.Entities.alias.PrescaleSetAlias;

/**
 *
 * @author dwthomas
 */
public class L1BunchGroupTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public L1BunchGroupTest() {
    }
    
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
        ConnectionManager.getInstance().unsetTestingConnection();
        }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
        }

    @Test
    public void testSet_name() {
            //not callable
    }

    @Test
    public void testForceLoad() throws Exception {
    }

    @Test
    public void testSet_partition() {
        System.out.println("set_partition");
        L1BunchGroup instance = new L1BunchGroup();
        Integer input = 3;
        instance.set_partition(input);
        Object Result = instance.get_partition();
        Integer expResult = 3;
        assertEquals(Result,expResult);
    }

    @Test
    public void testGet_partition() {
        System.out.println("get_partition");
        L1BunchGroup instance = new L1BunchGroup();
        Object Result = instance.get_partition();
        Integer expResult = null;
        assertEquals(Result,expResult);
    }

    @Test
    public void testSet_size() {
        System.out.println("set_size");
        L1BunchGroup instance = new L1BunchGroup();
        Integer input = 3;
        instance.set_size(input);
        Object Result = instance.get_size();
        Integer expResult = 3;
        assertEquals(Result,expResult);
    }

    @Test
    public void testGet_size() {
        System.out.println("get_size");
        L1BunchGroup instance = new L1BunchGroup();
        Object Result = instance.get_size();
        Integer expResult = 1;
        assertEquals(Result,expResult);
    }

    @Test
    public void testGet_internal_number() {
        System.out.println("get_internal_number");
        L1BunchGroup instance = new L1BunchGroup();
        Object Result = instance.get_internal_number();
        Integer expResult = -1;
        assertEquals(Result,expResult);
    }

    @Test
    public void testSet_Bunches() {
        System.out.println("set_Bunches");
        L1BunchGroup instance = new L1BunchGroup();
        ArrayList<L1Bunch> param1 = new ArrayList<>();
        L1Bunch param2 = new L1Bunch();
        param1.add(param2);
        instance.set_Bunches(param1);
        Object expResult = param1;
        Object Result = instance.get_Bunches();
        assertEquals(Result,expResult);
    }

           /**
     * Helper function to make an HLTTE_TE and handle
     * SQLExceptions.
     * @param id
     * @return 
     */
    public L1BunchGroup getL1BunchGroupWithParams(int id, int internal) {
        L1BunchGroup result = null;
        try {
            result = new L1BunchGroup(id);
            result.set_internal_number(internal);
        } catch (SQLException e) {
            fail(e.toString());
        }
        return result;
    }
    
             /**
     * Helper function to make an HLTTE_TE and handle
     * SQLExceptions.
     * @param id
     * @return 
     */
    public L1Bunch getL1BunchWithID(int id) {
        L1Bunch result = null;
        try {
            result = new L1Bunch(id);
        } catch (SQLException e) {
            fail(e.toString());
        }
        return result;
    }
    
    @Test
    public void testGet_Bunches() {
        System.out.println("get_Bunches");
        L1BunchGroup instance = new L1BunchGroup();
        ArrayList<L1Bunch> param1 = new ArrayList<>();
        L1Bunch param2 = getL1BunchWithID(-2712);
        param1.add(param2);
        instance.set_Bunches(param1);
        Object expResult = param1;
        Object Result = instance.get_Bunches();
        assertEquals(Result,expResult);
    }

    @Test
    public void testGetBunchesFromDb() throws Exception {
        System.out.println("getBunchesFromDb");
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        L1BunchGroup instance = new L1BunchGroup();
        instance.set_id(27);
        ArrayList<L1Bunch> expResult = new ArrayList();
        Object Result = instance.getBunchesFromDb();
        assertEquals(expResult,Result);
    }

    @Test
    public void testSave() throws Exception {
        System.out.println("save");
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        L1BunchGroup instance = new L1BunchGroup();
        ArrayList<L1Bunch> input = new ArrayList<>();
        L1Bunch param = new L1Bunch();
        input.add(param);
        instance.set_Bunches(input);
        Integer expResult = 1;
        Object Result = instance.save();
        assertEquals(expResult,Result);
    }

    @Test
    public void testSaveOld() throws Exception {
    }

    @Test
    public void testAddToTree() throws Exception {
    }

    @Test
    public void testGetEmptyBunchGroups() throws Exception {
        //System.out.println("GetEmptyBunchGroups");
        //MockResultSet resultset = resultSetHandler.createResultSet();
        //setEmptyInitInfo();
        //L1BunchGroup instance = new L1BunchGroup();
        //ArrayList<Integer> expResult = new ArrayList();
        //ConnectionManager param = null;
        //Object Result = instance.GetEmptyBunchGroupsPublic(param);
        //assertEquals(expResult,Result);
    }
    
    @Test
    public void testToString() {
        System.out.println("toString");
        L1BunchGroup instance = new L1BunchGroup();
        String expResult = "L1 Bunch Group";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
         
        L1BunchGroup instance2 = getL1BunchGroupWithParams(-2712,0);
        String expResult2 = "BG 0: (DBid=-2712, Size = 1)";
        Object Result2 = instance2.toString();
        assertEquals(expResult2, Result2);
        
        L1BunchGroup instance3 = getL1BunchGroupWithParams(-2712,-2712);
        String expResult3 = "(DBid=-2712, Size = 1)";
        Object Result3 = instance3.toString();
        assertEquals(expResult3,Result3);
        
              
    
        //String Result2 = instance2.toString();
        //assertEquals(expResult2,Result2);
    }

    @Test
    public void testClone() {
        System.out.println("clone");
        L1BunchGroup instance = new L1BunchGroup();
        Object expResult = instance;
        Object result = instance.clone();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_internal_number() {
        System.out.println("set_internal_number");
        L1BunchGroup instance = new L1BunchGroup();
        Integer input = -2712;
        instance.set_internal_number(input);
        Object expResult = input;
        Object Result = instance.get_internal_number();
        assertEquals(Result,expResult);
    }

    @Test
    public void testGetBunchGroups() throws Exception {
        System.out.println("getBunchGroups");
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        L1BunchGroup instance = new L1BunchGroup();
        List<L1BunchGroup> expResult = new ArrayList();
        Object Result = instance.getBunchGroups();
        assertEquals(expResult,Result);
    }

    @Test
    public void testGetTableName() {
        System.out.println("getTableName");
        L1BunchGroup instance = new L1BunchGroup();
        String expResult = "L1_BUNCH_GROUP";
        String result = instance.getTableName();
        assertEquals(expResult, result);
    }
    
    @Test
    public void testGet_min_names() {
        System.out.println("get_min_names");
        L1BunchGroup instance = new L1BunchGroup();
        
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("ID");
        expResult.add("Size");
        
        ArrayList<String> result = instance.get_min_names();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_min_info() {
        
        System.out.println("get_min_info");
        L1BunchGroup instance = new L1BunchGroup();
        
        ArrayList<Object> expResult = new ArrayList<>();
        expResult.add(-1);
        expResult.add(1);
        
        ArrayList<Object> result = instance.get_min_info();
        assertEquals(expResult, result);
    }
}
