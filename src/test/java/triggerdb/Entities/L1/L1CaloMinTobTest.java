/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.L1;

import java.sql.SQLException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author davethomas
 */
public class L1CaloMinTobTest {
    
    public L1CaloMinTobTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGet_thr_type() {
        System.out.println("get_thr_type");
        L1CaloMinTob instance = new L1CaloMinTob();
        Object Result = instance.get_thr_type();
        String expResult = "thr type";
        assertEquals (Result, expResult);
    }

    @Test
    public void testSet_thr_type() {
        System.out.println("get_thr_type");
        L1CaloMinTob instance = new L1CaloMinTob();
        String input = "something";
        instance.set_thr_type(input);
        Object Result = instance.get_thr_type();
        String expResult = input;
        assertEquals (Result, expResult);
    }

    @Test
    public void testGet_window() {
     System.out.println("get_window");
     L1CaloMinTob instance = new L1CaloMinTob();
     Object Result = instance.get_window();
     Integer expResult = 0;
     assertEquals (Result, expResult); 
    }

    @Test
    public void testSet_window() {
        System.out.println("set_window");
        L1CaloMinTob instance = new L1CaloMinTob();
        Integer input = 27;
        instance.set_window(input);
        Object Result = instance.get_window();
        Integer expResult = input;
        assertEquals (Result, expResult);
    }

    @Test
    public void testGet_pt_min() {
        System.out.println("get_pt_min");
        L1CaloMinTob instance = new L1CaloMinTob();
        Object Result = instance.get_pt_min();
        Integer expResult = -1;
         assertEquals (Result, expResult); 
    }

    @Test
    public void testSet_pt_min() {
        System.out.println("set_pt_min");
        L1CaloMinTob instance = new L1CaloMinTob();
        Integer input = 27;
        instance.set_pt_min(input);
        Object Result = instance.get_pt_min();
        Integer expResult = input;
        assertEquals (Result, expResult);
    }

    @Test
    public void testGet_eta_min() {
     System.out.println("get_eta_min");
     L1CaloMinTob instance = new L1CaloMinTob();
     Object Result = instance.get_eta_min();
     Integer expResult = -1;
     assertEquals (Result, expResult); 
    }

    @Test
    public void testSet_eta_min() {
        System.out.println("set_eta_min");
        L1CaloMinTob instance = new L1CaloMinTob();
        Integer input = 27;
        instance.set_eta_min(input);
        Object Result = instance.get_eta_min();
        Integer expResult = input;
        assertEquals (Result, expResult);
    }
        
    @Test
    public void testGet_eta_max() {
     System.out.println("get_eta_max");
     L1CaloMinTob instance = new L1CaloMinTob();
     Object Result = instance.get_eta_max();
     Integer expResult = -1;
     assertEquals (Result, expResult);
    }

    @Test
    public void testSet_eta_max() {
        System.out.println("set_eta_max");
        L1CaloMinTob instance = new L1CaloMinTob();
        Integer input = 27;
        instance.set_eta_max(input);
        Object Result = instance.get_eta_max();
        Integer expResult = input;
        assertEquals (Result, expResult);
    }

    @Test
    public void testGet_priority() {
     System.out.println("get_priority");
     L1CaloMinTob instance = new L1CaloMinTob();
     Object Result = instance.get_priority();
     Integer expResult = -1;
     assertEquals (Result, expResult);
    }

    @Test
    public void testSet_priority() {
        System.out.println("set_priority");
        L1CaloMinTob instance = new L1CaloMinTob();
        Integer input = 27;
        instance.set_priority(input);
        Object Result = instance.get_priority();
        Integer expResult = input;
        assertEquals (Result, expResult);
    }

    @Test
    public void testSave() throws Exception {
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        L1CaloMinTob instance = new L1CaloMinTob();
        String expResult = "L1 Calo Min Tob";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
        
        L1CaloMinTob instance2 = getL1CaloMinTobWithID(-1000000);
        String expResult2 = "L1 CALO MIN TOB: id=-1000000, name=null, ver=null";
        String Result2 = instance2.toString();
        assertEquals(expResult2,Result2);
    }

    
               /**
     * Helper function to make an HLTTE_TE and handle
     * SQLExceptions.
     * @param id
     * @return 
     */
    public L1CaloMinTob getL1CaloMinTobWithID(int id) {
        L1CaloMinTob result = null;
        try {
            result = new L1CaloMinTob(id);
        } catch (SQLException e) {
            fail(e.toString());
        }
        return result;
    }
    
    @Test
    public void testClone() {
        System.out.println("clone");
        L1CaloMinTob instance = new L1CaloMinTob();
        Object expResult = instance;
        Object result = instance.clone();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetTableName() {
        System.out.println("getTableName");
        L1CaloMinTob instance = new L1CaloMinTob();
        String expResult = "L1_CALO_MIN_TOB";
        String result = instance.getTableName();
        assertEquals(expResult, result);
    }
    
}
