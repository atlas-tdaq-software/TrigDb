package triggerdb.Entities.L1;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import static triggerdb.Entities.HLT.HLTPrescaleType.*;
import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.*;
import com.mockrunner.mock.jdbc.*;
import java.sql.PreparedStatement;
import java.util.AbstractMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Logger;
import javax.swing.tree.DefaultMutableTreeNode;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.TriggerMap;
import triggerdb.Entities.alias.PrescaleSetAlias;

/**
 *
 * @author dwthomas
 */
public class L1BunchGroupSetTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public L1BunchGroupSetTest() {
    }
    
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
        ConnectionManager.getInstance().unsetTestingConnection();
        }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
        }


    @Test
    public void testForceLoad() throws Exception {
    }

    @Test
    public void testGetBunchGroups() {
    }

    @Test
    public void testSetBunchGroups() {
    System.out.println("setBunchGroups");
    L1BunchGroupSet instance = new L1BunchGroupSet();
    ArrayList<AbstractMap.SimpleEntry<L1BunchGroup, String>> nullarray = null;
    ArrayList<AbstractMap.SimpleEntry<L1BunchGroup, String>> array = new ArrayList<>();
    instance.setBunchGroups(nullarray);
    Object expResult = array;
    Object Result1 = instance.getBunchGroups();
    assertEquals(expResult,Result1);
    instance.setBunchGroups(array);
    Object Result2 = instance.getBunchGroups();
    assertEquals(expResult,Result2);
        
        
    }

    @Test
    public void testSet_partition() {
        System.out.println("set_partition");
        L1BunchGroupSet instance = new L1BunchGroupSet();
        Integer input = 3;
        instance.set_partition(input);
        Object Result = instance.get_partition();
        Integer expResult = 3;
        assertEquals(Result,expResult);
    }

    @Test
    public void testGet_partition() {
        System.out.println("get_partition");
        L1BunchGroupSet instance = new L1BunchGroupSet();
        Integer expResult = 0;
        Object Result = instance.get_partition();
        assertEquals(expResult,Result);
    }

    @Test
    public void testGetBunchGroupsFromDb() throws Exception {
        System.out.println("getBunchGroupsFromDb");
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        L1BunchGroupSet instance = new L1BunchGroupSet();
        ArrayList<AbstractMap.SimpleEntry<L1BunchGroup, String>> expResult = new ArrayList();
        Object Result = instance.getBunchGroupsFromDb();
        assertEquals(expResult,Result);
    }

    @Test
    public void testSave() throws Exception {
        System.out.println("save");
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        L1BunchGroupSet instance = new L1BunchGroupSet();
        ArrayList<AbstractMap.SimpleEntry<L1BunchGroup, String>> input = new ArrayList();
        AbstractMap.SimpleEntry<L1BunchGroup, String> param;
        AbstractMap.SimpleEntry<L1BunchGroup, String> param2;
        L1BunchGroup subparam1 = new L1BunchGroup();
        String subparam2 = "";
        L1BunchGroup subparam3 = new L1BunchGroup();
        subparam3.set_partition(27);
        param = new AbstractMap.SimpleEntry<L1BunchGroup, String>(subparam1, subparam2);
        param2 = new AbstractMap.SimpleEntry<L1BunchGroup, String>(subparam3, subparam2);
        input.add(param);
        input.add(param2);
        instance.setBunchGroups(input);
        Integer expResult = 1;
        Object Result = instance.save();
        assertEquals(expResult,Result);
    }

    @Test
    public void testAddToTree() throws Exception {
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        L1BunchGroupSet instance = new L1BunchGroupSet();
        String expResult = "L1 Bunch Group Set";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
        
        L1BunchGroupSet instance2 = getL1BunchGroupSetWithID(-1000000);
        String expResult2 = "BUNCH GROUP SET: ID=-1000000, Name=, Version=1";
        String Result2 = instance2.toString();
        assertEquals(expResult2,Result2);
    }

          /**
     * Helper function to make an HLTTE_TE and handle
     * SQLExceptions.
     * @param id
     * @return 
     */
    public L1BunchGroupSet getL1BunchGroupSetWithID(int id) {
        L1BunchGroupSet result = null;
        try {
            result = new L1BunchGroupSet(id);
        } catch (SQLException e) {
            fail(e.toString());
        }
        return result;
    }
    
    
    
    @Test
    public void testSet_comment() {
        System.out.println("set_comment");
        L1BunchGroupSet instance = new L1BunchGroupSet();
        String input = "a string";
        instance.set_comment(input);
        Object Result = instance.get_comment();
        String expResult = input;
        assertEquals(Result,expResult);
    }

    @Test
    public void testGet_comment() {
        System.out.println("get_comment");
        L1BunchGroupSet instance = new L1BunchGroupSet();
        Object Result = instance.get_comment();
        String expResult = "Comment";
        assertEquals(Result,expResult);
    }

    @Test
    public void testClone() {
        System.out.println("clone");
        L1BunchGroupSet instance = new L1BunchGroupSet();
        Object expResult = instance;
        Object result = instance.clone();
        System.out.println("Additional output for occassional clone failures");
        System.out.println("Original: " + instance.get_name() + ", "
            + instance.get_version() + ", "
            + instance.get_comment() + ", "
            + instance.get_partition() + ", "
            + instance.get_id());
        System.out.println("Clone: " + ((L1BunchGroupSet) result).get_name() + ", "
            + ((L1BunchGroupSet) result).get_version() + ", "
            + ((L1BunchGroupSet) result).get_comment() + ", "
            + ((L1BunchGroupSet) result).get_partition() + ", "
            + ((L1BunchGroupSet) result).get_id());
        assertEquals(expResult, result);
    }

    @Test
    public void testGetTableName() {
        System.out.println("getTableName");
        L1BunchGroupSet instance = new L1BunchGroupSet();
        String expResult = "L1_BUNCH_GROUP_SET";
        String result = instance.getTableName();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetBunchGroupSets() throws Exception {
        System.out.println("getBunchGroupSets");
        L1BunchGroupSet instance = new L1BunchGroupSet();
        List<L1BunchGroupSet> param = new ArrayList<>();
        Object expResult = param;
        Object Result = instance.getBunchGroupSets();
        assertEquals(Result,expResult);
    }

    @Test
    public void testGetActiveBunchGroupsInPartition() throws Exception {
        System.out.println("getActiveBunchGroupsInPartition");
        L1BunchGroupSet instance = new L1BunchGroupSet();
        List<String> param = new ArrayList<>();
        Object expResult = param;
        Object Result = instance.getActiveBunchGroupsInPartition(27, 12);
        assertEquals(Result,expResult);
    }

    @Test
    public void testGetBunchGroupSetsPartition() throws Exception {
        System.out.println("getBunchGroupSetsPartition");
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        L1BunchGroupSet instance = new L1BunchGroupSet();
        List<L1BunchGroupSet> param = new ArrayList<>();
        Object expResult = param;
        Object Result = instance.getBunchGroupSetsPartition(27);
        Object Result2 = instance.getBunchGroupSetsPartition(0);
        assertEquals(Result,expResult);
        assertEquals(Result2,expResult);
    }
    
}
