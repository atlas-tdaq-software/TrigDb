/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.L1;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import com.mockrunner.mock.jdbc.MockResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;

/**
 *
 * @author dwthomas
 */
public class L1MuonThresholdSetTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    
    public L1MuonThresholdSetTest() {
    }
    
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @After
    public void tearDown() {
    }
    
    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
    }

    @Test
    public void testGet_name() {
        System.out.println("get_name");
        L1MuonThresholdSet instance = new L1MuonThresholdSet();
        String expResult = "L1 Muon Name";
        Object result = instance.get_name();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_name() {
        System.out.println("set_name");
        L1MuonThresholdSet instance = new L1MuonThresholdSet();
        String input = "a string";
        instance.set_name(input);
        String expResult = input;
        Object result = instance.get_name();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_rpc_available() {
        System.out.println("get_rpc_available");
        L1MuonThresholdSet instance = new L1MuonThresholdSet();
        Integer expResult = 0;
        Object result = instance.get_rpc_available();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_rpc_available() {
        System.out.println("set_rpc_available");
        L1MuonThresholdSet instance = new L1MuonThresholdSet();
        Integer input = 3;
        instance.set_rpc_available(input);
        Integer expResult = 3;
        Object result = instance.get_rpc_available();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_rpc_available_online() {
        System.out.println("get_rpc_available_online");
        L1MuonThresholdSet instance = new L1MuonThresholdSet();
        Integer expResult = 0;
        Object result = instance.get_rpc_available_online();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_rpc_available_online() {
        System.out.println("set_rpc_available_online");
        L1MuonThresholdSet instance = new L1MuonThresholdSet();
        Integer input = 3;
        instance.set_rpc_available_online(input);
        Integer expResult = 3;
        Object result = instance.get_rpc_available_online();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_rpc_set_ext_id() {
        System.out.println("get_rpc_set_ext_id");
        L1MuonThresholdSet instance = new L1MuonThresholdSet();
        Integer expResult = 0;
        Object result = instance.get_rpc_set_ext_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_rpc_set_ext_id() {
        System.out.println("set_rpc_set_ext_id");
        L1MuonThresholdSet instance = new L1MuonThresholdSet();
        Integer input = 3;
        instance.set_rpc_set_ext_id(input);
        Integer expResult = 3;
        Object result = instance.get_rpc_set_ext_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_rpc_set_name() {
        System.out.println("get_rpc_set_name");
        L1MuonThresholdSet instance = new L1MuonThresholdSet();
        String expResult = "RPC set name";
        Object result = instance.get_rpc_set_name();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_rpc_set_name() {
        System.out.println("set_rpc_set_name");
        L1MuonThresholdSet instance = new L1MuonThresholdSet();
        String input = "a string";
        instance.set_rpc_set_name(input);
        String expResult = "a string";
        Object result = instance.get_rpc_set_name();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_rpc_pt1_ext_id() {
        System.out.println("get_rpc_pt1_ext_id");
        L1MuonThresholdSet instance = new L1MuonThresholdSet();
        Integer expResult = 0;
        Object result = instance.get_rpc_pt1_ext_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_rpc_pt1_ext_id() {
        System.out.println("set_rpc_pt1_ext_id");
        L1MuonThresholdSet instance = new L1MuonThresholdSet();
        Integer input = 3;
        instance.set_rpc_pt1_ext_id(input);
        Integer expResult = 3;
        Object result = instance.get_rpc_pt1_ext_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_rpc_pt2_ext_id() {
        System.out.println("get_rpc_pt2_ext_id");
        L1MuonThresholdSet instance = new L1MuonThresholdSet();
        Integer expResult = 0;
        Object result = instance.get_rpc_pt2_ext_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_rpc_pt2_ext_id() {
        System.out.println("set_rpc_pt2_ext_id");
        L1MuonThresholdSet instance = new L1MuonThresholdSet();
        Integer input = 3;
        instance.set_rpc_pt2_ext_id(input);
        Integer expResult = 3;
        Object result = instance.get_rpc_pt2_ext_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_rpc_pt3_ext_id() {
        System.out.println("get_rpc_pt3_ext_id");
        L1MuonThresholdSet instance = new L1MuonThresholdSet();
        Integer expResult = 0;
        Object result = instance.get_rpc_pt3_ext_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_rpc_pt3_ext_id() {
        System.out.println("set_rpc_pt3_ext_id");
        L1MuonThresholdSet instance = new L1MuonThresholdSet();
        Integer input = 3;
        instance.set_rpc_pt3_ext_id(input);
        Integer expResult = 3;
        Object result = instance.get_rpc_pt3_ext_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_rpc_pt4_ext_id() {
        System.out.println("get_rpc_pt4_ext_id");
        L1MuonThresholdSet instance = new L1MuonThresholdSet();
        Integer expResult = 0;
        Object result = instance.get_rpc_pt4_ext_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_rpc_pt4_ext_id() {
        System.out.println("set_rpc_pt4_ext_id");
        L1MuonThresholdSet instance = new L1MuonThresholdSet();
        Integer input = 3;
        instance.set_rpc_pt4_ext_id(input);
        Integer expResult = 3;
        Object result = instance.get_rpc_pt4_ext_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_rpc_pt5_ext_id() {
        System.out.println("get_rpc_pt5_ext_id");
        L1MuonThresholdSet instance = new L1MuonThresholdSet();
        Integer expResult = 0;
        Object result = instance.get_rpc_pt5_ext_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_rpc_pt5_ext_id() {
        System.out.println("set_rpc_pt5_ext_id");
        L1MuonThresholdSet instance = new L1MuonThresholdSet();
        Integer input = 3;
        instance.set_rpc_pt5_ext_id(input);
        Integer expResult = 3;
        Object result = instance.get_rpc_pt5_ext_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_rpc_pt6_ext_id() {
        System.out.println("get_rpc_pt6_ext_id");
        L1MuonThresholdSet instance = new L1MuonThresholdSet();
        Integer expResult = 0;
        Object result = instance.get_rpc_pt6_ext_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_rpc_pt6_ext_id() {
        System.out.println("set_rpc_pt6_ext_id");
        L1MuonThresholdSet instance = new L1MuonThresholdSet();
        Integer input = 3;
        instance.set_rpc_pt6_ext_id(input);
        Integer expResult = 3;
        Object result = instance.get_rpc_pt6_ext_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_tgc_available() {
        System.out.println("get_tgc_available");
        L1MuonThresholdSet instance = new L1MuonThresholdSet();
        Integer expResult = 0;
        Object result = instance.get_tgc_available();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_tgc_available() {
        System.out.println("set_tgc_available");
        L1MuonThresholdSet instance = new L1MuonThresholdSet();
        Integer input = 3;
        instance.set_tgc_available(input);
        Integer expResult = 3;
        Object result = instance.get_tgc_available();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_tgc_available_online() {
        System.out.println("get_tgc_available_online");
        L1MuonThresholdSet instance = new L1MuonThresholdSet();
        Integer expResult = 0;
        Object result = instance.get_tgc_available_online();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_tgc_available_online() {
        System.out.println("set_tgc_available_online");
        L1MuonThresholdSet instance = new L1MuonThresholdSet();
        Integer input = 3;
        instance.set_tgc_available_online(input);
        Integer expResult = 3;
        Object result = instance.get_tgc_available_online();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_tgc_set_ext_id() {
        System.out.println("get_tgc_set_ext_id");
        L1MuonThresholdSet instance = new L1MuonThresholdSet();
        Integer expResult = 0;
        Object result = instance.get_tgc_set_ext_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_tgc_set_ext_id() {
        System.out.println("set_tgc_set_ext_id");
        L1MuonThresholdSet instance = new L1MuonThresholdSet();
        Integer input = 3;
        instance.set_tgc_set_ext_id(input);
        Integer expResult = 3;
        Object result = instance.get_tgc_set_ext_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_tgc_set_name() {
        System.out.println("get_tgc_set_name");
        L1MuonThresholdSet instance = new L1MuonThresholdSet();
        String expResult = "TGC set name";
        Object result = instance.get_tgc_set_name();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_tgc_set_name() {
        System.out.println("set_tgc_set_name");
        L1MuonThresholdSet instance = new L1MuonThresholdSet();
        String input = "a string";
        instance.set_tgc_set_name(input);
        String expResult = "a string";
        Object result = instance.get_tgc_set_name();
        assertEquals(expResult, result);
    }

    @Test
    public void testForceLoad() throws Exception {
    }

    @Test
    public void testSave() throws Exception {
        System.out.println("save");
        L1MuonThresholdSet instance = new L1MuonThresholdSet();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Integer expResult = 1;
        Object result = instance.save();
        assertEquals(expResult, result);
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        L1MuonThresholdSet instance = new L1MuonThresholdSet();
        String expResult= "L1 Muon Threshold Set";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
        
        L1MuonThresholdSet instance2 = getL1MuonThresholdSetWithID(2712);
        String expResult2 = "L1 MUON THRESHOLD SET: ID=2712, Name=L1 Muon Name, Version=1";
        Object Result2 = instance2.toString();
        assertEquals(expResult2, Result2);
    }

             /**
     * Helper function to make an HLTTE_TE and handle
     * SQLExceptions.
     * @param id
     * @return 
     */
    public L1MuonThresholdSet getL1MuonThresholdSetWithID(int id) {
        L1MuonThresholdSet result = null;
        try {
            result = new L1MuonThresholdSet(id);
        } catch (SQLException e) {
            fail(e.toString());
        }
        return result;
    }
    
    @Test
    public void testClone() {
        System.out.println("clone");
        L1MuonThresholdSet instance = new L1MuonThresholdSet();
        Object expResult = instance;
        Object result = instance.clone();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetTableName() {
        System.out.println("getTableName");
        L1MuonThresholdSet instance = new L1MuonThresholdSet();
        String expResult = "L1_MUON_THRESHOLD_SET";
        String result = instance.getTableName();
        assertEquals(expResult, result);
    }
    
}
