/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.L1;

import java.sql.SQLException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author davethomas
 */
public class L1PrescaleITest {
    
    public L1PrescaleITest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSet_val() throws Exception {
    }

    @Test
    public void testGet_val() {
    }

    @Test
    public void testGet_TMPSid() throws Exception {
    }

    @Test
    public void testGetHidden() throws Exception {
    }

    @Test
    public void testGet_default() throws Exception {
    }

    @Test
    public void testSet_default() throws Exception {
    }

    @Test
    public void testUpdate_comment() throws Exception {
    }

    @Test
    public void testSet_comment() throws Exception {
    }

    @Test
    public void testGet_comment() throws Exception {
    }

    public class L1PrescaleIImpl implements L1PrescaleI {

        public void set_val(int ctpId, Integer prescaleValue) throws SQLException {
        }

        public Integer get_val(int index) {
            return null;
        }

        public int get_TMPSid(int l1_menu_id) throws SQLException {
            return 0;
        }

        public boolean getHidden(int l1_menu_id) throws SQLException {
            return false;
        }

        public Integer get_default() throws SQLException {
            return null;
        }

        public void set_default(Integer def) throws SQLException {
        }

        public void update_comment(String _comment) throws SQLException {
        }

        public void set_comment(String inp) throws SQLException {
        }

        public String get_comment() throws SQLException {
            return "";
        }
    }
    
}
