package triggerdb.Entities.L1;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import static triggerdb.Entities.HLT.HLTPrescaleType.*;
import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.*;
import com.mockrunner.mock.jdbc.*;
import java.sql.PreparedStatement;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Logger;
import javax.swing.tree.DefaultMutableTreeNode;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.TriggerMap;
import triggerdb.Entities.alias.PrescaleSetAlias;

/**
 *
 * @author dwthomas
 */
public class L1BunchTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public L1BunchTest() {
    }
    
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
        ConnectionManager.getInstance().unsetTestingConnection();
        }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
        }


    @Test
    public void testGet_bunch_number() {
        System.out.println("get_bunch_number");
        L1Bunch instance = new L1Bunch();
        Integer expResult = 1;
        Object Result = instance.get_bunch_number();
        assertEquals(expResult,Result);
    }

    @Test
    public void testGet_bunch_group_id() {
        System.out.println("get_bunch_group_id");
        L1Bunch instance = new L1Bunch();
        Integer expResult = 1;
        Object Result = instance.get_bunch_group_id();
        assertEquals(expResult,Result);
    }

    @Test
    public void testSet_bunch_group_id() {
        System.out.println("set_bunch_group_id");
        L1Bunch instance = new L1Bunch();
        Integer input = 2712;
        instance.set_bunch_group_id(input);
        Object Result = instance.get_bunch_group_id();
        Integer expResult = input;
        assertEquals(expResult,Result);
    }

    @Test
    public void testSet_bunch_number() {
        System.out.println("set_bunch_number");
        L1Bunch instance = new L1Bunch();
        Integer input = 2712;
        instance.set_bunch_number(input);
        Object Result = instance.get_bunch_number();
        Integer expResult = input;
        assertEquals(expResult,Result);
    }

    @Test
    public void testSave() throws Exception {
         System.out.println("save");
         L1Bunch instance = new L1Bunch();
         MockResultSet resultset = resultSetHandler.createResultSet();
         setEmptyInitInfo();
         Integer expResult = 1;
         Object Result = instance.save();
         assertEquals (Result, expResult);
    }

    @Test
    public void testGetKey() {
        System.out.println("getKey");
        L1Bunch instance = new L1Bunch();
        TreeMap<String, Object> parametertree = new TreeMap<>();
        Object Result = instance.getKey();
        parametertree.put("BUNCH_GROUP_ID", 1);
        parametertree.put("BUNCH_NUMBER", 1);
        parametertree.put("ID", -1);
        Object expResult = parametertree;
        assertEquals(expResult,Result);
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        L1Bunch instance = new L1Bunch();
        String expResult = "L1 Bunch";
        String result = instance.toString();
        assertEquals(expResult, result);
        
        L1Bunch instance2 = getL1BunchWithID(-99);
        String expResult2 = "L1 BUNCH: id=-99, bunch_num=1";
        String result2 = instance2.toString();
        assertEquals(expResult2, result2);
    }

    @Test
    public void testClone() {
        //System.out.println("clone");
        //L1Bunch instance = new L1Bunch();
        //Object expResult = instance;
        //Object result = instance.clone();
        //assertEquals(expResult, result);
    }
   /**
     * Helper function to make a HLTParameter and handle
     * SQLExceptions.
     * @param id
     * @return 
     */
    public L1Bunch getL1BunchWithID(int id) {
        L1Bunch result = null;
        try {
            result = new L1Bunch(id);
        } catch (SQLException e) {
            fail(e.toString());
        }
        return result;
    }

    @Test
    public void testGet_min_names() {
        System.out.println("get_min_names");
        L1Bunch instance = getL1BunchWithID(-1992);
        
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("ID");
        expResult.add("Bunch Group ID");
        expResult.add("Bunch Number");
        
        ArrayList<String> result = instance.get_min_names();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_min_info() {
        
        System.out.println("get_min_info");
       L1Bunch instance = getL1BunchWithID(-2712);
        
        ArrayList<Object> expResult = new ArrayList<>();
        expResult.add(-2712);
        expResult.add(1);
        expResult.add(1);        
        
        ArrayList<Object> result = instance.get_min_info();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetTableName() {
        System.out.println("getTableName");
        L1Bunch instance = new L1Bunch();
        String expResult = "L1_BG_TO_B";
        String Result = instance.getTableName();
        assertEquals(expResult, Result);
    }
    
}
