package triggerdb.Entities.L1;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import static triggerdb.Entities.HLT.HLTPrescaleType.*;
import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.*;
import com.mockrunner.mock.jdbc.*;
import java.sql.PreparedStatement;
import java.util.AbstractMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Logger;
import javax.swing.tree.DefaultMutableTreeNode;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.TriggerMap;
import triggerdb.Entities.alias.PrescaleSetAlias;

/**
 *
 * @author dwthomas
 */
public class L1CaloIsoParamTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public L1CaloIsoParamTest() {
    }
    
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
        ConnectionManager.getInstance().unsetTestingConnection();
        }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
        }

    @Test
    public void testGet_iso_bit() {
        System.out.println("get_iso_bit");
        L1CaloIsoParam instance = new L1CaloIsoParam();
        Object Result = instance.get_iso_bit();
        Integer expResult = -1;
        assertEquals (Result, expResult); 
    }

    @Test
    public void testSet_iso_bit() {
        System.out.println("set_iso_bit");
        L1CaloIsoParam instance = new L1CaloIsoParam();
        Integer input = 27;
        instance.set_iso_bit(input);
        Object Result = instance.get_iso_bit();
        Object expResult = input;
        assertEquals (Result, expResult);
    }

    @Test
    public void testGet_offset() {
        System.out.println("get_offset");
        L1CaloIsoParam instance = new L1CaloIsoParam();
        Object Result = instance.get_offset();
        Integer expResult = -1;
        assertEquals (Result, expResult);
    }

    @Test
    public void testSet_offset() {
        System.out.println("set_offset");
        L1CaloIsoParam instance = new L1CaloIsoParam();
        Integer input = 27;
        instance.set_offset(input);
        Object Result = instance.get_offset();
        Object expResult = input;
        assertEquals (Result, expResult);
    }

    @Test
    public void testGet_slope() {
        System.out.println("get_slope");
        L1CaloIsoParam instance = new L1CaloIsoParam();
        Object Result = instance.get_slope();
        Integer expResult = -1;
        assertEquals (Result, expResult);
    }

    @Test
    public void testSet_slope() {
        System.out.println("set_slope");
        L1CaloIsoParam instance = new L1CaloIsoParam();
        Integer input = 27;
        instance.set_slope(input);
        Object Result = instance.get_slope();
        Object expResult = input;
        assertEquals (Result, expResult);
    }

    @Test
    public void testGet_min_cut() {
        System.out.println("get_min_cut");
        L1CaloIsoParam instance = new L1CaloIsoParam();
        Object Result = instance.get_min_cut();
        Integer expResult = -1;
        assertEquals (Result, expResult);
    }

    @Test
    public void testSet_min_cut() {
        System.out.println("set_min_cut");
        L1CaloIsoParam instance = new L1CaloIsoParam();
        Integer input = 27;
        instance.set_min_cut(input);
        Object Result = instance.get_min_cut();
        Object expResult = input;
        assertEquals (Result, expResult);
    }

    @Test
    public void testGet_upper_limit() {
        System.out.println("get_upper_limit");
        L1CaloIsoParam instance = new L1CaloIsoParam();
        Object Result = instance.get_upper_limit();
        Integer expResult = -1;
        assertEquals (Result, expResult);
    }

    @Test
    public void testSet_upper_limit() {
        System.out.println("set_upper_limit");
        L1CaloIsoParam instance = new L1CaloIsoParam();
        Integer input = 27;
        instance.set_upper_limit(input);
        Object Result = instance.get_upper_limit();
        Object expResult = input;
        assertEquals (Result, expResult);
    }

    @Test
    public void testGet_eta_min() {
        System.out.println("get_eta_min");
        L1CaloIsoParam instance = new L1CaloIsoParam();
        Object Result = instance.get_eta_min();
        Integer expResult = -1;
        assertEquals (Result, expResult);
    }

    @Test
    public void testSet_eta_min() {
        System.out.println("set_eta_min");
        L1CaloIsoParam instance = new L1CaloIsoParam();
        Integer input = 27;
        instance.set_eta_min(input);
        Object Result = instance.get_eta_min();
        Object expResult = input;
        assertEquals (Result, expResult);
    }

    @Test
    public void testGet_eta_max() {
        System.out.println("get_eta_max");
        L1CaloIsoParam instance = new L1CaloIsoParam();
        Object Result = instance.get_eta_max();
        Integer expResult = -1;
        assertEquals (Result, expResult);
    }

    @Test
    public void testSet_eta_max() {
        System.out.println("set_eta_max");
        L1CaloIsoParam instance = new L1CaloIsoParam();
        Integer input = 27;
        instance.set_eta_max(input);
        Object Result = instance.get_eta_max();
        Object expResult = input;
        assertEquals (Result, expResult);
    }

    @Test
    public void testGet_priority() {
        System.out.println("get_priority");
        L1CaloIsoParam instance = new L1CaloIsoParam();
        Object Result = instance.get_priority();
        Integer expResult = -1;
        assertEquals (Result, expResult);
    }

    @Test
    public void testSet_priority() {
        System.out.println("set_priority");
        L1CaloIsoParam instance = new L1CaloIsoParam();
        Integer input = 27;
        instance.set_priority(input);
        Object Result = instance.get_priority();
        Object expResult = input;
        assertEquals (Result, expResult);
    }

    @Test
    public void testSave() throws Exception {
        System.out.println("save");
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        L1CaloIsoParam instance = new L1CaloIsoParam();
        Integer expResult = 1;
        Object Result = instance.save();
        assertEquals(expResult,Result);
    }

    @Test
    public void testToString() {
    System.out.println("toString");
        L1CaloIsoParam instance = new L1CaloIsoParam();
        String expResult = "L1 Calo IsoParam";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
        
        L1CaloIsoParam instance2 = getL1CaloIsoParamWithID(-1000000);
        String expResult2 = "L1 CALO ISOPARAM: id=-1000000, iso bit=-1";
        String Result2 = instance2.toString();
        assertEquals(expResult2,Result2);
    }

    
           /**
     * Helper function to make an HLTTE_TE and handle
     * SQLExceptions.
     * @param id
     * @return 
     */
    public L1CaloIsoParam getL1CaloIsoParamWithID(int id) {
        L1CaloIsoParam result = null;
        try {
            result = new L1CaloIsoParam(id);
        } catch (SQLException e) {
            fail(e.toString());
        }
        return result;
    }

    @Test
    public void testClone() {
        System.out.println("clone");
        L1CaloIsoParam instance = new L1CaloIsoParam();
        Object expResult = instance;
        Object result = instance.clone();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetTableName() {
        System.out.println("getTableName");
        L1CaloIsoParam instance = new L1CaloIsoParam();
        String expResult = "L1_CALO_ISOPARAM";
        String result = instance.getTableName();
        assertEquals(expResult, result);
    }
    
    @Test
    public void testGet_min_names() {
        System.out.println("get_min_names");
        L1CaloIsoParam instance = new L1CaloIsoParam();
        
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("ID");
        expResult.add("Iso Bit");
        
        ArrayList<String> result = instance.get_min_names();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_min_info() {
        
        System.out.println("get_min_info");
        L1CaloIsoParam instance = new L1CaloIsoParam();
        
        ArrayList<Object> expResult = new ArrayList<>();
        expResult.add(-1);
        expResult.add(-1);
        
        ArrayList<Object> result = instance.get_min_info();
        assertEquals(expResult, result);
    }
    
}
