/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.L1;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import com.mockrunner.mock.jdbc.MockResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.Entities.HLT.HLTTriggerGroup;

/**
 *
 * @author davethomas
 */
public class L1RandomTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public L1RandomTest() {
    }
    
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @After
    public void tearDown() {
    }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
    }
    
    @Test
    public void testGet_cut0() {
        System.out.println("get_cut0");
        L1Random instance = new L1Random();
        Integer expResult = 0;
        Object result = instance.get_cut0();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_cut0() {
        System.out.println("set_cut0");
        L1Random instance = new L1Random();
        Integer input = 3;
        instance.set_cut0(input);
        Integer expResult = 3;
        Object result = instance.get_cut0();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_cut1() {
        System.out.println("get_cut1");
        L1Random instance = new L1Random();
        Integer expResult = 0;
        Object result = instance.get_cut1();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_cut1() {
        System.out.println("set_cut1");
        L1Random instance = new L1Random();
        Integer input = 3;
        instance.set_cut1(input);
        Integer expResult = 3;
        Object result = instance.get_cut1();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_cut2() {
        System.out.println("get_cut2");
        L1Random instance = new L1Random();
        Integer expResult = 0;
        Object result = instance.get_cut2();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_cut2() {
        System.out.println("set_cut2");
        L1Random instance = new L1Random();
        Integer input = 3;
        instance.set_cut2(input);
        Integer expResult = 3;
        Object result = instance.get_cut2();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_cut3() {
        System.out.println("get_cut3");
        L1Random instance = new L1Random();
        Integer expResult = 0;
        Object result = instance.get_cut3();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_cut3() {
        System.out.println("set_cut3");
        L1Random instance = new L1Random();
        Integer input = 3;
        instance.set_cut3(input);
        Integer expResult = 3;
        Object result = instance.get_cut3();
        assertEquals(expResult, result);
    }

    @Test
    public void testSave() throws Exception {
        System.out.println("save");
        L1Random instance = new L1Random();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Integer expResult = 1;
        Object result = instance.save();
        assertEquals(expResult, result);
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        L1Random instance = new L1Random();
        String expResult= "L1 Random";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
                
        L1Random instance2 = new L1Random();
        instance2.set_id(2712);
        String expResult2 = "L1 RANDOM: ID=2712, Cut 0=0, Cut 1=0, Cut 2=0, Cut 3=0";
        Object Result2 = instance2.toString();
        assertEquals(expResult2, Result2);
    }

    @Test
    public void testClone() {
        System.out.println("clone");
        L1Random instance = new L1Random();
        Object expResult = instance;
        Object result = instance.clone();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetTableName() {
        System.out.println("getTableName");
        L1Random instance = new L1Random();
        String expResult = "L1_RANDOM";
        String result = instance.getTableName();
        assertEquals(expResult, result);
    }
    
}
