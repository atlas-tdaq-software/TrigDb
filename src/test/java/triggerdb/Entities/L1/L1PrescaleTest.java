/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.L1;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import com.mockrunner.mock.jdbc.MockResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.Entities.HLT.HLTTriggerGroup;

/**
 *
 * @author davethomas
 */
public class L1PrescaleTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public L1PrescaleTest() {
    }
    
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @After
    public void tearDown() throws Exception {
        super.tearDown();
        ConnectionManager.getInstance().unsetTestingConnection();
    }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
    }
    
    @Test
    public void testGet_TMPSid() throws Exception {
        System.out.println("get_TMPSid");
        L1Prescale instance = new L1Prescale();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Integer expResult = -1;
        Integer input = 2712;
        Object result = instance.get_TMPSid(input);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetHidden() throws Exception {
        System.out.println("testGetHidden");
        L1Prescale instance = new L1Prescale();
        Integer input = 2712;
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Object Result = instance.getHidden(input);
        Boolean expResult = false;
        assertEquals(expResult, Result);
    }

    @Test
    public void testForceLoad() throws Exception {
    }

    @Test
    public void testGet_val() {
        System.out.println("get_val");
        L1Prescale instance = new L1Prescale();
        Integer expResult = -1;
        Object result = instance.get_val(5);
        assertEquals(expResult, result);  
    }

    @Test
    public void testSet_int_val() {
        System.out.println("set_int_val");
        L1Prescale instance = new L1Prescale();
        L1Prescale instance2 = new L1Prescale();
        final int input2 = 2712;
        final int expResult = 50000;
        instance2.set_int_val(input2, expResult);
        Object result2 = instance2.get_val(input2);
        Integer expResult2 = null;
        assertEquals(expResult2, result2);
        final int input = 27;
        instance.set_int_val(input, expResult);
        Object result = instance.get_val(input);
        assertEquals(expResult, result); 
    }

    @Test
    public void testSet_val() {
        //TO FINISH
        System.out.println("set_val");
        L1Prescale instance = new L1Prescale();
        L1Prescale instance2 = new L1Prescale();
        L1Prescale instance3 = new L1Prescale();
        L1Prescale instance4 = new L1Prescale();
        final int input = 27;
        final int input2 = 2712;
        final Integer cutval1 = 50000;
        final Integer cutval2 = 0x1000000;
        final Integer cutval3 = -0x1000000;
        final Integer cutval4 = 0;
        instance.set_val(input2, cutval1);
        Object result = instance.get_val(input2);
        Integer expResult = null;
        assertEquals(expResult, result);
        instance2.set_val(input, cutval2);
        Object result2 = instance2.get_val(input);
        Integer expResult2 = 0xFFFFFF;
        assertEquals(expResult2, result2);
        instance3.set_val(input, cutval3);
        Object result3 = instance3.get_val(input);
        Integer expResult3 = -0xFFFFFF;
        assertEquals(expResult3, result3);
        instance4.set_val(input, cutval4);
        Object result4 = instance4.get_val(input);
        Integer expResult4 = 1;
        assertEquals(expResult4, result4);
    }

    @Test
    public void testGet_shift_safe() {
        System.out.println("get_shift_safe");
        L1Prescale instance = new L1Prescale();
        Integer expResult = 0;
        Object result = instance.get_shift_safe();
        assertEquals(expResult, result);  
    }

    @Test
    public void testSet_shift_safe() {
        System.out.println("set_shift_safe");
        L1Prescale instance = new L1Prescale();
        Integer input = 3;
        instance.set_shift_safe(input);
        Integer expResult = 3;
        Object result = instance.get_shift_safe();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_default() {
        System.out.println("get_default");
        L1Prescale instance = new L1Prescale();
        Integer expResult = 0;
        Object result = instance.get_default();
        assertEquals(expResult, result);  
    }

    @Test
    public void testSet_default() {
        System.out.println("set_default");
        L1Prescale instance = new L1Prescale();
        Integer input = 3;
        instance.set_default(input);
        Integer expResult = 3;
        Object result = instance.get_default();
        assertEquals(expResult, result);
    }

    @Test
    public void testSave_0args() throws Exception {
        System.out.println("save_0args");
        L1Prescale instance = new L1Prescale();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Integer expResult = 1;
        Object result = instance.save();
        assertEquals(expResult, result);
    }

    @Test
    public void testSave_L1Menu() throws Exception {
        System.out.println("save_0args");
        L1Prescale instance = new L1Prescale();
        L1Menu param = new L1Menu();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Integer expResult = 1;
        Object result = instance.save(param);
        assertEquals(expResult, result);
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        L1Prescale instance = new L1Prescale();
        String expResult= " *: ";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
                
        L1Prescale instance2 = new L1Prescale();
        instance2.set_id(123);
        String expResult2 = "123:  v1";
        Object Result2 = instance2.toString();
        assertEquals(expResult2, Result2);

    }


    @Test
    public void testUpdate_comment() throws Exception {
        System.out.println("update_comment");
        L1Prescale instance = new L1Prescale();
        final String input = "a comment";
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        instance.update_comment(input);
        Object expResult = input;
        Object Result = instance.get_comment();
        assertEquals(expResult,Result);
        }

    @Test
    public void testSet_comment() {
        System.out.println("set_comment");
        L1Prescale instance = new L1Prescale();
        instance.set_comment("a string");
        String expResult = "a string";
        String result = instance.get_comment();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_comment() {
        System.out.println("get_comment");
        L1Prescale instance = new L1Prescale();
        String expResult = "";
        String result = instance.get_comment();
        assertEquals(expResult, result);
    }

    @Test
    public void testClone() {
        //USUAL TEST DOESN'T PASS!
    }

    @Test
    public void testLoadAll_0args() throws Exception {
        System.out.println("loadAll_0args");
        L1Prescale instance = new L1Prescale();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        ArrayList<L1Prescale> expResult = new ArrayList<>();
        Object Result = instance.loadAll();
        assertEquals(expResult, Result);
    }

    @Test
    public void testLoadAvailableSetNames_0args() throws Exception {
        System.out.println("loadAvailableSetNames_0args");
        L1Prescale instance = new L1Prescale();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        ArrayList<String> expResult = new ArrayList<>();
        Object Result = instance.loadAvailableSetNames();
        assertEquals(expResult, Result);
    }

    @Test
    public void testLoadAvailableSetNames_int() throws Exception {
        System.out.println("loadAvailableSetNames_int");
        L1Prescale instance = new L1Prescale();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        ArrayList<String> expResult = new ArrayList<>();
        Integer input = 50;
        Object Result = instance.loadAvailableSetNames(input);
        assertEquals(expResult, Result);
    }

    @Test
    public void testLoadAll_int() throws Exception {
        System.out.println("loadAll_int");
        L1Prescale instance = new L1Prescale();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        ArrayList<L1Prescale> expResult = new ArrayList<>();
        Integer input = 50;
        Object Result = instance.loadAll(input);
        assertEquals(expResult, Result);
    }

    @Test
    public void testGetTableName() {
            System.out.println("getTableName");
            L1Prescale instance = new L1Prescale();
            String expResult = "L1_PRESCALE_SET";
            String result = instance.getTableName();
            assertEquals(expResult, result);
    }

    @Test
    public void testSet_type() {
        System.out.println("set_type");
        L1Prescale instance = new L1Prescale();
        instance.set_type("a string");
        String expResult = "a string";
        String result = instance.get_type();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_type() {
        System.out.println("get_type");
        L1Prescale instance = new L1Prescale();
        String expResult = "";
        String result = instance.get_type();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_partition() {
        System.out.println("set_partition");
        L1Prescale instance = new L1Prescale();
        Integer input = 3;
        instance.set_partition(input);
        Integer expResult = 3;
        Object result = instance.get_partition();
        assertEquals(expResult, result);
        
    }

    @Test
    public void testGet_partition() {
        System.out.println("get_partition");
        L1Prescale instance = new L1Prescale();
        Integer expResult = 0;
        Object result = instance.get_partition();
        assertEquals(expResult, result);
    }
    
}
