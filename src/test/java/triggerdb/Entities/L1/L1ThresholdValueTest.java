/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.L1;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import com.mockrunner.mock.jdbc.MockResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.TriggerMap;
import triggerdb.Entities.HLT.HLTTriggerGroup;

/**
 *
 * @author davethomas
 */
public class L1ThresholdValueTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public L1ThresholdValueTest() {
    }
    
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @After
    public void tearDown() {
    }
    
    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
    }

    @Test
    public void testSet_type() {
        System.out.println("get_type");
        L1ThresholdValue instance = new L1ThresholdValue();
        String expResult = "type";
        Object result = instance.get_type();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_type() {
        System.out.println("set_type");
        L1ThresholdValue instance = new L1ThresholdValue();
        String input = "a string";
        instance.set_type(input);
        String expResult = "a string";
        Object result = instance.get_type();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_pt_cut() {
        System.out.println("get_pt_cut");
        L1ThresholdValue instance = new L1ThresholdValue();
        String expResult = "1.0";
        Object result = instance.get_pt_cut();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_pt_cut() {
        System.out.println("set_pt_cut");
        L1ThresholdValue instance = new L1ThresholdValue();
        String input = "a string";
        instance.set_pt_cut(input);
        String expResult = "a string";
        Object result = instance.get_pt_cut();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_eta_min() {
        System.out.println("get_eta_min");
        L1ThresholdValue instance = new L1ThresholdValue();
        Integer expResult = 1;
        Object result = instance.get_eta_min();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_eta_min() {
        System.out.println("set_eta_min");
        L1ThresholdValue instance = new L1ThresholdValue();
        Integer input = 3;
        instance.set_eta_min(input);
        Integer expResult = 3;
        Object result = instance.get_eta_min();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_eta_max() {
        System.out.println("get_eta_max");
        L1ThresholdValue instance = new L1ThresholdValue();
        Integer expResult = 1;
        Object result = instance.get_eta_max();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_eta_max() {
        System.out.println("get_eta_max");
        L1ThresholdValue instance = new L1ThresholdValue();
        Integer input = 3;
        instance.set_eta_max(input);
        Integer expResult = 3;
        Object result = instance.get_eta_max();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_phi_min() {
        System.out.println("get_phi_min");
        L1ThresholdValue instance = new L1ThresholdValue();
        Integer expResult = 1;
        Object result = instance.get_phi_min();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_phi_min() {
        System.out.println("get_phi_min");
        L1ThresholdValue instance = new L1ThresholdValue();
        Integer input = 3;
        instance.set_phi_min(input);
        Integer expResult = 3;
        Object result = instance.get_phi_min();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_phi_max() {
        System.out.println("get_phi_max");
        L1ThresholdValue instance = new L1ThresholdValue();
        Integer expResult = 1;
        Object result = instance.get_phi_max();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_phi_max() {
        System.out.println("get_phi_max");
        L1ThresholdValue instance = new L1ThresholdValue();
        Integer input = 3;
        instance.set_phi_max(input);
        Integer expResult = 3;
        Object result = instance.get_phi_max();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_em_isolation() {
        System.out.println("get_em_isolation");
        L1ThresholdValue instance = new L1ThresholdValue();
        String expResult = "1.0";
        Object result = instance.get_em_isolation();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_em_isolation() {
        System.out.println("set_em_isolation");
        L1ThresholdValue instance = new L1ThresholdValue();
        String input = "a string";
        instance.set_em_isolation(input);
        String expResult = "a string";
        Object result = instance.get_em_isolation();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_had_isolation() {
        System.out.println("get_had_isolation");
        L1ThresholdValue instance = new L1ThresholdValue();
        String expResult = "1.0";
        Object result = instance.get_had_isolation();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_had_isolation() {
        System.out.println("set_had_isolation");
        L1ThresholdValue instance = new L1ThresholdValue();
        String input = "a string";
        instance.set_had_isolation(input);
        String expResult = "a string";
        Object result = instance.get_had_isolation();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_had_veto() {
        System.out.println("get_had_veto");
        L1ThresholdValue instance = new L1ThresholdValue();
        String expResult = "1.0";
        Object result = instance.get_had_veto();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_had_veto() {
        System.out.println("set_had_veto");
        L1ThresholdValue instance = new L1ThresholdValue();
        String input = "a string";
        instance.set_had_veto(input);
        String expResult = "a string";
        Object result = instance.get_had_veto();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_window() {
        System.out.println("get_window");
        L1ThresholdValue instance = new L1ThresholdValue();
        Integer expResult = 1;
        Object result = instance.get_window();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_window() {
        System.out.println("get_window");
        L1ThresholdValue instance = new L1ThresholdValue();
        Integer input = 3;
        instance.set_window(input);
        Integer expResult = 3;
        Object result = instance.get_window();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_priority() {
        System.out.println("get_priority");
        L1ThresholdValue instance = new L1ThresholdValue();
        Integer expResult = 1;
        Object result = instance.get_priority();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_priority() {
        System.out.println("get_priority");
        L1ThresholdValue instance = new L1ThresholdValue();
        Integer input = 3;
        instance.set_priority(input);
        Integer expResult = 3;
        Object result = instance.get_priority();
        assertEquals(expResult, result);
    }

    @Test
    public void testSave() throws Exception {
        System.out.println("save");
        L1ThresholdValue instance = new L1ThresholdValue();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Integer expResult = 1;
        Object result = instance.save();
        assertEquals(expResult, result);
    }

    @Test
    public void testEquals() {
        System.out.println("equals");
        
        L1ThresholdValue instance = new L1ThresholdValue();
        L1ThresholdValue param1 = new L1ThresholdValue();
        L1ThresholdValue param2 = new L1ThresholdValue();
        Boolean expTrueResult = true;
        Boolean expFalseResult = false;
            
        String IncorrectType = "";
        Object Result1 = param1.equals(IncorrectType);
        assertEquals(Result1,expFalseResult);
        assertNotEquals(Result1,expTrueResult);
        
        instance.set_name("right name"); 
        param1.set_name("right name");
        param2.set_name("wrong name");
        Object TrueResult = param1.equals(instance);
        Object FalseResult = param2.equals(instance);
        assertEquals(TrueResult,expTrueResult);
        assertEquals(FalseResult,expFalseResult);
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        L1ThresholdValue instance = new L1ThresholdValue();
        String expResult= "L1 Threshold Value";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
                
        L1ThresholdValue instance2 = new L1ThresholdValue();
        instance2.set_id(2712);
        String expResult2 = "THRESHOLD VALUE:  (DBid=2712/V.1)";
        Object Result2 = instance2.toString();
        assertEquals(expResult2, Result2);
    }

    @Test
    public void testClone() {
        System.out.println("clone");
        L1ThresholdValue instance = new L1ThresholdValue();
        Object expResult = instance;
        Object result = instance.clone();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetKeyValue() {
        System.out.println("getKeyValue");
        L1ThresholdValue instance = new L1ThresholdValue();
        TriggerMap<String,Object> expResult = new TriggerMap<>();
        expResult.put("TYPE", "type");
        expResult.put("PT_CUT", "1.0");
        expResult.put("ETA_MIN", 1);
        expResult.put("ETA_MAX", 1);
        expResult.put("PHI_MIN", 1);
        expResult.put("PHI_MAX", 1);
        expResult.put("NAME", "");
        expResult.put("ID", -1);
        expResult.put("EM_ISOLATION", "1.0");
        expResult.put("HAD_ISOLATION", "1.0");
        expResult.put("HAD_VETO", "1.0");
        expResult.put("VERSION", 1);
        expResult.put("WINDOW", 1);
        expResult.put("PRIORITY", 1);
        Object result = instance.getKeyValue();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetTableName() {
        System.out.println("getTableName");
        L1ThresholdValue instance = new L1ThresholdValue();
        String expResult = "L1_TRIGGER_THRESHOLD_VALUE";
        String result = instance.getTableName();
        assertEquals(expResult, result);
    }
    
}
