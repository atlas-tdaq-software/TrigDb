/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.L1;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import com.mockrunner.mock.jdbc.MockResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.Entities.HLT.HLTPrescaleSet;

/**
 *
 * @author dwthomas
 */
public class L1MasterTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public L1MasterTest() {
    }
    
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
        ConnectionManager.getInstance().unsetTestingConnection();
    }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
    }
    
    @Test
    public void testCreate_new_links() {
    }

    @Test
    public void testForceLoad() throws Exception {
    }

    @Test
    public void testGet_muon_threshold_set_id() {
        System.out.println("get_muon_threshold_set_id");
        L1Master instance = new L1Master();
        Integer expResult = -1;
        Object result = instance.get_muon_threshold_set_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_muon_threshold_set_id() {
        System.out.println("get_muon_threshold_set_id");
        L1Master instance = new L1Master();
        Integer input = 3;
        instance.set_muon_threshold_set_id(input);
        Integer expResult = 3;
        Object result = instance.get_muon_threshold_set_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_menu_id() {
        System.out.println("get_menu_id");
        L1Master instance = new L1Master();
        Integer expResult = -1;
        Object result = instance.get_menu_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_menu_id() {
        System.out.println("set_menu_id");
        L1Master instance = new L1Master();
        Integer input = 3;
        instance.set_menu_id(input);
        Integer expResult = 3;
        Object result = instance.get_menu_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_muctpi_info_id() {
        System.out.println("get_muctpi_info_id");
        L1Master instance = new L1Master();
        Integer expResult = -1;
        Object result = instance.get_muctpi_info_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_muctpi_info_id() {
        System.out.println("set_muctpi_info_id");
        L1Master instance = new L1Master();
        Integer input = 3;
        instance.set_muctpi_info_id(input);
        Integer expResult = 3;
        Object result = instance.get_muctpi_info_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_random_id() {
        System.out.println("get_random_id");
        L1Master instance = new L1Master();
        Integer expResult = -1;
        Object result = instance.get_random_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_random_id() {
        System.out.println("set_random_id");
        L1Master instance = new L1Master();
        Integer input = 3;
        instance.set_random_id(input);
        Integer expResult = 3;
        Object result = instance.get_random_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_prescaled_clock_id() {
        System.out.println("get_prescaled_clock_id");
        L1Master instance = new L1Master();
        Integer expResult = -1;
        Object result = instance.get_prescaled_clock_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_prescaled_clock_id() {
        System.out.println("set_prescaled_clock_id");
        L1Master instance = new L1Master();
        Integer input = 3;
        instance.set_prescaled_clock_id(input);
        Integer expResult = 3;
        Object result = instance.get_prescaled_clock_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_calo_info_id() {
        System.out.println("get_calo_info_id");
        L1Master instance = new L1Master();
        Integer expResult = -1;
        Object result = instance.get_calo_info_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_calo_info_id() {
        System.out.println("set_calo_info_id");
        L1Master instance = new L1Master();
        Integer input = 3;
        instance.set_calo_info_id(input);
        Integer expResult = 3;
        Object result = instance.get_calo_info_id();
        assertEquals(expResult, result);
    }
/*
    @Test
    public void testGet_menu() {
        System.out.println("get_menu");
        L1Master instance = new L1Master();
        Object expResult = null;
        Object result = instance.get_menu();
        assertEquals(expResult, result);
    }
*/
    @Test
    public void testGet_muon_threshold_set() throws Exception {
        //FIX THIS!
        System.out.println("get_muon_threshold_set");
        L1Master instance = new L1Master();
        L1Master instance2 = new L1Master();
        Integer input = 3;
        instance2.set_muon_threshold_set_id(input);
        L1MuonThresholdSet param = new L1MuonThresholdSet();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        instance.forceLoad();
        instance2.forceLoad();
        Object expResult = null;
        Object result = instance.get_muon_threshold_set();
        Object result2 = instance2.get_muon_threshold_set();
        assertEquals(expResult, result);
        Object expResult2 = null;
        assertEquals(expResult2, result2); 
    }

    @Test
    public void testGet_random() throws Exception {
        //FIX THIS!
        System.out.println("get_random");
        L1Master instance = new L1Master();
        L1Master instance2 = new L1Master();
        Integer input = 3;
        instance2.set_random_id(input);
        L1Random param = new L1Random();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        instance.forceLoad();
        instance2.forceLoad();
        Object expResult = null;
        Object result = instance.get_random();
        Object result2 = instance2.get_random();
        assertEquals(expResult, result);
        Object expResult2 = null;
        assertEquals(expResult2, result2);       
    }

    @Test
    public void testGet_prescaled_clock() throws Exception {
        //FIX THIS!
        System.out.println("get_prescaled_clock");
        L1Master instance = new L1Master();
        L1Master instance2 = new L1Master();
        Integer input = 3;
        instance2.set_prescaled_clock_id(input);
        L1PrescaledClock param = new L1PrescaledClock();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        instance.forceLoad();
        instance2.forceLoad();
        Object expResult = null;
        Object result = instance.get_prescaled_clock();
        Object result2 = instance2.get_prescaled_clock();
        assertEquals(expResult, result);
        Object expResult2 = null;
        assertEquals(expResult2, result2);       
    }

    @Test
    public void testGet_muctpi_info() throws Exception {
        //FIX THIS!
        System.out.println("get_muctpi_info");
        L1Master instance = new L1Master();
        L1Master instance2 = new L1Master();
        Integer input = 3;
        instance2.set_muctpi_info_id(input);
        L1MuctpiInfo param = new L1MuctpiInfo();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        instance.forceLoad();
        instance2.forceLoad();
        Object expResult = null;
        Object result = instance.get_muctpi_info();
        Object result2 = instance2.get_muctpi_info();
        assertEquals(expResult, result);
        Object expResult2 = null;
        assertEquals(expResult2, result2);   
    }

    @Test
    public void testGet_calo_info() throws Exception {
        //FIX THIS!
        System.out.println("get_calo_info");
        L1Master instance = new L1Master();
        L1Master instance2 = new L1Master();
        Integer input = 3;
        instance2.set_calo_info_id(input);
        L1CaloInfo param = new L1CaloInfo();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        instance.forceLoad();
        instance2.forceLoad();
        Object expResult = null;
        Object result = instance.get_calo_info();
        Object result2 = instance2.get_calo_info();
        assertEquals(expResult, result);
        Object expResult2 = null;
        assertEquals(expResult2, result2);  
    }

    @Test
    public void testGet_ctpVersion() {
        System.out.println("get_ctpVersion");
        L1Master instance = new L1Master();
        Integer expResult = -1;
        Object result = instance.get_ctpVersion();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_ctpVersion() {
        System.out.println("set_ctpVersion");
        L1Master instance = new L1Master();
        Integer input = 3;
        instance.set_ctpVersion(input);
        Integer expResult = 3;
        Object result = instance.get_ctpVersion();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_l1Version() {
        System.out.println("get_l1Version");
        L1Master instance = new L1Master();
        Integer expResult = -1;
        Object result = instance.get_l1Version();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_l1Version() {
        System.out.println("set_l1Version");
        L1Master instance = new L1Master();
        Integer input = 3;
        instance.set_l1Version(input);
        Integer expResult = 3;
        Object result = instance.get_l1Version();
        assertEquals(expResult, result);
    }

    @Test
    public void testDoDiff() throws Exception {
    }
//WPV - disabled as test can't pass by comparing current times, which will differ
/*
    @Test
    public void testGet_min_info() throws Exception {
        System.out.println("get_min_info");
        L1Master instance = new L1Master();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Object Result = instance.get_min_info();
        Timestamp current = new Timestamp(System.currentTimeMillis());
        ArrayList<Object> expResult = new ArrayList<>();
        expResult.add(-1);
        expResult.add("");
        expResult.add(1);            
        expResult.add("");
        expResult.add("nobody");
        expResult.add(current);
        assertEquals(expResult, Result);
    }
*/
    @Test
    public void testGet_min_names() {
        System.out.println("get_min_names");
        L1Master instance = new L1Master();
        
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("ID");
        expResult.add("Name");
        expResult.add("Version");   
        expResult.add("Status");
        expResult.add("Creator");
        expResult.add("Date");                
        Object Result = instance.get_min_names();
        assertEquals(expResult, Result);
    }

    @Test
    public void testGetActiveId() throws Exception {
        System.out.println("getActiveId");
        L1Master instance = new L1Master();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Integer expResult = -1;
        Object result;
        result = instance.getActiveId();
        assertEquals(expResult, result);
    }

    @Test
    public void testSave() throws Exception {
        System.out.println("save");
        L1Master instance = new L1Master();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Integer expResult = 1;
        Object result = instance.save();
        assertEquals(expResult, result);
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        L1Master instance = new L1Master();
        String expResult= "L1 Master Table";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
        
        L1Master instance2 = getL1MasterWithID(2712);
        String expResult2 = "L1 MASTER: ID=2712, Name=, Version=1";
        Object Result2 = instance2.toString();
        assertEquals(expResult2, Result2);
    }

          /**
     * Helper function to make an HLTTE_TE and handle
     * SQLExceptions.
     * @param id
     * @return 
     */
    public L1Master getL1MasterWithID(int id) {
        L1Master result = null;
        try {
            result = new L1Master(id);
        } catch (SQLException e) {
            fail(e.toString());
        }
        return result;
    }

    @Test
    public void testAddToTree() throws Exception {
    }

    /*@Test
    public void testClone() {
        System.out.println("clone");
        L1Master instance = new L1Master();
        Object expResult = instance;
        Object result = instance.clone();
        assertEquals(expResult, result);
    }*/

    @Test
    public void testGet_status() {
        System.out.println("get_status");
        L1Master instance = new L1Master();
        Integer expResult = -1;
        Object result = instance.get_status();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_status() {
        System.out.println("set_status");
        L1Master instance = new L1Master();
        Integer input = 3;
        instance.set_status(input);
        Integer expResult = 3;
        Object result = instance.get_status();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_l1_menu() {
        System.out.println("set_l1_menu");
        L1Master instance = new L1Master();
        L1Menu param = new L1Menu();
        instance.set_l1_menu(param);
        Object Result = instance.get_l1_menu();
        Object expResult = param;
        assertEquals(expResult, Result);
    }

    @Test
    public void testSet_comment() {
        System.out.println("set_comment");
        L1Master instance = new L1Master();
        instance.set_comment("a string");
        String expResult = "a string";
        String result = instance.get_comment();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_comment() {
        System.out.println("get_comment");
        L1Master instance = new L1Master();
        String expResult = null;
        String result = instance.get_comment();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetL1Tables() throws Exception {
        System.out.println("getL1Tables");
        L1Master instance = new L1Master();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        List <L1Master> expResult = new ArrayList<>();
        Object result = instance.getL1Tables();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetTableName() {
        System.out.println("getTableName");
        L1Master instance = new L1Master();
        String expResult = "L1_MASTER_TABLE";
        String result = instance.getTableName();
        assertEquals(expResult, result);
    }

    @Test
    public void testFindDummy() {
        System.out.println("findDummy");
        L1Master instance = new L1Master();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Integer expResult = -1;
        Object result = instance.findDummy();
        assertEquals(expResult, result);
    }
    
}
