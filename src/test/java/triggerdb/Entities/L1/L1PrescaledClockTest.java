/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.L1;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import com.mockrunner.mock.jdbc.MockResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;

/**
 *
 * @author davethomas
 */
public class L1PrescaledClockTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public L1PrescaledClockTest() {
    }
    
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @After
    public void tearDown() {
    }
    
    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
    }

    @Test
    public void testSetKeys() {
    }

    @Test
    public void testGet_clock1() {
        System.out.println("get_clock1");
        L1PrescaledClock instance = new L1PrescaledClock();
        Integer expResult = 1;
        Object result = instance.get_clock1();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_clock2() {
        System.out.println("get_clock2");
        L1PrescaledClock instance = new L1PrescaledClock();
        Integer expResult = 2;
        Object result = instance.get_clock2();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_clock1() {
        System.out.println("set_clock1");
        L1PrescaledClock instance = new L1PrescaledClock();
        Integer input = 3;
        instance.set_clock1(input);
        Integer expResult = 3;
        Object result = instance.get_clock1();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_clock2() {
        System.out.println("set_clock2");
        L1PrescaledClock instance = new L1PrescaledClock();
        Integer input = 3;
        instance.set_clock2(input);
        Integer expResult = 3;
        Object result = instance.get_clock2();
        assertEquals(expResult, result);
    }

    @Test
    public void testSave() throws Exception {
        System.out.println("save");
        L1PrescaledClock instance = new L1PrescaledClock();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Integer expResult = 1;
        Object result = instance.save();
        assertEquals(expResult, result);
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        L1PrescaledClock instance = new L1PrescaledClock();
        String expResult= "L1 Prescaled Clock";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
                
        L1PrescaledClock instance2 = new L1PrescaledClock();
        instance2.set_id(2712);
        String expResult2 = "L1 PRESCALED CLOCK: id=2712, name=, ver=1";
        Object Result2 = instance2.toString();
        assertEquals(expResult2, Result2);
    }

    @Test
    public void testClone() throws CloneNotSupportedException {
        //USUAL TEST DOES NOT WORK!
    }

    @Test
    public void testGetTableName() {
            System.out.println("getTableName");
            L1PrescaledClock instance = new L1PrescaledClock();
            String expResult = "L1_PRESCALED_CLOCK";
            String result = instance.getTableName();
            assertEquals(expResult, result);
    }
    
}
