package triggerdb.Entities.L1;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import static triggerdb.Entities.HLT.HLTPrescaleType.*;
import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.*;
import com.mockrunner.mock.jdbc.*;
import java.sql.PreparedStatement;
import java.util.AbstractMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Logger;
import javax.swing.tree.DefaultMutableTreeNode;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.TriggerMap;
import triggerdb.Entities.alias.PrescaleSetAlias;

/**
 *
 * @author dwthomas
 */
public class L1CaloInfoTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public L1CaloInfoTest() {
    }
    
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @Override
    @After
    public void tearDown() {
    }
/*
    @Test
    public void testGet_global_em_scale() {
        System.out.println("get_global_em_scale");
         L1CaloInfo instance = new L1CaloInfo();
         Object Result = instance.get_global_em_scale();
         Double expResult = 1.0;
         assertEquals (Result, expResult); 
    }

    @Test
    public void testSet_global_em_scale() {
         System.out.println("set_global_em_scale");
         L1CaloInfo instance = new L1CaloInfo();
         Double input = 27.12;
         instance.set_global_em_scale(input);
         Object Result = instance.get_global_em_scale();
         Object expResult = input;
         assertEquals (Result, expResult); 
    }

    @Test
    public void testGet_global_jet_scale() {
        System.out.println("get_global_jet_scale");
        L1CaloInfo instance = new L1CaloInfo();
        Object Result = instance.get_global_jet_scale();
        Double expResult = 1.0;
        assertEquals (Result, expResult); 
    }

    @Test
    public void testSet_global_jet_scale() {
        System.out.println("set_global_jet_scale");
        L1CaloInfo instance = new L1CaloInfo();
        Double input = 27.12;
        instance.set_global_jet_scale(input);
        Object Result = instance.get_global_jet_scale();
        Object expResult = input;
        assertEquals (Result, expResult);
    }

    @Test
    public void testGet_xs_sigma_scale() {
        System.out.println("get_xs_sigma_scale");
        L1CaloInfo instance = new L1CaloInfo();
        Object Result = instance.get_xs_sigma_scale();
        Integer expResult = 0;
        assertEquals (Result, expResult); 
    }

    @Test
    public void testGet_xs_sigma_offset() {
        System.out.println("get_xs_sigma_offset");
        L1CaloInfo instance = new L1CaloInfo();
        Object Result = instance.get_xs_sigma_offset();
        Integer expResult = 0;
        assertEquals (Result, expResult); 
    }

    @Test
    public void testGet_xs_xe_min() {
        System.out.println("get_xs_xe_min");
        L1CaloInfo instance = new L1CaloInfo();
        Object Result = instance.get_xs_xe_min();
        Integer expResult = 0;
        assertEquals (Result, expResult); 
    }

    @Test
    public void testGet_xs_xe_max() {
        System.out.println("get_xs_xe_max");
        L1CaloInfo instance = new L1CaloInfo();
        Object Result = instance.get_xs_xe_max();
        Integer expResult = 0;
        assertEquals (Result, expResult); 
    }

    @Test
    public void testGet_xs_tesqrt_min() {
        System.out.println("get_xs_tesqrt_min");
        L1CaloInfo instance = new L1CaloInfo();
        Object Result = instance.get_xs_tesqrt_min();
        Integer expResult = 0;
        assertEquals (Result, expResult); 
    }

    @Test
    public void testGet_xs_tesqrt_max() {
        System.out.println("get_xs_tesqrt_max");
        L1CaloInfo instance = new L1CaloInfo();
        Object Result = instance.get_xs_tesqrt_max();
        Integer expResult = 0;
        assertEquals (Result, expResult); 
    }

    @Test
    public void testSet_xs_sigma_scale() {
        System.out.println("set_xs_sigma_scale");
        L1CaloInfo instance = new L1CaloInfo();
        Integer input = 27;
        instance.set_xs_sigma_scale(input);
        Object Result = instance.get_xs_sigma_scale();
        Object expResult = input;
        assertEquals (Result, expResult);
    }

    @Test
    public void testSet_xs_sigma_offset() {
        System.out.println("set_xs_sigma_offset");
        L1CaloInfo instance = new L1CaloInfo();
        Integer input = 27;
        instance.set_xs_sigma_offset(input);
        Object Result = instance.get_xs_sigma_offset();
        Object expResult = input;
        assertEquals (Result, expResult);
    }

    @Test
    public void testSet_xs_xe_min() {
        System.out.println("set_xs_xe_min");
        L1CaloInfo instance = new L1CaloInfo();
        Integer input = 27;
        instance.set_xs_xe_min(input);
        Object Result = instance.get_xs_xe_min();
        Object expResult = input;
        assertEquals (Result, expResult);
    }

    @Test
    public void testSet_xs_xe_max() {
        System.out.println("set_xs_xe_max");
        L1CaloInfo instance = new L1CaloInfo();
        Integer input = 27;
        instance.set_xs_xe_max(input);
        Object Result = instance.get_xs_xe_max();
        Object expResult = input;
        assertEquals (Result, expResult);
    }

    @Test
    public void testSet_xs_tesqrt_min() {
        System.out.println("set_xs_tesqrt_min");
        L1CaloInfo instance = new L1CaloInfo();
        Integer input = 27;
        instance.set_xs_tesqrt_min(input);
        Object Result = instance.get_xs_tesqrt_min();
        Object expResult = input;
        assertEquals (Result, expResult);
    }

    @Test
    public void testSet_xs_tesqrt_max() {
        System.out.println("set_xs_tesqrt_max");
        L1CaloInfo instance = new L1CaloInfo();
        Integer input = 27;
        instance.set_xs_tesqrt_max(input);
        Object Result = instance.get_xs_tesqrt_max();
        Object expResult = input;
        assertEquals (Result, expResult);
    }

    @Test
    public void testGet_min_tob_em() {
        System.out.println("get_min_tob_em");
        L1CaloInfo instance = new L1CaloInfo();
        Object Result = instance.get_min_tob_em();
        Integer expResult = -1;
        assertEquals (Result, expResult); 
    }

    @Test
    public void testSet_min_tob_em() {
        System.out.println("set_min_tob_em");
        L1CaloInfo instance = new L1CaloInfo();
        Integer input = 27;
        instance.set_min_tob_em(input);
        Object Result = instance.get_min_tob_em();
        Object expResult = input;
        assertEquals (Result, expResult);
    }

    @Test
    public void testGet_min_tob_tau() {
        System.out.println("get_min_tob_tau");
        L1CaloInfo instance = new L1CaloInfo();
        Object Result = instance.get_min_tob_tau();
        Integer expResult = -1;
        assertEquals (Result, expResult); 
    }

    @Test
    public void testSet_min_tob_tau() {
        System.out.println("set_min_tob_tau");
        L1CaloInfo instance = new L1CaloInfo();
        Integer input = 27;
        instance.set_min_tob_tau(input);
        Object Result = instance.get_min_tob_tau();
        Object expResult = input;
        assertEquals (Result, expResult);
    }

    @Test
    public void testGet_min_tob_jets() {
        System.out.println("get_min_tob_jets");
        L1CaloInfo instance = new L1CaloInfo();
        Object Result = instance.get_min_tob_jets();
        Integer expResult = -1;
        assertEquals (Result, expResult); 
    }

    @Test
    public void testSet_min_tob_jets() {
        System.out.println("set_min_tob_jets");
        L1CaloInfo instance = new L1CaloInfo();
        Integer input = 27;
        instance.set_min_tob_jets(input);
        Object Result = instance.get_min_tob_jets();
        Object expResult = input;
        assertEquals (Result, expResult);
    }

    @Test
    public void testGet_min_tob_jetl() {
        System.out.println("get_min_tob_jetl");
        L1CaloInfo instance = new L1CaloInfo();
        Object Result = instance.get_min_tob_jetl();
        Integer expResult = -1;
        assertEquals (Result, expResult); 
    }

    @Test
    public void testSet_min_tob_jetl() {
        System.out.println("set_min_tob_jetl");
        L1CaloInfo instance = new L1CaloInfo();
        Integer input = 27;
        instance.set_min_tob_jetl(input);
        Object Result = instance.get_min_tob_jetl();
        Object expResult = input;
        assertEquals (Result, expResult);
    }

    @Test
    public void testGet_all_tobs() throws Exception {
        System.out.println("get_all_tobs");
        L1CaloInfo instance = new L1CaloInfo();
        L1CaloMinTob em = new L1CaloMinTob(-1);
        L1CaloMinTob tau = new L1CaloMinTob(-1);
        L1CaloMinTob jets = new L1CaloMinTob(-1);
        L1CaloMinTob jetl = new L1CaloMinTob(-1);
        ArrayList<L1CaloMinTob> expResult = new ArrayList<>();
        expResult.add(em);
        expResult.add(tau);
        expResult.add(jets);
        expResult.add(jetl);
        Object Result = instance.get_all_tobs();
        assertEquals(expResult, Result);
    }

    @Test
    public void testGet_iso_ha_em() {
        System.out.println("get_iso_ha_em");
        L1CaloInfo instance = new L1CaloInfo();
        Object Result = instance.get_iso_ha_em();
        Integer expResult = -1;
        assertEquals (Result, expResult); 
    }

    @Test
    public void testSet_iso_ha_em() {
        System.out.println("set_iso_ha_em");
        L1CaloInfo instance = new L1CaloInfo();
        Integer input = 27;
        instance.set_iso_ha_em(input);
        Object Result = instance.get_iso_ha_em();
        Object expResult = input;
        assertEquals (Result, expResult);
    }

    @Test
    public void testGet_iso_em_em() {
        System.out.println("get_iso_em_em");
        L1CaloInfo instance = new L1CaloInfo();
        Object Result = instance.get_iso_em_em();
        Integer expResult = -1;
        assertEquals (Result, expResult); 
    }

    @Test
    public void testSet_iso_em_em() {
        System.out.println("set_iso_em_em");
        L1CaloInfo instance = new L1CaloInfo();
        Integer input = 27;
        instance.set_iso_em_em(input);
        Object Result = instance.get_iso_em_em();
        Object expResult = input;
        assertEquals (Result, expResult);
    }

    @Test
    public void testGet_iso_em_tau() {
        System.out.println("get_iso_em_tau");
        L1CaloInfo instance = new L1CaloInfo();
        Object Result = instance.get_iso_em_tau();
        Integer expResult = -1;
        assertEquals (Result, expResult); 
    }

    @Test
    public void testSet_iso_em_tau() {
        System.out.println("set_iso_em_tau");
        L1CaloInfo instance = new L1CaloInfo();
        Integer input = 27;
        instance.set_iso_em_tau(input);
        Object Result = instance.get_iso_em_tau();
        Object expResult = input;
        assertEquals (Result, expResult);
    }

    @Test
    public void testGet_all_isolations() throws Exception {
        System.out.println("get_all_isolations");
        L1CaloInfo instance = new L1CaloInfo();
        L1CaloIsolation HE = new L1CaloIsolation(-1);
        L1CaloIsolation EE = new L1CaloIsolation(-1);
        L1CaloIsolation ET = new L1CaloIsolation(-1);
        ArrayList<L1CaloIsolation> expResult = new ArrayList<>();
        expResult.add(HE);
        expResult.add(EE);
        expResult.add(ET);
        Object Result = instance.get_all_isolations();
        assertEquals(expResult, Result);
    }

    @Test
    public void testGet_xe_red_eta_min() {
        System.out.println("get_xe_red_eta_min");
        L1CaloInfo instance = new L1CaloInfo();
        Object Result = instance.get_xe_red_eta_min();
        Integer expResult = null;
        assertEquals (Result, expResult); 
    }

    @Test
    public void testSet_xe_red_eta_min() {
        System.out.println("set_xe_red_eta_min");
        L1CaloInfo instance = new L1CaloInfo();
        Integer input = 27;
        instance.set_xe_red_eta_min(input);
        Object Result = instance.get_xe_red_eta_min();
        Object expResult = input;
        assertEquals (Result, expResult);
    }

    @Test
    public void testGet_xe_red_eta_max() {
        System.out.println("get_xe_red_eta_max");
        L1CaloInfo instance = new L1CaloInfo();
        Object Result = instance.get_xe_red_eta_max();
        Integer expResult = null;
        assertEquals (Result, expResult); 
    }

    @Test
    public void testSet_xe_red_eta_max() {
        System.out.println("set_xe_red_eta_max");
        L1CaloInfo instance = new L1CaloInfo();
        Integer input = 27;
        instance.set_xe_red_eta_max(input);
        Object Result = instance.get_xe_red_eta_max();
        Object expResult = input;
        assertEquals (Result, expResult);
    }
*/
/*    @Test
    public void testSave() throws Exception {        
        System.out.println("save");
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        L1CaloInfo instance = new L1CaloInfo();
        Integer expResult = 1;
        Object Result = instance.save();
        assertEquals(expResult,Result);
        
    }
*/
    @Test
    public void testAddToTree() throws Exception {
    }

    @Test
    public void testDoDiff() throws Exception {
    }

  /*  @Test
    public void testToString() {
        System.out.println("toString");
        L1CaloInfo instance = new L1CaloInfo();
        String expResult = "L1 Calo Info";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
        
        L1CaloInfo instance2 = getL1CaloInfoWithID(-1000000);
        String expResult2 = "L1 CALO INFO: ID=-1000000, Name=, Version=1";
        String Result2 = instance2.toString();
        assertEquals(expResult2,Result2);
    }*/

    
           /**
     * Helper function to make an HLTTE_TE and handle
     * SQLExceptions.
     * @param id
     * @return 
     */
    public L1CaloInfo getL1CaloInfoWithID(int id) {
        L1CaloInfo result = null;
        try {
            result = new L1CaloInfo(id);
        } catch (SQLException e) {
            fail(e.toString());
        }
        return result;
    }
    /*
    @Test
    public void testClone() {
        System.out.println("clone");
        L1CaloInfo instance = new L1CaloInfo();
        Object expResult = instance;
        Object result = instance.clone();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetCaloSinCos() {
        System.out.println("getCaloSinCos");
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        L1CaloInfo instance = new L1CaloInfo();
        instance.set_id(27);
        ArrayList<L1CaloSinCos> expResult = new ArrayList();
        Object Result = instance.getCaloSinCosPublic();
        assertEquals(expResult,Result);
    }
    @Test
    public void testGetTableName() {
        System.out.println("getTableName");
        L1CaloInfo instance = new L1CaloInfo();
        String expResult = "L1_CALO_INFO";
        String result = instance.getTableName();
        assertEquals(expResult, result);
    }
    */
}
