/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.L1;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import com.mockrunner.mock.jdbc.MockResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;

/**
 *
 * @author dwthomas
 */
public class L1MuctpiInfoTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public L1MuctpiInfoTest() {
    }
    
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @After
    public void tearDown() {
    }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
    }
    
    @Test
    public void testGet_low_pt() {
        System.out.println("get_low_pt");
        L1MuctpiInfo instance = new L1MuctpiInfo();
        Integer expResult = 1;
        Object result = instance.get_low_pt();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_high_pt() {
        System.out.println("get_high_pt");
        L1MuctpiInfo instance = new L1MuctpiInfo();
        Integer expResult = 1;
        Object result = instance.get_high_pt();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_max_cand() {
        System.out.println("get_max_cand");
        L1MuctpiInfo instance = new L1MuctpiInfo();
        Integer expResult = 1;
        Object result = instance.get_max_cand();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_low_pt() {
        System.out.println("set_low_pt");
        L1MuctpiInfo instance = new L1MuctpiInfo();
        Integer input = 3;
        instance.set_low_pt(input);
        Integer expResult = 3;
        Object result = instance.get_low_pt();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_high_pt() {
        System.out.println("set_high_pt");
        L1MuctpiInfo instance = new L1MuctpiInfo();
        Integer input = 3;
        instance.set_high_pt(input);
        Integer expResult = 3;
        Object result = instance.get_high_pt();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_max_cand() {
        System.out.println("set_max_cand");
        L1MuctpiInfo instance = new L1MuctpiInfo();
        Integer input = 3;
        instance.set_max_cand(input);
        Integer expResult = 3;
        Object result = instance.get_max_cand();
        assertEquals(expResult, result);
    }

    @Test
    public void testSave() throws Exception {
        System.out.println("save");
        L1MuctpiInfo instance = new L1MuctpiInfo();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Integer expResult = 1;
        Object result = instance.save();
        assertEquals(expResult, result);
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        L1MuctpiInfo instance = new L1MuctpiInfo();
        String expResult= "L1 Muctpi Info";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
        
        L1MuctpiInfo instance2 = getL1MuctpiInfoWithID(2712);
        String expResult2 = "L1 MUCTPI INFO: ID=2712, Name=, Version=1";
        Object Result2 = instance2.toString();
        assertEquals(expResult2, Result2);
    }

              /**
     * Helper function to make an HLTTE_TE and handle
     * SQLExceptions.
     * @param id
     * @return 
     */
    public L1MuctpiInfo getL1MuctpiInfoWithID(int id) {
        L1MuctpiInfo result = null;
        try {
            result = new L1MuctpiInfo(id);
        } catch (SQLException e) {
            fail(e.toString());
        }
        return result;
    }
    
    @Test
    public void testClone() {
        System.out.println("clone");
        L1MuctpiInfo instance = new L1MuctpiInfo();
        Object expResult = instance;
        Object result = instance.clone();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetTableName() {
        System.out.println("getTableName");
        L1MuctpiInfo instance = new L1MuctpiInfo();
        String expResult = "L1_MUCTPI_INFO";
        String result = instance.getTableName();
        assertEquals(expResult, result);
    }
    
}
