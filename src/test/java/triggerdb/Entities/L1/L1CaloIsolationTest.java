package triggerdb.Entities.L1;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import static triggerdb.Entities.HLT.HLTPrescaleType.*;
import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.*;
import com.mockrunner.mock.jdbc.*;
import java.sql.PreparedStatement;
import java.util.AbstractMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Logger;
import javax.swing.tree.DefaultMutableTreeNode;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.TriggerMap;
import triggerdb.Entities.alias.PrescaleSetAlias;

/**
 *
 * @author dwthomas
 */
public class L1CaloIsolationTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public L1CaloIsolationTest() {
    }
    
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
        ConnectionManager.getInstance().unsetTestingConnection();
        }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
        }

    @Test
    public void testGet_thr_type() {
        System.out.println("get_thr_type");
        L1CaloIsolation instance = new L1CaloIsolation();
        Object Result = instance.get_thr_type();
        String expResult = "thr type";
        assertEquals (Result, expResult);
    }

    @Test
    public void testSet_thr_type() {
        System.out.println("get_thr_type");
        L1CaloIsolation instance = new L1CaloIsolation();
        String input = "something";
        instance.set_thr_type(input);
        Object Result = instance.get_thr_type();
        String expResult = input;
        assertEquals (Result, expResult);
    }

    @Test
    public void testGet_par() {
        System.out.println("get_par");
        L1CaloIsolation instance = new L1CaloIsolation();
        Integer input = 27;
        Integer index = 1;
        instance.set_par(index, input);
        Object Result = instance.get_par(index);
        Integer expResult = input;
        assertEquals (Result, expResult);
    }

    @Test
    public void testSet_par() {
        System.out.println("set_par");
        L1CaloIsolation instance = new L1CaloIsolation();
        Integer input = 27;
        Integer index = 1;
        instance.set_par(index, input);
        Object Result = instance.get_par(index);
        Integer expResult = input;
        assertEquals (Result, expResult);
    }

    @Test
    public void testSave() throws Exception {
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        L1CaloIsolation instance = new L1CaloIsolation();
        String expResult = "L1 Calo Isolation";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
        
        L1CaloIsolation instance2 = getL1CaloIsolationWithID(-1000000);
        String expResult2 = "L1 CALO ISOLATION: id=-1000000, threshold=thr type";
        String Result2 = instance2.toString();
        assertEquals(expResult2,Result2);
    }

    
           /**
     * Helper function to make an HLTTE_TE and handle
     * SQLExceptions.
     * @param id
     * @return 
     */
    public L1CaloIsolation getL1CaloIsolationWithID(int id) {
        L1CaloIsolation result = null;
        try {
            result = new L1CaloIsolation(id);
        } catch (SQLException e) {
            fail(e.toString());
        }
        return result;
    }

    @Test
    public void testClone() {
        //System.out.println("clone");
        //L1CaloIsolation instance = new L1CaloIsolation();
        //Object expResult = instance;
        //Object result = instance.clone();
        //assertEquals(expResult, result);
    }

    @Test
    public void testGetTableName() {
        System.out.println("getTableName");
        L1CaloIsolation instance = new L1CaloIsolation();
        String expResult = "L1_CALO_ISOLATION";
        String result = instance.getTableName();
        assertEquals(expResult, result);
    }
    
}
