package triggerdb.Entities.HLT;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import static triggerdb.Entities.HLT.HLTPrescaleType.*;
import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.*;
import com.mockrunner.mock.jdbc.*;
import java.sql.PreparedStatement;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Logger;
import javax.swing.tree.DefaultMutableTreeNode;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.TriggerMap;
import triggerdb.Entities.alias.PrescaleSetAlias;

/**
 *
 * @author dwthomas
 */
public class HLTTriggerStreamTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public HLTTriggerStreamTest() {
    }
    
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
        ConnectionManager.getInstance().unsetTestingConnection();
        }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
        }

    @Test
    public void testClone() {
        System.out.println("clone");
        HLTTriggerStream instance = new HLTTriggerStream();
        Object expResult = instance;
        Object result = instance.clone();
        assertEquals(expResult, result);
    }

    @Test
    public void testSave() throws Exception {
         System.out.println("save");
         HLTTriggerStream instance = new HLTTriggerStream();
         MockResultSet resultset = resultSetHandler.createResultSet();
         setEmptyInitInfo();
         Integer expResult = 1;
         Object Result = instance.save();
         assertEquals (Result, expResult);
    }

    @Test
    public void testEquals() {
        System.out.println("equals");
        HLTTriggerStream param1 = new HLTTriggerStream();
        param1.set_id(-2712);       
        assertNotEquals(param1, -2712);
        
        HLTTriggerStream param2 = new HLTTriggerStream();
        param2.set_id(-2712);        
        assertEquals(param1, param2);
        
        HLTTriggerStream param3 = new HLTTriggerStream();
        assertNotEquals(param1, param3);
       }

    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        HLTTriggerStream instance = new HLTTriggerStream();
        instance.set_id(-2712);
        
        int expResult = -1536775093;
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }

    @Test
    public void testDoDiff() throws Exception {
    }

    @Test
    public void testGet_obeyLB() {
        System.out.println("get_obeyLB");
        HLTTriggerStream instance = new HLTTriggerStream();
        Integer expResult = 0;
        Object Result = instance.get_obeyLB();
        assertEquals(expResult,Result);
    }

    @Test
    public void testSet_obeyLB() {
        System.out.println("set_obeyLB");
        HLTTriggerStream instance = new HLTTriggerStream();
        Integer input = 3;
        instance.set_obeyLB(input);
        Object Result = instance.get_obeyLB();
        Integer expResult = 3;
        assertEquals(Result,expResult);
    }

    @Test
    public void testGet_description() {
        System.out.println("get_description");
        HLTTriggerStream instance = new HLTTriggerStream();
        String expResult = "";
        Object Result = instance.get_description();
        assertEquals(expResult,Result);
    }

    @Test
    public void testSet_description() {
        System.out.println("set_description");
        HLTTriggerStream instance = new HLTTriggerStream();
        String input = "a descriptive string";
        instance.set_description(input);
        Object Result = instance.get_description();
        String expResult = "a descriptive string";
        assertEquals(Result,expResult);
    }

    @Test
    public void testGet_type() {
        System.out.println("get_type");
        HLTTriggerStream instance = new HLTTriggerStream();
        String expResult = "";
        Object Result = instance.get_type();
        assertEquals(expResult,Result);
    }

    @Test
    public void testSet_stream_prescale() {
        System.out.println("set_stream_prescale");
        HLTTriggerStream instance = new HLTTriggerStream();
        String input = "prescale";
        instance.set_stream_prescale(input);
        Object Result = instance.get_stream_prescale();
        String expResult = "prescale";
        assertEquals(Result,expResult);
    }

    @Test
    public void testGet_stream_prescale() {
        System.out.println("get_stream_prescale");
        HLTTriggerStream instance = new HLTTriggerStream();
        Object Result = instance.get_stream_prescale();
        String expResult = "";
        assertEquals(Result,expResult);
    }

    @Test
    public void testSet_type() {
        System.out.println("set_type");
        HLTTriggerStream instance = new HLTTriggerStream();
        String input = "a typical string";
        instance.set_type(input);
        Object Result = instance.get_type();
        String expResult = "a typical string";
        assertEquals(Result,expResult);
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        HLTTriggerStream instance = new HLTTriggerStream();
        String expResult = "HLT Stream";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
        
        HLTTriggerStream instance2 = getHLTTriggerStreamWithID(-1000000);
        String expResult2 = "STREAM: _ (DBid=-1000000)";
        String Result2 = instance2.toString();
        assertEquals(expResult2,Result2);
    }

    
       /**
     * Helper function to make an HLTTE_TE and handle
     * SQLExceptions.
     * @param id
     * @return 
     */
    public HLTTriggerStream getHLTTriggerStreamWithID(int id) {
        HLTTriggerStream result = null;
        try {
            result = new HLTTriggerStream(id);
        } catch (SQLException e) {
            fail(e.toString());
        }
        return result;
    }
    
    
    
    @Test
    public void testGet_min_names() {
        System.out.println("get_min_names");
        HLTTriggerStream instance = getHLTTriggerStreamWithID(-2712);
        
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("ID");
        expResult.add("Name");
                
        Object Result = instance.get_min_names();
        assertEquals(expResult, Result);
    }

    @Test
    public void testGet_min_info() {
        System.out.println("get_min_info");
        HLTTriggerStream instance = getHLTTriggerStreamWithID(-2712);
        
        ArrayList<Object> expResult = new ArrayList<>();
        expResult.add(-2712);
        expResult.add("");
                
        Object Result = instance.get_min_info();
        assertEquals(expResult, Result);
    }

    @Test
    public void testGetTableName() {
        System.out.println("getTableName");
        HLTTriggerStream instance = new HLTTriggerStream();
        String expResult = "HLT_TRIGGER_STREAM";
        String result = instance.getTableName();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetChains() throws Exception {
         System.out.println("getChains");
         HLTTriggerStream instance = new HLTTriggerStream();
         MockResultSet resultset = resultSetHandler.createResultSet();
         setEmptyInitInfo();
         ArrayList<HLTTriggerChain> expResult = new ArrayList<>();
         Object Result = instance.getChains(27);
         assertEquals (Result, expResult);
    }

    
}
