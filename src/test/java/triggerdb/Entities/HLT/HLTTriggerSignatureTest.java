package triggerdb.Entities.HLT;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import static triggerdb.Entities.HLT.HLTPrescaleType.*;
import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.*;
import com.mockrunner.mock.jdbc.*;
import java.sql.PreparedStatement;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Logger;
import javax.swing.tree.DefaultMutableTreeNode;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.TriggerMap;
import triggerdb.Entities.alias.PrescaleSetAlias;

/**
 *
 * @author dwthomas
 */
public class HLTTriggerSignatureTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public HLTTriggerSignatureTest() {
    }
    
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
        ConnectionManager.getInstance().unsetTestingConnection();
        }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
        }

    @Test
    public void testRenumberElements() throws Exception {
    }

    @Test
    public void testForceLoad() throws Exception {
    }

    @Test
    public void testGetElements() {
        System.out.println("getElements");
        HLTTriggerSignature instance = new HLTTriggerSignature();
        Object Result = instance.getElements();
        ArrayList<HLTTriggerElement> expResult = new ArrayList<>();
        assertEquals(Result,expResult);
    }

    @Test
    public void testSetElements() {
        System.out.println("setElements");
        HLTTriggerSignature instance = new HLTTriggerSignature();
        ArrayList<HLTTriggerElement> input = new ArrayList<>();
        HLTTriggerElement param = new HLTTriggerElement();
        input.add(param);
        instance.setElements(input);
        Object Result = instance.getElements();
        ArrayList<HLTTriggerElement> expResult = new ArrayList<>();
        expResult.add(param);
        assertEquals(Result,expResult);
    }
    
    @Test
    public void testGetElementsFromDb() throws SQLException {
        System.out.println("getElementsFromDb");
        HLTTriggerSignature instance = new HLTTriggerSignature();
        instance.set_id(27);
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Object Result = instance.getElementsFromDbPublic();
        ArrayList<HLTTriggerElement> expResult = new ArrayList<>();
        assertEquals(Result,expResult);
    }

    @Test
    public void testGet_logic() {
        System.out.println("get_logic");
        HLTTriggerSignature instance = new HLTTriggerSignature();
        Integer expResult = -1;
        Object Result = instance.get_logic();
        assertEquals(expResult,Result);
    }

    @Test
    public void testSet_logic() {
        System.out.println("set_logic");
        HLTTriggerSignature instance = new HLTTriggerSignature();
        Integer input = 3;
        instance.set_logic(input);
        Object Result = instance.get_logic();
        Integer expResult = 3;
        assertEquals(Result,expResult);
    }

    @Test
    public void testGet_signature_counter() {
        System.out.println("get_signature_counter");
        HLTTriggerSignature instance = new HLTTriggerSignature();
        Integer expResult = -1;
        Object Result = instance.get_signature_counter();
        assertEquals(expResult,Result);
    }

    @Test
    public void testGet_trigger_element_names() throws Exception {
        System.out.println("get_trigger_element_names");
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        HLTTriggerSignature instance = new HLTTriggerSignature();
        ArrayList<HLTTriggerElement> input = new ArrayList<>();
        HLTTriggerElement param = new HLTTriggerElement();
        input.add(param);
        instance.setElements(input);
        String expResult = "";
        Object Result = instance.get_trigger_element_names();
        assertEquals(expResult,Result);
    }

    @Test
    public void testAddToTree() throws Exception {
    }

    @Test
    public void testClone() {
        System.out.println("clone");
        HLTTriggerSignature instance = new HLTTriggerSignature();
        Object expResult = instance;
        ArrayList<HLTTriggerElement> input = new ArrayList<>();
        HLTTriggerElement param = new HLTTriggerElement();
        input.add(param);
        instance.setElements(input);
        Object result = instance.clone();
        assertEquals(expResult, result);
    }

    @Test
    public void testDoDiff() throws Exception {
    }

    @Test
    public void testSave() throws Exception {
         System.out.println("save");
         HLTTriggerSignature instance = new HLTTriggerSignature();
         MockResultSet resultset = resultSetHandler.createResultSet();
         setEmptyInitInfo();
         ArrayList<HLTTriggerElement> input = new ArrayList<>();
         HLTTriggerElement param = new HLTTriggerElement();
         HLTTriggerElement param2 = new HLTTriggerElement();
         param2.set_element_counter(27);
         input.add(param);
         input.add(param2);
         instance.setElements(input);
         Integer expResult = 1;
         Object Result = instance.save();
         assertEquals (Result, expResult);

    }

    @Test
    public void testSet_signature_counter() {
        System.out.println("set_signature_counter");
        HLTTriggerSignature instance = new HLTTriggerSignature();
        Integer input = 3;
        instance.set_signature_counter(input);
        Object Result = instance.get_signature_counter();
        Integer expResult = 3;
        assertEquals(Result,expResult);
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        HLTTriggerSignature instance = new HLTTriggerSignature();
        String expResult = "SIGNATURE: Counter=-1, Output TE(s)= (DBid=-1)";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
        
        HLTTriggerSignature instance2 = getHLTTriggerSignatureWithID(-2712);
        String expResult2 = "SIGNATURE: Counter=-1, Output TE(s)= (DBid=-2712)";
        Object Result2 = instance2.toString();
        assertEquals(expResult2, Result2);
    }

      /**
     * Helper function to make an HLTTE_TE and handle
     * SQLExceptions.
     * @param id
     * @return 
     */
    public HLTTriggerSignature getHLTTriggerSignatureWithID(int id) {
        HLTTriggerSignature result = null;
        try {
            result = new HLTTriggerSignature(id);
        } catch (SQLException e) {
            fail(e.toString());
        }
        return result;
    }
    
    @Test
    public void testGet_name() {
        System.out.println("get_name");
        HLTTriggerSignature instance = new HLTTriggerSignature();
        String expResult = "signature ";
        Object Result = instance.get_name();
        assertEquals(expResult,Result);
    }

    @Test
    public void testGet_min_names() {
        System.out.println("get_min_names");
        HLTTriggerSignature instance = getHLTTriggerSignatureWithID(-2712);
        
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("ID");
        expResult.add("Counter");
        expResult.add("Output Trigger Elements");
        expResult.add("Logic");
                
        Object Result = instance.get_min_names();
        assertEquals(expResult, Result);
    }

    @Test
    public void testGet_min_info() throws Exception {
    System.out.println("get_min_info");
        HLTTriggerSignature instance = getHLTTriggerSignatureWithID(-2712);
        
        ArrayList<Object> expResult = new ArrayList<>();
        expResult.add(-2712);
        expResult.add(-1);
        expResult.add("");
        expResult.add("-1");
                
        Object Result = instance.get_min_info();
        assertEquals(expResult, Result);
    }

    @Test
    public void testConfuseLoader() {
    }

    @Test
    public void testFindParentChains() throws Exception {
        System.out.println("findParentChains");
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        HLTTriggerSignature instance = new HLTTriggerSignature();
        ArrayList<HLTTriggerElement> input = new ArrayList<>();
        HLTTriggerElement param = new HLTTriggerElement();
        input.add(param);
        instance.setElements(input);
        Set<HLTTriggerChain> expResult = new HashSet();
        Object Result = instance.findParentChains();
        assertEquals(expResult,Result);
    }

    @Test
    public void testGetTableName() {
        System.out.println("getTableName");
        HLTTriggerSignature instance = new HLTTriggerSignature();
        String expResult = "HLT_TRIGGER_SIGNATURE";
        String result = instance.getTableName();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_hashfull() {
        System.out.println("get_hashfull");
        HLTTriggerSignature instance = new HLTTriggerSignature();
        Integer expResult = -1;
        Object Result = instance.get_hashfull();
        assertEquals(expResult,Result);
    }

    @Test
    public void testSet_hashfull_ArrayList() {
        System.out.println("set_hashfull_arraylist");
        HLTTriggerSignature instance = new HLTTriggerSignature();
        ArrayList<Integer> input = new ArrayList<>();
        input.add(-2712);
        input.add (-1706);
        instance.set_hashfull(input);
        Object expResult = 177100;
        Object Result = instance.get_hashfull();
        assertEquals(expResult,Result);
    }

    @Test
    public void testSet_hashfull_Integer() {
        System.out.println("set_hashfull_integer");
        HLTTriggerSignature instance = new HLTTriggerSignature();
        Integer input = -2712;
        instance.set_hashfull(input);
        Object expResult = -2712;
        Object Result = instance.get_hashfull();
        assertEquals(expResult,Result);
    }

    @Test
    public void testCheck_hash() throws Exception {
         System.out.println("check_hash");
         HLTTriggerSignature instance = new HLTTriggerSignature();
         MockResultSet resultset = resultSetHandler.createResultSet();
         setEmptyInitInfo();
         Integer expResult = -1;
         Object Result = instance.check_hash();
         assertEquals (Result, expResult);
    }

    @Test
    public void testDoublecheck() throws Exception {
        System.out.println("doublecheck");
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        HLTTriggerSignature instance = new HLTTriggerSignature();
        ArrayList<HLTTriggerElement> input = new ArrayList<>();
        HLTTriggerElement param = new HLTTriggerElement();
        input.add(param);
        instance.setElements(input);
        boolean expResult = true;
        Object Result = instance.doublecheck(27, input);
        assertEquals(expResult,Result);
    }
    
    @Test
    public void testAreTriggerElementsEqual(){
        System.out.println("areTriggerElementsEqual");
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        HLTTriggerSignature instance = new HLTTriggerSignature();
        ArrayList<HLTTriggerElement> input = new ArrayList<>();
        HLTTriggerElement param = new HLTTriggerElement();
        input.add(param);
        instance.setElements(input);
        HLTTriggerSignature inputsig = new HLTTriggerSignature();
        inputsig.set_id(27);
        boolean expResult = true;
        Object Result = instance.AreTriggerElementsEqualPublic(inputsig);
        assertEquals(expResult,Result);
    }

    @Test
    public void testEquals() {
         System.out.println("equals");
        
        HLTTriggerSignature instance = new HLTTriggerSignature();
        HLTTriggerSignature param1 = new HLTTriggerSignature();
        HLTTriggerSignature param2 = new HLTTriggerSignature();
        Boolean expTrueResult = true;
        Boolean expFalseResult = false;
        
        param1.set_logic(-1); 
        param2.set_logic(-2);
        Object TrueResult = param1.equals(instance);
        Object FalseResult = param2.equals(instance);
        assertEquals(TrueResult,expTrueResult);
        assertEquals(FalseResult,expFalseResult);
                
        String IncorrectType = "";
        Object Result2 = param1.equals(IncorrectType);
        assertEquals(Result2,expFalseResult);
        assertNotEquals(Result2,expTrueResult);
    }

    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        HLTTriggerSignature instance = new HLTTriggerSignature();
        instance.set_logic(-2712);
        ArrayList<HLTTriggerElement> input = new ArrayList<>();
        HLTTriggerElement param = new HLTTriggerElement();
        input.add(param);
        instance.setElements(input);
        Integer expResult = 83499;
        Integer result = instance.hashCode();
        assertEquals(expResult, result);
    }
    
}
