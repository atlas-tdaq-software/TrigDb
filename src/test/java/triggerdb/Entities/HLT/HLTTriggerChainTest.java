/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.HLT;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import static triggerdb.Entities.HLT.HLTPrescaleType.*;
import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.*;
import com.mockrunner.mock.jdbc.*;
import java.sql.PreparedStatement;
import java.util.logging.Logger;
import javax.swing.tree.DefaultMutableTreeNode;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.TriggerMap;

/**
 *
 * @author dwthomas
 */
public class HLTTriggerChainTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public HLTTriggerChainTest() {
    }
    
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
        ConnectionManager.getInstance().unsetTestingConnection();
        }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
        }


    @Test
    public void testGetTableName() {
        System.out.println("getTableName");
        HLTTriggerChain instance = new HLTTriggerChain();
        String expResult = "HLT_TRIGGER_CHAIN";
        String result = instance.getTableName();
        assertEquals(expResult, result);
    }

    @Test
    public void testForceLoad() throws Exception {
    }

    @Test
    public void testEquals() {
        System.out.println("equals");
        
        HLTTriggerChain instance = new HLTTriggerChain();
        HLTTriggerChain param1 = new HLTTriggerChain();
        HLTTriggerChain param2 = new HLTTriggerChain();
        HLTTriggerChain param3 = new HLTTriggerChain();
        HLTTriggerChain param4 = new HLTTriggerChain();
        HLTTriggerChain param5 = new HLTTriggerChain();
        HLTTriggerChain param6 = new HLTTriggerChain();
        Boolean expTrueResult = true;
        Boolean expFalseResult = false;
            
        String IncorrectType = "";
        Object Result1 = param1.equals(IncorrectType);
        assertEquals(Result1,expFalseResult);
        assertNotEquals(Result1,expTrueResult);
        
        instance.set_name("name1"); 
        param1.set_name("name1");
        param2.set_name("name1");
        param3.set_name("name1");
        param4.set_name("name1");
        param5.set_name("name2");
        Object FalseResult1 = param5.equals(instance);
        assertEquals(FalseResult1,expFalseResult);
        assertNotEquals(FalseResult1,expTrueResult);
        
        instance.set_comment("comment1"); 
        param1.set_comment("comment1");
        param2.set_comment("comment1");
        param3.set_comment("comment1");
        param4.set_comment("comment2");
        Object FalseResult2 = param4.equals(instance);
        assertEquals(FalseResult2,expFalseResult);
        assertNotEquals(FalseResult2,expTrueResult);
        
        instance.set_chain_counter(2); 
        param1.set_chain_counter(2);
        param2.set_chain_counter(2);
        param3.set_chain_counter(3);
        Object FalseResult3 = param3.equals(instance);
        assertEquals(FalseResult3,expFalseResult);
        assertNotEquals(FalseResult3,expTrueResult);
                
        instance.set_eb_point(2); 
        param1.set_eb_point(2);
        param2.set_eb_point(3);
        Object FalseResult4 = param2.equals(instance);
        assertEquals(FalseResult4,expFalseResult);
        assertNotEquals(FalseResult4,expTrueResult);
        
        param6.set_name("name1");
        param6.set_comment("comment1");
        param6.set_chain_counter(2);
        param6.set_eb_point(2);
        instance.set_lower_chain_name("name1");
        param1.set_lower_chain_name("name1");
        param6.set_lower_chain_name("name2");
        Object FalseResult5 = param6.equals(instance);
        Object TrueResult = param1.equals(instance);
        assertEquals(FalseResult5,expTrueResult);
        assertNotEquals(FalseResult5,expFalseResult);
        assertEquals(TrueResult,expFalseResult);
        assertNotEquals(TrueResult,expTrueResult);
        
        
        //test should be the following:
        //assertEquals(FalseResult5,expFalseResult);
        //assertNotEquals(FalseResult5,expTrueResult);
        //assertEquals(TrueResult,expTrueResult);
        //assertNotEquals(TrueResult,expFalseResult);
    }

    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        HLTTriggerChain instance = new HLTTriggerChain();
        Integer expResult = 2027329757;
        Integer result = instance.hashCode();
        assertEquals(expResult, result);
    }

    @Test
    public void testRenumberSignatures() throws Exception {
    }

    @Test
    public void testSet_hlt_master() {
        System.out.println("set_hlt_master");
        HLTTriggerChain instance = new HLTTriggerChain();
        HLTMaster param = new HLTMaster();
        instance.set_hlt_master(param);
        //Object expResult = param;
        //Object Result = instance.get_hlt_master();
        //assertEquals (expResult, Result);
    }

    @Test
    public void testGet_user_version() {
    }

    @Test
    public void testSet_user_version() {
        System.out.println("set_user_version");
        HLTTriggerChain instance = new HLTTriggerChain();
        Integer input = 3;
        instance.set_user_version(input);
        Object Result = instance.get_user_version();
        Integer expResult = 3;
        assertEquals(Result,expResult);
    }

    @Test
    public void testGet_chain_counter() {
    }

    @Test
    public void testSet_chain_counter() {
    }

    @Test
    public void testGet_lower_chain_name() {
    }

    @Test
    public void testSet_lower_chain_name() {
    }

    @Test
    public void testGet_eb_point() {
    }

    @Test
    public void testSet_eb_point() {
    }

    @Test
    public void testGetSignatures() {
    }

    @Test
    public void testSetSignatures() {
        System.out.println("setSignatures");
        //null pointer when arraylist is empty
        
        HLTTriggerChain instance2 = new HLTTriggerChain();
        ArrayList<HLTTriggerSignature> input2 = new ArrayList<>();
        HLTTriggerSignature param = new HLTTriggerSignature();
        input2.add(param);
        instance2.setSignatures(input2);
        Object Result2 = instance2.getSignatures();
        ArrayList<HLTTriggerSignature> expResult2 = new ArrayList<>();
        expResult2.add(param);
        assertEquals(Result2,expResult2);
    }

    @Test
    public void testGetTriggerGroups() {
    }

    @Test
    public void testSetTriggerGroups() {
        System.out.println("setTriggerGroups");
        //null pointer when arraylist is empty
        
        HLTTriggerChain instance2 = new HLTTriggerChain();
        ArrayList<HLTTriggerGroup> input2 = new ArrayList<>();
        HLTTriggerGroup param = new HLTTriggerGroup();
        input2.add(param);
        instance2.setTriggerGroups(input2);
        Object Result2 = instance2.getTriggerGroups();
        ArrayList<HLTTriggerGroup> expResult2 = new ArrayList<>();
        expResult2.add(param);
        assertEquals(Result2,expResult2);
    }

    @Test
    public void testGetTriggerGroupsFromDb() throws Exception {
    }

    @Test
    public void testGetTriggerTypes() throws Exception {
    }

    @Test
    public void testSetTriggerTypes() throws SQLException {
        System.out.println("setTriggerTypes");
        //null pointer when arraylist is empty
        
        HLTTriggerChain instance2 = new HLTTriggerChain();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        ArrayList<HLTTriggerType> input2 = new ArrayList<>();
        HLTTriggerType param = new HLTTriggerType();
        input2.add(param);
        instance2.setTriggerTypes(input2);
        Object Result2 = instance2.getTriggerTypes();
        ArrayList<HLTTriggerType> expResult2 = new ArrayList<>();
        expResult2.add(param);
        assertEquals(Result2,expResult2);
    }

    @Test
    public void testDropLinks() {
    }

    @Test
    public void testDropTypes() {
    }

    @Test
    public void testDropGroups() {
    }

    @Test
    public void testDropSignatures() {
    }

    @Test
    public void testAddToTree() throws Exception {
    }

    //@Test
    public void testClone() {
        System.out.println("clone");
        HLTTriggerChain instance = new HLTTriggerChain();
        Object Result = instance.clone();
        Object expResult = instance;
        assertEquals(Result,expResult);
        //this first step towards a test fails; should it?
    }

    @Test
    public void testDoDiff() throws Exception {
    }

    @Test
    public void testSave() throws Exception {
         System.out.println("save");
         HLTTriggerChain instance = new HLTTriggerChain();
         MockResultSet resultset = resultSetHandler.createResultSet();
         setEmptyInitInfo();
         Integer expResult = 1;
         Object Result = instance.save();
         assertEquals (Result, expResult);
    }
    
    @Test
    public void testAreExistingStreamsIdenticalToCurrentStreams(){
        HLTTriggerChain instance = new HLTTriggerChain();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        ArrayList<HLTTriggerStream> input = new ArrayList<>();
        HLTTriggerStream param = new HLTTriggerStream();
        input.add(param);
        Object Result = instance.StreamsIdenticalPublic(input);
        Boolean expResult = false;
        assertEquals(Result,expResult);
    }
    
    @Test
    public void testAreExistingGroupsIdenticalToCurrentGroups(){
        HLTTriggerChain instance = new HLTTriggerChain();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        ArrayList<HLTTriggerGroup> input = new ArrayList<>();
        HLTTriggerGroup param = new HLTTriggerGroup();
        input.add(param);
        Object Result = instance.GroupsIdenticalPublic(input);
        Boolean expResult = false;
        assertEquals(Result,expResult);
    }
    
    @Test
    public void testSet_valid_input() {
        System.out.println("set_valid_input");
        HLTTriggerChain instance = new HLTTriggerChain();
        Boolean input = true;
        instance.set_valid_input(input);
        Object Result = instance.get_valid_input();
        Boolean expResult = true;
        assertEquals(Result,expResult);
    }

    @Test
    public void testGet_valid_input() {
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        HLTTriggerChain instance = new HLTTriggerChain();
        String expResult = "HLT Chain";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
        
        HLTTriggerChain instance2 = new HLTTriggerChain();
        instance2.set_id(-27);
        String expResult2 = "CHAIN:  (DBid=-27/V.1)";
        String Result2 = instance2.toString();
        assertEquals(expResult2,Result2);
    }

    @Test
    public void testGet_min_names() {
        System.out.println("get_min_names");
        HLTTriggerChain instance = new HLTTriggerChain();
        
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("ID");
        expResult.add("Name");
        expResult.add("Version");
        expResult.add("L2 or EF");
        expResult.add("Chain Counter");
        
        ArrayList<String> result = instance.get_min_names();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_min_info() {
        System.out.println("get_min_info");
        HLTTriggerChain instance = new HLTTriggerChain();
        
        ArrayList<Object> expResult = new ArrayList<>();
       
        expResult.add(-1);
        expResult.add("");
        expResult.add(1);
        expResult.add(1);   
        expResult.add(1);   
        
        ArrayList<Object> result = instance.get_min_info();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_master() {
        System.out.println("set_master");
        HLTTriggerChain instance = new HLTTriggerChain();
        HLTMaster input = new HLTMaster();
        instance.set_master(input);
        Object Result = instance.get_master();
        HLTMaster expResult = input;
        assertEquals(Result,expResult);
    }

    @Test
    public void testGetStreams_0args() { 
    }

    @Test
    public void testSetStreams() {
        System.out.println("setStreams");
        HLTTriggerChain instance2 = new HLTTriggerChain();
        ArrayList<HLTTriggerStream> input2 = new ArrayList<>();
        HLTTriggerStream param = new HLTTriggerStream();
        input2.add(param);
        instance2.setStreams(input2);
        Object Result2 = instance2.getStreams();
        ArrayList<HLTTriggerStream> expResult2 = new ArrayList<>();
        expResult2.add(param);
        assertEquals(Result2,expResult2);
        
        HLTTriggerChain instance = new HLTTriggerChain();
        ArrayList<HLTTriggerStream> input = new ArrayList<>();;
        instance.setStreams(input);
        Object Result = instance.getStreams();
        ArrayList<HLTTriggerStream> expResult = new ArrayList<>();
        assertEquals(Result,expResult);
    }

    @Test
    public void testGetStreamsFromDb() throws Exception {
    }

    @Test
    public void testSet_comment() {
    }

    @Test
    public void testGet_comment() {
    }

    @Test
    public void testConfuseLoader() {
    }

    @Test
    public void testEditComment() throws Exception {
    }

    @Test
    public void testGetMenuFromMaster() throws Exception {
        System.out.println("getMenuFromMaster");
        HLTTriggerChain instance = new HLTTriggerChain();
        HLTMaster input = new HLTMaster();
        instance.set_master(input);
        Object Result = instance.GetMenuFromMaster();
        HLTTriggerMenu expResult = null;
        assertEquals(Result,expResult);
    }
    
}
