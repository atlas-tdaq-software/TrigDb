/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.HLT;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import static triggerdb.Entities.HLT.HLTPrescaleType.*;
import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.*;
import com.mockrunner.mock.jdbc.*;
import java.util.logging.Logger;
import javax.swing.tree.DefaultMutableTreeNode;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.FakeAbstractTable;

/**
 *
 * @author dwthomas
 */
public class HLTReleaseTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public HLTReleaseTest() {
    }
    
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
        ConnectionManager.getInstance().unsetTestingConnection();
        }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
        }
    
    @Test
    public void testClone() {
        System.out.println("clone");
        HLTRelease instance = new HLTRelease();
        Object expResult = instance;
        Object result = instance.clone();
        assertEquals(expResult, result);
    }
    
    @Test
    
    public void testSave() throws Exception {
         System.out.println("save");
         HLTRelease instance = new HLTRelease();
         MockResultSet resultset = resultSetHandler.createResultSet();
         setEmptyInitInfo();
         Integer expResult = 1;
         Object Result = instance.save();
         assertEquals (Result, expResult);
    }

    @Test
    public void testAddToTree() throws Exception {
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        HLTRelease instance = new HLTRelease();
        String expResult = "HLT Release";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
        
        HLTRelease instance2 = getHLTReleaseWithID(-1000000);
        String expResult2 = "HLT RELEASE: ";
        String Result2 = instance2.toString();
        assertEquals(expResult2,Result2);
    }

     /**
     * Helper function to make an HLTTE_TE and handle
     * SQLExceptions.
     * @param id
     * @return 
     */
    public HLTRelease getHLTReleaseWithID(int id) {
        HLTRelease result = null;
        try {
            result = new HLTRelease(id);
        } catch (SQLException e) {
            fail(e.toString());
        }
        return result;
    }
    
    @Test
    public void testGetSMKIDs() throws Exception {
        System.out.println("getSMKIDs");
         HLTRelease instance = new HLTRelease();
         MockResultSet resultset = resultSetHandler.createResultSet();
         setEmptyInitInfo();
         instance.set_id(27);
         ArrayList<Integer> expResult = new ArrayList<>();
         Object Result = instance.getSMKIDs();
         assertEquals (Result, expResult);
    }

    @Test
    public void testGetTableName() {
        System.out.println("getTableName");
        HLTRelease instance = new HLTRelease();
        String expResult = "HLT_RELEASE";
        String result = instance.getTableName();
        assertEquals(expResult, result);
    }
    
    @Test
    public void testGet_min_names() {
        System.out.println("get_min_names");
        HLTRelease instance = new HLTRelease();
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("ID");
        expResult.add("Name");       
        Object Result = instance.get_min_names();
        assertEquals(expResult, Result);
    }
    

    @Test
    public void testGet_min_info() {
        System.out.println("get_min_info");
        HLTRelease instance = new HLTRelease();
        ArrayList<Object> expResult = new ArrayList<>();
        expResult.add(-1);
        expResult.add("");
        Object Result = instance.get_min_info();
        assertEquals(expResult, Result);
    }
    
}
