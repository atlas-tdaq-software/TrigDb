/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.HLT;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import static triggerdb.Entities.HLT.HLTPrescaleType.*;
import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.*;
import com.mockrunner.mock.jdbc.*;
import java.sql.PreparedStatement;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Logger;
import javax.swing.tree.DefaultMutableTreeNode;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.TriggerMap;

/**
 *
 * @author dwthomas
 */
public class HLTTriggerElementTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public HLTTriggerElementTest() {
    }
    
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
        ConnectionManager.getInstance().unsetTestingConnection();
        }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
        }

    @Test
    public void testConfuseLoader() {
    }

    @Test
    public void testGet_topo_start_from() {
        System.out.println("get_topo_start_from");
        HLTTriggerElement instance = new HLTTriggerElement();
        ConnectionManager.TopoStartFromColumnExists = 1;
        String input = "a string";
        instance.set_topo_start_from(input);
        String expResult = input;
        String result = instance.get_topo_start_from();
        assertEquals(expResult, result);
        ConnectionManager.TopoStartFromColumnExists = 0;
        HLTTriggerElement instance2 = new HLTTriggerElement();
        String expResult2 = "";
        String result2 = instance2.get_topo_start_from();
        assertEquals(expResult2, result2);
    }

    @Test
    public void testSet_topo_start_from() {
    }

    @Test
    public void testForceLoad() throws Exception {
    }

    @Test
    public void testRenumberAlgorithms() throws Exception {
    }

    @Test
    public void testAddToTree() throws Exception {
    }

    @Test
    public void testClone() {
        System.out.println("clone");
        ConnectionManager.TopoStartFromColumnExists = 1;
        HLTTriggerElement instance = new HLTTriggerElement();
        ArrayList<HLTComponent> input1 = new ArrayList<>();
        HLTComponent param1 = new HLTComponent();
        input1.add(param1);
        instance.set_algorithms(input1);
        Object expResult = instance;
        Object result = instance.clone();
        assertEquals(expResult, result);
    }

    @Test
    public void testClone_simple() {
        System.out.println("clone_simple");
        ConnectionManager.TopoStartFromColumnExists = 1;
        HLTTriggerElement instance = new HLTTriggerElement();
        Object expResult = instance;
        Object result = instance.clone_simple();
        assertEquals(expResult, result);
        ConnectionManager.TopoStartFromColumnExists = 0;
    }

    @Test
    public void testDoDiff() throws Exception {
    }

    @Test
    public void testSave() throws Exception {
         System.out.println("save");
         HLTTriggerElement instance = new HLTTriggerElement();
         MockResultSet resultset = resultSetHandler.createResultSet();
         setEmptyInitInfo();
         ArrayList<HLTComponent> input1 = new ArrayList<>();
         HLTComponent param1 = new HLTComponent();
         input1.add(param1);
         instance.set_algorithms(input1);
         Integer expResult = 1;
         Object Result = instance.save();
         assertEquals (Result, expResult);
    }

    @Test
    public void testSet_element_counter() {
        System.out.println("set_element_counter");
        HLTTriggerElement instance = new HLTTriggerElement();
        Integer input = 3;
        instance.set_element_counter(input);
        Object Result = instance.get_element_counter();
        Integer expResult = 3;
        assertEquals(Result,expResult);
    }

    @Test
    public void testGet_element_counter() {
        System.out.println("get_element_counter");
        HLTTriggerElement instance = new HLTTriggerElement();
        Integer expResult = -1;
        Object Result = instance.get_element_counter();
        assertEquals(expResult,Result);
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        HLTTriggerElement instance = new HLTTriggerElement();
        String expResult = "HLT Trigger Element";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
        
        HLTTriggerElement instance2 = getHLTTriggerElementWithID(-2712);
        String expResult2 = "HLT TRIGGER ELEMENT: , Input TE(s)= (DBid=-2712)";
        Object Result2 = instance2.toString();
        assertEquals(expResult2, Result2);
    }

        /**
     * Helper function to make an HLTTE_TE and handle
     * SQLExceptions.
     * @param id
     * @return 
     */
    public HLTTriggerElement getHLTTriggerElementWithID(int id) {
        HLTTriggerElement result = null;
        try {
            result = new HLTTriggerElement(id);
        } catch (SQLException e) {
            fail(e.toString());
        }
        return result;
    }
    
    
    
    @Test
    public void testGet_min_names() {
        System.out.println("get_min_names");
        HLTTriggerElement instance = getHLTTriggerElementWithID(-2712);
        
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("ID");
        expResult.add("Name");
        expResult.add("Input TE Name(Type)");
                
        Object Result = instance.get_min_names();
        assertEquals(expResult, Result);
    }

    @Test
    public void testGet_min_info() {
        System.out.println("get_min_info");
        HLTTriggerElement instance = getHLTTriggerElementWithID(-2712);
        
        ArrayList<Object> expResult = new ArrayList<>();
        expResult.add(-2712);
        expResult.add("");
        expResult.add("");
                
        Object Result = instance.get_min_info();
        assertEquals(expResult, Result);
    }

    @Test
    public void testGet_input_trigger_element_names() {
    }

    @Test
    public void testGet_input_trigger_element_names_and_counters() {
    }

    @Test
    public void testGetAlgorithmIDsAndCounters() throws Exception {
        System.out.println("getAlgorithmIDsAndCounters");
        HLTTriggerElement instance = new HLTTriggerElement();
        instance.set_id(27);
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Map<Integer, Integer> expResult = new TreeMap();
        Object Result = instance.getAlgorithmIDsAndCounters();
        assertEquals(expResult,Result);
    }

    @Test
    public void testDropAlgorithms() {
    }

    @Test
    public void testGetAlgorithms() {
    }
    
    @Test
    public void testGetAlgorithmsFromDb() throws Exception{
        System.out.println("getAlgorithmsFromDb");
        HLTTriggerElement instance = new HLTTriggerElement();
        instance.set_id(27);
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        ArrayList<HLTComponent> expResult = new ArrayList();
        Object Result = instance.getAlgorithmsFromDbPublic();
        assertEquals(expResult,Result);
    }

    @Test
    public void testGetInputElements() {
        System.out.println("getInputElements");
        HLTTriggerElement instance = new HLTTriggerElement();
        instance.set_id(27);
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        ArrayList<HLTTE_TE> expResult = new ArrayList();
        HLTTE_TE param = new HLTTE_TE();
        instance.addTeTeLink(param);
        expResult.add(param);
        Object Result = instance.getInputElements();
        assertEquals(expResult,Result);
    }

    @Test
    public void testAddTeTeLink() {
    }

    @Test
    public void testSetInputElements() {
        System.out.println("setInputElements");
        HLTTriggerElement instance = new HLTTriggerElement();
        ArrayList<HLTTE_TE> input = new ArrayList<>();
        HLTTE_TE param1 = new HLTTE_TE();
        input.add(param1);
        instance.setInputElements(input);
        Object Result = instance.getInputElements();
        Object expResult = input;
        assertEquals(Result,expResult);
    }

    @Test
    public void testFindParentSignatures() throws Exception {
        System.out.println("findParentSignatures");
        HLTTriggerElement instance = new HLTTriggerElement();
        instance.set_id(27);
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Set<HLTTriggerSignature> expResult = new HashSet();
        Object Result = instance.findParentSignatures();
        assertEquals(expResult,Result);
    }

    @Test
    public void testSetAlgorithms() {
        System.out.println("setAlgorithms");
        HLTTriggerElement instance = new HLTTriggerElement();
        ArrayList<HLTComponent> input = new ArrayList<>();
        HLTComponent param1 = new HLTComponent();
        input.add(param1);
        instance.setAlgorithms(input);
        Object Result = instance.getAlgorithms();
        Object expResult = input;
        assertEquals(Result,expResult);
    }

    @Test
    public void testGetTableName() {
        System.out.println("getTableName");
        HLTTriggerElement instance = new HLTTriggerElement();
        String expResult = "HLT_TRIGGER_ELEMENT";
        String result = instance.getTableName();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_hash() {
        System.out.println("get_hash");
        HLTTriggerElement instance = new HLTTriggerElement();
        Integer expResult = -1;
        Object Result = instance.get_hash();
        assertEquals(expResult,Result);
    }

    @Test
    public void testSet_hash_0args() {
        System.out.println("set_hash_0args");
        HLTTriggerElement instance = new HLTTriggerElement();
        instance.set_hash();
        Object Result = instance.get_hash();
        Integer expResult = 13;
        assertEquals(Result,expResult);
    }

    @Test
    public void testSet_hash_Integer() {
        System.out.println("set_hash_Integer");
        HLTTriggerElement instance = new HLTTriggerElement();
        Integer input = 3;
        instance.set_hash(input);
        Object Result = instance.get_hash();
        Integer expResult = 3;
        assertEquals(Result,expResult);
    }

    @Test
    public void testCheck_hash() throws Exception {
        System.out.println("check_hash");
        HLTTriggerElement instance = new HLTTriggerElement();
        instance.set_id(27);
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        ArrayList<Integer> expResult = new ArrayList();
        Object Result = instance.check_hash();
        assertEquals(expResult,Result);
    }

    @Test
    public void testDoublecheck() throws Exception {
        System.out.println("doublecheck");
        HLTTriggerElement instance = new HLTTriggerElement();
        instance.set_id(27);
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Integer input1 = -1;
        ArrayList<HLTComponent> input2 = new ArrayList();
        HLTComponent param = new HLTComponent();
        input2.add(param);
        HLTTriggerElement input3 = new HLTTriggerElement();
        Boolean expResult = false;
        Object Result = instance.doublecheck(input1, input2, input3);
        assertEquals(expResult,Result);
    }

    @Test
    public void testEquals() {
        System.out.println("equals");
        
        HLTTriggerElement instance = new HLTTriggerElement();
        HLTTriggerElement param1 = new HLTTriggerElement();
        HLTTriggerElement param2 = new HLTTriggerElement();

        Boolean expTrueResult = true;
        Boolean expFalseResult = false;

        instance.set_hash(27);
        param1.set_hash(27);
        param2.set_hash(27);
        
        instance.set_name("right"); 
        param1.set_name("wrong");
        param2.set_name("right");
        Object FalseResult = param1.equals(instance);
        assertEquals(FalseResult,expFalseResult);
        
        param1.set_name("right");
        ConnectionManager.TopoStartFromColumnExists = 1;
        instance.set_topo_start_from("right");
        param1.set_topo_start_from("wrong");
        param2.set_topo_start_from("right");
        Object FalseResult2 = param1.equals(instance);
        assertEquals(FalseResult2,expFalseResult);
        
        ConnectionManager.TopoStartFromColumnExists = 0;
        param1.set_topo_start_from("right");
        Object TrueResult = param1.equals(instance);
        assertEquals(TrueResult,expTrueResult);
    }

    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        HLTTriggerElement instance = new HLTTriggerElement();
        ArrayList<HLTTE_TE> input = new ArrayList<>();
        HLTTE_TE param = new HLTTE_TE();
        input.add(param);
        instance.setInputElements(input);
        int expResult = 2593;
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }
    
}
