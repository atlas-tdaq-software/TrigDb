/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.HLT;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import static triggerdb.Entities.HLT.HLTPrescaleType.*;
import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.*;
import com.mockrunner.mock.jdbc.*;
import java.util.logging.Logger;
import javax.swing.tree.DefaultMutableTreeNode;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.TriggerMap;

/**
 *
 * @author dwthomas
 */
public class HLTTE_TETest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public HLTTE_TETest() {
    }
    
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
        ConnectionManager.getInstance().unsetTestingConnection();
        }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
        }

    @Test
    public void testClone() throws Exception{
        System.out.println("clone");
        HLTTE_TE instance = new HLTTE_TE();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Object Result = instance.clone();
        Object expResult = instance;
        assertEquals(Result,expResult);
    }

    @Test
    public void testSave() throws Exception {
         System.out.println("save");
         HLTTE_TE instance = new HLTTE_TE();
         MockResultSet resultset = resultSetHandler.createResultSet();
         setEmptyInitInfo();
         Integer expResult = 1;
         Object Result = instance.save();
         assertEquals (Result, expResult);
    }

    @Test
    public void testCheckIDs() throws Exception {
         System.out.println("checkIDs");
         HLTTE_TE instance = new HLTTE_TE();
         MockResultSet resultset = resultSetHandler.createResultSet();
         setEmptyInitInfo();
         Boolean expResult = true;
         Object Result = instance.checkIDs();
         assertEquals (Result, expResult);
    }

    @Test
    public void testGetKey() {
        System.out.println("getKey");
        HLTTE_TE instance = new HLTTE_TE();
        TriggerMap<String,Object> expResult = new TriggerMap<>();
        expResult.put("ID",-1);
        expResult.put("TE_COUNTER",-1);
        expResult.put("TE_ID",-1);
        expResult.put("TE_INP_ID","");
        expResult.put("TE_INP_TYPE","");   
        Object Result = instance.getKey();
        assertEquals(expResult,Result);
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        HLTTE_TE instance = new HLTTE_TE();
        String expResult = "HLT TE-TE";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
        
        HLTTE_TE instance2 = getHLTTE_TEWithID(-1000000);
        String expResult2 = "HLT TE-TE: Input Name= (DBid=-1000000)";
        String Result2 = instance2.toString();
        assertEquals(expResult2,Result2);
    }
    
        /**
     * Helper function to make an HLTTE_TE and handle
     * SQLExceptions.
     * @param id
     * @return 
     */
    public HLTTE_TE getHLTTE_TEWithID(int id) {
        HLTTE_TE result = null;
        try {
            result = new HLTTE_TE(id);
        } catch (SQLException e) {
            fail(e.toString());
        }
        return result;
    }

    @Test
    public void testGet_min_names() {
        System.out.println("get_min_names");
        HLTTE_TE instance = getHLTTE_TEWithID(-2712);
        
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("ID");
        expResult.add("Input TE Name");
        expResult.add("Input TE Counter");
        expResult.add("Input TE Type");
        
        Object Result = instance.get_min_names();
        assertEquals(expResult, Result);
    }

    @Test
    public void testGet_min_info() {
        System.out.println("get_min_info");
        HLTTE_TE instance = getHLTTE_TEWithID(-7000);
        instance.set_trigger_element_id(-1992);
        instance.set_element_inp_id("value");
        instance.set_element_inp_type("type");
        instance.set_element_counter(-3);
        
        ArrayList<Object> expResult = new ArrayList<>();
        expResult.add(-7000);
        expResult.add("value");
        expResult.add(-3);
        expResult.add("type");
        
        ArrayList<Object> Result = new ArrayList<>();
        Result = instance.get_min_info();
        assertEquals(expResult,Result);
    }

    @Test
    public void testGet_name() {
        System.out.println("get_name");
        HLTTE_TE instance = new HLTTE_TE();
        instance.set_element_inp_id("value");
        String expResult = "input TE value";
        Object Result = instance.get_name();
        assertEquals(expResult,Result);
    }

    @Test
    public void testGet_element_counter() {
        System.out.println("get_element_counter");
        HLTTE_TE instance = new HLTTE_TE();
        instance.set_element_counter(1234);
        Integer expResult = 1234;
        Integer Result = instance.get_element_counter();
        assertEquals(expResult,Result);
    }

    @Test
    public void testSet_element_counter() {
        System.out.println("set_element_counter");
        HLTTE_TE instance = new HLTTE_TE();
        Integer input = 3;
        instance.set_element_counter(input);
        }

    @Test
    public void testGet_element_inp_id() {
        System.out.println("get_element_inp_id");
        HLTTE_TE instance = new HLTTE_TE();
        instance.set_element_inp_id("value");
        String expResult = "value";
        String Result = instance.get_element_inp_id();
        assertEquals(expResult,Result);
    }

    @Test
    public void testSet_element_inp_id() {
        System.out.println("set_element_inp_id");
        HLTTE_TE instance = new HLTTE_TE();
        String input = "value";
        instance.set_element_inp_id(input);
        }

    @Test
    public void testGet_trigger_element_id() {
        System.out.println("get_trigger_element_id");
        HLTTE_TE instance = new HLTTE_TE();
        instance.set_trigger_element_id(4);
        Integer expResult = 4;
        Integer Result = instance.get_trigger_element_id();
        assertEquals(expResult,Result);
    }

    @Test
    public void testSet_trigger_element_id() {
        System.out.println("set_trigger_element_id");
        HLTTE_TE instance = new HLTTE_TE();
        Integer input = 5;
        instance.set_element_counter(input);
    }

    @Test
    public void testSet_element_inp_type() {
        System.out.println("set_element_inp_type");
        HLTTE_TE instance = new HLTTE_TE();
        String input = "value";
        instance.set_element_inp_id(input);
    }

    @Test
    public void testGet_element_inp_type() {
        System.out.println("get_element_inp_type");
        HLTTE_TE instance = new HLTTE_TE();
        instance.set_element_inp_type("value");
        String expResult = "value";
        String Result = instance.get_element_inp_type();
        assertEquals(expResult,Result);
    }

    @Test
    public void testGetTableName() {
        System.out.println("getTableName");
        HLTTE_TE instance = new HLTTE_TE();
        String expResult = "HLT_TE_TO_TE";
        String result = instance.getTableName();
        assertEquals(expResult, result);
    }

    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        HLTTE_TE instance = new HLTTE_TE();
        instance.set_trigger_element_id(73);
        
        int expResult = 2233;
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }
    
}
