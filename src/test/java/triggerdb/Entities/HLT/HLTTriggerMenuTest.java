package triggerdb.Entities.HLT;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import static triggerdb.Entities.HLT.HLTPrescaleType.*;
import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.*;
import com.mockrunner.mock.jdbc.*;
import java.sql.PreparedStatement;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Logger;
import javax.swing.tree.DefaultMutableTreeNode;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.TriggerMap;
import triggerdb.Entities.alias.PrescaleSetAlias;

/**
 *
 * @author dwthomas
 */
public class HLTTriggerMenuTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public HLTTriggerMenuTest() {
    }
    
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
        ConnectionManager.getInstance().unsetTestingConnection();
        }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
        }

    @Test
    public void testLoadMenu() throws Exception {
    }

    @Test
    public void testForceLoad() throws Exception {
    }

    @Test
    public void testConfuseChains() throws Exception {
    }

    @Test
    public void testGetAllAlgoIds() throws Exception {
        System.out.println("getAllAlgoIds");
        HLTTriggerMenu instance = new HLTTriggerMenu();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Set<Integer> expResult = new HashSet<>();
        Object Result = instance.getAllAlgoIds();
        assertEquals(Result,expResult);
    }

    @Test
    public void testGetAllAlgorithms() throws Exception {
        System.out.println("getAllAlgorithms");
        HLTTriggerMenu instance = new HLTTriggerMenu();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        ArrayList<HLTComponent> expResult = new ArrayList();
        Object Result = instance.getAllAlgorithms();
        assertEquals(Result,expResult);
    }

    @Test
    public void testLoadAlgorithms() throws Exception {
    }

    @Test
    public void testLoadElements() throws Exception {
    }

    @Test
    public void testLoadExpressStreams() throws Exception {
        System.out.println("loadExpressStreams");
        HLTTriggerMenu instance = new HLTTriggerMenu();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        ArrayList<Integer> expResult = new ArrayList();
        Object Result = instance.loadExpressStreams();
        assertEquals(Result,expResult);
    }

    @Test
    public void testLoadGroups() throws Exception {
    }

    @Test
    public void testLoadSignatures() throws Exception {
    }

    @Test
    public void testLoadStreams() throws Exception {
    }

    @Test
    public void testLoadTeTe() throws Exception {
    }

    @Test
    public void testLoadTypes() throws Exception {
    }

    @Test
    public void testGet_phase() {
        System.out.println("get_phase");
        HLTTriggerMenu instance = new HLTTriggerMenu();
        String expResult = "";
        Object Result = instance.get_phase();
        assertEquals(expResult,Result);
    }

    @Test
    public void testSet_phase() {
        System.out.println("set_phase");
        HLTTriggerMenu instance = new HLTTriggerMenu();
        String input = "Phase1Complete";
        instance.set_phase(input);
        String expResult = "Phase1Complete";
        Object Result = instance.get_phase();
        assertEquals(expResult,Result);
    }

    @Test
    public void testGet_consistent() {
        System.out.println("get_consistent");
        HLTTriggerMenu instance = new HLTTriggerMenu();
        Boolean expResult = false;
        Object Result = instance.get_consistent();
        assertEquals(expResult,Result);
    }

    @Test
    public void testGetAliases() throws Exception {
        System.out.println("getAliases");
        HLTTriggerMenu instance = new HLTTriggerMenu();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        List<PrescaleSetAlias> expResult = new ArrayList();
        Object Result = instance.getAliases();
        assertEquals(Result,expResult);
    }

    @Test
    public void testGetPrescaleSets() throws Exception {
        System.out.println("getPrescaleSets");
        HLTTriggerMenu instance = new HLTTriggerMenu();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        ArrayList<HLTPrescaleSet> expResult = new ArrayList();
        Object Result = instance.getPrescaleSets();
        assertEquals(Result,expResult);
    }

    @Test
    public void testAddPrescaleSet() throws Exception {
        System.out.println("addPrescaleSet");
        HLTTriggerMenu instance = new HLTTriggerMenu();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        HLTPrescaleSet input1 = new HLTPrescaleSet();
        Boolean input2 = true;
        Boolean expResult = false;
        Object Result = instance.addPrescaleSet(input1, input2);
        assertEquals(Result,expResult);
    }
    
    @Test
    public void testCheckPrescaleExist() throws Exception {
        System.out.println("checkPrescaleExist");
        HLTTriggerMenu instance = new HLTTriggerMenu();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        HLTPrescaleSet input = new HLTPrescaleSet();
        Boolean expResult = false;
        Object Result = instance.checkPrescaleExistPublic(input);
        assertEquals(Result,expResult);
    }

    @Test
    public void testSet_consistent() {
        System.out.println("set_consistent");
        HLTTriggerMenu instance = new HLTTriggerMenu();
        Boolean input = true;
        instance.set_consistent(input);
        Boolean expResult = true;
        Object Result = instance.get_consistent();
        assertEquals(expResult,Result);
    }

    @Test
    public void testAddToTree() throws Exception {
    }

    @Test
    public void testClone() { 
        System.out.println("clone");
        HLTTriggerMenu instance = new HLTTriggerMenu();
        Object expResult = instance;
        Object result = instance.clone();
        assertEquals(expResult, result);
    }

    @Test
    public void testSave() throws Exception {
         System.out.println("save");
         HLTTriggerMenu instance = new HLTTriggerMenu();
         MockResultSet resultset = resultSetHandler.createResultSet();
         setEmptyInitInfo();
         Integer expResult = 1;
         Object Result = instance.save();
         assertEquals (Result, expResult);
    }

    @Test
    public void testCompactsave() throws Exception {
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        HLTTriggerMenu instance = new HLTTriggerMenu();
        String expResult = "HLT Menu";
        String result = instance.toString();
        assertEquals(expResult, result);
        
        HLTTriggerMenu instance2 = getHLTTriggerMenuWithID(-99);
        String expResult2 = "HLT MENU: ID=-99, Name=, Version=1";
        String result2 = instance2.toString();
        assertEquals(expResult2, result2);
    }

          /**delete
     * Helper function to make a HLTParameter and handle
     * SQLExceptions.
     * @param id
     * @return 
     */
    public HLTTriggerMenu getHLTTriggerMenuWithID(int id) {
        HLTTriggerMenu result = null;
        try {
            result = new HLTTriggerMenu(id);
        } catch (SQLException e) {
            fail(e.toString());
        }
        return result;
    }
    @Test
    public void testDoDiff() throws Exception {
    }

    @Test
    public void testGetChains() throws Exception {
    }
    
    @Test
    public void testGetChainsFromMenu() throws Exception {
        System.out.println("getChainsFromMenu");
        HLTTriggerMenu instance = new HLTTriggerMenu();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        ArrayList<HLTTriggerChain> expResult = new ArrayList<>();
        Object Result = instance.getChainsFromMenuPublic();
        assertEquals(Result,expResult);
    }
    

    @Test
    public void testSetChains() throws SQLException {
        System.out.println("setChains");
        HLTTriggerMenu instance = new HLTTriggerMenu();
        ArrayList<HLTTriggerChain> expResult = new ArrayList<>();
        HLTTriggerChain parameter = new HLTTriggerChain();
        expResult.add(parameter);
        instance.setChains(expResult);
        Object Result = instance.getChains();
        assertEquals(expResult, Result);    
    }

    @Test
    public void testGetUnseededChains() throws Exception {
        System.out.println("getUnseededChains");
        HLTTriggerMenu instance = new HLTTriggerMenu();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        ArrayList<HLTTriggerChain> expResult = new ArrayList<>();
        Object Result = instance.getUnseededChains();
        assertEquals(Result,expResult);
    }

    @Test
    public void testGetIdFromName() throws Exception {
        System.out.println("getIdFromName");
        HLTTriggerMenu instance = new HLTTriggerMenu();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        String input = "a string";
        Integer expResult = -1;
        Object Result = instance.getIdFromName(input);
        assertEquals(Result,expResult);
    }

    @Test
    public void testGetTableName() {
        System.out.println("getTableName");
        HLTTriggerMenu instance = new HLTTriggerMenu();
        String expResult = "HLT_TRIGGER_MENU";
        String Result = instance.getTableName();
        assertEquals(expResult, Result);
    }
    
}
