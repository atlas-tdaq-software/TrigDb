/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.HLT;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import static triggerdb.Entities.HLT.HLTPrescaleType.*;
import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.*;
import com.mockrunner.mock.jdbc.*;
import java.util.logging.Logger;
import javax.swing.tree.DefaultMutableTreeNode;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.FakeAbstractTable;

/**
 *
 * @author dwthomas
 */
public class HLTPrescaleSetTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public HLTPrescaleSetTest() {
    }
    
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
        ConnectionManager.getInstance().unsetTestingConnection();
        }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
        }
    
    @Test
    public void testForceLoad() throws Exception {
         System.out.println("forceLoad");
         HLTPrescaleSet instance = new HLTPrescaleSet();
         MockResultSet resultset = resultSetHandler.createResultSet();
         setEmptyInitInfo();
         instance.forceLoad();
         Object Result = instance.getPrescales();
         ArrayList<HLTPrescale> expResult = new ArrayList<>();
         assertEquals (Result, expResult); 
         
    }

    @Test
    public void testGet_TMPSid() throws Exception {
         System.out.println("get_TMPSid");
         HLTPrescaleSet instance = new HLTPrescaleSet();
         MockResultSet resultset = resultSetHandler.createResultSet();
         setEmptyInitInfo();
         Integer Result = -1;
         Object expResult = instance.get_TMPSid(-2712);
         assertEquals (Result, expResult); 
    }

    @Test
    public void testGet_Menus() throws Exception {
         System.out.println("get_Menus");
         HLTPrescaleSet instance = new HLTPrescaleSet();
         MockResultSet resultset = resultSetHandler.createResultSet();
         setEmptyInitInfo();
         ArrayList<Integer> Result = new ArrayList<>();
         Object expResult = instance.get_Menus();
         assertEquals (Result, expResult); 
    }

    @Test
    public void testGetPrescales() {
        System.out.println("getPrescales");
        HLTPrescaleSet instance = new HLTPrescaleSet();
        ArrayList<HLTPrescale> expResult = new ArrayList<>();
        Object Result = instance.getPrescales();
        assertEquals(expResult,Result);
    }

    @Test
    public void testSetPrescales() {
        System.out.println("setPrescales");
        HLTPrescaleSet instance = new HLTPrescaleSet();
        ArrayList<HLTPrescale> param1 = new ArrayList<>();
        HLTPrescale param2 = new HLTPrescale();
        param1.add(param2);
        instance.setPrescales(param1);
        Object Result = instance.getPrescales();
        Object expResult = param1;
        assertEquals(expResult,Result);
    }

    @Test
    public void testAddPrescales() {
        System.out.println("addPrescales");
        HLTPrescaleSet instance = new HLTPrescaleSet();
        ArrayList<HLTPrescale> param1 = new ArrayList<>();
        HLTPrescale param2 = new HLTPrescale();
        param1.add(param2);
        instance.addPrescales(param1);
        Object Result = instance.getPrescales();
        Object expResult = param1;
        assertEquals(expResult,Result);
    }

    @Test
    public void testAddPrescale() {
        System.out.println("addPrescale");
        HLTPrescaleSet instance = new HLTPrescaleSet();
        ArrayList<HLTPrescale> param1 = new ArrayList<>();
        HLTPrescale param2 = new HLTPrescale();
        param1.add(param2);
        instance.addPrescale(param2);
        Object Result = instance.getPrescales();
        Object expResult = param1;
        assertEquals(expResult,Result);
    }
    
    
    @Test
    public void testGetPrescalesFromDb() throws SQLException {
        //System.out.println("getPrescalesFromDb");
        //HLTPrescaleSet instance = getHLTPrescaleSetWithID(3);
        //HLTPrescaleSet instance2 = getHLTPrescaleSetWithID(-3);
        //MockResultSet resultset = resultSetHandler.createResultSet();
        //ConnectionManager mgr = ConnectionManager.getInstance();
        //setEmptyInitInfo();
        //ArrayList<HLTPrescale> expResult = new ArrayList<>();
        //ArrayList<Object> expFilters = new ArrayList<>();
        //expFilters.add(3);
        //String whereClause = " WHERE HPR_PRESCALE_SET_ID=? ORDER BY HPR_CHAIN_COUNTER ASC";
        //ArrayList<AbstractTable> expATabs = mgr.forceLoadVector(new HLTPrescale(),whereClause,expFilters);
        //for (AbstractTable tab : expATabs){
        //    expResult.add((HLTPrescale) tab);
        //}
        //ArrayList<HLTPrescale> Result = new ArrayList<>();
        //Result = instance.getPrescalesFromDb();
        //assertEquals(expResult,Result);
    }

    @Test
    public void testGetRegularPrescales() {
        System.out.println("getRegularPrescales");
        HLTPrescaleSet instance = new HLTPrescaleSet();
        ArrayList<HLTPrescale> expResult = new ArrayList<>();
        ArrayList<HLTPrescale> param1 = new ArrayList<>();
        HLTPrescale param2 = new HLTPrescale();
        HLTPrescale param3 = new HLTPrescale();
        HLTPrescale param4 = new HLTPrescale();
        HLTPrescale param5 = new HLTPrescale();
        HLTPrescale param6 = new HLTPrescale();
        param2.set_type(Prescale);
        param3.set_type(ReRun);
        param4.set_type(Pass_Through);
        param5.set_type(Stream);
        param6.set_type(NotSet);
        param1.add(param2);
        param1.add(param3);
        param1.add(param4);
        param1.add(param5);
        param1.add(param6);
        expResult.add(param2);
        instance.addPrescales(param1);
        Object Result = instance.getRegularPrescales();
        assertEquals(expResult,Result);
   }

    @Test
    public void testGetNonReRunPrescales() {
        System.out.println("getNonReRunrescales");
        HLTPrescaleSet instance = new HLTPrescaleSet();
        ArrayList<HLTPrescale> expResult = new ArrayList<>();
        ArrayList<HLTPrescale> param1 = new ArrayList<>();
        HLTPrescale param2 = new HLTPrescale();
        HLTPrescale param3 = new HLTPrescale();
        HLTPrescale param4 = new HLTPrescale();
        HLTPrescale param5 = new HLTPrescale();
        HLTPrescale param6 = new HLTPrescale();
        param2.set_type(Prescale);
        param3.set_type(ReRun);
        param4.set_type(Pass_Through);
        param5.set_type(Stream);
        param6.set_type(NotSet);
        param1.add(param2);
        param1.add(param3);
        param1.add(param4);
        param1.add(param5);
        param1.add(param6);
        expResult.add(param2);
        expResult.add(param4);
        expResult.add(param5);
        expResult.add(param6);
        instance.addPrescales(param1);
        Object Result = instance.getNonReRunPrescales();
        assertEquals(expResult,Result);
    }

    @Test
    public void testAddToTree() throws Exception {
        System.out.println("addToTree");
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        HLTMaster instance = new HLTMaster();
        ArrayList<HLTPrescale> array = new ArrayList<>();
        HLTPrescale ps = new HLTPrescale();
        array.add(ps);
        //to be continued
    }

    @Test
    public void testClone() {
        System.out.println("clone");
        HLTPrescaleSet instance = new HLTPrescaleSet();
        Object expResult = instance;
        Object result = instance.clone();
        System.out.println("Additional output for occassional clone failures");
        System.out.println("Original: " + instance.get_name() + ", "
            + instance.get_version() + ", "
            + instance.get_comment() + ", "
            + instance.get_id());
        System.out.println("Clone: " + ((HLTPrescaleSet) result).get_name() + ", "
            + ((HLTPrescaleSet) result).get_version() + ", "
            + ((HLTPrescaleSet) result).get_comment() + ", "
            + ((HLTPrescaleSet) result).get_id());
        assertEquals(expResult, result);
    }

    @Test
    public void testDoDiff() throws Exception {
    } 

    //@Test //come back to this
    public void testgetMatchingPrescaleSetsFromDB() throws SQLException {
        System.out.println("getMatchingPrescalesFromDb");
        HLTPrescaleSet instance = new HLTPrescaleSet();
        setEmptyInitInfo();
        MockResultSet rset = resultSetHandler.createResultSet();
        rset.addRow(new Object[] {"1"});
        String statement = "SELECT HMT_ID FROM HLT_MASTER_TABLE WHERE HMT_NAME ='dummyHLT'";
        resultSetHandler.prepareResultSet(statement, rset, new Integer[] {27});
        String query = "string";
        ArrayList<Integer> prescales = new ArrayList<>();
        Integer entries = 7;
        ArrayList<Integer> expResult = new ArrayList<>();
        Logger lgr = Logger.getLogger("string1", "sun.util.logging.resources.logging");
        ConnectionManager mgr = new ConnectionManager(lgr);
        ArrayList<Integer> result = instance.getMatchingPrescaleSetsFromDBPublic(mgr, query, prescales, entries);
        assertEquals(expResult, result);
        verifySQLStatementExecuted(statement);
    }
    
    //@Test
    public void testSave_int() throws Exception {
         System.out.println("save_int");
         HLTPrescaleSet instance = new HLTPrescaleSet();
         MockResultSet resultset = resultSetHandler.createResultSet();
         setEmptyInitInfo();
         Integer expResult = -1;
         Object Result = instance.save(27);
         assertEquals (Result, expResult);
         
         HLTPrescaleSet instance2 = new HLTPrescaleSet();
         ArrayList<HLTPrescale> input = new ArrayList<>();
         HLTPrescale param = new HLTPrescale();
         input.add(param);
         instance2.setPrescales(input);
         Integer expResult2 = 1;
         Object Result2 = instance2.save(27);
         assertEquals (Result2, expResult2);  
    }
    
    @Test
    public void testSave_0args() throws Exception {
         System.out.println("save_0args");
         HLTPrescaleSet instance = new HLTPrescaleSet();
         MockResultSet resultset = resultSetHandler.createResultSet();
         setEmptyInitInfo();
         Integer expResult = -1;
         Object Result = instance.save();
         assertEquals (Result, expResult);
    }

    //@Test
    public void testSave_HLTTriggerMenu() throws Exception {
         System.out.println("save_HLTTriggerMenu");
         HLTPrescaleSet instance = new HLTPrescaleSet();
         HLTTriggerMenu input = new HLTTriggerMenu();
         MockResultSet resultset = resultSetHandler.createResultSet();
         setEmptyInitInfo();
         Integer expResult = -1;
         Object Result = instance.save(input);
         assertEquals (Result, expResult);
    }

    @Test
    public void testGet_comment() {
        System.out.println("get_comment");
        HLTPrescaleSet instance1 = new HLTPrescaleSet();
        instance1.set_comment(null);
        String expResult = "null comment";
        String result = instance1.get_comment();
        assertEquals(expResult, result);
        
        HLTPrescaleSet instance2 = new HLTPrescaleSet();
        instance2.set_comment("something helpful");
        String expResult2 = "something helpful";
        String result2 = instance2.get_comment();
        assertEquals(expResult2, result2);
    }

    @Test
    public void testSet_comment() {
        System.out.println("set_comment");
        HLTPrescaleSet instance1 = new HLTPrescaleSet();
        instance1.set_comment(null);
        String expResult = "null comment";
        String result = instance1.get_comment();
        assertEquals(expResult, result);
        
        HLTPrescaleSet instance2 = new HLTPrescaleSet();
        instance2.set_comment("something helpful");
        String expResult2 = "something helpful";
        String result2 = instance2.get_comment();
        assertEquals(expResult2, result2);
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        HLTPrescaleSet instance = new HLTPrescaleSet();
        String expResult= " *: ";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
        
        HLTPrescaleSet instance2 = getHLTPrescaleSetWithID(2712);
        String expResult2 = "2712:  v1";
        Object Result2 = instance2.toString();
        assertEquals(expResult2, Result2);
    }

          /**
     * Helper function to make an HLTTE_TE and handle
     * SQLExceptions.
     * @param id
     * @return 
     */
    public HLTPrescaleSet getHLTPrescaleSetWithID(int id) {
        HLTPrescaleSet result = null;
        try {
            result = new HLTPrescaleSet(id);
        } catch (SQLException e) {
            fail(e.toString());
        }
        return result;
    }
    
    
    @Test
    public void testLoadAll_0args() throws Exception {
         System.out.println("loadAll_0args");
         HLTPrescaleSet instance = new HLTPrescaleSet();
         MockResultSet resultset = resultSetHandler.createResultSet();
         setEmptyInitInfo();
         ArrayList<HLTPrescaleSet> expResult = new ArrayList<>();
         Object Result = instance.loadAll();
         assertEquals (Result, expResult);
    }

    @Test
    public void testLoadAvailableSetNames_0args() throws Exception {
         System.out.println("loadAvailableSetNames_0args");
         HLTPrescaleSet instance = new HLTPrescaleSet();
         MockResultSet resultset = resultSetHandler.createResultSet();
         setEmptyInitInfo();
         ArrayList<String> expResult = new ArrayList<>();
         Object Result = instance.loadAvailableSetNames();
         assertEquals (Result, expResult);
    }

    @Test
    public void testLoadAvailableSetNames_int() throws Exception {
         System.out.println("loadAvailableSetNames_int");
         HLTPrescaleSet instance = new HLTPrescaleSet();
         MockResultSet resultset = resultSetHandler.createResultSet();
         setEmptyInitInfo();
         ArrayList<String> expResult = new ArrayList<>();
         Object Result = instance.loadAvailableSetNames(27);
         assertEquals (Result, expResult);
    }

    @Test
    public void testLoadAll_int() throws Exception {
         System.out.println("loadAll_int");
         HLTPrescaleSet instance = new HLTPrescaleSet();
         MockResultSet resultset = resultSetHandler.createResultSet();
         setEmptyInitInfo();
         ArrayList<HLTPrescaleSet> expResult = new ArrayList<>();
         Object Result = instance.loadAll(27);
         assertEquals (Result, expResult);
    }

    //@Test
    public void test_Compare(){
        System.out.println("compare");
        HLTPrescaleSet instance = new HLTPrescaleSet();
        HLTPrescale input1 = new HLTPrescale();
        HLTPrescale input2 = new HLTPrescale();
        HLTPrescale input3 = new HLTPrescale();
        HLTPrescale input4 = new HLTPrescale();
        input1.set_chain_counter(27);
        input2.set_chain_counter(12);
        //nt Result1 = instance.compare(input1, input2);
    }
    
    @Test
    public void testGetTableName() {
        System.out.println("getTableName");
        HLTPrescaleSet instance = new HLTPrescaleSet();
        String expResult = "HLT_PRESCALE_SET";
        String result = instance.getTableName();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetPrescalesForChain() throws Exception {
        System.out.println("getPrescalesForChain");
        HLTPrescaleSet instance = new HLTPrescaleSet();
        HLTPrescaleSet instance2 = new HLTPrescaleSet();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        ArrayList<HLTPrescale> expResult = new ArrayList<>();
        ArrayList<HLTPrescale> expResult2 = new ArrayList<>();
        ArrayList<HLTPrescale> param1 = new ArrayList<>();
        HLTPrescale param2 = new HLTPrescale();
        HLTPrescale param3 = new HLTPrescale();
        param2.set_chain_counter(-2);
        param3.set_chain_counter(-3);
        param1.add(param2);
        param1.add(param3);
        expResult.add(param2);
        expResult2.add(param3);
        instance.addPrescales(param1);
        instance2.addPrescales(param1);
        Object Result = instance.GetPrescalesForChain(-2);
        Object Result2 = instance.GetPrescalesForChain(-3);
        assertEquals(expResult,Result);
        assertEquals(expResult2,Result2);
    }
}
