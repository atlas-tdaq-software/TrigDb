/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.HLT;

import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.*;
import com.mockrunner.mock.jdbc.*;
import java.util.Set;
import javax.swing.tree.DefaultMutableTreeNode;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.FakeAbstractTable;

/**
 *
 * @author dwthomas
 */
public class HLTMasterTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public HLTMasterTest() {
    }
    
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @After
    public void tearDown() {
    }
    
    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
    }

    @Test
    public void testForceLoad() throws Exception {
         System.out.println("forceLoad");
         HLTMaster instance = new HLTMaster();
         MockResultSet resultset = resultSetHandler.createResultSet();
         setEmptyInitInfo();
         instance.forceLoad();
         Object Result = instance.get_setup();
         HLTMaster expResult = null;
         assertEquals (Result, expResult); 
    }

    @Test
    public void testGet_setup_id() {
        System.out.println("get_setup_id");
        HLTMaster instance = new HLTMaster();
        Integer expResult = -1;
        Object Result = instance.get_setup_id();
        assertEquals(expResult,Result);
    }

    @Test
    public void testSet_setup_id() {
        System.out.println("set_setup_id");
        HLTMaster instance = new HLTMaster();
        Integer input = 3;
        instance.set_setup_id(input);
        Object Result = instance.get_setup_id();
        Integer expResult = 3;
        assertEquals(Result,expResult);
    }

    @Test
    public void testGet_menu_id() {
        System.out.println("get_menu_id");
        HLTMaster instance = new HLTMaster();
        Integer expResult = -1;
        Object Result = instance.get_menu_id();
        assertEquals(expResult,Result);
    }

    @Test
    public void testSet_menu_id() {
        System.out.println("set_menu_id");
        HLTMaster instance = new HLTMaster();
        Integer input = 3;
        instance.set_menu_id(input);
        Object Result = instance.get_menu_id();
        Integer expResult = 3;
        assertEquals(Result,expResult);
    }

    @Test
    public void testGet_menu() throws Exception {
        System.out.println("get_menu");
        HLTMaster instance = new HLTMaster(); 
        setEmptyInitInfo();
        MockResultSet rset = resultSetHandler.createResultSet();
        HLTSetup expResult = new HLTSetup(27);
        instance.set_setup(expResult);
        Object result = instance.get_setup();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_menu() throws SQLException {
        System.out.println("set_menu");
        HLTMaster instance = new HLTMaster();
        HLTTriggerMenu input = new HLTTriggerMenu();
        instance.set_menu(input);
        Object Result = instance.get_menu();
        Object expResult = input;
        assertEquals(Result,expResult);
    }

    @Test
    public void testGet_setup() throws Exception {
    }

    @Test
    public void testSet_setup() throws SQLException {
        System.out.println("set_setup");
        HLTMaster instance = new HLTMaster();
        HLTTriggerMenu input = new HLTTriggerMenu();
        instance.set_menu(input);
        Object Result = instance.get_menu();
        Object expResult = input;
        assertEquals(Result,expResult);
    }

   // @Test //REMEMBER THIS IS DISABLED!
    public void testAddToTree() throws Exception {
        System.out.println("addToTree");
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        HLTMaster instance = new HLTMaster();
        table.setKeyValue("TRIGGER_MENU_ID", 10000);
        Integer menu_id = (Integer) table.keyValue.get("TRIGGER_MENU_ID");
        table.setKeyValue("SETUP_ID", 2712);
        Integer setup_id = (Integer) table.keyValue.get("SETUP_ID");
        HLTTriggerMenu menu = getHLTTriggerMenuWithID(menu_id);
        HLTSetup setup = getHLTSetupWithID(setup_id);
        DefaultMutableTreeNode DMTN = new DefaultMutableTreeNode();
        DefaultMutableTreeNode expDMTN1 = new DefaultMutableTreeNode();
        DefaultMutableTreeNode expDMTN2 = new DefaultMutableTreeNode(menu);
        DefaultMutableTreeNode expDMTN3 = new DefaultMutableTreeNode(setup);
        instance.addToTree(DMTN,1);
        expDMTN1.add(expDMTN2);
        expDMTN1.add(expDMTN3);
        Object expResult = expDMTN1;
        Object Result = DMTN;
        assertEquals(expResult, Result);
    }

  /*  @Test
    public void testClone() {
        System.out.println("clone");
        HLTMaster instance = new HLTMaster();
        //MockResultSet resultset = resultSetHandler.createResultSet();
        //setEmptyInitInfo();
        Object expResult = instance;
        Object result = instance.clone();
        assertEquals(expResult, result);
    }
*/
    @Test
    public void testDoDiff() throws Exception {
        
    }

    @Test
    public void testSave() throws Exception {
        System.out.println("save");
        HLTMaster instance = new HLTMaster();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Integer expResult = 1;
        Object result = instance.save();
        assertEquals(expResult, result);
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        HLTMaster instance = new HLTMaster();
        String expResult = "HLT Master Table: Name=";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
        
        HLTMaster instance2 = getHLTMasterWithID(-1000000);
        String expResult2 = "HLT MASTER: ID=-1000000, Name=, Version=1";
        String Result2 = instance2.toString();
        assertEquals(expResult2,Result2);
    }
    
           /**
     * Helper function to make an HLTTE_TE and handle
     * SQLExceptions.
     * @param id
     * @return 
     */
    public HLTMaster getHLTMasterWithID(int id) {
        HLTMaster result = null;
        try {
            result = new HLTMaster(id);
        } catch (SQLException e) {
            fail(e.toString());
        }
        return result;
    }
    
               /**
     * Helper function to make an HLTTE_TE and handle
     * SQLExceptions.
     * @param id
     * @return 
     */
    public HLTTriggerMenu getHLTTriggerMenuWithID(int id) {
        HLTTriggerMenu result = null;
        try {
            result = new HLTTriggerMenu(id);
        } catch (SQLException e) {
            fail(e.toString());
        }
        return result;
    }
    
        
               /**
     * Helper function to make an HLTTE_TE and handle
     * SQLExceptions.
     * @param id
     * @return 
     */
    public HLTSetup getHLTSetupWithID(int id) {
        HLTSetup result = null;
        try {
            result = new HLTSetup(id);
        } catch (SQLException e) {
            fail(e.toString());
        }
        return result;
    }
    
    @Test
    public void testGet_status() {
        System.out.println("get_status");
        HLTMaster instance = new HLTMaster();
        Integer expResult = -1;
        Object Result = instance.get_status();
        assertEquals(expResult,Result);
    }

    @Test
    public void testSet_status() {
        System.out.println("set_status");
        HLTMaster instance = new HLTMaster();
        Integer input = 3;
        instance.set_status(input);
        Object Result = instance.get_status();
        Integer expResult = 3;
        assertEquals(Result,expResult);
    }

    @Test
    public void testSet_comment() {
        System.out.println("set_comment");
        HLTMaster instance = new HLTMaster();
        String input = "a comment";
        instance.set_comment(input);
        Object Result = instance.get_comment();
        String expResult = "a comment";
        assertEquals(Result,expResult);
    }

    @Test
    public void testGet_comment() {
        System.out.println("get_comment");
        HLTMaster instance = new HLTMaster();
        String expResult = "";
        Object Result = instance.get_comment();
        assertEquals(expResult,Result);
    }

    @Test
    public void testGetTableName() {
        System.out.println("getTableName");
        HLTMaster instance = new HLTMaster();
        String expResult = "HLT_MASTER_TABLE";
        String result = instance.getTableName();
        assertEquals(expResult, result);
    }

    @Test
    public void testFindDummy() {
        System.out.println("findDummy");
        HLTMaster instance = new HLTMaster();
        setEmptyInitInfo();
        MockResultSet rset = resultSetHandler.createResultSet();
        rset.addRow(new Object[] {"1"});
        String statement = "SELECT HMT_ID FROM HLT_MASTER_TABLE WHERE HMT_NAME ='dummyHLT'";
        resultSetHandler.prepareResultSet(statement, rset, new Integer[] {27});
        Integer expResult = -1;
        Integer result = instance.findDummy();
        assertEquals(expResult, result);
        verifySQLStatementExecuted(statement);
    }
    
}
