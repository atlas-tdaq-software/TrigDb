/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.HLT;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.*;
import com.mockrunner.mock.jdbc.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;

/**
 *
 * @author abozson, dwthomas
 */
public class HLTParameterTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public HLTParameterTest() {
    }
    
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
        ConnectionManager.getInstance().unsetTestingConnection();
    }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
    }

    /**
     *
     */
    @Test
    public void testConstructor_integer() {
        System.out.println("constrcutor (int)");
        HLTParameter instance1 = null;
        try {
            instance1 = new HLTParameter(-1);
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
        HLTParameter instance2 = new HLTParameter();
        assertEquals(instance1, instance2);
    }
    
    /**
     * Test of clone method, of class HLTParameter.
     */
    @Test
    public void testClone() {
        System.out.println("clone");
        HLTParameter instance = new HLTParameter();
        Object expResult = instance;
        Object result = instance.clone();
        assertEquals(expResult, result);
    }

    @Test
    public void testSave() throws Exception {
        System.out.println("save");
        HLTParameter instance = new HLTParameter();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Integer expResult = 1;
        Object result = instance.save();
        assertEquals(expResult, result);
        
         
        HLTParameter instance2 = new HLTParameter();
        instance2.set_value(repeatedCharacter('a', 4001));
        Integer expResult2 = -1;
        Integer result2 = instance2.save();
        assertEquals(expResult2, result2);
    }
    
    public String repeatedCharacter(char ch, int len) {
        char[] chars = new char[len];
        Arrays.fill(chars, ch);
        return new String(chars);
    }
    
    /**
     * Test of split method, of class HLTParameter.
     */
    @Test
    public void testSplit_one() {
        System.out.println("split (1)");
        HLTParameter instance = new HLTParameter();
        instance.set_value(repeatedCharacter('a', 2000));
        
        int expResult = 1;
        int result = instance.split().size();
        assertEquals(expResult, result);
    }
    
    /**
     *
     */
    @Test
    public void testSplit_multi() {
        System.out.println("split (>1)");
        HLTParameter instance = new HLTParameter();
        instance.set_value(repeatedCharacter('x', 10000));
        
        int expResult = 3;
        ArrayList<HLTParameter> splitResult = instance.split();
        int result = splitResult.size();
        assertEquals(expResult, result);
        
        int trailingCharCount = splitResult.get(2).get_value().length();
        int expCount = 2000;
        assertEquals(expCount, trailingCharCount);
    }

    /**
    /**
     * Test of get_value method, of class HLTParameter.
     */
    @Test
    public void testGet_Set_value() {
        System.out.println("set_get_value");
        HLTParameter instance = new HLTParameter();
        instance.set_value("test-value");
        String expResult = "test-value";
        String result = instance.get_value();
        assertEquals(expResult, result);
        
        instance.set_value("testvalue-2");
        String newResult = instance.get_value();
        assertNotEquals(newResult, expResult);
    }
//
//    /**
//     * Test of get_chain_user_version method, of class HLTParameter.
//     */
//    @Test
//    public void testGet_chain_user_version() {
//        System.out.println("get_chain_user_version");
//        HLTParameter instance = new HLTParameter();
//        Boolean expResult = null;
//        Boolean result = instance.get_chain_user_version();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of set_chain_user_version method, of class HLTParameter.
//     */
//    @Test
//    public void testSet_chain_user_version() {
//        System.out.println("set_chain_user_version");
//        Boolean inp = null;
//        HLTParameter instance = new HLTParameter();
//        instance.set_chain_user_version(inp);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of get_hash method, of class HLTParameter.
     */
    @Test
    public void testGet_hash() {
        System.out.println("get_hash");
        HLTParameter instance = new HLTParameter();
        instance.set_hash(1234);
        Integer expResult = 1234;
        Integer result = instance.get_hash();
        assertEquals(expResult, result);
    }

    /**
     * Test of set_hash method, of class HLTParameter.
     */
    @Test
    public void testSet_hash_0args() {
        System.out.println("set_hash");
        HLTParameter instance = new HLTParameter();
        instance.set_value("some trigger stuff");
        instance.set_hash();
        int expValue = instance.hashCode();
        int result = instance.get_hash();
        assertEquals(expValue, result);
    }

    /**
     * Test of toString method, of class HLTParameter.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        HLTParameter instance = new HLTParameter();
        String expResult = "HLT Parameter";
        String result = instance.toString();
        assertEquals(expResult, result);
        
        HLTParameter instance2 = getHLTParameterWithID(-99);
        String expResult2 = "HLT Parameter:  (DBid=-99)";
        String result2 = instance2.toString();
        assertEquals(expResult2, result2);
    }

    
    /**
     * Helper function to make a HLTParameter and handle
     * SQLExceptions.
     * @param id
     * @return 
     */
    public HLTParameter getHLTParameterWithID(int id) {
        HLTParameter result = null;
        try {
            result = new HLTParameter(id);
        } catch (SQLException e) {
            fail(e.toString());
        }
        return result;
    }
    
    /**
     * Test of get_min_names method, of class HLTParameter.
     */
    @Test
    public void testGet_min_names() {
        System.out.println("get_min_names");
        HLTParameter instance = getHLTParameterWithID(-1993);
        
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("ID");
        expResult.add("Name");
        expResult.add("Value");
        
        ArrayList<String> result = instance.get_min_names();
        assertEquals(expResult, result);
    }

    /**
     * Test of get_min_info method, of class HLTParameter.
     */
    @Test
    public void testGet_min_info() {
        System.out.println("get_min_info");
        HLTParameter instance = getHLTParameterWithID(-435);
        instance.set_value("val");
        
        ArrayList<Object> expResult = new ArrayList<>();
        expResult.add(-435);
        expResult.add("");
        expResult.add("val");        
        
        ArrayList<Object> result = instance.get_min_info();
        assertEquals(expResult, result);
    }
//
    /**
     * Test of getTableName method, of class HLTParameter.
     */
    @Test
    public void testGetTableName() {
        System.out.println("getTableName");
        HLTParameter instance = new HLTParameter();
        String expResult = "HLT_PARAMETER";
        String result = instance.getTableName();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class HLTParameter.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        
        HLTParameter param1 = new HLTParameter();
        param1.set_value("param");       
        assertNotEquals(param1, "hey I'm a string");
        
        HLTParameter param2 = new HLTParameter();
        param2.set_value("param");        
        assertEquals(param1, param2);
        
        HLTParameter param3 = new HLTParameter();
        param3.set_value("param3");
        assertNotEquals(param1, param3);
        
        HLTParameter param4 = new HLTParameter();
        param4.set_op("some_op");
        assertNotEquals(param1, param4);
    }

    /**
     *
     */
    @Test
    public void testEqualsTilde() {
        System.out.println("equals (~)");
        
        HLTParameter param1 = new HLTParameter();
        param1.set_value("~");
        HLTParameter param2 = new HLTParameter();
        param2.set_value("");
        
        assertEquals(param1, param2);
    }
    
    /**
     * Test of hashCode method, of class HLTParameter.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        HLTParameter instance = new HLTParameter();
        instance.set_value("");
        
        int expResult = 147961;
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }
    
    /**
     *
     */
    @Test
    public void testHashCodeTilde() {
        System.out.println("hashCode (~)");
        HLTParameter instance = new HLTParameter();
        instance.set_value("~");
        
        int expResult = 147961;
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }
    
}
