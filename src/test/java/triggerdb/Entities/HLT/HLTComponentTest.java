/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.HLT;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import com.mockrunner.mock.jdbc.MockResultSet;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.Entities.HLT.HLTMaster;
import triggerdb.Entities.HLT.HLTRelease;
import triggerdb.Entities.L1.L1Item;
import triggerdb.Entities.L1.L1Master;
import triggerdb.Entities.SMT.SuperMasterTable;
import triggerdb.Entities.Topo.TopoMaster;
import triggerdb.Entities.alias.HLTPrescaleSetAliasLumi;
import triggerdb.Entities.alias.L1PrescaleSetAliasLumi;
import triggerdb.Entities.alias.PrescaleSetAlias;
import triggerdb.PrescaleSetAliasComponent;
/**
 *
 * @author abozson, dwthomas
 */
public class HLTComponentTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public HLTComponentTest() {
    }
    
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
        ConnectionManager.getInstance().unsetTestingConnection();
    }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
    }

    /**
     *
     */
    @Test
    public void TestConstructorInt() {
        HLTComponent comp = null;
        try {
            comp = new HLTComponent(-1);
        } catch (SQLException e) {
            fail(e.toString());
        }
        HLTComponent exp = new HLTComponent();
        assertEquals(exp, comp);
    }
    
    /**
     * Test of forceLoad method, of class HLTComponent.
     */
//    @Test
//    public void testForceLoad() throws Exception {
//        System.out.println("forceLoad");
//        HLTComponent instance = new HLTComponent();
//        instance.forceLoad();
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of addToTree method, of class HLTComponent.
     */
//    @Test
//    public void testAddToTree() throws Exception {
//        System.out.println("addToTree");
//        DefaultMutableTreeNode treeNode = null;
//        int counter = 0;
//        HLTComponent instance = new HLTComponent();
//        instance.addToTree(treeNode, counter);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of doDiff method, of class HLTComponent.
//     */
//    @Test
//    public void testDoDiff() throws Exception {
//        System.out.println("doDiff");
//        AbstractTable t = null;
//        DefaultMutableTreeNode treeNode = null;
//        Set<String> linkstoignore = null;
//        HLTComponent instance = new HLTComponent();
//        int expResult = 0;
//        int result = instance.doDiff(t, treeNode, linkstoignore);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of equals method, of class HLTComponent.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        HLTComponent instance1 = new HLTComponent();
        HLTComponent instance2 = new HLTComponent();
        assertEquals(instance1, instance2);
        
        instance2.set_alias("Gary");
        assertNotEquals(instance1, instance2);
        
        HLTComponent instance3 = new HLTComponent();
        instance3.set_type("lemon");
        assertNotEquals(instance1, instance2);
        
        HLTComponent instance4 = (HLTComponent) instance3.clone();
        instance4.set_py_name("slimy");
        assertNotEquals(instance3, instance4);
        instance3.set_py_name("slimy");
        assertEquals(instance4, instance3);
        
        // Different types
        assertNotEquals(-234.5, instance1);
        HLTParameter param = new HLTParameter();
        assertNotEquals(instance1, param);
    }

    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        HLTComponent instance = new HLTComponent();
        Integer expResult = 318056437;
        Integer result = instance.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of clone method, of class HLTComponent.
     */
    @Test
    public void testClone() {
        System.out.println("clone");
        HLTComponent instance = new HLTComponent();
        instance.set_py_name("serpent");
        Object expResult = instance;
        Object result = instance.clone();
        assertEquals(expResult, result);
    }

 @Test
    public void testSave() throws SQLException {
        System.out.println("save");
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        HLTComponent instance = new HLTComponent();
        Integer expResult = 1;
        Integer result = instance.save();
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCompactsave() throws SQLException {
        System.out.println("compactsave");
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        HLTComponent instance = new HLTComponent();
        Integer expResult = 1;
        Integer result = instance.compactsave();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_list_mode() {
        System.out.println("set_list_mode");
        HLTComponent instance = new HLTComponent();
        boolean input = true;
        instance.set_list_mode(input);
        boolean expResult = true;
        boolean result = instance.get_list_mode();
        assertEquals(expResult, result);
    }
        
    @Test
    public void testSet_hash() {
        System.out.println("set_hash");
        HLTComponent instance = new HLTComponent();
        instance.set_hash();
        Integer expResult = 318056437;
        Integer result = instance.get_hash();
        assertEquals(expResult, result);
    }
    
    @Test
    public void testSet_hashfull() throws SQLException {
        System.out.println("set_hashfull");
        HLTComponent instance = new HLTComponent();
        ArrayList<Integer> input = new ArrayList<>();
        input.add(5);
        input.add(27);
        input.add(12);
        instance.set_hashfull(input);
        Integer expResult = 2223;
        Integer result = instance.get_hashfull();
        assertEquals(expResult, result);
        
        HLTComponent instance2 = new HLTComponent();
        Integer input2 = 27;
        instance2.set_hashfull(input2);
        Integer expResult2 = 27;
        Integer result2 = instance2.get_hashfull();
        assertEquals(expResult2, result2);
    }
//
    
    @Test 
    public void testCheck_hashfull() throws java.sql.SQLException {
        System.out.println("check_hashfull");
        HLTComponent instance = new HLTComponent();
        setEmptyInitInfo();
        MockResultSet rset = resultSetHandler.createResultSet();
        rset.addRow(new Object[] {"1"});
        String statement = "select hcp_id from HLT_COMPONENT where hcp_hashfull = ?";
        resultSetHandler.prepareResultSet(statement, rset, new Integer[] {27});
        Integer expResult = -1;
        Integer result = instance.check_hashfull();
        assertEquals(expResult, result);
        verifySQLStatementExecuted(statement);
    }
    
    @Test
    public void testDoublecheck() throws java.sql.SQLException {
        System.out.println("doublecheck");
        HLTComponent instance = new HLTComponent();
        setEmptyInitInfo();
        MockResultSet rset = resultSetHandler.createResultSet();
        Integer input = -1;
        ArrayList<HLTComponent> input2 = new ArrayList<>();
        HLTComponent param1 = new HLTComponent();
        input2.add(param1);
        ArrayList<HLTParameter> input3 = new ArrayList<>();
        HLTParameter param2 = new HLTParameter();
        input3.add(param2);
        Boolean expResult = false;
        Boolean result = instance.doublecheck(input, input2, input3);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCheckPara() throws java.sql.SQLException {
        System.out.println("checkPara");
        HLTComponent instance = new HLTComponent();
        setEmptyInitInfo();
        MockResultSet rset = resultSetHandler.createResultSet();
        ArrayList<HLTParameter> expResult = new ArrayList<>();
        ArrayList<HLTParameter> result = instance.checkParaPublic(-1);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testgetParameters() throws java.sql.SQLException {
        System.out.println("getParameters");
        HLTComponent instance = new HLTComponent(27);
        setEmptyInitInfo();
        MockResultSet rset = resultSetHandler.createResultSet();
        ArrayList<HLTParameter> expResult = new ArrayList<>();
        ArrayList<HLTParameter> result = instance.getParameters();
        assertEquals(expResult, result);
    }

    @Test
    public void testgetChildComponents() throws java.sql.SQLException {
        System.out.println("getChildComponents");
        HLTComponent instance = new HLTComponent(27);
        setEmptyInitInfo();
        MockResultSet rset = resultSetHandler.createResultSet();
        ArrayList<HLTComponent> expResult = new ArrayList<>();
        ArrayList<HLTComponent> result = instance.getChildComponents();
        assertEquals(expResult, result);
    }
    

    @Test
    public void testToString() {
        System.out.println("toString");
        HLTComponent instance = new HLTComponent();
        instance.set_list_mode(true);
        String expResult = "Alias ()";
        String result = instance.toString();
        assertEquals(expResult, result);
        
        HLTComponent instance2 = new HLTComponent();
        instance2.set_list_mode(false);
        String expResult2 = "HLT Component";
        String result2 = instance2.toString();
        assertEquals(expResult2, result2);
        
        HLTComponent instance3 = new HLTComponent();
        instance3.set_list_mode(false);
        instance3.set_id(27);
        String expResult3 = "COMPONENT: /Alias (DBid=27/V.1)";
        String result3 = instance3.toString();
        assertEquals(expResult3, result3);
    }
         
    @Test
    public void testGet_min_names() {
        System.out.println("get_min_names");
        HLTComponent instance = new HLTComponent();
        
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("ID");
        expResult.add("Alias");
        expResult.add("Version");
        expResult.add("Type");
                
        Object Result = instance.get_min_names();
        assertEquals(expResult, Result);
    }

    @Test
    public void testGet_min_info() {
        System.out.println("get_min_info");
        HLTComponent instance = new HLTComponent();
        
        ArrayList<Object> expResult = new ArrayList<>();
        expResult.add(-1);
        expResult.add("Alias");
        expResult.add(1);
        expResult.add("Type");
                
        Object Result = instance.get_min_info();
        assertEquals(expResult, Result);
    }
     
    @Test
    public void testFindAllParentSetups() throws java.sql.SQLException {
        System.out.println("findAllParentSetups");
        HLTComponent instance = new HLTComponent();
        Set<HLTSetup> param = new HashSet<>();
        HLTSetup param2 = new HLTSetup(27);
        param.add(param2);
        setEmptyInitInfo();
        MockResultSet rset = resultSetHandler.createResultSet();
        rset.addRow(new Object[] {"1"});
        String statement = "SELECT HST_ID, HST_NAME, HST_VERSION FROM HLT_ST_TO_CP, HLT_SETUP WHERE HST2CP_SETUP_ID=HST_ID AND HST2CP_COMPONENT_ID=? ";
        resultSetHandler.prepareResultSet(statement, rset, new HLTSetup[] {new HLTSetup(27)});
        Set<HLTSetup> expResult = new HashSet<>();
        Set<HLTSetup> result = instance.findAllParentSetups();
        assertEquals(expResult, result);
        verifySQLStatementExecuted(statement);
    }
    
    @Test
    public void testFindParentSetups() throws java.sql.SQLException {
        System.out.println("findParentSetups");
        HLTComponent instance = new HLTComponent();
        ArrayList<HLTSetup> param = new ArrayList<>();
        HLTSetup param2 = new HLTSetup(27);
        param.add(param2);
        setEmptyInitInfo();
        MockResultSet rset = resultSetHandler.createResultSet();
        rset.addRow(new Object[] {"1"});
        String statement = "SELECT HST_ID, HST_NAME, HST_VERSION FROM HLT_ST_TO_CP, HLT_SETUP WHERE HST2CP_SETUP_ID=HST_ID AND HST2CP_COMPONENT_ID=? ";
        resultSetHandler.prepareResultSet(statement, rset, new HLTSetup[] {new HLTSetup(27)});
        System.out.println(instance.getChainsQuery());
        ArrayList<HLTSetup> expResult = new ArrayList<>();
        ArrayList<HLTSetup> result = instance.findParentSetups();
        assertEquals(expResult, result);
        verifySQLStatementExecuted(statement);
    }
    
    @Test
    public void testFindParentElements() throws java.sql.SQLException {
        System.out.println("findParentElements");
        HLTComponent instance = new HLTComponent();
        ArrayList<HLTTriggerElement> param = new ArrayList<>();
        HLTTriggerElement param2 = new HLTTriggerElement(27);
        param.add(param2);
        setEmptyInitInfo();
        MockResultSet rset = resultSetHandler.createResultSet();
        rset.addRow(new Object[] {"1"});
        String statement = "SELECT HTE_HASH, HTE_ID, HTE_NAME, HTE_VERSION, HCP_ID FROM HLT_COMPONENT, HLT_TE_TO_CP, HLT_TRIGGER_ELEMENT WHERE HTE2CP_TRIGGER_ELEMENT_ID=HTE_ID AND HTE2CP_COMPONENT_ID=HCP_ID AND HCP_ID=?";
        resultSetHandler.prepareResultSet(statement, rset, new HLTTriggerElement[] {new HLTTriggerElement(27)});
        ArrayList<HLTTriggerElement> expResult = new ArrayList<>();
        ArrayList<HLTTriggerElement> result = instance.findParentElements();
        assertEquals(expResult, result);
        verifySQLStatementExecuted(statement);
    }
    
    
    @Test
    public void testGetChains() throws java.sql.SQLException {
        System.out.println("getChains");
        HLTComponent instance = new HLTComponent();
        ArrayList<HLTTriggerChain> param = new ArrayList<>();
        HLTTriggerChain param2 = new HLTTriggerChain(27);
        param.add(param2);
        setEmptyInitInfo();
        MockResultSet rset = resultSetHandler.createResultSet();
        rset.addRow(new Object[] {"1"});
        String statement = "SELECT HTM2TC_TRIGGER_MENU_ID, HTM2TC_TRIGGER_CHAIN_ID, HTC_ID, HTC_CHAIN_COUNTER HTC2TS_TRIGGER_CHAIN_ID, HTC2TS_TRIGGER_SIGNATURE_ID, HTS2TE_TRIGGER_SIGNATURE_ID, HTS2TE_TRIGGER_ELEMENT_ID, HTE2CP_TRIGGER_ELEMENT_ID, HTE2CP_COMPONENT_ID FROM HLT_TM_TO_TC, HLT_TRIGGER_CHAIN, HLT_TC_TO_TS, HLT_TS_TO_TE, HLT_TE_TO_CP WHERE HTM2TC_TRIGGER_MENU_ID=? AND HTC_ID = HTM2TC_TRIGGER_CHAIN_ID AND HTM2TC_TRIGGER_CHAIN_ID=HTC2TS_TRIGGER_CHAIN_ID AND HTC2TS_TRIGGER_SIGNATURE_ID=HTS2TE_TRIGGER_SIGNATURE_ID AND HTS2TE_TRIGGER_ELEMENT_ID=HTE2CP_TRIGGER_ELEMENT_ID AND HTE2CP_COMPONENT_ID =? ORDER BY HTC_CHAIN_COUNTER ASC";
        resultSetHandler.prepareResultSet(statement, rset, new HLTTriggerElement[] {new HLTTriggerElement(27)});
        ArrayList<HLTTriggerChain> expResult = new ArrayList<>();
        ArrayList<HLTTriggerChain> result = instance.getChains(27);
        assertEquals(expResult, result);
        verifySQLStatementExecuted(statement);
    }

    

//    /**
//     * Test of get_alias method, of class HLTComponent.
//     */
//    @Test
//    public void testGet_alias() {
//        System.out.println("get_alias");
//        HLTComponent instance = new HLTComponent();
//        String expResult = "";
//        String result = instance.get_alias();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of set_alias method, of class HLTComponent.
//     */
//    @Test
//    public void testSet_alias() {
//        System.out.println("set_alias");
//        String alias = "";
//        HLTComponent instance = new HLTComponent();
//        instance.set_alias(alias);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of get_py_package method, of class HLTComponent.
//     */
//    @Test
//    public void testGet_py_package() {
//        System.out.println("get_py_package");
//        HLTComponent instance = new HLTComponent();
//        String expResult = "";
//        String result = instance.get_py_package();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of set_py_package method, of class HLTComponent.
//     */
//    @Test
//    public void testSet_py_package() {
//        System.out.println("set_py_package");
//        String pyPackage = "";
//        HLTComponent instance = new HLTComponent();
//        instance.set_py_package(pyPackage);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of get_py_name method, of class HLTComponent.
//     */
//    @Test
//    public void testGet_py_name() {
//        System.out.println("get_py_name");
//        HLTComponent instance = new HLTComponent();
//        String expResult = "";
//        String result = instance.get_py_name();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of set_py_name method, of class HLTComponent.
//     */
//    @Test
//    public void testSet_py_name() {
//        System.out.println("set_py_name");
//        String pyName = "";
//        HLTComponent instance = new HLTComponent();
//        instance.set_py_name(pyName);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of set_algorithm_counter method, of class HLTComponent.
//     */
//    @Test
//    public void testSet_algorithm_counter() {
//        System.out.println("set_algorithm_counter");
//        int alg_counter = 0;
//        HLTComponent instance = new HLTComponent();
//        instance.set_algorithm_counter(alg_counter);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of get_algorithm_counter method, of class HLTComponent.
//     */
//    @Test
//    public void testGet_algorithm_counter() {
//        System.out.println("get_algorithm_counter");
//        HLTComponent instance = new HLTComponent();
//        Integer expResult = null;
//        Integer result = instance.get_algorithm_counter();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of get_type method, of class HLTComponent.
//     */
//    @Test
//    public void testGet_type() {
//        System.out.println("get_type");
//        HLTComponent instance = new HLTComponent();
//        String expResult = "";
//        String result = instance.get_type();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
    /**
     * Test of isChild method, of class HLTComponent.
     */
    @Test
    public void testIsChildFalse() {
        System.out.println("Get/set isChild");
        HLTComponent instance = new HLTComponent();
        boolean expResult = false;
        boolean result = instance.isChild();
        assertEquals(expResult, result);
    }
    
    /**
     *
     */
    @Test
    public void testIsChildTrue() {
        System.out.println("Get/set isChild");
        HLTComponent instance = new HLTComponent();
        instance.setIsChild(true);
        boolean expResult = true;
        boolean result = instance.isChild();
        assertEquals(expResult, result);
    }
//
//    /**
//     * Test of set_list_mode method, of class HLTComponent.
//     */
//    @Test
//    public void testSet_list_mode() {
//        System.out.println("set_list_mode");
//        boolean b = false;
//        HLTComponent instance = new HLTComponent();
//        instance.set_list_mode(b);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of set_type method, of class HLTComponent.
//     */
//    @Test
//    public void testSet_type() {
//        System.out.println("set_type");
//        String inp = "";
//        HLTComponent instance = new HLTComponent();
//        instance.set_type(inp);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of get_hash method, of class HLTComponent.
//     */
//    @Test
//    public void testGet_hash() {
//        System.out.println("get_hash");
//        HLTComponent instance = new HLTComponent();
//        Integer expResult = null;
//        Integer result = instance.get_hash();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of set_hash method, of class HLTComponent.
//     */
//    @Test
//    public void testSet_hash() {
//        System.out.println("set_hash");
//        HLTComponent instance = new HLTComponent();
//        instance.set_hash();
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of get_hashfull method, of class HLTComponent.
//     */
//    @Test
//    public void testGet_hashfull() {
//        System.out.println("get_hashfull");
//        HLTComponent instance = new HLTComponent();
//        Integer expResult = null;
//        Integer result = instance.get_hashfull();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of set_hashfull method, of class HLTComponent.
//     */
//    @Test
//    public void testSet_hashfull_ArrayList() {
//        System.out.println("set_hashfull");
//        ArrayList<Integer> hashes = null;
//        HLTComponent instance = new HLTComponent();
//        instance.set_hashfull(hashes);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of set_hashfull method, of class HLTComponent.
//     */
//    @Test
//    public void testSet_hashfull_Integer() {
//        System.out.println("set_hashfull");
//        Integer hash = null;
//        HLTComponent instance = new HLTComponent();
//        instance.set_hashfull(hash);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of check_hashfull method, of class HLTComponent.
//     */
//    @Test
//    public void testCheck_hashfull() throws Exception {
//        System.out.println("check_hashfull");
//        HLTComponent instance = new HLTComponent();
//        int expResult = 0;
//        int result = instance.check_hashfull();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of doublecheck method, of class HLTComponent.
//     */
//    @Test
//    public void testDoublecheck() throws Exception {
//        System.out.println("doublecheck");
//        int CompId = 0;
//        ArrayList<HLTComponent> currentComps = null;
//        ArrayList<HLTParameter> currentParas = null;
//        HLTComponent instance = new HLTComponent();
//        boolean expResult = false;
//        boolean result = instance.doublecheck(CompId, currentComps, currentParas);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getParameters method, of class HLTComponent.
//     */
//    @Test
//    public void testGetParameters() throws Exception {
//        System.out.println("getParameters");
//        HLTComponent instance = new HLTComponent();
//        ArrayList<HLTParameter> expResult = null;
//        ArrayList<HLTParameter> result = instance.getParameters();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getParameterIDs method, of class HLTComponent.
//     */
//    @Test
//    public void testGetParameterIDs() throws Exception {
//        System.out.println("getParameterIDs");
//        HLTComponent instance = new HLTComponent();
//        ArrayList<Integer> expResult = null;
//        ArrayList<Integer> result = instance.getParameterIDs();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getChildComponents method, of class HLTComponent.
//     */
//    @Test
//    public void testGetChildComponents() throws Exception {
//        System.out.println("getChildComponents");
//        HLTComponent instance = new HLTComponent();
//        ArrayList<HLTComponent> expResult = null;
//        ArrayList<HLTComponent> result = instance.getChildComponents();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getAllChildComponentIDs method, of class HLTComponent.
//     */
//    @Test
//    public void testGetAllChildComponentIDs() throws Exception {
//        System.out.println("getAllChildComponentIDs");
//        HLTComponent instance = new HLTComponent();
//        ArrayList<Integer> expResult = null;
//        ArrayList<Integer> result = instance.getAllChildComponentIDs();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getAllChildComponents method, of class HLTComponent.
//     */
//    @Test
//    public void testGetAllChildComponents() throws Exception {
//        System.out.println("getAllChildComponents");
//        HLTComponent instance = new HLTComponent();
//        ArrayList<HLTComponent> expResult = null;
//        ArrayList<HLTComponent> result = instance.getAllChildComponents();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of toString method, of class HLTComponent.
//     */
//    @Test
//    public void testToString() {
//        System.out.println("toString");
//        HLTComponent instance = new HLTComponent();
//        String expResult = "";
//        String result = instance.toString();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of get_min_names method, of class HLTComponent.
//     */
//    @Test
//    public void testGet_min_names() {
//        System.out.println("get_min_names");
//        HLTComponent instance = new HLTComponent();
//        ArrayList<String> expResult = null;
//        ArrayList<String> result = instance.get_min_names();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of get_min_info method, of class HLTComponent.
//     */
//    @Test
//    public void testGet_min_info() {
//        System.out.println("get_min_info");
//        HLTComponent instance = new HLTComponent();
//        ArrayList<Object> expResult = null;
//        ArrayList<Object> result = instance.get_min_info();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of findAllParentSetups method, of class HLTComponent.
//     */
//    @Test
//    public void testFindAllParentSetups() throws Exception {
//        System.out.println("findAllParentSetups");
//        HLTComponent instance = new HLTComponent();
//        Set<HLTSetup> expResult = null;
//        Set<HLTSetup> result = instance.findAllParentSetups();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of findParentSetups method, of class HLTComponent.
//     */
//    @Test
//    public void testFindParentSetups() throws Exception {
//        System.out.println("findParentSetups");
//        HLTComponent instance = new HLTComponent();
//        ArrayList<HLTSetup> expResult = null;
//        ArrayList<HLTSetup> result = instance.findParentSetups();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of findAllParentComponents method, of class HLTComponent.
//     */
//    @Test
//    public void testFindAllParentComponents() throws Exception {
//        System.out.println("findAllParentComponents");
//        HLTComponent instance = new HLTComponent();
//        Set<HLTComponent> expResult = null;
//        Set<HLTComponent> result = instance.findAllParentComponents();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of findParentComponents method, of class HLTComponent.
//     */
//    @Test
//    public void testFindParentComponents() throws Exception {
//        System.out.println("findParentComponents");
//        HLTComponent instance = new HLTComponent();
//        ArrayList<HLTComponent> expResult = null;
//        ArrayList<HLTComponent> result = instance.findParentComponents();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of findParentElements method, of class HLTComponent.
//     */
//    @Test
//    public void testFindParentElements() throws Exception {
//        System.out.println("findParentElements");
//        HLTComponent instance = new HLTComponent();
//        ArrayList<HLTTriggerElement> expResult = null;
//        ArrayList<HLTTriggerElement> result = instance.findParentElements();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of setChildren method, of class HLTComponent.
//     */
//    @Test
//    public void testSetChildren() {
//        System.out.println("setChildren");
//        List<HLTComponent> children = null;
//        HLTComponent instance = new HLTComponent();
//        instance.setChildren(children);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of loadAllChildrenComponents method, of class HLTComponent.
//     */
//    @Test
//    public void testLoadAllChildrenComponents() throws Exception {
//        System.out.println("loadAllChildrenComponents");
//        ArrayList<HLTComponent> parents = null;
//        ArrayList<HLTComponent> expResult = null;
//        ArrayList<HLTComponent> result = HLTComponent.loadAllChildrenComponents(parents);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getTableName method, of class HLTComponent.
//     */
//    @Test
//    public void testGetTableName() {
//        System.out.println("getTableName");
//        HLTComponent instance = new HLTComponent();
//        String expResult = "";
//        String result = instance.getTableName();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getChains method, of class HLTComponent.
//     */
//    @Test
//    public void testGetChains() throws Exception {
//        System.out.println("getChains");
//        int hlt_menu_id = 0;
//        HLTComponent instance = new HLTComponent();
//        ArrayList<HLTTriggerChain> expResult = null;
//        ArrayList<HLTTriggerChain> result = instance.getChains(hlt_menu_id);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//    
}
