package triggerdb.Entities.HLT;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import static triggerdb.Entities.HLT.HLTPrescaleType.*;
import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.*;
import com.mockrunner.mock.jdbc.*;
import java.sql.PreparedStatement;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Logger;
import javax.swing.tree.DefaultMutableTreeNode;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.TriggerMap;
import triggerdb.Entities.alias.PrescaleSetAlias;

/**
 *
 * @author dwthomas
 */
public class HLTTriggerTypeTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public HLTTriggerTypeTest() {
    }
    
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
        ConnectionManager.getInstance().unsetTestingConnection();
        }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
        }

    @Test
    public void testForceLoad() throws Exception {
    }

    @Test
    public void testClone() {
        System.out.println("clone");
        HLTTriggerType instance = new HLTTriggerType();
        Object expResult = instance;
        Object result = instance.clone();
        assertEquals(expResult, result);
    }

    @Test
    public void testEquals() {
        System.out.println("equals");
        
        HLTTriggerType instance = new HLTTriggerType();
        HLTTriggerType param1 = new HLTTriggerType();
        HLTTriggerType param2 = new HLTTriggerType();
        Boolean expTrueResult = true;
        Boolean expFalseResult = false;
            
        String IncorrectType = "";
        Object Result1 = param1.equals(IncorrectType);
        assertEquals(Result1,expFalseResult);
        assertNotEquals(Result1,expTrueResult);
        
        instance.set_id(-2); 
        param1.set_id(-2);
        param2.set_id(-3);
        Object TrueResult = param1.equals(instance);
        Object FalseResult = param2.equals(instance);
        assertEquals(TrueResult,expTrueResult);
        assertEquals(FalseResult,expFalseResult);
        
        instance.set_id(-1);
        param1.set_trigger_chain_id(-2);
        param2.set_trigger_chain_id(-3);
        instance.set_trigger_chain_id(-2);
        param1.set_typebit(-88);
        param2.set_typebit(-99);
        instance.set_typebit(-88);
        Object TrueTrueResult = param1.equals(instance);
        Object FalseFalseResult = param2.equals(instance);
        assertEquals(TrueTrueResult, expTrueResult);
        assertEquals(FalseFalseResult,expFalseResult);
        param1.set_trigger_chain_id(-3);
        param2.set_typebit(-88);
        Object TrueFalseResult = param2.equals(instance);
        Object FalseTrueResult = param1.equals(instance);
        assertEquals(TrueFalseResult, expFalseResult);
        assertEquals(FalseTrueResult,expFalseResult);
        
        instance.set_id(-2);
        param1.set_id(-1);
        param1.set_trigger_chain_id(-2);
        param2.set_trigger_chain_id(-3);
        instance.set_trigger_chain_id(-2);
        param1.set_typebit(-88);
        param2.set_typebit(-99);
        instance.set_typebit(-88);
        Object TrueTrueResult2 = param1.equals(instance);
        Object FalseFalseResult2 = param2.equals(instance);
        assertEquals(TrueTrueResult2, expTrueResult);
        assertEquals(FalseFalseResult2,expFalseResult);
        param1.set_trigger_chain_id(-3);
        param2.set_typebit(-88);
        Object TrueFalseResult2 = param2.equals(instance);
        Object FalseTrueResult2 = param1.equals(instance);
        assertEquals(TrueFalseResult2, expFalseResult);
        assertEquals(FalseTrueResult2,expFalseResult);
        
        instance.set_id(-1);
        param2.set_id(-1);
        param1.set_trigger_chain_id(-1);
        param2.set_trigger_chain_id(-1);
        instance.set_trigger_chain_id(-1);
        param1.set_typebit(-88);
        param2.set_typebit(-99);
        instance.set_typebit(-88);
        Object TrueResult2 = param1.equals(instance);
        Object FalseResult2 = param2.equals(instance);
        assertEquals(TrueResult2, expTrueResult);
        assertEquals(FalseResult2,expFalseResult);
        
        param1.set_trigger_chain_id(-2);
        param2.set_trigger_chain_id(-2);
        instance.set_trigger_chain_id(-1);
        param1.set_typebit(-88);
        param2.set_typebit(-99);
        instance.set_typebit(-88);
        Object TrueResult3 = param1.equals(instance);
        Object FalseResult3 = param2.equals(instance);
        assertEquals(TrueResult3, expTrueResult);
        assertEquals(FalseResult3,expFalseResult);
        
        param1.set_trigger_chain_id(-1);
        param2.set_trigger_chain_id(-1);
        instance.set_trigger_chain_id(-2);
        param1.set_typebit(-88);
        param2.set_typebit(-99);
        instance.set_typebit(-88);
        Object TrueResult4 = param1.equals(instance);
        Object FalseResult4 = param2.equals(instance);
        assertEquals(TrueResult4, expTrueResult);
        assertEquals(FalseResult4,expFalseResult);
    }

    @Test
    public void testSave() throws Exception {
         System.out.println("save");
         HLTTriggerType instance = new HLTTriggerType();
         MockResultSet resultset = resultSetHandler.createResultSet();
         setEmptyInitInfo();
         Integer expResult = 1;
         Object Result = instance.save();
         assertEquals (Result, expResult);
    }

    @Test
    public void testGet_trigger_chain_id() {
        System.out.println("get_trigger_chain_id");
        HLTTriggerType instance = new HLTTriggerType();
        Integer expResult = -1;
        Object Result = instance.get_trigger_chain_id();
        assertEquals(expResult,Result);
    }

    @Test
    public void testSet_trigger_chain_id() {
        System.out.println("set_trigger_chain_id");
        HLTTriggerType instance = new HLTTriggerType();
        Integer input = -1992;
        instance.set_trigger_chain_id(input);
        Integer expResult = instance.get_trigger_chain_id();
        assertEquals(expResult,input);
    }

    @Test
    public void testGet_trigger_chain() throws Exception {
        System.out.println("get_trigger_chain");
        HLTTriggerType instance = new HLTTriggerType();
        HLTTriggerChain expResult = null;
        Object Result = instance.get_trigger_chain();
        assertEquals(expResult,Result);
    }

    @Test
    public void testGet_typebit() {
        System.out.println("get_typebit");
        HLTTriggerType instance = new HLTTriggerType();
        Integer expResult = -1;
        Object Result = instance.get_typebit();
        assertEquals(expResult,Result);
    }

    @Test
    public void testSet_typebit() {
        System.out.println("set_typebit");
        HLTTriggerType instance = new HLTTriggerType();
        Integer input = -1992;
        instance.set_typebit(input);
        Integer expResult = instance.get_typebit();
        assertEquals(expResult,input);
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        HLTTriggerType instance = new HLTTriggerType();
        String expResult = "HLT Type";
        String result = instance.toString();
        assertEquals(expResult, result);
        
        HLTTriggerType instance2 = getHLTTriggerTypeWithID(-99);
        String expResult2 = "TRIGGER TYPE: -1 (DBid=-99)";
        String result2 = instance2.toString();
        assertEquals(expResult2, result2);
    }

    /**
     * Helper function to make a HLTParameter and handle
     * SQLExceptions.
     * @param id
     * @return 
     */
    public HLTTriggerType getHLTTriggerTypeWithID(int id) {
        HLTTriggerType result = null;
        try {
            result = new HLTTriggerType(id);
        } catch (SQLException e) {
            fail(e.toString());
        }
        return result;
    }
   
    
    @Test
    public void testGet_name() {
        System.out.println("get_name");
        HLTTriggerType instance = new HLTTriggerType();
        String expResult = "type -1";
        String result = instance.get_name();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_min_names() {
        System.out.println("get_min_names");
        HLTTriggerType instance = getHLTTriggerTypeWithID(-1992);
        
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("ID");
        expResult.add("Chain ID");
        expResult.add("Type Bit");
        
        ArrayList<String> result = instance.get_min_names();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_min_info() throws Exception {
        System.out.println("get_min_info");
       
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
         
        HLTTriggerType instance1 = new HLTTriggerType();
        HLTTriggerType instance2 = getHLTTriggerTypeWithID(2712);
        instance2.set_trigger_chain(null);
        HLTTriggerType instance3 = getHLTTriggerTypeWithID(2712);
        HLTTriggerChain param = new HLTTriggerChain();
        instance3.set_trigger_chain(param);

        
        ArrayList<Object> expResult1 = new ArrayList<>();
        expResult1.add("Filled on save");
        expResult1.add("Filled on save");
        expResult1.add(-1); 
        
        ArrayList<Object> expResult2 = new ArrayList<>();
        expResult2.add(2712);
        expResult2.add("Invalid");
        expResult2.add(-1); 
        
        ArrayList<Object> expResult3 = new ArrayList<>();
        expResult3.add(2712);
        expResult3.add("-1 (  ) ");
        expResult3.add(-1); 
        
        ArrayList<Object> result1 = instance1.get_min_info();
        ArrayList<Object> result2 = instance2.get_min_info();
        ArrayList<Object> result3 = instance3.get_min_info();
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
        assertEquals(expResult3, result3);
    
    }

    @Test
    public void testGetTableName() {
        System.out.println("getTableName");
        HLTTriggerType instance = new HLTTriggerType();
        String expResult = "HLT_TRIGGER_TYPE";
        String Result = instance.getTableName();
        assertEquals(expResult, Result);
    }
    
}
