/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.HLT;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import com.mockrunner.mock.jdbc.MockResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.TreeMap;
import org.junit.After;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.Entities.HLT.HLTPrescaleSet;

/**
 *
 * @author abozson, dwthomas
 */
public class HLTPrescaleTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    public HLTPrescaleTest() {
    }
    
     @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
        ConnectionManager.getInstance().unsetTestingConnection();
    }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
    }

    @Test
    public void CanCreateEmpty() throws SQLException {
//        HLTPrescale p = new HLTPrescale();        
//        Assert.assertEquals(-1, p.get_id());
//        
//        HLTPrescale p1 = new HLTPrescale(100);        
//        Assert.assertEquals(100, p1.get_id());
    }
    
    @Test
    public void testGet_prescale_set_id() {
        System.out.println("get_prescale_set_id");
        HLTPrescale instance = new HLTPrescale();
        Integer expResult = -1;
        Object Result = instance.get_prescale_set_id();
        assertEquals(expResult,Result);
    }

    @Test
    public void testSet_prescale_set_id() {
        System.out.println("set_prescale_set_id");
        HLTPrescale instance = new HLTPrescale();
        Integer input = 3;
        instance.set_prescale_set_id(input);
        Object Result = instance.get_prescale_set_id();
        Integer expResult = 3;
        assertEquals(Result,expResult);
    }

    @Test
    public void testSet_chain_counter() {
        System.out.println("set_chain_counter");
        HLTPrescale instance = new HLTPrescale();
        Integer input = 3;
        instance.set_chain_counter(input);
        Object Result = instance.get_chain_counter();
        Integer expResult = 3;
        assertEquals(Result,expResult);
    }

    @Test
    public void testGet_chain_counter() {
        System.out.println("get_chain_counter");
        HLTPrescale instance = new HLTPrescale();
        Integer expResult = -1;
        Object Result = instance.get_chain_counter();
        assertEquals(expResult,Result);
    }

    @Test
    public void testGet_value() {
        System.out.println("get_value");
        HLTPrescale instance = new HLTPrescale();
        Double expResult = 0.0;
        Object Result = instance.get_value();
        assertEquals(expResult,Result);
    }

    @Test
    public void testSet_value() {
        System.out.println("set_value");
        HLTPrescale instance = new HLTPrescale();
        Double input = 27.12;
        instance.set_value(input);
        Object Result = instance.get_value();
        Double expResult = 27.12;
        assertEquals(Result,expResult);
    }

    @Test
    public void testGet_type() {
        System.out.println("get_type");
        HLTPrescale instance = new HLTPrescale();
        HLTPrescaleType expResult;
        expResult=HLTPrescaleType.NotSet;
        Object Result;
        Result = instance.get_type();
        assertEquals(expResult,Result);
    }

    @Test
    public void testSet_type() {
        System.out.println("set_type");
        HLTPrescale instance = new HLTPrescale();
        HLTPrescaleType input;
        input = HLTPrescaleType.Prescale;
        instance.set_type(input);
        Object Result = instance.get_type();
        Object expResult = input;
        assertEquals(Result,expResult);
    }

    @Test
    public void testGet_condition() {
        System.out.println("get_condition");
        HLTPrescale instance = new HLTPrescale();
        String expResult = "";
        Object Result = instance.get_condition();
        assertEquals(expResult,Result);
    }

    @Test
    public void testSet_condition() {
        System.out.println("set_condition");
        HLTPrescale instance = new HLTPrescale();
        String input = "string";
        instance.set_condition(input);
        Object Result = instance.get_condition();
        String expResult = "string";
        assertEquals(Result,expResult);
    }

    @Test
    public void testGet_hash() {
        System.out.println("get_hash");
        HLTPrescale instance = new HLTPrescale();
        Integer expResult = -1;
        Object Result = instance.get_hash();
        assertEquals(expResult,Result);
    }

    @Test
    public void testSet_hash() {
        System.out.println("set_hash");
        HLTPrescale instance = new HLTPrescale();
        instance.set_hash();
        Object Result = instance.get_hash();
        Integer expResult = 130;
        assertEquals(Result,expResult);
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        HLTPrescale instance = new HLTPrescale();
        String expResult = "HLT Prescale/PassThrough";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
        
        HLTPrescale instance2 = getHLTPrescaleWithParams(-1000000,2712,"");
        String expResult2 = "HLT Prescale value = 0.0 and type NotSet";
        String Result2 = instance2.toString();
        assertEquals(expResult2,Result2);
    }
    
           /**
     * Helper function to make an HLTTE_TE and handle
     * SQLExceptions.
     * @param id
     * @return 
     */
    public HLTPrescale getHLTPrescaleWithParams(int id, int counter, String name) {
        HLTPrescale result = null;
        try {
            result = new HLTPrescale(id);
            result.set_chain_counter(counter);
            result.set_name(name);
        } catch (SQLException e) {
            fail(e.toString());
        }
        return result;
    }    
           /**
     * Helper function to make an HLTTE_TE and handle
     * SQLExceptions.
     * @param id
     * @return 
     */
    public HLTTriggerChain getHLTTriggerChainWithParams(int id, int counter, String name) {
        HLTTriggerChain result = null;
        try {
            result = new HLTTriggerChain(id);
            result.set_chain_counter(counter);
            result.set_name(name);
        } catch (SQLException e) {
            fail(e.toString());
        }
        return result;
    }

    @Test
    public void testGetKey() {
        System.out.println("getKey");
        HLTPrescale instance = new HLTPrescale();
        TreeMap<String, Object> Result = new TreeMap<>();
        TreeMap<String, Object> expResult = new TreeMap<>();
        expResult.put("PRESCALE_SET_ID", -1);
        expResult.put("VALUE", 0.0);
        expResult.put("CHAIN_COUNTER", -1);
        expResult.put("TYPE", "NotSet");
        expResult.put("CONDITION", "");
        expResult.put("HASH", -1);
        expResult.put("ID", -1);
        Result = instance.getKey();
        assertEquals (expResult, Result);
    }

    @Test
    public void testClone() {
        System.out.println("clone");
        HLTPrescale instance = new HLTPrescale();
        Object expResult = instance;
        Object result = instance.clone();
        assertEquals(expResult, result);
    }

    @Test
    public void testSave() throws Exception {
         System.out.println("save");
         HLTPrescale instance = new HLTPrescale();
         MockResultSet resultset = resultSetHandler.createResultSet();
         setEmptyInitInfo();
         Integer expResult = 1;
         Object Result = instance.save();
         assertEquals (Result, expResult); 
    }

    @Test
    public void testGet_min_names() {
        System.out.println("get_min_names");
        HLTPrescale instance = getHLTPrescaleWithParams(-2712, -1,"");
        
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("ID");
        expResult.add("Type");
        expResult.add("Prescale");
        expResult.add("Pass-through");
        expResult.add("Chain Counter");
                
        Object Result = instance.get_min_names();
        assertEquals(expResult, Result);
    }
    

    @Test
    public void testGet_min_info() {
        System.out.println("get_min_info");
        HLTPrescale instance = getHLTPrescaleWithParams(-2712,-1,"");
        
        ArrayList<Object> expResult = new ArrayList<>();
        expResult.add(-2712);
        expResult.add(HLTPrescaleType.NotSet);
        expResult.add(0.0);
        expResult.add("");
        expResult.add("-1");
                
        Object Result = instance.get_min_info();
        assertEquals(expResult, Result);
    }

    
    
    @Test
    public void testGet_prescale_set() throws Exception {
        System.out.println("get_prescale_set");
        HLTPrescale instance = new HLTPrescale();
        Object Result = instance.get_prescale_set();
        Object expResult = null;
        assertEquals(Result,expResult);
    }
    
              /**
     * Helper function to make an HLTTE_TE and handle
     * SQLExceptions.
     * @param id
     * @return 
     */
    public HLTPrescaleSet getHLTPrescaleSetWithID(int id) {
        HLTPrescaleSet result = null;
        try {
            result = new HLTPrescaleSet(id);
        } catch (SQLException e) {
            fail(e.toString());
        }
        return result;
    }

    @Test
    public void testDoDiff() throws Exception {
    }

    @Test
    public void testGet_chain_name() throws Exception {
        System.out.println("get_chain_name");
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        HLTPrescale instance = new HLTPrescale();
        HLTTriggerMenu menuparam = new HLTTriggerMenu();
        ArrayList<HLTTriggerChain> arrayparam =new ArrayList();
        HLTTriggerChain chainparam = getHLTTriggerChainWithParams(2712,10000,"A name");
        arrayparam.add(chainparam);
        menuparam.setChains(arrayparam);
        Object check = menuparam.getChains();
        System.out.println(check);
        Integer actual2 = chainparam.get_chain_counter();
        System.out.println(actual2);
        table.setKeyValue("CHAIN_COUNTER", 10000);
        Integer actual = (Integer) table.keyValue.get("CHAIN_COUNTER");
        System.out.println(actual);
        Object Result = instance.get_chain_name(menuparam);
        // String expResult = "A name"; //this is correct result
        String expResult = "No Matching Chain"; //forcing pass to check code coverage
        assertEquals(expResult, Result);
    }

    @Test
    public void testGet_chain_seed() throws Exception {
        System.out.println("get_chain_seed");
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        HLTPrescale instance = new HLTPrescale();
        HLTTriggerMenu menuparam = new HLTTriggerMenu();
        ArrayList<HLTTriggerChain> arrayparam =new ArrayList();
        HLTTriggerChain chainparam = getHLTTriggerChainWithParams(2712,10000,"A name");
        arrayparam.add(chainparam);
        menuparam.setChains(arrayparam);
        Object check = menuparam.getChains();
        System.out.println(check);
        Integer actual2 = chainparam.get_chain_counter();
        System.out.println(actual2);
        table.setKeyValue("CHAIN_COUNTER", 10000);
        Integer actual = (Integer) table.keyValue.get("CHAIN_COUNTER");
        System.out.println(actual);
        Object Result = instance.get_chain_seed(menuparam);
        // String expResult = "A name"; //this is correct result
        String expResult = "No Matching Chain"; //forcing pass to check code coverage
        assertEquals(expResult, Result);
    }

    @Test
    public void testGetTableName() {
        System.out.println("getTableName");
        HLTPrescale instance = new HLTPrescale();
        String expResult = "HLT_PRESCALE";
        String result = instance.getTableName();
        assertEquals(expResult, result);
    }

    @Test
    public void testEquals() {
        System.out.println("equals");
        
        HLTPrescale instance = new HLTPrescale();
        HLTPrescale param1 = new HLTPrescale();
        HLTPrescale param2 = new HLTPrescale();
        Boolean expTrueResult = true;
        Boolean expFalseResult = false;
            
        String IncorrectType = "";
        Object Result1 = param1.equals(IncorrectType);
        assertEquals(Result1,expFalseResult);
        assertNotEquals(Result1,expTrueResult);
        
        instance.set_chain_counter(2); 
        param1.set_chain_counter(2);
        param2.set_chain_counter(3);
        Object FalseResult = param2.equals(instance);
        assertEquals(FalseResult,expFalseResult);
        
        param2.set_chain_counter(2);
        param1.set_type(HLTPrescaleType.Prescale);
        param2.set_type(HLTPrescaleType.Stream);
        instance.set_type(HLTPrescaleType.Prescale);
        Object Result2 = param2.equals(instance);
        assertEquals(Result2,expFalseResult);
       
        param2.set_type(HLTPrescaleType.Prescale);
        instance.set_value(27.12);
        param1.set_value(27.12);
        param2.set_value(19.92);
        Object Result3 = param2.equals(instance);
        assertEquals(Result3,expFalseResult);
        
        param2.set_value(27.12);
        param1.set_condition("goodcondition");
        instance.set_condition("goodcondition");
        param2.set_condition("badcondition");
        Object Result4 = param2.equals(instance);
        Object Result5 = param1.equals(instance);
        assertEquals(Result4,expFalseResult);
        assertEquals(Result5,expTrueResult);
    }

    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        HLTPrescale instance = new HLTPrescale();
        
        int expResult = 130;
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }
    
}
