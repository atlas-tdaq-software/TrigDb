/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.HLT;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import static triggerdb.Entities.HLT.HLTPrescaleType.*;
import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.*;
import com.mockrunner.mock.jdbc.*;
import java.util.logging.Logger;
import javax.swing.tree.DefaultMutableTreeNode;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.TriggerMap;

/**
 *
 * @author dwthomas
 */
public class HLTTriggerGroupTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public HLTTriggerGroupTest() {
    }
    
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
        ConnectionManager.getInstance().unsetTestingConnection();
        }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
        }

    @Test
    public void testClone() {
        System.out.println("clone");
        HLTTriggerGroup instance = new HLTTriggerGroup();
        Object expResult = instance;
        Object result = instance.clone();
        assertEquals(expResult, result);
    }

    @Test
    public void testCheckIDs() throws Exception {
        System.out.println("get_TMPSid");
        HLTTriggerGroup instance = new HLTTriggerGroup();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Boolean expResult = true;
        Object result = instance.checkIDs();
        assertEquals(expResult, result);
        
    }

    @Test
    public void testGetKey() {
        System.out.println("getKey");
        HLTTriggerGroup instance = new HLTTriggerGroup();
        TriggerMap<String,Object> expResult = new TriggerMap<>();
        expResult.put("ID",-1);
        expResult.put("NAME","");
        expResult.put("TRIGGER_CHAIN_ID",-1);
        Object Result = instance.getKey();
        assertEquals(expResult,Result);
    }

    @Test
    public void testEquals() {
        System.out.println("equals");
        
        HLTTriggerGroup instance = new HLTTriggerGroup();
        HLTTriggerGroup param1 = new HLTTriggerGroup();
        HLTTriggerGroup param2 = new HLTTriggerGroup();
        Boolean expTrueResult = true;
        Boolean expFalseResult = false;
            
        String IncorrectType = "";
        Object Result1 = param1.equals(IncorrectType);
        assertEquals(Result1,expFalseResult);
        assertNotEquals(Result1,expTrueResult);
        
        instance.set_id(-2); 
        param1.set_id(-2);
        param2.set_id(-3);
        Object TrueResult = param1.equals(instance);
        Object FalseResult = param2.equals(instance);
        assertEquals(TrueResult,expTrueResult);
        assertEquals(FalseResult,expFalseResult);
        
        System.out.println(instance.get_name());
        System.out.println(param1.get_name());
        System.out.println(param2.get_name());
        instance.set_id(-1);
        param1.set_trigger_chain_id(-2);
        param2.set_trigger_chain_id(-3);
        instance.set_trigger_chain_id(-2);
        instance.set_name("");
        param1.set_name("");
        param2.set_name("");
        System.out.println(instance.get_name());
        System.out.println(param1.get_name());
        System.out.println(param2.get_name());
        Object Result = param1.equals(instance);
        Object Result2 = param2.equals(instance);
        assertEquals(Result,expTrueResult);
        assertEquals(Result2,expFalseResult);
       
        instance.set_trigger_chain_id(-1);
        param1.set_trigger_chain_id(-1);
        instance.set_name("goodName");
        param1.set_name("goodName");
        param2.set_name("badName");
        System.out.println(instance.get_name());
        System.out.println(param1.get_name());
        System.out.println(param2.get_name());
        Object Result3 = param1.equals(instance);
        Object Result4 = param2.equals(instance);
        assertEquals(Result3,expTrueResult);
        assertEquals(Result4,expFalseResult);
        
        instance.set_name("notempty");
        param1.set_name("alsonotempty");
        System.out.println(instance.get_name());
        System.out.println(param1.get_name());
        Object Result5 = param1.equals(instance);
        assertEquals(Result5,expFalseResult);
        
        param1.set_name("stillnotempty");
        instance.set_name("stillnotempty");
        Object Result6 = param1.equals(instance);
        assertEquals(Result6,expTrueResult);
        
        param1.set_name("");
        instance.set_name("blah");
        Object Result7 = param1.equals(instance);
        assertEquals(Result7,expTrueResult);
    }

    @Test
    public void testSave() throws Exception {
         System.out.println("save");
         HLTTriggerGroup instance = new HLTTriggerGroup();
         MockResultSet resultset = resultSetHandler.createResultSet();
         setEmptyInitInfo();
         Integer expResult = 1;
         Object Result = instance.save();
         assertEquals (Result, expResult);
    }

    @Test
    public void testGet_trigger_chain_id() {
        System.out.println("get_trigger_chain_id");
        HLTTriggerGroup instance = new HLTTriggerGroup();
        Integer expResult = -1;
        Object Result = instance.get_trigger_chain_id();
        assertEquals(expResult,Result);
    }

    @Test
    public void testSet_trigger_chain_id() {
        System.out.println("set_trigger_chain_id");
        HLTTriggerGroup instance = new HLTTriggerGroup();
        Integer input = 3;
        instance.set_trigger_chain_id(input);
        Object Result = instance.get_trigger_chain_id();
        Integer expResult = 3;
        assertEquals(Result,expResult);
    }
    
    @Test
    public void testGet_trigger_chain() throws SQLException{
        System.out.println("get_trigger_chain");
        HLTTriggerGroup instance = new HLTTriggerGroup();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Object Result = instance.get_trigger_chain_public();
        HLTTriggerChain expResult = null;
        assertEquals(Result,expResult);
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        HLTTriggerGroup instance = new HLTTriggerGroup();
        String expResult = "HLT Group";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
        
        HLTTriggerGroup instance2 = getHLTTriggerGroupWithID(-1000000);
        String expResult2 = "TRIGGER GROUP:  (DBid=-1000000)";
        String Result2 = instance2.toString();
        assertEquals(expResult2,Result2);
    }

       /**
     * Helper function to make an HLTTE_TE and handle
     * SQLExceptions.
     * @param id
     * @return 
     */
    public HLTTriggerGroup getHLTTriggerGroupWithID(int id) {
        HLTTriggerGroup result = null;
        try {
            result = new HLTTriggerGroup(id);
        } catch (SQLException e) {
            fail(e.toString());
        }
        return result;
    }
    
    @Test
    public void testGet_min_names() {
        System.out.println("get_min_names");
        HLTTriggerGroup instance = getHLTTriggerGroupWithID(-2712);
        
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("ID");
        expResult.add("Name");
        expResult.add("Chain ID");
                
        Object Result = instance.get_min_names();
        assertEquals(expResult, Result);
    }

    @Test
    public void testGet_min_info() throws Exception {
        System.out.println("get_min_info");
        HLTTriggerGroup instance = getHLTTriggerGroupWithID(-2712);
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        ArrayList<Object> expResult = new ArrayList<>();
        expResult.add(-2712);
        expResult.add("");
        expResult.add("Invalid");
                
        Object Result = instance.get_min_info();
        assertEquals(expResult, Result);
    }

    @Test
    public void testGetTableName() {
        System.out.println("getTableName");
        HLTTriggerGroup instance = new HLTTriggerGroup();
        String expResult = "HLT_TRIGGER_GROUP";
        String result = instance.getTableName();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetChains() throws Exception {
        System.out.println("getChains");
        HLTTriggerGroup instance = new HLTTriggerGroup();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Object Result = instance.getChains(-1);
        ArrayList<HLTTriggerChain> expResult = new ArrayList<>();
        assertEquals(Result,expResult);
    }
    
}
