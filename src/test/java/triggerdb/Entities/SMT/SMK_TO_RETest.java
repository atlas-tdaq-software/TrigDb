/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.SMT;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import com.mockrunner.mock.jdbc.MockResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;

/**
 *
 * @author davethomas
 */
public class SMK_TO_RETest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public SMK_TO_RETest() {
    }
    
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @After
    public void tearDown() {
    }
    
    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
    }


    @Test
    public void testGet_supermaster_id() {
        System.out.println("get_supermaster_id");
        SMK_TO_RE instance = new SMK_TO_RE();
        Integer expResult = -1;
        Object result = instance.get_supermaster_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_supermaster_id() {
        System.out.println("set_supermaster_id");
        SMK_TO_RE instance = new SMK_TO_RE();
        Integer input = 3;
        instance.set_supermaster_id(input);
        Integer expResult = 3;
        Object result = instance.get_supermaster_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_release_id() {
        System.out.println("set_release_id");
        SMK_TO_RE instance = new SMK_TO_RE();
        Integer input = 3;
        instance.set_release_id(input);
        Integer expResult = 3;
        Object result = instance.get_release_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_release_id() {
        System.out.println("set_release_id");
        SMK_TO_RE instance = new SMK_TO_RE();
        Integer input = 3;
        instance.set_release_id(input);
        Integer expResult = 3;
        Object result = instance.get_release_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testSave() throws Exception {
        System.out.println("save");
        SMK_TO_RE instance = new SMK_TO_RE();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Integer expResult = 1;
        Object result = instance.save();
        assertEquals(expResult, result);
    }

    @Test
    public void testClone() {
    }

    @Test
    public void testGetTableName() {    
        System.out.println("getTableName");
        SMK_TO_RE instance = new SMK_TO_RE();
        String expResult = "HLT_SMT_TO_HRE";
        String result = instance.getTableName();
        assertEquals(expResult, result);
    }
    
}
