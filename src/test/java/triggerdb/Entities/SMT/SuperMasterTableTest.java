/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.SMT;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import com.mockrunner.mock.jdbc.MockResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.Entities.HLT.HLTMaster;
import triggerdb.Entities.HLT.HLTRelease;
import triggerdb.Entities.L1.L1Master;
import triggerdb.Entities.Topo.TopoMaster;
import triggerdb.PrescaleSetAliasComponent;

/**
 *
 * @author davethomas
 */
public class SuperMasterTableTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public SuperMasterTableTest() {
    }
    
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @After
    public void tearDown() throws Exception {
        super.tearDown();
        ConnectionManager.getInstance().unsetTestingConnection();
    }
    
    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
    }

    @Test
    public void testForceLoad() throws Exception {
        
    }

    @Test
    public void testSetComment() throws Exception {
        System.out.println("setComment");
        SuperMasterTable instance = new SuperMasterTable();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        String input = "";
        instance.setComment(input);
        String expResult = "";
        Object result = instance.getComment();
        assertEquals(expResult, result);
        
        SuperMasterTable instance2 = new SuperMasterTable();
        String input2 = "SMS3cIY5WkoGGVlblGBhj6D5ocyaKrmixkGmVncZXEcqpB3J4m7N3mZOf2FI2Rx5nOx9UNDCiwDI2R2xR3nNKrFqIX76Unu833frPK1F4sN9AfjUalqxnBYU3gIovsKvMGeuvBNK7vhaCmE39FjZLvUPExjFEtZ7HqyBD8NYQpUP6OueLo5HFzWSkH55onEAOKsFfzcWbiUjWcZv1qmqZp2cEhz3xF0g1m00bazIUjBrJX3lRS0VX1L8bnqbZ2LoEDoINLWtuxHiq2cNpXt1BwTYrCmj7Qr6QlOWmNuX4yTq1powfnfbR9231JiQ9Y7TyhFBpUAgQ1ADQWTnwCLyoggLUboHTHvtl2aL8cZmZ9WzrYn62iyS6TtbSrS0Ky9M0OyMpjPQlBoGeCaSsSm5u7E62OnjTxaT8rpK2Kx5LoNqORiBU4k3axAbXOKS18y5gEAROOa4aSfHPwb2LzHxnjh4yZiO76htvxkuTAekUis3unDH7O9jtV1ERAY8h9qzrwI9R5aHVurcuX9iv5DziStmCPMJrInZQTSGfi4VneDZboI7jWTMA3oKu4Oogrsvij2IxInkKsyUG2QTIEov0l7PfN1immws791pen0KCiDn0YPUKXLblEFc2fizeAsgElncQEmRaTKRbibJFYYwJLpAMNWh8W1WwSy6XkIk0OawpueIr43hYfs8ukpeggTZyIZWuYGOQSXFrDHNH4ypS5jkQF6pyI3eir6EJcgk20cH9GRlApE91iJXHwaT2Vu1ZwOjaBC2AoiO9msSEAnPolBaUODoNDehkXQAsaHtGqp5hDQyZ2QmvSOrJrtoNkamIDEfZDJsxGHc9PubbcxhUoujAuTkgMutIVkOpADx13TOClQWZDCllXlc4QNRLyj9Jr7MlYKyJ7bYpIuhFCWjDuI4q1r4hppXHmgZxERHvfJKAIPfKB1W1jBJY9vCATykQhq4vPsjzyaGZIo00TnaYQnzj0aK4rYAZDnrasmu3";
        instance2.setComment(input2);
        String expResult2 = "";
        Object result2 = instance2.getComment();
        assertEquals(expResult2, result2);
        
        
    }

    @Test
    public void testGetComment() throws Exception {
        System.out.println("getComment");
        SuperMasterTable instance = new SuperMasterTable();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        String expResult = "";
        Object result = instance.getComment();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetReleases() throws Exception {
        System.out.println("setReleases");
        SuperMasterTable instance = new SuperMasterTable();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        ArrayList<HLTRelease> input = new ArrayList<>();
        HLTRelease param = new HLTRelease();
        input.add(param);
        instance.setReleases(input);
        Object expResult = input;
        Object result = instance.getReleases();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetReleases() throws Exception {
        System.out.println("getReleases");
        SuperMasterTable instance = new SuperMasterTable();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        ArrayList<HLTRelease> expResult = new ArrayList<>();
        Object result = instance.getReleases();
        assertEquals(expResult, result);
        
        SuperMasterTable instance2 = new SuperMasterTable();
        setEmptyInitInfo();
        ArrayList<HLTRelease> expResult2 = new ArrayList<>();
        HLTRelease input = new HLTRelease();
        input.set_id(2712);
        expResult2.add(input);
        Object result2 = instance2.getReleases();
        assertEquals(expResult, result2);
    }

    @Test
    public void testGet_l1_master_table_id() {
        System.out.println("get_l1_master_table_id");
        SuperMasterTable instance = new SuperMasterTable();
        Integer expResult = -1;
        Object result = instance.get_l1_master_table_id();
        assertEquals(expResult, result);
    }                  

    @Test
    public void testGet_topo_master_table_id() {
        System.out.println("get_topo_master_table_id");
        SuperMasterTable instance = new SuperMasterTable();
        Integer expResult = -1;
        Object result = instance.get_topo_master_table_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_hlt_master_table_id() {
        System.out.println("get_hlt_master_table_id");
        SuperMasterTable instance = new SuperMasterTable();
        Integer expResult = -1;
        Object result = instance.get_hlt_master_table_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_l1_master_table() throws Exception {
        System.out.println("get_l1_master_table");
        SuperMasterTable instance = new SuperMasterTable();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        L1Master expResult = new L1Master();
        Object result = instance.get_l1_master_table();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_topo_master_table() throws Exception {
        System.out.println("get_topo_master_table");
        SuperMasterTable instance = new SuperMasterTable();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        TopoMaster expResult = new TopoMaster();
        Object result = instance.get_topo_master_table();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_hlt_master_table() throws Exception {
        System.out.println("get_hlt_master_table");
        SuperMasterTable instance = new SuperMasterTable();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        HLTMaster expResult = new HLTMaster();
        Object result = instance.get_hlt_master_table();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_fromcommandline() {
        System.out.println("set_fromcommandline");
        SuperMasterTable instance = new SuperMasterTable();
        boolean expResult = true;
        instance.set_fromcommandline();
        Object Result = instance.get_fromcommandline();
        assertEquals(expResult, Result);
    }

    @Test
    public void testSet_l1_master_table() throws SQLException {
        System.out.println("set_l1_master_table");
        SuperMasterTable instance = new SuperMasterTable();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        L1Master expResult = new L1Master();
        instance.set_l1_master_table(expResult);
        Object Result = instance.get_l1_master_table();
        assertEquals(expResult, Result);
    }

    @Test
    public void testSet_topo_master_table() throws SQLException {
        System.out.println("set_topo_master_table");
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        SuperMasterTable instance = new SuperMasterTable();
        TopoMaster expResult = new TopoMaster();
        instance.set_topo_master_table(expResult);
        Object Result = instance.get_topo_master_table();
        assertEquals(expResult, Result);
    }

    @Test
    public void testSet_origin() {
        System.out.println("set_origin");
        SuperMasterTable instance = new SuperMasterTable();
        String input = "";
        instance.set_origin(input);
        String expResult = "";
        Object result = instance.get_origin();
        assertEquals(expResult, result);
        
    }

    @Test
    public void testGet_origin() {
        System.out.println("get_origin");
        SuperMasterTable instance = new SuperMasterTable();
        String expResult = "";
        Object result = instance.get_origin();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_parent_key() {
        System.out.println("set_parent_key");
        SuperMasterTable instance = new SuperMasterTable();
        Integer input = 3;
        instance.set_parent_key(input);
        Integer expResult = 3;
        Object result = instance.get_parent_key();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_parent_key() {
        System.out.println("get_parent_key");
        SuperMasterTable instance = new SuperMasterTable();
        Integer expResult = 0;
        Object result = instance.get_parent_key();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_status() {
        System.out.println("get_status");
        SuperMasterTable instance = new SuperMasterTable();
        Integer expResult = 0;
        Object result = instance.get_status();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_status() {
        System.out.println("set_status");
        SuperMasterTable instance = new SuperMasterTable();
        Integer input = 3;
        instance.set_status(input);
        Integer expResult = 3;
        Object result = instance.get_status();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_hlt_master_table() throws SQLException {
        System.out.println("set_hlt_master_table");
        SuperMasterTable instance = new SuperMasterTable();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        HLTMaster input = new HLTMaster();
        input.set_id(2712);
        instance.set_hlt_master_table(input);
        Object expResult = input;
        Object result = instance.get_hlt_master_table();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_l1_master_table_id() {
        System.out.println("set_l1_master_table_id");
        SuperMasterTable instance = new SuperMasterTable();
        Integer input = 3;
        instance.set_l1_master_table_id(input);
        Integer expResult = 3;
        Object result = instance.get_l1_master_table_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_topo_master_table_id() {
        System.out.println("set_topo_master_table_id");
        SuperMasterTable instance = new SuperMasterTable();
        Integer input = 3;
        instance.set_topo_master_table_id(input);
        Integer expResult = 3;
        Object result = instance.get_topo_master_table_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_hlt_master_table_id() {
        System.out.println("set_hlt_master_table_id");
        SuperMasterTable instance = new SuperMasterTable();
        Integer input = 3;
        instance.set_hlt_master_table_id(input);
        Integer expResult = 3;
        Object result = instance.get_hlt_master_table_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testAddToTree() throws Exception {
    }

    @Test
    public void testClone() {
        System.out.println("clone");
        SuperMasterTable instance = new SuperMasterTable();
        Object expResult = instance;
        Object result = instance.clone();
        assertEquals(expResult, result);
    }

    //@Test
    public void testSave() throws Exception {
        System.out.println("save");
        SuperMasterTable instance = new SuperMasterTable();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Integer expResult = 1;
        Object result = instance.save();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetAllSMKs() throws Exception {
        System.out.println("getAllSMKs");
        SuperMasterTable instance = new SuperMasterTable();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        ArrayList<Integer> expResult = new ArrayList<>();
        Object result = instance.getAllSMKs();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetAllSMKsAndNames() throws Exception {
        System.out.println("getAllSMKsAndNames");
        SuperMasterTable instance = new SuperMasterTable();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        ArrayList<String> expResult = new ArrayList<>();
        Object result = instance.getAllSMKsAndNames();
        assertEquals(expResult, result);
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        SuperMasterTable instance = new SuperMasterTable();
        String expResult= "Super Master Table";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
                
        SuperMasterTable instance2 = new SuperMasterTable();
        instance2.set_id(2712);
        String expResult2 = "2712:  v1";
        Object Result2 = instance2.toString();
        assertEquals(expResult2, Result2);
    }

    @Test
    public void testDoDiff() throws Exception {
    }

    @Test
    public void testSet_comment() {
        System.out.println("set_comment");
        SuperMasterTable instance = new SuperMasterTable();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        String input = "";
        instance.set_comment(input);
        String expResult = "";
        Object result = instance.get_comment();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_comment() throws Exception {
        System.out.println("get_comment");
        SuperMasterTable instance = new SuperMasterTable();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        String input = null;
        instance.set_comment(input);
        String expResult = "null comment";
        Object result = instance.get_comment();
        assertEquals(expResult, result);
        
        SuperMasterTable instance2 = new SuperMasterTable();
        String input2 = "a comment";
        instance2.set_comment(input2);
        String expResult2 = "a comment";
        Object result2 = instance2.get_comment();
        assertEquals(expResult2, result2);
    }

    @Test
    public void testEquals() {
       System.out.println("equals");
        
        SuperMasterTable instance = new SuperMasterTable();
        SuperMasterTable param = new SuperMasterTable();
        Boolean expTrueResult = true;
        Boolean expFalseResult = false;
            
        String IncorrectType = "";
        Object Result1 = param.equals(IncorrectType);
        assertEquals(Result1,expFalseResult);
        assertNotEquals(Result1,expTrueResult);
        
        instance.set_name("right name"); 
        param.set_name("right name");
        Object TrueResult = param.equals(instance);
        assertEquals(TrueResult,expTrueResult);
    }

    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        //unstable!
    }

    @Test
    public void testGet_min_names() {
        System.out.println("get_min_names");
        SuperMasterTable instance = new SuperMasterTable();
        
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("ID");
        expResult.add("Name");
        expResult.add("Version");
        expResult.add("Comment");
        expResult.add("Origin");
        expResult.add("Releases");
        expResult.add("Status");
        expResult.add("Menu Consistency");
        expResult.add("Creator");
        expResult.add("Date");
                
        Object Result = instance.get_min_names();
        assertEquals(expResult, Result);
    }

    @Test
    public void testGet_min_info() {
        System.out.println("get_min_info");
        SuperMasterTable instance = new SuperMasterTable();
        Timestamp current = new Timestamp(System.currentTimeMillis());
        
        ArrayList<Object> expResult = new ArrayList<>();
        expResult.add(-1);
        expResult.add("");
        expResult.add(1);
        expResult.add("");
        expResult.add("");
        expResult.add("none");
        expResult.add("");
        expResult.add("Not Run");
        expResult.add("nobody");
        //expResult.add(current);
                
        ArrayList<Object> Result = instance.get_min_info();
        Result.remove(Result.size() - 1);
        assertEquals(expResult, Result);
        
        SuperMasterTable instance2 = new SuperMasterTable();
        instance2.set_origin("EVOLUTION");
        
        ArrayList<HLTRelease> input2 = new ArrayList<>();
        HLTRelease param2 = new HLTRelease();
        input2.add(param2);
        instance2.setReleases(input2);
        
        instance2.set_status(1);
        instance2.setStatus(StatusBits.VALID, true);
        instance2.setStatus(StatusBits.MC, true);
        instance2.setStatus(StatusBits.REP, true);
        
        ArrayList<Object> expResult2 = new ArrayList<>();
        expResult2.add(-1);
        expResult2.add("");
        expResult2.add(1);
        expResult2.add("");
        expResult2.add("evolution of 0");
        expResult2.add("");
        expResult2.add("OnL, Val, MC, Rep");
        expResult2.add("Not Run");
        expResult2.add("nobody");
        //Timestamp current2 = new Timestamp(System.currentTimeMillis());
        //expResult2.add(current2);
                
        ArrayList<Object> Result2 = instance2.get_min_info();
        Result2.remove(Result2.size() - 1);
        assertEquals(expResult2, Result2);
    }

    @Test
    public void testIsOnline() {
        //tested implicitly in testGet_min_info
    }

    @Test
    public void testIsValid() {
    //tested implicitly in testGet_min_info
    }

    @Test
    public void testIsMC() {        
        //tested implicitly in testGet_min_info
    }

    @Test
    public void testIsRep() {
        //tested implicitly in testGet_min_info
    }

    @Test
    public void testGetStatus() {
        //tested implicitly in testGet_min_info
    }

    @Test
    public void testSetStatus() {
        //partially tested within testGet_min_info
        
        System.out.println("setStatus");
        SuperMasterTable instance = new SuperMasterTable();
        
        boolean expResult = false;
        
        instance.setStatus(StatusBits.VALID, true);
        instance.setStatus(StatusBits.VALID, false);
        Object Result = instance.getStatus(StatusBits.VALID);
        assertEquals(expResult, Result);
        
        
    }

    @Test
    public void testSetValid() {
        System.out.println("setValid");
        SuperMasterTable instance = new SuperMasterTable();
        instance.setValid(true);
        Object Result = instance.isValid();
        boolean expResult = true;
        assertEquals(expResult, Result);
    }

    @Test
    public void testSetOnline() {
        System.out.println("setOnline");
        SuperMasterTable instance = new SuperMasterTable();
        instance.setOnline(true);
        Object Result = instance.isOnline();
        boolean expResult = true;
        assertEquals(expResult, Result);
    }

    @Test
    public void testSetMC() {
        System.out.println("setMC");
        SuperMasterTable instance = new SuperMasterTable();
        instance.setMC(true);
        Object Result = instance.isMC();
        boolean expResult = true;
        assertEquals(expResult, Result);
    }

    @Test
    public void testSetRep() {
        System.out.println("setRep");
        SuperMasterTable instance = new SuperMasterTable();
        instance.setRep (true);
        Object Result = instance.isRep();
        boolean expResult = true;
        assertEquals(expResult, Result);
    }

    @Test
    public void testSetConsistencyResult() throws Exception {
        System.out.println("setConsistencyResult");
        SuperMasterTable instance = new SuperMasterTable();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        instance.set_id(3);
        Boolean expTrueResult = true;
        Boolean expFalseResult = false;
        
        instance.setConsistencyResult(false, false, false);
        Object result1 = instance.getStatus(StatusBits.CONS1);
        Object result2 = instance.getStatus(StatusBits.CONS2);
        assertEquals(expFalseResult, result1);
        assertEquals(expFalseResult, result2);
        
        instance.setConsistencyResult(true, false, false);
        Object result3 = instance.getStatus(StatusBits.CONS1);
        Object result4 = instance.getStatus(StatusBits.CONS2);
        assertEquals(expTrueResult, result3);
        assertEquals(expTrueResult, result4);
        
        instance.setConsistencyResult(true, false, true);
        Object result5 = instance.getStatus(StatusBits.CONS1);
        Object result6 = instance.getStatus(StatusBits.CONS2);
        assertEquals(expTrueResult, result5);
        assertEquals(expFalseResult, result6);
        
        instance.setConsistencyResult(true, true, false);
        Object result7 = instance.getStatus(StatusBits.CONS1);
        Object result8 = instance.getStatus(StatusBits.CONS2);
        assertEquals(expFalseResult, result7);
        assertEquals(expTrueResult, result8);
    }

    @Test
    public void testGetTableName() {
        
    }

    @Test
    public void testGetAliases() throws Exception {
        System.out.println("getAliases");
        SuperMasterTable instance = new SuperMasterTable();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        List<PrescaleSetAliasComponent> expResult = new ArrayList();
        Object Result = instance.getAliases();
        assertEquals(expResult, Result);
    }

    @Test
    public void testUpdateSmtUsed() throws Exception {
    }

    @Test
    public void testSave_releases() throws Exception {
    }
    
}
