/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.L1Links;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import com.mockrunner.mock.jdbc.MockResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.Entities.HLT.HLTTriggerGroup;

/**
 *
 * @author davethomas
 */
public class L1PitsTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public L1PitsTest() {
    }
    
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @After
    public void tearDown() {
    }
    
    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
    }

    @Test
    public void testDelete() throws Exception {
    }

    @Test
    public void testGet_pit_number() {
        System.out.println("get_pit_number");
        L1Pits instance = new L1Pits();
        Integer expResult = 0;
        Object result = instance.get_pit_number();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_pit_number() {
        System.out.println("set_pit_number");
        L1Pits instance = new L1Pits();
        Integer input = 3;
        instance.set_pit_number(input);
        Integer expResult = 3;
        Object result = instance.get_pit_number();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_threshold_bit() {
        System.out.println("get_threshold_bit");
        L1Pits instance = new L1Pits();
        Integer expResult = 0;
        Object result = instance.get_threshold_bit();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_threshold_bit() {
        System.out.println("set_threshold_bit");
        L1Pits instance = new L1Pits();
        Integer input = 3;
        instance.set_threshold_bit(input);
        Integer expResult = 3;
        Object result = instance.get_threshold_bit();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_tm_to_tt_id() {
        System.out.println("get_tm_to_tt_id");
        L1Pits instance = new L1Pits();
        Integer expResult = 0;
        Object result = instance.get_tm_to_tt_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_tm_to_tt_id() {
        System.out.println("set_tm_to_tt_id");
        L1Pits instance = new L1Pits();
        Integer input = 3;
        instance.set_tm_to_tt_id(input);
        Integer expResult = 3;
        Object result = instance.get_tm_to_tt_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testSave() throws Exception {
        System.out.println("save");
        L1Pits instance = new L1Pits();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Integer expResult = 1;
        Object result = instance.save();
        assertEquals(expResult, result);
    }

    @Test
    public void testClone() {
        System.out.println("clone");
        L1Pits instance = new L1Pits();
        Object expResult = instance;
        Object result = instance.clone();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetTableName() {
        System.out.println("getTableName");
        L1Pits instance = new L1Pits();
        String expResult = "L1_PITS";
        String result = instance.getTableName();
        assertEquals(expResult, result);
    }
    
}
