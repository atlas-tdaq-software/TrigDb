/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.L1Links;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import com.mockrunner.mock.jdbc.MockResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.Entities.L1.L1Threshold;

/**
 *
 * @author davethomas
 */
public class L1TM_TTTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public L1TM_TTTest() {
    }
    
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @After
    public void tearDown() {
    }
    
    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
    }

    @Test
    public void testGet_threshold() {
        System.out.println("get_threshold");
        L1TM_TT instance = new L1TM_TT();
        L1Threshold param = new L1Threshold();
        param.set_id(100);
        Object expResult = param;
        instance.set_threshold(param);
        Object Result = instance.get_threshold();
        assertEquals(expResult,Result);
        
        L1TM_TT instance2 = new L1TM_TT();
        L1Threshold param2 = null;
        Object expResult2 = param;
        instance2.set_threshold(param);
        Object Result2 = instance2.get_threshold();
        assertEquals(expResult2,Result2);
    }

    @Test
    public void testGet_threshold_id() {
        System.out.println("get_threshold_id");
        L1TM_TT instance = new L1TM_TT();
        Integer expResult = -1;
        Object result = instance.get_threshold_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_cable_connector() {
        System.out.println("set_cable_connector");
        L1TM_TT instance = new L1TM_TT();
        String input = "a string";
        instance.set_cable_connector(input);
        String expResult = "a string";
        Object result = instance.get_cable_connector();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_cable_start() {
        System.out.println("set_cable_start");
        L1TM_TT instance = new L1TM_TT();
        Integer input = 3;
        instance.set_cable_start(input);
        Integer expResult = 3;
        Object result = instance.get_cable_start();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_cable_end() {
        System.out.println("set_cable_end");
        L1TM_TT instance = new L1TM_TT();
        Integer input = 3;
        instance.set_cable_end(input);
        Integer expResult = 3;
        Object result = instance.get_cable_end();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_cable_clock() {
        System.out.println("set_cable_clock");
        L1TM_TT instance = new L1TM_TT();
        Integer input = 3;
        instance.set_cable_clock(input);
        Integer expResult = 3;
        Object result = instance.get_cable_clock();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_cable_name() {
        System.out.println("set_cable_name");
        L1TM_TT instance = new L1TM_TT();
        String input = "a string";
        instance.set_cable_name(input);
        String expResult = "a string";
        Object result = instance.get_cable_name();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_cable_ctpin() {
        System.out.println("set_cable_ctpin");
        L1TM_TT instance = new L1TM_TT();
        String input = "a string";
        instance.set_cable_ctpin(input);
        String expResult = "a string";
        Object result = instance.get_cable_ctpin();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_menu_id() {
        System.out.println("set_menu_id");
        L1TM_TT instance = new L1TM_TT();
        Integer input = 3;
        instance.set_menu_id(input);
        Integer expResult = 3;
        Object result = instance.get_menu_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_threshold() {
        System.out.println("set_threshold");
        L1TM_TT instance = new L1TM_TT();
        L1Threshold input = new L1Threshold();
        input.set_id(100);
        instance.set_threshold(input);
        Object expResult = input;
        Object result = instance.get_threshold();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_threshold_id() {
        System.out.println("set_threshold_id");
        L1TM_TT instance = new L1TM_TT();
        Integer input = 3;
        instance.set_threshold_id(input);
        Integer expResult = 3;
        Object result = instance.get_threshold_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_cable_connector() {
        System.out.println("get_cable_connector");
        L1TM_TT instance = new L1TM_TT();
        String expResult = new String();
        Object result = instance.get_cable_connector();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_cable_start() {
        System.out.println("get_cable_start");
        L1TM_TT instance = new L1TM_TT();
        Integer expResult = 0;
        Object result = instance.get_cable_start();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_cable_end() {
        System.out.println("get_cable_end");
        L1TM_TT instance = new L1TM_TT();
        Integer expResult = 0;
        Object result = instance.get_cable_end();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_cable_clock() {
        System.out.println("get_cable_clock");
        L1TM_TT instance = new L1TM_TT();
        Integer expResult = 0;
        Object result = instance.get_cable_clock();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_cable_name() {
        System.out.println("get_cable_name");
        L1TM_TT instance = new L1TM_TT();
        String expResult = new String();
        Object result = instance.get_cable_name();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_cable_ctpin() {
        System.out.println("get_cable_ctpin");
        L1TM_TT instance = new L1TM_TT();
        String expResult = new String();
        Object result = instance.get_cable_ctpin();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_menu_id() {
        System.out.println("get_menu_id");
        L1TM_TT instance = new L1TM_TT();
        Integer expResult = 0;
        Object result = instance.get_menu_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_pits() throws Exception {
        System.out.println("get_pits");
        L1TM_TT instance = new L1TM_TT();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        ArrayList<L1Pits> expResult = new ArrayList<>();
        Object result = instance.get_pits();
        assertEquals(expResult, result);
    }

    @Test
    public void testSave() throws Exception {
        System.out.println("save");
        L1TM_TT instance = new L1TM_TT();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Integer expResult = 1;
        Object result = instance.save();
        assertEquals(expResult, result);
    }

    @Test
    public void testClone() {
    }

    @Test
    public void testEquals() {
        System.out.println("equals");
        
        L1TM_TT instance = new L1TM_TT();
        L1TM_TT param1 = new L1TM_TT();
        L1TM_TT param2 = new L1TM_TT();
        L1TM_TT nullparam = null;
        Boolean expTrueResult = true;
        Boolean expFalseResult = false;
        
        Object NullResult = instance.equals(nullparam);
        assertEquals(expFalseResult,NullResult);
        
        String IncorrectType = "";
        Object Result1 = param1.equals(IncorrectType);
        assertEquals(Result1,expFalseResult);
                
        instance.set_cable_name("right name"); 
        param1.set_cable_name("right name");
        param2.set_cable_name("wrong name");
        Object TrueResult = param1.equals(instance);
        Object FalseResult = param2.equals(instance);
        assertEquals(TrueResult,expTrueResult);
        assertEquals(FalseResult,expFalseResult);
    }

    @Test
    public void testDoDiff() throws Exception {
    }

    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        L1TM_TT instance = new L1TM_TT();
        Integer expResult = 7;
        Object result = instance.hashCode();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetTableName() {
        System.out.println("getTableName");
        L1TM_TT instance = new L1TM_TT();
        String expResult = "L1_TM_TO_TT";
        String result = instance.getTableName();
        assertEquals(expResult, result);
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        L1TM_TT instance = new L1TM_TT();
        String expResult = "[ L1 Threshold   0 0 0]";
        Object result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
