/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.L1Links;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import com.mockrunner.mock.jdbc.MockResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.Entities.L1.L1Threshold;

/**
 *
 * @author davethomas
 */
public class L1TM_TTMTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public L1TM_TTMTest() {
    }
    
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @After
    public void tearDown() {
    }
    
    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
    }

    @Test
    public void testGet_threshold() throws Exception {
        System.out.println("get_threshold");
        L1TM_TTM instance = new L1TM_TTM(2712);
        L1Threshold param = new L1Threshold();
        param.set_id(100);
        Object expResult = param;
        instance.set_threshold(param);
        Object Result = instance.get_threshold();
        assertEquals(expResult,Result);
        
        //This test fails as expected, but changing the expResult to the correct result causes a null pointer exception, while the test below simply fails
        //L1TM_TTM instance2 = new L1TM_TTM(2712);
        //L1Threshold param2 = null;
        //Object expResult2 = param2;
        //instance2.set_threshold(param2);
        //Object Result2 = instance2.get_threshold();
        //assertEquals(expResult2,Result2);
    }

    @Test
    public void testGet_threshold_id() {
        System.out.println("get_threshold_id");
        L1TM_TTM instance = new L1TM_TTM(2712);
        Integer expResult = 0;
        Object result = instance.get_threshold_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_counter_name() {
        System.out.println("get_counter_name");
        L1TM_TTM instance = new L1TM_TTM(2712);
        String expResult = new String();
        Object result = instance.get_counter_name();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_bunch_group_id() {
        System.out.println("get_bunch_group_id");
        L1TM_TTM instance = new L1TM_TTM(2712);
        Integer expResult = 0;
        Object result = instance.get_threshold_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_multiplicity() {
        System.out.println("get_multiplicity");
        L1TM_TTM instance = new L1TM_TTM(2712);
        Integer expResult = 0;
        Object result = instance.get_multiplicity();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_internal_id() {
        System.out.println("get_internal_id");
        L1TM_TTM instance = new L1TM_TTM(2712);
        Integer expResult = 0;
        Object result = instance.get_internal_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_type() {
        System.out.println("get_type");
        L1TM_TTM instance = new L1TM_TTM(2712);
        String expResult = new String();
        Object result = instance.get_type();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_counter_name() {
        System.out.println("set_counter_name");
        L1TM_TTM instance = new L1TM_TTM(2712);
        String input = "a string";
        instance.set_counter_name(input);
        String expResult = "a string";
        Object result = instance.get_counter_name();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_bunch_group_id() {
        System.out.println("set_bunch_group_id");
        L1TM_TTM instance = new L1TM_TTM(2712);
        Integer input = 3;
        instance.set_bunch_group_id(input);
        Integer expResult = 3;
        Object result = instance.get_bunch_group_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_internal_id() {
        System.out.println("set_internal_id");
        L1TM_TTM instance = new L1TM_TTM(2712);
        Integer input = 3;
        instance.set_internal_id(input);
        Integer expResult = 3;
        Object result = instance.get_internal_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_multiplicity() {
        System.out.println("set_multiplicity");
        L1TM_TTM instance = new L1TM_TTM(2712);
        Integer input = 3;
        instance.set_multiplicity(input);
        Integer expResult = 3;
        Object result = instance.get_multiplicity();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_type() {
        System.out.println("set_type");
        L1TM_TTM instance = new L1TM_TTM(2712);
        String input = "a string";
        instance.set_type(input);
        String expResult = "a string";
        Object result = instance.get_type();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_menu_id() {
        System.out.println("set_menu_id");
        L1TM_TTM instance = new L1TM_TTM(2712);
        Integer input = 3;
        instance.set_menu_id(input);
        Integer expResult = 3;
        Object result = instance.get_menu_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_menu_id() {
        System.out.println("get_menu_id");
        L1TM_TTM instance = new L1TM_TTM(2712);
        Integer expResult = 0;
        Object result = instance.get_menu_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_threshold_id() {
        System.out.println("set_threshold_id");
        L1TM_TTM instance = new L1TM_TTM(2712);
        Integer input = 3;
        instance.set_threshold_id(input);
        Integer expResult = 3;
        Object result = instance.get_threshold_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testSave() throws Exception {
        System.out.println("save");
        L1TM_TTM instance = new L1TM_TTM(2712);
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Integer expResult = 2712;
        Object result = instance.save();
        assertEquals(expResult, result);
    }

    @Test
    public void testClone() {

    }

    @Test
    public void testDoDiff() throws Exception {
    }

    @Test
    public void testEquals() {
        System.out.println("equals");
        
        L1TM_TTM instance = new L1TM_TTM(2712);
        L1TM_TTM param1 = new L1TM_TTM(2712);
        L1TM_TTM param2 = new L1TM_TTM(2712);
        L1TM_TTM nullparam = null;
        Boolean expTrueResult = true;
        Boolean expFalseResult = false;
        
        Object NullResult = instance.equals(nullparam);
        assertEquals(expFalseResult,NullResult);
        
        String IncorrectType = "";
        Object Result1 = param1.equals(IncorrectType);
        assertEquals(Result1,expFalseResult);
                
        instance.set_counter_name("right name"); 
        param1.set_counter_name("right name");
        param2.set_counter_name("wrong name");
        Object TrueResult = param1.equals(instance);
        Object FalseResult = param2.equals(instance);
        assertEquals(TrueResult,expTrueResult);
        assertEquals(FalseResult,expFalseResult);
    }

    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        L1TM_TTM instance = new L1TM_TTM(2712);
        Integer expResult = 7;
        Object result = instance.hashCode();
        assertEquals(expResult, result);
    }

    @Test
    public void testReplace_TM2TTM_links() throws Exception {
    }

    @Test
    public void testGetTableName() {
        System.out.println("getTableName");
        L1TM_TTM instance = new L1TM_TTM(2712);
        String expResult = "L1_TM_TO_TT_MON";
        String result = instance.getTableName();
        assertEquals(expResult, result);
    }
    
}
