/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.Topo;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import com.mockrunner.mock.jdbc.MockResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.Entities.HLT.HLTMaster;
import triggerdb.Entities.HLT.HLTRelease;
import triggerdb.Entities.L1.L1Master;
import triggerdb.Entities.Topo.TopoMaster;
import triggerdb.PrescaleSetAliasComponent;

/**
 *
 * @author davethomas
 */
public class TopoMenuTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public TopoMenuTest() {
    }
    
@Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
         // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @Override
    @After
    public void tearDown() throws Exception {
        super.setUp();
        ConnectionManager.getInstance().unsetTestingConnection();
    }
    
    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
    }

    @Test
    public void testForceLoad() throws Exception {
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        TopoMenu instance = new TopoMenu();
        String expResult= "Topo Menu";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
                
        TopoMenu instance2 = new TopoMenu();
        instance2.set_id(2712);
        String expResult2 = "TOPO MENU: ID=2712, Name=, Version=1";
        Object Result2 = instance2.toString();
        assertEquals(expResult2, Result2);
    }

    @Test
    public void testGet_ctplink() {
    }

    @Test
    public void testSet_ctplink() {
        System.out.println("set_ctplink");
        TopoMenu instance = new TopoMenu();
        Integer input = 3;
        instance.set_ctplink(input);
        Integer expResult = 3;
        Object result = instance.get_ctplink();
        assertEquals(expResult, result);
    }

    @Test
    public void testAdd_config_TopoConfig() throws SQLException {
        System.out.println("add_config_TopoConfig");
        TopoMenu instance = new TopoMenu();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        TopoConfig input = new TopoConfig();
        instance.add_config(input);
        ArrayList<TopoConfig> expResult = new ArrayList<>();
        expResult.add(input);
        Object result = instance.get_configs();
        assertEquals(expResult, result);
    }

    @Test
    public void testAddToTree() throws Exception {
    }

    @Test
    public void testAdd_config_ArrayList() throws SQLException {
        System.out.println("add_config_ArrayList");
        TopoMenu instance = new TopoMenu();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        TopoConfig param = new TopoConfig();
        ArrayList<TopoConfig> input = new ArrayList<>();
        input.add(param);
        instance.add_config(input);
        ArrayList<TopoConfig> expResult = new ArrayList<>();
        expResult.add(param);
        Object result = instance.get_configs();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_configs() throws Exception {
        System.out.println("get_configs");
        TopoMenu instance = new TopoMenu();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        ArrayList<TopoConfig> expResult = new ArrayList<>();
        Object result = instance.get_configs();
        assertEquals(expResult, result);
    }

    @Test
    public void testAdd_outputlist() throws SQLException {
       System.out.println("add_outputlist");
        TopoMenu instance = new TopoMenu();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        TopoOutputList input = new TopoOutputList();
        instance.add_outputlist(input);
        TopoOutputList expResult = new TopoOutputList();
        Object result = instance.get_outputlist();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_outputlist() throws Exception {
        System.out.println("get_outputlist");
        TopoMenu instance = new TopoMenu();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        ArrayList<TopoOutputList> expResult = null;
        Object result = instance.get_outputlist();
        assertEquals(expResult, result);
    }

    @Test
    public void testAdd_algo_TopoAlgo() throws SQLException {
        System.out.println("add_algo_Topo");
        TopoMenu instance = new TopoMenu();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        TopoAlgo input = new TopoAlgo();
        instance.add_algo(input);
        ArrayList<TopoAlgo> expResult = new ArrayList<>();
        expResult.add(input);
        Object result = instance.get_algos();
        assertEquals(expResult, result);
    }

    @Test
    public void testAdd_algo_ArrayList() throws SQLException {
        System.out.println("add_algo_ArrayList");
        TopoMenu instance = new TopoMenu();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        TopoAlgo param = new TopoAlgo();
        ArrayList<TopoAlgo> input = new ArrayList<>();
        input.add(param);
        instance.add_algo(input);
        ArrayList<TopoAlgo> expResult = new ArrayList<>();
        expResult.add(param);
        Object result = instance.get_algos();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_algos() throws Exception {
        System.out.println("get_algos");
        TopoMenu instance = new TopoMenu();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        ArrayList<TopoAlgo> expResult = new ArrayList<>();
        Object result = instance.get_algos();
        assertEquals(expResult, result);
    }

    @Test
    public void testDoDiff() throws Exception {
    }

    @Test
    public void testSave() throws Exception {
        System.out.println("save");
        TopoMenu instance = new TopoMenu();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Integer expResult = 1;
        Object result = instance.save();
        assertEquals(expResult, result);
    }

    @Test
    public void testClone() {
        System.out.println("clone");
        TopoMenu instance = new TopoMenu();
        Object expResult = instance;
        Object result = instance.clone();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetTableName() {
    }

    @Test
    public void testGet_hash() {
        System.out.println("get_hash");
        TopoMenu instance = new TopoMenu();
        Integer expResult = 13;
        Object result = instance.get_hash();
        assertEquals(expResult, result);
    }

    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        TopoMenu instance = new TopoMenu();
        Integer expResult = 13;
        Object result = instance.hashCode();
        assertEquals(expResult, result);
    }
    
}
