/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.Topo;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import com.mockrunner.mock.jdbc.MockResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.Entities.HLT.HLTMaster;
import triggerdb.Entities.HLT.HLTRelease;
import triggerdb.Entities.L1.L1Master;
import triggerdb.Entities.Topo.TopoMaster;
import triggerdb.PrescaleSetAliasComponent;

/**
 *
 * @author davethomas
 */
public class TopoGenericTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public TopoGenericTest() {
    }
    
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
        ConnectionManager.getInstance().unsetTestingConnection();
    }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
    }

    @Test
    public void testForceLoad() throws Exception {
    }

    @Test
    public void testGet_value() {
    }

    @Test
    public void testSet_value() {
        System.out.println("set_value");
        TopoGeneric instance = new TopoGeneric();
        String input = "a string";
        instance.set_value(input);
        String expResult = "a string";
        Object result = instance.get_value();
        assertEquals(expResult, result);
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        TopoGeneric instance = new TopoGeneric();
        String expResult= "TOPO Generic: ID=-1, Name=, Value=";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
    }

    @Test
    public void testSave() throws Exception {
        System.out.println("save");
        TopoGeneric instance = new TopoGeneric();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Integer expResult = 1;
        Object result = instance.save();
        assertEquals(expResult, result);
    }

    @Test
    public void testCheckExisting() {
        System.out.println("checkExisting");
        TopoGeneric instance = new TopoGeneric();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Integer expResult = -1;
        TopoGeneric param = new TopoGeneric();
        ArrayList<TopoGeneric> input = new ArrayList<>();
        input.add(param);
        Object result = instance.checkExisting(input);
        assertEquals(expResult, result);
    }

    @Test
    public void testBatchsave() throws Exception {
        System.out.println("batchsave");
        TopoGeneric instance = new TopoGeneric();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        TopoGeneric param = new TopoGeneric();
        ArrayList<TopoGeneric> input = new ArrayList<>();
        input.add(param);
        Object result = instance.batchsave(input);
        TreeMap<Integer, Integer> expResult = new TreeMap<>();
        expResult.put(0,0);
        assertEquals(expResult, result);
    }

    @Test
    public void testEquals() {          
        System.out.println("equals");
        
        TopoGeneric instance = new TopoGeneric();
        TopoGeneric param1 = new TopoGeneric();
        TopoGeneric param2 = new TopoGeneric();
        Boolean expTrueResult = true;
        Boolean expFalseResult = false;
            
        String IncorrectType = "";
        Object Result1 = param1.equals(IncorrectType);
        assertEquals(Result1,expFalseResult);
        assertNotEquals(Result1,expTrueResult);
       
        instance.set_name("correct");
        param1.set_name("correct");
        param2.set_name("wrong");
        Object Result = param1.equals(instance);
        Object Result2 = param2.equals(instance);
        assertEquals(Result,expTrueResult);
        assertEquals(Result2,expFalseResult);
        
        instance.set_name("");
        param1.set_name("");
        param2.set_name("");
        param1.set_value("correct");
        param2.set_value("wrong");
        instance.set_value("correct");
        Object ResultA = param1.equals(instance);
        Object ResultB = param2.equals(instance);
        assertEquals(ResultA,expTrueResult);
        assertEquals(ResultB,expFalseResult);
    }

    @Test
    public void testClone() {
        System.out.println("clone");
        TopoGeneric instance = new TopoGeneric();
        Object expResult = instance;
        Object result = instance.clone();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetTableName() {
    }

    @Test
    public void testGet_min_names() {
        System.out.println("get_min_names");
        TopoGeneric instance = new TopoGeneric();
        
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("ID");
        expResult.add("Name");
        expResult.add("Value");
                
        Object Result = instance.get_min_names();
        assertEquals(expResult, Result);
    }

    @Test
    public void testGet_min_info() {
        System.out.println("get_min_info");
        TopoGeneric instance = new TopoGeneric();
        
        ArrayList<Object> expResult = new ArrayList<>();
        expResult.add(-1);
        expResult.add("");
        expResult.add("");
                
        Object Result = instance.get_min_info();
        assertEquals(expResult, Result);
    }
    
}
