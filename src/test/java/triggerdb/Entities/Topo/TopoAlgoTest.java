/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.Topo;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import com.mockrunner.mock.jdbc.MockResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.Entities.HLT.HLTMaster;
import triggerdb.Entities.HLT.HLTRelease;
import triggerdb.Entities.L1.L1Master;
import triggerdb.Entities.Topo.TopoMaster;
import triggerdb.PrescaleSetAliasComponent;

/**
 *
 * @author davethomas
 */
public class TopoAlgoTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public TopoAlgoTest() {
    }
    
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @After
    public void tearDown() {
    }
    
    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
    }

    @Test
    public void testGet_output() {
        System.out.println("get_output");
        TopoAlgo instance = new TopoAlgo();
        String expResult = "";
        Object result = instance.get_output();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_output() {
        System.out.println("set_output");
        TopoAlgo instance = new TopoAlgo();
        String input = "";
        instance.set_output(input);
        String expResult = "";
        Object result = instance.get_output();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_type() {
        System.out.println("get_type");
        TopoAlgo instance = new TopoAlgo();
        String expResult = "";
        Object result = instance.get_type();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_type() {
        System.out.println("set_type");
        TopoAlgo instance = new TopoAlgo();
        String input = "";
        instance.set_type(input);
        String expResult = "";
        Object result = instance.get_type();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_bits() {
        System.out.println("get_bits");
        TopoAlgo instance = new TopoAlgo();
        Integer expResult = -1;
        Object result = instance.get_bits();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_bits() {
        System.out.println("set_bits");
        TopoAlgo instance = new TopoAlgo();
        Integer input = 3;
        instance.set_bits(input);
        Integer expResult = 3;
        Object result = instance.get_bits();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_sort_deci() {
        System.out.println("get_sort_deci");
        TopoAlgo instance = new TopoAlgo();
        String expResult = "";
        Object result = instance.get_sort_deci();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_sort_deci() {
        System.out.println("set_sort_deci");
        TopoAlgo instance = new TopoAlgo();
        String input = "";
        instance.set_sort_deci(input);
        String expResult = "";
        Object result = instance.get_sort_deci();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_algo_id() {
        System.out.println("get_algo_id");
        TopoAlgo instance = new TopoAlgo();
        Integer expResult = -1;
        Object result = instance.get_algo_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_algo_id() {
        System.out.println("set_algo_id");
        TopoAlgo instance = new TopoAlgo();
        Integer input = 3;
        instance.set_algo_id(input);
        Integer expResult = 3;
        Object result = instance.get_algo_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testAdd_input_TopoAlgoInput() throws SQLException {
        System.out.println("add_input_TopoAlgoInput");
        TopoAlgo instance = new TopoAlgo();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        TopoAlgoInput input = new TopoAlgoInput();
        instance.add_input(input);
        ArrayList<TopoAlgoInput> expResult = new ArrayList<>();
        expResult.add(input);
        Object result = instance.get_inputs();
        assertEquals(expResult, result);
        
    }

    @Test
    public void testAdd_input_ArrayList() throws SQLException {
        System.out.println("add_input_ArrayList");
        TopoAlgo instance = new TopoAlgo();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        ArrayList<TopoAlgoInput> input = new ArrayList<>();
        instance.add_input(input);
        ArrayList<TopoAlgoInput> expResult = new ArrayList<>();
        Object result = instance.get_inputs();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_inputs() throws Exception {
        System.out.println("get_inputs");
        TopoAlgo instance = new TopoAlgo();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        ArrayList<TopoAlgoInput> expResult = new ArrayList<>();
        Object result = instance.get_inputs();
        assertEquals(expResult, result);
    }

    @Test
    public void testAdd_output_TopoAlgoOutput() throws SQLException {
        System.out.println("add_input_TopoAlgoOutput");
        TopoAlgo instance = new TopoAlgo();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        TopoAlgoOutput output = new TopoAlgoOutput();
        instance.add_output(output);
        ArrayList<TopoAlgoOutput> expResult = new ArrayList<>();
        expResult.add(output);
        Object result = instance.get_outputs();
        assertEquals(expResult, result);
    }

    @Test
    public void testAdd_output_ArrayList() throws SQLException {
        System.out.println("add_output_ArrayList");
        TopoAlgo instance = new TopoAlgo();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        ArrayList<TopoAlgoOutput> output = new ArrayList<>();
        instance.add_output(output);
        ArrayList<TopoAlgoOutput> expResult = new ArrayList<>();
        Object result = instance.get_outputs();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_outputs() throws Exception {
        System.out.println("get_outputs");
        TopoAlgo instance = new TopoAlgo();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        ArrayList<TopoAlgoOutput> expResult = new ArrayList<>();
        Object result = instance.get_outputs();
        assertEquals(expResult, result);
    }

    @Test
    public void testAdd_parameter_TopoParameter() throws SQLException {
        System.out.println("add_parameter_TopoParameter");
        TopoAlgo instance = new TopoAlgo();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        TopoParameter parameter = new TopoParameter();
        instance.add_parameter(parameter);
        ArrayList<TopoParameter> expResult = new ArrayList<>();
        expResult.add(parameter);
        Object result = instance.get_parameters();
        assertEquals(expResult, result);
    }

    @Test
    public void testAdd_parameter_ArrayList() throws SQLException {
        System.out.println("add_parameter_ArrayList");
        TopoAlgo instance = new TopoAlgo();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        ArrayList<TopoParameter> parameter = new ArrayList<>();
        instance.add_parameter(parameter);
        ArrayList<TopoParameter> expResult = new ArrayList<>();
        Object result = instance.get_parameters();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_parameters() throws Exception {
        //implicitly tested within Add_parameter
    }

    @Test
    public void testAdd_generic_TopoGeneric() throws SQLException {
        System.out.println("add_generic_TopoGeneric");
        TopoAlgo instance = new TopoAlgo();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        TopoGeneric generic = new TopoGeneric();
        instance.add_generic(generic);
        ArrayList<TopoGeneric> expResult = new ArrayList<>();
        expResult.add(generic);
        Object result = instance.get_generics();
        assertEquals(expResult, result);
    }

    @Test
    public void testAdd_generic_ArrayList() throws SQLException {
        System.out.println("add_generic_ArrayList");
        TopoAlgo instance = new TopoAlgo();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        ArrayList<TopoGeneric> generic = new ArrayList<>();
        instance.add_generic(generic);
        ArrayList<TopoGeneric> expResult = new ArrayList<>();
        Object result = instance.get_generics();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_generics() throws Exception {
        //implicitly tested within Add_generics
    }

    @Test
    public void testForceLoad() throws Exception {
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        TopoAlgo instance = new TopoAlgo();
        String expResult= "TOPO ALGORITHM: ID=-1, Name=, Type=, Output=, Sort/Deci=, AlgoId=-1";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
    }

    @Test
    public void testSave() throws Exception {
        System.out.println("save");
        TopoAlgo instance = new TopoAlgo();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Integer expResult = 1;
        Object result = instance.save();
        assertEquals(expResult, result);
    }

    @Test
    public void testBatchsave() throws Exception {
        System.out.println("batchsave");
        TopoAlgo instance = new TopoAlgo();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        TopoAlgo param = new TopoAlgo();
        ArrayList<TopoAlgo> input = new ArrayList<>();
        input.add(param);
        Object result = instance.batchsave(input);
        TreeMap<Integer, Integer> expResult = new TreeMap<>();
        expResult.put(0,0);
        assertEquals(expResult, result);
    }

    @Test
    public void testEquals() {
        System.out.println("equals");
        
        TopoAlgo instance = new TopoAlgo();
        TopoAlgo param1 = new TopoAlgo();
        TopoAlgo param2 = new TopoAlgo();
        Boolean expTrueResult = true;
        Boolean expFalseResult = false;
            
        String IncorrectType = "";
        Object Result1 = param1.equals(IncorrectType);
        assertEquals(Result1,expFalseResult);
        assertNotEquals(Result1,expTrueResult);
       
        instance.set_name("correct");
        param1.set_name("correct");
        param2.set_name("wrong");
        Object Result = param1.equals(instance);
        Object Result2 = param2.equals(instance);
        assertEquals(Result,expTrueResult);
        assertEquals(Result2,expFalseResult);
        
        instance.set_name("");
        param1.set_name("");
        param2.set_name("");
        param1.set_output("correct");
        param2.set_output("wrong");
        instance.set_output("correct");
        Object ResultA = param1.equals(instance);
        Object ResultB = param2.equals(instance);
        assertEquals(ResultA,expTrueResult);
        assertEquals(ResultB,expFalseResult);
       
        instance.set_output("");
        param1.set_output("");
        param2.set_output("");
        instance.set_type("goodName");
        param1.set_type("goodName");
        param2.set_type("badName");
        Object Result3 = param1.equals(instance);
        Object Result4 = param2.equals(instance);
        assertEquals(Result3,expTrueResult);
        assertEquals(Result4,expFalseResult);
        
        instance.set_type("");
        param1.set_type("");
        param2.set_type("");
        instance.set_bits(-2);
        param1.set_bits(-2);
        param2.set_bits(-3);
        Object Result5 = param1.equals(instance);
        Object Result6 = param2.equals(instance);
        assertEquals(Result5,expTrueResult);
        assertEquals(Result6,expFalseResult);
        
        instance.set_bits(-1);
        param1.set_bits(-1);
        param2.set_bits(-1);
        instance.set_sort_deci("right");
        param1.set_sort_deci("right");
        param2.set_sort_deci("wrong");
        Object Result7 = param1.equals(instance);
        Object Result8 = param2.equals(instance);
        assertEquals(Result7,expTrueResult);
        assertEquals(Result8,expFalseResult);
        

        instance.set_sort_deci("");
        param1.set_sort_deci("");
        param2.set_sort_deci("");
        instance.set_algo_id(-2);
        param1.set_algo_id(-2);
        param2.set_algo_id(-3);
        Object Result9 = param1.equals(instance);
        Object Result10 = param2.equals(instance);
        assertEquals(Result9,expTrueResult);
        assertEquals(Result10,expFalseResult);
    }

    @Test
    public void testClone() {
        System.out.println("clone");
        TopoAlgo instance = new TopoAlgo();
        Object expResult = instance;
        Object result = instance.clone();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetTableName() {
    }
    
    
    @Test
    public void testGet_min_info(){
        System.out.println("get_min_info");
        TopoAlgo instance = new TopoAlgo();
        
        ArrayList<Object> expResult = new ArrayList<>();
        expResult.add(-1);
        expResult.add("");
        expResult.add("");
        expResult.add("");
        expResult.add("");
        expResult.add(-1);
                
        Object Result = instance.get_min_info();
        assertEquals(expResult, Result);
    }
    
    @Test
    public void testGet_min_names(){
        System.out.println("get_min_names");
        TopoAlgo instance = new TopoAlgo();
        
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("ID");
        expResult.add("Name");
        expResult.add("Type");
        expResult.add("Output");
        expResult.add("Sort/Deci");
        expResult.add("AlgoId");
                
        Object Result = instance.get_min_names();
        assertEquals(expResult, Result);
    }
}
