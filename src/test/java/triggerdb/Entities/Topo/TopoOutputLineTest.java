/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.Topo;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import com.mockrunner.mock.jdbc.MockResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.Entities.HLT.HLTMaster;
import triggerdb.Entities.HLT.HLTRelease;
import triggerdb.Entities.L1.L1Master;
import triggerdb.Entities.Topo.TopoMaster;
import triggerdb.PrescaleSetAliasComponent;

/**
 *
 * @author davethomas
 */
public class TopoOutputLineTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public TopoOutputLineTest() {
    }
    
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
        ConnectionManager.getInstance().unsetTestingConnection();
    }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
    }

    @Test
    public void testForceLoad() throws Exception {
    }

    @Test
    public void testGet_algo_id() {
    }

    @Test
    public void testSet_algo_id() {
        System.out.println("set_algo_id");
        TopoOutputLine instance = new TopoOutputLine();
        Integer input = 3;
        instance.set_algo_id(input);
        Integer expResult = 3;
        Object result = instance.get_algo_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_algo_name() {
    }

    @Test
    public void testSet_algo_name() {
        System.out.println("set_algo_name");
        TopoOutputLine instance = new TopoOutputLine();
        String input = "a string";
        instance.set_algo_name(input);
        String expResult = "a string";
        Object result = instance.get_algo_name();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_triggerline() {
    }

    @Test
    public void testSet_triggerline() {
        System.out.println("set_triggerLine");
        TopoOutputLine instance = new TopoOutputLine();
        String input = "a string";
        instance.set_triggerline(input);
        String expResult = "a string";
        Object result = instance.get_triggerline();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_module() {
    }

    @Test
    public void testSet_module() {
        System.out.println("set_module");
        TopoOutputLine instance = new TopoOutputLine();
        Integer input = 3;
        instance.set_module(input);
        Integer expResult = 3;
        Object result = instance.get_module();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_fpga() {
    }

    @Test
    public void testSet_fpga() {
        System.out.println("set_fpga");
        TopoOutputLine instance = new TopoOutputLine();
        Integer input = 3;
        instance.set_fpga(input);
        Integer expResult = 3;
        Object result = instance.get_fpga();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_first_bit() {
    }

    @Test
    public void testSet_first_bit() {
        System.out.println("set_first_bit");
        TopoOutputLine instance = new TopoOutputLine();
        Integer input = 3;
        instance.set_first_bit(input);
        Integer expResult = 3;
        Object result = instance.get_first_bit();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_clock() {
    }

    @Test
    public void testSet_clock() {
        System.out.println("set_clock");
        TopoOutputLine instance = new TopoOutputLine();
        Integer input = 3;
        instance.set_clock(input);
        Integer expResult = 3;
        Object result = instance.get_clock();
        assertEquals(expResult, result);
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        TopoOutputLine instance = new TopoOutputLine();
        String expResult= "TOPO OUTPUT LINE: ID=-1, Name=, Triggerline=, Algo Id=-1, Module=-1, FPGA=-1, First Bit=-1, Clock=-1";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
    }

    @Test
    public void testSave() throws Exception {
        System.out.println("save");
        TopoOutputLine instance = new TopoOutputLine();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Integer expResult = 1;
        Object result = instance.save();
        assertEquals(expResult, result);
    }

    @Test
    public void testBatchsave() throws Exception {
        System.out.println("batchsave");
        TopoOutputLine instance = new TopoOutputLine();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        TopoOutputLine param = new TopoOutputLine();
        ArrayList<TopoOutputLine> input = new ArrayList<>();
        input.add(param);
        Object result = instance.batchsave(input);
        TreeMap<Integer, Integer> expResult = new TreeMap<>();
        expResult.put(0,0);
        assertEquals(expResult, result);
    }

    @Test
    public void testEquals() {
        System.out.println("equals");
        
        TopoOutputLine instance = new TopoOutputLine();
        TopoOutputLine param1 = new TopoOutputLine();
        TopoOutputLine param2 = new TopoOutputLine();
        Boolean expTrueResult = true;
        Boolean expFalseResult = false;
            
        String IncorrectType = "";
        Object Result1 = param1.equals(IncorrectType);
        assertEquals(Result1,expFalseResult);
        assertNotEquals(Result1,expTrueResult);
       
        instance.set_algo_name("correct");
        param1.set_algo_name("correct");
        param2.set_algo_name("wrong");
        Object Result = param1.equals(instance);
        Object Result2 = param2.equals(instance);
        assertEquals(Result,expTrueResult);
        assertEquals(Result2,expFalseResult);
        
        instance.set_algo_name("");
        param1.set_algo_name("");
        param2.set_algo_name("");
        param1.set_triggerline("correct");
        param2.set_triggerline("wrong");
        instance.set_triggerline("correct");
        Object ResultA = param1.equals(instance);
        Object ResultB = param2.equals(instance);
        assertEquals(ResultA,expTrueResult);
        assertEquals(ResultB,expFalseResult);
        
        instance.set_triggerline("");
        param1.set_triggerline("");
        param2.set_triggerline("");
        param1.set_algo_id(-2);
        param2.set_algo_id(-3);
        instance.set_algo_id(-2);
        Object Result3 = param1.equals(instance);
        Object Result4 = param2.equals(instance);
        assertEquals(Result3,expTrueResult);
        assertEquals(Result4,expFalseResult);
        
        instance.set_algo_id(-1);
        param1.set_algo_id(-1);
        param2.set_algo_id(-1);
        param1.set_module(-2);
        param2.set_module(-3);
        instance.set_module(-2);
        Object Result5 = param1.equals(instance);
        Object Result6 = param2.equals(instance);
        assertEquals(Result5,expTrueResult);
        assertEquals(Result6,expFalseResult);
        
        instance.set_module(-1);
        param1.set_module(-1);
        param2.set_module(-1);
        param1.set_fpga(-2);
        param2.set_fpga(-3);
        instance.set_fpga(-2);
        Object Result7 = param1.equals(instance);
        Object Result8 = param2.equals(instance);
        assertEquals(Result7,expTrueResult);
        assertEquals(Result8,expFalseResult);
        
        instance.set_fpga(-1);
        param1.set_fpga(-1);
        param2.set_fpga(-1);
        param1.set_first_bit(-2);
        param2.set_first_bit(-3);
        instance.set_first_bit(-2);
        Object Result9 = param1.equals(instance);
        Object Result10 = param2.equals(instance);
        assertEquals(Result9,expTrueResult);
        assertEquals(Result10,expFalseResult);
        
        instance.set_first_bit(-1);
        param1.set_first_bit(-1);
        param2.set_first_bit(-1);
        param1.set_clock(-2);
        param2.set_clock(-3);
        instance.set_clock(-2);
        Object Result11 = param1.equals(instance);
        Object Result12 = param2.equals(instance);
        assertEquals(Result11,expTrueResult);
        assertEquals(Result12,expFalseResult);
    }

    @Test
    public void testClone() {
        System.out.println("clone");
        TopoOutputLine instance = new TopoOutputLine();
        Object expResult = instance;
        Object result = instance.clone();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetTableName() {
    }

    @Test
    public void testGet_min_names() {
        System.out.println("get_min_names");
        TopoOutputLine instance = new TopoOutputLine();
        
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("ID");
        expResult.add("Algorithm Name");
        expResult.add("Triggerline");
        expResult.add("Algorithm ID");
        expResult.add("Module");
        expResult.add("FPGA");
        expResult.add("First Bit");
        expResult.add("Clock");
                
        Object Result = instance.get_min_names();
        assertEquals(expResult, Result);
    }

    @Test
    public void testGet_min_info() {
        System.out.println("get_min_info");
        TopoOutputLine instance = new TopoOutputLine();
        
        ArrayList<Object> expResult = new ArrayList<>();
        expResult.add(-1);
        expResult.add("");
        expResult.add("");
        expResult.add(-1);
        expResult.add(-1);
        expResult.add(-1);
        expResult.add(-1);
        expResult.add(-1);
                
        Object Result = instance.get_min_info();
        assertEquals(expResult, Result);
    }
    
}
