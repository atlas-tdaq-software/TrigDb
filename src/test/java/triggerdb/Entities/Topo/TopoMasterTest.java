/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.Topo;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import com.mockrunner.mock.jdbc.MockResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.Entities.HLT.HLTMaster;
import triggerdb.Entities.HLT.HLTRelease;
import triggerdb.Entities.L1.L1Master;
import triggerdb.Entities.Topo.TopoMaster;
import triggerdb.PrescaleSetAliasComponent;

/**
 *
 * @author davethomas
 */
public class TopoMasterTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public TopoMasterTest() {
    }
    
    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
         // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @Override
    @After
    public void tearDown() throws Exception {
        super.setUp();
        ConnectionManager.getInstance().unsetTestingConnection();
    }
    
    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
    }

    @Test
    public void testForceLoad() throws Exception {
    }

    @Test
    public void testGet_menu_id() {
        
    }

    @Test
    public void testSet_menu_id() {
        System.out.println("set_menu_id");
        TopoMaster instance = new TopoMaster();
        Integer input = 3;
        instance.set_menu_id(input);
        Integer expResult = 3;
        Object result = instance.get_menu_id();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_menu() throws Exception {
        System.out.println("get_menu");
        TopoMaster instance = new TopoMaster();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        TopoMenu expResult = null;
        Object result = instance.get_menu();
        assertEquals(expResult, result);
    }

    @Test
    public void testDoDiff() throws Exception {
        
    }

    @Test
    public void testSave() throws Exception {
        System.out.println("save");
        TopoMaster instance = new TopoMaster();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Integer expResult = 1;
        Object result = instance.save();
        assertEquals(expResult, result);
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        TopoMaster instance = new TopoMaster();
        String expResult= "Topo Master Table";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
                
        TopoMaster instance2 = new TopoMaster();
        instance2.set_id(2712);
        String expResult2 = "TOPO MASTER: ID=2712, Name=, Version=1";
        Object Result2 = instance2.toString();
        assertEquals(expResult2, Result2);
    }

    @Test
    public void testAddToTree() throws Exception {
    }

    @Test
    public void testClone() {
        System.out.println("clone");
        TopoMaster instance = new TopoMaster();
        Object expResult = instance;
        Object result = instance.clone();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_topo_menu() {
        System.out.println("set_topo_menu");
        TopoMaster instance = new TopoMaster();
        TopoMenu input = new TopoMenu();
        instance.set_topo_menu(input);
        Object expResult = new TopoMenu();
        Object result = instance.get_topo_menu();
        assertEquals(expResult, result);
    }

    @Test
    public void testSet_comment() {
        System.out.println("set_comment");
        TopoMaster instance = new TopoMaster();
        String input = "a string";
        instance.set_comment(input);
        String expResult = "a string";
        Object result = instance.get_comment();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_comment() {
    }

    @Test
    public void testGet_hash() {
    }

    @Test
    public void testSet_hash() {
        System.out.println("set_hash");
        TopoMaster instance = new TopoMaster();
        Integer input = 3;
        instance.set_hash(input);
        Integer expResult = 3;
        Object result = instance.get_hash();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetTopoTables() throws Exception {
        System.out.println("getTopoTables");
        TopoMaster instance = new TopoMaster();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        List<TopoMaster> expResult = new ArrayList<>();
        Object result = instance.getTopoTables();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetTableName() {
    }

    @Test
    public void testFindDummy() {
        System.out.println("findDummy");
        TopoMaster instance = new TopoMaster();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Integer expResult = -1;
        Object result = instance.findDummy();
        assertEquals(expResult, result);
    }
    
}
