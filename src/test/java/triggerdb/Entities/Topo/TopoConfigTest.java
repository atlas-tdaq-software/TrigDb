/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.Topo;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import com.mockrunner.mock.jdbc.MockResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.Entities.HLT.HLTMaster;
import triggerdb.Entities.HLT.HLTRelease;
import triggerdb.Entities.L1.L1Master;
import triggerdb.Entities.Topo.TopoMaster;
import triggerdb.PrescaleSetAliasComponent;

/**
 *
 * @author davethomas
 */
public class TopoConfigTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public TopoConfigTest() {
    }
    
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @After
    public void tearDown() throws Exception {
        super.setUp();
        ConnectionManager.getInstance().unsetTestingConnection();
    }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
    }

    @Test
    public void testForceLoad() throws Exception {
    }

    @Test
    public void testGet_value() {
    }

    @Test
    public void testSet_value() {
        System.out.println("set_value");
        TopoConfig instance = new TopoConfig();
        Integer input = 3;
        instance.set_value(input);
        Integer expResult = 3;
        Object result = instance.get_value();
        assertEquals(expResult, result);
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        TopoConfig instance = new TopoConfig();
        String expResult= "TOPO Config: ID=-1, Name=, Value=-1";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
    }

    @Test
    public void testSave() throws Exception {
        System.out.println("save");
        TopoConfig instance = new TopoConfig();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Integer expResult = 1;
        Object result = instance.save();
        assertEquals(expResult, result);
    }

    @Test
    public void testBatchsave() throws Exception {
        System.out.println("batchsave");
        TopoConfig instance = new TopoConfig();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        TopoConfig param = new TopoConfig();
        ArrayList<TopoConfig> input = new ArrayList<>();
        input.add(param);
        Object result = instance.batchsave(input);
        TreeMap<Integer, Integer> expResult = new TreeMap<>();
        expResult.put(0,0);
        assertEquals(expResult, result);
    }

    @Test
    public void testEquals() {
        System.out.println("equals");
        
        TopoConfig instance = new TopoConfig();
        TopoConfig param1 = new TopoConfig();
        TopoConfig param2 = new TopoConfig();
        Boolean expTrueResult = true;
        Boolean expFalseResult = false;
            
        String IncorrectType = "";
        Object Result1 = param1.equals(IncorrectType);
        assertEquals(Result1,expFalseResult);
        assertNotEquals(Result1,expTrueResult);
       
        instance.set_name("correct");
        param1.set_name("correct");
        param2.set_name("wrong");
        Object Result = param1.equals(instance);
        Object Result2 = param2.equals(instance);
        assertEquals(Result,expTrueResult);
        assertEquals(Result2,expFalseResult);
        
        instance.set_name("");
        param1.set_name("");
        param2.set_name("");
        param1.set_value(-2);
        param2.set_value(-3);
        instance.set_value(-2);
        Object ResultA = param1.equals(instance);
        Object ResultB = param2.equals(instance);
        assertEquals(ResultA,expTrueResult);
        assertEquals(ResultB,expFalseResult);
    }

    @Test
    public void testClone() {
        System.out.println("clone");
        TopoConfig instance = new TopoConfig();
        Object expResult = instance;
        Object result = instance.clone();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetTableName() {
    }

    @Test
    public void testGet_min_names() {
        System.out.println("get_min_names");
        TopoConfig instance = new TopoConfig();
        
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("ID");
        expResult.add("Name");
        expResult.add("Value");
                
        Object Result = instance.get_min_names();
        assertEquals(expResult, Result);
    }

    @Test
    public void testGet_min_info() {
        System.out.println("get_min_info");
        TopoConfig instance = new TopoConfig();
        
        ArrayList<Object> expResult = new ArrayList<>();
        expResult.add(-1);
        expResult.add("");
        expResult.add(-1);
                
        Object Result = instance.get_min_info();
        assertEquals(expResult, Result);
    }
    
}
