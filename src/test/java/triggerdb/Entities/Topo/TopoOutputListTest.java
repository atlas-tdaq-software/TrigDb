/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.Topo;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import com.mockrunner.mock.jdbc.MockResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.Entities.HLT.HLTMaster;
import triggerdb.Entities.HLT.HLTRelease;
import triggerdb.Entities.L1.L1Master;
import triggerdb.Entities.Topo.TopoMaster;
import triggerdb.PrescaleSetAliasComponent;

/**
 *
 * @author davethomas
 */
public class TopoOutputListTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public TopoOutputListTest() {
    }
    
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
        ConnectionManager.getInstance().unsetTestingConnection();
    }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
    }

    @Test
    public void testForceLoad() throws Exception {
    }

    @Test
    public void testGet_output_lines() throws Exception {
        System.out.println("get_output_lines");
        TopoOutputList instance = new TopoOutputList();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        ArrayList<TopoOutputLine> expResult = new ArrayList<>();
        Object result = instance.get_output_lines();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_DB_output_lines() throws Exception {
        System.out.println("get_DB_output_lines");
        TopoOutputList instance = new TopoOutputList();
        instance.set_id(2712);
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        ArrayList<TopoOutputLine> expResult = new ArrayList<>();
        Object result = instance.get_DB_output_lines();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_DB_output_lines_from_master() throws Exception {
        System.out.println("get_DB_output_lines_from_master");
        TopoOutputList instance = new TopoOutputList();
        instance.set_id(2712);
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        ArrayList<TopoOutputLine> expResult = new ArrayList<>();
        Object result = instance.get_DB_output_lines_from_master(2712);
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_DB_output_list_from_master() throws Exception {
        System.out.println("get_DB_output_list_from_master");
        TopoOutputList instance = new TopoOutputList();
        instance.set_id(2712);
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Integer expResult = -1;
        Object result = instance.get_DB_output_list_from_master(2712);
        assertEquals(expResult, result);
    }

    @Test
    public void testAddToTree() throws Exception {
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        TopoOutputList instance = new TopoOutputList();
        String expResult= "Topo To L1CTP";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
                
        TopoOutputList instance2 = new TopoOutputList();
        instance2.set_id(2712);
        String expResult2 = "TOPO 2 L1CTP: ID=2712, Name=";
        Object Result2 = instance2.toString();
        assertEquals(expResult2, Result2);
    }

    @Test
    public void testSave() throws Exception {
        System.out.println("save");
        TopoOutputList instance = new TopoOutputList();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Integer expResult = 1;
        Object result = instance.save();
        assertEquals(expResult, result);
    }

    @Test
    public void testClone() {
        System.out.println("clone");
        TopoOutputList instance = new TopoOutputList();
        Object expResult = instance;
        Object result = instance.clone();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetTableName() {
    }

    @Test
    public void testGet_min_names() {
        System.out.println("get_min_names");
        TopoOutputList instance = new TopoOutputList();
        
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("ID");
        expResult.add("Name");
                
        Object Result = instance.get_min_names();
        assertEquals(expResult, Result);
    }

    @Test
    public void testGet_min_info() {
        System.out.println("get_min_info");
        TopoOutputList instance = new TopoOutputList();
        
        ArrayList<Object> expResult = new ArrayList<>();
        expResult.add(-1);
        expResult.add("");
        
        Object Result = instance.get_min_info();
        assertEquals(expResult, Result);
    }
    
}
