/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.Topo;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import com.mockrunner.mock.jdbc.MockResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.Entities.HLT.HLTMaster;
import triggerdb.Entities.HLT.HLTRelease;
import triggerdb.Entities.L1.L1Master;
import triggerdb.Entities.Topo.TopoMaster;
import triggerdb.PrescaleSetAliasComponent;

/**
 *c
 * @author davethomas
 */
public class TopoAlgoOutputTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public TopoAlgoOutputTest() throws Exception {
        super.setUp();
        ConnectionManager.getInstance().unsetTestingConnection();
    }
    
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @After
    public void tearDown() {
    }
    
    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
    }

    @Test
    public void testForceLoad() throws Exception {
    }

    @Test
    public void testGet_value() {
    }

    @Test
    public void testSet_value() {
        System.out.println("set_value");
        TopoAlgoOutput instance = new TopoAlgoOutput();
        String input = "a string";
        instance.set_value(input);
        String expResult = "a string";
        Object result = instance.get_value();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_selection() {
    }

    @Test
    public void testSet_selection() {
        System.out.println("set_selection");
        TopoAlgoOutput instance = new TopoAlgoOutput();
        Integer input = 3;
        instance.set_selection(input);
        Integer expResult = 3;
        Object result = instance.get_selection();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_bitname() {
    }

    @Test
    public void testSet_bitname() {
        System.out.println("set_bitname");
        TopoAlgoOutput instance = new TopoAlgoOutput();
        String input = "a string";
        instance.set_bitname(input);
        String expResult = "a string";
        Object result = instance.get_bitname();
        assertEquals(expResult, result);
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        TopoAlgoOutput instance = new TopoAlgoOutput();
        String expResult= "TOPO ALGO OUTPUT: ID=-1, Name=, Value=, Selection=-1, Bit Name=";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
    }

    @Test
    public void testSave() throws Exception {
        System.out.println("save");
        TopoAlgoOutput instance = new TopoAlgoOutput();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Integer expResult = 1;
        Object result = instance.save();
        assertEquals(expResult, result);
    }

    @Test
    public void testCheckExisting() {
        System.out.println("checkExisting");
        TopoAlgoOutput instance = new TopoAlgoOutput();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Integer expResult = -1;
        TopoAlgoOutput param = new TopoAlgoOutput();
        ArrayList<TopoAlgoOutput> input = new ArrayList<>();
        input.add(param);
        Object result = instance.checkExisting(input);
        assertEquals(expResult, result);
    }

    @Test
    public void testBatchsave() throws Exception {
        System.out.println("batchsave");
        TopoAlgoOutput instance = new TopoAlgoOutput();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        TopoAlgoOutput param = new TopoAlgoOutput();
        ArrayList<TopoAlgoOutput> input = new ArrayList<>();
        input.add(param);
        Object result = instance.batchsave(input);
        TreeMap<Integer, Integer> expResult = new TreeMap<>();
        expResult.put(0,0);
        assertEquals(expResult, result);
    }

    @Test
    public void testEquals() {
        System.out.println("equals");
        
        TopoAlgoOutput instance = new TopoAlgoOutput();
        TopoAlgoOutput param1 = new TopoAlgoOutput();
        TopoAlgoOutput param2 = new TopoAlgoOutput();
        Boolean expTrueResult = true;
        Boolean expFalseResult = false;
            
        String IncorrectType = "";
        Object Result1 = param1.equals(IncorrectType);
        assertEquals(Result1,expFalseResult);
        assertNotEquals(Result1,expTrueResult);
       
        instance.set_name("correct");
        param1.set_name("correct");
        param2.set_name("wrong");
        Object Result = param1.equals(instance);
        Object Result2 = param2.equals(instance);
        assertEquals(Result,expTrueResult);
        assertEquals(Result2,expFalseResult);
        
        instance.set_name("");
        param1.set_name("");
        param2.set_name("");
        param1.set_value("correct");
        param2.set_value("wrong");
        instance.set_value("correct");
        Object ResultA = param1.equals(instance);
        Object ResultB = param2.equals(instance);
        assertEquals(ResultA,expTrueResult);
        assertEquals(ResultB,expFalseResult);
       
        instance.set_value("");
        param1.set_value("");
        param2.set_value("");
        instance.set_selection(-2);
        param1.set_selection(-2);
        param2.set_selection(-3);
        Object Result3 = param1.equals(instance);
        Object Result4 = param2.equals(instance);
        assertEquals(Result3,expTrueResult);
        assertEquals(Result4,expFalseResult);
        
        instance.set_selection(-1);
        param1.set_selection(-1);
        param2.set_selection(-1);
        instance.set_bitname("correct");
        param1.set_bitname("correct");
        param2.set_bitname("wrong");

        Object Result5 = param1.equals(instance);
        Object Result6 = param2.equals(instance);
        assertEquals(Result5,expTrueResult);
        assertEquals(Result6,expFalseResult);
    }

    @Test
    public void testClone() {
        System.out.println("clone");
        TopoAlgoOutput instance = new TopoAlgoOutput();
        Object expResult = instance;
        Object result = instance.clone();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetTableName() {
    }

    @Test
    public void testGet_min_names() {
        System.out.println("get_min_names");
        TopoAlgoOutput instance = new TopoAlgoOutput();
        
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("ID");
        expResult.add("Name");
        expResult.add("Value");
        expResult.add("Selection");
        expResult.add("Bit Name");
                
        Object Result = instance.get_min_names();
        assertEquals(expResult, Result);
    }

    @Test
    public void testGet_min_info() {
        System.out.println("get_min_info");
        TopoAlgoOutput instance = new TopoAlgoOutput();
        
        ArrayList<Object> expResult = new ArrayList<>();
        expResult.add(-1);
        expResult.add("");
        expResult.add("");
        expResult.add(-1);
        expResult.add("");
                
        Object Result = instance.get_min_info();
        assertEquals(expResult, Result);
    }
    
}
