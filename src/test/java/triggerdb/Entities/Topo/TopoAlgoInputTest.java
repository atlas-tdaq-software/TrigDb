/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.Topo;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import com.mockrunner.mock.jdbc.MockResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.Entities.HLT.HLTMaster;
import triggerdb.Entities.HLT.HLTRelease;
import triggerdb.Entities.L1.L1Master;
import triggerdb.Entities.Topo.TopoMaster;
import triggerdb.PrescaleSetAliasComponent;

/**
 *
 * @author davethomas
 */
public class TopoAlgoInputTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public TopoAlgoInputTest() {
    }
    
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @After
    public void tearDown() throws Exception {
        super.setUp();
        ConnectionManager.getInstance().unsetTestingConnection();
    }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
    }
    
    @Test
    public void testForceLoad() throws Exception {
    }

    @Test
    public void testGet_value() {
    }

    @Test
    public void testSet_value() {
        System.out.println("set_value");
        TopoAlgoInput instance = new TopoAlgoInput();
        String input = "a string";
        instance.set_value(input);
        String expResult = "a string";
        Object result = instance.get_value();
        assertEquals(expResult, result);
    }

    @Test
    public void testGet_position() {
    }

    @Test
    public void testSet_position() {
        System.out.println("set_position");
        TopoAlgoInput instance = new TopoAlgoInput();
        Integer input = 3;
        instance.set_position(input);
        Integer expResult = 3;
        Object result = instance.get_position();
        assertEquals(expResult, result);
    }

    @Test
    public void testToString() {    
        System.out.println("toString");
        TopoAlgoInput instance = new TopoAlgoInput();
        String expResult= "TOPO ALGO INPUT: ID=-1, Name=, Value=, Position=-1";
        Object Result = instance.toString();
        assertEquals(expResult, Result);
    }

    @Test
    public void testSave() throws Exception {
        System.out.println("save");
        TopoAlgoInput instance = new TopoAlgoInput();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Integer expResult = 1;
        Object result = instance.save();
        assertEquals(expResult, result);
    }

    @Test
    public void testCheckExisting() {
        System.out.println("checkExisting");
        TopoAlgoInput instance = new TopoAlgoInput();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        Integer expResult = -1;
        TopoAlgoInput param = new TopoAlgoInput();
        ArrayList<TopoAlgoInput> input = new ArrayList<>();
        input.add(param);
        Object result = instance.checkExisting(input);
        assertEquals(expResult, result);
    }

    @Test
    public void testBatchsave() throws Exception {
        System.out.println("batchsave");
        TopoAlgoInput instance = new TopoAlgoInput();
        MockResultSet resultset = resultSetHandler.createResultSet();
        setEmptyInitInfo();
        TopoAlgoInput param = new TopoAlgoInput();
        ArrayList<TopoAlgoInput> input = new ArrayList<>();
        input.add(param);
        Object result = instance.batchsave(input);
        TreeMap<Integer, Integer> expResult = new TreeMap<>();
        expResult.put(0,0);
        assertEquals(expResult, result);
    }

    @Test
    public void testEquals() {
        System.out.println("equals");
        
        TopoAlgoInput instance = new TopoAlgoInput();
        TopoAlgoInput param1 = new TopoAlgoInput();
        TopoAlgoInput param2 = new TopoAlgoInput();
        Boolean expTrueResult = true;
        Boolean expFalseResult = false;
            
        String IncorrectType = "";
        Object Result1 = param1.equals(IncorrectType);
        assertEquals(Result1,expFalseResult);
        assertNotEquals(Result1,expTrueResult);
       
        instance.set_name("correct");
        param1.set_name("correct");
        param2.set_name("wrong");
        Object Result = param1.equals(instance);
        Object Result2 = param2.equals(instance);
        assertEquals(Result,expTrueResult);
        assertEquals(Result2,expFalseResult);
        
        instance.set_name("");
        param1.set_name("");
        param2.set_name("");
        param1.set_value("correct");
        param2.set_value("wrong");
        instance.set_value("correct");
        Object ResultA = param1.equals(instance);
        Object ResultB = param2.equals(instance);
        assertEquals(ResultA,expTrueResult);
        assertEquals(ResultB,expFalseResult);
       
        instance.set_value("");
        param1.set_value("");
        param2.set_value("");
        instance.set_position(-2);
        param1.set_position(-2);
        param2.set_position(-3);
        Object Result3 = param1.equals(instance);
        Object Result4 = param2.equals(instance);
        assertEquals(Result3,expTrueResult);
        assertEquals(Result4,expFalseResult);
    }

    @Test
    public void testClone() {
        System.out.println("clone");
        TopoAlgoInput instance = new TopoAlgoInput();
        Object expResult = instance;
        Object result = instance.clone();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetTableName() {
    }

    @Test
    public void testGet_min_names() {
        System.out.println("get_min_names");
        TopoAlgoInput instance = new TopoAlgoInput();
        
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("ID");
        expResult.add("Name");
        expResult.add("Value");
        expResult.add("Position");
                
        Object Result = instance.get_min_names();
        assertEquals(expResult, Result);
    }

    @Test
    public void testGet_min_info() {
        System.out.println("get_min_info");
        TopoAlgoInput instance = new TopoAlgoInput();
        
        ArrayList<Object> expResult = new ArrayList<>();
        expResult.add(-1);
        expResult.add("");
        expResult.add("");
        expResult.add(-1);
                
        Object Result = instance.get_min_info();
        assertEquals(expResult, Result);
    }
    
}
