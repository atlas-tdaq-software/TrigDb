/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.HLTLinks;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author davethomas
 */
public class HLTTM_PSTest {
    
    public HLTTM_PSTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetTM2PS() throws Exception {
    }

    @Test
    public void testGetMenus() throws Exception {
    }

    @Test
    public void testUnhidePS() throws Exception {
    }

    @Test
    public void test_HLTTM_PS() {
    }

    @Test
    public void testGet_menu_id() {
        System.out.println("get_menu_id");
        HLTTM_PS instance = new HLTTM_PS();
        Integer expResult = -1;
        Object Result = instance.get_menu_id();
        assertEquals(expResult,Result);
    }

    @Test
    public void testSet_menu_id() {
        System.out.println("set_menu_id");
        HLTTM_PS instance = new HLTTM_PS();
        Integer input = -1992;
        instance.set_menu_id(input);
        Integer expResult = instance.get_menu_id();
        assertEquals(expResult,input);
    }

    @Test
    public void testGet_prescale_set_id() {
        System.out.println("get_prescale_set_id");
        HLTTM_PS instance = new HLTTM_PS();
        Integer expResult = -1;
        Object Result = instance.get_prescale_set_id();
        assertEquals(expResult,Result);
    }

    @Test
    public void testSet_prescale_set_id() {
        System.out.println("set_prescale_set_id");
        HLTTM_PS instance = new HLTTM_PS();
        Integer input = -1992;
        instance.set_prescale_set_id(input);
        Integer expResult = instance.get_prescale_set_id();
        assertEquals(expResult,input);
    }

    @Test
    public void testGet_hidden() {
        System.out.println("get_hidden");
        HLTTM_PS instance = new HLTTM_PS();
        Boolean expResult = false;
        Object Result = instance.get_hidden();
        assertEquals(expResult,Result);
    }

    @Test
    public void testSet_hidden() {
        System.out.println("set_hidden");
        HLTTM_PS instance = new HLTTM_PS();
        Boolean input = true;
        instance.set_hidden(input);
        Boolean expResult = instance.get_hidden();
        assertEquals(expResult,input);
    }

    @Test
    public void testSave() throws Exception {
    }

    @Test
    public void testClone() {
        //As yet unsupported
    }

    @Test
    public void testGetTableName() {
        System.out.println("getTableName");
        HLTTM_PS instance = new HLTTM_PS();
        String expResult = "HLT_TM_TO_PS";
        String result = instance.getTableName();
        assertEquals(expResult, result);
    }
    
}
