package triggerdb.Entities;

import org.junit.*;
import triggerdb.Connections.*;

import static org.junit.Assert.*;
import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.*;
import com.mockrunner.mock.jdbc.*;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by adambozson on 04/07/2016.
 */
public class AbstractTableTest extends BasicJDBCTestCaseAdapter{

    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }

    @After
    @Override
    public void tearDown() throws Exception {
        super.tearDown();
        ConnectionManager.getInstance().unsetTestingConnection();
    }
    
    @Test
    public void testGetQueryString() {
        String output = table.getQueryString();
        String expected = "SELECT ID, NAME, VERSION FROM FAKE_TABLE";
        assertEquals(expected, output);
    }

    @Test
    public void testSetName() {
        table.set_name("test_name");
        String actual = (String) table.keyValue.get("NAME");
        assertEquals("test_name", actual);
    }

    @Test
    public void testGetTableName() {
        String actual = table.getTableName();
        assertEquals("FAKE_TABLE", actual);
    }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
    }
    
    @Test
    public void testDoesExist() throws java.sql.SQLException {
        setEmptyInitInfo();
        
        // Configure the mock DB response
        MockResultSet result = resultSetHandler.createResultSet();
        result.addRow(new Object[] {"1"});
        String statement = "SELECT COUNT(*) FROM FAKE_TABLE WHERE ID=?";
        resultSetHandler.prepareResultSet(statement, result, new Integer[] {123});
        
        assertTrue(table.exists(123));        
        
        verifySQLStatementExecuted(statement);
    }
    
    @Test
    public void testDoesNotExist() throws java.sql.SQLException {
        setEmptyInitInfo();        
        assertFalse(table.exists(123));        
        verifySQLStatementExecuted("SELECT COUNT(*) FROM FAKE_TABLE WHERE ID=?");
    }
    
    @Test
    public void testLoadFromRsetDefault() throws java.sql.SQLException {
        MockResultSet result = resultSetHandler.createResultSet();
        result.addRow(new Object[] {"321", "test_name", "6789"});
        result.next();
        
        table.loadFromRset(result);
        
        assertEquals(321, table.get_id());
        assertEquals("test_name", table.get_name());
        assertEquals((Integer)6789, table.get_version());
    }
    
    @Test
    public void testLoadFromRsetModifiable()
            throws java.sql.SQLException, java.text.ParseException {
        MockResultSet result = resultSetHandler.createResultSet();
        result.addRow(new Object [] {"5", "2016-12-25 09:41:03", "test", "TRUE", "tttest", "100"});
        result.next();
        
        table = new FakeAbstractTable("L1PS_");
        table.loadFromRset(result);
        
        assertEquals(5, table.get_id());
        assertEquals("test", table.get_name());
        assertEquals((Integer)100, table.get_version());
        assertEquals(true, table.get_used());
        assertEquals("tttest", table.get_username());

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd hh mm ss");
        Date date = sdf.parse("2016 12 25 09 41 03");
        long millis = date.getTime();
        
        assertEquals(new java.sql.Timestamp(millis), table.get_modified());        
    }
    
    @Test(expected = java.sql.SQLException.class)
    public void testLoadFromRsetUnknownType() throws java.sql.SQLException {
        MockResultSet result = resultSetHandler.createResultSet();
        result.addRow(new Object[] {"321", "test_name", "6789", "something"});
        result.next();
        
        table.setKeyValue("z_test", new Object());
        table.loadFromRset(result);
    }
}