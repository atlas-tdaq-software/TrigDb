/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Diff;

import com.mockrunner.jdbc.BasicJDBCTestCaseAdapter;
import com.mockrunner.jdbc.PreparedStatementResultSetHandler;
import com.mockrunner.mock.jdbc.MockConnection;
import com.mockrunner.mock.jdbc.MockResultSet;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.InitInfo;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.FakeAbstractTable;
import triggerdb.Entities.HLT.HLTMaster;
import triggerdb.Entities.HLT.HLTRelease;
import triggerdb.Entities.L1.L1Item;
import triggerdb.Entities.L1.L1Master;
import triggerdb.Entities.SMT.SuperMasterTable;
import triggerdb.Entities.Topo.TopoMaster;
import triggerdb.Entities.alias.HLTPrescaleSetAliasLumi;
import triggerdb.Entities.alias.L1PrescaleSetAliasLumi;
import triggerdb.Entities.alias.PrescaleSetAlias;
import triggerdb.PrescaleSetAliasComponent;

/**
 *
 * @author davethomas
 */
public class DiffResultsTableModelTest extends BasicJDBCTestCaseAdapter {
    private FakeAbstractTable table;
    private PreparedStatementResultSetHandler resultSetHandler;
    
    public DiffResultsTableModelTest() {
    }
    
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        table = new FakeAbstractTable();
        
        // Mock DB setup
        MockConnection connection = getJDBCMockObjectFactory().getMockConnection();
        ConnectionManager.getInstance().setTestingConnection(connection);        
        resultSetHandler = connection.getPreparedStatementResultSetHandler();
    }
    
    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
        ConnectionManager.getInstance().unsetTestingConnection();
    }

    void setEmptyInitInfo() {
        ConnectionManager.getInstance().setInitInfo(new InitInfo());
    }
    @Test
    public void testGetHelp() {
        System.out.println("getHelp");
        DiffResultsTableModel instance = new DiffResultsTableModel();
        String expResult = "This table shows all differences relevant for this panel\n\n";
        Object result = instance.getHelp();
        assertEquals(expResult, result);
        
    }

    @Test
    public void testAddToHelp() {
        System.out.println("addToHelp");
        DiffResultsTableModel instance = new DiffResultsTableModel();
        String input = "a string";
        instance.addToHelp(input);
        String expResult = "This table shows all differences relevant for this panel\n\na string\n";
        Object result = instance.getHelp();
        assertEquals(expResult, result);
    }

    
    //THIS NEEDS FIXING
    //@Test
    public void testSetSMKIDs() {
        System.out.println("setSMKIDs");
        DiffResultsTableModel instance = new DiffResultsTableModel();
        instance.setColumnNamesNull();
        instance.setSMKIDs(27,12);
        ArrayList<String> expResult = new ArrayList<>();      
        expResult.add("Parameter");
        expResult.add("Value in SMK 27");
        expResult.add("Value in SMK 12");
        expResult.add("ID in SMK 27");
        expResult.add("ID in SMK 12");
        expResult.add("Value in key 1");
        expResult.add("Value in key 2");
        expResult.add("ID in key 1");
        expResult.add("ID in key 2");
        Object result = instance.getColumnNames();
        assertEquals(expResult, result);
        
        DiffResultsTableModel instance2 = new DiffResultsTableModel();
        instance2.setColumnNames("string1","string2","string3","string4","string5");
        instance2.setSMKIDs(27,12);
        ArrayList<String> expResult2 = new ArrayList<>();
        expResult2.add(1, "Value in SMK 27");
        expResult2.add(2, "Value in SMK 12");
        expResult2.add(3, "ID in SMK 27");
        expResult2.add(4, "ID in SMK 12");
        Object result2 = instance2.getColumnNames();
        assertEquals(expResult2, result2);
        
    }

    @Test
    public void testSetDefaultColumnNames() {
        System.out.println("setDefaultColumnNames");
        DiffResultsTableModel instance = new DiffResultsTableModel();
        instance.setColumnNamesNull();
        instance.setDefaultColumnNames();
        ArrayList<String> expResult = new ArrayList<>();      
        expResult.add("Parameter");
        expResult.add("Value in key 1");
        expResult.add("Value in key 2");
        expResult.add("ID in key 1");
        expResult.add("ID in key 2");
        Object result = instance.getColumnNames();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetColumnNames() {
        System.out.println("setColumnNames");
        DiffResultsTableModel instance = new DiffResultsTableModel();
        instance.setColumnNames("string1","string2","string3","string4","string5");
        ArrayList<String> expResult = new ArrayList<>();      
        expResult.add("string1");
        expResult.add("string2");
        expResult.add("string3");
        expResult.add("string4");
        expResult.add("string5");
        Object result = instance.getColumnNames();
        assertEquals(expResult, result);
    }

    @Test
    public void testAddRow_List() {
    }

    @Test
    public void testAddRow_List_MouseListener() {
    }

    @Test
    public void testInsertRow_int_List() {
    }

    @Test
    public void testInsertRow_3args() {
    }

    @Test
    public void testRemoveRow() {
    }

    @Test
    public void testResetContents() {
    }

    @Test
    public void testGetColumnCount() {
        System.out.println("getColumnCount");
        DiffResultsTableModel instance = new DiffResultsTableModel();
        Integer expResult = 5;
        Object result = instance.getColumnCount();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetRowCount() {
        System.out.println("getRowCount");
        DiffResultsTableModel instance = new DiffResultsTableModel();
        Integer expResult = 0;
        Object result = instance.getRowCount();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetRow() {
    }

    @Test
    public void testGetRows() {
        System.out.println("getRows");
        DiffResultsTableModel instance = new DiffResultsTableModel();
        List<DiffResultsRow> expResult = new ArrayList<>();
        Object result = instance.getRows();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetColumnName() {
        System.out.println("getColumnName");
        DiffResultsTableModel instance = new DiffResultsTableModel();
        String expResult = "Parameter";
        Object result = instance.getColumnName(0);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetValueAt() {
    }
    
}
