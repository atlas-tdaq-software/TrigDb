package triggerdb.Entities.Topo;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.TreeMap;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;

/**
 * Holds the contents of TOPO_OUTPUT_LINE.
 *
 * @author Alex Martyniuk
 */
public final class TopoOutputLine extends AbstractTable {

    /**
     * Standard constructor. If an ID of -1 is supplied we create a new, empty
     * record and don't attempt to load anything from the database. If the ID
     * supplied is greater than 0 we load this record from the database.
     *
     */
    public TopoOutputLine() {
        super("TOL_");
        setKeys();
    }
    
    /**
     *
     * @param input_id
     * @throws SQLException
     */
    public TopoOutputLine(final int input_id) throws SQLException {
        super(input_id, "TOL_");
        setKeys();
        if (input_id > 0) {
            forceLoad();
        }
    }
    
    private void setKeys() {
        keyValue.putFirst("ALGO_NAME", "", "ALGORITHM NAME");
        keyValue.putFirst("TRIGGERLINE", "", "TRIGGERLINE");
        keyValue.putFirst("ALGO_ID", -1, "ALGORITHM ID");
        keyValue.putFirst("MODULE", -1, "MODULE");
        keyValue.putFirst("FPGA", -1, "FPGA");
        keyValue.putFirst("FIRST_BIT", -1, "FIRST_BIT");
        keyValue.putFirst("CLOCK", -1, "CLOCK");
        
        keyValue.remove("NAME");
        keyValue.remove("VERSION");
    }

    /**
     * Force this record to be read again from the database.
     * @throws java.sql.SQLException
     */
    @Override
    public void forceLoad() throws SQLException {
        ConnectionManager.getInstance().forceLoad(this);
    }
    
    ///Get the algo ID of this outputline entry.

    /**
     *
     * @return
     */
    public Integer get_algo_id() {
        return (Integer) keyValue.get("ALGO_ID");
    }
    
    ///Change the algo ID of this outputline entry.

    /**
     *
     * @param inp
     */
    public void set_algo_id(final int inp) {
        keyValue.put("ALGO_ID", inp);
    }
    
    ///Get the algo name for this outputline entry.

    /**
     *
     * @return
     */
    public String get_algo_name() {
        return (String) keyValue.get("ALGO_NAME");
    }
   
    ///Change the algo name for this outputline entry.

    /**
     *
     * @param inp
     */
    public void set_algo_name(final String inp) {
        keyValue.put("ALGO_NAME", inp);
    }
    
    ///Get the triggerline of this outputline entry.

    /**
     *
     * @return
     */
    public String get_triggerline() {
        return (String) keyValue.get("TRIGGERLINE");
    }
    ///Change the triggerline of this outputline entry.

    /**
     *
     * @param inp
     */
    public void set_triggerline(final String inp) {
        keyValue.put("TRIGGERLINE", inp);
    }    
    
    ///Get the module of this outputline entry.

    /**
     *
     * @return
     */
    public Integer get_module() {
        return (Integer) keyValue.get("MODULE");
    }
    ///Change the module of this outputline entry.

    /**
     *
     * @param inp
     */
    public void set_module(final int inp) {
        keyValue.put("MODULE", inp);
    }
    
    ///Get the fpga of this outputline entry.

    /**
     *
     * @return
     */
    public Integer get_fpga() {
        return (Integer) keyValue.get("FPGA");
    }
    
    ///Change the fpga of this outputline entry.

    /**
     *
     * @param inp
     */
    public void set_fpga(final int inp) {
        keyValue.put("FPGA", inp);
    }
    
    ///Get the first bit of this outputline entry.

    /**
     *
     * @return
     */
    public Integer get_first_bit() {
        return (Integer) keyValue.get("FIRST_BIT");
    }
    
    ///Change the first bit of this outputline entry.

    /**
     *
     * @param inp
     */
    public void set_first_bit(final int inp) {
        keyValue.put("FIRST_BIT", inp);
    }
    
    ///Get the clock of this outputline entry.

    /**
     *
     * @return
     */
    public Integer get_clock() {
        return (Integer) keyValue.get("CLOCK");
    }
    
    ///Change the clock of this outputline entry.

    /**
     *
     * @param inp
     */
    public void set_clock(final int inp) {
        keyValue.put("CLOCK", inp);
    }
    
    @Override
    public String toString() {
        //Temporarily removed!
        /*if (get_id() == -1) {
            return "Topo Output Line";
        }*/
        return "TOPO OUTPUT LINE: ID=" + get_id() + ", Name=" + get_algo_name()+ ", Triggerline=" + get_triggerline() + ", Algo Id=" + get_algo_id()
                + ", Module=" + get_module() + ", FPGA=" + get_fpga() + ", First Bit=" + get_first_bit()
                + ", Clock=" + get_clock();
    }

    @Override
    public int save() throws SQLException {
        //Temporary no check save.
        set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix, get_id(), keyValue));
        return get_id();
    }
    
    /**
     * 
     * @param OutputLines
     * @return
     * @throws SQLException 
     */
    public TreeMap<Integer, Integer> batchsave(ArrayList<TopoOutputLine> OutputLines) throws SQLException {
        
        ArrayList<TreeMap<String, Object>> InputOutputLines = new ArrayList<>();
        for(TopoOutputLine outputLine:OutputLines){
            TreeMap<String, Object> table = new TreeMap<>();
            table.put("ALGO_NAME", outputLine.get_algo_name());
            table.put("TRIGGERLINE", outputLine.get_triggerline());
            table.put("ALGO_ID", outputLine.get_algo_id());
            table.put("MODULE", outputLine.get_module());
            table.put("FPGA", outputLine.get_fpga());
            table.put("FIRST_BIT", outputLine.get_first_bit());
            table.put("CLOCK", outputLine.get_clock());
            InputOutputLines.add(table);
        }
        TreeMap<Integer, Integer> results = ConnectionManager.getInstance().saveBatch("TOPO_OUTPUT_LINE", tablePrefix, 0, InputOutputLines);
        
        return results;
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj instanceof TopoOutputLine) {
            if (!this.get_algo_name().equals(((TopoOutputLine) obj).get_algo_name())) {
                return false;
            } else if (!this.get_triggerline().equals(((TopoOutputLine) obj).get_triggerline())) {
                return false;
            } else if (!this.get_algo_id().equals(((TopoOutputLine) obj).get_algo_id())) {
                return false;
            } else if (!this.get_module().equals(((TopoOutputLine) obj).get_module())) {
                return false;
            } else if (!this.get_fpga().equals(((TopoOutputLine) obj).get_fpga())) {
                return false;
            } else if (!this.get_first_bit().equals(((TopoOutputLine) obj).get_first_bit())) {
                return false;
            } else if (!this.get_clock().equals(((TopoOutputLine) obj).get_clock())) {
                return false;
            } else return true;
        } else {
            return false;
        }
    }
    
    @Override
    public Object clone() {
        TopoOutputLine copy = new TopoOutputLine();
        copy.set_algo_name(get_algo_name());
        copy.set_triggerline(get_triggerline());
        copy.set_algo_id(get_algo_id());
        copy.set_module(get_module());
        copy.set_fpga(get_fpga());
        copy.set_first_bit(get_first_bit());
        copy.set_clock(get_clock());
        return copy;
    }

    @Override
    public String getTableName() {
        return "TOPO_OUTPUT_LINE";
    }
    
        
     /**
     * Minimum names for the search results.
     * @return 
     */
    @Override
    public ArrayList<String> get_min_names() {
        ArrayList<String> info = new ArrayList<>();
        info.add("ID");
        info.add("Algorithm Name");
        info.add("Triggerline");
        info.add("Algorithm ID");
        info.add("Module");
        info.add("FPGA");
        info.add("First Bit");
        info.add("Clock");
        
        return info;
    }
    
    /**
     * Minimum information for the search results.
     * @return 
     */
    @Override
    public ArrayList<Object> get_min_info() {
        ArrayList<Object> info = new ArrayList<>();
        info.add(get_id());
        info.add(get_algo_name());
        info.add(get_triggerline());
        info.add(get_algo_id());
        info.add(get_module());
        info.add(get_fpga());
        info.add(get_first_bit());
        info.add(get_clock());
        return info;
    }
}
