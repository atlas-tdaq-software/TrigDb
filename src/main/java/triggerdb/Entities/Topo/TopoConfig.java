package triggerdb.Entities.Topo;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.TreeMap;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;

/**
 * Holds the contents of TOPO_CONFIG.
 *
 * @author Alex Martyniuk
 */
public final class TopoConfig extends AbstractTable {

    /**
     * Standard constructor. If an ID of -1 is supplied we create a new, empty
     * record and don't attempt to load anything from the database. If the ID
     * supplied is greater than 0 we load this record from the database.
     *
     */
    public TopoConfig() {
        super("TC_");
        setKeys();
    }

    /**
     *
     * @param input_id
     * @throws SQLException
     */
    public TopoConfig(final int input_id) throws SQLException {
        super(input_id, "TC_");
        setKeys();
        if (input_id > 0) {
            forceLoad();
        }
    }

    private void setKeys() {
        keyValue.putFirst("VALUE", -1, "VALUE");
        keyValue.remove("VERSION");        
    }
    /**
     * Force this record to be read again from the database.
     * @throws java.sql.SQLException
     */
    @Override
    public void forceLoad() throws SQLException {
        ConnectionManager.getInstance().forceLoad(this);
    }
    
    ///Get the value for this config.

    /**
     *
     * @return
     */
    public Integer get_value() {
        return (Integer) keyValue.get("VALUE");
    }
    
    ///Change the value for this config.

    /**
     *
     * @param inp
     */
    public void set_value(final int inp) {
        keyValue.put("VALUE", inp);
    }
    
    @Override
    public String toString() {
        //Temporaily removed
        /*if (get_id() == -1) {
            return "Topo Config";
        }*/
        return "TOPO Config: ID=" + get_id() + ", Name=" + get_name() + ", Value=" + get_value();
    }

    @Override
    public int save() throws SQLException {
        //Temporary no check save.
        set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix, get_id(), keyValue));
        return get_id();
    }
    
    /**
     * 
     * @param Configs
     * @return
     * @throws SQLException 
     */
    public TreeMap<Integer, Integer> batchsave(ArrayList<TopoConfig> Configs) throws SQLException {
        
        ArrayList<TreeMap<String, Object>> InputConfigs = new ArrayList<>();
        for(TopoConfig config:Configs){
            TreeMap<String, Object> table = new TreeMap<>();
            table.put("NAME", config.get_name());
            table.put("VALUE", config.get_value());
            InputConfigs.add(table);
        }
        TreeMap<Integer, Integer> results = ConnectionManager.getInstance().saveBatch("TOPO_CONFIG", tablePrefix, 0, InputConfigs);
        return results;
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj instanceof TopoConfig) {
            if (!this.get_name().equals(((TopoConfig) obj).get_name())) {
                return false;
            } else if (!this.get_value().equals(((TopoConfig) obj).get_value())) {
                return false;
            } else return true;
        } else {
            return false;
        }
    }
    
    @Override
    public Object clone() {
        TopoConfig copy = new TopoConfig();
        copy.set_name(get_name());
        copy.set_value(get_value());
        return copy;
    }

    @Override
    public String getTableName() {
        return "TOPO_CONFIG";
    }

    /**
     * Minimum names for the search results.
     * @return 
     */
    @Override
    public ArrayList<String> get_min_names() {
        ArrayList<String> info = new ArrayList<>();
        info.add("ID");
        info.add("Name");
        info.add("Value");
        return info;
    }
    
    /**
     * Minimum information for the search results.
     * @return 
     */
    @Override
    public ArrayList<Object> get_min_info() {
        ArrayList<Object> info = new ArrayList<>();
        info.add(get_id());
        info.add(get_name());
        info.add(get_value());
        return info;
    }
}
