package triggerdb.Entities.Topo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.logging.Level;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;

/**
 * Holds the contents of L1_TRIGGER_MENU.
 *
 * @author Alex Martyniuk
 */
public final class TopoGeneric extends AbstractTable {

    /**
     * Standard constructor. If an ID of -1 is supplied we create a new, empty
     * record and don't attempt to load anything from the database. If the ID
     * supplied is greater than 0 we load this record from the database.
     *
     */
    public TopoGeneric() {
        super("TG_");
        setKeys();
    }

    /**
     *
     * @param input_id
     * @throws SQLException
     */
    public TopoGeneric(final int input_id) throws SQLException {
        super(input_id, "TG_");
        setKeys();
        if (input_id > 0) {
            forceLoad();
        }
    }

    private void setKeys() {
        keyValue.putFirst("VALUE", "", "VALUE");
        keyValue.remove("VERSION");        
    }
    /**
     * Force this record to be read again from the database.
     * @throws java.sql.SQLException
     */
    @Override
    public void forceLoad() throws SQLException {
        ConnectionManager.getInstance().forceLoad(this);
    }
    
    ///Get the value for this generic.

    /**
     *
     * @return
     */
    public String get_value() {
        return (String) keyValue.get("VALUE");
    }
    
    ///Change the value for this generic.

    /**
     *
     * @param inp
     */
    public void set_value(final String inp) {
        keyValue.put("VALUE", inp);
    }
    
    @Override
    public String toString() {
        /*if (get_id() == -1) {
            return "Topo Generic";
        }*/
        return "TOPO Generic: ID=" + get_id() + ", Name=" + get_name() + ", Value=" + get_value();
    }

    @Override
    public int save() throws SQLException {
        //Temporary no check save.
        set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix, get_id(), keyValue));
        return get_id();
    }
    
    /**
     * Currently works one by one. Used only in development, but left incase people want to use it
     * @param Generics
     * @param Inputs
     * @return 
     */
    public int checkExisting(ArrayList<TopoGeneric> Generics){
        int out = -1;
        //select count(*),tai_id from topo_algo_input where tai_name||'.'||tai_value||'.'||tai_position in () group by tai_id having count(*) = ?;
        //Lets start making a query to the DB see if we have anything in there that matches.
        StringBuilder sb = new StringBuilder(); // Part 1 of the INSERT command
        
        sb.append("select count(*),");
                sb.append(tablePrefix);
                sb.append("ID from ");
                sb.append(getTableName());
                sb.append(" where tg_name||'.'||tg_value in (");
        
        ArrayList<String> toCheck = new ArrayList<>();
        for(TopoGeneric generic:Generics){
                sb.append("?,");
                toCheck.add(generic.get_name()+"."+generic.get_value());
        }
        sb.deleteCharAt(sb.length() - 1);
        sb.append(") group by tg_id having count(*) = 1");
        logger.info(sb.toString());
        try {
            PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(sb.toString());
            int i = 1;
            for(String check:toCheck){
                ps.setString(i,check);
                i++;
            }
            ResultSet rset = ConnectionManager.executeSelect(ps);
            while (rset.next()) {
               out = rset.getInt("TG_ID");
            }
            rset.close();
            ps.close();
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return out;
    }
    
    /**
     * 
     * @param Generics
     * @return
     * @throws SQLException 
     */
    public TreeMap<Integer, Integer> batchsave(ArrayList<TopoGeneric> Generics) throws SQLException {
        
        ArrayList<TreeMap<String, Object>> InputGenerics = new ArrayList<>();
        for(TopoGeneric generic:Generics){
            TreeMap<String, Object> table = new TreeMap<>();
            table.put("NAME", generic.get_name());
            table.put("VALUE", generic.get_value());
            InputGenerics.add(table);
        }
        TreeMap<Integer, Integer> results = ConnectionManager.getInstance().saveBatch("TOPO_GENERIC", tablePrefix, 0, InputGenerics);
        return results;
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj instanceof TopoGeneric) {
            if (!this.get_name().equals(((TopoGeneric) obj).get_name())) {
                return false;
            } else if (!this.get_value().equals(((TopoGeneric) obj).get_value())) {
                return false;
            } else return true;
        } else {
            return false;
        }
    }
    
    @Override
    public Object clone() {
        TopoGeneric copy = new TopoGeneric();
        copy.set_name(get_name());
        copy.set_value(get_value());
        return copy;
    }

    @Override
    public String getTableName() {
        return "TOPO_GENERIC";
    }
    
     /**
     * Minimum names for the search results.
     * @return 
     */
    @Override
    public ArrayList<String> get_min_names() {
        ArrayList<String> info = new ArrayList<>();
        info.add("ID");
        info.add("Name");
        info.add("Value");
        return info;
    }
    
    /**
     * Minimum information for the search results.
     * @return 
     */
    @Override
    public ArrayList<Object> get_min_info() {
        ArrayList<Object> info = new ArrayList<>();
        info.add(get_id());
        info.add(get_name());
        info.add(get_value());
        return info;
    }

}
