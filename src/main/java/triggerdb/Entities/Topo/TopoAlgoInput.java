package triggerdb.Entities.Topo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.logging.Level;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;

/**
 * Holds the contents of TOPO_ALGO_INPUT.
 *
 * @author Alex Martyniuk
 */
public final class TopoAlgoInput extends AbstractTable {
    /**
     * Standard constructor. If an ID of -1 is supplied we create a new, empty
     * record and don't attempt to load anything from the database. If the ID
     * supplied is greater than 0 we load this record from the database.
     *
     */
    public TopoAlgoInput() {
        super(-1,"TAI_");
        setKeys();
    }

    /**
     *
     * @param input_id
     * @throws SQLException
     */
    public TopoAlgoInput(final int input_id) throws SQLException {
        super(input_id, "TAI_");
        setKeys();
        if (input_id > 0) {
            forceLoad();
        }
    }

    private void setKeys() {
        keyValue.putFirst("VALUE", "", "VALUE");
        keyValue.putFirst("POSITION", -1, "POSITION"); 
        keyValue.remove("VERSION");
    }
    
    /**
     * Force this record to be read again from the database.
     * @throws java.sql.SQLException
     */
    @Override
    public void forceLoad() throws SQLException {
        ConnectionManager.getInstance().forceLoad(this);
    }
    
    ///Get the value for this input.

    /**
     *
     * @return
     */
    public String get_value() {
        return (String) keyValue.get("VALUE");
    }
    
    ///Change the value for this input.

    /**
     *
     * @param inp
     */
    public void set_value(final String inp) {
        keyValue.put("VALUE", inp);
    }
    
    ///Get the position of this algo input.

    /**
     *
     * @return
     */
    public Integer get_position() {
        return (Integer) keyValue.get("POSITION");
    }
    
    ///Change the position of this algo input.

    /**
     *
     * @param inp
     */
    public void set_position(final int inp) {
        keyValue.put("POSITION", inp);
    }
    
    @Override
    public String toString() {
        /*if (get_id() == -1) {
            return "Topo Algorithm Input";
        }*/
        return "TOPO ALGO INPUT: ID=" + get_id() + ", Name=" + get_name() + ", Value=" + get_value()
                 + ", Position=" + get_position();
    }

    @Override
    public int save() throws SQLException {
        //Temporary no check save.
        set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix, get_id(), keyValue));
        return get_id();
    }

    /**
     * Currently works one by one. Used only in development, but left incase people want to use it
     * @param Inputs
     * @return 
     */
    public int checkExisting(ArrayList<TopoAlgoInput> Inputs){
        int out = -1;
        //select count(*),tai_id from topo_algo_input where tai_name||'.'||tai_value||'.'||tai_position in () group by tai_id having count(*) = ?;
        //Lets start making a query to the DB see if we have anything in there that matches.
        StringBuilder sb = new StringBuilder(); // Part 1 of the INSERT command
        
        sb.append("select count(*),");
                sb.append(tablePrefix);
                sb.append("ID from ");
                sb.append(getTableName());
                sb.append(" where tai_name||'.'||tai_value||'.'||tai_position in (");
        
        ArrayList<String> toCheck = new ArrayList<>();
        for(TopoAlgoInput input:Inputs){
                sb.append("?,");
                toCheck.add(input.get_name()+"."+input.get_value()+"."+input.get_position());
        }
        sb.deleteCharAt(sb.length() - 1);
        sb.append(") group by tai_id having count(*) = 1");
        logger.info(sb.toString());
        try {
            PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(sb.toString());
            int i = 1;
            for(String check:toCheck){
                ps.setString(i,check);
                i++;
            }
            ResultSet rset = ConnectionManager.executeSelect(ps);
            while (rset.next()) {
               out = rset.getInt("TAI_ID");
            }
            rset.close();
            ps.close();
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return out;
    }
    
    /**
     * Run this to save a block of inputs
     * @param Inputs
     * @return
     * @throws SQLException 
     */
    public TreeMap<Integer, Integer> batchsave(ArrayList<TopoAlgoInput> Inputs) throws SQLException {

        ArrayList<TreeMap<String, Object>> InputInputs = new ArrayList<>();
        for(TopoAlgoInput input:Inputs){
                TreeMap<String, Object> table = new TreeMap<>();
                table.put("NAME", input.get_name());
                table.put("VALUE", input.get_value());
                table.put("POSITION", input.get_position());
                InputInputs.add(table);
        }
        TreeMap<Integer, Integer> results = ConnectionManager.getInstance().saveBatch("TOPO_ALGO_INPUT", tablePrefix, 0, InputInputs);
        return results;
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj instanceof TopoAlgoInput) {
            if (!this.get_name().equals(((TopoAlgoInput) obj).get_name())) {
                return false;
            } else if (!this.get_value().equals(((TopoAlgoInput) obj).get_value())) {
                return false;
            } else if (!this.get_position().equals(((TopoAlgoInput) obj).get_position())) {
                return false;
            } else return true;
        } else {
            return false;
        }
    }
    
    @Override
    public Object clone() {
        TopoAlgoInput copy = new TopoAlgoInput();
        copy.set_name(get_name());
        copy.set_value(get_value());
        copy.set_position(get_position());
        return copy;
    }

    @Override
    public String getTableName() {
        return "TOPO_ALGO_INPUT";
    }
    
    /**
     * Minimum names for the search results.
     * @return 
     */
    @Override
    public ArrayList<String> get_min_names() {
        ArrayList<String> info = new ArrayList<>();
        info.add("ID");
        info.add("Name");
        info.add("Value");
        info.add("Position");
        return info;
    }
    
    /**
     * Minimum information for the search results.
     * @return 
     */
    @Override
    public ArrayList<Object> get_min_info() {
        ArrayList<Object> info = new ArrayList<>();
        info.add(get_id());
        info.add(get_name());
        info.add(get_value());
        info.add(get_position());
        return info;
    }
}
