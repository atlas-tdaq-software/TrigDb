package triggerdb.Entities.Topo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.logging.Level;
import triggerdb.Entities.AbstractTable;
import triggerdb.Connections.ConnectionManager;

/**
 * Holds the contents of TOPO_ALGO_OUTPUT.
 *
 * @author Alex Martyniuk
 */
public final class TopoAlgoOutput extends AbstractTable {

    /**
     * Standard constructor. If an ID of -1 is supplied we create a new, empty
     * record and don't attempt to load anything from the database. If the ID
     * supplied is greater than 0 we load this record from the database.
     *
     */
    public TopoAlgoOutput() {
        super("TAO_");
        setKeys();
    }

    /**
     *
     * @param input_id
     * @throws SQLException
     */
    public TopoAlgoOutput(final int input_id) throws SQLException {
        super(input_id, "TAO_");
        setKeys();
        if (input_id > 0) {
            forceLoad();
        }
    }

    private void setKeys() {
        keyValue.putFirst("VALUE", "", "VALUE");
        keyValue.putFirst("SELECTION", -1, "SELECTION");        
        keyValue.putFirst("BITNAME", "", "BITNAME");
        keyValue.remove("VERSION");
    }
    /**
     * Force this record to be read again from the database.
     * @throws java.sql.SQLException
     */
    @Override
    public void forceLoad() throws SQLException {
        ConnectionManager.getInstance().forceLoad(this);
    }
    
    ///Get the value for this output.

    /**
     *
     * @return
     */
    public String get_value() {
        return (String) keyValue.get("VALUE");
    }
    
    ///Change the value for this output.

    /**
     *
     * @param inp
     */
    public void set_value(final String inp) {
        keyValue.put("VALUE", inp);
    }
    
    ///Get the selection of this algo output.

    /**
     *
     * @return
     */
    public Integer get_selection() {
        return (Integer) keyValue.get("SELECTION");
    }
    
    ///Change the selection of this algo output.

    /**
     *
     * @param inp
     */
    public void set_selection(final int inp) {
        keyValue.put("SELECTION", inp);
    }
    
    ///Get the value for this output.

    /**
     *
     * @return
     */
    public String get_bitname() {
        return (String) keyValue.get("BITNAME");
    }
    
    ///Change the value for this output.

    /**
     *
     * @param inp
     */
    public void set_bitname(final String inp) {
        keyValue.put("BITNAME", inp);
    }
    
    @Override
    public String toString() {
        /*if (get_id() == -1) {
            return "Topo Algorithm Output";
        }*/
        return "TOPO ALGO OUTPUT: ID=" + get_id() + ", Name=" + get_name() + ", Value=" + get_value()
                + ", Selection=" + get_selection() + ", Bit Name=" + get_bitname();
    }

    @Override
    public int save() throws SQLException {
        //Temporary no check save.
        set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix, get_id(), keyValue));
        return get_id();
    }
    
    /**
     * Currently works one by one. Used only in development, but left incase people want to use it
     * @param Outputs
     * @param Inputs
     * @return 
     */
    public int checkExisting(ArrayList<TopoAlgoOutput> Outputs){
        int out = -1;
        //select count(*),tai_id from topo_algo_input where tai_name||'.'||tai_value||'.'||tai_position in () group by tai_id having count(*) = ?;
        //Lets start making a query to the DB see if we have anything in there that matches.
        StringBuilder sb = new StringBuilder(); // Part 1 of the INSERT command
        
        sb.append("select count(*),");
                sb.append(tablePrefix);
                sb.append("ID from ");
                sb.append(getTableName());
                sb.append(" where tao_name||'.'||tao_value||'.'||tao_selection||'.'||tao_bitname in (");
        
        ArrayList<String> toCheck = new ArrayList<>();
        for(TopoAlgoOutput output:Outputs){
                sb.append("?,");
                String value = output.get_value();
                if(value.isEmpty()) value = "~";
                String bitname = output.get_bitname();
                if(bitname.isEmpty()) bitname = "~";
                toCheck.add(output.get_name()+"."+value+"."+output.get_selection()+"."+bitname);
        }
        sb.deleteCharAt(sb.length() - 1);
        sb.append(") group by tao_id having count(*) = 1");
        logger.info(sb.toString());
        try {
            PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(sb.toString());
            int i = 1;
            for(String check:toCheck){
                ps.setString(i,check);
                i++;
            }
            ResultSet rset = ConnectionManager.executeSelect(ps);
            while (rset.next()) {
               out = rset.getInt("TAO_ID");
            }
            rset.close();
            ps.close();
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return out;
    }
    
    /**
     * 
     * @param Outputs
     * @return
     * @throws SQLException 
     */
    public TreeMap<Integer, Integer> batchsave(ArrayList<TopoAlgoOutput> Outputs) throws SQLException {
        
        ArrayList<TreeMap<String, Object>> InputOutputs = new ArrayList<>();
        for(TopoAlgoOutput output:Outputs){
            TreeMap<String, Object> table = new TreeMap<>();
            table.put("NAME", output.get_name());
            table.put("VALUE", output.get_value());
            table.put("SELECTION", output.get_selection());
            table.put("BITNAME", output.get_bitname());
            InputOutputs.add(table);
        }
        TreeMap<Integer, Integer> results = ConnectionManager.getInstance().saveBatch("TOPO_ALGO_OUTPUT", tablePrefix, 0, InputOutputs);
        return results;
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj instanceof TopoAlgoOutput) {
            if (!this.get_name().equals(((TopoAlgoOutput) obj).get_name())) {
                return false;
            } else if (!this.get_value().equals(((TopoAlgoOutput) obj).get_value())) {
                return false;
            } else if (!this.get_selection().equals(((TopoAlgoOutput) obj).get_selection())) {
                return false;
            } else if (!this.get_bitname().equals(((TopoAlgoOutput) obj).get_bitname())) {
                return false;
            } else return true;
        } else {
            return false;
        }
    }
    
    @Override
    public Object clone() {
        TopoAlgoOutput copy = new TopoAlgoOutput();
        copy.set_name(get_name());
        copy.set_value(get_value());
        copy.set_selection(get_selection());
        copy.set_bitname(get_bitname());
        return copy;
    }

    @Override
    public String getTableName() {
        return "TOPO_ALGO_OUTPUT";
    }

    /**
     * Minimum names for the search results.
     * @return 
     */
    @Override
    public ArrayList<String> get_min_names() {
        ArrayList<String> info = new ArrayList<>();
        info.add("ID");
        info.add("Name");
        info.add("Value");
        info.add("Selection");
        info.add("Bit Name");
        return info;
    }
    
    /**
     * Minimum information for the search results.
     * @return 
     */
    @Override
    public ArrayList<Object> get_min_info() {
        ArrayList<Object> info = new ArrayList<>();
        info.add(get_id());
        info.add(get_name());
        info.add(get_value());
        info.add(get_selection());
        info.add(get_bitname());
        return info;
    }
}
