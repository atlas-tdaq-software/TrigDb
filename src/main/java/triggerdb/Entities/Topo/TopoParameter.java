package triggerdb.Entities.Topo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.logging.Level;
import triggerdb.Entities.AbstractTable;
import triggerdb.Connections.ConnectionManager;

/**
 * Holds the contents of L1_TRIGGER_MENU.
 *
 * @author Alex Martyniuk
 */
public final class TopoParameter extends AbstractTable {

    /**
     * Standard constructor. If an ID of -1 is supplied we create a new, empty
     * record and don't attempt to load anything from the database. If the ID
     * supplied is greater than 0 we load this record from the database.
     *
     */
    public TopoParameter() {
        super("TP_");
        setKeys();
    }

    /**
     *
     * @param param_id
     * @throws SQLException
     */
    public TopoParameter(final int param_id) throws SQLException {
        super(param_id, "TP_");
        setKeys();
        if (param_id > 0) {
            forceLoad();
        }
    }

    private void setKeys() {
        keyValue.putFirst("POSITION", -1, "POSITION"); 
        keyValue.putFirst("VALUE", "", "VALUE");
        keyValue.putFirst("SELECTION", -1, "SELECTION"); 
        keyValue.remove("VERSION");
    }
    /**
     * Force this record to be read again from the database.
     * @throws java.sql.SQLException
     */
    @Override
    public void forceLoad() throws SQLException {
        ConnectionManager.getInstance().forceLoad(this);
    }
    
    ///Get the position of this algo param.

    /**
     *
     * @return
     */
    public Integer get_position() {
        return (Integer) keyValue.get("POSITION");
    }
    
    ///Change the position of this algo param.

    /**
     *
     * @param inp
     */
    public void set_position(final int inp) {
        keyValue.put("POSITION", inp);
    }
    
    ///Get the value for this param.

    /**
     *
     * @return
     */
    public String get_value() {
        return (String) keyValue.get("VALUE");
    }
    
    ///Change the value for this param.

    /**
     *
     * @param inp
     */
    public void set_value(final String inp) {
        keyValue.put("VALUE", inp);
    }
    
    ///Get the selection of this algo param.

    /**
     *
     * @return
     */
    public Integer get_selection() {
        return (Integer) keyValue.get("SELECTION");
    }
    
    ///Change the selection of this algo param.

    /**
     *
     * @param inp
     */
    public void set_selection(final int inp) {
        keyValue.put("SELECTION", inp);
    }
    
    @Override
    public String toString() {
        /*if (get_id() == -1) {
            return "Topo Parameter";
        }*/
        return "TOPO PARAM: ID=" + get_id() + ", Name=" + get_name() + ", Value=" + get_value()
                + ", Position=" + get_position() + ", Selection=" + get_selection();
    }

    @Override
    public int save() throws SQLException {
        //Temporary no check save.
        set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix, get_id(), keyValue));
        return get_id();
    }

    /**
     * Currently works one by one. Used only in development, but left incase people want to use it
     * @param Parameters
     * @param Inputs
     * @return 
     */
    public int checkExisting(ArrayList<TopoParameter> Parameters){
        int out = -1;
        //select count(*),tai_id from topo_algo_input where tai_name||'.'||tai_value||'.'||tai_position in () group by tai_id having count(*) = ?;
        //Lets start making a query to the DB see if we have anything in there that matches.
        StringBuilder sb = new StringBuilder(); // Part 1 of the INSERT command
        
        sb.append("select count(*),");
                sb.append(tablePrefix);
                sb.append("ID from ");
                sb.append(getTableName());
                sb.append(" where tp_name||'.'||tp_value||'.'||tp_position||'.'||tp_selection in (");
        
        ArrayList<String> toCheck = new ArrayList<>();
        for(TopoParameter parameter:Parameters){
                sb.append("?,");
                String position = parameter.get_position().toString();
                if(position.isEmpty()) position = "~";
                String selection = parameter.get_selection().toString();
                if(selection.isEmpty()) selection = "~";
                toCheck.add(parameter.get_name()+"."+parameter.get_value()+"."+position+"."+selection);
        }
        sb.deleteCharAt(sb.length() - 1);
        sb.append(") group by tp_id having count(*) = 1");
        logger.info(sb.toString());
        try {
            PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(sb.toString());
            int i = 1;
            for(String check:toCheck){
                ps.setString(i,check);
                i++;
            }
            ResultSet rset = ConnectionManager.executeSelect(ps);
            while (rset.next()) {
               out = rset.getInt("TP_ID");
            }
            rset.close();
            ps.close();
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return out;
    }
    
    /**
     * 
     * @param Parameters
     * @return
     * @throws SQLException 
     */
    public TreeMap<Integer, Integer> batchsave(ArrayList<TopoParameter> Parameters) throws SQLException {
        
        ArrayList<TreeMap<String, Object>> InputParameters = new ArrayList<>();
        for(TopoParameter parameter:Parameters){
            TreeMap<String, Object> table = new TreeMap<>();
            table.put("NAME", parameter.get_name());
            table.put("POSITION", parameter.get_position());
            table.put("VALUE", parameter.get_value());
            table.put("SELECTION", parameter.get_selection());
            InputParameters.add(table);
        }
        TreeMap<Integer, Integer> results = ConnectionManager.getInstance().saveBatch("TOPO_PARAMETER", tablePrefix, 0, InputParameters);
        return results;
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (obj instanceof TopoParameter) {
            if (!this.get_name().equals(((TopoParameter) obj).get_name())) {
                return false;
            } else if (!this.get_position().equals(((TopoParameter) obj).get_position())) {
                return false;
            } else if (!this.get_value().equals(((TopoParameter) obj).get_value())) {
                return false;
            } else if (!this.get_selection().equals(((TopoParameter) obj).get_selection())) {
                return false;
            } else return true;
        } else {
            return false;
        }
    }
    
    @Override
    public Object clone() {
        TopoParameter copy = new TopoParameter();
        copy.set_name(get_name());
        copy.set_position(get_position());
        copy.set_value(get_value());
        copy.set_selection(get_selection());
        return copy;
    }

    @Override
    public String getTableName() {
        return "TOPO_PARAMETER";
    }

}
