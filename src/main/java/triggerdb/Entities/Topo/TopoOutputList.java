package triggerdb.Entities.Topo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.tree.DefaultMutableTreeNode;
import triggerdb.Entities.AbstractTable;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.L1.L1Menu;

/**
 * Holds the contents of TOPO_TO_L1CTP.
 * This might be a weird one. It attempts to link the Topo menu to the L1 CTP.
 * The CTP needs to know about the outputs of the Topo coming on the cables
 * into it.
 *
 * @author Alex Martyniuk
 */
public final class TopoOutputList extends AbstractTable {

    ArrayList<TopoOutputLine> outputLines = new ArrayList<>();
  //  private DefaultMutableTreeNode outputListLayer = null;
    private ArrayList<DefaultMutableTreeNode> outputLayer = new ArrayList<>();
//    private ArrayList<DefaultMutableTreeNode> configLayer = new ArrayList<>();
    /**
     * Standard constructor. If an ID of -1 is supplied we create a new, empty
     * record and don't attempt to load anything from the database. If the ID
     * supplied is greater than 0 we load this record from the database.
     *
     */
    public TopoOutputList() {
        super("OL_");
        keyValue.remove("VERSION");
    }

    /**
     *
     * @param input_id
     * @throws SQLException
     */
    public TopoOutputList(final int input_id) throws SQLException {
        super(input_id, "OL_");
        keyValue.remove("VERSION");
        if (input_id > 0) {
            forceLoad();
        }
    }

    /**
     * Force this record to be read again from the database.
     * @throws java.sql.SQLException
     */
    @Override
    public void forceLoad() throws SQLException {
        ConnectionManager.getInstance().forceLoad(this);
    }
    
    /**
     *
     * @return
     * @throws SQLException
     */
    public ArrayList<TopoOutputLine> get_output_lines() throws SQLException{
        if (outputLines.isEmpty()) {
            ArrayList<String> ID = new ArrayList<>();
            ID.add(String.valueOf(this.get_id()));           
            ArrayList<Integer> matchedLines = ConnectionManager.getInstance().getLinkedTable(ID,"TOPO_OUTPUT_LINK","TOPO_OUTPUT_LINE","TL_LINK_ID","TL_OUTPUT_ID","TOL_ID","TOL_ALGO_ID");
            for(Integer i:matchedLines){
                TopoOutputLine line = new TopoOutputLine(i);
                outputLines.add(line);
            }
        }
        return outputLines;
    }
    
    /**
     * Load all the L1Items for this L1Menu in one query.
     *
     * @return All the items belonging to this menu.
     * @throws java.sql.SQLException
     */
    public ArrayList<TopoOutputLine> get_DB_output_lines() throws SQLException {
        ArrayList<TopoOutputLine> outputlines = new ArrayList<>();
        // If no items present, query them from DB.
        if (this.get_id() > 0) {
            String query = "SELECT TOL_ALGO_ID, TOL_ALGO_NAME, TOL_FIRST_BIT, TOL_CLOCK,"
                    + "TOL_FPGA, TOL_ID, TOL_MODULE, TOL_TRIGGERLINE "
                    + "FROM TOPO_OUTPUT_LINK, "
                    + "TOPO_OUTPUT_LINE "
                    + "WHERE TL_LINK_ID=? "
                    + "AND TL_OUTPUT_ID=TOL_ID "
                    + "ORDER BY TOL_ID ASC";
            ConnectionManager mgr = ConnectionManager.getInstance();
            query = mgr.fix_schema_name(query);

            Connection con = mgr.getConnection();
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1, get_id());
            ResultSet rset = ps.executeQuery();
            // we know more or less how many items we have. this speeds up 
            // ResultSet fetching
            rset.setFetchSize(L1Menu.MAXITEMS);
            while (rset.next()) {
                // This is VERY slow! Creation of items takes 90% of the
                // total time. 
                TopoOutputLine line = new TopoOutputLine(-1);
                line.loadFromRset(rset);
                outputlines.add(line);
            }
            rset.close();
            ps.close();
        }
        return outputlines;
    }
    
    /**
     *
     * @param Topo_Master_Id
     * @return
     * @throws SQLException
     */
    public static ArrayList<TopoOutputLine> get_DB_output_lines_from_master(int Topo_Master_Id) throws SQLException {
        ArrayList<TopoOutputLine> outputlines = new ArrayList<>();
        // If no items present, query them from DB.
        if (Topo_Master_Id > 0) {
            String query = "SELECT TOL_ALGO_ID, TOL_ALGO_NAME, TOL_FIRST_BIT, TOL_CLOCK,TOL_FPGA, "
                    + "TOL_ID, TOL_MODULE, TOL_TRIGGERLINE FROM TOPO_OUTPUT_LINK, TOPO_OUTPUT_LINE "
                    + "WHERE TL_LINK_ID="
                    + "("
                    + "SELECT ttm_ctplink_id from "
                    + "(select tmt_trigger_menu_id from TOPO_MASTER_TABLE where TMT_ID = ?) "
                    + "master join "
                    + "(select ttm_ctplink_id,ttm_id from TOPO_TRIGGER_MENU) "
                    + "menu on master.tmt_trigger_menu_id=menu.ttm_id"
                    + ") "
                    + "AND TL_OUTPUT_ID=TOL_ID ORDER BY TOL_ID ASC"; 
            ConnectionManager mgr = ConnectionManager.getInstance();
            query = mgr.fix_schema_name(query);

            Connection con = mgr.getConnection();
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1, Topo_Master_Id);
            ResultSet rset = ps.executeQuery();
            // we know more or less how many items we have. this speeds up 
            // ResultSet fetching
            rset.setFetchSize(L1Menu.MAXITEMS);
            while (rset.next()) {
                // This is VERY slow! Creation of items takes 90% of the
                // total time. 
                TopoOutputLine line = new TopoOutputLine(-1);
                line.loadFromRset(rset);
                outputlines.add(line);
            }
            rset.close();
            ps.close();
        }
        return outputlines;
    }

    /**
     *
     * @param Topo_Master_Id
     * @return
     * @throws SQLException
     */
    public static int get_DB_output_list_from_master(int Topo_Master_Id) throws SQLException {
        int output_list = -1;
        // If no items present, query them from DB.
        if (Topo_Master_Id > 0) {
            String query = "SELECT OL_ID FROM TOPO_OUTPUT_LIST "
                    + "WHERE OL_ID="
                    + "("
                    + "SELECT ttm_ctplink_id from "
                    + "(select tmt_trigger_menu_id from TOPO_MASTER_TABLE where TMT_ID = ?) "
                    + "master join "
                    + "(select ttm_ctplink_id,ttm_id from TOPO_TRIGGER_MENU) "
                    + "menu on master.tmt_trigger_menu_id=menu.ttm_id"
                    + ")";
            ConnectionManager mgr = ConnectionManager.getInstance();
            query = mgr.fix_schema_name(query);
            //logger.fine(query);
            Connection con = mgr.getConnection();
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1, Topo_Master_Id);
            ResultSet rset = ps.executeQuery();
            // we know more or less how many items we have. this speeds up 
            // ResultSet fetching
            rset.setFetchSize(L1Menu.MAXITEMS);
            while(rset.next()){
                output_list = (rset.getInt("OL_ID"));
            }
            rset.close();
            ps.close();
        }
        return output_list;
    }
    
  @Override
    public void addToTree(DefaultMutableTreeNode treeNode, int counter) throws SQLException {
        --counter;

        if (counter >= 0) {
/*
            if (outputListLayer == null && get_outputlist() != null) {
                outputListLayer = new DefaultMutableTreeNode(get_outputlist());
                treeNode.add(outputListLayer);
                get_outputlist().addToTree(outputListLayer, counter);
            } else if (get_outputlist() != null) {
                get_outputlist().addToTree(outputListLayer, counter);
            }

            if (algoLayer.isEmpty()) {
                for (TopoAlgo algo : get_algos()) {
                    DefaultMutableTreeNode singleAlgoLayer = new DefaultMutableTreeNode(algo);
                    treeNode.add(singleAlgoLayer);
                    algoLayer.add(singleAlgoLayer);
                }
            } else {
                int i = 0;
                for (TopoAlgo algo : get_algos()) {
                    algo.addToTree(algoLayer.get(i), counter);
                    i++;
                }
            }
*/
            if (outputLayer.isEmpty()) {
                for (TopoOutputLine line : get_output_lines()) {
                    DefaultMutableTreeNode singlelineLayer = new DefaultMutableTreeNode(line);
                    treeNode.add(singlelineLayer);
                    outputLayer.add(singlelineLayer);
                }
            } else {
                int i = 0;
                for (TopoOutputLine line : get_output_lines()) {
                    line.addToTree(outputLayer.get(i), counter);
                    i++;
                }
            }
        }
    }   
    
    @Override
    public String toString() {
        if (get_id() == -1) {
            return "Topo To L1CTP";
        }
        return "TOPO 2 L1CTP: ID=" + get_id() + ", Name=" + get_name();
    }

    @Override
    public int save() throws SQLException {
        //Temporary no check save.
        set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix, get_id(), keyValue));
        return get_id();
    }

    @Override
    public Object clone() {
        TopoOutputList copy = new TopoOutputList();
        copy.set_name(get_name());
        return copy;
    }

    @Override
    public String getTableName() {
        return "TOPO_OUTPUT_LIST"; 
    }

       /**
     * Minimum names for the search results.
     * @return 
     */
    @Override
    public ArrayList<String> get_min_names() {
        ArrayList<String> info = new ArrayList<>();
        info.add("ID");
        info.add("Name");        
        return info;
    }
    
    /**
     * Minimum information for the search results.
     * @return 
     */
    @Override
    public ArrayList<Object> get_min_info() {
        ArrayList<Object> info = new ArrayList<>();
        info.add(get_id());
        info.add(get_name());
        return info;
    }
    
}
