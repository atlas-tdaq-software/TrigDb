package triggerdb.Entities.Topo;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import javax.swing.tree.DefaultMutableTreeNode;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;

/**
 * Holds the contents of TOPO_TRIGGER_MENU.
 *
 * @author Alex Martyniuk
 */
public final class TopoMenu extends AbstractTable {

    /**
     * Max number of Topo items. I believe this is correct please check..
     */
    public static int MAXITEMS = 192;
    /**
     * XML Labels
     */
    public static final String XML_MENU_TAG = "TOPO_MENU";

    /**
     *
     */
    public static final String XML_NAME_ATTRIBUTE = "name";

    /**
     *
     */
    public static final String XML_ID_ATTRIBUTE = "id";

    /**
     *
     */
    public static final String XML_VERSION_ATTRIBUTE = "version";

    //A container to hold the attached generics
    private ArrayList<TopoConfig> configs = new ArrayList<>();
    //A container to hold the attached generics
    private TopoOutputList outputList;
    //A container to hold the attached generics
    private ArrayList<TopoAlgo> algos = new ArrayList<>();

    private DefaultMutableTreeNode outputListLayer = null;
    private ArrayList<DefaultMutableTreeNode> algoLayer = new ArrayList<>();
    private ArrayList<DefaultMutableTreeNode> configLayer = new ArrayList<>();

    /**
     * Standard constructor. If an ID of -1 is supplied we create a new, empty
     * record and don't attempt to load anything from the database. If the ID
     * supplied is greater than 0 we load this record from the database.
     *
     */
    public TopoMenu() {
        super("TTM_");
        setKeys();
    }

    /**
     *
     * @param input_id
     * @throws SQLException
     */
    public TopoMenu(final int input_id) throws SQLException {
        super(input_id, "TTM_");
        setKeys();
        if (input_id > 0) {
            forceLoad();
        }
    }

    private void setKeys() {
        keyValue.putFirst("CTPLINK_ID", -1, "CTP LINK ID");
    }

    /**
     * Force this record to be read again from the database.
     *
     * @throws java.sql.SQLException
     */
    @Override
    public void forceLoad() throws SQLException {
        ConnectionManager.getInstance().forceLoad(this);
    }

    @Override
    public String toString() {
        if (get_id() == -1) {
            return "Topo Menu";
        }
        return "TOPO MENU: ID=" + get_id() + ", Name=" + get_name() + ", Version=" + get_version();
    }

    ///Get the CTP link ID of the TopoMenu
    /**
     *
     * @return
     */
    public Integer get_ctplink() {
        return (Integer) keyValue.get("CTPLINK_ID");
    }
    ///Change the CTP link ID in this TopoMenu

    /**
     *
     * @param inp
     */
    public void set_ctplink(final int inp) {
        keyValue.put("CTPLINK_ID", inp);
    }

    /**
     * Attach an config to this menu
     *
     * @param config
     */
    public void add_config(TopoConfig config) {
        configs.add(config);
    }

    @Override
    public void addToTree(DefaultMutableTreeNode treeNode, int counter) throws SQLException {
        --counter;

        if (counter >= 0) {

            if (outputListLayer == null && get_outputlist() != null) {
                outputListLayer = new DefaultMutableTreeNode(get_outputlist());
                treeNode.add(outputListLayer);
                get_outputlist().addToTree(outputListLayer, counter);
            } else if (get_outputlist() != null) {
                get_outputlist().addToTree(outputListLayer, counter);
            }

            if (algoLayer.isEmpty()) {
                for (TopoAlgo algo : get_algos()) {
                    DefaultMutableTreeNode singleAlgoLayer = new DefaultMutableTreeNode(algo);
                    treeNode.add(singleAlgoLayer);
                    algoLayer.add(singleAlgoLayer);
                }
            } else {
                int i = 0;
                for (TopoAlgo algo : get_algos()) {
                    algo.addToTree(algoLayer.get(i), counter);
                    i++;
                }
            }

            if (configLayer.isEmpty()) {
                for (TopoConfig config : get_configs()) {
                    DefaultMutableTreeNode singleConfigLayer = new DefaultMutableTreeNode(config);
                    treeNode.add(singleConfigLayer);
                    configLayer.add(singleConfigLayer);
                }
            } else {
                int i = 0;
                for (TopoConfig config : get_configs()) {
                    config.addToTree(configLayer.get(i), counter);
                    i++;
                }
            }
        }
    }

    /**
     *
     * @param config
     */
    public void add_config(ArrayList<TopoConfig> config) {
        configs.addAll(config);
    }

    /**
     * Return the configs attached to this menu
     *
     * @return
     * @throws java.sql.SQLException
     */
    public ArrayList<TopoConfig> get_configs() throws SQLException {
        if (configs.isEmpty()) {
            logger.finer("In loop");
            ArrayList<String> ID = new ArrayList<>();
            ID.add(String.valueOf(this.get_id()));
            ArrayList<Integer> matchedConfigs = ConnectionManager.getInstance().getLinkedTable(ID, "TTM_TO_TC", "TOPO_CONFIG", "TTM2TC_MENU_ID", "TTM2TC_CONFIG_ID", "TC_ID", "TC_ID");
            for (Integer i : matchedConfigs) {
                TopoConfig config = new TopoConfig(i);
                configs.add(config);
            }
        }
        return configs;
    }

    /**
     * Attach an output list to this menu This is not an actual link the the DB,
     * but the link table does not exist yet.
     *
     * @param list
     */
    public void add_outputlist(TopoOutputList list) {
        outputList = list;
    }

    /**
     * Return the outputs attached to this menu (when no ctp link exists)
     *
     * @return
     * @throws java.sql.SQLException
     */
    public TopoOutputList get_outputlist() throws SQLException {
        if (outputList == null && new TopoOutputList().isValid(get_ctplink())) {
            outputList = new TopoOutputList(get_ctplink());
        }
        return outputList;
    }

    /**
     * Attach an algo to this menu
     *
     * @param algo
     */
    public void add_algo(TopoAlgo algo) {
        algos.add(algo);
    }

    /**
     *
     * @param algo
     */
    public void add_algo(ArrayList<TopoAlgo> algo) {
        algos.addAll(algo);
    }

    /**
     * Return the outputs attached to this menu (when no ctp link exists)
     *
     * @return
     * @throws java.sql.SQLException
     */
    public ArrayList<TopoAlgo> get_algos() throws SQLException {
        if (algos.isEmpty()) {
            ArrayList<String> ID = new ArrayList<>();
            ID.add(String.valueOf(this.get_id()));
            ArrayList<Integer> matchedAlgos = ConnectionManager.getInstance().getLinkedTable(ID, "TTM_TO_TA", "TOPO_ALGO", "TTM2TA_MENU_ID", "TTM2TA_ALGO_ID", "TA_ID", "TA_ALGO_ID");
            for (Integer i : matchedAlgos) {
                logger.log(Level.FINER, "Algo in menu : {0}", i);
                TopoAlgo algo = new TopoAlgo(i);
                algos.add(algo);
            }
        }
        return algos;
    }

    @Override
    public int doDiff(AbstractTable t, DefaultMutableTreeNode treeNode, java.util.Set<String> linkstoignore) throws SQLException {
        /// Don't consider the menu id
        if (linkstoignore == null) {
            linkstoignore = new java.util.HashSet<>();
        }

        int ndiff = super.doDiff(t, treeNode, linkstoignore);

        if (t instanceof TopoMenu) {
            TopoMenu menu = (TopoMenu) t;

            ndiff += triggerdb.Diff.CompareTables.compareTables(this.get_algos(), menu.get_algos(), treeNode);
            ndiff += triggerdb.Diff.CompareTables.compareTables(this.get_configs(), menu.get_configs(), treeNode);

        }
        return ndiff;
    }

    /**
     * Need to implement a new version save with clever queries not dumb
     * loops....
     *
     * @return
     * @throws SQLException
     */
    @Override
    public int save() throws SQLException {
        ConnectionManager mgr = ConnectionManager.getInstance();
        set_id(mgr.save(getTableName(), tablePrefix, get_id(), keyValue));
        return get_id();
    }

    @Override
    public Object clone() {
        TopoMenu copy = new TopoMenu();
        copy.set_name(get_name());
        copy.set_version(get_version());
        copy.set_ctplink(get_ctplink());
        return copy;
    }

    @Override
    public String getTableName() {
        return "TOPO_TRIGGER_MENU";
    }

    /**
     *
     * @return
     */
    public int get_hash() {
        return this.hashCode();
    }

    @Override
    /*
     * Hashcode override.
     * This will override the hashcode for the table. It should give a unique hash for the whole parameter.
     */
    public int hashCode() {
        long fill = 13;
        fill += 31 * this.get_name().hashCode(); //string: ok
        //+37*this.get_op().hashCode()
        //+41*this.get_value().hashCode()
        //+43*this.get_chain_user_version().hashCode();

        return ((int) fill) ^ ((int) (fill >> 32));
    }

}
