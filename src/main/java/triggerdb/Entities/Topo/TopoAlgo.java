package triggerdb.Entities.Topo;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.logging.Level;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.HLT.HLTRelease;
import triggerdb.Entities.SMT.StatusBits;

/**
 * Holds the contents of TOPO_ALGO.
 *
 * @author Alex Martyniuk
 */
public final class TopoAlgo extends AbstractTable {

    //A container to hold the attached inputs
    private ArrayList<TopoAlgoInput> inputs = new ArrayList<>();
    //A container to hold the attached outputs
    private ArrayList<TopoAlgoOutput> outputs = new ArrayList<>();
    //A container to hold the attached parameters
    private ArrayList<TopoParameter> parameters = new ArrayList<>();
    //A container to hold the attached generics
    private ArrayList<TopoGeneric> generics = new ArrayList<>();
    
    /**
     * Standard constructor. If an ID of -1 is supplied we create a new, empty
     * record and don't attempt to load anything from the database. If the ID
     * supplied is greater than 0 we load this record from the database.
     *
     */
    public TopoAlgo() {
        super("TA_");
        setKeys();
    }

    /**
     *
     * @param input_id
     * @throws SQLException
     */
    public TopoAlgo(final int input_id) throws SQLException {
        super(input_id, "TA_");
        setKeys();
        if (input_id > 0) {
            forceLoad();
        }
    }

    private void setKeys() {
        keyValue.putFirst("OUTPUT", "", "OUTPUT");
        keyValue.putFirst("TYPE", "", "TYPE");
        keyValue.putFirst("BITS", -1, "BITS");
        keyValue.putFirst("SORT_DECI", "", "SORT OR DECISION");
        keyValue.putFirst("ALGO_ID", -1, "ALGO ID");
        keyValue.remove("VERSION");
    }
    
    ///Get the algo ouput for this algo.

    /**
     *
     * @return
     */
    public String get_output() {
        return (String) keyValue.get("OUTPUT");
    }
   
    ///Change the algo output for this algo.

    /**
     *
     * @param inp
     */
    public void set_output(final String inp) {
        keyValue.put("OUTPUT", inp);
    }

    ///Get the algo type for this algo.

    /**
     *
     * @return
     */
    public String get_type() {
        return (String) keyValue.get("TYPE");
    }
   
    ///Change the algo type for this algo.

    /**
     *
     * @param inp
     */
    public void set_type(final String inp) {
        keyValue.put("TYPE", inp);
    }
    ///Get the algo bits of this algo.

    /**
     *
     * @return
     */
    public Integer get_bits() {
        return (Integer) keyValue.get("BITS");
    }
    
    ///Change the algo ID of this algo.

    /**
     *
     * @param inp
     */
    public void set_bits(final int inp) {
        keyValue.put("BITS", inp);
    }

    ///Get the algo sort or deci type for this algo.

    /**
     *
     * @return
     */
    public String get_sort_deci() {
        return (String) keyValue.get("SORT_DECI");
    }
   
    /**
     * Change the algo sort or deci type for this algo.
     * @param inp 
     */
    public void set_sort_deci(final String inp) {
        keyValue.put("SORT_DECI", inp);
    }
    
    /**
     * Get the algo ID of this algo entry.
     * @return 
     */
    public Integer get_algo_id() {
        return (Integer) keyValue.get("ALGO_ID");
    }
    
    ///Change the algo ID of this algo entry.

    /**
     *
     * @param inp
     */
    public void set_algo_id(final int inp) {
        keyValue.put("ALGO_ID", inp);
    }
    
    /**
     * Attach an input to this algorithm
     * @param input 
     */
    public void add_input(TopoAlgoInput input) {
        inputs.add(input);
    }

    /**
     *
     * @param input
     */
    public void add_input(ArrayList<TopoAlgoInput> input) {
        inputs.addAll(input);
    }
    
    /**
     * Return the inputs attached to this algorithm
     * @return 
     * @throws java.sql.SQLException 
     */
    public ArrayList<TopoAlgoInput> get_inputs() throws SQLException{
        if (inputs.isEmpty()) {
            ArrayList<String> ID = new ArrayList<>();
            ID.add(String.valueOf(this.get_id()));
            ArrayList<Integer> matchedInputs = ConnectionManager.getInstance().getLinkedTable(ID,"TA_TO_TI","TOPO_ALGO_INPUT","TA2TI_ALGO_ID","TA2TI_INPUT_ID","TAI_ID","TAI_POSITION");
            for(Integer i:matchedInputs){
                TopoAlgoInput input = new TopoAlgoInput(i);
                inputs.add(input);
            }
        }
        return inputs;
    }
    
     /**
     * Attach an output to this algorithm
     * @param output 
     */
    public void add_output(TopoAlgoOutput output) {
        outputs.add(output);
    }

    /**
     *
     * @param output
     */
    public void add_output(ArrayList<TopoAlgoOutput> output) {
        outputs.addAll(output);
    }
    
    /**
     * Return the outputs attached to this algorithm
     * @return 
     * @throws java.sql.SQLException 
     */
    public ArrayList<TopoAlgoOutput> get_outputs() throws SQLException{
        if (outputs.isEmpty()) {
            ArrayList<String> ID = new ArrayList<>();
            ID.add(String.valueOf(this.get_id()));
            ArrayList<Integer> matchedOutputs = ConnectionManager.getInstance().getLinkedTable(ID,"TA_TO_TO","TOPO_ALGO_OUTPUT","TA2TO_ALGO_ID","TA2TO_OUTPUT_ID","TAO_ID","TAO_SELECTION");
            for(Integer i:matchedOutputs){
                TopoAlgoOutput output = new TopoAlgoOutput(i);
                outputs.add(output);
            }
        }
        return outputs;
    }
    
    /** Attach an parameter to this algorithm
     * @param parameter 
     */
    public void add_parameter(TopoParameter parameter) {
        parameters.add(parameter);
    }

    /**
     *
     * @param parameter
     */
    public void add_parameter(ArrayList<TopoParameter> parameter) {
        parameters.addAll(parameter);
    }
    
    /**
     * Return the parameters attached to this algorithm
     * @return 
     * @throws java.sql.SQLException 
     */
    public ArrayList<TopoParameter> get_parameters() throws SQLException{
        if (parameters.isEmpty()) {
            ArrayList<String> ID = new ArrayList<>();
            ID.add(String.valueOf(this.get_id()));
            ArrayList<Integer> matchedParameters = ConnectionManager.getInstance().getLinkedTable(ID,"TA_TO_TP","TOPO_PARAMETER","TA2TP_ALGO_ID","TA2TP_PARAM_ID","TP_ID","TP_POSITION");
            for(Integer i:matchedParameters){
                TopoParameter parameter = new TopoParameter(i);
                parameters.add(parameter);
            }
        }
        return parameters;
    }
    
    /** Attach an generic to this algorithm
     * @param generic 
     */
    public void add_generic(TopoGeneric generic) {
        generics.add(generic);
    }

    /**
     *
     * @param generic
     */
    public void add_generic(ArrayList<TopoGeneric> generic) {
        generics.addAll(generic);
    }
    
    /**
     * Return the generics attached to this algorithm
     * @return 
     * @throws java.sql.SQLException 
     */
    public ArrayList<TopoGeneric> get_generics() throws SQLException{
        if (generics.isEmpty()) {
            ArrayList<String> ID = new ArrayList<>();
            ID.add(String.valueOf(this.get_id()));
            ArrayList<Integer> matchedGenerics = ConnectionManager.getInstance().getLinkedTable(ID,"TA_TO_TG","TOPO_GENERIC","TA2TG_ALGO_ID","TA2TG_GENERIC_ID","TG_ID","TG_ID");
            for(Integer i:matchedGenerics){
                TopoGeneric generic = new TopoGeneric(i);
                generics.add(generic);
            }
        }
        return generics;
    }
    
    
    /**
     * Force this record to be read again from the database.
     * @throws java.sql.SQLException
     */
    @Override
    public void forceLoad() throws SQLException {
        ConnectionManager.getInstance().forceLoad(this);
    }
    
    @Override
    public String toString() {
        /*if (get_id() == -1) {
            return "Topo Algorithm";
        }*/
        return "TOPO ALGORITHM: ID=" + get_id() + ", Name=" + get_name() + ", Type=" + get_type()
                + ", Output=" + get_output()+ ", Sort/Deci=" + get_sort_deci() + ", AlgoId=" + get_algo_id();
    }

    @Override
    public int save() throws SQLException {
        //Temporary no check save.
        set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix, get_id(), keyValue));
        return get_id(); 
    }
    
    /**
     * 
     * @param Algos
     * @return
     * @throws SQLException 
     */
    public TreeMap<Integer, Integer> batchsave(ArrayList<TopoAlgo> Algos) throws SQLException {
        
        ArrayList<TreeMap<String, Object>> InputAlgos = new ArrayList<>();
        for(TopoAlgo algo:Algos){
            TreeMap<String, Object> table = new TreeMap<>();
            table.put("NAME", algo.get_name());
            table.put("OUTPUT", algo.get_output());
            table.put("TYPE", algo.get_type());
            table.put("BITS", algo.get_bits());
            table.put("SORT_DECI", algo.get_sort_deci());
            table.put("ALGO_ID", algo.get_algo_id());
            InputAlgos.add(table);
        }
        TreeMap<Integer, Integer> results = ConnectionManager.getInstance().saveBatch("TOPO_ALGO", tablePrefix, 0, InputAlgos);
        return results;
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj instanceof TopoAlgo) {
            if (!this.get_name().equals(((TopoAlgo) obj).get_name())) {
                return false;
            } else if (!this.get_output().equals(((TopoAlgo) obj).get_output())) {
                return false;
            } else if (!this.get_type().equals(((TopoAlgo) obj).get_type())) {
                return false;
            } else if (!this.get_bits().equals(((TopoAlgo) obj).get_bits())) {
                return false;
            } else if (!this.get_sort_deci().equals(((TopoAlgo) obj).get_sort_deci())) {
                return false;
            } else if (!this.get_algo_id().equals(((TopoAlgo) obj).get_algo_id())) {
                return false;
            } else return true;
        } else {
            return false;
        }
    }
    
    @Override
    public Object clone() {
        TopoAlgo copy = new TopoAlgo();
        copy.set_name(get_name());
        copy.set_output(get_output());
        copy.set_type(get_type());
        copy.set_bits(get_bits());
        copy.set_sort_deci(get_sort_deci());
        copy.set_algo_id(get_algo_id());
        return copy;
    }

    @Override
    public String getTableName() {
        return "TOPO_ALGO";
    }
    
    /**
     * Minimum names for the search results.
     * @return 
     */
    @Override
    public ArrayList<String> get_min_names() {
        ArrayList<String> info = new ArrayList<>();
        info.add("ID");
        info.add("Name");
        info.add("Type");
        info.add("Output");
        info.add("Sort/Deci");
        info.add("AlgoId");
        return info;
    }
    
     /**
     * Minimum information for the search results.
     * @return 
     */
    @Override
    public ArrayList<Object> get_min_info() {
        ArrayList<Object> info = new ArrayList<>();
        info.add(get_id());
        info.add(get_name());
        info.add(get_type());
        info.add(get_output());
        info.add(get_sort_deci());
        info.add(get_algo_id());

        return info;
    }
}
