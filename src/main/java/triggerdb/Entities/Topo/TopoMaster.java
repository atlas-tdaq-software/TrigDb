package triggerdb.Entities.Topo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import javax.swing.tree.DefaultMutableTreeNode;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;

/**
 * TopoMaster table row. To hold the data from TOPO_MASTER_TABLE. A Master table
 * only contains simple one-to-one links, but a lot of them.
 *
 * @author Alex Martyniuk
 */
public final class TopoMaster extends AbstractTable {

    /**
     * TopoMenu object that this TopoMaster links to.
     */
    private TopoMenu topoMenu = null;
    /**
     * The tree knows if it has loaded the branches containing TopoMenu.
     */
    private DefaultMutableTreeNode menuLayer = null;

    /**
     * Standard constructor. If an ID of -1 is supplied we create a new, empty
     * record and don't attempt to load anything from the database. If the ID
     * supplied is greater than 0 we load this record from the database.
     *
     */
    public TopoMaster() {
        super(-1,"TMT_");
        setKeys();
    }

    /**
     *
     * @param input_id
     * @throws SQLException
     */
    public TopoMaster(final int input_id) throws SQLException {
        super(input_id, "TMT_");
        setKeys();
        if (input_id > 0) {
            this.forceLoad();
        }
    }

    private void setKeys() {
        this.keyValue.putFirst("HASH", -1, "HashCode for the table");
        this.keyValue.putFirst("TRIGGER_MENU_ID", -1, "Menu ID");
        this.keyValue.putFirst("COMMENT", "", "COMMENT");
    }
    /**
     * After an edit we need to force the record to read from the database.
     * @throws java.sql.SQLException
     */
    @Override
    public void forceLoad() throws SQLException {
        menuLayer = null;
        topoMenu = null;

        ConnectionManager.getInstance().forceLoad(this);
    }


    ///Get the ID of the TopoMenu this TopoMaster uses.

    /**
     *
     * @return
     */
    public Integer get_menu_id() {
        return (Integer) keyValue.get("TRIGGER_MENU_ID");
    }
    ///Change the TopoMenu ID in this TopoMaster.

    /**
     *
     * @param inp
     */
    public void set_menu_id(final int inp) {
        keyValue.put("TRIGGER_MENU_ID", inp);
    }

    ///Load the TopoMenu object, if it hasn't been loaded before go to the DB to get it.

    /**
     *
     * @return
     * @throws SQLException
     */
    public TopoMenu get_menu() throws SQLException {
        if (topoMenu == null && new TopoMenu().isValid(get_menu_id())) {
            topoMenu = new TopoMenu(get_menu_id());
        }

        return topoMenu;
    }

    @Override
    public int doDiff(AbstractTable t, DefaultMutableTreeNode treeNode, java.util.Set<String> linkstoignore) throws SQLException {
        /// Don't consider the menu id
        if (linkstoignore == null) {
            linkstoignore = new java.util.HashSet<>();
        }
        linkstoignore.add("TRIGGER_MENU_ID");
        int ndiff = super.doDiff(t, treeNode, linkstoignore);

        if (t instanceof TopoMaster) {
            TopoMaster master = (TopoMaster) t;
            if (!Objects.equals(this.get_menu_id(), master.get_menu_id())) { //different menus
                DefaultMutableTreeNode menu = new DefaultMutableTreeNode(master.get_menu().getTableName());
                treeNode.add(menu);
                ndiff += this.get_menu().doDiff(master.get_menu(), menu, null);
            }
        }

        return ndiff;
    }

    ///Save is simple, since TopoMaster has no link tables connected to it.
    /**
     * Since TopoMaster has no link tables, we can assume the IDs in all the
     * fields are correct and do a simple save.
     *
     * @return The ID of the saved TopoMaster table record.
     * @throws java.sql.SQLException Stop when we have an SQL problem.
     */
    @Override
    public int save() throws SQLException {
        /// Don't save if only the name is different
        String name = get_name();
        keyValue.remove("NAME");
        ConnectionManager mgr = ConnectionManager.getInstance();
        ArrayList<Integer> ids = mgr.get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");

        keyValue.put("NAME", name);

        if (ids.isEmpty()) {
            set_id(mgr.save(getTableName(), tablePrefix, get_id(), keyValue));
        } else {
            set_id(ids.get(0));
        }

        return get_id();
    }

    ///String representation of the object.
    /**
     * So the tree view knows what information to display.
     *
     * @return A string containing the id, name and version of the record.
     */
    @Override
    public String toString() {
        if (get_id() == -1) {
            return "Topo Master Table";
        }

        return "TOPO MASTER: ID=" + get_id() + ", Name=" + get_name() + ", Version=" + get_version();
    }

    ///Add all the linked records to the tree.
    /**
     * Add all the children to the tree.
     * @throws java.sql.SQLException
     */
    @Override
    public void addToTree(DefaultMutableTreeNode treeNode, int counter) throws SQLException {
        --counter;

        if (counter >= 0) {

            if (menuLayer == null && get_menu() != null) {
                menuLayer = new DefaultMutableTreeNode(get_menu());
                treeNode.add(menuLayer);
                get_menu().addToTree(menuLayer, counter);
            } else if (get_menu() != null) {
                get_menu().addToTree(menuLayer, counter);
            }
        }

    }

    ///Make a copy of this TopoMasterTable.
    /**
     * Note that we can just do a simple copy of the IDs of the linked tables.
     *
     * @return A copy of this TopoMasterTable.
     */
    @Override
    public Object clone() {
        TopoMaster copy = new TopoMaster();
        copy.set_menu_id(get_menu_id());
        copy.set_name(get_name());
        copy.set_version(get_version());
        copy.set_comment(get_comment());
        copy.set_hash(get_hash());
        return copy;
    }

    ///Set the TopoMenu object.

    /**
     *
     * @param menu
     */
    public void set_topo_menu(TopoMenu menu) {
        topoMenu = menu;
    }
    
    public TopoMenu get_topo_menu() {
        return topoMenu;
    }

    ///Set the comment, a user defined (helpful) string describing the configuration.

    /**
     *
     * @param inp
     */
    public void set_comment(String inp) {
        keyValue.put("COMMENT", inp);
    }

    ///Get the comment.

    /**
     *
     * @return
     */
    public String get_comment() {
        return (String) keyValue.get("COMMENT");
    }
    
    ///Get the hash.

    /**
     *
     * @return
     */
    public Integer get_hash() {
        return (Integer) keyValue.get("HASH");
    }
    
    ///Set the hash. Details still need to be defined.

    /**
     *
     * @param inp
     */
    public void set_hash(final int inp) {
        //if(inp == 0){
        //    inp = this.get_menu().get_hash();
        //}
        keyValue.put("HASH", inp);
    }

    ///////////////////////////////////////////
    // STATIC METHODS
    ///////////////////////////////////////////
    /**
     * Query the DB for the list of Topo Master table.
     *
     * @return The list of Topo Master Tables sorted by STATUS and TopoMT_ID
     * (STATUS=1 first, then TopoMT_ID highest to lowest).
     * @throws java.sql.SQLException
     */
    public static List<TopoMaster> getTopoTables() throws SQLException {
        List<TopoMaster> l1mastertables = new ArrayList<>();
        String q0 = new TopoMaster().getQueryString();

        try {
            ConnectionManager mgr = ConnectionManager.getInstance();

            String where = " ORDER BY TMT_STATUS DESC, TMT_ID DESC ";
            String query = mgr.fix_schema_name(q0 + where);
            try (PreparedStatement ps = mgr.getConnection().prepareStatement(query); ResultSet rset = ps.executeQuery()) {
                while (rset.next()) {
                    TopoMaster master = new TopoMaster();
                    master.loadFromRset(rset);
                    l1mastertables.add(master);
                }
                rset.close();
                ps.close();
            }
        } catch (SQLException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
            throw e;
        }
        return l1mastertables;
    }

    @Override
    public String getTableName() {
        return "TOPO_MASTER_TABLE";
    }

    /**
     *
     * @return
     */
    public int findDummy() {
String query = "SELECT TMT_ID FROM TOPO_MASTER_TABLE WHERE TMT_NAME ='dummyTopo'";
        int id = -1;
        try {
            PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
            ResultSet rset = ConnectionManager.executeSelect(ps);
            while (rset.next()) {
               id=rset.getInt("TMT_ID");
            }
            rset.close();
            ps.close();
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return id;
    }
}