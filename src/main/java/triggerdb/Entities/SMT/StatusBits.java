/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package triggerdb.Entities.SMT;

/**
 *
 * This enum defines the meaning of the bits
 * in the Status variable for the SuperMasterTables
 *
 * The definitions are:
 *
 * Bit Position (from right)    | Integer val   | Name      | Use
 *          0                   |   1           | ONLINE    | 1 = Key for online running
 *          1                   |   2           | VALID     | 1 = Validated key
 *          2                   |   4           | MC        | 1 = Key for MC (offline) running
 *          3                   |   8           | REP       | 1 = Key for replication to tier 2
 *          4                   |   16          | CONS1     | seem CONS2
 *          5                   |   32          | CONS2     | 00 = consistency not run, 01 = consistency run with errors, 10 = consistency run with warnings, 11 = consistency run, no errors, no warnings
 *
 * @author markowen
 */
public enum StatusBits {

    /**
     *
     */
    ONLINE(1),

    /**
     *
     */
    VALID(2),

    /**
     *
     */
    MC(4),

    /**
     *
     */
    REP(8),
 
    /**
     *
     */
    CONS1(16), 

    /**
     *
     */
    CONS2(32);

        private final Integer val;

        StatusBits(int i) {
            val = i;
        }

    /**
     *
     * @return
     */
    public int toInt() {return val;}

        // decode consistency info

    /**
     *
     * @param theint
     * @return
     */
        static public String decodeConsistency(Integer theint) {
            if( (theint & StatusBits.CONS1.toInt()) == 0 && (theint & StatusBits.CONS2.toInt()) == 0 ) return "Not Run";
            if( (theint & StatusBits.CONS1.toInt()) == 0 && (theint & StatusBits.CONS2.toInt()) > 0 ) return "Has Errors";
            if( (theint & StatusBits.CONS1.toInt()) > 0 && (theint & StatusBits.CONS2.toInt()) == 0 ) return "Has Warnings";
            return "Good";
        }
}
