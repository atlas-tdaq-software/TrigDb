package triggerdb.Entities.SMT;

import java.sql.SQLException;
import java.util.ArrayList;

import triggerdb.Entities.AbstractTable;
import triggerdb.Connections.ConnectionManager;

///Link between the supermaster table and the release
/**
 * 
 * @author Paul Bell 6.5.2009
 */
public class SMK_TO_RE extends AbstractTable {

    ///Empty constructor.

    /**
     *
     */
    public SMK_TO_RE() {
        super(-1, "SMT2RE_");

        keyValue.putFirst("SUPER_MASTER_TABLE_ID", -1, "Supermaster ID");
        keyValue.putFirst("RELEASE_ID", -1, "Release ID");
                
        keyValue.remove("NAME");
        keyValue.remove("VERSION");
    }
    
    ///Get the SM ID.

    /**
     *
     * @return
     */
    public Integer get_supermaster_id() {
        return (Integer) keyValue.get("SUPER_MASTER_TABLE_ID");
    }
    ///Set the SM ID.

    /**
     *
     * @param inp
     */
    public void set_supermaster_id(Integer inp) {
        keyValue.put("SUPER_MASTER_TABLE_ID", inp);
    }

    ///Get the rel ID.

    /**
     *
     * @return
     */
    public Integer get_release_id() {
        return (Integer) keyValue.get("RELEASE_ID");
    }
    ///Set the rel ID.

    /**
     *
     * @param inp
     */
    public void set_release_id(Integer inp) {
        keyValue.put("RELEASE_ID", inp);
    }

    ///Save the supermaster to release link
    /**
     * Check if this link already exists before saving.  This is done at the 
     * start using the map of db field and value.
     * 
     * @return The new ID if one is assigned, or the ID of the existing record.
     * @throws java.sql.SQLException Stop if we have an SQL problem
     */
    @Override
    public int save() throws SQLException {
       
        ArrayList<Integer> ids = ConnectionManager.getInstance().get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");

        if (ids.size() <= 0) {
            set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix, get_id(), keyValue));
        } else {
            set_id(ids.get(0));
            forceLoad();
        }

        return get_id();
    }

    ///Clone is not supported here.
    @Override
    public Object clone() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getTableName() {
        return "HLT_SMT_TO_HRE";
    }
}
