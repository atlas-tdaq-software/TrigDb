package triggerdb.Entities.SMT;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import javax.swing.JOptionPane;
import javax.swing.tree.DefaultMutableTreeNode;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.HLT.HLTMaster;
import triggerdb.Entities.HLT.HLTRelease;
import triggerdb.Entities.L1.L1Master;
import triggerdb.Entities.Topo.TopoMaster;
import triggerdb.PrescaleSetAliasComponent;

/**
 * Holds the Super Master Table while we have it in memory. Also contains the
 * references to the L1 Master and HLT Master tables and a flag containing a
 * user defined status.
 *
 * @author Simon Head
 */
public final class SuperMasterTable extends AbstractTable {

    /**
     * A pointer to the L1 Master table linked to by this record
     */
    private L1Master l1_master_table = null;
   /**
     * A pointer to the Topo Master table linked to by this record
     */
    private TopoMaster topo_master_table = null;
    /**
     * A pointer to the HLT Master table linked to by this record
     */
    private HLTMaster hlt_master_table = null;
    /**
     * Used by the tree
     */
    private DefaultMutableTreeNode l1Layer = null;
   /**
     * Used by the tree
     */
    private DefaultMutableTreeNode topoLayer = null;
    /**
     * Used by the tree
     */
    private DefaultMutableTreeNode hltLayer = null;
    /**
     * Used by the tree
     */
    private ArrayList<DefaultMutableTreeNode> tree_data_rel = new ArrayList<>();
    /**
     * Used by the tree
     */
    private DefaultMutableTreeNode treeNode = null;
    /**
     * Origin of this SM e.g. copied from another DB
     */
    //private String origin = "";
    /**
     * Parent history e.g. which key was edited to give this one
     */
    //private Integer parent_key = 0;
    ///Releases to be used in conjunction
    private ArrayList<HLTRelease> releases_v = null;
    ///is this being created from the commandline
    private boolean fromcommandline = false;

    /**
     *
     */
    public SuperMasterTable() {
        super(-1, "SMT_");
        DefineKeyValuePairs();   
    }

    /**
     * Normal constructor. Mode takes one of two values, either
     * AbstractTable.NONE to read no information from the database, or
     * AbstractTable.MAX to read a single row specified by the ID. Note that
     * reading a row in at a time is very slow, so we try not to in general.
     *
     * @param input_id The ID of the record to read, -1 means don't read
     * anything.
     * @throws java.sql.SQLException
     */
    public SuperMasterTable(final int input_id) throws SQLException {
        super(input_id, "SMT_");
        DefineKeyValuePairs();        

        if (input_id > 0) {
            forceLoad();
            l1_master_table = new L1Master(get_l1_master_table_id());
        }
    }

    private void DefineKeyValuePairs() {
        keyValue.putFirst("PARENT_HISTORY_KEY", 0, "Key of parent");
        keyValue.putFirst("ORIGIN", "", "Origin");
        keyValue.putFirst("COMMENT", "", "Comment");
        keyValue.putFirst("STATUS", 0, "Status");
        keyValue.putFirst("L1_MASTER_TABLE_ID", -1, "L1 Master Table ID");
        keyValue.putFirst("TOPO_MASTER_TABLE_ID", -1, "Topo Master Table ID");
        keyValue.putFirst("HLT_MASTER_TABLE_ID", -1, "HLT Master Table ID");
    }

    /**
     * Force this row to be reloaded from the database.
     * @throws java.sql.SQLException
     */
    @Override
    public void forceLoad() throws SQLException {
        l1Layer = null;
        hltLayer = null;
        treeNode = null;
        l1_master_table = null;
        topo_master_table = null;
        hlt_master_table = null;
        tree_data_rel = new ArrayList<>();

        ConnectionManager.getInstance().forceLoad(this);
    }
    
    /**
     *
     * @param newComment
     * @throws SQLException
     */
    public void setComment(String newComment) throws SQLException {

        String comment = newComment;
        
        if (comment.length() > 1000) {
            comment = comment.substring(0, 998);
        }

        if (comment.isEmpty()) {
            comment = "~";
        }

        String query = "UPDATE SUPER_MASTER_TABLE SET SMT_COMMENT=? WHERE SMT_ID=?";
        query = ConnectionManager.getInstance().fix_schema_name(query);
        try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query)) {
            ps.setString(1, comment);
            ps.setInt(2, get_id());
            ps.executeUpdate();
            ps.close();
        }
    }

    /**
     *
     * @return
     * @throws SQLException
     */
    public String getComment() throws SQLException {
        String comment = "";
        //get the comment direct from the DB
        String query = "SELECT SMT_COMMENT FROM SUPER_MASTER_TABLE WHERE SMT_ID =?";
        query = ConnectionManager.getInstance().fix_schema_name(query);
        try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query)) {
            ps.setInt(1, get_id());
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                comment = rset.getString("SMT_COMMENT");
            }
            rset.close();
            ps.close();
        }

        return comment;
    }

    //Set but don't save the releases. Useful to attach releases to a clone.

    /**
     *
     * @param releases
     */
    public void setReleases(ArrayList<HLTRelease> releases){
        releases_v = releases;
    }
    
    /**
     * Load all the valid releases for this supermaster key.
     *
     * @return the list of releases.
     * @throws java.sql.SQLException
     */
    public ArrayList<HLTRelease> getReleases() throws SQLException {
        if (releases_v == null) {
            releases_v = new ArrayList<>();

            if (get_id() > 0) {
                String query = (new HLTRelease(-1)).getQueryString();
                query = query.replace("FROM ", "FROM SUPER_MASTER_TABLE, HLT_SMT_TO_HRE, ");

                String and = " WHERE "
                        + "SMT_ID=? "
                        + "AND "
                        + "SMT2RE_SUPER_MASTER_TABLE_ID=SMT_ID "
                        + "AND "
                        + "SMT2RE_RELEASE_ID=HRE_ID "
                        + "ORDER BY HRE_NAME";

                query = ConnectionManager.getInstance().fix_schema_name(query + and);
                try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query)) {
                    ps.setInt(1, get_id());
                    ResultSet rset;
                    rset = ps.executeQuery();
                    while (rset.next()) {
                        HLTRelease rel = new HLTRelease(-1);
                        rel.loadFromRset(rset);
                        releases_v.add(rel);
                    }

                    rset.close();
                    ps.close();
                }
            }
        }
        return releases_v;
    }

    /**
     * Return the level one Master Table ID.
     *
     * @return L1 Master Table ID.
     */
    public Integer get_l1_master_table_id() {
        return (Integer) keyValue.get("L1_MASTER_TABLE_ID");
    }
    
        /**
     * Return the Topo Master Table ID.
     *
     * @return Topo Master Table ID.
     */
    public Integer get_topo_master_table_id() {
        return (Integer) keyValue.get("TOPO_MASTER_TABLE_ID");
    }

    /**
     * Return the HLT Master Table ID.
     *
     * @return HLT Master Table ID.
     */
    public Integer get_hlt_master_table_id() {
        return (Integer) keyValue.get("HLT_MASTER_TABLE_ID");
    }

    /**
     * Get a pointer to the L1 master record corresponding to the master table
     * ID in this record.
     *
     * @return Pointer to L1 Master Table.
     * @throws java.sql.SQLException
     */
    public L1Master get_l1_master_table() throws SQLException {
        if(l1_master_table == null){
            l1_master_table = new L1Master(get_l1_master_table_id());
        }
        return l1_master_table;
    }
    
   /**
     * Get a pointer to the Topo master record corresponding to the master table
     * ID in this record.
     *
     * @return Pointer to Topo Master Table.
     * @throws java.sql.SQLException
     */
    public TopoMaster get_topo_master_table() throws SQLException {
        if (topo_master_table == null) {
            topo_master_table = new TopoMaster(get_topo_master_table_id());
        }
        return topo_master_table;
    }

    /**
     * Get a pointer to the hlt master record corresponding to the master table
     * id in this record.
     *
     * @return Pointer to HLT Master Table.
     * @throws java.sql.SQLException
     */
    public HLTMaster get_hlt_master_table() throws SQLException {
        if (hlt_master_table == null) {
            hlt_master_table = new HLTMaster(get_hlt_master_table_id());
        }
        return hlt_master_table;
    }

    /**
     * set if this is being created from the command line.
     */
    public void set_fromcommandline() {
        fromcommandline = true;
    }

    public boolean get_fromcommandline() {
        return fromcommandline;
    }
    /**
     * Set the pointer to the L1 Master Table. Note that this doesn't update the
     * value of the L1 master table itself. Maybe that's not so nice?
     *
     * @param mast New L1 Master Table.
     */
    public void set_l1_master_table(final L1Master mast) {
        l1_master_table = mast;
    }
    
   /**
     * Set the pointer to the Topo Master Table. Note that this doesn't update the
     * value of the Topo master table itself. Maybe that's not so nice?
     *
     * @param mast New Topo Master Table.
     */
    public void set_topo_master_table(final TopoMaster mast) {
        topo_master_table = mast;
    }

    /**
     * set the origin.
     * @param origin
     */
    public void set_origin(final String origin) {
        keyValue.put("ORIGIN", origin);
    }

    /**
     * get the origin.
     * @return 
     */
    public String get_origin() {
        return (String) keyValue.get("ORIGIN");
    }

    /**
     * set the parent key
     * @param inp
     */
    public void set_parent_key(final Integer inp) {
        keyValue.put("PARENT_HISTORY_KEY", inp);
    }

    /**
     * get the parent key.
     * @return 
     */
    public Integer get_parent_key() {
        return (Integer) keyValue.get("PARENT_HISTORY_KEY");
    }

    /**
     * Get the value of the status field. See StatusBits class for definition.
     *
     * @return The value of the status field.
     */
    public Integer get_status() {
        return (Integer) keyValue.get("STATUS");
    }

    /**
     * Set the value of the status field. See StatusBits class for definition.
     * In general status bits should be changed with various set* methods.
     *
     * @param inp The new value for status.
     */
    public void set_status(final int inp) {
        keyValue.put("STATUS", inp);
    }

    /**
     * Set the pointer to the L1 Master Table. Note that this doesn't update the
     * value of the HLT master table itself. Maybe that's not so nice?
     *
     * @param mast New HLT Master Table.
     */
    public void set_hlt_master_table(final HLTMaster mast) {
        hlt_master_table = mast;
    }

    /**
     * Set the L1 Master Table ID.
     *
     * @param inp The L1 Master Table ID.
     */
    public void set_l1_master_table_id(final int inp) {
        keyValue.put("L1_MASTER_TABLE_ID", inp);
    }
    
    /**
     * Set the Topo Master Table ID.
     *
     * @param inp The Topo Master Table ID.
     */
    public void set_topo_master_table_id(final int inp) {
        keyValue.put("TOPO_MASTER_TABLE_ID", inp);
    }

    /**
     * Set the HLT Master Table ID.
     *
     * @param inp The HLT Master Table ID.
     */
    public void set_hlt_master_table_id(final int inp) {
        keyValue.put("HLT_MASTER_TABLE_ID", inp);
    }

    /**
     * Add the L1 Master and the HLT master tables to the tree.
     * Need to check how to put the Topo Master in here sensibly.
     * @throws java.sql.SQLException
     */
    @Override
    public void addToTree(final DefaultMutableTreeNode treeNode, int counter) throws SQLException {
        this.treeNode = treeNode;
        --counter;

        if (counter >= 0) {
            if (l1Layer == null && get_l1_master_table() != null) {
                l1Layer = new DefaultMutableTreeNode(get_l1_master_table());
                treeNode.add(l1Layer);
                L1Master master = get_l1_master_table();
                master.addToTree(l1Layer, counter);
            } else if (get_l1_master_table() != null) {
                treeNode.add(l1Layer);
            }
            
            if (topoLayer == null && get_topo_master_table() != null) {
                topoLayer = new DefaultMutableTreeNode(get_topo_master_table());
                treeNode.add(topoLayer);
                TopoMaster master = get_topo_master_table();
                master.addToTree(topoLayer, counter);
            } else if (get_topo_master_table() != null) {
                treeNode.add(topoLayer);
            }

            if (hltLayer == null && get_hlt_master_table() != null) {
                hltLayer = new DefaultMutableTreeNode(get_hlt_master_table());
                treeNode.add(hltLayer);
                HLTMaster master = get_hlt_master_table();
                master.addToTree(hltLayer, counter);
            } else if (get_hlt_master_table() != null) {
                treeNode.add(hltLayer);
            }

            // Trigger Stream
            if (tree_data_rel.isEmpty()) {
                for (HLTRelease release : getReleases()) {
                    DefaultMutableTreeNode anotherLayer = new DefaultMutableTreeNode(release);
                    treeNode.add(anotherLayer);
                    tree_data_rel.add(anotherLayer);
                    release.addToTree(anotherLayer, counter);
                }
            } else {
                int i = 0;
                for (HLTRelease release : getReleases()) {
                    release.addToTree(tree_data_rel.get(i), counter);
                    ++i;
                }
            }
        }
    }

    /**
     * Method is used to create a copy of this object.
     *
     * @return Copy of this object.
     */
    @Override
    public Object clone() {
        SuperMasterTable copy = new SuperMasterTable();
        copy.set_name(get_name());
        copy.set_version(get_version());
        copy.set_l1_master_table_id(get_l1_master_table_id());
        copy.set_topo_master_table_id(get_topo_master_table_id());
        copy.set_hlt_master_table_id(get_hlt_master_table_id());
        copy.set_parent_key(get_parent_key());
        copy.set_origin(get_origin());
        copy.set_comment(get_comment());
        copy.set_status(get_status());
        /*try {
                copy.setReleases(getReleases());
        } catch (SQLException ex) {
            Logger.getLogger(SuperMasterTable.class.getName()).log(Level.SEVERE, null, ex);
        }*/
        return copy;
    }

    /**
     * Routine to record the object in the database. Does a very simple check of
     * all the fields (not ID, version, used, username). If an existing record
     * matches then we use that. Otherwise we write a new one.
     *
     * @return The ID of the recorded record.
     * @throws SQLException Stop on SQL problem.
     */
    @Override
    public int save() throws SQLException {

        int matchingID = -1;

        String name = get_name();
        String origin = get_origin();
        Integer parent_key = get_parent_key();
        String comment = get_comment();
        Integer status = get_status();

        logger.log(Level.FINE, "In Super Master Save for SM: {0} comment  {1}", new Object[]{name, comment});

        //step 0: look for candidates with same properties - don't want to compare comment or origin
        keyValue.remove("PARENT_HISTORY_KEY");
        keyValue.remove("ORIGIN");
        keyValue.remove("COMMENT");
        keyValue.remove("STATUS");

        ConnectionManager cm = ConnectionManager.getInstance();
        ArrayList<Integer> candidate_ids = cm.get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");
        logger.log(Level.FINE, "Number of candidate SM keys) : {0}", candidate_ids.size());

        //Now loop over the candidates
        for (Integer sm_id : candidate_ids) {
            //if we got to here, we have a match so stop
            matchingID = sm_id;
            break;
        }

        //put these back now
        keyValue.put("PARENT_HISTORY_KEY", parent_key);
        keyValue.put("ORIGIN", origin);
        keyValue.put("STATUS", status);
        keyValue.put("COMMENT", comment);

        //save new SMK if required otherwise just set_id to matched SMK
        if (matchingID < 0) {
            //set version
            int version = 1 + cm.getMaxVersion(getTableName(), tablePrefix, get_name());
            keyValue.put("VERSION", version);
            set_id(cm.save(getTableName(), tablePrefix, get_id(), keyValue));
        } else {
            set_id(matchingID);
        }
        
        //create output for log file and on screen dialog
        String outputLOG = "Save complete \n";
        outputLOG += "-----------------------------------" + "\n";
        if (matchingID < 0) {
            outputLOG += "Saved new SuperMasterTable with key: " + get_id() + "\n";
        }else{
            outputLOG += "New configuration is identical to the SuperMasterTable with key: " + get_id() + "\n";
        }
        outputLOG += "-----------------------------------" + "\n";
        
        //check which DB before adding relevant output
        String dbname = cm.getInitInfo().getUserName();
        if(dbname.equals("ATLAS_CONF_TRIGGER_REPR_W") || dbname.equals("ATLAS_CONF_TRIGGER_RUN2_MC_W")){
            //menu experts only need the SMK when using the MC and reprocessing DB's
            outputLOG += "For prescale numbers either:" + "\n";
            outputLOG += "- check the prescale panel for default keys (all items enabled)" + "\n";
            outputLOG += "- or upload the relevant prescale xmls to get the desired prescale keys" + "\n";
            outputLOG += "-----------------------------------" + "\n";
        }else{
            //L1 experts should pass on menu experts only need the SMK when using the MC and reprocessing DB's
            //   could choose to be more specific - but for now this message is used for all other DB's
            outputLOG += "For new L1 menu uploads please pass on these ID's to help the menu expert upload:" + "\n";
            outputLOG += "- L1 Master  : " + get_l1_master_table_id() + "\n";
            outputLOG += "- Topo Master  : " + get_topo_master_table_id() + "\n";
            outputLOG += "-----------------------------------" + "\n";
        }
        //if not running only in CL mode update the output and display a dialog for experts
        if (!fromcommandline) {
            JOptionPane.showMessageDialog(null, outputLOG, "Save Complete", JOptionPane.INFORMATION_MESSAGE);
        }
        //logfile message kept as with dialog incase running from CL only
        //other details of table and menu ID's are quoted earlier in log messages
        logger.info(outputLOG);

        //add memory usage details to log - not really needed so could downgrade to fine
        long heapTotalMemory = Runtime.getRuntime().totalMemory();
        long heapFreeMemory = Runtime.getRuntime().freeMemory();
        outputLOG = " Memory usage: ";
        outputLOG += "heapTotalMemory = " + heapTotalMemory / (1024 * 1024) + " MB, ";
        outputLOG += "heapFreeMemory = " + heapFreeMemory / (1024 * 1024) + " MB";
        logger.info(outputLOG);
        
        return this.get_id();
    }

    /**
     *
     * @return
     * @throws SQLException
     */
    public ArrayList<Integer> getAllSMKs() throws SQLException {

        ArrayList<Integer> results = new ArrayList<>();

        String query = "SELECT SMT_ID FROM SUPER_MASTER_TABLE";

        query = ConnectionManager.getInstance().fix_schema_name(query);
        try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query)) {
            ResultSet rset;
            rset = ps.executeQuery();
            while (rset.next()) {
                results.add(rset.getInt(1));
            }

            rset.close();
            ps.close();
        }
        return results;
    }
    
    /**
     *
     * @return
     * @throws SQLException
     */
    public ArrayList<String> getAllSMKsAndNames() throws SQLException {

        ArrayList<String> results = new ArrayList<>();

        String query = "SELECT SMT_ID,SMT_NAME FROM SUPER_MASTER_TABLE";

        query = ConnectionManager.getInstance().fix_schema_name(query);
        try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query)) {
            ResultSet rset;
            rset = ps.executeQuery();
            while (rset.next()) {
                results.add(rset.getInt("SMT_ID") + ": " + rset.getString("SMT_NAME"));
            }

            rset.close();
            ps.close();
        }
        return results;
    }
    
    /**
     * So the tree view knows what information to display in the tree.
     *
     * @return A string containing the id, name and version of the record.
     */
    @Override
    public String toString() {
        if (get_id() == -1) {
            return "Super Master Table";
        }
        return get_id() + ": " + get_name() + " v" + get_version();

    }

    @Override
    public int doDiff(final AbstractTable t,
            final DefaultMutableTreeNode treeNode,
            java.util.Set<String> linkstoignore) throws SQLException {
        if (linkstoignore == null) {
            linkstoignore = new java.util.HashSet<>();
        }
        linkstoignore.add("L1_MASTER_TABLE_ID");
        linkstoignore.add("TOPO_MASTER_TABLE_ID");
        linkstoignore.add("HLT_MASTER_TABLE_ID");
        int ndiff = super.doDiff(t, treeNode, linkstoignore);

        if (t instanceof SuperMasterTable) {
            SuperMasterTable smt = (SuperMasterTable) t;
            if (!Objects.equals(this.get_l1_master_table_id(), smt.get_l1_master_table_id())) { // L1 is different
                DefaultMutableTreeNode l1 = new DefaultMutableTreeNode(smt.get_l1_master_table().getTableName());
                treeNode.add(l1);
                this.get_l1_master_table().doDiff(smt.get_l1_master_table(), l1, null);
            }
            if (!Objects.equals(this.get_topo_master_table_id(), smt.get_topo_master_table_id())) { // L1 is different
                DefaultMutableTreeNode topo = new DefaultMutableTreeNode(smt.get_topo_master_table().getTableName());
                treeNode.add(topo);
                this.get_topo_master_table().doDiff(smt.get_topo_master_table(), topo, null);
            }
            if (!Objects.equals(this.get_hlt_master_table_id(), smt.get_hlt_master_table_id())) { ///HLT is different
                DefaultMutableTreeNode hlt = new DefaultMutableTreeNode(smt.get_hlt_master_table().getTableName());
                treeNode.add(hlt);
                this.get_hlt_master_table().doDiff(smt.get_hlt_master_table(), hlt, null);
            }

        }

        return ndiff;
    }

    /**
     * Set the comment.
     *
     * @param inp The new comment.
     */
    public void set_comment(final String inp) {
        keyValue.put("COMMENT", inp);
    }

    /**
     * Get the value of comment.
     *
     * @return The value of comment.
     */
    public String get_comment() {
        if (keyValue.get("COMMENT") == null) {
            return "null comment";
        } else {
            return (String) keyValue.get("COMMENT");
        }
    }

    @Override
    public boolean equals(final Object other) {
        if (!(other instanceof SuperMasterTable)) {
            return false;
        }

        return getSearchData().equals(((SuperMasterTable) other).getSearchData());
    }

    @Override
    public int hashCode() {
        return keyValue.hashCode(); //abstractmap - may not be safe!
    }

    /**
     * Minimum names for the search results.
     * @return 
     */
    @Override
    public ArrayList<String> get_min_names() {
        ArrayList<String> info = new ArrayList<>();
        info.add("ID");
        info.add("Name");
        info.add("Version");
        info.add("Comment");
        info.add("Origin");
        info.add("Releases");
        info.add("Status");
        info.add("Menu Consistency");
        //info.add("Status of RPC/TGC");
        info.add("Creator");
        info.add("Date");
        return info;
    }

    /**
     * Minimum information for the search results.
     * @return 
     */
    @Override
    public ArrayList<Object> get_min_info() {
        ArrayList<Object> info = new ArrayList<>();
        info.add(get_id());
        info.add(get_name());
        info.add(get_version());
        String comment = get_comment();
        info.add(comment.equals("~") ? "" : comment);
        //Origin
        String origin = get_origin();
        if (origin.equals("EVOLUTION")) {
            info.add("evolution of " + get_parent_key());
        } else {
            info.add(origin.toLowerCase());
        }
        //Releases
        String releases = "";
        try {
            for (HLTRelease rel : getReleases()) {
                releases += rel.get_name() + ", ";
            }
        } catch (SQLException ex) {
            logger.log(Level.WARNING, "Release name not set.", ex);
        }
        if (releases.isEmpty()) {
            releases = "none";
        } else {
            releases = releases.substring(0, releases.length() - 2);
        }
        info.add(releases);

        //Status
        String status = "";
        if (isOnline()) {
            status += "OnL, ";
        }
        if (isValid()) {
            status += "Val, ";
        }
        if (isMC()) {
            status += "MC, ";
        }
        if (isRep()) {
            status += "Rep, ";
        }
        status = status.trim();
        if (status.endsWith(",")) {
            status = status.substring(0, status.length() - 1);
        }
        info.add(status);

        String consistency = StatusBits.decodeConsistency(get_status());
        info.add(consistency);

        //if (statusMuonConfiguration) {
        //    info.add("<html><font color='#00BF00'>ready");
        //} else {
        //    info.add("<html><font color='#FF0000'>not ready");
        //}

        //Username and date
        info.add(get_username());
        info.add(get_modified());

        return info;
    }

    /**
     * Returns true if the ONLINE status bit is true.
     *
     * @return
     */
    public boolean isOnline() {
        return (get_status() & StatusBits.ONLINE.toInt()) > 0;
    }

    /**
     * Returns true is the VALID status bit is true.
     *
     * @return
     */
    public boolean isValid() {
        return (get_status() & StatusBits.VALID.toInt()) > 0;
    }

    /**
     * Returns true is the MC status bit is true.
     *
     * @return
     */
    public boolean isMC() {
        return (get_status() & StatusBits.MC.toInt()) > 0;
    }

    /**
     * Returns true is the REP status bit is true.
     *
     * @return
     */
    public boolean isRep() {
        return (get_status() & StatusBits.REP.toInt()) > 0;
    }

    /**
     * Get the status of one of the StatusBits
     *
     * @param thebit - status bit to query
     * @return status of the bit (true or false)
     */
    public boolean getStatus(final StatusBits thebit) {
        return (get_status() & thebit.toInt()) > 0;
    }

    /**
     * Set the status of one of the StatusBits
     *
     * @param thebit - the bit to set
     * @param sw - what to set the bit to (true or false)
     */
    public void setStatus(final StatusBits thebit, final boolean sw) {
        if (sw && !getStatus(thebit)) {
            set_status(get_status() | thebit.toInt());
        }
        if (!sw && getStatus(thebit)) {
            set_status(get_status() ^ thebit.toInt());
        }
    }

    /**
     *
     * @param sw
     */
    public void setValid(final boolean sw) {
        setStatus(StatusBits.VALID, sw);
    }

    /**
     *
     * @param sw
     */
    public void setOnline(final boolean sw) {
        setStatus(StatusBits.ONLINE, sw);
    }

    /**
     *
     * @param sw
     */
    public void setMC(final boolean sw) {
        setStatus(StatusBits.MC, sw);
    }

    /**
     *
     * @param sw
     */
    public void setRep(final boolean sw) {
        setStatus(StatusBits.REP, sw);
    }

    /**
     * Set the values of the consistency bits. It will update the DB record if
     * we have a valid ID (i.e. when we run from MainPanel, not NewViewer).
     *
     * @param ran - have we run the consistency checker?
     * @param haderrors - did the results have errors?
     * @param hadwarnings - did the results have warnings?
     * @throws java.sql.SQLException
     */
    public void setConsistencyResult(final boolean ran, final boolean haderrors,
            final boolean hadwarnings) throws SQLException {
        if (!ran) {
            setStatus(StatusBits.CONS1, false);
            setStatus(StatusBits.CONS2, false);
        } else {
            if (haderrors) {
                setStatus(StatusBits.CONS1, false);
                setStatus(StatusBits.CONS2, true);
            } else if (hadwarnings) {
                setStatus(StatusBits.CONS1, true);
                setStatus(StatusBits.CONS2, false);
            } else {
                setStatus(StatusBits.CONS1, true);
                setStatus(StatusBits.CONS2, true);
            }

        }//consistency was run

        if (get_id() > 0) { //only update if it is real DB record
            Integer status_i = get_status();
            //save to database - edit property of this sm!
            String query = "UPDATE SUPER_MASTER_TABLE SET SMT_STATUS=? WHERE SMT_ID=?";
            query = ConnectionManager.getInstance().fix_schema_name(query);
            try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query)) {
                ps.setInt(1, status_i);
                ps.setInt(2, this.get_id());
                ps.executeUpdate();
                ps.close();
            }
        }//id > 0
    }

    @Override
    public String getTableName() {
        return "SUPER_MASTER_TABLE";
    }
    
    /**
     * Query the DB for the list of Prescale Set Aliases linked to this SMT.
     * @return the list of prescale set alias.
     * @throws java.sql.SQLException
     */
    public List<PrescaleSetAliasComponent> getAliases() throws SQLException{        
        return PrescaleSetAliasComponent.getAliasList(this.get_id());
    }
    
    /**
     *
     * @param value
     * @param smt_id
     * @throws SQLException
     */
    public void UpdateSmtUsed(int value, int smt_id) throws SQLException {
        String update = "UPDATE SUPER_MASTER_TABLE SET SMT_USED= " +  value + " WHERE SMT_ID=?";

        update = ConnectionManager.getInstance().fix_schema_name(update);
        try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(update)) {
            ps.setInt(1, smt_id);
            ps.executeUpdate();
            ps.close();
        }
    }

    /**
     *
     * @param releases
     * @throws SQLException
     */
    public void save_releases(List<HLTRelease> releases) throws SQLException {
        for(HLTRelease release:releases){
            SMK_TO_RE link = new SMK_TO_RE();
            link.set_release_id(release.get_id());
            link.set_supermaster_id(this.get_id());
            link.save();
        }
    }

}
