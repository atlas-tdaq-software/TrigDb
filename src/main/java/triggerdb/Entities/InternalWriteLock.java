package triggerdb.Entities;

import triggerdb.Connections.ConnectionManager;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.regex.Pattern;
import java.util.Enumeration;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import triggerdb.UploadException;

/**
 *
 * @author Miroslav Nozicka
 */
public final class InternalWriteLock extends AbstractTable {
    
    /** Number of tries to lock the DB before returning an error. */
    private static int TRY_LOCKDB_N = 1;
    /** Time to wait between DB lock intervals in seconds.*/
    private static int TRY_LOCKDB_TIME = 3;

    /**
     * Set the number of lock db tries.
     * @param N the number of lock db tries.
     */
    public static void setTRY_LOCKDB_N(int N) {
        InternalWriteLock.TRY_LOCKDB_N = N;
    }

    /**
     * set time to wait between lock db tries.
     * @param T time to wait between consecutive lock db tries.
     */
    public static void setTRY_LOCKDB_TIME(int T) {
        InternalWriteLock.TRY_LOCKDB_TIME = T;
    }
    /** Singleton pointer. */
    private static InternalWriteLock intWriteLock = null;

    /**
     * Normal constructor.  Tell it to load a specific ID, or set the ID=-1 if
     * you wish to create a new record.
     *
     */
    public InternalWriteLock() {
        super("TW_");
        setKeys();

        // User name is one of the key values for identification
        String userName = ConnectionManager.getInstance().getInitInfo().getUserName();
        keyValue.putFirst("USERNAME", userName, "Username");
        // Set the process if not loaded from DB
        set_process(getProcessString());
    }

    /**
     *
     * @param inputId
     * @throws SQLException
     */
    public InternalWriteLock(final int inputId) throws SQLException {
        super(inputId, "TW_");
        setKeys();

        if (inputId > 0) {
            forceLoad();
        } else {
            // User name is one of the key values for identification
            String userName = ConnectionManager.getInstance().getInitInfo().getUserName();
            keyValue.putFirst("USERNAME", userName, "Username");
            // Set the process if not loaded from DB
            set_process(getProcessString());
        }
    }

    private void setKeys() {
        keyValue.remove("VERSION");
        keyValue.remove("NAME");
        keyValue.remove("USED");

        keyValue.putFirst("PROCESS", "", "Process");
        keyValue.putFirst("DESCRIPTION", "", "Description");
    }
    
    
    /**
     * Obtain system variables to construct the process description Should be
     * in format [sysUser]@[hostname].[domain];IP:[ip address].
     *
     * @return The String to write in the TW_PROCESS field.
     */
    public static String getProcessString() {
        // Get System username
        String sysUserName = System.getProperty("user.name");

        // Get the local hostname
        String localHostName = "localhost";
        String localIP = "0.0.0.0";
        try {
            localHostName = java.net.InetAddress.getLocalHost().getHostName();
            localIP = java.net.InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
        }

        Pattern hostPattern = Pattern.compile("(([\\w\\-]+)(\\.+)){2,}(\\w+)");
        Pattern ipPattern = Pattern.compile("((\\d+)(\\.+)){3}(\\d+)");

        // Successfully matching local hostname
        if (hostPattern.matcher(localHostName).matches()
                && ipPattern.matcher(localIP).matches()) {
            //logger.info("Return string: " + sysUserName + "@" + localHostName
            //        + "; IP: " + localIP);
            return (sysUserName + "@" + localHostName + "; IP: " + localIP);
        }

        // Local name does not match - search the active interfaces
        String host = "";
        String ip = "";
        try {
            for (Enumeration<NetworkInterface> ifaces = java.net.NetworkInterface.getNetworkInterfaces(); ifaces.hasMoreElements();) {
                NetworkInterface iface = ifaces.nextElement();
                // Consider only active and non virtual interfaces
                // Following line wokrs only in Java 1.6
                // if (iface.isUp()&& !iface.isVirtual()) {
                for (Enumeration<InetAddress> inetAddrss = iface.getInetAddresses(); inetAddrss.hasMoreElements();) {
                    InetAddress inetAddr = inetAddrss.nextElement();
                    String hostName = inetAddr.getHostName();
                    String ipAddress = inetAddr.getHostAddress();
                    if (hostPattern.matcher(hostName).matches() && ipPattern.matcher(ipAddress).matches()) {
                        if ((host.isEmpty() && ip.isEmpty()) || hostName.startsWith(localHostName)) {
                            host = hostName;
                            ip = ipAddress;
                        }
                    }
                }
            }

        } catch (SocketException e) {
        }

        if (host.isEmpty()) {
            host = localHostName;
        }
        if (ip.isEmpty()) {
            ip = localIP;
        }

        return (sysUserName + "@" + host + "; IP: " + ip);

    }

    /**
     * Get the process
     *
     * @return The process string.
     */
    public String get_process() {
        return (String) keyValue.get("PROCESS");
    }

    /**
     * Set the process.
     *
     * @param process
     */
    public void set_process(final String process) {
        keyValue.put("PROCESS", process);
    }

    /**
     * Get the description.
     * @return the description of the lock.
     */
    public String getDescription() {
        return (String) keyValue.get("DESCRIPTION");
    }

    /**
     * Set the process.
     * @param description the new description to set.
     */
    public void setDescription(final String description) {
        keyValue.put("DESCRIPTION", description);
    }

    /**
     * Set the internal lock if possible.
     *
     * @return <code>true</code> if lock set.
     * @throws triggerdb.UploadException
     */
    public synchronized Boolean setLock() throws UploadException {
        try {
            int tries = TRY_LOCKDB_N;

            while (this.isLocked() && tries > 0) {
                String usr = this.get_username();
                tries--;
                if (tries > 0) {
                    logger.log(Level.WARNING, " DB is already locked by {0}. I will wait {1} sec and try again {2} times.", new Object[]{usr, TRY_LOCKDB_TIME, tries});
                    this.wait(TRY_LOCKDB_TIME * 1000);
                }
            }
            if (this.isLocked()) {
                String message = "Can't obtain write lock - existing lock from " + get_username() + " found";
                logger.warning(message);
                throw new UploadException(message);
            }
        } catch (SQLException | InterruptedException e) {
            logger.log(Level.WARNING, "Can not set the lock:{0}", e.getMessage());
            System.exit(-1);
        }
        int id = 0;
        // Save the lock
        try {
            id = save();
        } catch (SQLException e) {
            if (e.getErrorCode() == 1031) {
                logger.severe("No write permissions to this database with this account");
                System.exit(-1);
            } else {
                logger.log(Level.WARNING, "Can not save the lock:{0}", e.getMessage());
            }
        }
        return id == 1;
    }

    /**
     * Set the lock with description.
     *
     * @param description
     * @return
     * @throws SQLException
     * @throws triggerdb.UploadException
     */
    public Boolean setLock(String description) throws SQLException, UploadException {
        setDescription(description);
        return setLock();
    }

    // Remove the internal lock

    /**
     *
     * @return
     * @throws SQLException
     */
    public Boolean removeLock() throws SQLException {
        int id = get_id();
        ConnectionManager cn = ConnectionManager.getInstance();
        if (id > 0) {
            try {
                String query = "DELETE FROM TT_WRITELOCK WHERE ";
                query += tablePrefix + "ID=" + id;
                int ndel;
                PreparedStatement ps = cn.getConnection().prepareStatement(cn.fix_schema_name(query));
                ndel = ps.executeUpdate();
                ps.close();
                if (ndel > 0) {
                    set_id(-1);
                }
                return (ndel > 0);
            } catch (SQLException e) {
                logger.log(Level.SEVERE, "Can not remove lock ID {0}: {1}", new Object[]{id, e.getMessage()});
                return false;
            }
        }

        // In case there is no id -test if a lock is present
        try {
            if (!isLocked()) {
                logger.fine("No internal lock present");
                return true;
            }
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Caught:{0}", e.getMessage());
            System.exit(-1);
        }

        // Get the existing ID's
        ArrayList<Integer> ids;
        try {
            ids = cn.get_allIDs(getTableName(), tablePrefix);
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Can not get existing lock IDs: {0}", e.getMessage());
            return false;
        }

        int nremovedLocks = 0;
        int nIdenticLocks = 0;
        String query = "DELETE FROM TT_WRITELOCK WHERE ";
        query += tablePrefix + "ID in (" + id;
        // Compare key values against the existing if identical - remove
        for (Integer lockID : ids) {
            InternalWriteLock existingLock = new InternalWriteLock(lockID);
            if (existingLock.get_process().equals(get_process())
                    && existingLock.get_username().equals(get_username())) {
                query += lockID + ",";
                nIdenticLocks++;
            } else {
                logger.warning("Different Lock found - if necessary use forceRemove");
            }

        }
        if (nIdenticLocks > 0) {
            // Remove the last character from the query
            query = query.substring(0, query.length());
            query += ")";
            try (PreparedStatement ps = cn.getConnection().prepareStatement(cn.fix_schema_name(query))) {
                logger.fine(ps.toString());
                nremovedLocks = ps.executeUpdate();
                ps.close();
            }
            catch (SQLException e) {
                logger.log(Level.SEVERE, "Can not remove lock ID {0}: {1}", new Object[]{id, e.getMessage()});
            }

            if (nremovedLocks == nIdenticLocks) {
                set_id(-1);
                return true;
            }
        }

        return false;
    }

    // Force remove the internal lock

    /**
     *
     * @return
     * @throws SQLException
     */
    public Boolean forceRemoveLock() throws SQLException {
        int id = 0;
        try {
            ConnectionManager cn = ConnectionManager.getInstance();
            ArrayList<Integer> ids = cn.get_allIDs(getTableName(), tablePrefix);
            if (ids.size() < 1) {
                logger.fine("No internal lock present");
                return true;
            }
            // Create SQL query for delete of all locks
            String query = "DELETE FROM TT_WRITELOCK WHERE ";
            query += tablePrefix + "ID IN (";
            // Loop through the internal locks - there should only be 1
            for (Integer lockId : ids) {
                query += lockId;
            }
            query += ")";

            PreparedStatement ps = cn.getConnection().prepareStatement(cn.fix_schema_name(query));
            logger.fine(query);
            int ndel = ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Can not remove the lock: {0}", e.getMessage());
            return false;
        }
        set_id(0);
        // Test if the forceRemove was successfull
        return !isLocked();
    }

    /** 
     * Get the lock
     * 
     * @return <code>true</code> if DB already locked else <code>false</code>.
     * @throws SQLException 
     */
    public Boolean isLocked() throws SQLException {
        int id = 0;
        try {
            ArrayList<Integer> ids = ConnectionManager.getInstance().get_allIDs(getTableName(), tablePrefix);
            if (ids.size() >= 1) {
                id = ids.get(0);
                if (ids.size() > 1) {
                    logger.log(Level.SEVERE, "DB inconsistent: Multiple entries in TT_WRITELOCK table: {0}", ids.toString());
                }
            }
        } catch (SQLException e) {
            logger.log(Level.INFO, "Can not get the TT_WRITELOCK ID: {0}", e.getMessage());
            throw e;
        }

        return (id != 0);
    }

    /**
     * This needs to be a singleton so we can manage the connection
     * to the database properly.
     *
     * @return Instance of the singleton
     * @throws java.sql.SQLException
     */
    public static synchronized InternalWriteLock getInstance() throws SQLException {
        if (intWriteLock == null) {
            intWriteLock = new InternalWriteLock(0);
        }
        return intWriteLock;
    }

    /**
     * Convert the information to String for pretty printout to console.
     *
     * @return information about the write lock.
     */
    @Override
    public String toString() {
        String output = "Trigger Tool Internal Write Lock:\n";
        if (get_id() > 0) {
            output += "\tID:\t" + get_id() + "\n";
        }
        output += "\tUser:\t" + get_username() + "\n";
        output += "\tProcess:\t" + get_process() + "\n";
        output += "\tStart time:\t" + get_modified().toString() + "\n";
        output += "\tDescription:\t" + getDescription() + "\n";
        return output;
    }

    /**
     * Save the table information.
     *
     * @return the id of the saved row.
     * @throws SQLException
     */
    @Override
    public int save() throws SQLException {
        ArrayList<Integer> ids = ConnectionManager.getInstance().get_IDs(getTableName(),
                tablePrefix, keyValue, null, "ID");
        if (ids.isEmpty()) {
            set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix,
                    get_id(), keyValue));
        } else {
            set_id(ids.get(0));
        }
        return get_id();
    }

    /**
     * Clone the table information.
     *
     * @return a new object with the same data as <code>this</code>.
     */
    @Override
    public Object clone(){
        InternalWriteLock copy = new InternalWriteLock();
        copy.setDescription(getDescription());
        copy.set_process(get_process());
        return copy;
    }

    @Override
    public String getTableName() {
        return "TT_WRITELOCK";
    }
}
