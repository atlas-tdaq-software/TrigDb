package triggerdb.Entities;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.tree.DefaultMutableTreeNode;
import triggerdb.Connections.ConnectionManager;
import triggerdb.DiffMutableTreeNode;
import triggerdb.TriggerMap;

///Base class for all the tables in the TriggerTool.
/**
 * All tables should inherit from this class. Contains common functionality to
 * display in the search box on the main screen
 *
 * @author Paul Bell
 * @author Simon Head
 */
public abstract class AbstractTable implements Comparable<AbstractTable> {

    /**
     * Message Log.
     */
    //protected static final Logger logger = Logger.getLogger("TriggerDb");
    /** Message Log */
    protected static final Logger logger = Logger.getLogger("TriggerTool");

    /**
     * Used when generating SQL statements. Override with correct name.
     */
    //private String tableName = "NULL";
    /**
     * Used when generating SQL . Override with correct prefix.
     */
    public String tablePrefix = "NULL_";
    /**
     * Map for SQL, value and description of the key. This is a three-way map.
     * Here the key is the SQL statement without the table prefix. The object is
     * the value from the database. The map should be initialised with putFirst
     * where you supply three arguments. The SQL, an example object (e.g. new
     * Integer(-1) ) and a text description that is displayed by the
     * TriggerTool.
     */
    //protected TriggerMap<String, Object> keyValue = new TriggerMap<String, Object>();
    
    /**
     * Map for SQL, value and description of the key.This is a three-way map.
     Here the key is the SQL statement without the table prefix. The object is
     the value from the database. The map should be initialised with putFirst
     where you supply three arguments. The SQL, an example object (e.g. new
     Integer(-1) ) and a text description that is displayed by the
     TriggerTool.
     * 
     */
    
    public TriggerMap<String, Object> keyValue = new TriggerMap<>();

    /**
     *
     */
    static protected java.util.Set<String> keystoignore = null;

    /**
     *
     */
    public static final String QUERY_COUNT = "SELECT COUNT(*) FROM ";

    /**
     * Construct and return the database query from the keyValue TriggerMap.
     * This returns the database query as a string, where the position of the ?
     * in the prepared statement is matched to the position in the loadFromRset
     * function.
     *
     * @return String Database query without a WHERE clause.
     */
    // TODO change this keytValues for static constants. 
    public final String getQueryString() {
        StringBuilder sb = new StringBuilder(500);
        sb.append("SELECT ");
        for (Iterator<String> it = keyValue.keySet().iterator(); it.hasNext();) {
            String key = it.next();
            sb.append(tablePrefix);
            sb.append(key);
            if (it.hasNext()) {
                sb.append(", ");
            }
        }

        sb.append(" FROM ").append(getTableName());
        return sb.toString();
    }    
    
    /**
     * Function to determine whether two abstract tables are the same. Compares
     * the internal TriggerMaps of the two objects to determine if they are the
     * same. Requires the two objects be of the same class.
     *
     * @param o - Object to compare this AbstractTable with
     * @return true for equal objects, false otherwise.
     */
    @Override
    public boolean equals(Object o) {
        if (o == null && this == null) {
            return true;
        }
        if (o == null || this == null) {
            return false;
        } 
        if (o.getClass().equals(this.getClass())) {
            AbstractTable obj = (AbstractTable) o;

            // / check IDs first - should allow fast rejection
            if (this.get_id() != obj.get_id()) {
                return false;
            }
            if (!this.getTriggerMap().equals(obj.getTriggerMap())) {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 6;
        hash = 31 * hash
                + (null == getTriggerMap() ? 0 : getTriggerMap().hashCode()); //hash of abstract map, may not be safe if map contains integers or possibly enums
        return hash;
    }

    /**
     * Return
     * <code>code</code> if there is an entry in DB with given id.
     * @param id
     * @return 
     * @throws java.sql.SQLException 
     */
    public final boolean exists(final int id) throws SQLException {
        int nEntries = -1;
        String query = AbstractTable.QUERY_COUNT + this.getTableName() + " WHERE " + this.tablePrefix + "ID=?";
        ConnectionManager mgr = ConnectionManager.getInstance();
        query = mgr.fix_schema_name(query);

        PreparedStatement ps = mgr.getConnection().prepareStatement(query);
        ps.setInt(1, id);
        ResultSet rset = ps.executeQuery();

        while (rset.next()) {
            nEntries = rset.getInt(1);
        }
         
        rset.close();
        ps.close();
        return nEntries > 0;
    }

    /**
     * Read into this record the parameters from a rset. The ? in the query
     * should be matched to the get methods within this.
     *
     * @param rset Result set that contains records found with getQueryString.
     * @throws java.sql.SQLException Exception when things go wrong, we should
     * stop.
     */
    public void loadFromRset(final ResultSet rset) throws SQLException {
        int i = 1;
        for (String key : keyValue.keySet()) {
            Object value = null;
               
            Object original = keyValue.get(key); 
            if (original instanceof Integer) {
                value = rset.getInt(i);
            } else if (original instanceof Long) {
                value = rset.getLong(i);
            } else if (original instanceof java.lang.Double) {
                value = rset.getDouble(i);
            } else if (original instanceof Boolean) {
                value = rset.getBoolean(i);
            } else if (original instanceof String) {
                value = rset.getString(i);
            } else if (original instanceof Timestamp) {
                value = rset.getTimestamp(i);
            } else {
                throw new SQLException(" SQL failed to determine the type of the original object: " + key + ". \n"
                        + "Name of class having a missmatch:" + original.getClass().getName());
            }
            keyValue.put(key, value);
            ++i;
        }
    }

    /**
     * Function to recover the table name for the class, or class that extends
     * this one.
     *
     * @return The table name as a string.
     */
    public String tab_name() {
        return getTableName();
    }

    // /Return the table prefix along with the underscore.
    /**
     * Function to recover the prefix for the class that extends this one.
     *
     * @return The table prefix as a string, with the underscore.
     */
    public String tab_prefix() {
        return tablePrefix;
    }

    // /Construct and add the keys for ID, Name, Version etc.
    /**
     * Constructor when we want to start with an ID. Only here to be overloaded.
     *
     * @param Prefix The table prefix.
     */
    public AbstractTable(String Prefix) {
        this(-1, Prefix);
    }
    
    // /Construct and add the keys for ID, Name, Version etc.
    /**
     * Constructor when we want to start with an ID. Only here to be overloaded.
     *
     * @param input_id ID we want to read.
     * @param Prefix The table prefix.
     */
    public AbstractTable(int input_id, String Prefix) {
        tablePrefix = Prefix;
        keyValue.putFirst("ID", input_id, "ID");
        keyValue.putFirst("NAME", "", "Name");
        keyValue.putFirst("VERSION", 1, "Version");
        //Put in a switch as not all tables have used/username/modified time now.
        if(tablePrefix.equals("SMT_") || tablePrefix.equals("L1MT_") || tablePrefix.equals("HMT_") ||  
           tablePrefix.equals("TW_") || tablePrefix.equals("PSA_") || tablePrefix.equals("L1PS_") ||
           tablePrefix.equals("HPS_") || tablePrefix.equals("L1BGS_")){
            keyValue.putFirst("USED", false, "Used");
            keyValue.putFirst("USERNAME", "nobody", "Username");
            keyValue.putFirst("MODIFIED_TIME", new Timestamp(System.currentTimeMillis()), "Modified");
        }
        if (keystoignore == null) {
            keystoignore = new java.util.HashSet<>();
            keystoignore.add("ID");
            keystoignore.add("USERNAME");
            keystoignore.add("MODIFIED_TIME");
        }
    }

    // /Force this row to be reloaded.
    /**
     * If we've just updated a record we'll need to force the program to reload
     * it from memory, like this. If the class contains the references of other
     * objects then we need to null those in this class too, so that they're
     * correctly loaded.
     * @throws java.sql.SQLException
     */
    public void forceLoad() throws SQLException {
        ConnectionManager.getInstance().forceLoad(this);
    }

    // /Return the ID.
    /**
     * Get the unique ID for this record. 0 or less means assign one when it is
     * saved
     *
     * @return Unique ID.
     */
    public int get_id() {
        return (Integer) keyValue.get("ID");
    }

    // /Return the name.
    /**
     * Get the name for the record.
     *
     * @return Record name.
     */
    public String get_name() {
        return (String) keyValue.get("NAME");
    }

    // /Return the version.
    /**
     * Get the version.
     *
     * @return version.
     */
    public Integer get_version() {
        return (Integer) keyValue.get("VERSION");
    }
    
    // /Return the modified time.
    /**
     * Time / Date of the last modification. If the timestamp read from the
     * database is null we replace it with a 'now' timestamp and print an error
     * message.
     *
     * @return modified time.
     */
    public Timestamp get_modified() {
        if (keyValue.get("MODIFIED_TIME") == null) {
            keyValue.put("MODIFIED_TIME", new Timestamp(System.currentTimeMillis()));
        }
        return (Timestamp) keyValue.get("MODIFIED_TIME");
    }

    // /Set the modified time.
    /**
     * Set the date that the record was modified.
     *
     * @param inp The new date.
     */
    public void set_modified(java.util.Date inp) {
        keyValue.put("MODIFIED_TIME", inp);
    }

    // /Set the username.
    /**
     * Set the username for this object.
     *
     * @param inp The new username.
     */
    public void set_username(String inp) {
        keyValue.put("USERNAME", inp);
    }

    // /Set the used flag.
    /**
     * Boolean. Has this record been used in a run? Probably slow if we want to
     * do it this way for an entire configuration. Instead we should use
     * triggertool.Components.SetUsed. Which bypasses reading the entire
     * configuration into memory first.
     *
     * @return Used true or false
     */
    public Boolean get_used() {
        return (Boolean) keyValue.get("USED");
    }

    // /Get the username.
    /**
     * Return the user name for the person who last edited the record.
     *
     * @return User who last modified the record.
     */
    public String get_username() {
        return (String) keyValue.get("USERNAME");
    }

    // /Set the ID.
    /**
     * Set the id of the record. Less than or equal to 0 means it'll be assigned
     * on a save. You probably don't want to assign a number here.
     *
     * @param inp Integer id to set.
     */
    public void set_id(int inp) {
        keyValue.put("ID", inp);
    }

    /**
     * Change the name for the record. Normally has to be less than 50
     * characters (I think).
     *
     * @param name new name for the record.
     */
    public void set_name(final String name) {
        keyValue.put("NAME", name);
    }

    // /Set the version.
    /**
     * The combination of name and version number is unique in the database So
     * we should make sure we're sensible.
     *
     * @param inp Version number.
     */
    public void set_version(Integer inp) {
        keyValue.put("VERSION", inp);
    }

    // /Set the used flag.
    /**
     * Set the used flag for this record.
     *
     * @param flag True is for used, False is for unused.
     */
    public void set_used(Boolean flag) {
        keyValue.put("USED", flag);
    }

    // /Get the values of this row to show in the table.
    /**
     * Returns a TreeMap (sorted HashMap) of the Human readable key mapped to
     * the row data.
     *
     * @return
     */
    public TreeMap<String,Object> getRowData() {
        return keyValue.getRowData();
    }

    // /Get the map used for searching (human to SQL).
    /**
     * Human readable key mapped to SQL Query.
     *
     * @return Human readable key mapped to SQL Query.
     */
    public HashMap getSearchData() {
        return keyValue.getSearchData();
    }

    // /Get the SQL to value map for this row.
    /**
     * Returns a map of SQL mapped to the value for this row.
     *
     * @return HashMap of SQL and data.
     */
    public TriggerMap<String,Object> getTriggerMap() {
        return keyValue;
    }

    /**
     * Save this record to the DB. Overload this with the save routine specific
     * to the class we're working with. This can grow to be a fairly complicated
     * routine, since it has to check the existence of this record, any child
     * records and if the link tables contain the correct counters.
     *
     * @return 
     * @throws SQLException Shouldn't happen, but if it does halt.
     */
    public abstract int save() throws SQLException;

    /**
     * Save this table. this saves this table only, taking care that we do not
     * create a new entry if the table already exist.
     *
     * @return the id of the new table.
     * @throws SQLException
     */
    protected final int saveSimple() throws SQLException {
        ConnectionManager mgr = ConnectionManager.getInstance();
        List<Integer> ids = mgr.get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");
        if (ids.isEmpty()) {
            this.set_id(mgr.save(getTableName(), tablePrefix, get_id(), keyValue));
        } else {
            this.set_id(ids.get(0));
        }
        return get_id();

    }

    // /Return a string representation of this class.
    /**
     * So the tree view knows what information to display in the tree
     *
     * @return A string containing the id, name and version of the record
     */
    @Override
    public String toString() {
        return "id=" + get_id() + ", name=" + get_name() + ", ver="
                + get_version();
    }

    // /Get the names for the search results.
    /**
     * The names for the items to show in the search results Id, name and
     * version by default
     *
     * @return Vector list of column headings
     */
    public ArrayList<String> get_min_names() {
        ArrayList<String> info = new ArrayList<>();
        info.add("ID");
        info.add("Name");
        info.add("Version");
        return info;
    }

    // /Get the values for the search results.
    /**
     * Matching data for the headings. All LVL1 will use this, but some HLT
     * records are special and will over-ride.
     *
     * @return Vector list of data.
     * @throws java.sql.SQLException
     */
    public ArrayList<Object> get_min_info() throws SQLException{
        ArrayList<Object> info = new ArrayList<>();
        info.add(get_id());
        info.add(get_name());
        info.add(get_version());
        return info;
    }

    // /Method to add this to the tree.
    /**
     * This method is called to add the current record children to the tree
     * view So by default we terminate the tree at this item
     *
     * @param treeNode the tree we're adding this record to
     * @param counter
     * @throws java.sql.SQLException
     */
    public void addToTree(DefaultMutableTreeNode treeNode, int counter) throws SQLException {
    }

    /**
     * Function to find the differences between this table & another by comparing
     * the internal trigger maps. Certain keys in the trigger map can be ignored
     * by providing a Set of strings. Results are added to the provided tree
     * node.
     *
     * @param t - Table to compare with.
     * @param treeNode - tree node for the results.
     * @param linkstoignore - keys in the triggermap that are to be ignored.
     * @return number of differences.
     * @throws java.sql.SQLException
     */
    public int doDiff(AbstractTable t, DefaultMutableTreeNode treeNode,
            java.util.Set<String> linkstoignore) throws SQLException {
        int ndiff = 0;

        TreeMap map1 = keyValue;
        TreeMap map2 = t.keyValue;

        Iterator i = map1.keySet().iterator();
        while (i.hasNext()) {
            String key = (String) i.next();
            Object value1 = map1.get(key);
            Object value2 = map2.get(key);

            // / ignore certain keys
            if (keystoignore.contains(key)) {
                continue;
            }
            if (linkstoignore != null) {
                if (linkstoignore.contains(key)) {
                    continue;
                }
            }

            if (value1 != null && value2 != null) {
                if (!value1.equals(value2)) {

                    treeNode.add(new DiffMutableTreeNode(key, value1.toString(), value2.toString()));
                    ndiff++;
                }
            }
        }

        return ndiff;

    }

    // /Create a copy of this object and link-table children.
    /**
     * So we can make copies of an instance. I think we should work on replacing
     * this with copy constructors soon. Performs a deep copy, including
     * children.
     *
     * @return A copy of this object.
     */
    @Override
    public abstract Object clone() ;

    // /Check a ID has a matching entry in the DB before we load.
    /**
     * Before loading a certain ID we should check that it actually exists in
     * the Database. For Oracle this isn't so important, because the constraints
     * on keys are strict. But it's not always the case on MySQL, where you can
     * point at something that doesn't actually exist in the database.
     *
     * @param id The ID of the record we wish to check. Table name and prefix
     * comes from the overloaded class.
     * @return True if the record exists, false if it does not.
     */
    public boolean isValid(int id) {
        boolean valid = false;
        try {
            String query = "SELECT " + tablePrefix + "ID FROM " + getTableName()
                    + " WHERE " + tablePrefix + "ID=?";
            ConnectionManager mgr = ConnectionManager.getInstance();
            query = mgr.fix_schema_name(query);

            PreparedStatement ps = mgr.getConnection().prepareStatement(query);
            ps.setInt(1, id);
            ResultSet rset = ps.executeQuery();

            int counter_because_oracle_is_awkward = 0;

            while (rset.next()) {
                ++counter_because_oracle_is_awkward;
            }
            if (counter_because_oracle_is_awkward == 1) {
                valid = true;
            }
            rset.close();
            ps.close();
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Select statement is invalid: {0} \n STacktrace: {1}", new Object[]{e.getMessage(), e.getStackTrace()});
            return false;
        }
        return valid;
    }
    
    @Override
    public int compareTo(final AbstractTable o) {
        int comp = 0;
        int lhs = this.get_id();
        int rhs = o.get_id();
        if (lhs < rhs) {
            comp = -1;
        } else if (lhs > rhs) {
            comp = 1;
        }
        return comp;
    }

    // /Set the modified time.
    /**
     * Function to change the modified time to a new value.
     *
     * @param timestamp The new modified time.
     */
    public void set_modified_time(Timestamp timestamp) {
        keyValue.put("MODIFIED_TIME", timestamp);
    }

    /**
     * @return the tableName
     */
    abstract public String getTableName();
}
