package triggerdb.Entities.TopoLinks;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.TreeMap;
import triggerdb.Entities.AbstractTable;
import triggerdb.Connections.ConnectionManager;

/**
 * Link between the topo algo and its generic components
 *
 * @author Alex Martyniuk
 */
public final class TA_TG extends AbstractTable {

    /**
     * Empty constructor.
     */
    public TA_TG() { 
        super("TA2TG_");
        setKeys();
    }

    /**
     *
     * @param ta2tgId
     * @throws SQLException
     */
    public TA_TG(final int ta2tgId) throws SQLException { 
        super(ta2tgId, "TA2TG_");
        setKeys();
        if (ta2tgId > 0) {
            forceLoad();
        }
    }

    private void setKeys() {
        keyValue.putFirst("ALGO_ID", -1, "Algorithm ID");
        keyValue.putFirst("GENERIC_ID", -1, "Generic ID");
        
        keyValue.remove("NAME");
        keyValue.remove("VERSION");
    }
    
    /**Get the algo ID
     * @return .*/
    public Integer get_algo_id() {
        return (Integer) keyValue.get("ALGO_ID");
    }
    
    /**Set the algo ID
     * @param inp.*/
    public void set_algo_id(final Integer inp) {
        keyValue.put("ALGO_ID", inp);
    }

    /** Get the generic ID
     * @return .*/
    public Integer get_generic_id() {
        return (Integer) keyValue.get("GENERIC_ID");
    }
    
    /** Set the generic ID
     * @param inp.*/
    public void set_generic_id(final Integer inp) {
        keyValue.put("GENERIC_ID", inp);
    }

    
    /**
     * A simple old style save.
     * Please use the batch save one where possible.
     *
     * @return The new ID if one is assigned, or the ID of the existing record.
     * @throws java.sql.SQLException Stop if we have an SQL problem
     */
    @Override
    public int save() throws SQLException {
        ConnectionManager mgr = ConnectionManager.getInstance();

        ArrayList<Integer> ids = mgr.get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");

        if (ids.size() <= 0) {
            set_id(mgr.save(getTableName(), tablePrefix, get_id(), keyValue));
        } else {
            set_id(ids.get(0));
            forceLoad();
        }

        return get_id();
    }
    
    /**
     * Run this to save a block of inputs
     * @param Generics
     * @return
     * @throws SQLException 
     */
    public TreeMap<Integer, Integer> batchsave(ArrayList<TA_TG> Generics) throws SQLException {

        ArrayList<TreeMap<String, Object>> GenericInputs = new ArrayList<>();
        for(TA_TG generic:Generics){
                TreeMap<String, Object> table = new TreeMap<>();
                table.put("ALGO_ID", generic.get_algo_id());
                table.put("GENERIC_ID", generic.get_generic_id());
                GenericInputs.add(table);
        }
        TreeMap<Integer, Integer> results = ConnectionManager.getInstance().saveBatch("TA_TO_TG", tablePrefix, 0, GenericInputs);
        return results;
    }

    /**Clone is not supported here
     * @return .*/
    @Override
    public Object clone() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getTableName() {
        return "TA_TO_TG";
    }
}
