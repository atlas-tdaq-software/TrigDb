package triggerdb.Entities.TopoLinks;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.TreeMap;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;

/**
 * Link between a menu and prescale set. This is needed since a menu is not
 * copied (no new id) if prescale sets are added or removed
 *
 * @author Alex Martyniuk
 */
public final class TTM_TC extends AbstractTable {

    /**
     * Empty constructor.
     */
    public TTM_TC() { 
        super("TTM2TC_");
        setKeys();
    }

    /**
     *
     * @param ttm2tcId
     * @throws SQLException
     */
    public TTM_TC(final int ttm2tcId) throws SQLException { 
        super(-1,"TTM2TC_");
        setKeys();
        if (ttm2tcId > 0) {
            forceLoad();
        }
    }

    private void setKeys() {
        keyValue.putFirst("MENU_ID", -1, "Menu ID");
        keyValue.putFirst("CONFIG_ID", -1, "Config ID");
        
        keyValue.remove("NAME");
        keyValue.remove("VERSION");        
    }
    
    /**Get the algo ID
     * @return .*/
    public Integer get_menu_id() {
        return (Integer) keyValue.get("MENU_ID");
    }
    
    /**Set the algo ID
     * @param inp.*/
    public void set_menu_id(final Integer inp) {
        keyValue.put("MENU_ID", inp);
    }

    /** Get the config ID
     * @return .*/
    public Integer get_config_id() {
        return (Integer) keyValue.get("CONFIG_ID");
    }
    
    /** Set the config ID
     * @param inp.*/
    public void set_config_id(final Integer inp) {
        keyValue.put("CONFIG_ID", inp);
    }

    
    /**
     * A simple old style save.
     * Please use the batch save one where possible.
     *
     * @return The new ID if one is assigned, or the ID of the existing record.
     * @throws java.sql.SQLException Stop if we have an SQL problem
     */
    @Override
    public int save() throws SQLException {
        ConnectionManager mgr = ConnectionManager.getInstance();

        ArrayList<Integer> ids = mgr.get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");

        if (ids.size() <= 0) {
            set_id(mgr.save(getTableName(), tablePrefix, get_id(), keyValue));
        } else {
            set_id(ids.get(0));
            forceLoad();
        }

        return get_id();
    }
    
    /**
     *
     * @param Configs
     * @return
     * @throws SQLException
     */
    public TreeMap<Integer, Integer> batchsave(ArrayList<TTM_TC> Configs) throws SQLException {

        ArrayList<TreeMap<String, Object>> ConfigInputs = new ArrayList<>();
        for(TTM_TC config:Configs){
                TreeMap<String, Object> table = new TreeMap<>();
                table.put("CONFIG_ID", config.get_config_id());
                table.put("MENU_ID", config.get_menu_id());
                ConfigInputs.add(table);
        }
        TreeMap<Integer, Integer> results = ConnectionManager.getInstance().saveBatch("TTM_TO_TC", tablePrefix, 0, ConfigInputs);
        return results;
    }

    /**Clone is not supported here
     * @return .*/
    @Override
    public Object clone() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getTableName() {
        return "TTM_TO_TC";
    }
}
