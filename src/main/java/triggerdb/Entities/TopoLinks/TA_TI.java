package triggerdb.Entities.TopoLinks;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.TreeMap;
import triggerdb.Entities.AbstractTable;
import triggerdb.Connections.ConnectionManager;

/**
 * Link between the algorithm and its input tables
 *
 * @author Alex Martyniuk
 */
public final class TA_TI extends AbstractTable {

    /**
     * Empty constructor.
     */
    public TA_TI() { 
        super("TA2TI_");
        setKeys();
    }
    
    /**
     *
     * @param ta2tiId
     * @throws SQLException
     */
    public TA_TI(final int ta2tiId) throws SQLException { 
        super(ta2tiId, "TA2TI_");
        setKeys();
        if (ta2tiId > 0) {
            forceLoad();
        }
    }
    
    private void setKeys() {
        keyValue.putFirst("ALGO_ID", -1, "Algorithm ID");
        keyValue.putFirst("GENERIC_ID", -1, "Generic ID");

        keyValue.remove("NAME");
        keyValue.remove("VERSION");
    }
    /**Get the algo ID
     * @return .*/
    public Integer get_algo_id() {
        return (Integer) keyValue.get("ALGO_ID");
    }
    
    /**Set the algo ID
     * @param inp.*/
    public void set_algo_id(final Integer inp) {
        keyValue.put("ALGO_ID", inp);
    }

    /** Get the input ID
     * @return .*/
    public Integer get_input_id() {
        return (Integer) keyValue.get("INPUT_ID");
    }
    
    /** Set the input ID
     * @param inp.*/
    public void set_input_id(final Integer inp) {
        keyValue.put("INPUT_ID", inp);
    }

    
    /**
     * A simple old style save.
     * Please use the batch save one where possible.
     *
     * @return The new ID if one is assigned, or the ID of the existing record.
     * @throws java.sql.SQLException Stop if we have an SQL problem
     */
    @Override
    public int save() throws SQLException {
        ConnectionManager mgr = ConnectionManager.getInstance();

        ArrayList<Integer> ids = mgr.get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");

        if (ids.size() <= 0) {
            set_id(mgr.save(getTableName(), tablePrefix, get_id(), keyValue));
        } else {
            set_id(ids.get(0));
            forceLoad();
        }

        return get_id();
    }

    /**
     *
     * @param Inputs
     * @return
     * @throws SQLException
     */
    public TreeMap<Integer, Integer> batchsave(ArrayList<TA_TI> Inputs) throws SQLException {

        ArrayList<TreeMap<String, Object>> InputInputs = new ArrayList<>();
        for(TA_TI input:Inputs){
                TreeMap<String, Object> table = new TreeMap<>();
                table.put("ALGO_ID", input.get_algo_id());
                table.put("INPUT_ID", input.get_input_id());
                InputInputs.add(table);
        }
        TreeMap<Integer, Integer> results = ConnectionManager.getInstance().saveBatch("TA_TO_TI", tablePrefix, 0, InputInputs);
        return results;
    }
    
    /**Clone is not supported here
     * @return .*/
    @Override
    public Object clone() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getTableName() {
        return "TA_TO_TI";
    }
}
