package triggerdb.Entities.TopoLinks;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.TreeMap;
import triggerdb.Entities.AbstractTable;
import triggerdb.Connections.ConnectionManager;

/**
 * A link form the topo trigger menu to the trigger algos
 *
 * @author Alex Martyniuk
 */
public final class TTM_TA extends AbstractTable {

    /**
     * Empty constructor.
     */
    public TTM_TA() { 
        super("TTM2TA_");
        setKeys();
    }

    /**
     *
     * @param ttm2taId
     * @throws SQLException
     */
    public TTM_TA(final int ttm2taId) throws SQLException { 
        super(-1,"TTM2TA_");
        setKeys();
        if (ttm2taId > 0) {
            forceLoad();
        }
    }

    private void setKeys() {
        keyValue.putFirst("MENU_ID", -1, "Menu ID");
        keyValue.putFirst("ALGO_ID", -1, "Algorithm ID");
        
        keyValue.remove("NAME");
        keyValue.remove("VERSION");
    }
    /** Get the menu ID
     * @return .*/
    public Integer get_menu_id() {
        return (Integer) keyValue.get("MENU_ID");
    }
    
    /** Set the menu ID
     * @param inp.*/
    public void set_menu_id(final Integer inp) {
        keyValue.put("MENU_ID", inp);
    }

    /**Get the algo ID
     * @return .*/
    public Integer get_algo_id() {
        return (Integer) keyValue.get("ALGO_ID");
    }
    
    /**Set the algo ID
     * @param inp.*/
    public void set_algo_id(final Integer inp) {
        keyValue.put("ALGO_ID", inp);
    }
    
    /**
     * A simple old style save.
     * Please use the batch save one where possible.
     *
     * @return The new ID if one is assigned, or the ID of the existing record.
     * @throws java.sql.SQLException Stop if we have an SQL problem
     */
    @Override
    public int save() throws SQLException {
        ConnectionManager mgr = ConnectionManager.getInstance();

        ArrayList<Integer> ids = mgr.get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");

        if (ids.size() <= 0) {
            set_id(mgr.save(getTableName(), tablePrefix, get_id(), keyValue));
        } else {
            set_id(ids.get(0));
            forceLoad();
        }

        return get_id();    
    }

    /**
     *
     * @param Algos
     * @return
     * @throws SQLException
     */
    public TreeMap<Integer, Integer> batchsave(ArrayList<TTM_TA> Algos) throws SQLException {

        ArrayList<TreeMap<String, Object>> AlgoInputs = new ArrayList<>();
        for(TTM_TA algo:Algos){
                TreeMap<String, Object> table = new TreeMap<>();
                table.put("ALGO_ID", algo.get_algo_id());
                table.put("MENU_ID", algo.get_menu_id());
                AlgoInputs.add(table);
        }
        TreeMap<Integer, Integer> results = ConnectionManager.getInstance().saveBatch("TTM_TO_TA", tablePrefix, 0, AlgoInputs);
        return results;
    }
    
    /**Clone is not supported here
     * @return .*/
    @Override
    public Object clone() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getTableName() {
        return "TTM_TO_TA";
    }
}
