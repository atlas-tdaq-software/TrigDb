package triggerdb.Entities.TopoLinks;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.TreeMap;
import triggerdb.Entities.AbstractTable;
import triggerdb.Connections.ConnectionManager;

/**
 * Link between the topo algo and its output
 *
 * @author Alex Martyniuk
 */
public final class TA_TO extends AbstractTable {

    /**
     * Empty constructor.
     */
    public TA_TO() { 
        super("TA2TO_");
        setKeys();
    }

    /**
     *
     * @param ta2t0Id
     * @throws SQLException
     */
    public TA_TO(final int ta2t0Id) throws SQLException { 
        super(-1,"TA2TO_");  // Alex, should this really be -1???
        setKeys();
        if (ta2t0Id > 0) {
            forceLoad();
        }
    }

    private void setKeys() {
        keyValue.putFirst("ALGO_ID", -1, "Algorithm ID");
        keyValue.putFirst("OUTPUT_ID", -1, "Output ID");
        
        keyValue.remove("NAME");
        keyValue.remove("VERSION");
    }
    
    /**Get the algo ID
     * @return .*/
    public Integer get_algo_id() {
        return (Integer) keyValue.get("ALGO_ID");
    }
    
    /**Set the algo ID
     * @param inp.*/
    public void set_algo_id(final Integer inp) {
        keyValue.put("ALGO_ID", inp);
    }

    /** Get the output ID
     * @return .*/
    public Integer get_output_id() {
        return (Integer) keyValue.get("OUTPUT_ID");
    }
    
    /** Set the output ID
     * @param inp.*/
    public void set_output_id(final Integer inp) {
        keyValue.put("OUTPUT_ID", inp);
    }

    
    /**
     * A simple old style save.
     * Please use the batch save one where possible.
     *
     * @return The new ID if one is assigned, or the ID of the existing record.
     * @throws java.sql.SQLException Stop if we have an SQL problem
     */
    @Override
    public int save() throws SQLException {
        ConnectionManager mgr = ConnectionManager.getInstance();

        ArrayList<Integer> ids = mgr.get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");

        if (ids.size() <= 0) {
            set_id(mgr.save(getTableName(), tablePrefix, get_id(), keyValue));
        } else {
            set_id(ids.get(0));
            forceLoad();
        }

        return get_id();
    }
    
    /**
     *
     * @param Outputs
     * @return
     * @throws SQLException
     */
    public TreeMap<Integer, Integer> batchsave(ArrayList<TA_TO> Outputs) throws SQLException {

        ArrayList<TreeMap<String, Object>> OutputOutputs = new ArrayList<>();
        for(TA_TO output:Outputs){
                TreeMap<String, Object> table = new TreeMap<>();
                table.put("ALGO_ID", output.get_algo_id());
                table.put("OUTPUT_ID", output.get_output_id());
                OutputOutputs.add(table);
        }
        TreeMap<Integer, Integer> results = ConnectionManager.getInstance().saveBatch("TA_TO_TO", tablePrefix, 0, OutputOutputs);
        return results;
    }

    /**Clone is not supported here
     * @return .*/
    @Override
    public Object clone() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getTableName() {
        return "TA_TO_TO";
    }
}
