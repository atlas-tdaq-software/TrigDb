package triggerdb.Entities.TopoLinks;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.TreeMap;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;

/**
 * Link between the topo algo and its parameters
 *
 * @author Alex Martyniuk
 */
public final class TA_TP extends AbstractTable {

    /**
     * Empty constructor.
     */
    public TA_TP() { 
        super("TA2TP_");
        setKeys();
    }

    /**
     *
     * @param ta2tpId
     * @throws SQLException
     */
    public TA_TP(final int ta2tpId) throws SQLException { 
        super(-1,"TA2TP_");
        setKeys();
        if (ta2tpId > 0) {
            forceLoad();
        }
    }

    private void setKeys() {
        keyValue.putFirst("ALGO_ID", -1, "Algorithm ID");
        keyValue.putFirst("PARAMETER_ID", -1, "Parameter ID");
        
        keyValue.remove("NAME");
        keyValue.remove("VERSION");
    }

    /**Get the algo ID
     * @return .*/
    public Integer get_algo_id() {
        return (Integer) keyValue.get("ALGO_ID");
    }
    
    /**Set the algo ID
     * @param inp.*/
    public void set_algo_id(final Integer inp) {
        keyValue.put("ALGO_ID", inp);
    }

    /** Get the parameter ID
     * @return .*/
    public Integer get_parameter_id() {
        return (Integer) keyValue.get("PARAMETER_ID");
    }
    
    /** Set the parameter ID
     * @param inp.*/
    public void set_parameter_id(final Integer inp) {
        keyValue.put("PARAMETER_ID", inp);
    }

    
    /**
     * A simple old style save.
     * Please use the batch save one where possible.
     *
     * @return The new ID if one is assigned, or the ID of the existing record.
     * @throws java.sql.SQLException Stop if we have an SQL problem
     */
    @Override
    public int save() throws SQLException {
        ConnectionManager mgr = ConnectionManager.getInstance();

        ArrayList<Integer> ids = mgr.get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");

        if (ids.size() <= 0) {
            set_id(mgr.save(getTableName(), tablePrefix, get_id(), keyValue));
        } else {
            set_id(ids.get(0));
            forceLoad();
        }

        return get_id();
    }
    
    /**
     *
     * @param Parameters
     * @return
     * @throws SQLException
     */
    public TreeMap<Integer, Integer> batchsave(ArrayList<TA_TP> Parameters) throws SQLException {

        ArrayList<TreeMap<String, Object>> ParameterInputs = new ArrayList<>();
        for(TA_TP parameter:Parameters){
                TreeMap<String, Object> table = new TreeMap<>();
                table.put("ALGO_ID", parameter.get_algo_id());
                table.put("PARAM_ID", parameter.get_parameter_id());
                ParameterInputs.add(table);
        }
        TreeMap<Integer, Integer> results = ConnectionManager.getInstance().saveBatch("TA_TO_TP", tablePrefix, 0, ParameterInputs);
        return results;
    }

    /**Clone is not supported here
     * @return .*/
    @Override
    public Object clone() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getTableName() {
        return "TA_TO_TP";
    }
}
