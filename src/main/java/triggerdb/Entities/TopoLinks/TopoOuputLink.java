package triggerdb.Entities.TopoLinks;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.TreeMap;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;

/**
 * A link from the topo->ctp link table to the ouput list for this topo menu. 
 *
 * @author Alex Martyniuk
 */
public final class TopoOuputLink extends AbstractTable {

    /**
     * Standard constructor. If an ID of -1 is supplied we create a new, empty
     * record and don't attempt to load anything from the database. If the ID
     * supplied is greater than 0 we load this record from the database.
     *
     */
    public TopoOuputLink() { 
        super("TL_");
        setKeys();
    }  

    /**
     *
     * @param input_id
     * @throws SQLException
     */
    public TopoOuputLink(final int input_id) throws SQLException { 
        super(input_id, "TL_");
        setKeys();
        if (input_id > 0) {
            forceLoad();
        }
    }  


    private void setKeys() {
        keyValue.putFirst("TOPO_ID", -1, "Topo list ID");
        keyValue.putFirst("L1_ID", -1, "Output ID");
        
        keyValue.remove("NAME");
        keyValue.remove("VERSION");
    }

    /**Get the link ID
     * @return .*/
    public Integer get_link_id() {
        return (Integer) keyValue.get("LINK_ID");
    }
    
    /**Set the link ID
     * @param inp.*/
    public void set_link_id(final Integer inp) {
        keyValue.put("LINK_ID", inp);
    }

    /** Get the output ID
     * @return .*/
    public Integer get_output_id() {
        return (Integer) keyValue.get("OUTPUT_ID");
    }
    
    /** Set the output ID
     * @param inp.*/
    public void set_output_id(final Integer inp) {
        keyValue.put("OUTPUT_ID", inp);
    }

    
    /**
     * A simple old style save.
     * Please use the batch save one where possible.
     *
     * @return The new ID if one is assigned, or the ID of the existing record.
     * @throws java.sql.SQLException Stop if we have an SQL problem
     */
    @Override
    public int save() throws SQLException {
        ConnectionManager mgr = ConnectionManager.getInstance();

        ArrayList<Integer> ids = mgr.get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");

        if (ids.size() <= 0) {
            set_id(mgr.save(getTableName(), tablePrefix, get_id(), keyValue));
        } else {
            set_id(ids.get(0));
            forceLoad();
        }

        return get_id();
    }
    
    /**
     * Run this to save a block of inputs
     * @param Links
     * @return
     * @throws SQLException 
     */
    public TreeMap<Integer, Integer> batchsave(ArrayList<TopoOuputLink> Links) throws SQLException {

        ArrayList<TreeMap<String, Object>> LinkInputs = new ArrayList<>();
        for(TopoOuputLink link:Links){
                TreeMap<String, Object> table = new TreeMap<>();
                table.put("LINK_ID", link.get_link_id());
                table.put("OUTPUT_ID", link.get_output_id());
                LinkInputs.add(table);
        }
        TreeMap<Integer, Integer> results = ConnectionManager.getInstance().saveBatch("TOPO_OUTPUT_LINK", tablePrefix, 0, LinkInputs);
        return results;
    }

    /**Clone is not supported here
     * @return .*/
    @Override
    public Object clone() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getTableName() {
        return "TOPO_OUTPUT_LINE";
    }
}
