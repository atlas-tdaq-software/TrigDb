package triggerdb.Entities.L1;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.L1Links.L1TM_PS;

/**
 * Holds the prescales for a configuration. A configuration can typically have
 * more than one prescale set assigned to it, especially if we're using some
 * sort of auto prescale tool. Setting a prescale to -1 disables that L1Item.
 *
 * @author Simon Head
 * @author Paul Bell
 */
public final class L1Prescale extends AbstractTable implements L1PrescaleI {

    /**
     * Normal constructor.
     *
     */
    public L1Prescale() {
        super(-1, "L1PS_");
        DefineKeyValuePairs();
    }

    private void DefineKeyValuePairs() {
        keyValue.putFirst("LUMI", "0.0", "Luminosity");
        keyValue.putFirst("SHIFT_SAFE", 0, "Shift Safe");
        keyValue.putFirst("DEFAULT", 0, "Default");
        keyValue.putFirst("COMMENT", "", "Comment");
        keyValue.putFirst("TYPE", "", "Type");
	keyValue.putFirst("PARTITION", 0, "Partition");

        for (int i = 1; i <= L1Prescale.LENGTH; i++) {
            keyValue.putFirst("VAL" + i, -1, "Value" + i);
        }
    }

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /**
     * Number of L1 PRESCALES.
     */
    public static final int LENGTH = 512;
    /**
     * Default Prescale.
     */
    public static final int DEF_L1PS = -1;
    private static final String TABLE_NAME = "L1_PRESCALE_SET";

    /**
     * Normal constructor.
     *
     * @param inputId the primary key of the prescale table on the DB.
     * @throws java.sql.SQLException
     */
    public L1Prescale(final int inputId) throws SQLException {
        super(inputId, "L1PS_");
        DefineKeyValuePairs();

        if (inputId > 0) {
            this.forceLoad();
        }
    }

    /**
     * Get the id of the link to the menu.
     *
     * @param l1_menu_id The menu id
     * @return the id of the link TM <-> PSset.
     */
    @Override
    public int get_TMPSid(int l1_menu_id) throws SQLException {
        return L1TM_PS.getTM2PS(l1_menu_id, this.get_id());
    }

    /**
     * Checks whether this prescale set hidden in this menu.
     *
     * @param l1_menu_id The menu id.
     * @return <code>true</code> if hidden else <code>false</code>
     */
    @Override
    public boolean getHidden(int l1_menu_id) throws SQLException {
        boolean hidden = false;
        String query = "SELECT L1TM2PS_USED, L1TM2PS_ID, L1TM2PS_PRESCALE_SET_ID, L1TM2PS_TRIGGER_MENU_ID "
                + "FROM "
                + "L1_TM_TO_PS "
                + "WHERE "
                + "L1TM2PS_TRIGGER_MENU_ID=? "
                + "AND "
                + "L1TM2PS_PRESCALE_SET_ID=? "
                + "ORDER BY L1TM2PS_ID DESC";

        query = ConnectionManager.getInstance().fix_schema_name(query);
        try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query)) {
            ps.setInt(1, l1_menu_id);
            ps.setInt(2, get_id());
            try (ResultSet rset = ps.executeQuery()) {
                while (rset.next()) {
                    hidden = rset.getBoolean("L1TM2PS_USED");
                }
                if (rset.next()) {
                    logger.log(Level.SEVERE, "More than one TMPS link found for L1 menu-pss {0} {1}", new Object[]{l1_menu_id, get_id()});
                }
                rset.close();
                ps.close();
            }
        }

        return hidden;
    }

    /**
     * After an edit we need to force the record to read from the database.
     *
     * @throws java.sql.SQLException
     */
    @Override
    public void forceLoad() throws SQLException {
        ConnectionManager.getInstance().forceLoad(this);
    }

    /**
     * Get one of the 256 prescales, 1 up to and including 256.
     *
     * @param index the index of the value, which is the CTP_ID + 1 !
     * @return the Long value of the L1PSNumber at position idx.
     */
    @Override
    public Integer get_val(int index) {
        return (Integer) keyValue.get("VAL" + index);
    }

    /**
     * Set one of the 512 prescales, 1 up to and including 512.
     *
     * @param ctpId the ctp id + 1 [1-512]
     * @param prescaleValue
     */
    public void set_int_val(final int ctpId, final int prescaleValue) {
        if ((ctpId <= 0) || (ctpId > L1Prescale.LENGTH)) {
            logger.log(Level.SEVERE, "CTP ID ({0}) OUT OF RANGE", ctpId);
        } else {
            this.keyValue.put("VAL" + ctpId, prescaleValue);
        }
    }

    /**
     * Set one of the 512 prescales, 1 up to and including 512.
     *
     * @param ctpId the ctp id +1. Set IN1 up to and including 256.
     * @param cutValue the cut value checks that it is between -0xFFFFFF and
     * 0xFFFFFF and not 0x0
     */
    @Override
    public void set_val(final int ctpId, final Integer cutValue) {
        if ((ctpId <= 0) || (ctpId > L1Prescale.LENGTH)) {
            logger.log(Level.SEVERE, "CTP ID ({0}) OUT OF RANGE", ctpId);
            return;
        }
        int validCut = cutValue; // we check this here to make sure that there is nothing invalid inserted into the DB
        if (validCut > 0xFFFFFF) {
            logger.log(Level.WARNING, "For CTP ID {0} found cut larger than 0xFFFFFF, adjusted to maximum", ctpId);
            validCut = 0xFFFFFF;
        }
        if (validCut < -0xFFFFFF) {
            logger.log(Level.WARNING, "For CTP ID {0} found cut smaller than -0xFFFFFF, adjusted to -0xFFFFFF", ctpId);
            validCut = -0xFFFFFF;
        }
        if (validCut == 0) {
            logger.log(Level.WARNING, "For CTP ID {0} found cut at 0, adjusted to 0x1", ctpId);
            validCut = 1;
        }
        this.keyValue.put("VAL" + ctpId, validCut);
    }

    /**
     * Shift safe flag.
     *
     * @return
     */
    public Integer get_shift_safe() {
        return (Integer) keyValue.get("SHIFT_SAFE");
    }

    /**
     * Set shift safe.
     *
     * @param inp
     */
    public void set_shift_safe(final Integer inp) {
        keyValue.put("SHIFT_SAFE", inp);
    }

    /**
     * Is this default?
     */
    @Override
    public Integer get_default() {
        return (Integer) keyValue.get("DEFAULT");
    }

    /**
     * Set default.
     *
     * @param inp
     */
    @Override
    public void set_default(final Integer inp) {
        keyValue.put("DEFAULT", inp);
    }

    /**
     * First check if the L1 Prescale Set exists, if not save this record.
     * Otherwise return the ID of the lowest ID Prescale Set that matches.
     *
     * @return the id of the saved prescale set.
     * @throws java.sql.SQLException if there is some problem.
     */
    @Override
    public int save() throws SQLException {
        String name = this.get_name();
        this.keyValue.remove("COMMENT"); // don't compare comments
        if(name.contains("AUTO_")) {
            this.keyValue.remove("NAME");
        } // don't compare names, for Autoprescaler
        //this.keyValue.remove("TYPE"); // don't compare types
        
        ArrayList<Integer> ids;
        if (ConnectionManager.getInstance() != null) {
            ConnectionManager cm = ConnectionManager.getInstance();
            ids = cm.get_IDs(this.getTableName(), this.tablePrefix,
                    this.keyValue, null, "ID");
            if (ids.isEmpty()) {
                keyValue.put("NAME", name);//put back now
                set_id(ConnectionManager.getInstance().save(getTableName(),
                        tablePrefix, get_id(), keyValue));
            } else {
                keyValue.put("NAME", name);//put back now
                set_id(ids.get(0));
                keyValue.put("NAME", name);//put back now
            }
        } else {
            System.err.println("no conn manager ");
        }

        return get_id();
    }

    /**
     * Save this L1Prescale and links it to the given L1Menu
     *
     * @param onL1Menu the L1Menu to link to.
     * @return The id of the newly saved L1PRescale.
     * @throws java.sql.SQLException
     */
    public int save(final L1Menu onL1Menu) throws SQLException {
        int psSaveId;

        psSaveId = this.save();

        // Add the saved prescale to the given menu, this will create a new
        // Link table if needed.
        onL1Menu.addPrescaleSet(this, true);

        // Get the id of the link table.
        return psSaveId;
    }

    /**
     * Save this L1Prescale and links it to the given L1Menu
     *
     * @param onL1Menu the L1Menu to link to.
     * @return The the L1TM_PS Link table.
     */
    //public L1TM_PS save(final L1Menu onL1Menu) {
    //    return new L1TM_PS(L1TM_PS.getTM2PS(this.save(onL1Menu), onL1Menu.get_id()));
    //}
    /**
     * String description of the Prescale Set.
     *
     * @return
     */
    @Override
    public String toString() {
        if (this.get_id() < 0) {
            return " *: " + this.get_name();
        }
        return get_id() + ": " + get_name() + " v" + get_version();
    }

    @Override
    public void update_comment(final String _comment) throws SQLException {
        this.set_comment(_comment);
        String query = "UPDATE L1_PRESCALE_SET SET L1PS_COMMENT=? WHERE L1PS_ID=?";
        ConnectionManager mgr = ConnectionManager.getInstance();
        query = mgr.fix_schema_name(query);

        try (PreparedStatement ps = mgr.getConnection().prepareStatement(query)) {
            ps.setString(1, _comment);
            ps.setInt(2, this.get_id());
            ps.executeUpdate();
            ps.close();
        }
    }

    /**
     * Set a comment about this PSS.
     *
     */
    @Override
    public void set_comment(final String inp) {
        keyValue.put("COMMENT", inp);
    }

    /**
     * Get the comment about this configuration.
     *
     * @return
     */
    @Override
    public String get_comment() {
        if (keyValue.get("COMMENT") == null) {
            return "null comment";
        } else {
            return (String) keyValue.get("COMMENT");
        }
    }

    /**
     * To create a copy of this object.
     *
     * @return
     */
    @Override
    public Object clone() {
        L1Prescale copy = new L1Prescale();

        copy.set_shift_safe(get_shift_safe());
        copy.set_default(get_default());

        for (int i = 1; i <= L1Prescale.LENGTH; ++i) {
           // copy.set_val(i, get_val(i));
        }
        return copy;
    }

    /**
     * Load all L1Prescale form DB.
     *
     * @return a List with all L1Presacle.
     * @throws java.sql.SQLException
     */
    public static ArrayList<L1Prescale> loadAll() throws SQLException {
        ArrayList<L1Prescale> psSets = new ArrayList<>();
        ConnectionManager mgr = ConnectionManager.getInstance();
        String query = "SELECT L1PS_ID FROM L1_PRESCALE_SET "
                + "ORDER BY L1PS_ID DESC";

        query = mgr.fix_schema_name(query);
        try (PreparedStatement ps = mgr.getConnection().prepareStatement(query);
                ResultSet rset = ps.executeQuery()) {
            while (rset.next()) {
                L1Prescale l1ps = new L1Prescale(rset.getInt("L1PS_ID"));
                psSets.add(l1ps);
            }
            rset.close();
            ps.close();
        }

        return psSets;
    }

    /**
     * Load all L1Prescale form DB.
     *
     * @return a List with all L1Prescale.
     * @throws java.sql.SQLException
     */
    public static ArrayList<String> loadAvailableSetNames() throws SQLException {
        ArrayList<String> psSets = new ArrayList<>();
        ConnectionManager mgr = ConnectionManager.getInstance();
        String query = "SELECT L1PS_ID,L1PS_NAME FROM L1_PRESCALE_SET "
                + "ORDER BY L1PS_ID DESC";

        query = mgr.fix_schema_name(query);
        try (PreparedStatement ps = mgr.getConnection().prepareStatement(query);
                ResultSet rset = ps.executeQuery()) {
            while (rset.next()) {
                psSets.add(rset.getInt("L1PS_ID") + ": " + rset.getString("L1PS_NAME"));
            }
            rset.close();
            ps.close();
        }

        return psSets;
    }

    /**
     *
     * @param l1MenuId
     * @return
     * @throws SQLException
     */
    public static ArrayList<String> loadAvailableSetNames(final int l1MenuId) throws SQLException {
        ArrayList<String> psSets = new ArrayList<>();
        ConnectionManager mgr = ConnectionManager.getInstance();
        String query = "select * from (select  L1TM2PS_PRESCALE_SET_ID, L1TM2PS_TRIGGER_MENU_ID "
                + "FROM L1_TM_TO_PS where  L1TM2PS_TRIGGER_MENU_ID=?) lhs\n"
                + "join (select L1PS_ID, L1PS_NAME from L1_PRESCALE_SET order by L1PS_ID DESC) rhs\n"
                + "on L1TM2PS_PRESCALE_SET_ID=L1PS_ID";

        query = mgr.fix_schema_name(query);
        try (PreparedStatement ps = mgr.getConnection().prepareStatement(query)) {
            ps.setInt(1, l1MenuId);
            try (ResultSet rset = ps.executeQuery()) {
                while (rset.next()) {
                    psSets.add(rset.getInt("L1PS_ID") + ": " + rset.getString("L1PS_NAME"));
                }
                rset.close();
                ps.close();
            }
        }

        return psSets;
    }

    /**
     * Load all L1Prescale linked to a certain L1 Trigger Menu.
     *
     * @param l1MenuId The id of the menu to query.
     * @return a List with all L1Presacle linked to l1MenuId.
     * @throws java.sql.SQLException
     */
    public static ArrayList<L1Prescale> loadAll(final int l1MenuId) throws SQLException {
        ArrayList<L1Prescale> psSets = new ArrayList<>();
        ConnectionManager mgr = ConnectionManager.getInstance();
        String query = "SELECT L1PS_ID, L1TM2PS_PRESCALE_SET_ID, "
                + "L1TM2PS_TRIGGER_MENU_ID "
                + "FROM L1_PRESCALE_SET, L1_TM_TO_PS "
                + "WHERE "
                + "L1TM2PS_TRIGGER_MENU_ID=? "
                + "AND "
                + "L1TM2PS_PRESCALE_SET_ID=L1PS_ID "
                + "ORDER BY L1PS_ID DESC";

        query = mgr.fix_schema_name(query);
        try (PreparedStatement ps = mgr.getConnection().prepareStatement(query)) {
            ps.setInt(1, l1MenuId);
            try (ResultSet rset = ps.executeQuery()) {
                while (rset.next()) {
                    L1Prescale l1ps = new L1Prescale(rset.getInt("L1PS_ID"));
                    psSets.add(l1ps);
                }
                rset.close();
                ps.close();
            }
        }

        return psSets;
    }

    @Override
    public String getTableName() {
        return L1Prescale.TABLE_NAME;
    }

    /**
     *
     * @param type
     */
    public void set_type(String type) {
        keyValue.put("TYPE", type);
    }

    /**
     *
     * @return
     */
    public String get_type() {
        return (String) keyValue.get("TYPE");
    }

    /**
     *
     * @param partition
     */
    public void set_partition(Integer partition) {
         keyValue.put("PARTITION", partition);
    }
   
    /**
     *
     * @return
     */
    public Integer get_partition() {
         return (Integer) keyValue.get("PARTITION");
    }
}
