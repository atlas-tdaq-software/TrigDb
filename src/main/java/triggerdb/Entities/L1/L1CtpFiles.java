package triggerdb.Entities.L1;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;

/**
 * L1 CtpFiles class, holds the various L1CtpFiles.
 * The files are held in memory as Strings, but are written
 * to the DB as 'clobs'.
 * Note there is a special case for the 'Empty' file set,
 * where no entries (other than name & ID) are written to the db.
 *
 * @author markowen
 */
public final class L1CtpFiles extends AbstractTable {

    /// Store in memory the contents of the files
    private String s_lut = null;
    private String s_cam = null;
    private String s_mon_sel_slot7 = null;
    private String s_mon_sel_slot8 = null;
    private String s_mon_sel_slot9 = null;
    private String s_mon_sel_ctpmon = null;
    private String s_mon_dec_slot7 = null;
    private String s_mon_dec_slot8 = null;
    private String s_mon_dec_slot9 = null;
    private String s_mon_dec_ctpmon = null;

    ///Normal constructor.

    /**
     *
     */
    public L1CtpFiles() {
        super("L1CF_");
    }

    /**
     *
     * @param input_id
     * @throws SQLException
     */
    public L1CtpFiles(int input_id) throws SQLException {
        super(input_id, "L1CF_");
        if (input_id > 0) {
            forceLoad();
        }
    }

    /**
     * Function initializes all internal strings to be empty.
     * Needed for the empty records.
     */
    private boolean initEmpty() {
        if (get_name().equals("Empty")) {
            s_lut = "";
            s_cam = "";
            s_mon_sel_slot7 = "";
            s_mon_sel_slot8 = "";
            s_mon_sel_slot9 = "";
            s_mon_sel_ctpmon = "";
            s_mon_dec_slot7 = "";
            s_mon_dec_slot8 = "";
            s_mon_dec_slot9 = "";
            s_mon_dec_ctpmon = "";
            return true;
        }
        return false;
    }

    /**
     *
     * @return
     */
    public String get_lut() {
        if (s_lut == null) {
            if (!initEmpty()) {
                s_lut = ConnectionManager.getInstance().get_clob(getTableName(), tablePrefix, get_id(), "LUT");
            }
        }
        return s_lut;
    }

    /**
     *
     * @return
     */
    public String get_cam() {
        if (s_cam == null) {
            if (!initEmpty()) {
                s_cam = ConnectionManager.getInstance().get_clob(getTableName(), tablePrefix, get_id(), "CAM");
            }
        }
        return s_cam;
    }

    /**
     *
     * @return
     */
    public String get_mon_sel_slot7() {
        if (s_mon_sel_slot7 == null) {
            if (!initEmpty()) {
                s_mon_sel_slot7 = ConnectionManager.getInstance().get_clob(getTableName(), tablePrefix, get_id(), "MON_SEL_SLOT7");
            }
        }
        return s_mon_sel_slot7;
    }

    /**
     *
     * @return
     */
    public String get_mon_sel_slot8() {
        if (s_mon_sel_slot8 == null) {
            if (!initEmpty()) {
                s_mon_sel_slot8 = ConnectionManager.getInstance().get_clob(getTableName(), tablePrefix, get_id(), "MON_SEL_SLOT8");
            }
        }
        return s_mon_sel_slot8;
    }

    /**
     *
     * @return
     */
    public String get_mon_sel_slot9() {
        if (s_mon_sel_slot9 == null) {
            if (!initEmpty()) {
                s_mon_sel_slot9 = ConnectionManager.getInstance().get_clob(getTableName(), tablePrefix, get_id(), "MON_SEL_SLOT9");
            }
        }
        return s_mon_sel_slot9;
    }

    /**
     *
     * @return
     */
    public String get_mon_sel_ctpmon() {
        if (s_mon_sel_ctpmon == null) {
            if (!initEmpty()) {
                s_mon_sel_ctpmon = ConnectionManager.getInstance().get_clob(getTableName(), tablePrefix, get_id(), "MON_SEL_CTPMON");
            }
        }
        return s_mon_sel_ctpmon;
    }

    /**
     *
     * @return
     */
    public String get_mon_dec_slot7() {
        if (s_mon_dec_slot7 == null) {
            if (!initEmpty()) {
                s_mon_dec_slot7 = ConnectionManager.getInstance().get_clob(getTableName(), tablePrefix, get_id(), "MON_DEC_SLOT7");
            }
        }
        return s_mon_dec_slot7;
    }

    /**
     *
     * @return
     */
    public String get_mon_dec_slot8() {
        if (s_mon_dec_slot8 == null) {
            if (!initEmpty()) {
                s_mon_dec_slot8 = ConnectionManager.getInstance().get_clob(getTableName(), tablePrefix, get_id(), "MON_DEC_SLOT8");
            }
        }
        return s_mon_dec_slot8;
    }

    /**
     *
     * @return
     */
    public String get_mon_dec_slot9() {
        if (s_mon_dec_slot9 == null) {
            if (!initEmpty()) {
                s_mon_dec_slot9 = ConnectionManager.getInstance().get_clob(getTableName(), tablePrefix, get_id(), "MON_DEC_SLOT9");
            }
        }
        return s_mon_dec_slot9;
    }

    /**
     *
     * @return
     */
    public String get_mon_dec_ctpmon() {
        if (s_mon_dec_ctpmon == null) {
            if (!initEmpty()) {
                s_mon_dec_ctpmon = ConnectionManager.getInstance().get_clob(getTableName(), tablePrefix, get_id(), "MON_DEC_CTPMON");
            }
        }
        return s_mon_dec_ctpmon;
    }
    //Functions to get CTP files form file when made from TMC

    /**
     *
     * @param filename
     * @return
     */
    public String get_lut_fromfile(String filename) {
        if (!initEmpty()) {
            s_lut = readFile(filename);
        }
        return s_lut;
    }

    /**
     *
     * @param filename
     * @return
     */
    public String get_cam_fromfile(String filename) {
        if (!initEmpty()) {
            s_cam = readFile(filename);
        }
        return s_cam;
    }

    /**
     *
     * @param filename
     * @return
     */
    public String get_mon_sel_slot7_fromfile(String filename) {
        if (!initEmpty()) {
            s_mon_sel_slot7 = readFile(filename);
        }
        return s_mon_sel_slot7;
    }

    /**
     *
     * @param filename
     * @return
     */
    public String get_mon_sel_slot8_fromfile(String filename) {
        if (!initEmpty()) {
            s_mon_sel_slot8 = readFile(filename);
        }
        return s_mon_sel_slot8;
    }

    /**
     *
     * @param filename
     * @return
     */
    public String get_mon_sel_slot9_fromfile(String filename) {
        if (!initEmpty()) {
            s_mon_sel_slot9 = readFile(filename);
        }
        return s_mon_sel_slot9;
    }

    /**
     *
     * @param filename
     * @return
     */
    public String get_mon_sel_ctpmon_fromfile(String filename) {
        if (!initEmpty()) {
            s_mon_sel_ctpmon = readFile(filename);
        }
        return s_mon_sel_ctpmon;
    }

    /**
     *
     * @param filename
     * @return
     */
    public String get_mon_dec_slot7_fromfile(String filename) {
        if (!initEmpty()) {
            s_mon_dec_slot7 = readFile(filename);
        }
        return s_mon_dec_slot7;
    }

    /**
     *
     * @param filename
     * @return
     */
    public String get_mon_dec_slot8_fromfile(String filename) {
        if (!initEmpty()) {
            s_mon_dec_slot8 = readFile(filename);
        }
        return s_mon_dec_slot8;
    }

    /**
     *
     * @param filename
     * @return
     */
    public String get_mon_dec_slot9_fromfile(String filename) {
        if (!initEmpty()) {
            s_mon_dec_slot9 = readFile(filename);
        }
        return s_mon_dec_slot9;
    }

    /**
     *
     * @param filename
     * @return
     */
    public String get_mon_dec_ctpmon_fromfile(String filename) {
        if (!initEmpty()) {
            s_mon_dec_ctpmon = readFile(filename);
        }
        return s_mon_dec_ctpmon;
    }

    // These functions set the files from input txt files.
    // These functions only change the internal Strings, to save to the db the save() function should be used.

    /**
     *
     * @param filename
     */
    public void set_lut_fromfile(String filename) {
        s_lut = readFile(filename);
    }

    /**
     *
     * @param filename
     */
    public void set_cam_fromfile(String filename) {
        s_cam = readFile(filename);
    }

    /**
     *
     * @param filename
     */
    public void set_mon_sel_slot7_fromfile(String filename) {
        s_mon_sel_slot7 = readFile(filename);
    }

    /**
     *
     * @param filename
     */
    public void set_mon_sel_slot8_fromfile(String filename) {
        s_mon_sel_slot8 = readFile(filename);
    }

    /**
     *
     * @param filename
     */
    public void set_mon_sel_slot9_fromfile(String filename) {
        s_mon_sel_slot9 = readFile(filename);
    }

    /**
     *
     * @param filename
     */
    public void set_mon_sel_ctpmon_fromfile(String filename) {
        s_mon_sel_ctpmon = readFile(filename);
    }

    /**
     *
     * @param filename
     */
    public void set_mon_dec_slot7_fromfile(String filename) {
        s_mon_dec_slot7 = readFile(filename);
    }

    /**
     *
     * @param filename
     */
    public void set_mon_dec_slot8_fromfile(String filename) {
        s_mon_dec_slot8 = readFile(filename);
    }

    /**
     *
     * @param filename
     */
    public void set_mon_dec_slot9_fromfile(String filename) {
        s_mon_dec_slot9 = readFile(filename);
    }

    /**
     *
     * @param filename
     */
    public void set_mon_dec_ctpmon_fromfile(String filename) {
        s_mon_dec_ctpmon = readFile(filename);
    }

    /**
     * This function loads all the files from the db if they
     * have not already been loaded.
     */
    public void get_all() {
        get_lut();
        get_cam();
        get_mon_sel_slot7();
        get_mon_sel_slot8();
        get_mon_sel_slot9();
        get_mon_sel_ctpmon();
        get_mon_dec_slot7();
        get_mon_dec_slot8();
        get_mon_dec_slot9();
        get_mon_dec_ctpmon();
    }

    /**
     * This function writes all the clobs to the DB. It assumes the corresponding Strings
     * have been loaded into memory.
     */
    private void set_all_clobs_from_memory() {
        ConnectionManager.getInstance().set_clob_from_str(getTableName(), "L1CF_LUT", get_lut(), tablePrefix, get_id());
        ConnectionManager.getInstance().set_clob_from_str(getTableName(), "L1CF_CAM", get_cam(), tablePrefix, get_id());
        ConnectionManager.getInstance().set_clob_from_str(getTableName(), "L1CF_MON_SEL_SLOT7", get_mon_sel_slot7(), tablePrefix, get_id());
        ConnectionManager.getInstance().set_clob_from_str(getTableName(), "L1CF_MON_SEL_SLOT8", get_mon_sel_slot8(), tablePrefix, get_id());
        ConnectionManager.getInstance().set_clob_from_str(getTableName(), "L1CF_MON_SEL_SLOT9", get_mon_sel_slot9(), tablePrefix, get_id());
        ConnectionManager.getInstance().set_clob_from_str(getTableName(), "L1CF_MON_SEL_CTPMON", get_mon_sel_ctpmon(), tablePrefix, get_id());
        ConnectionManager.getInstance().set_clob_from_str(getTableName(), "L1CF_MON_DEC_SLOT7", get_mon_dec_slot7(), tablePrefix, get_id());
        ConnectionManager.getInstance().set_clob_from_str(getTableName(), "L1CF_MON_DEC_SLOT8", get_mon_dec_slot8(), tablePrefix, get_id());
        ConnectionManager.getInstance().set_clob_from_str(getTableName(), "L1CF_MON_DEC_SLOT9", get_mon_dec_slot9(), tablePrefix, get_id());
        ConnectionManager.getInstance().set_clob_from_str(getTableName(), "L1CF_MON_DEC_CTPMON", get_mon_dec_ctpmon(), tablePrefix, get_id());
    }

    /**
     * Save function.  Tricky.
     * Always create a new record. Update is not allowed.
     * 
     * @return the id of the new record
     * @throws java.sql.SQLException
     */
    @Override
    public int save() throws SQLException {
        boolean createnew = false;

        if (get_name().equals("Empty")) {
            String query = "SELECT L1CF_ID FROM L1_CTP_FILES WHERE L1CF_NAME='Empty'";
            query = ConnectionManager.getInstance().fix_schema_name(query);
            logger.fine(query);
            PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
            ResultSet rset = ps.executeQuery();
            int foundatid = -1;

            while (rset.next()) {
                foundatid = rset.getInt("L1CF_ID");
            }

            if (foundatid == -1) {
                createnew = true;
            }

            rset.close();
            ps.close();

            set_id(foundatid);

            if (!createnew) {
                return foundatid;
            }
        } else { // not an empty L1 CTP Files object

            /// Load all existing ctp file objects
            ///// + reverse order so check latest first for faster matches
            String query = "SELECT L1CF_ID FROM L1_CTP_FILES"
                                + " ORDER BY L1CF_ID DESC";
            query = ConnectionManager.getInstance().fix_schema_name(query);
            logger.fine(query);
            PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
            ResultSet rset = ps.executeQuery();
            
            /// compare current file to those in DB to search for a match
            ///// Load each group of L1CtpFiles individually to reduce memory usage
            ///// Once loaded compare each individual file within the L1CtpFiles
            
            L1CtpFiles match = null;
            while (rset.next()) {
                L1CtpFiles testfile = new L1CtpFiles(rset.getInt("L1CF_ID"));
                
                // Ignore anything with name 'Empty'
                if (testfile.get_name().equals("Empty")) {
                    continue;
                }
                if (!testfile.get_cam().equals(this.get_cam())) {
                    continue;
                }
                // Do LUT check after CAM as former are the larger file size
                if (!testfile.get_lut().equals(this.get_lut())) {
                    continue;
                }
                if (!testfile.get_mon_sel_slot7().equals(this.get_mon_sel_slot7())) {
                    continue;
                }
                if (!testfile.get_mon_sel_slot8().equals(this.get_mon_sel_slot8())) {
                    continue;
                }
                if (!testfile.get_mon_sel_slot9().equals(this.get_mon_sel_slot9())) {
                    continue;
                }
                if (!testfile.get_mon_sel_ctpmon().equals(this.get_mon_sel_ctpmon())) {
                    continue;
                }
                if (!testfile.get_mon_dec_slot7().equals(this.get_mon_dec_slot7())) {
                    continue;
                }
                if (!testfile.get_mon_dec_slot8().equals(this.get_mon_dec_slot8())) {
                    continue;
                }
                if (!testfile.get_mon_dec_slot9().equals(this.get_mon_dec_slot9())) {
                    continue;
                }
                if (!testfile.get_mon_dec_ctpmon().equals(this.get_mon_dec_ctpmon())) {
                    continue;
                }
                // If we get to here then this file is a match
                logger.log(Level.INFO, "Found match for Ctp Files, id = {0}", testfile.get_id());
                match = testfile;
                break;
                
            }
            rset.close();
            ps.close();

            if (match != null) {
                return match.get_id();
            }
        }

        logger.info("Saving new L1 ctp files");
        set_id(-1);

        int version = 1 + ConnectionManager.getInstance().getMaxVersion(getTableName(), tablePrefix, get_name());
        keyValue.put("VERSION", version);
        set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix, get_id(), keyValue));

        /// For the case of non-empty files, need to save the clobs that are in memory
        if (!get_name().equals("Empty")) {
            logger.info("L1 ctp files not empty, setting the clobs from memory");
            set_all_clobs_from_memory();
        }

        return get_id();
    }

    ///String representation of this object.
    /**
     * So the tree view knows what information to display in the tree
     *
     * @return A string containing the get_id(), name and version of the record
     */
    @Override
    public String toString() {
        if (get_id() == -1) {
            return "L1 CTP Files";
        }
        return "L1 CTP FILES: get_id()=" + get_id() + ", name=" + get_name() + ", ver=" + get_version();
    }

    ///Create a copy of this object.
    @Override
    public Object clone() {
        L1CtpFiles copy = new L1CtpFiles();
        copy.set_name(get_name());
        copy.set_version(get_version());
        return copy;
    }

    /**
     * This function reads all the contents of file 'filename' and returns the
     * contents as a String.
     * @param filename file to be read
     * @return String of the file contents, "" in case of error.
     */
    static String readFile(String filename) {
        try {
            File fFile = new File(filename);
            FileInputStream fis = new FileInputStream(fFile);
            InputStreamReader irdr = new InputStreamReader(fis);
            BufferedReader brdr = new BufferedReader(irdr, 8192);

            String line;
            int nlines = 0;
            StringBuilder buffer = new StringBuilder();
            logger.fine("going to read file");
            while ((line = brdr.readLine()) != null) {
                nlines++;
                buffer.append(line).append("\n");
            }
            logger.log(Level.FINE, "read lines:  {0}", nlines);
            brdr.close();
            return buffer.toString();
        } catch (IOException e) {
            logger.log(Level.WARNING, "Reading file {0} failed.", filename);
            return "";
        }

    }

    @Override
    public String getTableName() {
        return "L1_CTP_FILES";
    }
    
    /**
     *
     * @return
     * @throws SQLException
     */
    public List<L1CtpFiles> getL1CtpFiles() throws SQLException
    {
        List<L1CtpFiles> list = new ArrayList<>();
        String query;
        query = super.getQueryString();
        query += " ORDER BY L1CF_ID ASC";
        query = ConnectionManager.getInstance().fix_schema_name(query);

        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ResultSet rset = ps.executeQuery();

        while (rset.next()) {
            L1CtpFiles cf = new L1CtpFiles(-1);
            cf.loadFromRset(rset);
            list.add(cf);
        }

        rset.close();
        ps.close();

        return list;
    }
}
