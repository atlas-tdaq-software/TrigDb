package triggerdb.Entities.L1;

import java.sql.SQLException;
import java.util.ArrayList;

import triggerdb.Entities.AbstractTable;
import triggerdb.Connections.ConnectionManager;

/**
 *
 * @author William
 */
public final class L1CaloMinTob extends AbstractTable {

    /** Message Log */
    public L1CaloMinTob() {
        super(-1, "L1CMT_");

        keyValue.putFirst("THR_TYPE", "thr type", "thr type");
        keyValue.putFirst("WINDOW", 0, "window");
        keyValue.putFirst("PT_MIN", -1, "pt min");
        keyValue.putFirst("ETA_MIN", -1, "eta min");
        keyValue.putFirst("ETA_MAX", -1, "eta max");
        keyValue.putFirst("PRIORITY", -1, "priority");
        
        keyValue.remove("NAME");
        keyValue.remove("VERSION");
        
    }

    /** Message Log
     * @param input_id
     * @throws java.sql.SQLException */
    public L1CaloMinTob(int input_id) throws SQLException {
        super(input_id, "L1CMT_");

        keyValue.putFirst("THR_TYPE", "thr type", "thr type");
        keyValue.putFirst("WINDOW", 0, "window");
        keyValue.putFirst("PT_MIN", -1, "pt min");
        keyValue.putFirst("ETA_MIN", -1, "eta min");
        keyValue.putFirst("ETA_MAX", -1, "eta max");
        keyValue.putFirst("PRIORITY", -1, "priority");
        
        keyValue.remove("NAME");
        keyValue.remove("VERSION");
        
        if (input_id > 0) {
            forceLoad();
        }
    }

    /**
     *
     * @return
     */
    public String get_thr_type() {
        return (String) keyValue.get("THR_TYPE");
    }
    
    /**
     *
     * @param inp
     */
    public void set_thr_type(String inp) {
        keyValue.put("THR_TYPE", inp);
    }
    
    /**
     *
     * @return
     */
    public Integer get_window() {
        return (Integer) keyValue.get("WINDOW");
    }

    /**
     *
     * @param inp
     */
    public void set_window(int inp) {
        keyValue.put("WINDOW", inp);
    }
    
    /**
     *
     * @return
     */
    public Integer get_pt_min() {
        return (Integer) keyValue.get("PT_MIN");
    }

    /**
     *
     * @param inp
     */
    public void set_pt_min(int inp) {
        keyValue.put("PT_MIN", inp);
    }
    
    /**
     *
     * @return
     */
    public Integer get_eta_min() {
        return (Integer) keyValue.get("ETA_MIN");
    }

    /**
     *
     * @param inp
     */
    public void set_eta_min(int inp) {
        keyValue.put("ETA_MIN", inp);
    }
    
    /**
     *
     * @return
     */
    public Integer get_eta_max() {
        return (Integer) keyValue.get("ETA_MAX");
    }
    
    /**
     *
     * @param inp
     */
    public void set_eta_max(int inp) {
        keyValue.put("ETA_MAX", inp);
    }

    /**
     *
     * @return
     */
    public Integer get_priority() {
        return (Integer) keyValue.get("PRIORITY");
    }
    
    /**
     *
     * @param inp
     */
    public void set_priority(int inp) {
        keyValue.put("PRIORITY", inp);
    }

    @Override
    public int save() throws SQLException {
        ConnectionManager mgr = ConnectionManager.getInstance();

        ArrayList<Integer> ids = mgr.get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");

        if (ids.isEmpty()) {
            set_id(mgr.save(getTableName(), tablePrefix, get_id(), keyValue));
        } else {
            set_id(ids.get(0));
        }

        return get_id();
    }

    /**
     * So the tree view knows what information to display in the tree
     * 
     * @return A string containing the id, name and version of the record
     */
    @Override
    public String toString() {
        if (get_id() == -1) {
            return "L1 Calo Min Tob";
        }
        return "L1 CALO MIN TOB: id=" + get_id() + ", name=" + get_name() + ", ver=" + get_version();
    }

    @Override
    public Object clone() {
        L1CaloMinTob copy = new L1CaloMinTob();
        copy.set_id(-1);
        copy.set_thr_type(get_thr_type());
        copy.set_window(get_window());
        copy.set_pt_min(get_pt_min());
        copy.set_eta_min(get_eta_min());
        copy.set_eta_max(get_eta_max());
        copy.set_priority(get_priority());
        return copy;
    }

    @Override
    public String getTableName() {
        return "L1_CALO_MIN_TOB";
    }
    
    /**
     * Minimum names for the search results.
     * @return 
     */
    @Override
    public ArrayList<String> get_min_names() {
        ArrayList<String> info = new ArrayList<>();
        info.add("ID");
        info.add("Threshold Type");
        info.add("Window");
        return info;
    }
    
    /**
     * Minimum information for the search results.
     * @return 
     */
    @Override
    public ArrayList<Object> get_min_info() {
        ArrayList<Object> info = new ArrayList<>();
        info.add(get_id());
        info.add(get_thr_type());
        info.add(get_window());
        return info;
    }
}
