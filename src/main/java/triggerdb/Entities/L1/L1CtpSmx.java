package triggerdb.Entities.L1;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;

/**
 * L1 CtpSmx class, holds the various vhdl and smx files.
 * The files are held in memory as Strings, but are written
 * to the DB as 'clobs'.
 * Note there is a special case for the 'Empty' file set,
 * where no entries (other than name & ID) are written to the db.
 *
 * @author markowen
 */
public final class L1CtpSmx extends AbstractTable {

    /// Store in memory the contents of the files.
    private String output = null;
    private String vhdl_slot7 = null;
    private String vhdl_slot8 = null;
    private String vhdl_slot9 = null;
    private String svfi_slot7 = null;
    private String svfi_slot8 = null;
    private String svfi_slot9 = null;

    /**
     *
     */
    public L1CtpSmx() {
        super("L1SMX_");
    }

    /**
     *
     * @param input_id
     * @throws SQLException
     */
    public L1CtpSmx(int input_id) throws SQLException {
        super(input_id, "L1SMX_");
        if (input_id > 0) {
            forceLoad();
        }
    }

    /**
     *
     * @return
     */
    public String get_output() {
        if (output == null) {
            if (!initEmpty()) {
                output = ConnectionManager.getInstance().get_clob(getTableName(), tablePrefix, get_id(), "OUTPUT");
            }
        }
        return output;
    }

    /**
     *
     * @return
     */
    public String get_svfi_slot7() {
        if (svfi_slot7 == null) {
            if (!initEmpty()) {
                svfi_slot7 = ConnectionManager.getInstance().get_clob(getTableName(), tablePrefix, get_id(), "SVFI_SLOT7");
            }
        }
        return svfi_slot7;
    }

    /**
     *
     * @return
     */
    public String get_svfi_slot8() {
        if (svfi_slot8 == null) {
            if (!initEmpty()) {
                svfi_slot8 = ConnectionManager.getInstance().get_clob(getTableName(), tablePrefix, get_id(), "SVFI_SLOT8");
            }
        }
        return svfi_slot8;
    }

    /**
     *
     * @return
     */
    public String get_svfi_slot9() {
        if (svfi_slot9 == null) {
            if (!initEmpty()) {
                svfi_slot9 = ConnectionManager.getInstance().get_clob(getTableName(), tablePrefix, get_id(), "SVFI_SLOT9");
            }
        }
        return svfi_slot9;
    }

    /**
     *
     * @return
     */
    public String get_vhdl_slot7() {
        if (vhdl_slot7 == null) {
            if (!initEmpty()) {
                vhdl_slot7 = ConnectionManager.getInstance().get_clob(getTableName(), tablePrefix, get_id(), "VHDL_SLOT7");
            }
        }
        return vhdl_slot7;
    }

    /**
     *
     * @return
     */
    public String get_vhdl_slot8() {
        if (vhdl_slot8 == null) {
            if (!initEmpty()) {
                vhdl_slot8 = ConnectionManager.getInstance().get_clob(getTableName(), tablePrefix, get_id(), "VHDL_SLOT8");
            }
        }
        return vhdl_slot8;
    }

    /**
     *
     * @return
     */
    public String get_vhdl_slot9() {
        if (vhdl_slot9 == null) {
            if (!initEmpty()) {
                vhdl_slot9 = ConnectionManager.getInstance().get_clob(getTableName(), tablePrefix, get_id(), "VHDL_SLOT9");
            }
        }
        return vhdl_slot9;
    }

    //////////////////
    ////////////// These functions update the internal Strings, based on an input file.
    ////////////// The record should then be saved with the save() function.

    /**
     *
     * @param filename
     */
    public void set_output_fromfile(String filename) {
        output = L1CtpFiles.readFile(filename);
        
        
        
    }

    /**
     *
     * @param filename
     */
    public void set_svfi_slot7_fromfile(String filename) {
        svfi_slot7 = L1CtpFiles.readFile(filename);
    }

    /**
     *
     * @param filename
     */
    public void set_svfi_slot8_fromfile(String filename) {
        svfi_slot8 = L1CtpFiles.readFile(filename);
    }

    /**
     *
     * @param filename
     */
    public void set_svfi_slot9_fromfile(String filename) {
        svfi_slot9 = L1CtpFiles.readFile(filename);
    }

    /**
     *
     * @param filename
     */
    public void set_vhdl_slot7_fromfile(String filename) {
        vhdl_slot7 = L1CtpFiles.readFile(filename);
    }

    /**
     *
     * @param filename
     */
    public void set_vhdl_slot8_fromfile(String filename) {
        vhdl_slot8 = L1CtpFiles.readFile(filename);
    }

    /**
     *
     * @param filename
     */
    public void set_vhdl_slot9_fromfile(String filename) {
        vhdl_slot9 = L1CtpFiles.readFile(filename);
    }

    //////////////////
    ////////////// These function update the database entry with the given file.
    ////////////// Only allow this for the SVFI files

    /**
     *
     * @param filename
     */
    public void update_svfi_slot7(String filename) {
        if (get_id() > 0) {
            ConnectionManager.getInstance().set_clob(getTableName(), "L1SMX_SVFI_SLOT7", filename, tablePrefix, get_id());
            get_svfi_slot7();
        } else {
            logger.severe("Could not update svfi slot7 since ID <= 0");
        }
    }

    /**
     *
     * @param filename
     */
    public void update_svfi_slot8(String filename) {
        if (get_id() > 0) {
            ConnectionManager.getInstance().set_clob(getTableName(), "L1SMX_SVFI_SLOT8", filename, tablePrefix, get_id());
            get_svfi_slot8();
        } else {
            logger.severe("Could not update svfi slot8 since ID <= 0");
        }
    }

    /**
     *
     * @param filename
     */
    public void update_svfi_slot9(String filename) {
        if (get_id() > 0) {
            ConnectionManager.getInstance().set_clob(getTableName(), "L1SMX_SVFI_SLOT9", filename, tablePrefix, get_id());
            get_svfi_slot9();
        } else {
            logger.severe("Could not update svfi slot9 since ID <= 0");
        }
    }

    /// Function loads all the files from the db.

    /**
     *
     */
    public void get_all() {
        get_output();
        get_vhdl_slot7();
        get_vhdl_slot8();
        get_vhdl_slot9();
        get_svfi_slot7();
        get_svfi_slot8();
        get_svfi_slot9();
    }

    /**
     * Function initializes all internal strings to be empty.
     * Needed for the empty records.
     */
    private boolean initEmpty() {
        if (get_name().equals("Empty")) {
            output = "";
            vhdl_slot7 = "";
            vhdl_slot8 = "";
            vhdl_slot9 = "";
            svfi_slot7 = "";
            svfi_slot8 = "";
            svfi_slot9 = "";
            return true;
        }
        return false;
    }

    /**
     * This function writes all the clobs to the DB. It assumes the corresponding Strings
     * have been loaded into memory.
     * In the case of the Strings being null (can happen when uploading only vhdl files)
     * we store empty Strings in these places. These will presumably be updated later once the
     * svfi files have been created.
     */
    private void set_all_clobs_from_memory() {
        ConnectionManager.getInstance().set_clob_from_str(getTableName(), "L1SMX_OUTPUT",     get_output(),     tablePrefix, get_id());
        ConnectionManager.getInstance().set_clob_from_str(getTableName(), "L1SMX_VHDL_SLOT7", get_vhdl_slot7(), tablePrefix, get_id());
        ConnectionManager.getInstance().set_clob_from_str(getTableName(), "L1SMX_VHDL_SLOT8", get_vhdl_slot8(), tablePrefix, get_id());
        ConnectionManager.getInstance().set_clob_from_str(getTableName(), "L1SMX_VHDL_SLOT9", get_vhdl_slot9(), tablePrefix, get_id());
        ConnectionManager.getInstance().set_clob_from_str(getTableName(), "L1SMX_SVFI_SLOT7", get_svfi_slot7(), tablePrefix, get_id());
        ConnectionManager.getInstance().set_clob_from_str(getTableName(), "L1SMX_SVFI_SLOT8", get_svfi_slot8(), tablePrefix, get_id());
        ConnectionManager.getInstance().set_clob_from_str(getTableName(), "L1SMX_SVFI_SLOT9", get_svfi_slot9(), tablePrefix, get_id());
    }

    /**
     * Save the CTP SMX record.  We never want to overwrite an existing record.
     * Hence we set the id = -1 in the save function to force a new one to be
     * created.
     *
     * @return id of the new record
     * @throws java.sql.SQLException
     */
    @Override
    public int save() throws SQLException {
        boolean createnew = false;

        if (get_name().equals("Empty")) {
            String query = "SELECT L1SMX_ID FROM L1_CTP_SMX WHERE L1SMX_NAME='Empty'";
            query = ConnectionManager.getInstance().fix_schema_name(query);
            int foundatid;
            try ( PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query); 
                  ResultSet rset = ps.executeQuery()) {
                foundatid = -1;
                while (rset.next()) {
                    foundatid = rset.getInt("L1SMX_ID");
                }   
                if (foundatid == -1) {
                    createnew = true;
                }
                rset.close();
                ps.close();
            }
            if (!createnew) {
                return foundatid;
            }
        } else { /// not an empty record
            /// Load all existing ctp file objects
            String query = "SELECT L1SMX_ID FROM L1_CTP_SMX ORDER BY L1SMX_ID DESC"; // TODO change to Descending
            ConnectionManager mgr = ConnectionManager.getInstance();
            query = mgr.fix_schema_name(query);
            logger.fine(query);
            ArrayList<L1CtpSmx> allfiles;
            try (PreparedStatement ps = mgr.getConnection().prepareStatement(query); ResultSet rset = ps.executeQuery()) {
                allfiles = new ArrayList<>();
                while (rset.next()) {
                    allfiles.add(new L1CtpSmx(rset.getInt("L1SMX_ID")));
                }
                rset.close();
                ps.close();
            }

            //// Look for matches
            L1CtpSmx match = null;
            for (L1CtpSmx testfiles : allfiles) {
                // Ignore anything with name 'Empty'
                if (testfiles.get_name().equals("Empty")) {
                    continue;
                }

                // Test each individual file
                if (!testfiles.get_output().equals(this.get_output())) {
                    continue;
                }
                if (!testfiles.get_svfi_slot7().equals(this.get_svfi_slot7())) {
                    continue;
                }
                if (!testfiles.get_svfi_slot8().equals(this.get_svfi_slot8())) {
                    continue;
                }
                if (!testfiles.get_svfi_slot9().equals(this.get_svfi_slot9())) {
                    continue;
                }
                if (!testfiles.get_vhdl_slot7().equals(this.get_vhdl_slot7())) {
                    continue;
                }
                if (!testfiles.get_vhdl_slot8().equals(this.get_vhdl_slot8())) {
                    continue;
                }
                if (!testfiles.get_vhdl_slot9().equals(this.get_vhdl_slot9())) {
                    continue;
                }


                // If we get to here this file is a match
                logger.log(Level.INFO, "Found match for Smx Files, id = {0}", testfiles.get_id());
                match = testfiles;
                break;
            }
            if (match != null) {
                logger.log(Level.INFO, "Matching L1CtpSmx, id = {0}", match.get_id());
                return match.get_id();
            }
        }


        set_id(-1);
        int version = 1 + ConnectionManager.getInstance().getMaxVersion(getTableName(), tablePrefix, get_name());
        keyValue.put("VERSION", version);

        logger.log(Level.INFO, "Saving new L1CtpSmx files with name ''{0}'', version {1}", new Object[]{get_name(), version});

        set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix, get_id(), keyValue));

        /// For the case of non-empty files, need to save the clobs that are in memory
        if (!get_name().equals("Empty")) {
            logger.info("L1 smx files not empty, setting the clobs from memory");
            set_all_clobs_from_memory();
        }

        return get_id();
    }

    /**
     * So the tree view knows what information to display in the tree
     *
     * @return A string containing the id, name and version of the record
     */
    @Override
    public String toString() {
        if (get_id() == -1) {
            return "L1 CTP SMX";
        }
        return "L1 CTP SMX: id=" + get_id() + ", name=" + get_name() + ", ver=" + get_version();
    }

    @Override
    public Object clone() {
        L1CtpSmx copy = new L1CtpSmx();
        copy.set_name(get_name());
        copy.set_version(get_version());
        copy.set_id(-1);
        return copy;
    }

    @Override
    public String getTableName() {
        return "L1_CTP_SMX";
    }
        
    /**
     *
     * @return
     * @throws SQLException
     */
    public List<L1CtpSmx> getL1CtpSmx() throws SQLException
    {
        List<L1CtpSmx> list = new ArrayList<>();
        String query;
        query = super.getQueryString();
        query += " ORDER BY L1SMX_ID ASC";
        query = ConnectionManager.getInstance().fix_schema_name(query);

        try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query); ResultSet rset = ps.executeQuery()) {
            while (rset.next()) {
                L1CtpSmx cf = new L1CtpSmx(-1);
                cf.loadFromRset(rset);
                list.add(cf);
            }
            rset.close();
            ps.close();
        }

        return list;
    }
}