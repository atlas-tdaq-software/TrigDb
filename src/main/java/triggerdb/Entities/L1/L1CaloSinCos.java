package triggerdb.Entities.L1;

import java.sql.SQLException;

import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;

/**
 *
 * @author William
 */
public final class L1CaloSinCos extends AbstractTable {

    /** Message Log */
    public L1CaloSinCos() {
        super("L1CSC_");
        keyValue.putFirst("ETA_MIN", -1, "Eta Min");
        keyValue.putFirst("ETA_MAX", -1, "Eta Max");
        keyValue.putFirst("PHI_MIN", -1, "Phi Min");
        keyValue.putFirst("PHI_MAX", -1, "Phi Max");
        for (int i = 1; i < 9; ++i) {
            keyValue.putFirst("VAL" + i, -1, "Value " + i);
        }
    }

    /** Message Log
     * @param input_id
     * @throws java.sql.SQLException */
    public L1CaloSinCos(int input_id) throws SQLException {
        super(input_id, "L1CSC_");

        keyValue.putFirst("ETA_MIN", -1, "Eta Min");
        keyValue.putFirst("ETA_MAX", -1, "Eta Max");
        keyValue.putFirst("PHI_MIN", -1, "Phi Min");
        keyValue.putFirst("PHI_MAX", -1, "Phi Max");

        for (int i = 1; i < 9; ++i) {
            keyValue.putFirst("VAL" + i, -1, "Value " + i);
        }

        if (input_id > 0) {
            forceLoad();
        }
    }

    /**
     *
     * @param i
     * @return
     */
    public Integer get_val(int i) {
        return (Integer) keyValue.get("VAL" + i);
    }

    /**
     *
     * @return
     */
    public Integer get_eta_min() {
        return (Integer) keyValue.get("ETA_MIN");
    }

    /**
     *
     * @return
     */
    public Integer get_eta_max() {
        return (Integer) keyValue.get("ETA_MAX");
    }

    /**
     *
     * @return
     */
    public Integer get_phi_min() {
        return (Integer) keyValue.get("PHI_MIN");
    }

    /**
     *
     * @return
     */
    public Integer get_phi_max() {
        return (Integer) keyValue.get("PHI_MAX");
    }

    /**
     *
     * @param inp
     */
    public void set_eta_min(int inp) {
        keyValue.put("ETA_MIN", inp);
    }

    /**
     *
     * @param inp
     */
    public void set_eta_max(int inp) {
        keyValue.put("ETA_MAX", inp);
    }

    /**
     *
     * @param inp
     */
    public void set_phi_min(int inp) {
        keyValue.put("PHI_MIN", inp);
    }

    /**
     *
     * @param inp
     */
    public void set_phi_max(int inp) {
        keyValue.put("PHI_MAX", inp);
    }

    /**
     *
     * @param i
     * @param inp
     */
    public void set_val(int i, int inp) {
        keyValue.put("VAL" + i, inp);
    }

    @Override
    public int save() throws SQLException {
        set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix, get_id(), keyValue));

        return get_id();
    }

    /**
     * So the tree view knows what information to display in the tree
     * 
     * @return A string containing the id, name and version of the record
     */
    @Override
    public String toString() {
        if (get_id() == -1) {
            return "L1 Calo Sin Cos";
        }
        return "L1 CALO SIN COS: id=" + get_id() + ", name=" + get_name() + ", ver=" + get_version();
    }

    @Override
    public Object clone() {
        L1CaloSinCos copy = new L1CaloSinCos();
        copy.set_id(-1);
        copy.set_eta_min(get_eta_min());
        copy.set_eta_max(get_eta_max());
        copy.set_phi_min(get_phi_min());
        copy.set_phi_max(get_phi_max());
        copy.set_name(get_name());
        copy.set_version(get_version());
        for (int i = 1; i < 9; ++i) {
            copy.set_val(i, get_val(i));
        }
        return copy;
    }

    @Override
    public String getTableName() {
        return "L1_CALO_SIN_COS";
    }
}
