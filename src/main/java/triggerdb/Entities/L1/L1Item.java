package triggerdb.Entities.L1;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.logging.Level;
import javax.swing.tree.DefaultMutableTreeNode;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.HLT.HLTMaster;

/**
 * Hold a row from L1_TRIGGER_ITEM
 * Implementation for the L1_TRIGGER_ITEM table in the database.  A single
 * instance of this object holds a row from the database.  This class also
 * contains all the editors to change the properties of this row.
 * 
 * @author Simon Head
 */
public final class L1Item extends AbstractTable {

    ///Used by the tree for all the nodes of thresholds.
    private ArrayList<DefaultMutableTreeNode> tree_items = new ArrayList<>();
    ///Reference of the L1Master, so that we can use it to attach new L1Items to.

    /**
     *
     */
    public L1Master l1master = null;
    ///Reference of the HLTMaster, so that we can use it to attach new Chains to.

    /**
     *
     */
    public HLTMaster hltmaster = null;
    ///All the L1Threshold records this L1Item uses.
    private ArrayList<L1Threshold> thresholds;

    /**
     * Create a L1Item object.
     * Standard constructor. If an ID of -1 is supplied we create a new, empty
     * record and don't attempt to load anything from the database.  If the ID
     * supplied is greater than 0 we load this record from the database.
     */
    public L1Item() {
        super(-1, "L1TI_");
        DefineKeyValues();
        thresholds = new ArrayList<>();
    }
    
    /**
     * Create a L1Item object.
     * Standard constructor. If an ID of -1 is supplied we create a new, empty
     * record and don't attempt to load anything from the database.  If the ID
     * supplied is greater than 0 we load this record from the database.
     * 
     * @param input_id The ID to load or -1.
     * @throws java.sql.SQLException
     */
    public L1Item(int input_id) throws SQLException {
        super(input_id, "L1TI_");
        DefineKeyValues();
        
        if (input_id > 0) {
            forceLoad();
            setThresholds(getThresholdsFromDb());
        }
}

    private void DefineKeyValues() {
        keyValue.putFirst("PRIORITY", "LOW", "Priority");
        keyValue.putFirst("DEFINITION", "(1)", "Definition");
        keyValue.putFirst("GROUP", 1, "Group");
        keyValue.putFirst("TRIGGER_TYPE", 0, "Trigger Type");
        keyValue.putFirst("CTP_ID", 1, "CTP ID");
        keyValue.putFirst("COMMENT", "Comment", "Comment");
        keyValue.putFirst("PARTITION", 1, "Partition [1-3]");
        if(ConnectionManager.L1ItemMonitorColumnExists == 1){
                keyValue.putFirst("MONITOR", "None", "Item monitor");
        }
    }
    
    public void initialize_monitor(){
        if(ConnectionManager.L1ItemMonitorColumnExists == 0){
                keyValue.put("MONITOR", "");
        }
        if(ConnectionManager.L1ItemMonitorColumnExists == 1){
                keyValue.put("MONITOR", "None");
        }
}

    ///edit l1 item comment

    /**
     *
     * @throws SQLException
     */
    public void editComment() throws SQLException {

        String comment = get_comment();
        //if(comment==null) comment="~";

        ////System.out.println("PJB item comment changed so saving to DB " + comment);

        String query = "UPDATE L1_TRIGGER_ITEM SET L1TI_COMMENT=? WHERE L1TI_ID=?";
        query = ConnectionManager.getInstance().fix_schema_name(query);

        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ps.setString(1, comment);
        ps.setInt(2, get_id());
        ps.executeUpdate();
        ps.close();
    }


    ///Set the panel where only thresholds should be displayed.
    //public void set_config_threshold_list(ThresholdDisplay td) {
    //    this.td = td;
    // }
    ///Set the L1 Master Table.

    /**
     *
     * @param mast
     */
    public void set_l1_master(L1Master mast) {
        l1master = mast;
    }

    ///Set the HLT Master Table.

    /**
     *
     * @param mast
     */
    public void set_hlt_master(HLTMaster mast) {
        hltmaster = mast;
    }


    ///Force this L1Item to be read from the database again.
    @Override
    public void forceLoad() throws SQLException {
        tree_items = new ArrayList<>();
        ConnectionManager.getInstance().forceLoad(this);
    }

    /**
     *
     * @return
     */
    public ArrayList<L1Threshold> getThresholds(){
        if (thresholds == null){
            try {
                thresholds = getThresholdsFromDb();
            } catch (SQLException ex) {
                logger.severe("SQLException while loading thresholds.");
            }
        }
        return thresholds;
    }

    /**
     *
     * @param list
     */
    public void setThresholds(ArrayList<L1Threshold> list){
        thresholds = list;
    }
    
    ///Load all the L1Thresholds connected to this L1Item.
    /**
     * Single SQL query to load all the L1Thresholds that belong to this L1Item.
     * Only loads the thresholds the first time it is called.  If you call it
     * again, it'll remember that it has already loaded them.  Calling forceLoad
     * will make it forget and ask the Database again.
     * 
     * @return A vector of L1Thresholds that belong to this L1Item.
     * @throws java.sql.SQLException
     */
    private ArrayList<L1Threshold> getThresholdsFromDb() throws SQLException {
        ArrayList<L1Threshold> thList = new ArrayList<>();
        L1Threshold i = new L1Threshold();
        String query = i.getQueryString();

        query = query.replace("FROM ", ", L1TI2TT_POSITION, L1TI2TT_MULTIPLICITY FROM L1_TRIGGER_ITEM, L1_TI_TO_TT, ");

        String and = " WHERE "
                + "L1TI_ID=? "
                + "AND "
                + "L1TI2TT_TRIGGER_ITEM_ID=L1TI_ID "
                + "AND "
                + "L1TI2TT_TRIGGER_THRESHOLD_ID=L1TT_ID "
                + "ORDER BY "
                + "L1TI2TT_POSITION ASC";

        query = ConnectionManager.getInstance().fix_schema_name(query + and);

        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ps.setInt(1, get_id());
        ResultSet rset = ps.executeQuery();

        while (rset.next()) {
            L1Threshold t = new L1Threshold();
            t.loadFromRset(rset);
            t.set_position(rset.getInt("L1TI2TT_POSITION"));
            t.set_multiplicity(rset.getInt("L1TI2TT_MULTIPLICITY"));
            thList.add(t);
        }

        rset.close();
        ps.close();

        return thList;
    }

    ///Maybe this isn't a good idea. Numbers thresholds 1 to n, for this item.

    /**
     *
     * @throws SQLException
     */
    public void renumberThresholds() throws SQLException {
        if (thresholds != null) {
            int i = 1;
            for (L1Threshold threshold : getThresholds()) {
                threshold.set_position(i);
                ++i;
            }
        }
    }

    ///Get the value of the user defined comment for a L1Item.

    /**
     *
     * @return
     */
    public String get_comment() {
        if (keyValue.get("COMMENT") == null) {
            return "null comment";
        } else {
            return (String) keyValue.get("COMMENT");
        }
    }

    ///Set the user defined comment for this L1Item.

    /**
     *
     * @param inp
     */
    public void set_comment(String inp) {
        keyValue.put("COMMENT", inp);
    }

    ///Get the Trigger Type.

    /**
     *
     * @return
     */
    public String get_trigger_type() {
        Integer ttype_int = (Integer) keyValue.get("TRIGGER_TYPE");
        String ttype_bin = Integer.toBinaryString(ttype_int);
        while (ttype_bin.length() < 8) {
            ttype_bin = "0" + ttype_bin;
        }
        return ttype_bin;
    }

    ///Get the Trigger Type as int.

    /**
     *
     * @return
     */
    public int get_trigger_type_i() {
        int ttype_int = (Integer) keyValue.get("TRIGGER_TYPE");
        return ttype_int;
    }

    ///Set the Trigger Type.

    /**
     *
     * @param inp
     */
    public void set_trigger_type(String inp) {
        int radix = 2;
        if (inp.length() < 4) {
            radix = 10;
        }
        Integer ttype_int = Integer.parseInt(inp, radix);
        keyValue.put("TRIGGER_TYPE", ttype_int);
    }

    ///Get the CTP ID.

    /**
     *
     * @return
     */
    public Integer get_ctp_id() {
        return (Integer) keyValue.get("CTP_ID");
    }

    // Get the partition

    /**
     *
     * @return
     */
    public Integer get_partition() {
        return (Integer) keyValue.get("PARTITION");
    }

    
    
    ///Get the definition string.
    /**
     * The definition string defines the relationship between the L1Thresholds
     * in this L1Item.  For example an item containing a single threshold
     * will have the definition (1).  If the AND of two thresholds is to be used
     * for the trigger we would have (1&2) where the position in L1_TI_TO_TT
     * should be set as 1, for one threshold and 2 for the other.  For the and
     * of three items we have (1&2&3) etc.
     * 
     * @return String definition.
     */
    public String get_definition() {
        return (String) keyValue.get("DEFINITION");
    }

    ///Set the priority (LOW or HIGH).

    /**
     *
     * @return
     */
    public String get_priority() {
        return (String) keyValue.get("PRIORITY");
    }

    ///Get the group.

    /**
     *
     * @return
     */
    public Integer get_group() {
        return (Integer) keyValue.get("GROUP");
    }
    
    /**
     * Set the Monitor for this item. Can monitor TBP|TAP|TAV|
     * 
     * @return String definition.
     */
    public String get_monitor() {
        if(ConnectionManager.L1ItemMonitorColumnExists == 1){
            return (String) keyValue.get("MONITOR");
        } else {
            String empty = "";
            return empty;
        }
    }

    ///Set the CTP ID. Note that two items can't have the same CTP ID. 0 to 255.

    /**
     *
     * @param inp
     */
    public void set_ctp_id(int inp) {
        keyValue.put("CTP_ID", inp);
    }

    // Set the partition

    /**
     *
     * @param partition
     */
    public void set_partition(Integer partition) {
        keyValue.put("PARTITION", partition);
    }

    ///Set the definition string.

    /**
     *
     * @param inp
     */
    public void set_definition(String inp) {
        keyValue.put("DEFINITION", inp);
    }

    ///Set the priority.

    /**
     *
     * @param inp
     */
    public void set_priority(String inp) {
        keyValue.put("PRIORITY", inp);
    }

    ///Set the group.

    /**
     *
     * @param inp
     */
    public void set_group(Integer inp) {
        keyValue.put("GROUP", inp);
    }
    
    ///Set the monitor

    /**
     *
     * @param inp
     */
    public void set_monitor(String inp) {
        if(ConnectionManager.L1ItemMonitorColumnExists == 1){
            keyValue.put("MONITOR", inp);
        }
    }

    ///Display an L1Item in the tree, and the L1Threshold children.
    /**
     * Add all the children to the tree.
     * @throws java.sql.SQLException
     */
    @Override
    public void addToTree(DefaultMutableTreeNode treeNode, int counter) throws SQLException {
        --counter;

        if (counter >= 0) {

            if (tree_items.isEmpty()) {
                for (L1Threshold threshold : getThresholds()) {
                    DefaultMutableTreeNode anotherLayer = new DefaultMutableTreeNode(threshold);
                    treeNode.add(anotherLayer);
                    tree_items.add(anotherLayer);
                }
            } else {
                int i = 0;
                for (L1Threshold threshold : getThresholds()) {
                    threshold.addToTree(tree_items.get(i), counter);
                    ++i;
                }
            }
        }
    }

    ///String representation of the object.
    /**
     * So the tree view knows what information to display in the tree
     * 
     * @return A string containing the id, name and version of the record
     */
    @Override
    public String toString() {
        return "ITEM: " + get_name() + ", def: " + get_definition() + " (DBid=" + get_id() + "/V." + get_version() + ")";
    }

    ///Copy the L1Item.
    /**
     * Create a copy of the L1Item.  Note that this should be a deep copy,
     * so this is a bit careless.
     * @return
     */
    @Override
    public Object clone() {
        L1Item copy = new L1Item();
        copy.set_definition(get_definition());
        copy.set_priority(get_priority());
        copy.set_group(get_group());
        copy.set_name(get_name());
        copy.set_version(get_version());
        return copy;
    }

    @Override
    public int doDiff(AbstractTable t, DefaultMutableTreeNode treeNode, java.util.Set<String> linkstoignore) throws SQLException {
        int ndiff = super.doDiff(t, treeNode, linkstoignore);

        if (t instanceof L1Item) {
            L1Item item = (L1Item) t;
            /// compare thresholds
            ndiff += triggerdb.Diff.CompareTables.compareTables(this.getThresholds(), item.getThresholds(), treeNode);
        }

        return ndiff;
    }

    /**
     * Save the L1Item to that database.
     * Implementation to save the L1Item to the database.  This is tricky
     * because we also have to check the values of multiplicity and position
     * in the L1_TI_TO_TT link table.
     * 
     * @return The ID where the object was saved.
     * @throws java.sql.SQLException Halt on an SQL problem.
     */
    @Override
    public int save() throws SQLException {
        //logger.fine("In Save for L1 Item: " + get_name());

        //FIRST STEP - find list of candidated by matching the name
        int matchingID = -1;
        //NB we don't want to include the comment in this
        String comment = get_comment();
        keyValue.remove("COMMENT");

        ArrayList<Integer> candidate_ids = ConnectionManager.getInstance().get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");
        //
        //Now loop over the candidates
        //
        L1Item candidate;
        for (Integer canditem_id : candidate_ids) {

            //match the thresholds
            candidate = new L1Item(canditem_id);
            logger.log(Level.FINER, "potential match has this many thresholds: {0}", candidate.getThresholds().size());
            //if the size is different we can stop here
            if (candidate.getThresholds().size() != getThresholds().size()) {
                logger.finer("different number of thresholds so going to next candidate");
                continue;
            }
            logger.log(Level.FINER, "potential match {0} has same number of thresholds ({1})", new Object[]{canditem_id, getThresholds().size()});
            //if same number of inputs, are they the same ones? loop over inputs and see if the match has them
            //
            ArrayList<Integer> thresholds_found = new ArrayList<>();
            for (L1Threshold threshold : getThresholds()) {

                TreeMap<String, Object> fields_values = new TreeMap<>();
                fields_values.put("TRIGGER_ITEM_ID", canditem_id);
                fields_values.put("TRIGGER_THRESHOLD_ID", threshold.get_id());
                fields_values.put("MULTIPLICITY", threshold.get_multiplicity());
                fields_values.put("POSITION", threshold.get_position());

                ArrayList<Integer> ti2ttIDs = ConnectionManager.getInstance().get_IDs("L1_TI_TO_TT", "L1TI2TT_", fields_values, null, "ID");
                if (ti2ttIDs.size() > 0) {
                    thresholds_found.add(threshold.get_id());
                }
            }
            //now, thresholds_found will have same size as getThresholds if the match is the same!
            if (thresholds_found.size() != getThresholds().size()) {
                logger.finer("same number of thresholds but not identical so going to next candidate");
                continue;
            }
            thresholds_found.clear();

            logger.log(Level.FINER, "item candidate matches exactly: {0}", canditem_id);
            //  
            //if we got to here, we have a match so stop
            matchingID = canditem_id;
            break;
        }

        if (matchingID < 0) {
            logger.finer("Saving new item");

            //put back comment
            keyValue.put("COMMENT", comment);
            set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix, get_id(), keyValue));
            logger.finer("Saving new item's threshold");

            for (L1Threshold thr : getThresholds()) {
                TreeMap<String, Object> link = new TreeMap<>();
                link.put("TRIGGER_ITEM_ID", get_id());
                link.put("TRIGGER_THRESHOLD_ID", thr.get_id());
                link.put("POSITION", thr.get_position());
                link.put("MULTIPLICITY", thr.get_multiplicity());

                ConnectionManager.getInstance().save("L1_TI_TO_TT", "L1TI2TT_", -1, link);
            }

            logger.log(Level.FINE, "Saving L1Item at id = {0} ] {1}", new Object[]{get_id(), get_name()});
        } else {
            set_id(matchingID);
            logger.log(Level.FINER, "[SAVE Item :: returned id = {0} ] {1}", new Object[]{get_id(), get_name()});
        }

        return get_id();
    }

    ///Minimum details to show in the search results.
    @Override
    public ArrayList<String> get_min_names() {
        ArrayList<String> info = new ArrayList<>();
        info.add("ID");
        info.add("Name");
        info.add("Version");
        info.add("Comment");
        return info;
    }

    ///Minimum details to show in the search results.
    @Override
    public ArrayList<Object> get_min_info() {
        ArrayList<Object> info = new ArrayList<>();
        info.add(get_id());
        info.add(get_name());
        info.add(get_version());
        info.add(get_comment());
        return info;
    }

    @Override
    public String getTableName() {
        return "L1_TRIGGER_ITEM";
    }
    
}
