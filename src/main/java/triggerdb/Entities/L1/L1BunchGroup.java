package triggerdb.Entities.L1;

import triggerdb.Connections.ConnectionManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;

import javax.swing.tree.DefaultMutableTreeNode;
import triggerdb.Entities.AbstractTable;

/**
 * Bunch Group contains Bunches. Class that represents a row in the Bunch Group
 * table. Has child tables in the form of L1Bunch.
 *
 * @author Simon Head
 */
public final class L1BunchGroup extends AbstractTable {

    /**
     * Internal number belongs to the bgs to bg link, so has its own variable.
     */
    private Integer internalNumber = -1;
    /**
     * Vector of all bunches in this bunch group.
     */
    private ArrayList<L1Bunch> items = new ArrayList<>();
    /**
     * True if the children need adding to the tree.
     */
    private boolean needtoadd = true;

    /**
     * Normal constructor without loading
     *
     */
    public L1BunchGroup() {
        super(-1, "L1BG_");
        DefineKeyValuePairs();
    }

    /**
     * Normal constructor. Pass the ID (-1 = do not load).
     *
     * @param input_id The ID of the row to read.
     * @throws java.sql.SQLException
     */
    public L1BunchGroup(final int input_id) throws SQLException {
        super(input_id, "L1BG_");
        DefineKeyValuePairs();

        if (input_id > 0) {
            forceLoad();
            set_Bunches(getBunchesFromDb());
        }
    }

    private void DefineKeyValuePairs() {
        //keyValue.putFirst("PARTITION", new Integer(0), "Menu partition");
        keyValue.putFirst("SIZE", 1, "Size");
        keyValue.remove("NAME");
        keyValue.remove("VERSION");
    }

    @Override
    public void set_name(final String name) {
        // since we removed the L1BG_NAME from the table, we need to protect from calling this function
        logger.log(Level.WARNING, "Someone is trying to set the name of the bunchgroup to {0}", name);
    }

    @Override
    public void forceLoad() throws SQLException {
        items = null;
        needtoadd = true;
        ConnectionManager.getInstance().forceLoad(this);
    }

    /**
     *
     * @param i
     */
    public void set_partition(Integer i) {
        keyValue.put("PARTITION", i);
    }

    /**
     *
     * @return
     */
    public Integer get_partition() {
        return (Integer) keyValue.get("PARTITION");
    }

    // /Set the size.
    /**
     * Set the size of the bunch group.
     *
     * @param inp Size number.
     */
    public void set_size(Integer inp) {
        keyValue.put("SIZE", inp);
    }

    // /Return the size.
    /**
     * Get the size.
     *
     * @return size.
     */
    public Integer get_size() {
        return (Integer) keyValue.get("SIZE");
    }

    /**
     * Get the internal number of the bunch group (0 to 15). Bunch Groups are
     * numbered (0 to 15). This keeps track of which one this Bunch Group is.
     *
     * @return Bunch Group Internal number.
     */
    public Integer get_internal_number() {
        return internalNumber;
    }

    /**
     *
     * @param bunches
     */
    public void set_Bunches(ArrayList<L1Bunch> bunches) {
        items = bunches;
    }

    /**
     *
     * @return
     */
    public ArrayList<L1Bunch> get_Bunches() {
        return items;
    }

    /**
     * Load all the bunches for this bunch group in a speedy fashion, and only
     * if they've not been read already.
     *
     * @return Vector of Bunches in this Bunch Group.
     * @throws java.sql.SQLException
     */
    public ArrayList<L1Bunch> getBunchesFromDb() throws SQLException {
        ArrayList<L1Bunch> bunches = new ArrayList<>();

        if (this.get_id() > 0) {
            String query = (new L1Bunch()).getQueryString();
            String and = " WHERE " + "L1BG2B_BUNCH_GROUP_ID=? " + "ORDER BY "
                    + "L1BG2B_BUNCH_NUMBER ASC";
            ConnectionManager mgr = ConnectionManager.getInstance();
            Connection con = mgr.getConnection();

            query = mgr.fix_schema_name(query + and);
            try (PreparedStatement ps = con.prepareStatement(query)) {
                ps.setInt(1, this.get_id());
                try (ResultSet rset = ps.executeQuery()) {
                    while (rset.next()) {
                        L1Bunch b = new L1Bunch();
                        b.loadFromRset(rset);
                        bunches.add(b);
                    }
                    rset.close();
                }
                ps.close();
            }
        }
        return bunches;
    }

    /**
     * Save function first check if this bunch group already exists in the
     * database. Takes into account the children (bunches). If it exists then we
     * return the ID of the existing record. Otherwise we create the Bunch Group
     * and all the bunches.
     *
     * @return ID of the saved / found Bunch Group.
     * @throws java.sql.SQLException Stop on exception.
     */
    @Override
    public int save() throws SQLException {

        final long startTime = System.currentTimeMillis();

        logger.log(Level.FINE, "In L1 Bunch Group Save for: BG with {0} bunches, internal number {1} and partition {2}",
                new Object[]{get_Bunches().size(), get_internal_number(), get_partition()});

        keyValue.remove("COMMENT");    // don't compare comments

        ArrayList<Integer> bunchids_vec = new ArrayList<>();
        for (L1Bunch link : get_Bunches()) {
            bunchids_vec.add(link.get_bunch_number());
            logger.log(Level.FINE, "   Number: {0}", link.get_bunch_number());
        }

        Set<Integer> duptest = new HashSet<>(bunchids_vec);

        if (duptest.size() < bunchids_vec.size()) {
            logger.log(Level.WARNING, "Bunch list contains duplicate bunches - ignoring");
            return -1;
        }

        int numEntries = bunchids_vec.size();
        logger.log(Level.FINE, "   Number of bunches: {0}", numEntries);
        ArrayList<Integer> ids2 = new ArrayList<>();
        ConnectionManager mgr = ConnectionManager.getInstance();

        if (numEntries > 0) {
            ids2 = GetExistingBunchGroups(numEntries, mgr, bunchids_vec);
        }
        else{
            ids2 = GetEmptyBunchGroups(mgr);
        }

        logger.log(Level.FINE, "Number of matching items in db 3 = {0}", ids2.size());

        if (ids2.isEmpty()) {
            logger.log(Level.FINE, "Candidate group unique, saving as new bunch group");
            keyValue.put("SIZE", numEntries); //Test this works!
            int partition = 0; //get_partition();
            keyValue.remove("PARTITION"); //Test this works!
            set_id(mgr.save(getTableName(), tablePrefix, get_id(), keyValue));
            //endTime = System.currentTimeMillis();

            if (numEntries > 0) {
                SaveBunches();
            }
            keyValue.put("PARTITION", partition); //Test this works!
            //System.out.println("saved, Total execution time: " + (endTime - startTime));
            final long endTime = System.currentTimeMillis();

            logger.log(Level.FINE, "Bunch Group Save Complete - Total Time: {0} ms", (endTime - startTime));

        } else {
            logger.log(Level.FINE, "Candidate group duplicates existing group: {0} - Save aborted", ids2.get(0));
            set_id(ids2.get(0));
        }

        //System.out.println("End Total execution time: " + (endTime - startTime));
        return get_id();
    }

    private ArrayList<Integer> GetExistingBunchGroups(int numEntries, ConnectionManager mgr, ArrayList<Integer> bunchids_vec) throws SQLException {
        
        ArrayList<Integer> ids2 = new ArrayList<>();
         
        logger.log(Level.FINE, "Query for Candidate Bunch Group with {0} entries", numEntries);
        java.lang.String query = "Select * from (SELECT L1BG2B_BUNCH_GROUP_ID,count(*) FROM L1_BG_TO_B where L1BG2B_BUNCH_NUMBER IN (select tempvar from TEMP) "
                + " GROUP BY L1BG2B_BUNCH_GROUP_ID having count(*) = ?) match "
                + " join (SELECT L1BG_ID FROM L1_BUNCH_GROUP where L1BG_SIZE = ?) total on match.L1BG2B_BUNCH_GROUP_ID = total.L1BG_ID";
        
        mgr.getConnection().setAutoCommit(false);
        query = mgr.fix_schema_name(query);
        //System.out.println(query);

        try (PreparedStatement ps = mgr.getConnection().prepareStatement(query)) {
            
            java.lang.String insert = "insert into TEMP (TEMPVAR) values(?)";
            insert = mgr.fix_schema_name(insert);
            PreparedStatement temp = mgr.getConnection().prepareStatement(insert);
            Iterator<Integer> psIter = bunchids_vec.iterator();
            while (psIter.hasNext()) {
                temp.setInt(1, psIter.next());
                temp.addBatch();
                
            }
            temp.executeBatch();
            
            ps.setInt(1, numEntries);
            ps.setInt(2, numEntries);
            
            try (ResultSet rset = ps.executeQuery()) {
                //endTime = System.currentTimeMillis();
                //System.out.println("Query Compete Total execution time: " + (endTime - startTime));
                
                while (rset.next()) {
                    ids2.add(rset.getInt("L1BG2B_BUNCH_GROUP_ID"));
                }
                rset.close();
            }
            mgr.getConnection().commit();
            temp.close();
            ps.close();
            mgr.getConnection().setAutoCommit(true);
            
        }
        return ids2;
    }

    private void SaveBunches() throws SQLException {
        ArrayList<TreeMap<String, Object>> table = new ArrayList<>();
        //System.out.println("Ready to Save Total execution time: " + (endTime - startTime));
        for (L1Bunch link : get_Bunches()) {
            link.set_id(-1);
            link.set_bunch_group_id(get_id());
            table.add(link.getKey());
        }

        TreeMap<Integer, Integer> results = ConnectionManager.getInstance().saveBatch("L1_BG_TO_B", "L1BG2B_", 0, table);

        for (L1Bunch link : get_Bunches()) {
            link.set_id(results.get(link.get_bunch_number()));

        }
    }

    private ArrayList<Integer> GetEmptyBunchGroups(ConnectionManager mgr) throws SQLException {
        String query = "select * from L1_BUNCH_GROUP where L1BG_size = 0";
        ArrayList<Integer> ids2;
        //System.out.println(query);
        query = ConnectionManager.getInstance().fix_schema_name(query);
        try (PreparedStatement ps = mgr.getConnection().prepareStatement(query)) {
            try (ResultSet rset = ps.executeQuery()) {
                //endTime = System.currentTimeMillis();
                //System.out.println("Query Compete Total execution time: " + (endTime - startTime));
                ids2 = new ArrayList<>();
                while (rset.next()) {
                    ids2.add(rset.getInt("L1BG_ID"));
                    //System.out.println("match");
                }
                rset.close();
            }
            ps.close();
        }
        return ids2;

    }
    
    //public ArrayList<Integer> GetEmptyBunchGroupsPublic(ConnectionManager mgr) throws SQLException {
      //  return GetEmptyBunchGroups(mgr);
    //}

 
    // /Add all the children to the tree.
    @Override
    public void addToTree(DefaultMutableTreeNode treeNode, int counter) throws SQLException {
        --counter;
        if (counter >= 0) {
            if (needtoadd) {
                for (L1Bunch bunch : getBunchesFromDb()) {
                    treeNode.add(new DefaultMutableTreeNode(bunch));
                    needtoadd = false;
                }
            }
        }
    }

    // /String representation of the bunch group.
    /**
     * So the tree view knows what information to display in the tree.
     *
     * @return A string containing the id, name and version of the record.
     */
    @Override
    public String toString() {
        if (get_id() == -1) {
            return "L1 Bunch Group";
        }

        if (get_internal_number() > -1) {
            return "BG " + get_internal_number() + ": (DBid=" + get_id() + ", Size = " + get_size() + ")";
        } else {
            return "(DBid=" + get_id() + ", Size = " + get_size() + ")";
        }
    }

    // /Create a copy of the bunch group only.
    /**
     * Replace with copy constructor. Should this be a deep copy?
     *
     * @return Copy of object.
     */
    @Override
    public Object clone() {
        L1BunchGroup copy = new L1BunchGroup();

        //copy.set_name(get_name());
        //copy.set_version(get_version());
        return copy;
    }

    /**
     * Set the Internal number of the bunch group. This is the order number of
     * the group in a set.
     *
     * @param intNumber New value to set.
     */
    public void set_internal_number(int intNumber) {
        internalNumber = intNumber;
    }

    /**
     * Reads the list of all bunch groups from DB.
     *
     * @return the list of all bunch groups in DB.
     * @throws java.sql.SQLException
     */
    public static List<L1BunchGroup> getBunchGroups() throws SQLException {
        ConnectionManager mgr = ConnectionManager.getInstance();
        List<L1BunchGroup> bunchGroupList = new ArrayList<>();
        List<AbstractTable> groups = mgr.forceLoadVector(new L1BunchGroup(),
                "ORDER BY L1BG_ID DESC", new ArrayList());
        for (AbstractTable gr : groups) {
            bunchGroupList.add((L1BunchGroup) gr);
            gr.get_version();
        }
        return bunchGroupList;
    }

    @Override
    public String getTableName() {
        return "L1_BUNCH_GROUP";
    }
    
     /**
     * Minimum names for the search results.
     * @return 
     */
    @Override
    public ArrayList<String> get_min_names() {
        ArrayList<String> info = new ArrayList<>();
        info.add("ID");
        info.add("Size");
        return info;
    }
    
    /**
     * Minimum information for the search results.
     * @return 
     */
    @Override
    public ArrayList<Object> get_min_info() {
        ArrayList<Object> info = new ArrayList<>();
        info.add(get_id());
        info.add(get_size());
        return info;
    }
}
