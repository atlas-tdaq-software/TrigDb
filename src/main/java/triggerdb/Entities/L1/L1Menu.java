package triggerdb.Entities.L1;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import javax.swing.tree.DefaultMutableTreeNode;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.L1Links.L1Pits;
import triggerdb.Entities.L1Links.L1TM_PS;
import triggerdb.Entities.L1Links.L1TM_TT;
import triggerdb.Entities.L1Links.L1TM_TTM;
import triggerdb.Entities.alias.PrescaleSetAlias;

/**
 * Holds the contents of L1_TRIGGER_MENU.
 *
 * @author Simon Head
 */
public final class L1Menu extends AbstractTable {

    /**
     * Max number of L1 items.
     */
    public static int MAXITEMS = 512;
    /**
     * XML Labels
     */
    public static final String XML_MENU_TAG = "TriggerMenu";

    /**
     *
     */
    public static final String XML_NAME_ATTRIBUTE = "name";

    /**
     *
     */
    public static final String XML_ID_ATTRIBUTE = "id";

    /**
     *
     */
    public static final String XML_VERSION_ATTRIBUTE = "version";

    /**
     *
     */
    public static final String XML_PHASE_ATTRIBUTE = "phase";
    /**
     * ArrayList of Thresholds that don't belong to Items in the configuration.
     */
    private List<L1Threshold> forcedThresholds = null;
    /**
     * ArrayList of Monitoring information links.
     */
    private List<L1TM_TTM> monitors = null;
    /**
     * TM to TT cable information for each L1Threshold.
     */
    private List<L1TM_TT> cables = null;
    /**
     * Link to the CTP Files used by this L1Menu.
     */
    private L1CtpFiles ctpfiles = null;
    /**
     * Link to the CTP Switch Matrix files used by this L1Menu.
     */
    private L1CtpSmx ctpsmx = null;
    /**
     * Used by the tree to remember if we loaded the L1Items.
     */
    private List<DefaultMutableTreeNode> tree_items;
    /**
     * Used by thre tree to remember if we loaded the CTP Files.
     */
    private DefaultMutableTreeNode ctpfile_dmtn;
    /**
     * Used by the tree to remember if we loaded the SMX.
     */
    private DefaultMutableTreeNode ctpsmx_dmtn;
    /**
     * ArrayList of L1Item from the TM_TO_TI link table.
     */
    private ArrayList<L1Item> items;
    /**
     * ArrayList of L1Prescales from the TM_TO_PS link table.
     */
    private ArrayList<L1Prescale> prescalesets;
    private ArrayList<String> prescaleSetInfo;
    /**
     * List of Prescale Alias linked to this menu.
     */
    private final List<PrescaleSetAlias> aliases;

    /**
     *
     */
    public L1Menu() {
        super(-1, "L1TM_");
        this.prescalesets = new ArrayList<>();
        this.prescaleSetInfo = new ArrayList<>();
        this.aliases = new ArrayList<>();
        DefineKeyValuePairs();
    }

    /**
     * Standard constructor. If an ID of -1 is supplied we create a new, empty
     * record and don't attempt to load anything from the database. If the ID
     * supplied is greater than 0 we load this record from the database.
     *
     * @param input_id The ID to load or -1.
     * @throws java.sql.SQLException
     */
    public L1Menu(final int input_id) throws SQLException {
        super(input_id, "L1TM_");
        this.prescalesets = new ArrayList<>();
        this.prescaleSetInfo = new ArrayList<>();
        this.aliases = new ArrayList<>();
        DefineKeyValuePairs();

        if (input_id > 0) {
            forceLoad();
            setItems(getItemsFromDb());
        }
    }

    private void DefineKeyValuePairs() {
        keyValue.putFirst("PHASE", "", "Phase");
        keyValue.putFirst("CTP_SAFE", 0, "CTP Safe");
        keyValue.putFirst("CTP_FILES_ID", -1, "CTP Files ID");
        keyValue.putFirst("CTP_SMX_ID", -1, "CTP SMX ID");
    }

    /**
     * Force this record to be read again from the database.
     * @throws java.sql.SQLException
     */
    @Override
    public void forceLoad() throws SQLException {
        tree_items = new ArrayList<>();
        ctpfile_dmtn = null;
        ctpsmx_dmtn = null;
        cables = null;
        forcedThresholds = null;
        monitors = null;
        ctpfiles = null;
        ctpsmx = null;
        items = null;

        ConnectionManager.getInstance().forceLoad(this);
    }

    /**
     * Load all the threshold values for this menu in one SQL query.
     *
     * @throws java.sql.SQLException
     */
    public void loadThresholdValues() throws SQLException {
        String query = (new L1ThresholdValue()).getQueryString();
        query = query.replace("FROM ", ", L1TT2TTV_TRIGGER_THRESHOLD_ID, L1TT2TTV_ID FROM L1_TRIGGER_MENU, L1_TM_TO_TI, L1_TI_TO_TT, L1_TT_TO_TTV, ");
        String and = " WHERE "
                + "L1TM_ID=? "
                + "AND "
                + "L1TM2TI_TRIGGER_MENU_ID=L1TM_ID "
                + "AND "
                + "L1TM2TI_TRIGGER_ITEM_ID=L1TI2TT_TRIGGER_ITEM_ID "
                + "AND "
                + "L1TI2TT_TRIGGER_THRESHOLD_ID=L1TT2TTV_TRIGGER_THRESHOLD_ID "
                + "AND "
                + "L1TTV_ID=L1TT2TTV_TRIG_THRES_VALUE_ID";
        query = ConnectionManager.getInstance().fix_schema_name(query + and);

        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ps.setInt(1, get_id());
        ResultSet rset = ps.executeQuery();

        Set<Integer> tt2ttv_ids = new HashSet<>();

        while (rset.next()) {
            if (!tt2ttv_ids.contains(rset.getInt("L1TT2TTV_ID"))) {
                tt2ttv_ids.add(rset.getInt("L1TT2TTV_ID"));

                ArrayList<L1Threshold> addtothese = new ArrayList<>();
                for (L1Item item : getItems()) {
                    for (L1Threshold thr : item.getThresholds()) {
                        if (thr.get_id() == rset.getInt("L1TT2TTV_TRIGGER_THRESHOLD_ID")) {
                            addtothese.add(thr);
                        }
                    }
                }

                //want to add the same tv object in every place it appears - change
                //one, change them all in the overview panel. Not like for other things.
                L1ThresholdValue tv = new L1ThresholdValue();
                tv.loadFromRset(rset);
                for (L1Threshold thr : addtothese) {
                    thr.getThresholdValues().add(tv);
                }
            }
        }

        rset.close();
        ps.close();
    }

    /**
     * Set the CTP files object
     *
     * @param l1CtpFiles
     */
    public void set_ctp_files(final L1CtpFiles l1CtpFiles) {
        ctpfiles = l1CtpFiles;
    }

    public L1CtpFiles get_ctp_files(){
        return ctpfiles;
    }
    
    
    /**
     * Set the CTP Switch Matrix object.
     *
     * @param l1CtpSmx
     */
    public void set_ctp_smx(final L1CtpSmx l1CtpSmx) {
        ctpsmx = l1CtpSmx;
    }

    public L1CtpSmx get_ctp_smx(){
        return ctpsmx;
    }
    
    
    @Override
    public String toString() {
        if (get_id() == -1) {
            return "L1 Menu";
        }
        return "L1 MENU: ID=" + get_id() + ", Name=" + get_name() + ", Version=" + get_version();
    }

    /**
     * @return the phase.
     */
    public String get_phase() {
        return (String) keyValue.get("PHASE");
    }

    /**
     * @return true if this L1Menu is CTP safe
     */
    public Integer get_ctp_safe() {
        return (Integer) keyValue.get("CTP_SAFE");
    }

    /**
     * @return the CTP Files ID.
     */
    public Integer get_ctp_files_id() {
        return (Integer) keyValue.get("CTP_FILES_ID");
    }

    /**
     * @return the CTP SMX ID.
     */
    public Integer get_ctp_smx_id() {
        return (Integer) keyValue.get("CTP_SMX_ID");
    }

    /**
     * Set the phase.
     *
     * @param phase
     */
    public void set_phase(final String phase) {
        keyValue.put("PHASE", phase);
    }

    /**
     * Set CTP Safe true or false.
     *
     * @param cptSafe
     */
    public void set_ctp_safe(final Integer cptSafe) {
        keyValue.put("CTP_SAFE", cptSafe);
    }

    /**
     * Set the CTP SMX ID.
     *
     * @param cptSmxId
     */
    public void set_ctp_smx_id(final Integer cptSmxId) {
        keyValue.put("CTP_SMX_ID", cptSmxId);
    }

    /**
     * Set the CTP Files ID.
     *
     * @param ctpFilesId
     */
    public void set_ctp_files_id(final int ctpFilesId) {
        keyValue.put("CTP_FILES_ID", ctpFilesId);
    }

    /**
     * Load all the L1Thresholds for this menu in one SQL query.
     *
     * @throws java.sql.SQLException
     */
    public void loadThresholds() throws SQLException {
        List<L1Item> _items = this.getItems();
        Collections.sort(_items);

        String query = (new L1Threshold()).getQueryString();
        query = query.replace("FROM ", ", L1TM2TI_TRIGGER_ITEM_ID, L1TI2TT_POSITION, L1TI2TT_MULTIPLICITY "
                + "FROM L1_TM_TO_TI, L1_TI_TO_TT, ");

        String and = " WHERE L1TM2TI_TRIGGER_MENU_ID=? "
                + "AND "
                + "L1TM2TI_TRIGGER_ITEM_ID=L1TI2TT_TRIGGER_ITEM_ID "
                + "AND "
                + "L1TT_ID=L1TI2TT_TRIGGER_THRESHOLD_ID "
                + "ORDER BY L1TM2TI_TRIGGER_ITEM_ID ASC," // Order by item Id, 
                + "L1TI2TT_POSITION ASC"; // then by position in item.
        //logger.info(query + and);
        ConnectionManager mgr = ConnectionManager.getInstance();
        query = mgr.fix_schema_name(query + and);

        PreparedStatement ps = mgr.getConnection().prepareStatement(query);
        ps.setInt(1, get_id());
        ResultSet rset = ps.executeQuery();
        rset.setFetchSize(1000);

        HashMap<Integer, ArrayList<L1Threshold>> map = new HashMap<>();
        while (rset.next()) {
            int queryId = rset.getInt("L1TM2TI_TRIGGER_ITEM_ID");
            L1Threshold threshold = new L1Threshold();
            threshold.loadFromRset(rset);
            threshold.set_position(rset.getInt("L1TI2TT_POSITION"));
            threshold.set_multiplicity(rset.getInt("L1TI2TT_MULTIPLICITY"));

            if (map.containsKey(queryId)) {
                map.get(queryId).add(threshold);
            } else {
                ArrayList<L1Threshold> list = new ArrayList<>();
                list.add(threshold);
                map.put(queryId, list);
            }
        }

        for (Map.Entry<Integer, ArrayList<L1Threshold>> entry : map.entrySet()) {
            for (L1Item item : _items) {
                if (item.get_id() == entry.getKey()) {
                    item.setThresholds(entry.getValue());
                }
            }
        }

        rset.close();
        ps.close();
    }

    /**
     * @return the list of items linked to htis menu.
     * @throws java.sql.SQLException
     */
    public List<Integer> getItemIds() throws SQLException {
        ArrayList<Integer> ids = new ArrayList<>();

        if (get_id() > 0) {
            String query = "SELECT L1TI_ID "
                    + "FROM L1_TRIGGER_MENU, L1_TM_TO_TI, L1_TRIGGER_ITEM "
                    + "WHERE L1TM_ID=? AND L1TM2TI_TRIGGER_MENU_ID=L1TM_ID AND L1TM2TI_TRIGGER_ITEM_ID=L1TI_ID "
                    + "ORDER BY L1TI_CTP_ID ASC";
            query = ConnectionManager.getInstance().fix_schema_name(query);

            PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
            ps.setInt(1, get_id());
            ResultSet rset = ps.executeQuery();

            while (rset.next()) {
                ids.add(rset.getInt("L1TI_ID"));
            }

            rset.close();
            ps.close();
        }

        return ids;
    }

    /**
     *
     * @return
     */
    public List<L1Item> getItems() {
        if (items == null) {
            try {
                items = getItemsFromDb();
            } catch (SQLException e) {
                logger.severe("SQLException while loading the L1Items in L1Menu.");
            }
        }
        return items;
    }

    /**
     *
     * @param list
     */
    public void setItems(ArrayList<L1Item> list) {
        items = list;
    }

    /**
     * Load all the L1Items for this L1Menu in one query.
     *
     * @return All the items belonging to this menu.
     * @throws java.sql.SQLException
     */
    private ArrayList<L1Item> getItemsFromDb() throws SQLException {
        // If no items present, query them from DB.
        if (this.items == null) {
            // It speeds up things to preallocate mem beforehand.
            this.items = new ArrayList<>(L1Menu.MAXITEMS);
            if (this.get_id() > 0) {
                String query = "SELECT L1TI_COMMENT, L1TI_CTP_ID, L1TI_DEFINITION, L1TI_GROUP, "
                        + "L1TI_ID, L1TI_NAME, L1TI_PRIORITY, "
                        + "L1TI_TRIGGER_TYPE, L1TI_PARTITION, L1TI_VERSION "
                        + "FROM L1_TM_TO_TI, "
                        + "L1_TRIGGER_ITEM "
                        + "WHERE L1TM2TI_TRIGGER_MENU_ID=? "
                        + "AND L1TM2TI_TRIGGER_ITEM_ID=L1TI_ID "
                        + "ORDER BY L1TI_CTP_ID ASC";
                ConnectionManager mgr = ConnectionManager.getInstance();
                query = mgr.fix_schema_name(query);

                Connection con = mgr.getConnection();
                PreparedStatement ps = con.prepareStatement(query);
                ps.setInt(1, get_id());
                ResultSet rset = ps.executeQuery();
                // we know more or less how many items we have. this speeds up 
                // ResultSet fetching
                rset.setFetchSize(L1Menu.MAXITEMS); 
                while (rset.next()) {
                    // This is VERY slow! Creation of items takes 90% of the
                    // total time. 
                    L1Item item = new L1Item(rset.getInt("L1TI_ID"));
                    this.items.add(item);
                }
                rset.close();
                ps.close();
            }
        }
        return this.items;
    }

    /**
     * Load all the forced thresholds for this L1Menu in one query.
     *
     * @return the list of forced thresholds linked to this menu.
     * @throws java.sql.SQLException
     */
    public List<L1Threshold> getForcedThresholds() throws SQLException {
        if (forcedThresholds == null) {
            forcedThresholds = new ArrayList<>();

            if (get_id() > 0) {
                String query = (new L1Threshold()).getQueryString();
                query = query.replace("FROM ", "FROM L1_TRIGGER_MENU, L1_TM_TO_TT_FORCED, ");

                String and = " WHERE "
                        + "L1TM_ID=? "
                        + "AND "
                        + "L1TM2TTF_TRIGGER_MENU_ID=L1TM_ID "
                        + "AND "
                        + "L1TM2TTF_TRIGGER_THRESHOLD_ID=L1TT_ID";

                query = ConnectionManager.getInstance().fix_schema_name(query + and);

                PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
                ps.setInt(1, get_id());
                ResultSet rset = ps.executeQuery();

                while (rset.next()) {
                    L1Threshold threshold = new L1Threshold(rset.getInt("L1TT_ID"));
                    threshold.loadFromRset(rset);
                    forcedThresholds.add(threshold);
                }

                rset.close();
                ps.close();
            }
        }

        return forcedThresholds;
    }

    /**
     * Load all the monitor information for this L1Menu.
     *
     * @return the list of monitors.
     * @throws java.sql.SQLException
     */
    public List<L1TM_TTM> getMonitors() throws SQLException {
        if (monitors == null) {
            monitors = new ArrayList<>();

            if (get_id() > 0) {
                String query = (new L1TM_TTM(-1)).getQueryString();
                String and = " WHERE L1TM2TTM_TRIGGER_MENU_ID=? ORDER BY L1TM2TTM_ID ASC";
                query = ConnectionManager.getInstance().fix_schema_name(query + and);

                PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
                ps.setInt(1, get_id());
                ResultSet rset = ps.executeQuery();

                while (rset.next()) {
                    L1TM_TTM monitor = new L1TM_TTM(-1);
                    monitor.loadFromRset(rset);
                    monitors.add(monitor);
                }

                rset.close();
                ps.close();
            }
        }
        return monitors;
    }

    /**
     * Get the list of PrescaleAlias linked to this menu.
     *
     * @return the list of <code>PrescaleAlias</code> linked to this menu.
     * @throws java.sql.SQLException
     */
    public List<PrescaleSetAlias> getAliases() throws SQLException {
        if (this.aliases.isEmpty()) {
            this.loadAliases();
        }
        return this.aliases;
    }

    /**
     * Load all the valid prescale sets for this menu.
     *
     * @return the list of prescale sets linked to this menu.
     * @throws java.sql.SQLException
     */
    public List<L1Prescale> getPrescaleSets() throws SQLException {
        if (this.prescalesets == null) {
            this.prescalesets = new ArrayList<>();
        }
        if (this.prescalesets.isEmpty()) {
            this.prescalesets.addAll(this.getPrescalesSets(true));
        }
        return this.prescalesets;
    }

    /**
     * Queries the DB for the list of non hidden prescale sets linked to this
     * menu.
     *
     * @return the list of non-hidden prescale sets.
     * @throws java.sql.SQLException
     */
    public List<L1Prescale> getPrescalesSetsNonHidden() throws SQLException {
        return this.getPrescalesSets(false);
    }

    /**
     * Get the cable information from the L1Menu to L1Threshold link table.
     *
     * @return
     * @throws java.sql.SQLException
     */
    public List<L1TM_TT> getTMTT() throws SQLException {
        if (cables == null) {
            cables = new ArrayList<>();

            if (get_id() > 0) {
                String query = (new L1TM_TT()).getQueryString();
                String and = " WHERE L1TM2TT_TRIGGER_MENU_ID=? ORDER BY L1TM2TT_TRIGGER_THRESHOLD_ID ASC, L1TM2TT_CABLE_NAME ASC, L1TM2TT_CABLE_START ASC";

                query = ConnectionManager.getInstance().fix_schema_name(query + and);
                try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query)) {
                    ps.setInt(1, get_id());
                    try (ResultSet rset = ps.executeQuery()) {
                        while (rset.next()) {
                            L1TM_TT cable = new L1TM_TT();
                            cable.loadFromRset(rset);
                            cables.add(cable);
                        }
                        rset.close();
                        ps.close();
                    }
                }
            }
        }
        return cables;
    }

    /**
     * Load the CTP files record from the DB, if it hasn't been loaded already.
     *
     * @return
     * @throws java.sql.SQLException
     */
    public L1CtpFiles get_files() throws SQLException {
        if (ctpfiles == null && new L1CtpFiles().isValid(get_ctp_files_id())) {
            ctpfiles = new L1CtpFiles(get_ctp_files_id());
        }
        return ctpfiles;
    }

    /**
     * Load the Switch matrix record from the DB, if it hasn't been loaded
     * already.
     *
     * @return
     * @throws java.sql.SQLException
     */
    public L1CtpSmx get_smx() throws SQLException {
        if (ctpsmx == null && new L1CtpSmx().isValid(get_ctp_smx_id())) {
            ctpsmx = new L1CtpSmx(get_ctp_smx_id());
        }

        return ctpsmx;
    }

    /**
     * Add all the children to the tree.
     *
     * @param treeNode the TreeNode to add this items to.
     * @param counter : Warning this is changed in this method.
     * @throws java.sql.SQLException
     */
    @Override
    public void addToTree(final DefaultMutableTreeNode treeNode, int counter) throws SQLException {
        --counter;

        if (counter >= 0) {
            if (tree_items.isEmpty()) {
                for (L1Item item : getItems()) {
                    DefaultMutableTreeNode anotherLayer = new DefaultMutableTreeNode(item);
                    treeNode.add(anotherLayer);
                    tree_items.add(anotherLayer);
                }

            } else {
                int i = 0;

                Long start = System.nanoTime();

                for (L1Item item : getItems()) {
                    item.addToTree(tree_items.get(i), counter);
                    ++i;
                }

                Long time = System.nanoTime() - start;
                logger.log(Level.FINE, "Time: {0} seconds", (1. * time / (1000 * 1000 * 1000)));

            }

            if (get_smx() != null && ctpsmx_dmtn == null) {
                ctpsmx_dmtn = new DefaultMutableTreeNode(get_smx());
                treeNode.add(ctpsmx_dmtn);
            }

            if (get_files() != null && ctpfile_dmtn == null) {
                ctpfile_dmtn = new DefaultMutableTreeNode(get_files());
                treeNode.add(ctpfile_dmtn);
            }
        }
    }

    /**
     * Create a copy of this menu. Should be a deep copy and isn't.
     *
     * @return
     */
    @Override
    public Object clone() {
        L1Menu copy = new L1Menu();
        copy.set_name(get_name());
        copy.set_version(get_version());
        copy.set_ctp_files_id(get_ctp_files_id());
        copy.set_ctp_smx_id(get_ctp_smx_id());
        copy.set_phase(get_phase());
        copy.set_ctp_safe(get_ctp_safe());
        return copy;
    }

    @Override
    public int doDiff(final AbstractTable t,
            final DefaultMutableTreeNode treeNode,
            final java.util.Set<String> linkstoignore) throws SQLException {
        Set<String> ignore;
        if (linkstoignore == null) {
            ignore = new java.util.HashSet<>();
        } else {
            ignore = linkstoignore;
        }
        ignore.add("CTP_FILES_ID");
        ignore.add("CTP_SMX_ID");
        int ndiff = super.doDiff(t, treeNode, ignore);

        if (t instanceof L1Menu) {
            L1Menu menu = (L1Menu) t;
            /// Check L1 items
            ndiff += triggerdb.Diff.CompareTables.compareTables(this.getItems(), menu.getItems(), treeNode);
            /// check L1 TMTT
            ndiff += triggerdb.Diff.CompareTables.compareTables(this.getTMTT(), menu.getTMTT(), treeNode);
            /// check the monitoring
            ndiff += triggerdb.Diff.CompareTables.compareTables(this.getMonitors(), menu.getMonitors(), treeNode);
            /// check forced thresholds
            ndiff += triggerdb.Diff.CompareTables.compareTables(this.getForcedThresholds(), menu.getForcedThresholds(), treeNode);
            if (!Objects.equals(this.get_ctp_files_id(), menu.get_ctp_files_id())) { //Different L1 ctp files
                DefaultMutableTreeNode ctp_files = new DefaultMutableTreeNode("L1 CTP Files");
                treeNode.add(ctp_files);
                ndiff += this.get_files().doDiff(menu.get_files(), ctp_files, null);
            }
            if (!Objects.equals(this.get_ctp_smx_id(), menu.get_ctp_smx_id())) { //Different L1 ctp smx
                DefaultMutableTreeNode smx = new DefaultMutableTreeNode("L1 CTP SMX");
                treeNode.add(smx);
                ndiff += this.get_smx().doDiff(menu.get_smx(), smx, null);
            }
        }

        return ndiff;

    }

    /**
     * Save is fairly complicated because of all the different link tables. Do
     * not check if the prescales are the same or not, but do remember to add
     * them!
     *
     * @return The DB id of the saved L1 Menu.
     * @throws java.sql.SQLException
     */
    @Override
    public int save() throws SQLException {

        /// Don't count name difference
        String name = get_name();
        keyValue.remove("NAME");

        //logger.fine("L1Menu: " + get_name() + " has " + getItems().size() + " items");
        //items
        ArrayList<Integer> ti_vec = new ArrayList<>(this.getItems().size());
        for (L1Item item : this.getItems()) {
            ti_vec.add(item.get_id());
        }
        Collections.sort(ti_vec);

        //forced thresholds
        ArrayList<Integer> ttf_vec = new ArrayList<>(this.getForcedThresholds().size());
        for (L1Threshold threshold : this.getForcedThresholds()) {
            ttf_vec.add(threshold.get_id());
        }
        Collections.sort(ttf_vec);
        //candidates
        List<Integer> menu_ids = ConnectionManager.getInstance().get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");
        //note that 2 menus may be identical but if one is linked to smx and ctp it won't
        //be taken as a candidate

        // put the name back
        keyValue.put("NAME", name);

        //candidates with same items
        ArrayList<Integer> hcpPar_ids = new ArrayList<>(menu_ids.size());
        for (Integer menu_id : menu_ids) {
            List<Integer> component_parIDs = ConnectionManager.getInstance().get_ItemList("L1_TM_TO_TI", "L1TM2TI", "L1TM2TI_TRIGGER_MENU", "L1TM2TI_TRIGGER_ITEM", menu_id);
            Collections.sort(component_parIDs);
            if (ti_vec.equals(component_parIDs)) {
                hcpPar_ids.add(menu_id);
            }
        }
        Collections.sort(hcpPar_ids);

        //candidates with same  forced thresholds
        ArrayList<Integer> after_forced_thresholds = new ArrayList<>(hcpPar_ids.size());
        for (Integer menu_id : hcpPar_ids) {
            List<Integer> component_parIDs = ConnectionManager.getInstance().get_ItemList("L1_TM_TO_TT_FORCED", "L1TM2TTF", "L1TM2TTF_TRIGGER_MENU", "L1TM2TTF_TRIGGER_THRESHOLD", menu_id);
            Collections.sort(component_parIDs);
            if (ttf_vec.equals(component_parIDs)) {
                after_forced_thresholds.add(menu_id);
            }
        }

        //check the cable assignments
        ArrayList<Integer> after_cables = new ArrayList<>(after_forced_thresholds.size());
        for (Integer menu_id : after_forced_thresholds) {

            //load all the links
            ArrayList<L1TM_TT> t2 = new ArrayList<>();

            List<L1TM_TT> linkedcables = getTMTT();
            Collection<L1TM_TT> linkedcables_hs = new HashSet<>(linkedcables);

            String query = (new L1TM_TT()).getQueryString();
            String and = " WHERE L1TM2TT_TRIGGER_MENU_ID=? ORDER BY L1TM2TT_TRIGGER_THRESHOLD_ID ASC, L1TM2TT_CABLE_NAME ASC, L1TM2TT_CABLE_START ASC";

            query = ConnectionManager.getInstance().fix_schema_name(query + and);

            PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
            ps.setInt(1, menu_id);
            ResultSet rset = ps.executeQuery();

            while (rset.next()) {
                L1TM_TT cable = new L1TM_TT();
                cable.loadFromRset(rset);
                t2.add(cable);
            }

            rset.close();
            ps.close();

            Collection<L1TM_TT> t2_hs = new HashSet<>(t2);
            if (linkedcables_hs.equals(t2_hs)) {
                logger.fine("Cables match");
                after_cables.add(menu_id);
            } else {
                logger.log(Level.FINE, "Cables don''t match: cand size {0}  our size: {1}", new Object[]{t2.size(), getTMTT().size()});
            }
        }

        //check the monitors
        ArrayList<Integer> after_monitors = new ArrayList<>(after_cables.size());
        for (Integer menu_id : after_cables) {

            ArrayList<L1TM_TTM> t2 = new ArrayList<>();
            String query = (new L1TM_TTM(-1)).getQueryString();
            query = query.replace("L1TM2TTM_BUNCH_GROUP_SET_ID", "L1TM2TTM_BUNCH_GROUP_ID");
            String and = " WHERE L1TM2TTM_TRIGGER_MENU_ID=? ORDER BY L1TM2TTM_ID ASC";
            ConnectionManager mgr = ConnectionManager.getInstance();
            query = mgr.fix_schema_name(query + and);

            PreparedStatement ps = mgr.getConnection().prepareStatement(query);
            ps.setInt(1, menu_id);
            ResultSet rset = ps.executeQuery();

            while (rset.next()) {
                L1TM_TTM monitor = new L1TM_TTM(-1);
                monitor.loadFromRset(rset);
                t2.add(monitor);
            }

            rset.close();
            ps.close();

            logger.log(Level.FINER, "menu {0} has {1} monitors", new Object[]{menu_id, t2.size()});
            List<L1TM_TTM> _monitors = this.getMonitors();
            if (t2.equals(_monitors)) {
                logger.finest("Monitors match");
                after_monitors.add(menu_id);
            } else {
                logger.finest("Monitors don't match");
            }
        }

        //save a new one if no match found
        if (after_monitors.isEmpty()) {

            ConnectionManager mgr = ConnectionManager.getInstance();
            this.set_id(-1);
            set_id(mgr.save(getTableName(), tablePrefix, this.get_id(), keyValue));
            //save items
            for (L1Item item : getItems()) {
                TreeMap<String, Object> link = new TreeMap<>();
                link.put("TRIGGER_MENU_ID", get_id());
                link.put("TRIGGER_ITEM_ID", item.get_id());
                ConnectionManager.getInstance().save("L1_TM_TO_TI", "L1TM2TI_", -1, link);
            }

            //forced threshold links
            for (L1Threshold threshold : getForcedThresholds()) {
                TreeMap<String, Object> link = new TreeMap<>();
                link.put("TRIGGER_MENU_ID", get_id());
                link.put("TRIGGER_THRESHOLD_ID", threshold.get_id());
                ConnectionManager.getInstance().save("L1_TM_TO_TT_FORCED", "L1TM2TTF_", -1, link);
            }

            //cable assignments
            for (L1TM_TT link : getTMTT()) {

                logger.log(Level.FINEST, "Pits init: {0}", link.get_pits().size());
                logger.finest("Saving tm to tt link");

                link.set_id(-1);
                link.set_menu_id(get_id());

                boolean found = false;

                //look through item thresholds
                for (L1Item itm : getItems()) {
                    for (L1Threshold thr : itm.getThresholds()) {
                        if (link.get_threshold().get_name().equals(thr.get_name())) {
                            link.set_threshold_id(thr.get_id());
                            found = true;
                            break;
                        }
                    }
                }
                //try looking through forced thresholds if we need to
                if (!found) {
                    for (L1Threshold thr : getForcedThresholds()) {
                        if (link.get_threshold().get_name().equals(thr.get_name())) {
                            link.set_threshold_id(thr.get_id());
                            found = true;
                            break;
                        }
                    }
                }

                //if still not found
                if (!found) {
                    logger.log(Level.WARNING, "This menu has cables for threshold {0} that doesn''t exist - cable removed!", link.get_threshold().get_name());
                } else {
                    logger.log(Level.FINER, "Threshold found for cable {0} so saving and copying pits", link.get_cable_name());
                    int id = link.save();
                    //also copy pits!
                    logger.log(Level.FINER, "Pits to save: {0}", link.get_pits().size());
                    for (L1Pits pit : link.get_pits()) {
                        pit.set_tm_to_tt_id(id);
                        pit.set_id(-1);
                        pit.save();
                    }
                }
            }

            //monitors
            for (L1TM_TTM link : this.getMonitors()) {

                link.set_id(-1);
                link.set_menu_id(get_id());

                boolean found = false;

                //look through item thresholds
                for (L1Item itm : getItems()) {
                    for (L1Threshold thr : itm.getThresholds()) {
                        if (link.get_threshold().get_name().equals(thr.get_name())) {
                            link.set_threshold_id(thr.get_id());
                            found = true;
                            break;
                        }
                    }
                }
                //try looking through forced thresholds if we need to
                if (!found) {
                    for (L1Threshold thr : getForcedThresholds()) {
                        if (link.get_threshold().get_name().equals(thr.get_name())) {
                            link.set_threshold_id(thr.get_id());
                            found = true;
                            break;
                        }
                    }
                }

                //if still not found
                if (!found) {
                    logger.log(Level.WARNING, "This menu has monitors for threshold {0} that doesn''t exist - cable removed!", link.get_threshold().get_name());
                } else {
                    logger.log(Level.FINE, "Threshold found for monitor {0} so saving", link.get_name());
                    link.save();
                }
                //note that if there is a new threshold with no monitor, nothing is created - shouldn't add thresholds!
            }

        } else {
            this.set_id(after_monitors.get(0));
        }

        return this.get_id();
    }

    /**
     * CTP Files and SMX can be changed without changing the ID of this record!
     *
     * @throws java.sql.SQLException
     */
    public void replace_ctp_links() throws SQLException {
        String query = "UPDATE L1_TRIGGER_MENU SET L1TM_CTP_SMX_ID=?, L1TM_CTP_FILES_ID=? WHERE L1TM_ID=?";
        query = ConnectionManager.getInstance().fix_schema_name(query);
        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ps.setInt(1, get_ctp_smx_id());
        ps.setInt(2, get_ctp_files_id());
        ps.setInt(3, get_id());
        ps.executeUpdate();
        ps.close();
    }

    @Override
    public String getTableName() {
        return "L1_TRIGGER_MENU";
    }

    /**
     * Queries the DB for the list of prescale sets linked to this menu.
     *
     * @param showAll <code>true</code> to get all prescale sets (hidden and non
     * hidden), <code>false</code> otherwise.
     * @return the list of non-hidden prescale sets.
     * @throws java.sql.SQLException
     */
    public List<L1Prescale> getPrescalesSets(final boolean showAll) throws SQLException {
        List<L1Prescale> sets = new ArrayList<>();
        if (this.get_id() > 0) {
            String query = (new L1Prescale(-1)).getQueryString();
            query = query.replace("FROM ", "FROM L1_TRIGGER_MENU, L1_TM_TO_PS, ");
            String and = " WHERE L1TM_ID=? "
                    + "AND L1TM2PS_TRIGGER_MENU_ID=L1TM_ID "
                    + "AND L1TM2PS_PRESCALE_SET_ID=L1PS_ID";
            if (!showAll) {
                and += " AND L1TM2PS_USED=0 ";
            }
            and += " ORDER BY L1PS_ID DESC";
            ConnectionManager mgr = ConnectionManager.getInstance();
            query = mgr.fix_schema_name(query + and);
            PreparedStatement ps = mgr.getConnection().prepareStatement(query);
            ps.setInt(1, this.get_id());
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                L1Prescale psset = new L1Prescale();
                psset.loadFromRset(rset);
                sets.add(psset);
            }
            rset.close();
            ps.close();
        }
        return sets;
    }

    /**
     *
     * @param showAll
     * @return
     * @throws SQLException
     */
    public List<String> getPrescalesSetsInfo(final boolean showAll) throws SQLException {
        List<String> sets = new ArrayList<>();
        if (this.get_id() > 0) {
            String query = (new L1Prescale(-1)).getQueryString();
            query = query.replace("FROM ", "FROM L1_TRIGGER_MENU, L1_TM_TO_PS, ");
            String and = " WHERE L1TM_ID=? "
                    + "AND L1TM2PS_TRIGGER_MENU_ID=L1TM_ID "
                    + "AND L1TM2PS_PRESCALE_SET_ID=L1PS_ID";
            if (!showAll) {
                and += " AND L1TM2PS_USED=0 ";
            }
            and += " ORDER BY L1PS_ID DESC";
            ConnectionManager mgr = ConnectionManager.getInstance();
            query = mgr.fix_schema_name(query + and);
            PreparedStatement ps = mgr.getConnection().prepareStatement(query);
            ps.setInt(1, this.get_id());
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                String info = rset.getInt("L1PS_ID") + ": " + rset.getString("L1PS_NAME") + " v" + rset.getString("L1PS_VERSION");
                sets.add(info);
            }
            rset.close();
            ps.close();
        }
        return sets;
    }

    /**
     *
     * @param showAll
     * @param partitions
     * @return
     * @throws SQLException
     */
    public List<String> getPrescaleSetInfoPartition(final boolean showAll, final ArrayList<Integer> partitions) throws SQLException {
        List<String> sets = new ArrayList<>();
        if (this.get_id() > 0) {
            String query = (new L1Prescale(-1)).getQueryString();
            query = query.replace("FROM ", "FROM L1_TRIGGER_MENU, L1_TM_TO_PS, ");
            String and = " WHERE L1TM_ID=? "
                    + "AND L1TM2PS_TRIGGER_MENU_ID=L1TM_ID "
                    + "AND L1TM2PS_PRESCALE_SET_ID=L1PS_ID ";
            if (partitions.contains(0)) {
                and += "AND (L1PS_PARTITION is NULL OR L1PS_PARTITION in (";
            } else {
                and += "AND L1PS_PARTITION in (";

            }
            for (int var = 0; var < partitions.size(); var++) {
                and += "?";
                if (var < partitions.size() - 1) {
                    and += ",";
                }
            }
            if (partitions.contains(0)) {
                and += "))";
            } else {
                and += ")";
            }
            if (!showAll) {
                and += " AND L1TM2PS_USED=0 ";
            }
            and += " ORDER BY L1PS_ID DESC";
            ConnectionManager mgr = ConnectionManager.getInstance();
            query = mgr.fix_schema_name(query + and);
            //System.out.println("query " + query);
            PreparedStatement ps = mgr.getConnection().prepareStatement(query);
            ps.setInt(1, this.get_id());
            int counter = 2;
            for (Integer partition : partitions) {
                ps.setInt(counter, partition);
                counter++;
            }
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                String info = rset.getInt("L1PS_ID") + ": " + rset.getString("L1PS_NAME") + " v" + rset.getString("L1PS_VERSION");
                sets.add(info);
            }
            rset.close();
            ps.close();
        }
        return sets;
    }

    /**
     * Link a new L1 Prescale Set to this L1 Menu.
     *
     * @param l1ps the new L1 Prescale Set to link.
     * @param forceVissible if prescale set already exist, ensure that it is
     * vissible.
     * @return <code>true</code> if prescale set alredy exist.
     * @throws java.sql.SQLException
     */
    public boolean addPrescaleSet(final L1Prescale l1ps, boolean forceVissible) throws SQLException {
        int menuId = this.get_id();
        int psId = l1ps.get_id();

        boolean exists = this.checkPrescaleExist(l1ps);
        if (!exists) {
            L1TM_PS newlink = new L1TM_PS();
            newlink.set_menu_id(menuId);
            newlink.set_prescale_set_id(psId);
            newlink.save();

            this.prescalesets.clear();
            logger.log(Level.FINE, " Prescale \"{0}\" added to menu \"{1} \" ", new Object[]{l1ps, this});
        } else {
            L1TM_PS.unhidePS(menuId, psId);
            logger.log(Level.FINER, " Prescale {0} already linked to L1 Menu {1}", new Object[]{l1ps, this});
        }
        //logger.info("new id " + newId);
        return exists;
    }

    private boolean checkPrescaleExist(final L1Prescale l1ps) throws SQLException {
        boolean exists = false;
        int psId = l1ps.get_id();
        for (L1Prescale _ps : this.getPrescaleSets()) {
            if (_ps.get_id() == psId) {
                exists = true;
                break;
            }
        }
        return exists;
    }

    /**
     * Query the DB for the aliases linked to this menu and adds them to
     * <code>this.aliases</code>.
     */
    private void loadAliases() throws SQLException {
        this.aliases.clear();

        String query = "SELECT DISTINCT(L1PSA_ALIAS_ID) "
                + "FROM L1_TM_TO_PS ,L1_PRESCALE_SET_ALIAS "
                + "WHERE L1TM2PS_TRIGGER_MENU_ID=? "
                + "AND L1TM2PS_ID=l1psa_l1tm2ps_id ";
        ConnectionManager mgr = ConnectionManager.getInstance();
        query = mgr.fix_schema_name(query);
        ArrayList<Integer> aliaseIds = new ArrayList<>();
        PreparedStatement ps = mgr.getConnection().prepareStatement(query);
        // Set the menu id
        ps.setInt(1, this.get_id());

        ResultSet rset = ps.executeQuery();
        while (rset.next()) {
            int aliasId = rset.getInt("L1PSA_ALIAS_ID");
            aliaseIds.add(aliasId);
        }
        rset.close();
        ps.close();

        for (Integer aliasId : aliaseIds) {
            this.aliases.add(new PrescaleSetAlias(aliasId));
        }
    }
}
