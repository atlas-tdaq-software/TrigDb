package triggerdb.Entities.L1;

import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.tree.DefaultMutableTreeNode;

import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;

/**
 * Record to hold a row for L1 Calo Info. This is linked from the L1 Master
 * Table.
 *
 * @author Simon Head
 */
public final class L1CaloInfo extends AbstractTable {

    private static final String XS_SIGMA_SCALE = "XS_SIGMA_SCALE";
    private static final String XS_SIGMA_OFFSET = "XS_SIGMA_OFFSET";
    private static final String XS_XE_MIN = "XS_XE_MIN";
    private static final String XS_XE_MAX = "XS_XE_MAX";
    private static final String XS_TESQRT_MIN = "XS_TESQRT_MIN";
    private static final String XS_TESQRT_MAX = "XS_TESQRT_MAX";

    private ArrayList<DefaultMutableTreeNode> sinCosLayer = new ArrayList<>();
    private ArrayList<DefaultMutableTreeNode> isoLayer = new ArrayList<>();
    private ArrayList<DefaultMutableTreeNode> tobLayer = new ArrayList<>();

    //Mark TODO: remove db schema req
    /// db schema version from which we have MET significance L1 triggers
    //public static final int DBVER_WITH_XS = 10;
    /**
     * Normal constructor. Pass an ID, or set -1 to create a new record.
     *
     */
    public L1CaloInfo() {
        super(-1, "L1CI_");

        keyValue.putFirst("GLOBAL_EM_SCALE", 1.0, "Global EM Scale");
        keyValue.putFirst("GLOBAL_JET_SCALE", 1.0, "Global JET Scale");
        ConnectionManager mgr = ConnectionManager.getInstance();
        final int dbver = mgr.loadSchemaVersion(mgr.getConnection());
        keyValue.putFirst(XS_SIGMA_SCALE, 0, "Significance Scale");
        keyValue.putFirst(XS_SIGMA_OFFSET, 0, "Significance Offset ");
        keyValue.putFirst(XS_XE_MIN, 0, "significance XE cut min");
        keyValue.putFirst(XS_XE_MAX, 0, "significance XE cut max");
        keyValue.putFirst(XS_TESQRT_MIN, 0, "Mininum Sqrt of TE");
        keyValue.putFirst(XS_TESQRT_MAX, 0, "Max Sqrt of TE");
        keyValue.putFirst("MIN_TOB_EM", -1, "min_tob_em");
        keyValue.putFirst("MIN_TOB_TAU", -1, "min_tob_tau");
        keyValue.putFirst("MIN_TOB_JETS", -1, "min_tob_jets");
        keyValue.putFirst("MIN_TOB_JETL", -1, "min_tob_jetl");
        keyValue.putFirst("ISO_HA_EM", -1, "iso_ha_em");
        keyValue.putFirst("ISO_EM_EM", -1, "iso_em_em");
        keyValue.putFirst("ISO_EM_TAU", -1, "iso_em_tau");

    }

    /**
     * Normal constructor. Pass an ID, or set -1 to create a new record.
     *
     * @param input_id The ID of the record to read, or -1 to create new.
     * @throws java.sql.SQLException
     */
    public L1CaloInfo(int input_id) throws SQLException {
        super(input_id, "L1CI_");

        keyValue.putFirst("GLOBAL_EM_SCALE", 1.0, "Global EM Scale");
        keyValue.putFirst("GLOBAL_JET_SCALE", 1.0, "Global JET Scale");
        ConnectionManager mgr = ConnectionManager.getInstance();
        final int dbver = mgr.loadSchemaVersion(mgr.getConnection());
        ///Mark TODO: remove the check below
//        if(dbver >= DBVER_WITH_XS) {
        keyValue.putFirst(XS_SIGMA_SCALE, 0, "Significance Scale");
        keyValue.putFirst(XS_SIGMA_OFFSET, 0, "Significance Offset ");
        keyValue.putFirst(XS_XE_MIN, 0, "significance XE cut min");
        keyValue.putFirst(XS_XE_MAX, 0, "significance XE cut max");
        keyValue.putFirst(XS_TESQRT_MIN, 0, "Mininum Sqrt of TE");
        keyValue.putFirst(XS_TESQRT_MAX, 0, "Max Sqrt of TE");
//        } 

        keyValue.putFirst("MIN_TOB_EM", -1, "min_tob_em");
        keyValue.putFirst("MIN_TOB_TAU", -1, "min_tob_tau");
        keyValue.putFirst("MIN_TOB_JETS", -1, "min_tob_jets");
        keyValue.putFirst("MIN_TOB_JETL", -1, "min_tob_jetl");
        keyValue.putFirst("ISO_HA_EM", -1, "iso_ha_em");
        keyValue.putFirst("ISO_EM_EM", -1, "iso_em_em");
        keyValue.putFirst("ISO_EM_TAU", -1, "iso_em_tau");

        if (input_id > 0) {
            forceLoad();
        }
    }

    /**
     * Get the global em scale.
     *
     * @return The global em scale.
     */
    public Double get_global_em_scale() {
        return (Double) keyValue.get("GLOBAL_EM_SCALE");
    }

    /**
     * Set the global em scale.
     *
     * @param inp The new value of the global em scale.
     */
    public void set_global_em_scale(final Double inp) {
        keyValue.put("GLOBAL_EM_SCALE", inp);
    }

    /**
     * Get the global jet scale.
     *
     * @return The global jet scale.
     */
    public Double get_global_jet_scale() {
        return (Double) keyValue.get("GLOBAL_JET_SCALE");
    }

    /**
     * Set the global jet scale.
     *
     * @param inp The new value of the global jet scale.
     */
    public void set_global_jet_scale(final Double inp) {
        keyValue.put("GLOBAL_JET_SCALE", inp);
    }

    /**
     * Get the scale for the miss ET significance calculation.
     *
     * @return the scale for the miss ET sig look up table.
     */
    public Integer get_xs_sigma_scale() {
        return (Integer) keyValue.get(XS_SIGMA_SCALE);
    }

    /**
     * Get the offset for the miss ET significance calculation.
     *
     * @return the offset for the miss ET sig look up table.
     */
    public Integer get_xs_sigma_offset() {
        return (Integer) keyValue.get(XS_SIGMA_OFFSET);
    }

    /**
     * Get the minimum miss ET
     *
     * @return the minimum ET cut.
     */
    public Integer get_xs_xe_min() {
        return (Integer) keyValue.get(XS_XE_MIN);
    }

    /**
     * Get the maximum miss ET
     *
     * @return the maximum ET cut.
     */
    public Integer get_xs_xe_max() {
        return (Integer) keyValue.get(XS_XE_MAX);
    }

    /**
     * Get the minimum sqrt of the Total E.
     *
     * @return the minimum Total E cut.
     */
    public Integer get_xs_tesqrt_min() {
        return (Integer) keyValue.get(XS_TESQRT_MIN);
    }

    /**
     * Get the max sqrt of the Total E.
     *
     * @return the max Total E cut.
     */
    public Integer get_xs_tesqrt_max() {
        return (Integer) keyValue.get(XS_TESQRT_MAX);
    }

    /**
     * Get the scale for the miss ET significance calculation.
     *
     * @param sigma_scale
     */
    public void set_xs_sigma_scale(final Integer sigma_scale) {
        keyValue.put(XS_SIGMA_SCALE, sigma_scale);
    }

    /**
     * Set the offset for the miss ET significance calculation.
     *
     * @param sigma_offset
     */
    public void set_xs_sigma_offset(final Integer sigma_offset) {
        keyValue.put(XS_SIGMA_OFFSET, sigma_offset);
    }

    /**
     * Set the minimum miss ET
     *
     * @param xe_min
     */
    public void set_xs_xe_min(final Integer xe_min) {
        keyValue.put(XS_XE_MIN, xe_min);
    }

    /**
     * Set the maximum miss ET
     *
     * @param xe_max
     */
    public void set_xs_xe_max(final Integer xe_max) {
        keyValue.put(XS_XE_MAX, xe_max);
    }

    /**
     * Set the minimum sqrt of the Total E.
     *
     * @param tesqrt_min
     */
    public void set_xs_tesqrt_min(final Integer tesqrt_min) {
        keyValue.put(XS_TESQRT_MIN, tesqrt_min);
    }

    /**
     * Set the max sqrt of the Total E.
     *
     * @param tesqrt_max
     */
    public void set_xs_tesqrt_max(final Integer tesqrt_max) {
        keyValue.put(XS_TESQRT_MAX, tesqrt_max);
    }

    /**
     * Get the min tob em id
     *
     * @return The min tob em id
     */
    public Integer get_min_tob_em() {
        return (Integer) keyValue.get("MIN_TOB_EM");
    }

    /**
     * Set the min tob em id
     *
     * @param inp The new value of the min tob em id
     */
    public void set_min_tob_em(final Integer inp) {
        keyValue.put("MIN_TOB_EM", inp);
    }

    /**
     * Get the min tob tau id
     *
     * @return The min tob tau id
     */
    public Integer get_min_tob_tau() {
        return (Integer) keyValue.get("MIN_TOB_TAU");
    }

    /**
     * Set the min tob tau id
     *
     * @param inp The new value of the min tob tau id
     */
    public void set_min_tob_tau(final Integer inp) {
        keyValue.put("MIN_TOB_TAU", inp);
    }

    /**
     * Get the min tob jets id
     *
     * @return The min tob jets id
     */
    public Integer get_min_tob_jets() {
        return (Integer) keyValue.get("MIN_TOB_JETS");
    }

    /**
     * Set the min tob jets id
     *
     * @param inp The new value of the min tob jets id
     */
    public void set_min_tob_jets(final Integer inp) {
        keyValue.put("MIN_TOB_JETS", inp);
    }

    /**
     * Get the min tob jetl id
     *
     * @return The min tob jetl id
     */
    public Integer get_min_tob_jetl() {
        return (Integer) keyValue.get("MIN_TOB_JETL");
    }

    /**
     * Set the min tob jetl id
     *
     * @param inp The new value of the min tob jetl id
     */
    public void set_min_tob_jetl(final Integer inp) {
        keyValue.put("MIN_TOB_JETL", inp);
    }

    /*
     * Quick function to return an array of all the attached Tobs
     * Should this class load these once created? So they are already in memory?
     */
    /**
     *
     * @return @throws SQLException
     */
    public ArrayList<L1CaloMinTob> get_all_tobs() throws SQLException {
        L1CaloMinTob tobEM = new L1CaloMinTob(this.get_min_tob_em());
        L1CaloMinTob tobTAU = new L1CaloMinTob(this.get_min_tob_tau());
        L1CaloMinTob tobJETS = new L1CaloMinTob(this.get_min_tob_jets());
        L1CaloMinTob tobJETL = new L1CaloMinTob(this.get_min_tob_jetl());
        ArrayList<L1CaloMinTob> TobList = new ArrayList<>();
        TobList.add(tobEM);
        TobList.add(tobTAU);
        TobList.add(tobJETS);
        TobList.add(tobJETL);
        return TobList;
    }

    /**
     * Get the iso ha em id
     *
     * @return The is ha em id
     */
    public Integer get_iso_ha_em() {
        return (Integer) keyValue.get("ISO_HA_EM");
    }

    /**
     * Set the is ha em id
     *
     * @param inp The new value of the iso ha em id
     */
    public void set_iso_ha_em(final Integer inp) {
        keyValue.put("ISO_HA_EM", inp);
    }

    /**
     * Get the iso em em id
     *
     * @return The is em em id
     */
    public Integer get_iso_em_em() {
        return (Integer) keyValue.get("ISO_EM_EM");
    }

    /**
     * Set the is em em id
     *
     * @param inp The new value of the iso em em id
     */
    public void set_iso_em_em(final Integer inp) {
        keyValue.put("ISO_EM_EM", inp);
    }

    /**
     * Get the iso em tau id
     *
     * @return The is em tau id
     */
    public Integer get_iso_em_tau() {
        return (Integer) keyValue.get("ISO_EM_TAU");
    }

    /**
     * Set the is em tau id
     *
     * @param inp The new value of the iso em tau id
     */
    public void set_iso_em_tau(final Integer inp) {
        keyValue.put("ISO_EM_TAU", inp);
    }

    /*
     * Quick function to return an array of all the attached isolations
     * Should this class load these once created? So they are already in memory?
     */
    /**
     *
     * @return @throws SQLException
     */
    public ArrayList<L1CaloIsolation> get_all_isolations() throws SQLException {
        L1CaloIsolation calo_ha_em_iso = new L1CaloIsolation(this.get_iso_ha_em());
        L1CaloIsolation calo_em_em_iso = new L1CaloIsolation(this.get_iso_em_em());
        L1CaloIsolation calo_em_tau_iso = new L1CaloIsolation(this.get_iso_em_tau());
        ArrayList<L1CaloIsolation> IsoList = new ArrayList<>();
        IsoList.add(calo_ha_em_iso);
        IsoList.add(calo_em_em_iso);
        IsoList.add(calo_em_tau_iso);
        return IsoList;
    }

    /**
     * Get the iso em tau id
     *
     * @return The is em tau id
     */
    public Integer get_xe_red_eta_min() {
        return (Integer) keyValue.get("XE_RED_ETA_MIN");
    }

    /**
     *
     * @param inp
     */
    public void set_xe_red_eta_min(final Integer inp) {
        keyValue.put("XE_RED_ETA_MIN", inp);
    }

    /**
     * Get the iso em tau id
     *
     * @return The is em tau id
     */
    public Integer get_xe_red_eta_max() {
        return (Integer) keyValue.get("XE_RED_ETA_MAX");
    }

    /**
     *
     * @param inp
     */
    public void set_xe_red_eta_max(final Integer inp) {
        keyValue.put("XE_RED_ETA_MAX", inp);
    }

    /**
     * Simple save, no children. Simple save routine that checks for a match of
     * this record, ignoring the usual fields (fluff).
     *
     * @return The ID of the saved record.
     * @throws java.sql.SQLException Stop on SQL problems.
     */
    @Override
    public int save() throws SQLException {
        ConnectionManager mgr = ConnectionManager.getInstance();
        final int dbver = mgr.loadSchemaVersion(mgr.getConnection());
        //Mark TODO: remove this check of db version
//        if(dbver < DBVER_WITH_XS && keyValue.containsKey(XS_SIGMA_SCALE)) {
//            logger.warning("DB does not support XS trigger, Removing XS entries from L1CaloInfo before saving");
//            
//            keyValue.remove(XS_SIGMA_SCALE);
//            keyValue.remove(XS_SIGMA_OFFSET);
//            keyValue.remove(XS_XE_MIN);
//            keyValue.remove(XS_XE_MAX);
//            keyValue.remove(XS_TESQRT_MIN);
//            keyValue.remove(XS_TESQRT_MAX);
//        }

        ArrayList<Integer> ids = mgr.get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");

        if (ids.isEmpty()) {
            set_id(mgr.save(getTableName(), tablePrefix, get_id(), keyValue));
        } else {
            set_id(ids.get(0));
        }

        return get_id();
    }

    /**
     * Add all the children to the tree.
     * @throws java.sql.SQLException
     */
    @Override
    public void addToTree(final DefaultMutableTreeNode treeNode, int counter) throws SQLException {
        --counter;
       
        if (counter >= 0) {
            if (sinCosLayer.isEmpty()) {
                for (L1CaloSinCos csc : getCaloSinCos()) {//what is sincos? why empty method?
                    DefaultMutableTreeNode anotherLayer = new DefaultMutableTreeNode(csc);
                    treeNode.add(anotherLayer);
                    sinCosLayer.add(anotherLayer);
                }
            } else {
                int i = 0;
                for (L1CaloSinCos csc : getCaloSinCos()) {
                    csc.addToTree(sinCosLayer.get(i), counter);
                    i++;
                }
            }
        }
         if (counter >= 0) {
            if (isoLayer.isEmpty()) {
                for (L1CaloIsolation iso : get_all_isolations()) {
                    DefaultMutableTreeNode singleIsoLayer = new DefaultMutableTreeNode(iso);
                    treeNode.add(singleIsoLayer);
                    isoLayer.add(singleIsoLayer);
                }
            } else {
                int i = 0;
                for (L1CaloIsolation iso : get_all_isolations()) {
                    iso.addToTree(isoLayer.get(i), counter);
                    i++;
                }
            }
        }
          if (counter >= 0) {
            if (tobLayer.isEmpty()) {
                for (L1CaloMinTob tob : get_all_tobs()) {
                    DefaultMutableTreeNode singletobLayer = new DefaultMutableTreeNode(tob);
                    treeNode.add(singletobLayer);
                    tobLayer.add(singletobLayer);
                }
            } else {
                int i = 0;
                for (L1CaloMinTob tob : get_all_tobs()) {
                    tob.addToTree(tobLayer.get(i), counter);
                    i++;
                }
            }
        }
    }

    @Override
    public int doDiff(AbstractTable t, DefaultMutableTreeNode treeNode, java.util.Set<String> linkstoignore) throws SQLException {
        /// Don't consider the menu id
        if (linkstoignore == null) {
            linkstoignore = new java.util.HashSet<>();
        }

        int ndiff = super.doDiff(t, treeNode, linkstoignore);

        if (t instanceof L1CaloInfo) {
            L1CaloInfo info = (L1CaloInfo) t;

            ndiff += triggerdb.Diff.CompareTables.compareTables(this.get_all_isolations(), info.get_all_isolations(), treeNode);
            ndiff += triggerdb.Diff.CompareTables.compareTables(this.get_all_tobs(), info.get_all_tobs(), treeNode);

        }
        return ndiff;
    }

    /**
     * Display a string representation of this object.
     *
     * @return String representation.
     */
    @Override
    public String toString() {
        if (get_id() == -1) {
            return "L1 Calo Info";
        }
        return "L1 CALO INFO: ID=" + get_id() + ", Name=" + get_name() + ", Version=" + get_version();
    }

    /**
     * Create a copy of this record in memory.
     *
     * @return Copy of this record.
     */
    @Override
    public Object clone() {
        L1CaloInfo copy = new L1CaloInfo();
        copy.set_name(get_name());
        copy.set_version(get_version());
        copy.set_global_em_scale(get_global_em_scale());
        copy.set_global_jet_scale(get_global_jet_scale());
        //Mark TODO missing metxs? and new tables (iso + min tob)
        return copy;
    }

    /**
     * Not implemented yet.
     *
     * @return Empty vector.
     */
    private ArrayList<L1CaloSinCos> getCaloSinCos() {
        return new ArrayList<>();
    }
    
    public ArrayList<L1CaloSinCos> getCaloSinCosPublic(){
        return getCaloSinCos();
    }

    @Override
    public String getTableName() {
        return "L1_CALO_INFO";
    }
}
