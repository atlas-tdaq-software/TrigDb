package triggerdb.Entities.L1;

import java.sql.SQLException;

/**
 *
 * Interface that declares most methods of L1Prescale. This is implemented by 
 * {@link triggertool.L1Records.L1Prescale} and {@link L1PrescaleSetAlias}
 * @author Tiago Perez <tperez@cern.ch>
 * @version 2012-04-23
 */
public interface L1PrescaleI {

    /**
     * Set one of the 256 prescales, 1 up to and including 256.
     *
     * @param ctpId the ctp id. Set one of the 256 prescales, 1 up to and
     * including 256.
     * @param prescaleValue the input value. NOTE: the prescaleValues are
     * instances of L1PSNumber, therefore is mandatory to use the "longValue()"
     * method in the queries.
     * @throws java.sql.SQLException
     */
    public void set_val(final int ctpId, final Integer prescaleValue) throws SQLException;

    /**
     *
     * @param index
     * @return
     */
    public Integer get_val(final int index);

    /**
     * Get the id of the link to the menu.
     *
     * @param l1_menu_id The menu id
     * @return the id of the link TM <-> PSset.
     * @throws java.sql.SQLException
     */
    public int get_TMPSid(final int l1_menu_id) throws SQLException;

    /**
     * Checks whether this prescale set hidden in this menu.
     *
     * @param l1_menu_id The menu id.
     * @return
     * <code>true</code> if hidden else
     * <code>false</code>
     * @throws java.sql.SQLException
     */
    public boolean getHidden(final int l1_menu_id) throws SQLException;

    /**
     * Is this default?
     * @return 
     * @throws java.sql.SQLException 
     */
    public Integer get_default() throws SQLException;

    /**
     * Set default.
     * @param def
     * @throws java.sql.SQLException
     */
    public void set_default(final Integer def) throws SQLException;

    /**
     * 
     * @param _comment 
     * @throws java.sql.SQLException 
     */
    public void update_comment(final String _comment) throws SQLException;

    /**
     * Set a comment about this PSS.
     *
     * @param inp
     * @throws java.sql.SQLException
     */
    public void set_comment(final String inp) throws SQLException;

    /**
     * Get the comment about this configuration.
     *
     * @return
     * @throws java.sql.SQLException
     */
    public String get_comment() throws SQLException;
}
