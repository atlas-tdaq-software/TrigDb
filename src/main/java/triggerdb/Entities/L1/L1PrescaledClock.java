package triggerdb.Entities.L1;

import java.sql.SQLException;
import java.util.ArrayList;

import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;

///L1 Prescaled Clock
/**
 * Class to hold the information from L1_PRESCALED_CLOCK in memory
 * whilst the user browses or edits.  It only holds two unique variables
 * clock1 and clock2 which are both integer
 * 
 * @author Simon Head
 * @author Johannes Noller
 */
public final class L1PrescaledClock extends AbstractTable {

    ///Make a L1PrescaledClock object.
    /**
     * Standard constructor. If an ID of -1 is supplied we create a new, empty
     * record and don't attempt to load anything from the database.  If the ID
     * supplied is greater than 0 we load this record from the database.
     * 
     */
    public L1PrescaledClock() {
        super("L1PC_");
        setKeys();
    }

    /**
     *
     * @param input_id
     * @throws SQLException
     */
    public L1PrescaledClock(int input_id) throws SQLException {
        super(input_id, "L1PC_");
        setKeys();
        if (input_id > 0) {
            forceLoad();
        }
    }

    void setKeys() {
        keyValue.putFirst("CLOCK1", 1, "Clock 1");
        keyValue.putFirst("CLOCK2", 2, "Clock 2");
    }
    
    ///Get the value of Clock 1.

    /**
     *
     * @return
     */
    public Integer get_clock1() {
        return (Integer) keyValue.get("CLOCK1");
    }

    ///Get the value of Clock 2.

    /**
     *
     * @return
     */
    public Integer get_clock2() {
        return (Integer) keyValue.get("CLOCK2");
    }

    ///Set the value of Clock 1.

    /**
     *
     * @param inp
     */
    public void set_clock1(int inp) {
        keyValue.put("CLOCK1", inp);
    }

    ///Set the value of Clock 2.

    /**
     *
     * @param inp
     */
    public void set_clock2(int inp) {
        keyValue.put("CLOCK2", inp);
    }

    ///Since prescaled clock has no children, the save routine is simple.
    @Override
    public int save() throws SQLException {
        ArrayList<Integer> ids = ConnectionManager.getInstance().get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");

        if (ids.isEmpty()) {
            set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix, get_id(), keyValue));
        } else {
            set_id(ids.get(0));
        }

        return get_id();
    }

    ///String representation of the object.
    /**
     * Prepare the string that we will show in the tree view.
     * @return 
     */
    @Override
    public String toString() {
        if (get_id() == -1) {
            return "L1 Prescaled Clock";
        }

        return "L1 PRESCALED CLOCK: id=" + get_id() + ", name=" + get_name() + ", ver=" + get_version();
    }

    ///Copy this object and place it in a new object.
    /** 
     * Create a full copy of this class.  used = false and id = -1, ready for
     * when the user wants to save.
     * @return 
     */
    @Override
    public Object clone() {
        L1PrescaledClock copy = new L1PrescaledClock();
        copy.set_name(get_name());
        copy.set_version(get_version());
        copy.set_clock1(get_clock1());
        copy.set_clock2(get_clock2());
        return copy;
    }

    @Override
    public String getTableName() {
         return "L1_PRESCALED_CLOCK";
   }
}