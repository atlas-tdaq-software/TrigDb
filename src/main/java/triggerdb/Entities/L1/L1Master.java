package triggerdb.Entities.L1;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import javax.swing.tree.DefaultMutableTreeNode;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;

/**
 * L1Master table row. To hold the data from L1_MASTER_TABLE. A Master table
 * only contains simple one-to-one links, but a lot of them.
 *
 * @author Simon Head
 */
public final class L1Master extends AbstractTable {

    /**
     * L1Menu object that this L1Master links to.
     */
    private L1Menu l1Menu = null;
    /**
     * L1PrescaledClock object that this L1Master links to.
     */
    private L1PrescaledClock l1PrescaledClock = null;
    /**
     * L1MuctpiInfo object that this L1Master links to.
     */
    private L1MuctpiInfo l1MuctpiInfo = null;
    /**
     * L1CaloInfo object that this L1Master links to.
     */
    private L1CaloInfo l1CaloInfo = null;
    /**
     * L1MuonThresholdSet object that this L1Master links to.
     */
    private L1MuonThresholdSet l1MuonThresholdSet = null;
    /**
     * The tree knows if it has laded the branches containing L1Menu.
     */
    private DefaultMutableTreeNode menuLayer = null;
    /**
     * The tree knows if it has laded the branches containing L1Random.
     */
    private DefaultMutableTreeNode randomLayer = null;
    /**
     * The tree knows if it has laded the branches containing L1PrescaledClock.
     */
    private DefaultMutableTreeNode pcLayer = null;
    /**
     * The tree knows if it has laded the branches containing L1MuctpiInfo.
     */
    private DefaultMutableTreeNode muctpiLayer = null;
    /**
     * The tree knows if it has laded the branches containing L1MuonThresholdSet.
     */
    private DefaultMutableTreeNode mtsLayer = null;
    /**
     * The tree knows if it has laded the branches containing L1CaloInfo.
     */
    private DefaultMutableTreeNode ciLayer = null;
    /**
     * L1Random object that this L1Master links to.
     */
    private L1Random l1Random = null;
    
    /**
     *
     */
    public L1Master() {
        super(-1, "L1MT_");
        DefineKeyValuePairs();
    }
        
    /**
     * Standard constructor. If an ID of -1 is supplied we create a new, empty
     * record and don't attempt to load anything from the database. If the ID
     * supplied is greater than 0 we load this record from the database.
     *
     * @param input_id The ID to load or -1.
     * @throws java.sql.SQLException
     */
    public L1Master(final int input_id) throws SQLException {
        super(input_id, "L1MT_");
        DefineKeyValuePairs();

        if (input_id > 0) {
            this.forceLoad();                    
            l1Menu = new L1Menu(get_menu_id());
        }
    }

    private void DefineKeyValuePairs() {
        this.keyValue.putFirst("TRIGGER_MENU_ID", -1, "Menu ID");
        this.keyValue.putFirst("RANDOM_ID", -1, "Random ID");
        this.keyValue.putFirst("MUCTPI_INFO_ID", -1, "MuCTPi Info ID");
        this.keyValue.putFirst("PRESCALED_CLOCK_ID", -1, "PC ID");
        this.keyValue.putFirst("CALO_INFO_ID", -1, "Calo Info ID");
        this.keyValue.putFirst("STATUS", -1, "Status");
        this.keyValue.putFirst("MUON_THRESHOLD_SET_ID", -1, "MTS ID");
        this.keyValue.putFirst("CTPVERSION", -1, "CTP Version");
        this.keyValue.putFirst("L1VERSION", -1, "L1 Version");
    }

    ///Create the minimum records needed for an empty L1 Configuration.
    /**
     * Create one of all the tables that the L1Master table links to. Since we
     * can only have 8 L1BunchGroup records too, create those now.
     */
    public void create_new_links() {
        l1Random = new L1Random();
        l1PrescaledClock = new L1PrescaledClock();
        l1PrescaledClock.set_name("L1PC");
        l1MuctpiInfo = new L1MuctpiInfo();
        l1MuctpiInfo.set_name("L1Muctpi");
        l1CaloInfo = new L1CaloInfo();
        l1CaloInfo.set_name("L1CaloInfo");
    }

    ///Force the L1Master to be read from the database again.
    /**
     * After an edit we need to force the record to read from the database.
     * @throws java.sql.SQLException
     */
    @Override
    public void forceLoad() throws SQLException {
        menuLayer = null;
        randomLayer = null;
        pcLayer = null;
        muctpiLayer = null;
        mtsLayer = null;
        ciLayer = null;
        l1Menu = null;
        l1Random = null;
        l1PrescaledClock = null;
        l1MuctpiInfo = null;
        l1CaloInfo = null;
        l1MuonThresholdSet = null;

        ConnectionManager.getInstance().forceLoad(this);
    }

    
    ///Get the ID of the muon threshold set.

    /**
     *
     * @return
     */
    public Integer get_muon_threshold_set_id() {
        return (Integer) keyValue.get("MUON_THRESHOLD_SET_ID");
    }
    ///Set the ID of the muon threshold set

    /**
     *
     * @param inp
     */
    public void set_muon_threshold_set_id(final int inp) {
        keyValue.put("MUON_THRESHOLD_SET_ID", inp);
    }

    ///Get the ID of the L1Menu this L1Master uses.

    /**
     *
     * @return
     */
    public Integer get_menu_id() {
        return (Integer) keyValue.get("TRIGGER_MENU_ID");
    }
    ///Change the L1Menu ID in this L1Master.

    /**
     *
     * @param inp
     */
    public void set_menu_id(final int inp) {
        keyValue.put("TRIGGER_MENU_ID", inp);
    }

    ///Get the ID of the L1MuctpiInfo this L1Master is linked to.,

    /**
     *
     * @return
     */
    public Integer get_muctpi_info_id() {
        return (Integer) keyValue.get("MUCTPI_INFO_ID");
    }
    ///Set the L1MuctpiInfo ID.

    /**
     *
     * @param inp
     */
    public void set_muctpi_info_id(final int inp) {
        keyValue.put("MUCTPI_INFO_ID", inp);
    }

    ///Get the ID of the L1Random this L1Master is linked to.

    /**
     *
     * @return
     */
    public Integer get_random_id() {
        return (Integer) keyValue.get("RANDOM_ID");
    }

    ///Set the L1Random ID.

    /**
     *
     * @param inp
     */
    public void set_random_id(final int inp) {
        keyValue.put("RANDOM_ID", inp);
    }

    ///Get the ID of the L1PrescaledClock this L1Master is linked to.

    /**
     *
     * @return
     */
    public Integer get_prescaled_clock_id() {
        return (Integer) keyValue.get("PRESCALED_CLOCK_ID");
    }
    ///Set the L1PrescaledClock ID.

    /**
     *
     * @param inp
     */
    public void set_prescaled_clock_id(final int inp) {
        keyValue.put("PRESCALED_CLOCK_ID", inp);
    }

    ///Get the ID of the L1CaloInfo that this L1Master is linked to.

    /**
     *
     * @return
     */
    public Integer get_calo_info_id() {
        return (Integer) keyValue.get("CALO_INFO_ID");
    }
    ///Set the L1CaloInfo ID.

    /**
     *
     * @param inp
     */
    public void set_calo_info_id(final int inp) {
        keyValue.put("CALO_INFO_ID", inp);
    }

    ///Load the L1Menu object, if it hasn't been loaded before go to the DB to get it.

    /**
     *
     * @return
     */
    public L1Menu get_menu() throws SQLException {
        if(l1Menu == null){
            l1Menu = new L1Menu(get_menu_id());
        }
        return l1Menu;
    }

    ///Load the L1Muon Threshold Set object, if it hasn't been loaded before go to the DB to get it.

    /**
     *
     * @return
     * @throws SQLException
     */
    public L1MuonThresholdSet get_muon_threshold_set() throws SQLException {
        if (l1MuonThresholdSet == null && new L1MuonThresholdSet().isValid(get_muon_threshold_set_id())) {
            l1MuonThresholdSet = new L1MuonThresholdSet(get_muon_threshold_set_id());
        }
        return l1MuonThresholdSet;
    }

    ///Load the L1Random object, if it hasn't been loaded before go to the DB to get it.

    /**
     *
     * @return
     * @throws SQLException
     */
    public L1Random get_random() throws SQLException {
        if (l1Random == null && new L1Random().isValid(get_random_id())) {
            l1Random = new L1Random(get_random_id());
        }
        return l1Random;
    }
    
    ///Load the L1PrescaledClock object, if it hasn't been loaded before go to the DB to get it.

    /**
     *
     * @return
     * @throws SQLException
     */
    public L1PrescaledClock get_prescaled_clock() throws SQLException {
        if (l1PrescaledClock == null && new L1PrescaledClock().isValid(get_prescaled_clock_id())) {
            l1PrescaledClock = new L1PrescaledClock(get_prescaled_clock_id());
        }
        return l1PrescaledClock;
    }

    ///Load the L1MuctpiInfo object, if it hasn't been loaded before go to the DB to get it.

    /**
     *
     * @return
     * @throws SQLException
     */
    public L1MuctpiInfo get_muctpi_info() throws SQLException {
        if (l1MuctpiInfo == null && new L1MuctpiInfo().isValid(get_muctpi_info_id())) {
            l1MuctpiInfo = new L1MuctpiInfo(get_muctpi_info_id());
        }
        return l1MuctpiInfo;
    }

    ///Load the L1CaloInfo object, if it hasn't been loaded before go to the DB to get it.

    /**
     *
     * @return
     * @throws SQLException
     */
    public L1CaloInfo get_calo_info() throws SQLException {
        if (l1CaloInfo == null && new L1CaloInfo().isValid(get_calo_info_id())) {
            l1CaloInfo = new L1CaloInfo(get_calo_info_id());
        }
        return l1CaloInfo;
    }

    /**
     *
     * @return
     */
    public Integer get_ctpVersion() {
        return (Integer) keyValue.get("CTPVERSION");
    }

    /**
     *
     * @param ctpVersion
     */
    public void set_ctpVersion(int ctpVersion) {
         keyValue.put("CTPVERSION", ctpVersion);
    }

    /**
     *
     * @return
     */
    public Integer get_l1Version() {
        return (Integer) keyValue.get("L1VERSION");
    }

    /**
     *
     * @param l1Version
     */
    public void set_l1Version(int l1Version) {
         keyValue.put("L1VERSION", l1Version);
    }
    
    @Override
    public int doDiff(AbstractTable t, DefaultMutableTreeNode treeNode, java.util.Set<String> linkstoignore) throws SQLException {
        /// Don't consider the menu id
        if (linkstoignore == null) {
            linkstoignore = new java.util.HashSet<>();
        }
        linkstoignore.add("TRIGGER_MENU_ID");
        linkstoignore.add("MUCTPI_INFO_ID");
        linkstoignore.add("RANDOM_ID");
        linkstoignore.add("PRESCALED_CLOCK_ID");
        linkstoignore.add("CALO_INFO_ID");
        int ndiff = super.doDiff(t, treeNode, linkstoignore);

        if (t instanceof L1Master) {
            L1Master master = (L1Master) t;
            if (!Objects.equals(this.get_menu_id(), master.get_menu_id())) { //different menus
                DefaultMutableTreeNode menu = new DefaultMutableTreeNode(master.get_menu().getTableName());
                treeNode.add(menu);
                ndiff += this.get_menu().doDiff(master.get_menu(), menu, null);
            }
            if (!Objects.equals(this.get_muctpi_info_id(), master.get_muctpi_info_id())) { // different mu ctpi info
                DefaultMutableTreeNode muctpi = new DefaultMutableTreeNode("L1_MUCTPI_Info");
                treeNode.add(muctpi);
                ndiff += this.get_muctpi_info().doDiff(master.get_muctpi_info(), muctpi, null);
            }
            if (!Objects.equals(this.get_random_id(), master.get_random_id())) { //different L1Random
                DefaultMutableTreeNode random = new DefaultMutableTreeNode("L1_Random");
                treeNode.add(random);
                ndiff += this.get_random().doDiff(master.get_random(), random, null);
            }
            if (!Objects.equals(this.get_prescaled_clock_id(), master.get_prescaled_clock_id())) { // Different prescaled clock
                DefaultMutableTreeNode clock = new DefaultMutableTreeNode("L1_Prescaled_Clock");
                treeNode.add(clock);
                ndiff += this.get_prescaled_clock().doDiff(master.get_prescaled_clock(), clock, null);
            }
            if (!Objects.equals(this.get_calo_info_id(), master.get_calo_info_id())) { //different L1 calo info
                DefaultMutableTreeNode l1calo = new DefaultMutableTreeNode("L1_Calo_Info");
                treeNode.add(l1calo);
                ndiff += this.get_calo_info().doDiff(master.get_calo_info(), l1calo, null);
            }

        }

        return ndiff;
    }

    @Override
    public ArrayList<Object> get_min_info() throws SQLException {
        ArrayList<Object> info = super.get_min_info();
        info.add(get_status() % 2 == 1 ? "Active L1 Menu" : "");
        info.add(get_username());
        info.add(get_modified());
        return info;
    }

    @Override
    public ArrayList<String> get_min_names() {
        ArrayList<String> names = super.get_min_names();
        names.add("Status");
        names.add("Creator");
        names.add("Date");
        return names;
    }

 
    
    /**
     * 
     * @return id of active L1 Master entry
     * @throws java.sql.SQLException
     */
    public static int getActiveId() throws SQLException {
        int activeId = -1;
        ConnectionManager mgr = ConnectionManager.getInstance();
        StringBuilder q = new StringBuilder(100);
        q.append("SELECT L1MT_ID FROM ")
         .append(mgr.getInitInfo().getSchemaModifier())
         .append("L1_MASTER_TABLE WHERE L1MT_STATUS=1");
        ResultSet rset;
        Statement stmt = mgr.getConnection().createStatement();
        logger.log(Level.INFO, q.toString());
        rset = stmt.executeQuery(q.toString());
        if(rset.next()) {
            activeId = rset.getInt("L1MT_ID");
        }
        rset.close();
        stmt.close();
        return activeId;
    }
    
    ///Save is simple, since L1Master has no link tables connected to it.
    /**
     * Since L1Master has no link tables, we can assume the IDs in all the
     * fields are correct and do a simple save.
     *
     * @return The ID of the saved L1Master table record.
     * @throws java.sql.SQLException Stop when we have an SQL problem.
     */
    @Override
    public int save() throws SQLException {
        /// Don't save if only the name is different
        String name = get_name();
        keyValue.remove("NAME");
        ConnectionManager mgr = ConnectionManager.getInstance();
        ArrayList<Integer> ids = mgr.get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");

        keyValue.put("NAME", name);

        if (ids.isEmpty()) {
            set_id(mgr.save(getTableName(), tablePrefix, get_id(), keyValue));
        } else {
            set_id(ids.get(0));
        }

        return get_id();
    }

    ///String representation of the object.
    /**
     * So the tree view knows what information to display.
     *
     * @return A string containing the id, name and version of the record.
     */
    @Override
    public String toString() {
        if (get_id() == -1) {
            return "L1 Master Table";
        }

        return "L1 MASTER: ID=" + get_id() + ", Name=" + get_name() + ", Version=" + get_version();
    }

    ///Add all the linked records to the tree.
    /**
     * Add all the children to the tree.
     * @throws java.sql.SQLException
     */
    @Override
    public void addToTree(DefaultMutableTreeNode treeNode, int counter) throws SQLException {
        --counter;

        if (counter >= 0) {

            if (menuLayer == null && get_menu() != null) {
                menuLayer = new DefaultMutableTreeNode(get_menu());
                treeNode.add(menuLayer);
                get_menu().addToTree(menuLayer, counter);
            } else if (get_menu() != null) {
                get_menu().addToTree(menuLayer, counter);
            }

            if (mtsLayer == null && get_muon_threshold_set() != null) {
                mtsLayer = new DefaultMutableTreeNode(get_muon_threshold_set());
                treeNode.add(mtsLayer);
                get_muon_threshold_set().addToTree(mtsLayer, counter);
            } else if (get_muon_threshold_set() != null) {
                get_muon_threshold_set().addToTree(mtsLayer, counter);
            }

            if (muctpiLayer == null && get_muctpi_info() != null) {
                treeNode.add(muctpiLayer = new DefaultMutableTreeNode(get_muctpi_info()));
            }

            if (randomLayer == null && get_random() != null) {
                treeNode.add(randomLayer = new DefaultMutableTreeNode(get_random()));
            }

            if (pcLayer == null && get_prescaled_clock() != null) {
                treeNode.add(pcLayer = new DefaultMutableTreeNode(get_prescaled_clock()));
            }

            if (ciLayer == null && get_calo_info() != null) {
                ciLayer = new DefaultMutableTreeNode(get_calo_info());
                treeNode.add(ciLayer);
                get_calo_info().addToTree(ciLayer, counter);
            }
        }

    }

    ///Make a copy of this L1MasterTable.
    /**
     * Note that we can just do a simple copy of the IDs of the linked tables.
     *
     * @return A copy of this L1MasterTable.
     */
    @Override
    public Object clone() {
        L1Master copy = new L1Master();
        copy.set_menu_id(get_menu_id());
        copy.set_muon_threshold_set_id(get_muon_threshold_set_id());
        copy.set_muctpi_info_id(get_muctpi_info_id());
        copy.set_random_id(get_random_id());
        copy.set_prescaled_clock_id(get_prescaled_clock_id());
        copy.set_name(get_name());
        copy.set_version(get_version());
        copy.set_calo_info_id(get_calo_info_id());
        copy.set_status(get_status());
        return copy;
    }

    ///Get the status.

    /**
     *
     * @return
     */
    public Integer get_status() {
        return (Integer) keyValue.get("STATUS");
    }

    ///Set the status. Details still need to be defined.

    /**
     *
     * @param inp
     */
    public void set_status(final int inp) {
        keyValue.put("STATUS", inp);
    }

    ///Set the L1Menu object.

    /**
     *
     * @param menu
     */
    public void set_l1_menu(L1Menu menu) {
        l1Menu = menu;
    }

    public L1Menu get_l1_menu (){
        return l1Menu;
    }
    ///Set the comment, a user defined (helpful) string describing the configuration.

    /**
     *
     * @param inp
     */
    public void set_comment(String inp) {
        keyValue.put("COMMENT", inp);
    }

    ///Get the comment.

    /**
     *
     * @return
     */
    public String get_comment() {
        return (String) keyValue.get("COMMENT");
    }

    ///////////////////////////////////////////
    // STATIC METHODS
    ///////////////////////////////////////////
    /**
     * Query the DB for the list of L1 Master table.
     *
     * @return The list of L1 Master Tables sorted by STATUS and L1MT_ID
     * (STATUS=1 first, then L1MT_ID highest to lowest).
     * @throws java.sql.SQLException
     */
    public static List<L1Master> getL1Tables() throws SQLException{
        List<L1Master> l1mastertables = new ArrayList<>();
        String q0 = new L1Master().getQueryString();

        ConnectionManager mgr = ConnectionManager.getInstance();

        String where = " ORDER BY L1MT_STATUS DESC, L1MT_ID DESC ";
        String query = mgr.fix_schema_name(q0 + where);
        PreparedStatement ps = mgr.getConnection().prepareStatement(query);
        ResultSet rset = ps.executeQuery();
        while (rset.next()) {
            L1Master master = new L1Master();
            master.loadFromRset(rset);
            l1mastertables.add(master);
        }
        rset.close();
        ps.close();
        return l1mastertables;
    }

    @Override
    public String getTableName() {
        return "L1_MASTER_TABLE";
    }

    /**
     *
     * @return
     */
    public int findDummy() {
        String query = "SELECT L1MT_ID FROM L1_MASTER_TABLE WHERE L1MT_NAME ='dummyL1'";
        int id = -1;
        query = ConnectionManager.getInstance().fix_schema_name(query);
        try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query); ResultSet rset = ConnectionManager.executeSelect(ps)) {
            while (rset.next()) {
                id=rset.getInt("L1MT_ID");
            }
            rset.close();
            ps.close();
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return id;
    }
}
