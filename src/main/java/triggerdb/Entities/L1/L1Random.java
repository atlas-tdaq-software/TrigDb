package triggerdb.Entities.L1;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;

///L1 Random table.
/**
 * Class contains code to read and manipulate a single row from the L1 Random
 * table.  Note that a configuration contains one L1 Random record, which
 * defines the details of the Random Clock triggers.  
 * 
 * @author Simon Head
 */
public final class L1Random extends AbstractTable {
    
    /**
     *
     */
    public L1Random() {
        super("L1R_");
        setKeys();
     }
     
    /**
     * Standard constructor. If an ID of -1 is supplied we create a new, empty
     * record and don't attempt to load anything from the database.  If the ID
     * supplied is greater than 0 we load this record from the database.
     * 
     * @param input_id The ID to load or -1.
     * @throws java.sql.SQLException
     */
    public L1Random(int input_id) throws SQLException {
        super(input_id, "L1R_");
        setKeys();
        if (input_id > 0) {
            forceLoad();
        }
    }

    private void setKeys() {
        keyValue.putFirst("CUT0", 0, "Cut 0");
        keyValue.putFirst("CUT1", 0, "Cut 1");
        keyValue.putFirst("CUT2", 0, "Cut 2");
        keyValue.putFirst("CUT3", 0, "Cut 3");
        keyValue.remove("NAME");
        keyValue.remove("VERSION");
    }
    
    ///Get the value of Cut0.

    /**
     *
     * @return
     */
    public Integer get_cut0() {
        return (Integer) keyValue.get("CUT0");
        }

    ///Set the value of Cut0.

    /**
     *
     * @param inp
     */
    public void set_cut0(int inp) {
        keyValue.put("CUT0", inp);
    }
    
    ///Get the value of Cut1.

    /**
     *
     * @return
     */
    public Integer get_cut1() {
        return (Integer) keyValue.get("CUT1");
    }

    ///Set the value of Cut1.

    /**
     *
     * @param inp
     */
    public void set_cut1(int inp) {
        keyValue.put("CUT1", inp);
    }

    ///Get the value of Cut2.

    /**
     *
     * @return
     */
    public Integer get_cut2() {
        return (Integer) keyValue.get("CUT2");
    }

    ///Set the value of Cut2.

    /**
     *
     * @param inp
     */
    public void set_cut2(int inp) {
        keyValue.put("CUT2", inp);
    }

    ///Get the value of Cut3.

    /**
     *
     * @return
     */
    public Integer get_cut3() {
        return (Integer) keyValue.get("CUT3");
    }

    ///Set the value of Cut3.

    /**
     *
     * @param inp
     */
    public void set_cut3(int inp) {
        keyValue.put("CUT3", inp);
    }
    
    ///Save as normal, easy as it has no children.
    /**
     * Save the L1Random record as normal.  First create a hashtable of all the
     * properties (excluding ID) then call the ConnectionManager save function
     * which takes care of the database specific commands.
     * @return 
     * @throws java.sql.SQLException
     */
     @Override
    public int save() throws SQLException {
        ConnectionManager mgr = ConnectionManager.getInstance();
        List<Integer> ids = mgr.get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");

        if (ids.isEmpty()) {
            set_id(mgr.save(getTableName(), tablePrefix, get_id(), keyValue));
        } else {
            set_id(ids.get(0));
        }
        return get_id();
    }

    ///String representation of the Random Trigger.
    /**
     * So the tree view knows what information to display in the tree
     * 
     * @return A string containing the id, name and version of the record
     */
    @Override
    public String toString() {
        if (get_id() == -1) {
            return "L1 Random";
        }
        return "L1 RANDOM: ID=" + get_id() + ", Cut 0=" + get_cut0() + ", Cut 1=" + get_cut1() + ", Cut 2=" + get_cut2() + ", Cut 3=" + get_cut3();
    }

    ///Make a copy of the object.
    /**
     * To ensure Java can correctly copy this class.  The ID is set to -1,
     * so that if the record is saved a new ID will be assigned.
     * 
     * @return A L1Random Object with ID = -1
     */
    @Override
    public Object clone() {
        L1Random copy = new L1Random();
        copy.set_id(-1);
        copy.set_cut0(get_cut0());
        copy.set_cut1(get_cut1());
        copy.set_cut2(get_cut2());
        copy.set_cut3(get_cut3());
        return copy;
    }

    @Override
    public String getTableName() {
         return "L1_RANDOM";
   }
    
    /**
     * Minimum names for the search results.
     * @return 
     */
    @Override
    public ArrayList<String> get_min_names() {
        ArrayList<String> info = new ArrayList<>();
        info.add("ID");
        info.add("Cut 0");
        info.add("Cut 1");
        info.add("Cut 2");
        info.add("Cut 3");
        return info;
    }
    
    /**
     * Minimum information for the search results.
     * @return 
     */
    @Override
    public ArrayList<Object> get_min_info() {
        ArrayList<Object> info = new ArrayList<>();
        info.add(get_id());
        info.add(get_cut0());
        info.add(get_cut1());
        info.add(get_cut2());
        info.add(get_cut3());        
        return info;
    }
}