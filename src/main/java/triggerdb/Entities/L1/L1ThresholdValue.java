package triggerdb.Entities.L1;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;
import triggerdb.TriggerMap;

/**
 * Holds the L1 Trigger Threshold Value in memory. A threshold value has a 
 * lot of fields, but they might not all be used depending on what type
 * of threshold this is.
 * 
 * @author Simon Head
 * @author Johannes Noller
 */
public final class L1ThresholdValue extends AbstractTable {
    
    /**
     *
     */
    public L1ThresholdValue() {
        super("L1TTV_");
        setKeys();
    }
    
    ///Standard constructor.
    /**
     * Pass a value greater than one to load that record, or -1 to create an
     * empty new L1ThresholdValue.
     * 
     * @param input_id The Inital ID.
     * @throws java.sql.SQLException
     */
    public L1ThresholdValue(int input_id) throws SQLException {
        super(input_id, "L1TTV_");
        setKeys();
        if (input_id > 0) {
            forceLoad();
        }
    }

    private void setKeys() {
        keyValue.putFirst("TYPE", "type", "Type");
        keyValue.putFirst("PT_CUT", "1.0", "Pt Cut");
        keyValue.putFirst("ETA_MIN", 1, "Eta Min");
        keyValue.putFirst("ETA_MAX", 1, "Eta Max");
        keyValue.putFirst("PHI_MIN", 1, "Phi Min");
        keyValue.putFirst("PHI_MAX", 1, "Phi Max");
        keyValue.putFirst("EM_ISOLATION", "1.0", "EM Isol");
        keyValue.putFirst("HAD_ISOLATION", "1.0", "Had Isol");
        keyValue.putFirst("HAD_VETO", "1.0", "Had Veto");
        keyValue.putFirst("WINDOW", 1, "Window");
        keyValue.putFirst("PRIORITY", 1, "Priority");
    }
    
    
    ///Set the Trigger Type. This should be allowed.

    /**
     *
     * @param inp
     */
    public void set_type(String inp) {
        keyValue.put("TYPE", inp);
    }
    ///Get the Trigger Type

    /**
     *
     * @return
     */
    public String get_type() {
        return (String) keyValue.get("TYPE");
    }

    ///Get the PT cut as a string!

    /**
     *
     * @return
     */
    public String get_pt_cut() {
        return (String) keyValue.get("PT_CUT");
    }
    ///Set the PT cut.

    /**
     *
     * @param inp
     */
    public void set_pt_cut(String inp) {
        keyValue.put("PT_CUT", inp);
    }

    ///Get Eta Min.

    /**
     *
     * @return
     */
    public Integer get_eta_min() {
        return (Integer) keyValue.get("ETA_MIN");
    }
    ///Set Eta Min.

    /**
     *
     * @param inp
     */
    public void set_eta_min(Integer inp) {
        keyValue.put("ETA_MIN", inp);
    }
    
    ///Get Eta Max.

    /**
     *
     * @return
     */
    public Integer get_eta_max() {
        return (Integer) keyValue.get("ETA_MAX");
    }
    ///Set Eta Max.

    /**
     *
     * @param inp
     */
    public void set_eta_max(Integer inp) {
        keyValue.put("ETA_MAX", inp);
    }
    
    ///Get Phi Min.

    /**
     *
     * @return
     */
    public Integer get_phi_min() {
        return (Integer) keyValue.get("PHI_MIN");
    }
    ///Set Phi Min.

    /**
     *
     * @param inp
     */
    public void set_phi_min(Integer inp) {
        keyValue.put("PHI_MIN", inp);
    }
    
    ///Get Phi Max.

    /**
     *
     * @return
     */
    public Integer get_phi_max() {
        return (Integer) keyValue.get("PHI_MAX");
    }
    ///Set Phi Max.

    /**
     *
     * @param inp
     */
    public void set_phi_max(Integer inp) {
        keyValue.put("PHI_MAX", inp);
    }

    ///Get EM Isolation.

    /**
     *
     * @return
     */
    public String get_em_isolation() {
        return (String) keyValue.get("EM_ISOLATION");
    }
    ///Set the EM Isolation.

    /**
     *
     * @param inp
     */
    public void set_em_isolation(String inp) {
        keyValue.put("EM_ISOLATION", inp);
    }

    ///Get Hadronic Isolation.

    /**
     *
     * @return
     */
    public String get_had_isolation() {
        return (String) keyValue.get("HAD_ISOLATION");
    }
    ///Set the Hadronic Isolation.

    /**
     *
     * @param inp
     */
    public void set_had_isolation(String inp) {
        keyValue.put("HAD_ISOLATION", inp);
    }

    ///Get Hadronic Veto.

    /**
     *
     * @return
     */
    public String get_had_veto() {
        return (String) keyValue.get("HAD_VETO");
    }
    ///Set the Hadronic Veto.

    /**
     *
     * @param inp
     */
    public void set_had_veto(String inp) {
        keyValue.put("HAD_VETO", inp);
    }

    ///Get the window size (4,6,8 for jets).

    /**
     *
     * @return
     */
    public Integer get_window() {
        return (Integer) keyValue.get("WINDOW");
    }
    ///Set the window size for jets.

    /**
     *
     * @param inp
     */
    public void set_window(Integer inp) {
        keyValue.put("WINDOW", inp);
    }

    ///Get the priority (High or Low).

    /**
     *
     * @return
     */
    public Integer get_priority() {
        return (Integer) keyValue.get("PRIORITY");
    }
    ///Set the prinority.

    /**
     *
     * @param inp
     */
    public void set_priority(Integer inp) {
        keyValue.put("PRIORITY", inp);
    }

    ///Save L1ThresholdValue to DB.
    /**
     * Save this ThresholdValue.  Since a threshold value has no children,
     * we can do this in a simple way.
     * 
     * @return The ID that this ThresholdValue was saved at.
     * @throws java.sql.SQLException
     */
    @Override
    public int save() throws SQLException {
        ConnectionManager mgr = ConnectionManager.getInstance();
        ArrayList<Integer> ids = mgr.get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");
        if (ids.isEmpty()) {
            logger.log(Level.FINE, "Saving L1 Threshold {0}", get_name());
            set_id(mgr.save(getTableName(), tablePrefix, get_id(), keyValue));
        } else {
            set_id(ids.get(0));
        }
        return get_id();
    }
    
        /** Compare to thresholds to see if they're equal
     * @param other
     * @return .*/
    @Override
    public boolean equals(Object other) {    
        //System.out.println("Equals of L1TValue");
        if (other instanceof L1ThresholdValue) {
            L1ThresholdValue others = (L1ThresholdValue) other;
            //System.out.println("Names: "+get_name() +" "+others.get_name());
            if (get_name().equals(others.get_name())) {
                boolean same = true;
                //System.out.println("Names: "+get_name() +" "+others.get_name()+" same " +same);
                if(!get_type().equals(others.get_type())) return false;
                //System.out.println("Type: "+get_type() +" "+others.get_type() +" same " +same);
                if(!get_em_isolation().equals(others.get_em_isolation())) return false;
                //System.out.println("EM Iso: "+get_em_isolation() +" "+others.get_em_isolation() +" same " +same);
                if(!get_eta_max().equals(others.get_eta_max())) return false;
                //System.out.println("Eta Max: "+get_eta_max() +" "+others.get_eta_max() +" same " +same);
                if(!get_eta_min().equals(others.get_eta_min())) return false;
                //System.out.println("Eta Min: "+get_eta_min() +" "+others.get_eta_min() +" same " +same);
                if(!get_had_isolation().equals(others.get_had_isolation())) return false;
                //System.out.println("Had Iso: "+get_had_isolation() +" "+others.get_had_isolation() +" same " +same);
                if(!get_had_veto().equals(others.get_had_veto())) return false;
                //System.out.println("Had Veto: "+get_had_veto() +" "+others.get_had_veto() +" same " +same);
                if(!get_phi_max().equals(others.get_phi_max())) return false;
                //System.out.println("Phi Max: "+get_phi_max() +" "+others.get_phi_max() +" same " +same);
                if(!get_phi_min().equals(others.get_phi_min())) return false;
                //System.out.println("Phi Min: "+get_phi_min() +" "+others.get_phi_min() +" same " +same);
                if(!get_priority().equals(others.get_priority())) return false;
                //System.out.println("Priority: "+get_priority() +" "+others.get_priority() +" same " +same);
                if(!get_pt_cut().equals(others.get_pt_cut())) return false;
                //System.out.println("Pt Cut: "+get_pt_cut() +" "+others.get_pt_cut() +" same " +same);
                if(!get_window().equals(others.get_window())) return false;
                //System.out.println("Window: "+get_window() +" "+others.get_window() +" same " +same);
                return same;
            }
        }
        return false;
    }
    

    ///A string representation of this record.
    /**
     * So the tree view knows what information to display in the tree
     * 
     * @return A string containing the id, name and version of the record
     */
    @Override
    public String toString() {
        if (get_id() == -1) {
            return "L1 Threshold Value";
        }
        return "THRESHOLD VALUE: " + get_name() + " (DBid=" + get_id() + "/V."+ get_version() + ")";
    }

    ///Copy the object.
    /**
     * Make a duplicate of this Threshold Value, but with an ID of -1.
     * 
     * @return A copy of this object.
     */
    @Override
    public Object clone() {
        L1ThresholdValue copy = new L1ThresholdValue();
        copy.set_type(get_type());
        copy.set_pt_cut(get_pt_cut());
        copy.set_eta_min(get_eta_min());
        copy.set_eta_max(get_eta_max());
        copy.set_phi_min(get_phi_min());
        copy.set_phi_max(get_phi_max());
        copy.set_em_isolation(get_em_isolation());
        copy.set_had_isolation(get_had_isolation());
        copy.set_had_veto(get_had_veto());
        copy.set_priority(get_priority());
        copy.set_window(get_window());
        copy.set_name(get_name());
        copy.set_version(get_version());
        return copy;
    }        
    
    ///L1Threshold needs to know about the contents of this record.
    /**
     * As a special case the L1Threshold needs to know about the contents
     * of its L1ThresholdValue so that it can display the correct information
     * on the overview panel.
     * 
     * @return Map used to hold information about the DB record.
     */
    public TriggerMap<String, Object> getKeyValue() {
        return keyValue;
    }

    @Override
    public String getTableName() {
         return "L1_TRIGGER_THRESHOLD_VALUE";
    }
    
}