package triggerdb.Entities.L1;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.TreeMap;

import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;

///Bunch Number, belongs to BunchGroup.
/**
 * This table holds the Bunch Number for a single bunch, which is part of a
 * BunchGroupSet.
 * 
 * @author Simon Head
 */
public final class L1Bunch extends AbstractTable {
    
    /**
     *
     */
    public L1Bunch() {
        super(-1, "L1BG2B_");
        DefineKeyValuePairs();
    }
    
    /**
     * Normal constructor.  Specify -1 for the ID and mode equal to 
     * AbstractTable.NONE to create a new record.  Or specify the ID and 
     * AbstractTable.MAX to read from the database.
     * 
     * @param input_id Input ID
     * @throws java.sql.SQLException
     */
    public L1Bunch(int input_id) throws SQLException {
        super(input_id, "L1BG2B_");
        DefineKeyValuePairs();

        if (input_id > 0) {
            forceLoad();
        }
    }

    private void DefineKeyValuePairs() {
        keyValue.putFirst("BUNCH_NUMBER", 1, "Number");
        keyValue.putFirst("BUNCH_GROUP_ID", 1, "Bunch Group ID");
        keyValue.remove("NAME");
        keyValue.remove("VERSION");
    }

    /**
     * Get the bunch number.
     * 
     * @return The bunch number.
     */
    public Integer get_bunch_number() {
        return (Integer) keyValue.get("BUNCH_NUMBER");
    }

    /**
     * Get the ID of the bunch group that this bunch is part of.
     * 
     * @return The Bunch Group ID.
     */
    public Integer get_bunch_group_id() {
        return (Integer) keyValue.get("BUNCH_GROUP_ID");
    }

    /**
     * Set the parent Bunch Group ID that this Bunch belongs to.
     * 
     * @param inp Bunch Group ID.
     */
    public void set_bunch_group_id(int inp) {
        keyValue.put("BUNCH_GROUP_ID", inp);
    }

    /**
     * Set the Bunch Number for this bunch.
     * 
     * @param inp The new Bunch Number.
     */
    public void set_bunch_number(int inp) {
        keyValue.put("BUNCH_NUMBER", inp);
    }

    /**
     * The save function is simple, since the Bunch Group save method will
     * already have checked if this exists or not. Therefore we can just write
     * the record to the database if it's called.
     * 
     * @return ID of the record in the database.
     * @throws java.sql.SQLException Stop if it goes wrong.
     */
    @Override
    public int save() throws SQLException {
        set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix, get_id(), keyValue));
        return get_id();
    }

    /**
     *
     * @return
     */
    public TreeMap<String,Object> getKey(){
        return keyValue;
    }
   
    ///String representation of the bunch.
    /**
     * So the tree view knows what information to display in the tree
     *
     * @return A string containing the id, name and version of the record
     */
    @Override
    public String toString() {
        if (get_id() == -1) {
            return "L1 Bunch";
        }
        return "L1 BUNCH: id=" + get_id() + ", bunch_num=" + get_bunch_number();
    }

    ///Function to create a copy of this object.
    /**
     * Replace with copy constructor. At the moment makes a copy.
     * 
     * @return Copy of this object.
     */
    @Override
    public Object clone() {
        L1Bunch copy = new L1Bunch();
        copy.set_name(get_name());
        copy.set_version(get_version());
        copy.set_bunch_number(get_bunch_number());
        copy.set_bunch_group_id(get_bunch_group_id());
        copy.set_id(-1);

        return copy;
    }

    ///Customised minimum names to show in search results.
    /**
     * Minimum data to show as the results of a search on the main screen.
     * 
     * @return Vector of only a few items.
     */
    @Override
    public ArrayList<String> get_min_names() {
        ArrayList<String> info = new ArrayList<>();
        info.add("ID");
        info.add("Bunch Group ID");
        info.add("Bunch Number");
        return info;
    }

    ///Minimum data to show in search results.
    /**
     * Minimum data to show as the results of a search on the main screen.
     * 
     * @return Vector of only a few items.
     */
    @Override
    public ArrayList<Object> get_min_info() {
        ArrayList<Object> info = new ArrayList<>();
        info.add(get_id());
        info.add(get_bunch_group_id());
        info.add(get_bunch_number());
        return info;
    }

    @Override
    public String getTableName() {
         return "L1_BG_TO_B";
   }
}