package triggerdb.Entities.L1;

import java.sql.SQLException;
import java.util.ArrayList;

import triggerdb.Entities.AbstractTable;
import triggerdb.Connections.ConnectionManager;

/**
 *
 * @author William
 */
public final class L1CaloIsoParam extends AbstractTable {

    /** Message Log */
    public L1CaloIsoParam() {
        super(-1, "L1CIP_");

        keyValue.putFirst("ISO_BIT", -1, "iso bit");
        keyValue.putFirst("OFFSET", -1, "offset");
        keyValue.putFirst("SLOPE", -1, "slope");
        keyValue.putFirst("MIN_CUT", -1, "min cut");
        keyValue.putFirst("UPPER_LIMIT", -1, "upper limit");
        keyValue.putFirst("ETA_MIN", -1, "Eta Min");
        keyValue.putFirst("ETA_MAX", -1, "Eta Max");
        keyValue.putFirst("PRIORITY", -1, "priority");

        keyValue.remove("NAME");
        keyValue.remove("VERSION");
        
    }

    
    /** Message Log
     * @param input_id
     * @throws java.sql.SQLException */
    public L1CaloIsoParam(int input_id) throws SQLException {
        super(input_id, "L1CIP_");

        keyValue.putFirst("ISO_BIT", -1, "iso bit");
        keyValue.putFirst("OFFSET", -1, "offset");
        keyValue.putFirst("SLOPE", -1, "slope");
        keyValue.putFirst("MIN_CUT", -1, "min cut");
        keyValue.putFirst("UPPER_LIMIT", -1, "upper limit");
        keyValue.putFirst("ETA_MIN", -1, "Eta Min");
        keyValue.putFirst("ETA_MAX", -1, "Eta Max");
        keyValue.putFirst("PRIORITY", -1, "priority");

        keyValue.remove("NAME");
        keyValue.remove("VERSION");
        
        if (input_id > 0) {
            forceLoad();
        }
    }

    /**
     *
     * @return
     */
    public Integer get_iso_bit() {
        return (Integer) keyValue.get("ISO_BIT");
    }

    /**
     *
     * @param inp
     */
    public void set_iso_bit(int inp) {
        keyValue.put("ISO_BIT", inp);
    }

    /**
     *
     * @return
     */
    public Integer get_offset() {
        return (Integer) keyValue.get("OFFSET");
    }

    /**
     *
     * @param inp
     */
    public void set_offset(int inp) {
        keyValue.put("OFFSET", inp);
    }

    /**
     *
     * @return
     */
    public Integer get_slope() {
        return (Integer) keyValue.get("SLOPE");
    }

    /**
     *
     * @param inp
     */
    public void set_slope(int inp) {
        keyValue.put("SLOPE", inp);
    }

    /**
     *
     * @return
     */
    public Integer get_min_cut() {
        return (Integer) keyValue.get("MIN_CUT");
    }

    /**
     *
     * @param inp
     */
    public void set_min_cut(int inp) {
        keyValue.put("MIN_CUT", inp);
    }

    /**
     *
     * @return
     */
    public Integer get_upper_limit() {
        return (Integer) keyValue.get("UPPER_LIMIT");
    }

    /**
     *
     * @param inp
     */
    public void set_upper_limit(int inp) {
        keyValue.put("UPPER_LIMIT", inp);
    }

    /**
     *
     * @return
     */
    public Integer get_eta_min() {
        return (Integer) keyValue.get("ETA_MIN");
    }

    /**
     *
     * @param inp
     */
    public void set_eta_min(int inp) {
        keyValue.put("ETA_MIN", inp);
    }
    
    /**
     *
     * @return
     */
    public Integer get_eta_max() {
        return (Integer) keyValue.get("ETA_MAX");
    }
    
    /**
     *
     * @param inp
     */
    public void set_eta_max(int inp) {
        keyValue.put("ETA_MAX", inp);
    }

    /**
     *
     * @return
     */
    public Integer get_priority() {
        return (Integer) keyValue.get("PRIORITY");
    }
    
    /**
     *
     * @param inp
     */
    public void set_priority(int inp) {
        keyValue.put("PRIORITY", inp);
    }

    @Override
    public int save() throws SQLException {
        ConnectionManager mgr = ConnectionManager.getInstance();

        ArrayList<Integer> ids = mgr.get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");

        if (ids.isEmpty()) {
            set_id(mgr.save(getTableName(), tablePrefix, get_id(), keyValue));
        } else {
            set_id(ids.get(0));
        }
                
        return get_id();
    }

    /**
     * So the tree view knows what information to display in the tree
     * 
     * @return A string containing the id, name and version of the record
     */
    @Override
    public String toString() {
        if (get_id() == -1) {
            return "L1 Calo IsoParam";
        }
        return "L1 CALO ISOPARAM: id=" + get_id() + ", iso bit=" + get_iso_bit();
    }

    @Override
    public Object clone() {
        L1CaloIsoParam copy = new L1CaloIsoParam();
        copy.set_id(-1);
        copy.set_iso_bit(get_iso_bit());
        copy.set_offset(get_offset());
        copy.set_slope(get_slope());
        copy.set_min_cut(get_min_cut());
        copy.set_upper_limit(get_upper_limit());
        copy.set_eta_min(get_eta_min());
        copy.set_eta_max(get_eta_max());
        copy.set_priority(get_priority());
        
        return copy;
    }

    @Override
    public String getTableName() {
        return "L1_CALO_ISOPARAM";
    }
    
    
    /**
     * Minimum names for the search results.
     * @return 
     */
    @Override
    public ArrayList<String> get_min_names() {
        ArrayList<String> info = new ArrayList<>();
        info.add("ID");
        info.add("Iso Bit");
        return info;
    }
    
    /**
     * Minimum information for the search results.
     * @return 
     */
    @Override
    public ArrayList<Object> get_min_info() {
        ArrayList<Object> info = new ArrayList<>();
        info.add(get_id());
        info.add(get_iso_bit());
        return info;
    }
}
