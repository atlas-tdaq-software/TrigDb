package triggerdb.Entities.L1;

import java.sql.SQLException;
import java.util.ArrayList;

import triggerdb.Entities.AbstractTable;
import triggerdb.Connections.ConnectionManager;

/**
 *
 * @author William
 */
public final class L1CaloIsolation extends AbstractTable {

    /** Message Log */
    public L1CaloIsolation() {
        super(-1, "L1CIS_");

        keyValue.putFirst("THR_TYPE", "thr type", "thr type");
        for (int i = 1; i < 6; ++i) {
            keyValue.putFirst("PAR" + i + "_ID", -1, "Par " + i + " id");
        }
        keyValue.remove("NAME");
        keyValue.remove("VERSION");
        
    }

    
    /** Message Log
     * @param input_id
     * @throws java.sql.SQLException */
    public L1CaloIsolation(int input_id) throws SQLException {
        super(input_id, "L1CIS_");

        keyValue.putFirst("THR_TYPE", "thr type", "thr type");
        
        for (int i = 1; i < 6; ++i) {
            keyValue.putFirst("PAR" + i + "_ID", -1, "Par " + i + " id");
        }

        keyValue.remove("NAME");
        keyValue.remove("VERSION");
        
        if (input_id > 0) {
            forceLoad();
        }
    }

    /**
     *
     * @return
     */
    public String get_thr_type() {
        return (String) keyValue.get("THR_TYPE");
    }

    /**
     *
     * @param inp
     */
    public void set_thr_type(String inp) {
        keyValue.put("THR_TYPE", inp);
    }
    
    /**
     *
     * @param i
     * @return
     */
    public Integer get_par(int i) {
        return (Integer) keyValue.get("PAR" + i + "_ID");
    }

    /**
     *
     * @param i
     * @param inp
     */
    public void set_par(int i, int inp) {
        keyValue.put("PAR" + i + "_ID", inp);
    }

    @Override
    public int save() throws SQLException {
        ConnectionManager mgr = ConnectionManager.getInstance();

        ArrayList<Integer> ids = mgr.get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");

        if (ids.isEmpty()) {
            set_id(mgr.save(getTableName(), tablePrefix, get_id(), keyValue));
        } else {
            set_id(ids.get(0));
        }

        return get_id();
    }

    /**
     * So the tree view knows what information to display in the tree
     * 
     * @return A string containing the id, name and version of the record
     */
    @Override
    public String toString() {
        if (get_id() == -1) {
            return "L1 Calo Isolation";
        }
        return "L1 CALO ISOLATION: id=" + get_id() + ", threshold=" + get_thr_type();
    }

    @Override
    public Object clone() {
        L1CaloIsolation copy = new L1CaloIsolation();
        copy.set_id(-1);
        copy.set_thr_type(get_thr_type());
        for (int i = 1; i < 6; ++i) {
            copy.set_par(i, get_par(i));
        }
        return copy;
    }

    @Override
    public String getTableName() {
        return "L1_CALO_ISOLATION";
    }
    
    /**
     * Minimum names for the search results.
     * @return 
     */
    @Override
    public ArrayList<String> get_min_names() {
        ArrayList<String> info = new ArrayList<>();
        info.add("ID");
        info.add("Threshold Type");
        return info;
    }
    
    /**
     * Minimum information for the search results.
     * @return 
     */
    @Override
    public ArrayList<Object> get_min_info() {
        ArrayList<Object> info = new ArrayList<>();
        info.add(get_id());
        info.add(get_thr_type());     
        return info;
    }
}
