package triggerdb.Entities.L1;

import java.util.ArrayList;
import java.util.List;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.AbstractMap;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.logging.Level;
import javax.swing.tree.DefaultMutableTreeNode;

import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;

/**
 * A group of Bunch Groups (up to the 16 allowed). Class to hold the 16 bunch
 * groups used within a single configuration.
 *
 * @author Simon Head
 */
public final class L1BunchGroupSet extends AbstractTable {

    /**
     * Max number of groups in set.
     */
    public static int MAXGROUPS = 16;
    /**
     * Holds the links that show in the tree
     */
    private ArrayList<DefaultMutableTreeNode> tree_data = new ArrayList<>();
    /**
     * Vector of bunch groups that belong to this set. They should be numbered 1
     * to 8.
     */
    //private ArrayList<L1BunchGroup> bunchGroups;
    private ArrayList<AbstractMap.SimpleEntry<L1BunchGroup, String>> bunchGroups;

    /**
     *
     */
    public L1BunchGroupSet() {
        super(-1, "L1BGS_");
        bunchGroups = new ArrayList<>();

        DefineKeyValuePairs();
    }

    /**
     * Usual constructor. Pass the input ID. -1 means don't load, otherwise this
     * row is read from the database.
     *
     * @param input_id The ID of the row to read.
     * @throws java.sql.SQLException
     */
    public L1BunchGroupSet(final int input_id) throws SQLException {
        super(input_id, "L1BGS_");

        DefineKeyValuePairs();

        if (input_id > 0) {
            forceLoad();
            setBunchGroups(getBunchGroupsFromDb());
        }
    }

    private void DefineKeyValuePairs() {
        keyValue.putFirst("COMMENT", "Comment", "Comment");
        keyValue.putFirst("PARTITION", 0, "Menu partition");
        keyValue.remove("USED");
    }

    ///Reload from the database.
    /**
     * Force the information to be read from the database again. This means
     * clearing the vector of children and the tree.
     *
     * @throws java.sql.SQLException
     */
    @Override
    public void forceLoad() throws SQLException {
        tree_data = new ArrayList<>();
        bunchGroups = null;

        ConnectionManager.getInstance().forceLoad(this);
    }

    /**
     *
     * @return
     */
    public ArrayList<AbstractMap.SimpleEntry<L1BunchGroup, String>> getBunchGroups() {
        if (bunchGroups == null) {
            bunchGroups = new ArrayList<>();
        }
        return bunchGroups;
    }

    /**
     *
     * @param list
     */
    public void setBunchGroups(ArrayList<AbstractMap.SimpleEntry<L1BunchGroup, String>> list) {
        bunchGroups = list;
    }

    /**
     *
     * @param i
     */
    public void set_partition(Integer i) {
        keyValue.put("PARTITION", i);
    }

    /**
     *
     * @return
     */
    public Integer get_partition() {
        return (Integer) keyValue.get("PARTITION");
    }

    /**
     * Load all the Bunch Groups that belong to this Bunch Group Set. This
     * recovers all the bunch groups with a single query and only if they've not
     * been read from the database already (and forceLoad has not been called).
     *
     * @return Vector of the Bunch Groups that belong to this Bunch Group Set.
     * @throws java.sql.SQLException
     */
    public ArrayList<AbstractMap.SimpleEntry<L1BunchGroup, String>> getBunchGroupsFromDb() throws SQLException {
        ArrayList<AbstractMap.SimpleEntry<L1BunchGroup, String>> list = new ArrayList<>();
        String query = (new L1BunchGroup()).getQueryString();

        query = query.replace("FROM ", ", L1BGS2BG_INTERNAL_NUMBER, L1BGS2BG_BUNCH_GROUP_NAME "
                + "FROM L1_BUNCH_GROUP_SET, L1_BGS_TO_BG, ");

        String and = " WHERE "
                + "L1BGS_ID=? "
                + "AND L1BGS2BG_BUNCH_GROUP_SET_ID=L1BGS_ID "
                + "AND L1BGS2BG_BUNCH_GROUP_ID=L1BG_ID "
                + "ORDER BY "
                + "L1BGS2BG_INTERNAL_NUMBER ASC";

        ConnectionManager mgr = ConnectionManager.getInstance();
        query = mgr.fix_schema_name(query + and);
        //logger.info(query+" ID="+get_id());
        PreparedStatement ps = mgr.getConnection().prepareStatement(query);
        ps.setInt(1, this.get_id());
        ResultSet rset = ps.executeQuery();

        while (rset.next()) {
            //Need to load the bunch group with the bunches, so the constructor need the id.
            L1BunchGroup bg = new L1BunchGroup(rset.getInt("L1BG_ID"));
            bg.loadFromRset(rset);
            bg.set_internal_number(rset.getInt("L1BGS2BG_INTERNAL_NUMBER"));
            String bgNameInSet = rset.getString("L1BGS2BG_BUNCH_GROUP_NAME");
            list.add(new AbstractMap.SimpleEntry<>(bg, bgNameInSet));
        }

        rset.close();
        ps.close();

        return list;
    }

    ///Save routine also has to check the bunch groups.
    /**
     * Save routine checks for the existence of this record with the correct
     * child relationships and counters in the link tables. If a match in the
     * database is found it returns its ID. Otherwise it writes a new Bunch
     * Group Set.
     *
     * @return ID of the Bunch Group Set
     * @throws java.sql.SQLException Stop on a problem.
     */
    @Override
    public int save() throws SQLException {

        final long startTime = System.currentTimeMillis();

        String name = this.get_name();
        logger.log(Level.INFO, "In L1 Bunch Group Set Save for: {0}", name);
        ConnectionManager mgr = ConnectionManager.getInstance();

        ArrayList<AbstractMap.SimpleEntry<L1BunchGroup, String>> bgs = getBunchGroups();

        if (!bgs.isEmpty()) {

            StringBuilder sb = new StringBuilder();

            int numEntries = bgs.size();
            for (int var = 0; var < numEntries; var++) {
                sb.append("?");
                if (var < (numEntries - 1)) {
                    sb.append(",");
                }
            }

            java.lang.String query = "select * from (SELECT L1BGS2BG_BUNCH_GROUP_SET_ID,count(*) FROM L1_BGS_TO_BG where L1BGS2BG_BUNCH_GROUP_ID||'.'||L1BGS2BG_INTERNAL_NUMBER||'.'||L1BGS2BG_BUNCH_GROUP_NAME IN (";
            query += sb.toString() + ") GROUP BY L1BGS2BG_BUNCH_GROUP_SET_ID having count(*) = ?) match ";
            query += "join (Select L1BGS_ID from L1_BUNCH_GROUP_SET where l1bgs_partition = ?) total on match.L1BGS2BG_BUNCH_GROUP_SET_ID = total.L1BGS_ID";

            query = mgr.fix_schema_name(query);

            //System.out.println(query);
            PreparedStatement ps = mgr.getConnection().prepareStatement(query);

            Iterator<AbstractMap.SimpleEntry<L1BunchGroup, String>> psIter = bgs.iterator();

            //System.out.println("entries: " + numEntries + " " + bgs.size());
            int psCounter = 1;
            while (psIter.hasNext()) {
                AbstractMap.SimpleEntry<L1BunchGroup, String> entry = psIter.next();
                ps.setString(psCounter, entry.getKey().get_id() + "." + (psCounter - 1) + "." + entry.getValue().trim());
                psCounter++;
            }

            ps.setInt(psCounter++, numEntries);
            ps.setInt(psCounter, get_partition());

            try (ResultSet result = ps.executeQuery()) {

                Boolean duplicate = result.next();

                //long endTime = System.currentTimeMillis();
                //System.out.println("Query Ready Total execution time: " + (endTime - startTime));
                if (!duplicate) {
                    //keyValue.put("NAME", name);//put back now
                    //keyValue.put("PARTITION", 0);

                    int save = mgr.save(getTableName(), tablePrefix, get_id(), keyValue);

                    set_id(save);

                    ArrayList<TreeMap<String, Object>> table = new ArrayList<>();
                    for (AbstractMap.SimpleEntry<L1BunchGroup, String> bg : getBunchGroups()) {
                        TreeMap<String, Object> link = new TreeMap<>();
                        link.put("BUNCH_GROUP_SET_ID", get_id());
                        link.put("BUNCH_GROUP_ID", bg.getKey().get_id());
                        link.put("INTERNAL_NUMBER", bg.getKey().get_internal_number());
                        link.put("BUNCH_GROUP_NAME", bg.getValue());
                        if (bg.getKey().get_partition() != null) {
                            link.put("PARTITION", bg.getKey().get_partition());
                        } else {
                            link.put("PARTITION", 0);
                        }
                        table.add(link);
                        //    mgr.save("L1_BGS_TO_BG", "L1BGS2BG_", -1, link);
                    }
                    TreeMap<Integer, Integer> results = ConnectionManager.getInstance().saveBatch("L1_BGS_TO_BG", "L1BGS2BG_", -1, table);
                    final long endTime = System.currentTimeMillis();

                    logger.log(Level.FINE, "Saved, Total execution time: {0}", (endTime - startTime));
                } else {
                    logger.log(Level.FINE, "Duplicates bunch group set: {0}", result.getInt("L1BGS2BG_BUNCH_GROUP_SET_ID"));
                    set_id(result.getInt("L1BGS2BG_BUNCH_GROUP_SET_ID"));
                }
                result.close();
                ps.close();
            }
        } else {
            logger.log(Level.FINE, "No bunch group sets to load");
        }

        return get_id();
    }

    /**
     * Add all the children to the tree.
     *
     * @param treeNode
     * @param counter
     * @throws java.sql.SQLException
     */
    @Override
    public void addToTree(DefaultMutableTreeNode treeNode, int counter) throws SQLException {
        --counter;

        if (counter >= 0) {
            if (tree_data.isEmpty()) {

                for (AbstractMap.SimpleEntry<L1BunchGroup, String> bg : getBunchGroups()) {
                    DefaultMutableTreeNode anotherLayer = new DefaultMutableTreeNode(bg.getKey());
                    treeNode.add(anotherLayer);
                    tree_data.add(anotherLayer);
                }
            } else {
                int i = 0;
                for (AbstractMap.SimpleEntry<L1BunchGroup, String> bg : getBunchGroups()) {
                    bg.getKey().addToTree(tree_data.get(i), counter);
                    ++i;
                }

            }
        }
    }

    /**
     * So the tree view knows what information to display in the tree
     *
     * @return A string containing the id, name and version of the record
     */
    @Override
    public String toString() {
        if (get_id() == -1) {
            return "L1 Bunch Group Set";
        }
        return "BUNCH GROUP SET: ID=" + get_id() + ", Name=" + get_name() + ", Version=" + get_version();
    }

    ///Set a comment about this BGS

    /**
     *
     * @param inp
     */
    public void set_comment(String inp) {
        keyValue.put("COMMENT", inp);
    }

    ///Get the comment about this configuration.

    /**
     *
     * @return
     */
    public String get_comment() {
        return (String) keyValue.get("COMMENT");
    }

    @Override
    public Object clone() {
        L1BunchGroupSet copy = new L1BunchGroupSet();
        copy.set_name(get_name());
        copy.set_version(get_version());
        copy.set_comment(get_comment());
        return copy;
    }

    @Override
    public String getTableName() {
        return "L1_BUNCH_GROUP_SET";
    }

    /**
     * Reads the list of all bunch group sets from DB.
     *
     * @return the list of all bunch group sets in DB.
     * @throws java.sql.SQLException
     */
    public static List<L1BunchGroupSet> getBunchGroupSets() throws SQLException {
        //        String query = "SELECT L1BGS_ID FROM L1_BUNCH_GROUP_SET ORDER BY L1BGS_ID DESC";
        List<L1BunchGroupSet> setList = new ArrayList<>();
        ConnectionManager mgr = ConnectionManager.getInstance();
        List<AbstractTable> sets = mgr.forceLoadVector(new L1BunchGroupSet(),
                "ORDER BY L1BGS_ID DESC", new ArrayList());
        for (AbstractTable set : sets) {
            setList.add((L1BunchGroupSet) set);
        }
        return setList;
    }

    /**
     *
     * @param smk
     * @param partition
     * @return
     * @throws SQLException
     */
    public static List<String> getActiveBunchGroupsInPartition(int smk, int partition) throws SQLException {
        List<String> groups = new ArrayList<>();
        ConnectionManager mgr = ConnectionManager.getInstance();

        String query = "select distinct L1TT_NAME "
                + "from( select smt_id,l1tm2ti_trigger_item_id from "
                + "(select smt_id,smt_l1_master_table_id from super_master_table) l1master "
                + "join "
                + "(select l1mt_id,l1mt_trigger_menu_id,l1tm2ti_trigger_item_id "
                + "from (select l1mt_id,l1mt_trigger_menu_id from l1_master_table) l1menu "
                + "join "
                + "(select l1tm2ti_trigger_menu_id,l1tm2ti_trigger_item_id from l1_tm_to_ti) l1items "
                + "on l1menu.l1mt_trigger_menu_id = l1items.l1tm2ti_trigger_menu_id) l1menuitems "
                + "on l1master.smt_l1_master_table_id = l1menuitems.l1mt_id) supermaster "
                + "join( "
                + "select l1ti_id,l1tt_name from (select l1ti_id,l1ti_partition from l1_trigger_item) items "
                + "join ("
                + "select L1TI2TT_TRIGGER_ITEM_ID,L1tt_name "
                + "from (select l1_ti_to_tt.L1TI2TT_TRIGGER_THRESHOLD_ID,l1_ti_to_tt.L1TI2TT_TRIGGER_ITEM_ID from l1_ti_to_tt) lhs "
                + "join (select l1_trigger_threshold.L1TT_ID,l1_trigger_threshold.L1TT_TYPE,l1_trigger_threshold.L1TT_NAME from l1_trigger_threshold where l1tt_type='BGRP') rhs "
                + "on lhs.L1TI2TT_TRIGGER_THRESHOLD_ID = rhs.l1tt_id) thresholds "
                + "on items.l1ti_id = thresholds.L1TI2TT_TRIGGER_ITEM_ID "
                + "where l1ti_partition=?) bunchgroups "
                + "on supermaster.l1tm2ti_trigger_item_id = bunchgroups.l1ti_id where smt_id=? order by l1tt_name asc";
        query = query.toUpperCase();
        query = mgr.fix_schema_name(query);

        PreparedStatement ps = mgr.getConnection().prepareStatement(query);
        ps.setInt(1, partition);
        ps.setInt(2, smk);
        try (ResultSet result = ps.executeQuery()) {
            while (result.next()) {
                groups.add(result.getString("L1TT_NAME"));
            }
            result.close();
        }
        ps.close();
        System.out.println("SMK " + smk + " has " + groups.size() + " active bunch groups in partition " + partition);
        return groups;
    }

    /**
     *
     * @param partition
     * @return
     * @throws SQLException
     */
    public static List<L1BunchGroupSet> getBunchGroupSetsPartition(int partition) throws SQLException {
        //        String query = "SELECT L1BGS_ID FROM L1_BUNCH_GROUP_SET ORDER BY L1BGS_ID DESC";
        String queryElement;
        if (partition == 0) {
            queryElement = "ORDER BY L1BGS_ID DESC";
        } else {
            queryElement = "WHERE L1BGS_PARTITION=" + partition + "ORDER BY L1BGS_ID DESC";
        }
        List<L1BunchGroupSet> setList = new ArrayList<>();
        ConnectionManager mgr = ConnectionManager.getInstance();
        List<AbstractTable> sets = mgr.forceLoadVector(new L1BunchGroupSet(),
                queryElement, new ArrayList());
        for (AbstractTable set : sets) {
            setList.add((L1BunchGroupSet) set);
        }
        return setList;
    }
}
