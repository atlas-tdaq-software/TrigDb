package triggerdb.Entities.L1;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.tree.DefaultMutableTreeNode;

import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;

///Available thresholds 
/**
 * Class to hold the available threshold sets for the L1 muon trigger
 * 
 * @author Simon Head
 */
public final class L1MuonThresholdSet extends AbstractTable {

    ///Holds the links that show in the tree
    private List<DefaultMutableTreeNode> tree_data = new ArrayList<>();
    
    ///Constructor.
    /**
     * Usual constructor but don't load.
     */
    public L1MuonThresholdSet() {
        super("L1MTS_");
        setKeys();
    }

    /**
     * Usual constructor.  Pass the input ID. -1 means don't load, otherwise
     * this row is read from the database.
     * 
     * @param input_id The ID of the row to read.
     * @throws java.sql.SQLException
     */
    public L1MuonThresholdSet(int input_id) throws SQLException {
        super(input_id, "L1MTS_");
        setKeys();
        if (input_id > 0) {
            forceLoad();
        }
    }

    private void setKeys() {
   
        keyValue.putFirst("NAME", "L1 Muon Name", "Name");
        keyValue.putFirst("RPC_AVAILABLE", 0, "RPC Available");
        keyValue.putFirst("RPC_AVAILABLE_ONLINE", 0, "RPC Available Online");
        
        keyValue.putFirst("RPC_SET_EXT_ID", 0, "RPC Set Ext ID");
        keyValue.putFirst("RPC_SET_NAME", "RPC set name", "RPC set name");
        
        keyValue.putFirst("RPC_PT1_EXT_ID", 0, "RPC PT1 Ext ID");
        keyValue.putFirst("RPC_PT2_EXT_ID", 0, "RPC PT2 Ext ID");
        keyValue.putFirst("RPC_PT3_EXT_ID", 0, "RPC PT3 Ext ID");
        keyValue.putFirst("RPC_PT4_EXT_ID", 0, "RPC PT4 Ext ID");
        keyValue.putFirst("RPC_PT5_EXT_ID", 0, "RPC PT5 Ext ID");
        keyValue.putFirst("RPC_PT6_EXT_ID", 0, "RPC PT6 Ext ID");
                
        keyValue.putFirst("TGC_AVAILABLE", 0, "TGC Available");
        keyValue.putFirst("TGC_AVAILABLE_ONLINE", 0, "TGC Available Online");
        
        keyValue.putFirst("TGC_SET_EXT_ID", 0, "TGC Set Ext ID");
        keyValue.putFirst("TGC_SET_NAME", "TGC set name", "TGC set name");
    }
    
    @Override
    public String get_name() {
        return (String) keyValue.get("NAME");
    }
   
    @Override
    public void set_name(String inp) {
        keyValue.put("NAME", inp);
    }
    
    ///Get rpc availability

    /**
     *
     * @return
     */
    public Integer get_rpc_available() {
        return (Integer) keyValue.get("RPC_AVAILABLE");
    }
    ///Set rpc availability

    /**
     *
     * @param inp
     */
    public void set_rpc_available(Integer inp) {
        keyValue.put("RPC_AVAILABLE", inp);
    }
    
    ///Get rpc availability online

    /**
     *
     * @return
     */
    public Integer get_rpc_available_online() {
        return (Integer) keyValue.get("RPC_AVAILABLE_ONLINE");
    }
    ///Set rpc availability online

    /**
     *
     * @param inp
     */
    public void set_rpc_available_online(Integer inp) {
        keyValue.put("RPC_AVAILABLE_ONLINE", inp);
    }

    ///Get rpc set ext id

    /**
     *
     * @return
     */
    public Integer get_rpc_set_ext_id() {
        return (Integer) keyValue.get("RPC_SET_EXT_ID");
    }
    ///Set rpc set ext id

    /**
     *
     * @param inp
     */
    public void set_rpc_set_ext_id(int inp) {
        keyValue.put("RPC_SET_EXT_ID", inp);
    }

    ///Get rpc set name

    /**
     *
     * @return
     */
    public String get_rpc_set_name() {
        return (String) keyValue.get("RPC_SET_NAME");
    }
    ///Set rpc set name

    /**
     *
     * @param inp
     */
    public void set_rpc_set_name(String inp) {
        keyValue.put("RPC_SET_NAME", inp);
    }

    
    ///Get rpc set pt1

    /**
     *
     * @return
     */
    public Integer get_rpc_pt1_ext_id() {
        return (Integer) keyValue.get("RPC_PT1_EXT_ID");
    }
    ///Set rpc set pt1

    /**
     *
     * @param inp
     */
    public void set_rpc_pt1_ext_id(int inp) {
        keyValue.put("RPC_PT1_EXT_ID", inp);
    }
    
    ///Get rpc set pt2

    /**
     *
     * @return
     */
    public Integer get_rpc_pt2_ext_id() {
        return (Integer) keyValue.get("RPC_PT2_EXT_ID");
    }
    ///Set rpc set pt2

    /**
     *
     * @param inp
     */
    public void set_rpc_pt2_ext_id(int inp) {
        keyValue.put("RPC_PT2_EXT_ID", inp);
    }

    ///Get rpc set pt3

    /**
     *
     * @return
     */
    public Integer get_rpc_pt3_ext_id() {
        return (Integer) keyValue.get("RPC_PT3_EXT_ID");
    }
    ///Set rpc set pt3

    /**
     *
     * @param inp
     */
    public void set_rpc_pt3_ext_id(int inp) {
        keyValue.put("RPC_PT3_EXT_ID", inp);
    }

    ///Get rpc set pt4

    /**
     *
     * @return
     */
    public Integer get_rpc_pt4_ext_id() {
        return (Integer) keyValue.get("RPC_PT4_EXT_ID");
    }
    ///Set rpc set pt4

    /**
     *
     * @param inp
     */
    public void set_rpc_pt4_ext_id(int inp) {
        keyValue.put("RPC_PT4_EXT_ID", inp);
    }

    ///Get rpc set pt5

    /**
     *
     * @return
     */
    public Integer get_rpc_pt5_ext_id() {
        return (Integer) keyValue.get("RPC_PT5_EXT_ID");
    }
    ///Set rpc set pt5

    /**
     *
     * @param inp
     */
    public void set_rpc_pt5_ext_id(int inp) {
        keyValue.put("RPC_PT5_EXT_ID", inp);
    }

    ///Get rpc set pt6

    /**
     *
     * @return
     */
    public Integer get_rpc_pt6_ext_id() {
        return (Integer) keyValue.get("RPC_PT6_EXT_ID");
    }
    ///Set rpc set pt6

    /**
     *
     * @param inp
     */
    public void set_rpc_pt6_ext_id(int inp) {
        keyValue.put("RPC_PT6_EXT_ID", inp);
    }

    ///Get tgc availability

    /**
     *
     * @return
     */
    public Integer get_tgc_available() {
        return (Integer) keyValue.get("TGC_AVAILABLE");
    }
    ///Set tgc availability

    /**
     *
     * @param inp
     */
    public void set_tgc_available(Integer inp) {
        keyValue.put("TGC_AVAILABLE", inp);
    }
    
    ///Get tgc availability online

    /**
     *
     * @return
     */
    public Integer get_tgc_available_online() {
        return (Integer) keyValue.get("TGC_AVAILABLE_ONLINE");
    }
    ///Set tgc availability online

    /**
     *
     * @param inp
     */
    public void set_tgc_available_online(Integer inp) {
        keyValue.put("TGC_AVAILABLE_ONLINE", inp);
    }

    ///Get tgc set ext id

    /**
     *
     * @return
     */
    public Integer get_tgc_set_ext_id() {
        return (Integer) keyValue.get("TGC_SET_EXT_ID");
    }
    ///Set tgc set ext id

    /**
     *
     * @param inp
     */
    public void set_tgc_set_ext_id(int inp) {
        keyValue.put("TGC_SET_EXT_ID", inp);
    }

    ///Get rpc set name

    /**
     *
     * @return
     */
    public String get_tgc_set_name() {
        return (String) keyValue.get("TGC_SET_NAME");
    }
    ///Set rpc set name

    /**
     *
     * @param inp
     */
    public void set_tgc_set_name(String inp) {
        keyValue.put("TGC_SET_NAME", inp);
    }

    ///Reload from the database.
    /**
     * Force the information to be read from the database again.  This means
     * clearing the vector of children and the tree.
     * @throws java.sql.SQLException
     */
    @Override
    public void forceLoad() throws SQLException {
        tree_data = new ArrayList<>();
        ConnectionManager.getInstance().forceLoad(this);
    }

    
    ///Save routine also has to check the bunch groups.
    /**
     * Save routine checks for the existence of this record with the correct
     * child relationships and counters in the link tables.  If a match in the
     * database is found it returns its ID
     * 
     * @return ID 
     * @throws java.sql.SQLException Stop on a problem.
     */
    @Override
    public int save() throws SQLException {
        ConnectionManager mgr = ConnectionManager.getInstance();
        ArrayList<Integer> ids = mgr.get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");
        if (ids.size() <= 0) {
            set_id(mgr.save(getTableName(), tablePrefix, get_id(), keyValue));
        } else {
            set_id(ids.get(0));
            //forceLoad();
        }
        return get_id();
    }

    ///String representation of the object.
    /**
     * So the tree view knows what information to display in the tree
     * 
     * @return A string containing the id, name and version of the record
     */
    @Override
    public String toString() {
        if (get_id() == -1) {
            return "L1 Muon Threshold Set";
        }
        return "L1 MUON THRESHOLD SET: ID=" + get_id() + ", Name=" + get_name() + ", Version=" + get_version();
    }

    @Override
    public Object clone() {
        L1MuonThresholdSet copy = new L1MuonThresholdSet();
        copy.set_name(get_name());
        copy.set_version(get_version());
        copy.set_rpc_available(get_rpc_available());
        copy.set_rpc_available_online(get_rpc_available_online());
        copy.set_rpc_pt1_ext_id(get_rpc_pt1_ext_id());
        copy.set_rpc_pt2_ext_id(get_rpc_pt2_ext_id());
        copy.set_rpc_pt3_ext_id(get_rpc_pt3_ext_id());
        copy.set_rpc_pt4_ext_id(get_rpc_pt4_ext_id());
        copy.set_rpc_pt5_ext_id(get_rpc_pt5_ext_id());
        copy.set_rpc_pt6_ext_id(get_rpc_pt6_ext_id());
        copy.set_rpc_set_ext_id(get_rpc_set_ext_id());
        copy.set_tgc_available(get_tgc_available());
        copy.set_tgc_available_online(get_tgc_available_online());
        copy.set_tgc_set_ext_id(get_tgc_set_ext_id());
        return copy;
    }

    @Override
    public String getTableName() {
         return "L1_MUON_THRESHOLD_SET";
   }

}