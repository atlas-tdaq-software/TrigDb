package triggerdb.Entities.L1;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;

///Muon CTPI info.
/**
 * Class to hold the Muon CTPi Info.
 * 
 * @author Simon Head
 */
public final class L1MuctpiInfo extends AbstractTable {

    /**
     * Standard constructor. If an ID of -1 is supplied we create a new, empty
     * record and don't attempt to load anything from the database.  If the ID
     * supplied is greater than 0 we load this record from the database.
     * 
     */
    public L1MuctpiInfo() {
        super("L1MI_");
        setKeys();
    }

    /**
     * Standard constructor. If an ID of -1 is supplied we create a new, empty
     * record and don't attempt to load anything from the database.  If the ID
     * supplied is greater than 0 we load this record from the database.
     * 
     * @param input_id The ID to load or -1.
     * @throws java.sql.SQLException
     */
    public L1MuctpiInfo(final int input_id) throws SQLException {
        super(input_id, "L1MI_");
        setKeys();
        if (input_id > 0) {
            forceLoad();
        }
    }

    private void setKeys() {
        keyValue.putFirst("MAX_CAND", 1, "Max Cand");
        keyValue.putFirst("HIGH_PT", 1, "High Pt");
        keyValue.putFirst("LOW_PT", 1, "Low Pt");
    }
    
    /** Get the value of Low PT
     * @return .*/
    public Integer get_low_pt() {
        return (Integer) keyValue.get("LOW_PT");
    }

    /** Get the value of High PT
     * @return .*/
    public Integer get_high_pt() {
        return (Integer) keyValue.get("HIGH_PT");
    }

    /** Get the value of Max Cand
     * @return .*/
    public Integer get_max_cand() {
        return (Integer) keyValue.get("MAX_CAND");
    }

    /** Set the value of Low PT
     * @param inp.*/
    public void set_low_pt(final int inp) {
        keyValue.put("LOW_PT", inp);
    }

    /** Set the value of High PT
     * @param inp.*/
    public void set_high_pt(final int inp) {
        keyValue.put("HIGH_PT", inp);
    }

    /** Get the value of Max Cand
     * @param inp.*/
    public void set_max_cand(final int inp) {
        keyValue.put("MAX_CAND", inp);
    }

    // L1MuctpiInfo has no children, so we can do a simple save.
    /**
     * Save the class to the database.  Here we also make sure that the in-memory object
     * gets the id from the database.  If the id was -1 before the save, the id of the 
     * class after the save is the new position in the database.
     * @return the id of the saved object.
     * @throws java.sql.SQLException
     */
    @Override
    public int save() throws SQLException {
        ConnectionManager mgr = ConnectionManager.getInstance();
        ArrayList<Integer> ids = mgr.get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");
        Collections.sort(ids);

        if (ids.isEmpty()) {
            this.set_id(mgr.save(getTableName(), tablePrefix, get_id(), keyValue));
        } else {
            this.set_id(ids.get(0));
            //this.forceLoad();
        }
        return this.get_id();
    }

    /**
     * This is the string that will appear in the tree view.
     * @return 
     */
    @Override
    public String toString() {
        if (get_id() == -1) {
            return "L1 Muctpi Info";
        }
        return "L1 MUCTPI INFO: ID=" + get_id() + ", Name=" + get_name() + ", Version=" + get_version();
    }
    /**
     * The clone method will create an exact copy, except that the ID
     * will be set to -1 and the used flag set to false
     * @return 
     */
    @Override
    public Object clone() {
        L1MuctpiInfo copy = new L1MuctpiInfo();
        copy.set_name(get_name());
        copy.set_version(get_version());
        copy.set_low_pt(get_low_pt());
        copy.set_high_pt(get_high_pt());
        copy.set_max_cand(get_max_cand());
        return copy;
    }

    @Override
    public String getTableName() {
         return "L1_MUCTPI_INFO";
   }
}