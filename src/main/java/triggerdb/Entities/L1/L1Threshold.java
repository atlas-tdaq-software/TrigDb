package triggerdb.Entities.L1;

import java.awt.Color;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.TreeMap;
import javax.swing.tree.DefaultMutableTreeNode;

import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;


/**
 * Holds the L1 Trigger Threshold in memory whilst we play with it. Also
 * defines the allowed Trigger Types.
 * 
 * @author Simon Head
 * @author Johannes Noller
 * @author Alex Martyniuk
 */
public final class L1Threshold extends AbstractTable //implements Comparable<L1Threshold> 
{
    /** String Trigger Type for Muons.*/
    public final static String MUON_TYPE = "MUON";
    /** String Trigger Type for Electrons / Photons. */
    public final static String EM_TYPE = "EM";
    /** String Trigger Type for Jets.*/
    public final static String JET_TYPE = "JET";
    /** String Trigger Type for Taus.*/
    public final static String TAU_TYPE = "TAU";
    /** String Trigger Type for Missing Energy.*/
    public final static String XE_TYPE = "XE";
    /** String Trigger Type for Missing Energy Significance.*/
    public final static String XS_TYPE = "XS";
    /** String Trigger Type for Jet Energy. */
    public final static String JE_TYPE = "JE";
    /** String Trigger Type for Total Energy.*/
    public final static String TE_TYPE = "TE";
    ///String Trigger Type for Forward Jets.

    /**
     *
     */
    public final static String JF_TYPE = "JF";
    ///String Trigger Type for Backwards Jets.

    /**
     *
     */
    public final static String JB_TYPE = "JB";
    ///String Trigger Type for the Prescaled Clcok.

    /**
     *
     */
    public final static String PSC_TYPE = "PCLK";
    ///String Trigger Type for Bunch Groups.

    /**
     *
     */
    public final static String BG_TYPE = "BGRP";
    ///String Trigger Type for the Random triggers.

    /**
     *
     */
    public final static String RND_TYPE = "RNDM";
    ///String Trigger Type for NIM triggers.

    /**
     *
     */
    public final static String NIM_TYPE = "NIM";
    ///String Trigger Type for CALREQ triggers.

    /**
     *
     */
    public final static String CALREQ_TYPE = "CALREQ";
    ///String Trigger Type for BCM triggers.

    /**
     *
     */
    public final static String BCM_TYPE = "BCM";

    /**
     *
     */
    public final static String BCMCMB_TYPE = "BCMCMB";
    ///String Trigger Type for LUCID triggers.

    /**
     *
     */
    public final static String LUCID_TYPE = "LUCID";
    ///String Trigger Type for ZDC triggers.

    /**
     *
     */
    public final static String ZDC_TYPE = "ZDC";
    ///String Trigger Type for ROMAN POT triggers.

    /**
     *
     */
    public final static String RP_TYPE = "RP";
    ///String Trigger Type for MBTS Single triggers.

    /**
     *
     */
    public final static String MBTS_SINGLE_TYPE = "MBTSSI";
    ///String Trigger Type for MBTS Multi triggers.

    /**
     *
     */
    public final static String MBTS_MULTI_TYPE = "MBTS";
    ///String Trigger Type for ZB triggers.

    /**
     *
     */
    public final static String ZB_TYPE = "ZB";
    //String Trigger Type for Topo seeded Triggers

    /**
     *
     */
    public final static String TOPO_TYPE = "TOPO";
    //String Trigger Type for Topo seeded Triggers

    /**
     *
     */
    public final static String BPTX_TYPE = "BPTX";
    ///String Trigger Type for Undefined.

    /**
     *
     */
    public final static String EMPTY_TYPE = "NULL";

    /**
     *
     */
    public final static String ALFA_TYPE = "ALFA";
    ///Added?
    private boolean added = false;
    ///L1Master link.

    /**
     *
     */
    public L1Master l1master;
    ///Position of threshold in an item, for link table.
    private Integer position = -1;
    ///Multiplicity of this threshold in an item.
    private Integer multiplicity = 1;
    /** threshold values in this threshold. */
    private ArrayList<L1ThresholdValue> tvs;
    /** Has this been changed? */
    public boolean changed = false;
    /** Map of threshold type to the colour it should be painted.*/
    public static final HashMap<String, Color> COLOURS = new HashMap<>();           
    static{
        COLOURS.put(MUON_TYPE, new Color(119, 195, 113));
        COLOURS.put(EM_TYPE, new Color(131, 148, 202));
        COLOURS.put(JET_TYPE, new Color(239, 71, 74));
        COLOURS.put(TAU_TYPE, new Color(254, 241, 97));
        COLOURS.put(XE_TYPE, new Color(226, 127, 179));
        COLOURS.put(XS_TYPE, Color.GREEN);
        COLOURS.put(TE_TYPE, new Color(0, 168, 226));
        COLOURS.put(JE_TYPE, new Color(255, 215, 143));
        COLOURS.put(JF_TYPE, new Color(244, 128, 126));
        COLOURS.put(JB_TYPE, new Color(244, 128, 126));

        COLOURS.put(NIM_TYPE, new Color(185, 226, 255));
        COLOURS.put(BCM_TYPE, new Color(255, 250, 250));
        COLOURS.put(BCMCMB_TYPE, new Color(255, 250, 250));
        COLOURS.put(LUCID_TYPE, new Color(238, 233, 233));

        COLOURS.put(ZDC_TYPE, new Color(238, 239, 222));
        COLOURS.put(RP_TYPE, new Color(205, 197, 191));
        COLOURS.put(RND_TYPE, new Color(205, 240, 230));
        COLOURS.put(BG_TYPE, new Color(205, 240, 230));
        COLOURS.put(CALREQ_TYPE, new Color(205, 240, 230));
        COLOURS.put(PSC_TYPE, new Color(205, 240, 230));

        COLOURS.put(MBTS_SINGLE_TYPE, new Color(159, 182, 205));
        COLOURS.put(MBTS_MULTI_TYPE, new Color(159, 182, 205));

        COLOURS.put(ZB_TYPE, new Color(216, 176, 48));        
        COLOURS.put(TOPO_TYPE, new Color(16, 176, 48));        
        COLOURS.put(ALFA_TYPE, new Color(16, 176, 48));        
    }

    /**
     *
     */
    public L1Threshold() {
        super(-1, "L1TT_");
        DefineKeyValuePairs();
        
        tvs = new ArrayList<>();
    }
    
     /**
     * Standard constructor. If an ID of -1 is supplied we create a new, empty
     * record and don't attempt to load anything from the database.  If the ID
     * supplied is greater than 0 we load this record from the database.
     * 
     * @param input_id The ID to load or -1.
     * @throws java.sql.SQLException
     */
    public L1Threshold(int input_id) throws SQLException {
        super(input_id, "L1TT_");
        DefineKeyValuePairs();
        
        if (input_id > 0) {
            forceLoad();
            setThresholdValues(getThresholdValuesFromDb());
        }
    }  
    
    private void DefineKeyValuePairs() {
        keyValue.putFirst("TYPE", "", "Type*");
        keyValue.putFirst("BITNUM", 1, "Bitnum*");
        keyValue.putFirst("ACTIVE", 0, "Active");
        keyValue.putFirst("MAPPING", 0, "Mapping");
        keyValue.putFirst("BCDELAY", -1, "BCDelay");
        keyValue.putFirst("SEED", "", "Seed");
        keyValue.putFirst("SEED_MULTI", -1, "SeedMulti");
    }
    
    ///Get the multiplicity of this Thresholds. Stored in link table.

    /**
     *
     * @return
     */
    public Integer get_multiplicity() {
        return multiplicity;
    }

    ///Get the position used in the definition string. Stored in link table.

    /**
     *
     * @return
     */
    public Integer get_position() {
        return position;
    }

    ///Set the position used in the definition string. Stored in link table.

    /**
     *
     * @param n
     */
    public void set_position(int n) {
        position = n;
    }
    ///Set this threshold so the multiplicity isn't displayed.
    //public void set_threshold_only(boolean torf) {
    // threshold_only = torf;
    //}
    final static String internals[] = {
        "RNDM0", "RNDM1", "RNDM2", "RNDM3", "PCLK0", "PCLK1",
        "BGRP0", "BGRP1", "BGRP2", "BGRP3", "BGRP4", "BGRP5", "BGRP6",
        "BGRP7", "BGRP8", "BGRP9", "BGRP10", "BGRP11", "BGRP12", "BGRP13", "BGRP14", "BGRP15"
    };

    /**
     *
     * @param thr
     * @return
     */
    static public boolean isInternalThreshold(final L1Threshold thr) {
        for (String intthr : internals) {
            if (intthr.equals(thr.get_name())) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @return
     */
    public ArrayList<L1ThresholdValue> getThresholdValues(){
        if (tvs == null){
            try {
                tvs = getThresholdValuesFromDb();
            } catch (SQLException ex) {
                logger.severe("SQLException while loading threshold values");
            }
        }
        return tvs;
    }

    /**
     *
     * @param list
     */
    public void setThresholdValues(ArrayList<L1ThresholdValue> list){
        tvs = list;
    }
     /**
     * Load all the threshold values for this threshold.
     * 
     * @return The list of all values for this threshold.
     * @throws java.sql.SQLException
     */
    private ArrayList<L1ThresholdValue> getThresholdValuesFromDb() throws SQLException {
        ArrayList<L1ThresholdValue> list = new ArrayList<>();
        L1ThresholdValue i = new L1ThresholdValue();
        String query = i.getQueryString();

        query = query.replace("FROM ", "FROM L1_TRIGGER_THRESHOLD, L1_TT_TO_TTV, ");

        String and = " WHERE "
                + "L1TT_ID=? "
                + "AND "
                + "L1TT2TTV_TRIGGER_THRESHOLD_ID=L1TT_ID "
                + "AND "
                + "L1TT2TTV_TRIG_THRES_VALUE_ID=L1TTV_ID";

        ConnectionManager mgr = ConnectionManager.getInstance();
        query = mgr.fix_schema_name(query + and);

        Connection con = mgr.getConnection();
        try (PreparedStatement ps = con.prepareStatement(query)) {
            ps.setInt(1, get_id());
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                L1ThresholdValue t = new L1ThresholdValue();
                t.loadFromRset(rset);
                list.add(t);
            }
            rset.close();
            ps.close();
        }
        
        return list;
    }

//    ///Return which screen this L1Threshold displayed on.
//    public NewJPanelY get_gui() {
//        return pan;
//    }
    ///Return the master table that this threshold belongs to.

    /**
     *
     * @return
     */
    public L1Master get_l1_master() {
        return l1master;
    }

    ///Reload the threshold from the database.
    @Override
    public void forceLoad() throws SQLException {
        tvs = null;
        ConnectionManager.getInstance().forceLoad(this);
    }

    ///How many bits does this threshold use?

    /**
     *
     * @return
     */
    public Integer get_bitnum() {
        return (Integer) keyValue.get("BITNUM");
    }

    ///Return the type of this threshold.

    /**
     *
     * @return
     */
    public String get_type() {
        return (String) keyValue.get("TYPE");
    }

    ///Return true if active is set.

    /**
     *
     * @return
     */
    public Integer get_active() {
        return (Integer) keyValue.get("ACTIVE");
    }

    ///Return the mapping.  Imporant for CTP.

    /**
     *
     * @return
     */
    public Integer get_mapping() {
        return (Integer) keyValue.get("MAPPING");
    }

    ///Return the BCdelay. Positive for ZB triggers.

    /**
     *
     * @return
     */
    public Integer get_bcdelay() {
        return (Integer) keyValue.get("BCDELAY");
    }

    ///Return the BCdelay. Positive for ZB triggers.

    /**
     *
     * @return
     */
    public String get_seed() {
        return (String) keyValue.get("SEED");
    }

    ///Return the BCdelay. Positive for ZB triggers.

    /**
     *
     * @return
     */
    public Integer get_seed_multi() {
        return (Integer) keyValue.get("SEED_MULTI");
    }

    ///Set this Threshold as active.

    /**
     *
     * @param inp
     */
    public void set_active(Integer inp) {
        keyValue.put("ACTIVE", inp);
    }

    ///Set the mapping.

    /**
     *
     * @param inp
     */
    public void set_mapping(int inp) {
        keyValue.put("MAPPING", inp);
    }

    ///Set the bitnum, used by the trigger tool. Not the user.

    /**
     *
     * @param inp
     */
    public void set_bitnum(int inp) {
        keyValue.put("BITNUM", inp);
    }

    ///Set the BCdelay. Positive for ZB triggers.

    /**
     *
     * @param inp
     */
    public void set_bcdelay(int inp) {
        keyValue.put("BCDELAY", inp);
    }

    ///Set the BCdelay. Positive for ZB triggers.

    /**
     *
     * @param inp
     */
    public void set_seed(String inp) {
        keyValue.put("SEED", inp);
    }

    ///Set the BCdelay. Positive for ZB triggers.

    /**
     *
     * @param inp
     */
    public void set_seed_multi(int inp) {
        keyValue.put("SEED_MULTI", inp);
    }

    ///Add this threshold and threshold values to the tree.
    /**
     * Add all the children to the tree.
     * @throws java.sql.SQLException
     */
    @Override
    public void addToTree(DefaultMutableTreeNode treeNode, int counter) throws SQLException {
        if (counter >= 0) {
            if (!added) {
                for (L1ThresholdValue ttv : getThresholdValues()) {
                    DefaultMutableTreeNode anotherLayer = new DefaultMutableTreeNode(ttv);
                    treeNode.add(anotherLayer);
                    added = true;
                }
            }
        }
    }

    ///String representation of this object.
    /**
     * So the tree view knows what information to display in the tree
     * 
     * @return A string containing the id, name and version of the record
     */
    @Override
    public String toString() {
        if (get_id() == -1) {
            return "L1 Threshold";
        }
        return "THRESHOLD: " + get_name() + " (DBid=" + get_id() + "/V." + get_version() + ")";
    }

    ///Deep copy of L1 Threshold.
    /**
     * Preform a deep copy of a threshold, and make a copy of all its children
     * L1ThresholdValues.
     * 
     * @return The copied object.
     */
    @Override
    public Object clone() {
        L1Threshold copy = new L1Threshold();
        copy.set_name(get_name());
        copy.set_version(get_version());
        copy.set_id(-1);
        copy.set_bitnum(get_bitnum());
        copy.set_active(get_active());
        copy.set_mapping(get_mapping());
        copy.set_bcdelay(get_bcdelay());
        copy.set_seed(get_seed());
        copy.set_seed_multi(get_seed_multi());

        for (L1ThresholdValue ttv : getThresholdValues()) {
            L1ThresholdValue ttvClone = (L1ThresholdValue) ttv.clone();
            copy.getThresholdValues().add(ttvClone);
        }
        
        return copy;
    }

    @Override
    public int doDiff(AbstractTable t, DefaultMutableTreeNode treeNode, java.util.Set<String> linkstoignore) throws SQLException {
        int ndiff = super.doDiff(t, treeNode, linkstoignore);

        if (t instanceof L1Threshold) {
            L1Threshold thres = (L1Threshold) t;
            /// compare threshold values
            ndiff += triggerdb.Diff.CompareTables.compareTables(this.getThresholdValues(), thres.getThresholdValues(), treeNode);

        }

        return ndiff;
    }

    ///Save this threshold, but check the threshold values too.
    @Override
    public int save() throws SQLException {

        ArrayList<Integer> tt_vec = new ArrayList<>();
        for (L1ThresholdValue link : getThresholdValues()) {
            tt_vec.add(link.get_id());
        }
        Collections.sort(tt_vec);

        ConnectionManager mgr = ConnectionManager.getInstance();
        //get candidates
        ArrayList<Integer> threshold_ids = mgr.get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");
        ArrayList<Integer> hcpPar_ids = new ArrayList<>();

        //if(threshold_ids.size()==0) logger.info("-----PJB no candidate thresholds for " + get_name());

        //check children
        for (Integer threshold_id : threshold_ids) {
            ArrayList<Integer> component_parIDs = mgr.get_ItemList("L1_TT_TO_TTV", "L1TT2TTV", "L1TT2TTV_TRIGGER_THRESHOLD", "L1TT2TTV_TRIG_THRES_VALUE", threshold_id);
            Collections.sort(component_parIDs);

            if (tt_vec.equals(component_parIDs)) {
                hcpPar_ids.add(threshold_id);
                break;
            }
        }

        if (hcpPar_ids.isEmpty()) {
            set_id(mgr.save(getTableName(), tablePrefix, get_id(), keyValue));

            for (L1ThresholdValue ttv : getThresholdValues()) {
                TreeMap<String, Object> link = new TreeMap<>();
                link.put("TRIGGER_THRESHOLD_ID", get_id());
                link.put("TRIG_THRES_VALUE_ID", ttv.get_id());

                mgr.save("L1_TT_TO_TTV", "L1TT2TTV_", -1, link);
            }
        } else {
            set_id(hcpPar_ids.get(0));
        }

        return get_id();
    }
    
    ///Save this threshold, but check the threshold values too.

    /**
     *
     * @return
     * @throws SQLException
     */
    public int compactsave() throws SQLException {

        ArrayList<Integer> tt_vec = new ArrayList<>();
        for (L1ThresholdValue link : getThresholdValues()) {
            tt_vec.add(link.get_id());
        }
        Collections.sort(tt_vec);

        ConnectionManager mgr = ConnectionManager.getInstance();
        //get candidates
        ArrayList<Integer> threshold_ids = mgr.get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");
        ArrayList<Integer> hcpPar_ids = new ArrayList<>();

        //if(threshold_ids.size()==0) logger.info("-----PJB no candidate thresholds for " + get_name());

        //This reduces the number of queries to the DB incase there are multiple
        //candidate Ids.
        ArrayList<int[]> holderParams = null;
        if(threshold_ids.size()>0){
            holderParams = mgr.get_CompactItemList("L1_TT_TO_TTV", "L1TT2TTV", "L1TT2TTV_TRIGGER_THRESHOLD", "L1TT2TTV_TRIG_THRES_VALUE", threshold_ids)
;
        }
        for (Integer threshold_id : threshold_ids) {
            ArrayList<Integer> component_parIDs = new ArrayList<>();
            for(int[] i : holderParams){
                if(i[0] == threshold_id) component_parIDs.add(i[1]);
            }
            Collections.sort(component_parIDs);
            if (tt_vec.equals(component_parIDs)) {
                hcpPar_ids.add(threshold_id);
                break;
            }
        }

        if (hcpPar_ids.isEmpty()) {
            set_id(mgr.save(getTableName(), tablePrefix, get_id(), keyValue));

            for (L1ThresholdValue ttv : getThresholdValues()) {
                TreeMap<String, Object> link = new TreeMap<>();
                link.put("TRIGGER_THRESHOLD_ID", get_id());
                link.put("TRIG_THRES_VALUE_ID", ttv.get_id());

                mgr.save("L1_TT_TO_TTV", "L1TT2TTV_", -1, link);
            }
        } else {
            set_id(hcpPar_ids.get(0));
        }

        return get_id();
    }


    ///Set the L1MasterTable.

    /**
     *
     * @param master
     */
    public void set_l1_master(L1Master master) {
        l1master = master;
    }

    /**
     * Set the multiplicity in the link table.
     * 
     * @param m the multiplicity.
     */
    public void set_multiplicity(final int m) {
        multiplicity = m;
    }

    /** Compare to thresholds to see if they're equal
     * @param other.
     * @return */
    @Override
    public boolean equals(Object other) {
        //System.out.println("Equals of L1Threshold");
        if (other instanceof L1Threshold) {
            L1Threshold others = (L1Threshold) other;
            //System.out.println("Names: "+get_name() +" "+others.get_name());
            if (get_name().equals(others.get_name())) {
                boolean same = true;
                for (L1ThresholdValue v1 : this.getThresholdValues()) {
                    if (!others.getThresholdValues().contains(v1)) {
                        same = false;
                    }
                    if (!same) {
                        return false; //early exit once we have one threshold not in the second Vector
                    }
                }
                for (L1ThresholdValue v2 : others.getThresholdValues()) {
                    if (!this.getThresholdValues().contains(v2)) {
                        same = false;
                    }
                    if (!same) {
                        return false; //early exit once we have one threshold not in the second Vector
                    }
                }

                //Check the multiplicity of the threshold
                if(!get_multiplicity().equals(others.get_multiplicity())){
                    same = false;
                }
                return same;
            }
        }
        return false;
    }

    /** Equals makes use of t
     * @return his.*/
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + (get_name() != null ? get_name().hashCode() : 0); //string: ok
        return hash;
    }

    @Override
    public String getTableName() {
        return "L1_TRIGGER_THRESHOLD";
    }
    
    /**
     *
     * @param inp
     */
    public void set_type(final String inp){
        getTriggerMap().put("TYPE", inp);
    }
    
    
}
