package triggerdb.Entities.HLT;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.TreeMap;
import triggerdb.Entities.AbstractTable;
import triggerdb.Connections.ConnectionManager;

///HLT Trigger Group.
/**
 * This table contains the group info.  For monitoring purposes
 * trigger chains can be grouped together, one can think of
 * electron, muon, tau groups, etc.
 */
public class HLTTriggerGroup extends AbstractTable {

    ///Chain, used only inside this class to get the chain name.
    private HLTTriggerChain chain = null;

    /**
     *
     */
    public HLTTriggerGroup() {
        super(-1, "HTG_");
        DefineKeyValuePairs();
    }
    
    ///Default constructor.
    /**
     * Constructor.  Pass the ID of the record you want to load.  Note that this
     * loads them one at a time, so you may want to pass -1 (which means don't
     * load) and then populate the data in some other way.
     * 
     * @param input_id -1 to create a new record, or the number of the record to
     * load.  Note that when save is called, if the record already exists the -1
     * will be ignored and the existing record used.
     * @throws java.sql.SQLException
     */
    public HLTTriggerGroup(int input_id) throws SQLException {
        super(input_id, "HTG_");
        DefineKeyValuePairs();

        if (input_id > 0) {
            forceLoad();
        }
    }

    private void DefineKeyValuePairs() {
        keyValue.remove("VERSION");
        keyValue.putFirst("TRIGGER_CHAIN_ID", -1, "Trigger Chain ID");
    }

    ///Create a copy of this group.
    /**
     * When the user clicks the copy button we copy the class.  This contains 
     * the code to set all the variables in the new class to the correct values.
     * These are used instead of Copy Constructors so that we don't need to know
     * what the class is an instance of, to make a copy of it.
     * @return 
     */
    @Override
    public Object clone() {
        HLTTriggerGroup copy = new HLTTriggerGroup();
        copy.set_trigger_chain_id(get_trigger_chain_id());
        copy.set_name(get_name());
        return copy;
    }
    
    /**
     *
     * @return
     * @throws SQLException
     */
    public boolean checkIDs() throws SQLException{
         ArrayList<Integer> groupIDs = ConnectionManager.getInstance().get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");
         return groupIDs.isEmpty();
    }
      
    /**
     *
     * @return
     */
    public TreeMap<String,Object> getKey(){
        return keyValue;
    }
    
    @Override
    public boolean equals(Object object){
        if (!(object instanceof HLTTriggerGroup)){
            return false;
        }
        
        if (this.get_id() != -1 && ((HLTTriggerGroup)object).get_id() != -1)
        {
            return ((HLTTriggerGroup)object).get_id() == get_id(); 
        }
        else{
            boolean returnValue = true;
            if (this.get_trigger_chain_id() != -1 && ((HLTTriggerGroup)object).get_trigger_chain_id() != -1){
                returnValue = this.get_trigger_chain_id().equals(((HLTTriggerGroup)object).get_trigger_chain_id());
            }
            if (!this.get_name().isEmpty() && !((HLTTriggerGroup)object).get_name().isEmpty()) {
                returnValue = returnValue && this.get_name().equals(((HLTTriggerGroup)object).get_name());
            }
            return returnValue;
        }
    }
    
    ///Simple save, called by chain.
    /** 
     * Save the Trigger Group.  This isn't particularly clever, it simply checks
     * if a group that is exactly like this one exists already.  If it does, 
     * then the function returns the ID of the existing group, otherwise it 
     * creates a new one and returns the ID of that.
     *  
     * @return 
     * @throws SQLException Stop on an SQL problem.
     */
    @Override
    public int save() throws SQLException {
        ArrayList<Integer> groupIDs = ConnectionManager.getInstance().get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");

        if (groupIDs.isEmpty()) {
            set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix, get_id(), keyValue));
        } else {
            set_id(groupIDs.get(0));
        }

        return get_id();
    }

    ///Get the trigger chain (ID) this group belongs to.
    /**
     * ID of the parent trigger chain.  A group can only belong to one chain.
     * 
     * @return Integer ID of the parent trigger chain.
     */
    public Integer get_trigger_chain_id() {
        return (Integer) keyValue.get("TRIGGER_CHAIN_ID");
    }

    ///Set the ID of the trigger chain this belongs to.
    /**
     * Set the trigger chain ID to a new value.
     * 
     * @param inp The new trigger chain ID.
     */
    public void set_trigger_chain_id(int inp) {
        keyValue.put("TRIGGER_CHAIN_ID", inp);
    }

    ///Get the actual chain object.
    /**
     * In general classes would only be able to load those below them.  However
     * this private method recovers the chain that this group belongs to (since
     * a group can only belong to one chain 
     * 
     * @return A TriggerChain object if it could be loaded, otherwise null.
     */
    private HLTTriggerChain get_trigger_chain() throws SQLException {
        if (chain == null && new HLTTriggerChain().isValid(get_trigger_chain_id())) {
            chain = new HLTTriggerChain(get_trigger_chain_id());
        }

        return chain;
    }
    
    public HLTTriggerChain get_trigger_chain_public() throws SQLException {
        return get_trigger_chain();
    }

    ///String representation of group.
    /**
     * If the record contains no data, to string returns something a bit like
     * the name of the class.  This is used in the combo box on the main screen.
     * If the record does contain data then it outputs what will be shown in the
     * tree.
     * 
     * @return String representation of the object.
     */
    @Override
    public String toString() {
        if (get_id() == -1) {
            return "HLT Group";
        }
        return "TRIGGER GROUP: " + get_name() + " (DBid=" + get_id() + ")";
        
    }

    ///Get the minimum column names for the search results.
    /**
     * Titles for the search results table.
     * 
     * @return Vector of String names.
     */
    @Override
    public ArrayList<String> get_min_names() {
        ArrayList<String> info = new ArrayList<>();
        info.add("ID");
        info.add("Name");
        info.add("Chain ID");

        return info;
    }

    ///Get the minimum information for the search results.
    /**
     * The minimum information to show in the search results.
     * 
     * @return Vector of ID, Name and Chain ID and name.
     * @throws java.sql.SQLException
     */
    @Override
    public ArrayList<Object> get_min_info() throws SQLException {
        ArrayList<Object> info = new ArrayList<>();
        info.add(get_id());
        info.add(get_name());

        if (get_trigger_chain() == null) {
            info.add("Invalid");
        } else {
            info.add(get_trigger_chain_id() + " ( " + get_trigger_chain().get_name() + " ) ");
        }

        return info;
    }

    
    @Override
    public String getTableName() {
         return "HLT_TRIGGER_GROUP";
    }
    
    //find all the chains of the selected groups of this supermaster table, and fill in the list table

    /**
     *
     * @param hlt_menu_id
     * @return
     * @throws SQLException
     */
    public ArrayList<HLTTriggerChain> getChains(int hlt_menu_id) throws SQLException {
        ArrayList<HLTTriggerChain> chains_v = new ArrayList<>(); 
        
        String query = "SELECT " +
                       "HTG_NAME, HTG_TRIGGER_CHAIN_ID, " + 
                       "HTC_ID, HTC_CHAIN_COUNTER, " +
                       "HTM2TC_TRIGGER_CHAIN_ID, "
                +      "HTM2TC_TRIGGER_MENU_ID, HTM_ID " +
                       "FROM " +
                       "HLT_TRIGGER_GROUP, HLT_TRIGGER_CHAIN, HLT_TM_TO_TC, HLT_TRIGGER_MENU " +
                       "WHERE " +
                       "HTM_ID=? " +
                       "AND " +
                       "HTM_ID=HTM2TC_TRIGGER_MENU_ID " +
                       "AND " +
                       "HTM2TC_TRIGGER_CHAIN_ID=HTC_ID " +
                       "AND " +
                       "HTC_ID=HTG_TRIGGER_CHAIN_ID " +
                       "AND " +
                       "HTG_NAME=? " +
                       "ORDER BY HTC_CHAIN_COUNTER ASC";

        query = ConnectionManager.getInstance().fix_schema_name(query);

        //System.out.println("Chains query " + query);

        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ps.setInt(1, hlt_menu_id);
        ps.setString(2, get_name());
        ResultSet rset = ps.executeQuery();
        while (rset.next()) {
            HLTTriggerChain ch = new HLTTriggerChain(rset.getInt("HTC_ID"));
            chains_v.add(ch);
        }
        rset.close();
        ps.close();
            
        return chains_v; 
    }
    

}