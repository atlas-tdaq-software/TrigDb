package triggerdb.Entities.HLT;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.logging.Level;
import javax.swing.tree.DefaultMutableTreeNode;
import triggerdb.Entities.AbstractTable;
import triggerdb.Connections.ConnectionManager;

/**
 * HLT Trigger Chain mapping class.
 *
 * @author Paul Bell
 * @author Simon Head
 */
public final class HLTTriggerChain extends AbstractTable {

    /**Signatures that are part of this chain.*/
    private ArrayList<HLTTriggerSignature> signatures = new ArrayList<>();
    ///Streams that this chain contains.
    private ArrayList<HLTTriggerStream> streams = new ArrayList<>();
    ///Types for this chain.
    private ArrayList<HLTTriggerType> types = new ArrayList<>();
    ///Groups for this chain.
    private ArrayList<HLTTriggerGroup> groups = new ArrayList<>();
    private boolean needtoadd_type = true;
    private boolean needtoadd_group = true;
    private ArrayList<DefaultMutableTreeNode> tree_data = new ArrayList<>();
    private ArrayList<DefaultMutableTreeNode> tree_data_stream = new ArrayList<>();

    @Override
    public String getTableName() {
        return "HLT_TRIGGER_CHAIN";
    }

    
    private boolean valid_lower_chain = false;

    /**
     *
     */
    public HLTMaster hltmaster = null;
    // Static Queries

    /**
     *
     */
    public static final String QUERY_STREAMS = "SELECT HTR_DESCRIPTION, HTR_ID,"
            + " HTR_NAME, HTR_OBEYLB, HTR_TYPE, HTC2TR_TRIGGER_STREAM_PRESCALE "
            + "FROM HLT_TC_TO_TR, HLT_TRIGGER_STREAM "
            + "WHERE HTC2TR_TRIGGER_CHAIN_ID=? "
            + "AND HTC2TR_TRIGGER_STREAM_ID=HTR_ID ";

    /* Default constructor
    */

    /**
     *
     */

    public HLTTriggerChain() {
        super(-1, "HTC_");
        DefineKeyValuePairs();
    }
    
    /** Constructor to be used when the chain need to loaded from the db
     * @param input_id.
     * @throws java.sql.SQLException*/
    public HLTTriggerChain(int input_id) throws SQLException {
        super(input_id, "HTC_");
        DefineKeyValuePairs();

        if (input_id > 0) {
            forceLoad();
            streams = getStreamsFromDb();
            types = getTriggerTypesFromDb();
            groups = getTriggerGroupsFromDb();
            signatures = getSignaturesFromDb();
        }
    }
    
    private void DefineKeyValuePairs() {
        keyValue.putFirst("USER_VERSION", 1, "User Version");
        keyValue.putFirst("COMMENT", "Comment", "Comment");
        keyValue.putFirst("CHAIN_COUNTER", 1, "Counter");
        keyValue.putFirst("LOWER_CHAIN_NAME", "", "Lower Chain");
        keyValue.putFirst("EB_POINT", -1, "Event building point");
    }
        
    @Override
    public void forceLoad() throws SQLException {
        signatures = new ArrayList<>();
        streams = new ArrayList<>();
        types = new ArrayList<>();
        groups = new ArrayList<>();
        tree_data = new ArrayList<>();
        needtoadd_type = true;
        needtoadd_group = true;
        tree_data_stream = new ArrayList<>();
        ConnectionManager.getInstance().forceLoad(this);
    }
    
    
    @Override
    public boolean equals(final Object obj) {
        if (obj instanceof HLTTriggerChain) {
            if (!this.get_name().equals(((HLTTriggerChain) obj).get_name())) {
                return false;
            } else if (!this.get_comment().equals(((HLTTriggerChain) obj).get_comment())) {
                return false;
            } else if (!this.get_chain_counter().equals(((HLTTriggerChain) obj).get_chain_counter())) {
                return false;
            } else if (!this.get_eb_point().equals(((HLTTriggerChain) obj).get_eb_point())) {
                return false;
            } else return (!this.get_lower_chain_name().equals(((HLTTriggerChain) obj).get_lower_chain_name()));
        } else {
            return false;
        }
    }

    @Override
    /*
     * Hashcode override.
     * There are two hashes here, both will give a different `unique' hash.
     * Need to pick one and stick with it. Not sure which will reduce the chance
     * of overlap the most?
     */
    public int hashCode () {
        long fill = 13;
        fill = fill+31*this.get_name().hashCode()
               +37*this.get_comment().hashCode()
               +41*this.get_chain_counter() //integer - do not hash!
               +43*this.get_lower_chain_name().hashCode()
               +47*this.get_eb_point();
        
        return ((int) fill) ^ ((int) (fill >> 32));
    }

    /** Renumber the signatures - bad idea?
     * @throws java.sql.SQLException */
    public void renumberSignatures() throws SQLException {
        if (!signatures.isEmpty()) {
            int i = 1;
            for (HLTTriggerSignature signature : getSignatures()) {
                signature.set_signature_counter(i);
                ++i;
            }
        }
    }

    /** Set the master record.  Needed to add EF chains, for example
     * @param mast.*/
    public void set_hlt_master(HLTMaster mast) {
        hltmaster = mast;
    }

    /** Get the user version.
     * @return  */
    public Integer get_user_version() {
        return (Integer) keyValue.get("USER_VERSION");
    }

    /** Set the user version
     * @param inp.*/
    public void set_user_version(int inp) {
        keyValue.put("USER_VERSION", inp);
    }

    /** Get the chain counter
     * @return .*/
    public Integer get_chain_counter() {
        return (Integer) keyValue.get("CHAIN_COUNTER");
    }

    /** Set the chain counter
     * @param inp.*/
    public void set_chain_counter(int inp) {
        keyValue.put("CHAIN_COUNTER", inp);
    }

    /** Get the lower chain name.
     * @return  */
    public String get_lower_chain_name() {
        return (String) keyValue.get("LOWER_CHAIN_NAME");
    }

    /** Set the lower chain name
     * @param inp.*/
    public void set_lower_chain_name(String inp) {
        keyValue.put("LOWER_CHAIN_NAME", inp);
    }

    /**
     *
     * @return
     */
    public Integer get_eb_point() {
        return (Integer) keyValue.get("EB_POINT");
    }
    
    /**
     *
     * @param inp
     */
    public void set_eb_point(int inp) {
        keyValue.put("EB_POINT", inp);
    }

    /**
     *
     * @return
     */
    public ArrayList<HLTTriggerSignature> getSignatures() {
        if (signatures.isEmpty()) {   
            try{
                signatures = getSignaturesFromDb();
            } catch (SQLException ex){
                logger.severe("SQLException in getting HLTSignatures." + ex.getMessage());
                signatures = new ArrayList<>(); 
            }
        }
        return signatures;
    }

    /**
     *
     * @param sign
     */
    public void setSignatures(ArrayList<HLTTriggerSignature> sign) {
        signatures = sign;
    }

    /**
     * Load all the signatures for this chain.
     * 
     * @return A vector with all signatures for this chain.
     * @throws java.sql.SQLException
     */
    private ArrayList<HLTTriggerSignature> getSignaturesFromDb() throws SQLException {
        ArrayList<HLTTriggerSignature> signatureList = new ArrayList<>();        
        HLTTriggerSignature sig = new HLTTriggerSignature();
        String query = sig.getQueryString();

        query = query.replace("FROM ",
                ", HTC2TS_SIGNATURE_COUNTER FROM HLT_TRIGGER_CHAIN, "
                + "HLT_TC_TO_TS, ");

        String where = " WHERE HTC_ID=? "
                + "AND HTC2TS_TRIGGER_CHAIN_ID=HTC_ID "
                + "AND HTC2TS_TRIGGER_SIGNATURE_ID=HTS_ID "
                + "ORDER BY HTC2TS_SIGNATURE_COUNTER";
        
        ConnectionManager mgr = ConnectionManager.getInstance();
        ArrayList<Object> filters = new ArrayList<>();
        filters.add(this.get_id());
        // Array to hold the values of "element_counter".
        ArrayList<Integer> sigCounter = new ArrayList<>();
        // Hack forceLoadVector to return the signature_counter from
        // link table.
        ArrayList<AbstractTable> sigTabList = mgr.forceLoadVector(sig, query, where, filters, 4, sigCounter);
        
        if (sigTabList.size() == sigCounter.size()) {
            for (int idx = 0; idx < sigTabList.size(); idx++) {
                HLTTriggerSignature sigTab = (HLTTriggerSignature) sigTabList.get(idx);
                HLTTriggerSignature signatureWithElement = new HLTTriggerSignature(sigTab.get_id());
                signatureWithElement.set_signature_counter(sigCounter.get(idx));
                logger.log(Level.FINER, "Loaded {0}", signatureWithElement.toString());
                signatureList.add(signatureWithElement);
            }
        } else {
            String msg = " Problems loading trigger signatures for "
                    + "chain: " + this.toString()
                    + " The number of \"signature counters\" "
                    + "doesn't match the number of HLTTriggerSignatures.";
            logger.severe(msg);
        }
        return signatureList;
    }

    /**
     *
     * @return
     */
    public ArrayList<HLTTriggerGroup> getTriggerGroups() {
        if (groups.isEmpty()) {
            try{
                groups = getTriggerGroupsFromDb();
            } catch (SQLException ex){
                logger.severe("SQLException in getting HLTElements." + ex.getMessage());
                groups = new ArrayList<>();
            }
        }
        return groups;
    }

    /**
     *
     * @param groups
     */
    public void setTriggerGroups(ArrayList<HLTTriggerGroup> groups) {
        this.groups = groups;
    }

        
     /** Load all the groups for this chain
     * @return .
     * @throws java.sql.SQLException*/
    public ArrayList<HLTTriggerGroup> getTriggerGroupsFromDb() throws SQLException {
        ArrayList<HLTTriggerGroup> groupList = new ArrayList<>();

        String query = "SELECT HTG_ID FROM HLT_TRIGGER_GROUP WHERE HTG_TRIGGER_CHAIN_ID=? ORDER BY HTG_ID ASC";
        query = ConnectionManager.getInstance().fix_schema_name(query);
        ArrayList<Integer> ids = new ArrayList<>();
        try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query)) {
            ps.setInt(1, get_id());
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                ids.add(rset.getInt("HTG_ID"));
            }

            rset.close();
            ps.close();
        }

        Iterator<Integer> it = ids.iterator();

        while (it.hasNext()) {
            groupList.add(new HLTTriggerGroup(it.next()));
        }
        return groupList;
    }
    
    /**
     *
     * @return
     * @throws SQLException
     */
    public ArrayList<HLTTriggerType> getTriggerTypes() throws SQLException {
        if (types.isEmpty()) {
            try{
                types = getTriggerTypesFromDb();
            } catch (SQLException ex){
                logger.severe("SQLException in getting HLTTriggerTypes." + ex.getMessage());
                types = new ArrayList<>();
                throw ex;
            }
        }
        return types;
    }

    /**
     *
     * @param types
     */
    public void setTriggerTypes(ArrayList<HLTTriggerType> types) {
        this.types = types;
    }

    /**Load all the types for this chain.
     * @return
     * @throws java.sql.SQLException  */
    private ArrayList<HLTTriggerType> getTriggerTypesFromDb() throws SQLException{
        ArrayList<HLTTriggerType> typeList = new ArrayList<>();

        if (get_id() > 0) {
            String query = "SELECT HTT_ID FROM HLT_TRIGGER_TYPE WHERE HTT_TRIGGER_CHAIN_ID=? ORDER BY HTT_ID ASC";
            query = ConnectionManager.getInstance().fix_schema_name(query);
            ArrayList<Integer> ids = new ArrayList<>();

            PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
            ps.setInt(1, get_id());
            ResultSet rset = ps.executeQuery();

            while (rset.next()) {
                int anid = rset.getInt("HTT_ID");
                ids.add(anid);
            }

            rset.close();
            ps.close();

            Iterator<Integer> it = ids.iterator();

            while (it.hasNext()) {
                Integer anid = it.next();
                typeList.add(new HLTTriggerType(anid));
            }
        }

        return typeList;
    }

    /**
     *
     */
    public void dropLinks() {
        this.dropTypes();
        this.dropGroups();
        this.dropSignatures();
    }

    /**
     *
     */
    public void dropTypes() {
        if (!types.isEmpty()) {
            types.clear();
        }
    }

    /**
     *
     */
    public void dropGroups() {
        if (!groups.isEmpty()) {
            groups.clear();
        }
    }

    /**
     *
     */
    public void dropSignatures() {
        if (!signatures.isEmpty()) {
            signatures.clear();
        }
    }

    /**
     * Add children to tree. This adds trigger type, trigger stream and step.
     * @throws java.sql.SQLException
     */
    @Override
    public void addToTree(DefaultMutableTreeNode treeNode, int counter) throws SQLException {
        --counter;

        if (counter >= 0) {
            // Trigger Type
            ArrayList<HLTTriggerType> links_tctt = getTriggerTypes();
            Iterator<HLTTriggerType> myIterator_tctt = links_tctt.iterator();

            if (needtoadd_type) {
                while (myIterator_tctt.hasNext()) {
                    HLTTriggerType test = (HLTTriggerType) myIterator_tctt.next();
                    DefaultMutableTreeNode anotherLayer = new DefaultMutableTreeNode(test);
                    treeNode.add(anotherLayer);
                }
                needtoadd_type = false;
            }

            // Trigger Group
            ArrayList<HLTTriggerGroup> links_tctg = getTriggerGroups();
            Iterator<HLTTriggerGroup> myIterator_tctg = links_tctg.iterator();

            if (needtoadd_group) {
                while (myIterator_tctg.hasNext()) {
                    HLTTriggerGroup test = (HLTTriggerGroup) myIterator_tctg.next();
                    DefaultMutableTreeNode anotherLayer = new DefaultMutableTreeNode(test);
                    treeNode.add(anotherLayer);
                }
                needtoadd_group = false;
            }

            // Trigger Stream
            if (tree_data_stream.isEmpty()) {
                for (HLTTriggerStream stream : getStreams()) {
                    DefaultMutableTreeNode anotherLayer = new DefaultMutableTreeNode(stream);
                    treeNode.add(anotherLayer);
                    tree_data_stream.add(anotherLayer);
                    stream.addToTree(anotherLayer, counter);
                }
            } else {
                int i = 0;
                for (HLTTriggerStream stream : getStreams()) {
                    stream.addToTree(tree_data_stream.get(i), counter);
                    ++i;
                }
            }

            if (tree_data.isEmpty()) {
                for (HLTTriggerSignature signature : getSignatures()) {
                    DefaultMutableTreeNode anotherLayer = new DefaultMutableTreeNode(signature);
                    treeNode.add(anotherLayer);
                    tree_data.add(anotherLayer);
                }
            } else {
                int i = 0;
                for (HLTTriggerSignature signature : getSignatures()) {
                    signature.addToTree(tree_data.get(i), counter);
                    ++i;
                }
            }
        }
    }

    
    /**
     * Make a copy of this chain.
     * When the user clicks the copy button we copy the class. This contains the
     * code to set all the variables in the new class to the correct values.
     * 
     * Don't increment the version this will be done automatically.
     * @return 
     */
    @Override
    public Object clone() {
        HLTTriggerChain copy = new HLTTriggerChain();
        copy.set_user_version(get_user_version());
        copy.set_chain_counter(get_chain_counter());
        copy.set_lower_chain_name(get_lower_chain_name());
        copy.set_name(get_name());
        copy.set_version(get_version());
        copy.set_hlt_master(hltmaster);
        copy.set_id(get_id());
            
        for (HLTTriggerStream stream : streams) {
            copy.streams.add((HLTTriggerStream) stream.clone());
        }
        for (HLTTriggerType type : types) {
            copy.types.add((HLTTriggerType) type.clone());
        }
        for (HLTTriggerSignature signature : signatures) {
            copy.signatures.add((HLTTriggerSignature) signature.clone());
        }

        for (HLTTriggerGroup group : groups) {
            copy.groups.add((HLTTriggerGroup) group.clone());
        }
        
        return copy;
    }

    @Override
    public int doDiff(AbstractTable t, DefaultMutableTreeNode treeNode, java.util.Set<String> linkstoignore) throws SQLException {
        int ndiff = super.doDiff(t, treeNode, linkstoignore);

        if (t instanceof HLTTriggerChain) {
            HLTTriggerChain chain = (HLTTriggerChain) t;
            //System.out.println("Loading children for chain " + chain.get_name());
            /// Look for signatures
            ndiff += triggerdb.Diff.CompareTables.compareTables(this.getSignatures(), chain.getSignatures(), treeNode);
            /// Look for types
            ndiff += triggerdb.Diff.CompareTables.compareTables(this.getTriggerTypes(), chain.getTriggerTypes(), treeNode);
            /// Look for groups
            ndiff += triggerdb.Diff.CompareTables.compareTables(this.getTriggerGroups(), chain.getTriggerGroups(), treeNode);
            /// Look for streams
            ndiff += triggerdb.Diff.CompareTables.compareTables(this.getStreams(), chain.getStreams(), treeNode);

            /// drop links to save memory
            this.dropLinks();
            chain.dropLinks();
        }

        return ndiff;
    }

    ///Save routine is super complex.
    /**
     * Contains the code that saves the record to the database.
     * PJB 25/4/08 want a better way to do this - if the name matches, start 
     * comparing inputs, then if the input matches start comparing algorithms
     * also need to do this in types and group
     * @return 
     * @throws java.sql.SQLException 
     */
    @Override
    public int save() throws SQLException {
        int matchingID = -1;

        logger.log(Level.FINER,"Chain name       : {0}"
                + "\n" + "Num of signatures: {1}" + "\n"
                + "Num of types     : {2}" + "\n" + "Num of groups    : {3}"
                + "\n" + "Num of streams   : {4}", 
                new Object[]{get_name(), signatures.size(), types.size(), 
                    groups.size(), streams.size()});

        //Map of signature IDs with their counters - for comparison later
        TreeMap<Integer, Integer> signature_ids = new TreeMap<>();
        for (HLTTriggerSignature signature : signatures) {
            signature_ids.put(signature.get_id(), signature.get_signature_counter());
        }

        //STEP 1 - look for candidate chains with name name, type and counter
        ArrayList<Integer> htc_nameIds = ConnectionManager.getInstance().get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");
        for (Integer cha_id : htc_nameIds) {
            //FIRST STEP - match the signatures
            TreeMap<Integer, Integer> sigresults = ConnectionManager.getInstance().getConstrainedItemList("HTC2TS_TRIGGER_SIGNATURE_ID","HTC2TS_SIGNATURE_COUNTER","HLT_TC_TO_TS", "HTC2TS_TRIGGER_CHAIN_ID",cha_id);
            if (!sigresults.equals(signature_ids)) {
                continue;
            }
            //SECOND STEP - match the streams
            HLTTriggerChain existingChain = new HLTTriggerChain(cha_id);
            if (!areExistingStreamsIdenticalToCurrentStreams(existingChain.getStreams())) {
                continue;
            }
            //THIRD STEP - match groups
            if (!areExistingGroupsIdenticalToCurrentGroups(existingChain.getTriggerGroups())) {
                continue;
            }
            //FOURTH STEP - match types
            /*if (!areExistingTypesIdenticalToCurrentTypes(existingChain.getTriggerTypes())) {
                continue;
            }*/

            matchingID = cha_id;
            break;
        }

        if (matchingID < 0) {
            logger.fine("Saving new Chain");

            int version = 1 + ConnectionManager.getInstance().getMaxVersion(getTableName(), tablePrefix, get_name());
            keyValue.put("VERSION", version);

            set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix, get_id(), keyValue));

              //save parameters
            ArrayList<TreeMap<String, Object>> table = new ArrayList<>();
            
            for (HLTTriggerSignature signature : signatures) {
                TreeMap<String, Object> link = new TreeMap<>();
                link.put("TRIGGER_CHAIN_ID", get_id());
                link.put("TRIGGER_SIGNATURE_ID", signature.get_id());
                link.put("SIGNATURE_COUNTER", signature.get_signature_counter());
                table.add(link);
                //ConnectionManager.getInstance().save("HLT_TC_TO_TS", "HTC2TS_", -1, link);
            }
            TreeMap<Integer, Integer> results = ConnectionManager.getInstance().saveBatch("HLT_TC_TO_TS", "HTC2TS_", 0, table);

            table.clear();
            for (HLTTriggerStream stream : streams) {
                TreeMap<String, Object> link = new TreeMap<>();
                link.put("TRIGGER_CHAIN_ID", get_id());
                link.put("TRIGGER_STREAM_ID", stream.get_id());
                link.put("TRIGGER_STREAM_PRESCALE", stream.get_stream_prescale());
                table.add(link);
                //ConnectionManager.getInstance().save("HLT_TC_TO_TR", "HTC2TR_", -1, link);
            }
            TreeMap<Integer, Integer> results1 = ConnectionManager.getInstance().saveBatch("HLT_TC_TO_TR", "HTC2TR_", 0, table);

            //Save the chain-ttype
            table.clear();

            for (HLTTriggerGroup link : groups) {
                link.set_id(-1);
                link.set_trigger_chain_id(get_id());
                if(link.checkIDs()){
                    table.add(link.getKey());
                }   
                //link.save(); // wpv patch
            }
            TreeMap<Integer, Integer> results3 = ConnectionManager.getInstance().saveBatch("HLT_TRIGGER_GROUP", "HTG_", 0, table);
            
     
            logger.log(Level.FINER, "[SAVE CHAIN :: new id = {0} ] {1}", new Object[]{get_id(), get_name()});
        } else {
            set_id(matchingID);
            logger.log(Level.FINER, "[SAVE CHAIN :: returned id = {0} ] {1}", new Object[]{get_id(), get_name()});
        }
        return get_id();
    }

    private Boolean areExistingStreamsIdenticalToCurrentStreams(ArrayList<HLTTriggerStream> existingStreams){
        return existingStreams.containsAll(streams) && streams.containsAll(existingStreams);
    }
    
    public Boolean StreamsIdenticalPublic(ArrayList<HLTTriggerStream> existingStreams) {
        return areExistingStreamsIdenticalToCurrentStreams(existingStreams);
   }

    
    private Boolean areExistingGroupsIdenticalToCurrentGroups(ArrayList<HLTTriggerGroup> existingGroups){
        return existingGroups.containsAll(groups) && groups.containsAll(existingGroups);
    }
    
    public Boolean GroupsIdenticalPublic(ArrayList<HLTTriggerGroup> existingGroups) {
        return areExistingGroupsIdenticalToCurrentGroups(existingGroups);
   }
    
    /**
     * Set valid lower chain.  Used in working out if anything in the menu
     * doesn't start properly.
     * @param b
     */
    public void set_valid_input(boolean b) {
        valid_lower_chain = b;
    }

    /** Is the lower chain name also in the menu? Set externally
     * @return .*/
    public boolean get_valid_input() {
        return valid_lower_chain;
    }

    ///String representation of the chain.
    @Override
    public String toString() {
        if (get_id() == -1) {
            return "HLT Chain";
        }
        return "CHAIN: " + get_name() + " (DBid=" + get_id() + "/V." + get_version() + ")";

    }

    ///Minimum names for the search results.
    @Override
    public ArrayList<String> get_min_names() {
        ArrayList<String> info = new ArrayList<>();
        info.add("ID");
        info.add("Name");
        info.add("Version");
        info.add("L2 or EF");
        info.add("Chain Counter");
        return info;
    }

    ///Minimum information for the search results.
    @Override
    public ArrayList<Object> get_min_info() {
        ArrayList<Object> info = new ArrayList<>();
        info.add(get_id());
        info.add(get_name());
        info.add(get_version());
        info.add(get_user_version());
        info.add(get_chain_counter());
        return info;
    }

    ///Set the master table.

    /**
     *
     * @param inp
     */
    public void set_master(HLTMaster inp) {
        hltmaster = inp;
    }
    
    public HLTMaster get_master(){
        return hltmaster;
    }

    /**
     *
     * @return
     */
    public ArrayList<HLTTriggerStream> getStreams() {
        if (streams.isEmpty()) {
            try{
                streams = getStreamsFromDb();
            } catch (SQLException ex){
                logger.severe("SQLException in getting HLTStream." + ex.getMessage());
                streams = new ArrayList<>();                
            }
        }
        return streams;
    }
    
    /**
     *
     * @param streams
     */
    public void setStreams(ArrayList<HLTTriggerStream> streams) {
        this.streams = streams;
    }
        
    /**
     * Load the streams for this chain from the database.
     * @return 
     * @throws java.sql.SQLException 
     */
    public ArrayList<HLTTriggerStream> getStreamsFromDb() throws SQLException {
        ArrayList<HLTTriggerStream> streamList = new ArrayList<>();

        if (get_id() > 0) {
            String query = HLTTriggerChain.QUERY_STREAMS;
            query = ConnectionManager.getInstance().fix_schema_name(query);
            PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);

            ps.setInt(1, this.get_id());
            ResultSet rset = ps.executeQuery();

            while (rset.next()) {
                HLTTriggerStream t = new HLTTriggerStream();
                t.loadFromRset(rset);
                t.set_stream_prescale(rset.getString("HTC2TR_TRIGGER_STREAM_PRESCALE"));
                streamList.add(t);
            }

            rset.close();
            ps.close();
        }
        return streamList;
    }

    /**
     * Set a comment for this cha
     * @param inp
     */
    public void set_comment(String inp) {
        keyValue.put("COMMENT", inp);
    }

    ///Get a comment for this chain.

    /**
     *
     * @return
     */
    public String get_comment() {
        return (String) keyValue.get("COMMENT");
    }

    /**
     * Initializes streams, types, groups and signature vectors, so that we can
     * add these items without automatically initializing them from DB.
     * Only if you know what you're doing. Used to speed up loading of menu.     
     */
    public void confuseLoader() {
        streams = new ArrayList<>();
        types = new ArrayList<>();
        groups = new ArrayList<>();
        signatures = new ArrayList<>();
    }

    ///edit l1 item comment

    /**
     *
     * @throws SQLException
     */
    public void editComment() throws SQLException {
        String query = "UPDATE HLT_TRIGGER_CHAIN SET HTC_COMMENT=? WHERE HTC_ID=?";
        query = ConnectionManager.getInstance().fix_schema_name(query);
        try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query)) {
            ps.setString(1, get_comment());
            ps.setInt(2, get_id());
            ps.executeUpdate();
            ps.close();
        }
    }
   
    /**
     *
     * @return
     * @throws SQLException
     */
    public HLTTriggerMenu GetMenuFromMaster() throws SQLException
    {
        return hltmaster.get_menu();
    }
}    

