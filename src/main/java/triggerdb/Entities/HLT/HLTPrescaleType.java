/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package triggerdb.Entities.HLT;

/**
 *
 * @author Michele
 */
public enum HLTPrescaleType {

    /**
     *
     */
    Prescale,

    /**
     *
     */
    Pass_Through,

    /**
     *
     */
    Stream,

    /**
     *
     */
    ReRun,

    /**
     *
     */
    NotSet
}
