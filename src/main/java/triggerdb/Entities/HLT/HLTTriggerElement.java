package triggerdb.Entities.HLT;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import javax.swing.tree.DefaultMutableTreeNode;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;

/**
 * HLT Trigger Element object.
 *
 * @author Paul Bell
 * @author Simon Head
 */
public final class HLTTriggerElement extends AbstractTable {

    /** Vector of input trigger elements. */
    private ArrayList<HLTTE_TE> tete = new ArrayList<>();
    /** Vector of components that belong to a trigger element. */
    private ArrayList<HLTComponent> algorithms =  new ArrayList<>();
    /** So the tree knows what to display. */
    private boolean needtoadd_tete = true;
    /** Useful for the tree. */
    private final ArrayList<DefaultMutableTreeNode> tree_data = new ArrayList<>();
    /** Held in a link table. */
    private Integer element_counter = -1;

    /**
     *
     */
    public HLTTriggerElement() {
        super("HTE_");        
        setKeys();
    }
    
    /**
     * Default constructor. It load the object from DB with the id 
     * <code>input_id</code>
     * 
     * @param input_id the Database ID of this object.
     * @throws java.sql.SQLException
     */
    public HLTTriggerElement(final int input_id) throws SQLException {
        super(input_id, "HTE_");
        
        setKeys();
        
        if (input_id > 0) {
            forceLoad();
            tete = loadInputElementsFromDb();
            
        }
    }

    private void setKeys() {
        keyValue.putFirst("HASH", -1, "HashCode ofr the table");
        if(ConnectionManager.TopoStartFromColumnExists == 1){
            keyValue.putFirst("TOPO_START_FROM", "~", "Element seeded from a topo item");
        }
    }

    /** Trick the class into thinking that the children have been loaded. */
    public void confuseLoader() {
        tete = new ArrayList<>();
        algorithms = new ArrayList<>();
    }
    
    ///Get the topo start from value
    /**
     * This value describes what Topo item this HLT chain originated from so that 
     * information about that Topo item can be retrieved.
     * @return 
     */
    public String get_topo_start_from() {
        if(ConnectionManager.TopoStartFromColumnExists == 1){
            return (String) keyValue.get("TOPO_START_FROM");
        } else {
            String empty = "";
            return empty;
        }
    }

    ///Set the topo start from string.

    /**
     *
     * @param inp
     */
    public void set_topo_start_from(String inp) {
        if(ConnectionManager.TopoStartFromColumnExists == 1){
            keyValue.put("TOPO_START_FROM", inp);
        }
    }
    
    /**
     * After an edit we need to force the record to read from the database.
     * @throws java.sql.SQLException
     */
    @Override
    public void forceLoad() throws SQLException {
        needtoadd_tete = true;
        algorithms = null;
        tete = new ArrayList<>();
        ConnectionManager.getInstance().forceLoad(this);
    }

    ///Probably not a good idea, fiddles with numbers in links?

    /**
     *
     * @throws SQLException
     */
    public void renumberAlgorithms() throws SQLException {
        if (algorithms != null) {
            int i = 0;
            for (HLTComponent component : getAlgorithms()) {
                component.set_algorithm_counter(i);
                ++i;
            }
        }
    }

    /**
     * Add components to the tree view.
     * 
     * @param treeNode
     * @param counter 
     * @throws java.sql.SQLException 
     */
    @Override
    public void addToTree(DefaultMutableTreeNode treeNode, int counter) throws SQLException {
        --counter;
        if (counter >= 0) {

            //inputs
            ArrayList<HLTTE_TE> inputs = getInputElements();
            Iterator<HLTTE_TE> inputs_it = inputs.iterator();
            
            if (needtoadd_tete) {
                while (inputs_it.hasNext()) {
                    HLTTE_TE test = inputs_it.next();
                    DefaultMutableTreeNode anotherLayer = new DefaultMutableTreeNode(test);
                    treeNode.add(anotherLayer);
                }
                needtoadd_tete = false;
            }
            
            if (tree_data.isEmpty()) {
                for (HLTComponent comp : getAlgorithms()) {
                    DefaultMutableTreeNode anotherLayer = new DefaultMutableTreeNode(comp);
                    treeNode.add(anotherLayer);
                    tree_data.add(anotherLayer);
                }
            } else {
                int i = 0;
                for (HLTComponent comp : getAlgorithms()) {
                    comp.addToTree(tree_data.get(i), counter);
                    ++i;
                }
            }
        }
    }

    
    /**
     * Make a deep copy of a trigger element.
     * When the user clicks the copy button we copy the class. This contains the
     * code to set all the variables in the new class to the correct values
     * 
     * Don't increment the version this will be done automagically
     * @return 
     */
    @Override
    public Object clone() {
        HLTTriggerElement copy = new HLTTriggerElement();
        copy.set_id(get_id());
        copy.set_name(get_name());
        if(ConnectionManager.TopoStartFromColumnExists == 1){
            copy.set_topo_start_from(get_topo_start_from());
        }
        copy.set_version(get_version());
        for (HLTTE_TE tetelink : getInputElements()) {
            HLTTE_TE newtete = (HLTTE_TE) tetelink.clone();
            copy.getInputElements().add(newtete);
        }
        for (HLTComponent comp : getAlgorithms()) {
            HLTComponent newcomp = (HLTComponent) comp.clone();
            copy.getAlgorithms().add(newcomp);
        }

        return copy;
    }

    /**
     * Make a shallow copy.
     * Creates a new HLT Trigger Elements containing this trigger map but not
     * the children.
     * @return a copy of this object without children/
     */
    public HLTTriggerElement clone_simple() {
        HLTTriggerElement copy = new HLTTriggerElement();
        copy.set_id(this.get_id());
        copy.set_name(get_name());
        copy.set_version(get_version());
        if(ConnectionManager.TopoStartFromColumnExists == 1){
            copy.set_element_counter(get_element_counter());   
        }
        copy.set_topo_start_from(get_topo_start_from());
        return copy;
    }
    
    @Override
    public int doDiff(AbstractTable t, DefaultMutableTreeNode treeNode, java.util.Set<String> linkstoignore) throws SQLException {
        int ndiff = super.doDiff(t, treeNode, linkstoignore);
        
        if (t instanceof HLTTriggerElement) {
            HLTTriggerElement ele = (HLTTriggerElement) t;
            /// compare algos
            ndiff += triggerdb.Diff.CompareTables.compareTables(this.getAlgorithms(), ele.getAlgorithms(), treeNode);
            /// compare input TEs
            ndiff += triggerdb.Diff.CompareTables.compareTables(this.getInputElements(), ele.getInputElements(), treeNode);

            //free memory by dropping links to HLT algos
            this.dropAlgorithms();
            ele.dropAlgorithms();
        }
        
        return ndiff;
    }

    public void set_algorithms(ArrayList<HLTComponent> input){
        algorithms = input;
    }
    
    
    /**
     * Save the trigger element, check input TE and components.
     * Contains the code that saves the record to the database Again, see LVL1
     * for an implementation.
     * 
     * @return 
     * @throws SQLException Stop on SQL problems.
     */
    @Override
    public int save() throws SQLException {

        //logger.fine("In HLT TE Save for TE: " + get_name());
        //logger.info("Num of input TEs = " + getInputElements().size());
        //logger.info("Num of algorithms: " + getAlgorithms().size());

        // Map of comp IDs with their counters - for comparison late
        TreeMap<Integer, Integer> comp_ids = new TreeMap<>();
        for (HLTComponent comp : algorithms) {
            comp_ids.put(comp.get_id(), comp.get_algorithm_counter());
        }
        //NB treemaps are sorted by key, so should be able to compare them!

        //PJB 25/4/08 want a better way to do this - if the name matches, start comparing inputs, 
        //then if the input matches start comparing algorithms
        //also need to do this in types and groups etc!
        int matchingID = -1;
        //FIRST STEP - find list of candidated by matching the name and used

        ArrayList<Integer> hteName_ids = ConnectionManager.getInstance().get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");
        //logger.fine("Number of candidate TEs (with matching name) : " + hteName_ids.size());
        //
        //Now loop over the candidates
        //
        for (Integer iTE : hteName_ids) {

            //FIRST STEP - match the components
            TreeMap<Integer, Integer> compresults = ConnectionManager.getInstance().getConstrainedItemList("HTE2CP_COMPONENT_ID","HTE2CP_ALGORITHM_COUNTER","HLT_TE_TO_CP","HTE2CP_TRIGGER_ELEMENT_ID",iTE);
            if (!compresults.equals(comp_ids)) {
                //logger.fine("PJB different components so going to next candidate");
                continue;
            }

            //SECOND STEP - match the input TEs
            //get one of the candidate TEs with matching name
            HLTTriggerElement matchTETE = new HLTTriggerElement(iTE);
            //logger.fine("PJB potential match has this many input TEs: " + matchTETE.getInputElements().size());
            //if the size is different we can stop here
            if (matchTETE.getInputElements().size() != getInputElements().size()) {
                //logger.fine("PJB different number of inputs so going to next candidate");
                continue;
            }

            //logger.fine("PJB potential match " + iTE + " has same number of inputs (" + getInputElements().size() + ")");
            //if same number of inputs, are they the same ones? loop over inputs and see if the match has them
            //
            ArrayList<String> Inputs_Found = new ArrayList<>();
            for (HLTTE_TE Input : getInputElements()) {
                
                TreeMap<String, Object> fields_values2 = new TreeMap<>();
                fields_values2.put("TE_ID", iTE);
                fields_values2.put("TE_INP_ID", Input.get_element_inp_id());
                fields_values2.put("TE_INP_TYPE", Input.get_element_inp_type());
                fields_values2.put("TE_COUNTER", Input.get_element_counter());
                //ids of TETEs belonging to candidate which match what we are trying to save
                //logger.info("PJB this TE has input " + )
                ArrayList<Integer> te2teIDs = ConnectionManager.getInstance().get_IDs("HLT_TE_TO_TE", "HTE2TE_", fields_values2, null, "ID");
                if (te2teIDs.size() > 0) {
                    Inputs_Found.add(Input.get_element_inp_id());
                }
            }
            //now, TEInputs_Found will have same size as getInputElements if the match is the same!
            if (Inputs_Found.size() != getInputElements().size()) {
                //logger.fine("PJB same number of inputs but not identical so going to next candidate");
                continue;
            }
            Inputs_Found.clear();

            //logger.fine("PJB TE candidate matches exactly: " + iTE);
            //  
            //if we got to here, we have a match so stop
            matchingID = iTE;
            break;
        }
        
        if (matchingID < 0) {
            //logger.fine("Saving new TE");

            int version = 1 + ConnectionManager.getInstance().getMaxVersion(getTableName(), tablePrefix, get_name());
            keyValue.put("VERSION", version);
            set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix, get_id(), keyValue));

            ArrayList<TreeMap<String, Object>> table = new ArrayList<>();
            int algoCounter = 0;
            for (HLTComponent comp : algorithms) {
                TreeMap<String, Object> link = new TreeMap<>();
                link.put("TRIGGER_ELEMENT_ID", get_id());
                link.put("COMPONENT_ID", comp.get_id());
                //link.put("ALGORITHM_COUNTER", comp.get_algorithm_counter());
                link.put("ALGORITHM_COUNTER", algoCounter);
                algoCounter++;
                table.add(link);
                //ConnectionManager.getInstance().save("HLT_TE_TO_CP", "HTE2CP_", -1, link);
            }
            TreeMap<Integer, Integer> results = ConnectionManager.getInstance().saveBatch("HLT_TE_TO_CP", "HTE2CP_", 0, table);

            table.clear();
            for (HLTTE_TE link : tete) {
                link.set_id(-1);
                link.set_trigger_element_id(get_id());
                //logger.fine("PJB going to save TETE with id " + link.get_id() + " for TE id " + link.get_element_inp_id());
                if (link.checkIDs()) {
                    table.add(link.getKey());
                }
                //link.save(); //WPV batch
            }
            TreeMap<Integer, Integer> results1 = ConnectionManager.getInstance().saveBatch("HLT_TE_TO_TE", "HTE2TE_", 0, table);
         
            logger.log(Level.FINE, "[SAVE TE :: new id = {0} ] {1}", new Object[]{get_id(), get_name()});
        } else {
            ////id = hteNameInputAlg_ids.get(0);
            set_id(matchingID);
            //logger.fine("[SAVE TE :: returned id = " + get_id() + " ] " + get_name());
        }

        //logger.fine("TE id as selected by save: " + get_id());

        return get_id();
    }

    /**
     * Set the element counter in the link table. 
     * @param iTE the counter on the link table.
     */
    public void set_element_counter(int iTE) {
        element_counter = iTE;
    }

    /**
     * Get the element counter from the link table.
     * @return The counter form the link table.
     */
    public Integer get_element_counter() {
        return element_counter;
    }

    ///String representation of the object.
    @Override
    public String toString() {
        if (get_id() == -1) {
            return "HLT Trigger Element";
        }
        return "HLT TRIGGER ELEMENT: " + get_name()
                + ", Input TE(s)=" + get_input_trigger_element_names()
                + " (DBid=" + get_id() + ")";
    }

    ///Minimum names for search results.
    @Override
    public ArrayList<String> get_min_names() {
        ArrayList<String> info = new ArrayList<>();
        info.add("ID");
        info.add("Name");
        info.add("Input TE Name(Type)");
        return info;
    }

    ///Minimum info for search result.
    @Override
    public ArrayList<Object> get_min_info() {
        ArrayList<Object> info = new ArrayList<>();
        info.add(get_id());
        info.add(get_name());
        info.add(get_input_trigger_element_names_and_counters());
        return info;
    }

    ///Returns a string of names of all the input trigger elements.

    /**
     *
     * @return
     */
    public String get_input_trigger_element_names() {
        String inp_te_names = "";
        getInputElements();
        Iterator<HLTTE_TE> myIterator = tete.iterator();
        while (myIterator.hasNext()) {
            HLTTE_TE link_TETE = myIterator.next();
            if (inp_te_names.length() == 0) {
                inp_te_names = link_TETE.get_element_inp_id() + "(" + link_TETE.get_element_inp_type() + ")";
            } else {
                inp_te_names = inp_te_names + ", " + link_TETE.get_element_inp_id() + "(" + link_TETE.get_element_inp_type() + ")";
            }
        }
        return inp_te_names;
    }

    ///Returns a string of names of all the input trigger elements also with counter information.

    /**
     *
     * @return
     */
    public String get_input_trigger_element_names_and_counters() {
        String inp_te_names = "";
        getInputElements();
        Iterator<HLTTE_TE> myIterator = tete.iterator();
        while (myIterator.hasNext()) {
            HLTTE_TE link_TETE = (HLTTE_TE) myIterator.next();
            if (inp_te_names.length() == 0) {
                inp_te_names = link_TETE.get_element_counter() + ": " + link_TETE.get_element_inp_id() + "(" + link_TETE.get_element_inp_type() + ")";
            } else {
                inp_te_names = inp_te_names + ", " + link_TETE.get_element_counter() + ": " + link_TETE.get_element_inp_id() + "(" + link_TETE.get_element_inp_type() + ")";
            }
        }
        return inp_te_names;
    }
    
    /**
     *
     * @return
     * @throws SQLException
     */
    public Map<Integer, Integer> getAlgorithmIDsAndCounters() throws SQLException {
        
        Map<Integer, Integer> algoids = new TreeMap<>();
        
        if (get_id() > 0) {
            
            String query = "SELECT HCP_ID, HTE2CP_ALGORITHM_COUNTER FROM HLT_COMPONENT, HLT_TRIGGER_ELEMENT, HLT_TE_TO_CP";
            
            String and = " WHERE "
                    + "HTE_ID=? "
                    + "AND "
                    + "HTE2CP_TRIGGER_ELEMENT_ID=HTE_ID "
                    + "AND "
                    + "HTE2CP_COMPONENT_ID=HCP_ID ";
            
            query = ConnectionManager.getInstance().fix_schema_name(query + and);
            try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query)) {
                ps.setInt(1, get_id());
                try(ResultSet rset = ps.executeQuery()) {
                    while (rset.next()) {
                        Integer id = rset.getInt("HCP_ID");
                        Integer counter = rset.getInt("HTE2CP_ALGORITHM_COUNTER");
                        if (algoids.containsKey(id)) { /// already seen this algo, add the counter
                            logger.severe("This element uses the same algorithm twice, cannot deal with that case");
                        } else {
                            algoids.put(id, counter);
                        }
                    }
                    rset.close();
                    ps.close();
                }
            }
        }//id > 0
        else {
            logger.severe("Tried to get algo IDs when TE does not have an ID");
        }
        return algoids;
        
    }

    /**
     * Function drops the links to the child algorithms.
     * Should allow to control memory use.
     * Still experimental - please use with care.
     */
    public void dropAlgorithms() {
        if (algorithms != null) {
            algorithms.clear();
            algorithms = null;
        }
    }

    /**
     *
     * @return
     */
    public ArrayList<HLTComponent> getAlgorithms() {
        if (algorithms == null || algorithms.isEmpty()) {
            try {
                algorithms = getAlgorithmsFromDb();
            } catch (SQLException ex) {
                logger.severe("Could not load algorithms from db in HLT trigger element");
            }
        }
        
        return algorithms;
    }
    
    /** 
     * get the components (ie the algorithms (and with them their tools) from 
     * the TE-CP link)
     * 
     * @return the list of Algorithms and Tools used by this element.
     * @throws java.sql.SQLException
     */
    private ArrayList<HLTComponent> getAlgorithmsFromDb() throws SQLException {
        ArrayList<HLTComponent> algos = new ArrayList<>();
            
        if (get_id() > 0) {
            String query = new HLTComponent().getQueryString();

            query = query.replace(" FROM ", ", HTE2CP_ALGORITHM_COUNTER FROM "); //11

            query = query.replace("FROM ", "FROM HLT_TRIGGER_ELEMENT, HLT_TE_TO_CP, ");

            String and = " WHERE "
                    + "HTE_ID=? "
                    + "AND "
                    + "HTE2CP_TRIGGER_ELEMENT_ID=HTE_ID "
                    + "AND "
                    + "HTE2CP_COMPONENT_ID=HCP_ID "
                    + "ORDER BY HTE2CP_ALGORITHM_COUNTER ASC";

            query = ConnectionManager.getInstance().fix_schema_name(query + and);

            logger.log(Level.FINEST, "The query {0}", query);

            PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
            ps.setInt(1, get_id());
            ResultSet rset = ps.executeQuery();

            while (rset.next()) {
                HLTComponent comp = new HLTComponent();
                comp.loadFromRset(rset);
                comp.set_algorithm_counter(rset.getInt("HTE2CP_ALGORITHM_COUNTER"));
                algos.add(comp);
            }
	    //System.out.println("algos " + algos.size());
            rset.close();
            ps.close();
        }
        
        return algos;
    }
    
    public ArrayList<HLTComponent> getAlgorithmsFromDbPublic() throws SQLException{
        return getAlgorithmsFromDb();
    }

    /**
     * Load all the input trigger elements.
     * 
     * @return the Vector with all input Elements.
     */
    public ArrayList<HLTTE_TE> getInputElements()  {
	if (tete == null) {
            try {
                tete = loadInputElementsFromDb();
            } catch (SQLException ex) {
                logger.severe("Could not load input elements in HLTTriggerElement");
            }
        }
        return tete;
    }
    
    /**
     *
     * @param link
     */
    public void addTeTeLink(HLTTE_TE link){
        tete.add(link);
    }
    
    private ArrayList<HLTTE_TE> loadInputElementsFromDb() throws SQLException{
        ArrayList<HLTTE_TE> list = new ArrayList<>();

        if (get_id() > 0) {
            String query = "SELECT HTE2TE_ID "
                    + "FROM HLT_TE_TO_TE "
                    + "WHERE HTE2TE_TE_ID=? "
                    + "ORDER BY HTE2TE_TE_COUNTER";

            query = ConnectionManager.getInstance().fix_schema_name(query);
            ArrayList<Integer> ids = new ArrayList<>();

            PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
            ps.setInt(1, get_id());
            ResultSet rset = ps.executeQuery();

            while (rset.next()) {
                int idn = rset.getInt("HTE2TE_ID");
                ids.add(idn);
            }
            rset.close();
            ps.close();

            Iterator<Integer> it = ids.iterator();

            while (it.hasNext()) {
                list.add(new HLTTE_TE(it.next()));
            }   
        }
        
        return list;
    }

    /**
     *
     * @param inputElements
     */
    public void setInputElements(final ArrayList<HLTTE_TE> inputElements) {
        tete = inputElements;
    }
    
    /**
     *
     * @return
     * @throws SQLException
     */
    public Set<HLTTriggerSignature> findParentSignatures() throws SQLException {
        Set<HLTTriggerSignature> sigs = new HashSet<>();
        
        String query = (new HLTTriggerSignature()).getQueryString();
        
        query = query.replace("FROM ", ", HTC2TS_SIGNATURE_COUNTER, HTS2TE_ELEMENT_COUNTER FROM HLT_TRIGGER_ELEMENT, HLT_TS_TO_TE, HLT_TC_TO_TS, ");
        
        String and = " WHERE "
                + "HTE_ID=? "
                + "AND "
                + "HTS2TE_TRIGGER_SIGNATURE_ID=HTS_ID "
                + "AND "
                + "HTS2TE_TRIGGER_ELEMENT_ID=HTE_ID "
                + "ORDER BY HTS2TE_ELEMENT_COUNTER ASC";
        
        query = ConnectionManager.getInstance().fix_schema_name(query + and);
        //logger.info("findParSig query: " + query);
        
        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ps.setInt(1, get_id());
        ResultSet rset = ps.executeQuery();
        while (rset.next()) {
            HLTTriggerSignature sig = new HLTTriggerSignature();
            sig.loadFromRset(rset);
            sig.set_signature_counter(rset.getInt("HTS2TE_ELEMENT_COUNTER"));
            //logger.info("Found sig: " + sig.get_id());
            sigs.add(sig);
        }
        rset.close();
        ps.close();
        
        return sigs;
    }

    /**
     * Sets the given vector as algorithms for this element.
     * @param algos the new algorithm vector.
     */
    public void setAlgorithms(final ArrayList<HLTComponent> algos) {
        algorithms = algos;
    }

    @Override
    public String getTableName() {
        return "HLT_TRIGGER_ELEMENT";
    }
    
         /**
     * Experimental, setting a hashcode representing the whole table
     * Will be used to attempt to speed up saving and recovery of DBs.
     * @return The hashcode
     */
    public Integer get_hash() {
        return (Integer) keyValue.get("HASH");
    }
    ///Set the hash for this prescale

    /**
     *
     */
    public void set_hash() {
        Integer inp = this.hashCode();
        keyValue.put("HASH", inp);
    }
    
    //Explicitly set the hashcode for split parameters

    /**
     *
     * @param hash
     */
    public void set_hash(Integer hash) {
        Integer inp = hash;
        keyValue.put("HASH", inp);
    }
    
    /**
     *
     * @return
     * @throws SQLException
     */
    public ArrayList<Integer> check_hash() throws SQLException {
        ArrayList<Integer> results = new ArrayList();
        String q = "select hte_id from HLT_TRIGGER_ELEMENT where hte_hash = ?";
        q = ConnectionManager.getInstance().fix_schema_name(q);
        try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(q)) {
            ps.setInt(1, this.get_hash());
            try(ResultSet rset = ConnectionManager.executeSelect(ps)) {
                while (rset.next()) {
                    if(rset.getInt("HTE_ID") > 0) {
                        results.add(rset.getInt("HTE_ID"));
                    }
                }
                rset.close();
                ps.close();
            }
        }
        return results;
    }
    
     /*
     * Ok, as we just loaded and double checked all the algorithms, now we do the
     * same for the elements. I will just check that the algorithms match, no need
     * to go down to the parameters?
     */

    /**
     *
     * @param matchElemId
     * @param new_element
     * @param currentAlgos
     * @return
     * @throws SQLException
     */

    public boolean doublecheck(int matchElemId, ArrayList<HLTComponent> currentAlgos, HLTTriggerElement new_element) throws SQLException {
        ArrayList<HLTComponent> returnedAlgos = new ArrayList<>();       
        
        HLTTriggerElement nu = new HLTTriggerElement(matchElemId);
        returnedAlgos.addAll(nu.getAlgorithms());
        
        Boolean algosok = currentAlgos.containsAll(returnedAlgos) && returnedAlgos.containsAll(currentAlgos);
 	ArrayList<HLTTE_TE> newtete = new_element.getInputElements();
 	ArrayList<HLTTE_TE> checktete = nu.getInputElements();
 	algosok &= (newtete.size() == checktete.size());
 	
 	return algosok;
    }
    
        /*
     * Overriding the equals as we changed the hashcode. They should match logic.
     * Possibly needs the algos? But if it does not have an ID then it has no links.
     * Check algos separately??? ^^^^ see above ^^^^
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj instanceof HLTTriggerElement) {
            if (!this.get_name().equals(((HLTTriggerElement) obj).get_name()) || this.hashCode() != ((HLTTriggerElement) obj).hashCode()) {
                return false;
            } else if(ConnectionManager.TopoStartFromColumnExists == 1 && !this.get_topo_start_from().equals(((HLTTriggerElement) obj).get_topo_start_from())){
                return false;
            }
        }
        return true;
    }
    

     /*
     * Hashcode override.
     * This will override the hashcode for the table. It should give a unique hash for the whole parameter.
     */
    @Override
    public int hashCode() {
        int hash;
        long fill = 13;
        fill += 31*this.get_name().hashCode();
        if(ConnectionManager.TopoStartFromColumnExists == 1){
            fill += 37*this.get_topo_start_from().hashCode();
        }
        int j = 1;
        for(HLTComponent comp:this.getAlgorithms()){
            fill += (37+(j*(j+5)))*comp.get_hashfull();
            j++;
        }
        for(HLTTE_TE TE_TE:this.getInputElements()){
            fill += (37+(j*(j+5)))*TE_TE.hashCode(); // check TE, may not be safe
            j++;
        } 

        hash = ((int) fill) ^ ((int) (fill >> 32));
        return hash;
    }
}
