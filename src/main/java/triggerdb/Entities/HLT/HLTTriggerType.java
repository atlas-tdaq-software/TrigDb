package triggerdb.Entities.HLT;

import java.sql.*;
import java.util.ArrayList;
import triggerdb.Entities.AbstractTable;
import triggerdb.Connections.ConnectionManager;

///HLT Trigger Type.
/**
 * HLT Trigger Type object.
 *
 * @author Paul Bell
 * @author Simon Head
 */
public class HLTTriggerType extends AbstractTable {

    ///Chain that this type belongs to.
    private HLTTriggerChain chain = null;

    ///Force the trigger type to be read from the DB.
    @Override
    public void forceLoad() throws SQLException {
        chain = null;
        ConnectionManager.getInstance().forceLoad(this);
    }

    ///Default constructor.

    /**
     *
     */
    public HLTTriggerType() {
        super("HTT_");
        setKeys();
    }

    ///Default constructor.

    /**
     *
     * @param input_id
     * @throws SQLException
     */
    public HLTTriggerType(int input_id) throws SQLException {
        super(input_id, "HTT_");
        setKeys();
        if (input_id > 0) {
            forceLoad();
        }
    }

    private void setKeys() {
        keyValue.remove("NAME");
        keyValue.remove("VERSION");
        keyValue.putFirst("TRIGGER_CHAIN_ID", -1, "Chain ID");
        keyValue.putFirst("TYPEBIT", -1, "Type Bit");
    }
    ///Make a copy of the trigger type.
    /**
     * When the user clicks the copy button we copy the class. This contains the
     * code to set all the variables in the new class to the correct values
     * 
     * Don't increment the version this will be done automagically
     * @return 
     */
    @Override
    public Object clone() {
        HLTTriggerType copy = new HLTTriggerType();
        copy.set_trigger_chain_id(get_trigger_chain_id());
        copy.set_typebit(get_typebit());
        return copy;
    }
        
    @Override
    public boolean equals(Object object){
        if (!(object instanceof HLTTriggerType)){
            return false;
        }
        
        if (this.get_id() != -1 && ((HLTTriggerType)object).get_id() != -1)
        {
            return ((HLTTriggerType)object).get_id() == get_id(); 
        }
        else{
            boolean returnValue = true;
            if (this.get_trigger_chain_id() != -1 && ((HLTTriggerType)object).get_trigger_chain_id() != -1){
                returnValue = this.get_trigger_chain_id().equals(((HLTTriggerType)object).get_trigger_chain_id());
            }
            return returnValue && this.get_typebit().equals(((HLTTriggerType)object).get_typebit());
        }    }
    
    ///Save the trigger type.
    /**
     * Save the Trigger Type.
     * 
     * @return 
     * @throws SQLException Stop on SQL problems.
     */
    @Override
    public int save() throws SQLException {
        ArrayList<Integer> typeIDs = ConnectionManager.getInstance().get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");

        if (typeIDs.isEmpty()) {
            set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix, get_id(), keyValue));
        } else {
            set_id(typeIDs.get(0));
        }

        return get_id();
    }

    ///Get the trigger chain ID.

    /**
     *
     * @return
     */
    public Integer get_trigger_chain_id() {
        return (Integer) keyValue.get("TRIGGER_CHAIN_ID");
    }

    ///Set the trigger chain ID.

    /**
     *
     * @param inp
     */
    public void set_trigger_chain_id(int inp) {
        keyValue.put("TRIGGER_CHAIN_ID", inp);
    }

    public void set_trigger_chain(HLTTriggerChain inp) {
        chain = inp;
    }
    ///Get the trigger chain with ID set in set_trigger_chain_id().

    /**
     *
     * @return
     * @throws SQLException
     */
    public HLTTriggerChain get_trigger_chain() throws SQLException {
        if (chain == null && new HLTTriggerChain().isValid(get_trigger_chain_id())) {
            chain = new HLTTriggerChain(get_trigger_chain_id());
        }

        return chain;
    }

    ///Get the typebit.

    /**
     *
     * @return
     */
    public Integer get_typebit() {
        return (Integer) keyValue.get("TYPEBIT");
    }

    //?Set the typebit.

    /**
     *
     * @param inp
     */
    public void set_typebit(int inp) {
        keyValue.put("TYPEBIT", inp);
    }

    ///String representation of a trigger type.
    @Override
    public String toString() {
        if (get_id() == -1) {
            return "HLT Type";
        }
        return "TRIGGER TYPE: " + get_typebit() + " (DBid=" + get_id() + ")";
        
    }

    ///type has no name in the usual way.
    @Override
    public String get_name() {
        String name = "type " + get_typebit();
        return name;
    }
    
    ///Minimum column names to show in search results.
    @Override
    public ArrayList<String> get_min_names() {
        ArrayList<String> info = new ArrayList<>();
        info.add("ID");
        info.add("Chain ID");
        info.add("Type Bit");
        return info;
    }

    ///Minimum information to show in search results.
    @Override
    public ArrayList<Object> get_min_info() throws SQLException {
        ArrayList<Object> info = new ArrayList<>();
        if (get_id() <= 0) {
            info.add("Filled on save");
        } else {
            info.add(get_id());
        }

        if (get_trigger_chain() == null && get_id() <= 0) {
            info.add("Filled on save");
        } else if (get_trigger_chain() == null) {
            info.add("Invalid");
        } else {
            info.add(get_trigger_chain_id() + " ( " + get_trigger_chain().get_name() + " ) ");
        }

        info.add(get_typebit());
        return info;
    }

    @Override
    public String getTableName() {
        return "HLT_TRIGGER_TYPE";
    }
}