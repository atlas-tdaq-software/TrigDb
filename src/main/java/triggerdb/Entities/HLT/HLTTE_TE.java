package triggerdb.Entities.HLT;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import triggerdb.Entities.AbstractTable;
import triggerdb.Connections.ConnectionManager;

///Input Trigger Elements.
/**
 * This table contains the TE-TE info.  This is not really a link table, but is 
 * equivalent in the schema to the groups and types tables
 */
public class HLTTE_TE extends AbstractTable {


    ///Default constructor.

    /**
     *
     */
    public HLTTE_TE() {
        super("HTE2TE_");
        DefineKeyValuePairs();
    }
    
    /**
     *
     * @param input_id
     * @throws SQLException
     */
    public HLTTE_TE(int input_id) throws SQLException {
        super(input_id, "HTE2TE_");
        DefineKeyValuePairs();
        if (input_id > 0) {
            forceLoad();
        }
    }
    
    private void DefineKeyValuePairs() {
        keyValue.remove("NAME");
        keyValue.remove("VERSION");
        keyValue.putFirst("TE_ID", -1, "TE ID");
        keyValue.putFirst("TE_INP_ID", "", "Input Name");
        keyValue.putFirst("TE_INP_TYPE", "", "Input Type");
        keyValue.putFirst("TE_COUNTER", -1, "Input Counter");
    }

    ///Create a copy of this record.
    /**
     * When the user clicks the copy button we copy the class.  This contains the code to 
     * set all the variables in the new class to the correct values
     * 
     * Don't increment the version this will be done automatically
     * @return 
     */
    @Override
    public Object clone() {
        HLTTE_TE copy = null;
        try {
            copy = new HLTTE_TE(get_id());
        } catch (SQLException ex) {
            Logger.getLogger(HLTTE_TE.class.getName()).log(Level.SEVERE, null, ex);
        }
        copy.set_trigger_element_id(get_trigger_element_id());
        copy.set_element_inp_id(get_element_inp_id());
        copy.set_element_inp_type(get_element_inp_type());
        copy.set_element_counter(get_element_counter());
        return copy;
    }

    ///Save this record.  TE-TE is the final table, so save is simple.
    /** 
     * Save the TE_TE.
     * 
     * @return 
     * @throws SQLException Stop on SQL problems.
     */
    @Override
    public int save() throws SQLException {
       
        ArrayList<Integer> TETEIDs = ConnectionManager.getInstance().get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");

        logger.log(Level.FINE, "PJB in save of TETE {0}", TETEIDs.size());
        
        if (TETEIDs.isEmpty()) {
            logger.log(Level.FINE, "PJB now saving TETE with id {0}", get_id());
            set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix, get_id(), keyValue)); 
        } else {
            set_id(TETEIDs.get(0));
        }

        return get_id();
    }

    /**
     *
     * @return
     * @throws SQLException
     */
    public boolean checkIDs() throws SQLException{
          ArrayList<Integer> TETEIDs = ConnectionManager.getInstance().get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");
          return TETEIDs.isEmpty();
    }
    
    /**
     *
     * @return
     */
    public TreeMap<String,Object> getKey(){
        return keyValue;
    }
    
    ///String representation of this object.
    @Override
    public String toString() {
        if (get_id() == -1) {
            return "HLT TE-TE";
        }
        return "HLT TE-TE: Input Name=" + get_element_inp_id() + " (DBid=" + get_id() + ")";
    }

    ///Minimum names for Search Results.
    @Override
    public ArrayList<String> get_min_names() {
        ArrayList<String> info = new ArrayList<>();
        info.add("ID");
        info.add("Input TE Name");
        info.add("Input TE Counter");
        info.add("Input TE Type");
        return info;
    }

    ///Minimum information for search results.
    @Override
    public ArrayList<Object> get_min_info() {
        ArrayList<Object> info = new ArrayList<>();
        info.add(get_id());
        info.add(get_element_inp_id());
        info.add(get_element_counter());
        info.add(get_element_inp_type());
        return info;
    }

    ///has no name in the usual way.
    @Override
    public String get_name() {
        String name = "input TE " + get_element_inp_id();
        return name;
    }
    
    ///Trigger Element counter. Special meaning for some values?

    /**
     *
     * @return
     */
    public Integer get_element_counter() {
        return (Integer) keyValue.get("TE_COUNTER");
    }
    ///Set the trigger element counter, special meaning for some values?

    /**
     *
     * @param inp
     */
    public void set_element_counter(int inp) {
        keyValue.put("TE_COUNTER", inp);
    }

    ///Get the Input Trigger Element name.

    /**
     *
     * @return
     */
    public String get_element_inp_id() {
        return (String) keyValue.get("TE_INP_ID");
    }
    ///Set the Input Trigger Element name.

    /**
     *
     * @param inp
     */
    public void set_element_inp_id(String inp) {
        keyValue.put("TE_INP_ID", inp);
    }

    ///Get the Trigger Element ID.

    /**
     *
     * @return
     */
    public Integer get_trigger_element_id() {
        return (Integer) keyValue.get("TE_ID");
    }
    ///Set the Trigger Element ID.

    /**
     *
     * @param inp
     */
    public void set_trigger_element_id(int inp) {
        keyValue.put("TE_ID", inp);
    }

    ///Set the input TE type.

    /**
     *
     * @param inp
     */
    public void set_element_inp_type(String inp) {
        keyValue.put("TE_INP_TYPE", inp);
    }
    ///Get the input TE type.

    /**
     *
     * @return
     */
    public String get_element_inp_type() {
        return (String) keyValue.get("TE_INP_TYPE");
    }

    @Override
    public String getTableName() {
        return "HLT_TE_TO_TE";
   }
    
    @Override
     /*
     * Hashcode override.
     * This will override the hashcode for the table. It should give a unique hash for the whole parameter.
     */
    public int hashCode() {
        int hash;
        long fill = 13;
        fill = fill+31*this.get_trigger_element_id() //integer - do not hash!
                +37*this.get_element_inp_id().hashCode() //string: ok
                +41*this.get_element_inp_type().hashCode() //string: ok
                +43*this.get_element_counter(); //integer - do not hash!
        
        hash = ((int) fill) ^ ((int) (fill >> 32));
        return hash;
    }
}