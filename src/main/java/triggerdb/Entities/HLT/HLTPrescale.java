package triggerdb.Entities.HLT;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.TreeMap;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;

/**
 * Class to hold a HLT Prescales and Pass Throughs.
 * 
 * @author Paul Bell
 * @author Simon Head
 */
public final class HLTPrescale extends AbstractTable {

    /** Default express stream prescale value. */
    /** A link to the prescale set this prescale belongs to. */
    private HLTPrescaleSet prescaleSet = null;
    
    /**
     *
     */
    public static final Double DefaultPrescaleValue = 1.0;

    /**
     *
     */
    public HLTPrescale() {
        super("HPR_");
        DefineKeyValuePairs();
    }
    
    /**
     * Standard constructor.  If an ID != -1 is given then we load that record
     * from the database, otherwise a new (null) object is created.
     * 
     * @param input_id Row in Database or -1 to create new.
     * @throws java.sql.SQLException
     */
    public HLTPrescale(final int input_id) throws SQLException {
        super(input_id, "HPR_");
        DefineKeyValuePairs();        

        if (input_id > 0) {
            forceLoad();
        }
    }

    private void DefineKeyValuePairs() {
        keyValue.remove("NAME");
        keyValue.remove("VERSION");
        keyValue.putFirst("PRESCALE_SET_ID", -1, "Prescale Set ID");
        keyValue.putFirst("VALUE", 0.0, "Prescale value");
        keyValue.putFirst("CHAIN_COUNTER", -1, "Chain Counter");
        keyValue.putFirst("TYPE", "NotSet", "Type");
        keyValue.putFirst("CONDITION", "", "Condition for rerun and stream types");
        keyValue.putFirst("HASH", -1, "HashCode for the table");
    }

    /**
     *  Prescales can be deletes. This deletes this row. Not sure we use this now - PJB.
     */
//    public void delete() {
//            String query = "DELETE FROM " + tableName + " WHERE " + tablePrefix + "ID=?";
//            query = ConnectionManager.getInstance().fix_schema_name(query);
//            try {
//                PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
//                ps.setInt(1, get_id());
//                ps.executeUpdate();
//                ps.close();
//            } catch (SQLException ex) {
//                logger.warning("DELETE FAILED: " + query);
//            }
//    }

    /** 
     * Get the prescale set ID.
     * @return the id of the parent set.
     */
     public Integer get_prescale_set_id() {
        return (Integer) keyValue.get("PRESCALE_SET_ID");
    }

    /**
     * Set the prescale set ID.
     * 
     * @param inp The id of the parent set.
     */
    public void set_prescale_set_id(final int inp) {
        keyValue.put("PRESCALE_SET_ID", inp);
    }

    /**
     * Set the chain counter.
     * 
     * @param inp The chain counter.
     */
    public void set_chain_counter(final int inp) {
        keyValue.put("CHAIN_COUNTER", inp);
    }

    /** 
     * Get chain counter.
     * @return The chain counter.
     */
    public Integer get_chain_counter() {
        return (Integer) keyValue.get("CHAIN_COUNTER");
    }

    /**
     * Get the value.
     * 
     * @return The prescale value
     */
    public Double get_value() {
        return (Double) keyValue.get("VALUE");
    }

    /**
     * Set the prescale value.
     * @param value
     */
    public void set_value(final double value) {
        keyValue.put("VALUE", value);
    }

    /**
     * Get the type of the prescale. 
     * @return rerun, prescale, pass through, stream
     */
    public HLTPrescaleType get_type() {
        return HLTPrescaleType.valueOf(keyValue.get("TYPE").toString());
    }

    /**
     *
     * @param inp
     */
    public void set_type(HLTPrescaleType inp) {
        keyValue.put("TYPE", inp.toString());
    }

    /**
     * Get the condition of the prescale. 
     * @return stream or 
     */
    public String get_condition() {
        return (String) keyValue.get("CONDITION");
    }

    /**
     *
     * @param inp
     */
    public void set_condition(String inp) {
        keyValue.put("CONDITION", inp);
    }
    
     /**
     * Experimental, setting a hashcode representing the whole table
     * Will be used to attempt to speed up saving and recovery of DBs.
     * @return The hashcode
     */
    public Integer get_hash() {
        return (Integer) keyValue.get("HASH");
    }
    ///Set this prescale for an L2 or EF chain.

    /**
     *
     */
    public void set_hash() {
        Integer inp = this.hashCode();
        keyValue.put("HASH", inp);
        //System.out.println("Save " + inp);
    }

    ///String representation of this prescale.
    @Override
    public String toString() {
        if (get_id() == -1) {
            return "HLT Prescale/PassThrough";
        }

        return "HLT Prescale value = " + get_value() + " and type " + get_type();
    }
      
    /**
     *
     * @return
     */
    public TreeMap<String,Object> getKey(){
        return keyValue;
    }
    ///Create a copy of this prescale.
    /**
     * When the user clicks the copy button we copy the class. This contains the
     * code to set all the variables in the new class to the correct values
     * @return 
     */
    @Override
    public Object clone() {
        HLTPrescale copy = new HLTPrescale();
        copy.set_value(get_value());
        copy.set_chain_counter(get_chain_counter());
        copy.set_type(get_type());
        copy.set_prescale_set_id(get_prescale_set_id());
        return copy;
    }

    /**
     * Contains the code that saves the record to the database.
     * Save this prescale, should be called by prescale set only.
     * 
     * @return 
     * @throws SQLException Stop on SQL problem.
     */
    @Override
    public int save() throws SQLException {
        //logger.info("In HLT Prescale Save");
        //logger.info("HLT PS key values: " + keyValue.toString());
        
        //Vector<Integer> hpr_ids = ConnectionManager.getInstance().get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");
        //ArrayList<Integer> hpr_ids2 = ConnectionManager.getInstance().get_hashIDs(getTableName(), tablePrefix, this.hashCode(), false);
        
        //if (hpr_ids2.isEmpty()) {
            set_hash();
            set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix, get_id(), keyValue));
        /*} else {
            set_id(hpr_ids2.get(0));
        }*/

        return get_id();
    }

    ///Get the minimum names for the seach results.
    @Override
    public ArrayList<String> get_min_names() {
        ArrayList<String> info = new ArrayList<>();
        info.add("ID");
        info.add("Type");
        info.add("Prescale");
        info.add("Pass-through");
        info.add("Chain Counter");
        return info;
    }

    ///Get the minimum information for the seach results.
    @Override
    public ArrayList<Object> get_min_info() {
        ArrayList<Object> info = new ArrayList<>();
        info.add(get_id());
        info.add(get_type());
        info.add(get_value());
        info.add(get_condition());
        info.add(get_chain_counter().toString());
        return info;
    }

    ///Get the prescale set that owns this prescale.

    /**
     *
     * @return
     * @throws SQLException
     */
    public HLTPrescaleSet get_prescale_set() throws SQLException {
        if (prescaleSet == null && new HLTPrescaleSet().isValid(get_prescale_set_id())) {
            prescaleSet = new HLTPrescaleSet(get_prescale_set_id());
        }

        return prescaleSet;
    }

    @Override
    public int doDiff(AbstractTable t, javax.swing.tree.DefaultMutableTreeNode treeNode, java.util.Set<String> linkstoignore) throws SQLException {
        if (linkstoignore == null) {
            linkstoignore = new java.util.HashSet<>();
        }         
        linkstoignore.add("PRESCALE_SET_ID");

        int ndiff = super.doDiff(t, treeNode, linkstoignore);

        return ndiff;
    }

    ///Get the chain name, given a menu, for this prescale.
    /**
     * Get the chain name that is associated with this Prescale, when you provide
     * the menu that this belongs to.  Probably we don't even need to pass it a
     * menu, since a prescale can only belong to one prescale set. And that prescale
     * set can only belong to one menu. So we could get the information this
     * way instead. NO - ps sets can go with many menus!
     * 
     * @param menu The menu that this prescale belongs to.
     * @return The Chain name.
     * @throws java.sql.SQLException
     */
    public String get_chain_name(HLTTriggerMenu menu) throws SQLException {
        String chain_name = "No Matching Chain";

        for (HLTTriggerChain chain : menu.getChains()) {
            int chain_cnter = chain.get_chain_counter();
            if (chain_cnter == get_chain_counter()) {
                chain_name = chain.get_name();
            }
        }
        return chain_name;
    }

    ///Get the chain l1 inputs, given a menu, for this prescale.

    /**
     *
     * @param menu
     * @return
     * @throws SQLException
     */
    public String get_chain_seed(HLTTriggerMenu menu) throws SQLException {
        String seed = "No Matching Chain";

        for (HLTTriggerChain chain : menu.getChains()) {
            int chain_cnter = chain.get_chain_counter();

            if (chain_cnter == get_chain_counter()) {
                seed = chain.get_lower_chain_name();
            }
        }
        return seed;
    }

    @Override
    public String getTableName() {
        return "HLT_PRESCALE";
    }
    
    /**
     * As we overrode the hashcode need to make the equals match....
     * @param obj
     * @return 
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj instanceof HLTPrescale) {
            if (!this.get_chain_counter().equals(((HLTPrescale) obj).get_chain_counter())) {
                return false;
            } if (!this.get_type().equals(((HLTPrescale) obj).get_type())) {
                return false;
            } if (!this.get_value().equals(((HLTPrescale) obj).get_value())) {
                return false;
            } return this.get_condition().equals(((HLTPrescale) obj).get_condition());
        } else {
            return false;
        }
    }
    
     /**
     * Should hopefully provide a `unique' hashcode per prescale.
     * @return 
     */
    @Override
    public int hashCode () {
        long fill = 13;
        fill = fill+31*this.get_chain_counter() // integer: do not hash
               +37*this.get_type().ordinal() //enum: do not hash
               +41*this.get_value().hashCode() //double: ok - although not entirely sure hashing is sensible
               +43*this.get_condition().hashCode(); //string: ok
        
        return ((int) fill) ^ ((int) (fill >> 32));
    }
}