package triggerdb.Entities.HLT;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;
import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.logging.Level;
import javax.swing.tree.DefaultMutableTreeNode;

import triggerdb.Entities.AbstractTable;
import triggerdb.Connections.ConnectionManager;
import triggerdb.DiffMutableTreeNode;

/**
 * Class representing the component table. HLT Software Component object. Has
 * some clever methods for when a component has children records (which can be
 * parameters or other components!)
 *
 * @author Paul Bell
 * @author Simon Head
 */
public final class HLTComponent extends AbstractTable {

    /**
     * Vector of child parameter records.
     */
    private ArrayList<HLTParameter> parameters = null;
    /**
     * Vector of all child component records.
     */
    private ArrayList<HLTComponent> allchildcomponents = null;
    /**
     * Vector of all child component ID.
     */
    private ArrayList<Integer> allchildcomponentIDs = null;
    /**
     * Vector of direct child component records.
     */
    private ArrayList<HLTComponent> childcomponents = null;
    /**
     * Special mode for one of the panels.
     */
    private boolean list_mode = false;
    /**
     * Counter is held in link table, so not part of the map.
     */
    private Integer algorithm_counter = -1;
    /**
     * So the tree knows what it has loaded.
     */
    private ArrayList<DefaultMutableTreeNode> tree_data_comp = new ArrayList<>();
    private ArrayList<DefaultMutableTreeNode> tree_data_para = new ArrayList<>();
    private boolean isChild = false; //for drawing

    /**
     * Normal constructor. Tell it to load a specific ID, or set the ID=-1 if
     * you wish to create a new record.
     */
    public HLTComponent() {
        super("HCP_");
        setKeys();
    }

    /**
     *
     * @param input_id
     * @throws SQLException
     */
    public HLTComponent(final int input_id) throws SQLException {
        super(input_id, "HCP_");
        setKeys();
        if (input_id > 0) {
            forceLoad();
        }

    }

    private void setKeys() {
        keyValue.putFirst("HASH", -1, "HashCode for the table");
        keyValue.putFirst("HASHFULL", -1, "HashCodeFull for the table and the linked parameters");
        keyValue.putFirst("ALIAS", "Alias", "Alias");
        keyValue.putFirst("TYPE", "Type", "Type");
        keyValue.putFirst("PY_NAME", "Python Name", "Python Name");
        keyValue.putFirst("PY_PACKAGE", "Python Package", "Python Package");
    }

    /**
     * Force the data to be read from the DB again.
     *
     * @throws java.sql.SQLException
     */
    @Override
    public void forceLoad() throws SQLException {
        parameters = null;
        allchildcomponents = null;
        allchildcomponentIDs = null;
        tree_data_comp = new ArrayList<>();
        tree_data_para = new ArrayList<>();
        ConnectionManager.getInstance().forceLoad(this);
    }

    ///Add to the tree.
    /**
     * It's used when the user clicks on the tree to expand it. We have to keep
     * count of the child records we add.
     *
     * @param treeNode The node to add this to.
     * @param counter Used so that we only load one level more than the user can
     * see, and don't load the entire tree.
     * @throws java.sql.SQLException
     */
    @Override
    public void addToTree(DefaultMutableTreeNode treeNode, int counter) throws SQLException {
        --counter;

        if (counter >= 0) {

            if (tree_data_para.isEmpty()) {
                for (HLTParameter param : getParameters()) {
                    DefaultMutableTreeNode anotherLayer = new DefaultMutableTreeNode(param);
                    treeNode.add(anotherLayer);
                    tree_data_para.add(anotherLayer);
                }
            } else {
                int i = 0;
                for (HLTParameter param : getParameters()) {
                    param.addToTree(tree_data_para.get(i), counter);
                    ++i;
                }
            }

            if (tree_data_comp.isEmpty()) {
                for (HLTComponent childcomp : getChildComponents()) {
                    DefaultMutableTreeNode anotherLayer = new DefaultMutableTreeNode(childcomp);
                    treeNode.add(anotherLayer);
                    tree_data_comp.add(anotherLayer);
                }
            } else {
                int i = 0;
                for (HLTComponent childcomp : getChildComponents()) {
                    childcomp.addToTree(tree_data_comp.get(i), counter);
                    ++i;
                }
            }
        }
    }

    @Override
    public int doDiff(AbstractTable t, DefaultMutableTreeNode treeNode, Set<String> linkstoignore) throws SQLException {
        int ndiff = super.doDiff(t, treeNode, linkstoignore);

        if (t instanceof HLTComponent) {
            HLTComponent comp = (HLTComponent) t;

            /// Check algorithm counter
            if (!this.get_algorithm_counter().equals(comp.get_algorithm_counter())) {
                ndiff++;
                treeNode.add(new DiffMutableTreeNode("Algorithm Counter", this.get_algorithm_counter().toString(), comp.get_algorithm_counter().toString()));
            }

            /// Look for parameter changes           
            ndiff += triggerdb.Diff.CompareTables.compareTables(this.getParameters(), comp.getParameters(), treeNode);

            /// Look for child component changes
            ndiff += triggerdb.Diff.CompareTables.compareTables(this.getChildComponents(), comp.getChildComponents(), treeNode);

        }

        return ndiff;
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj instanceof HLTComponent) {
            final HLTComponent other = (HLTComponent) obj;
            return this.hashCode() == other.hashCode();
        } else {
            return false;
        }
    }

    @Override
    /*
     * Hashcode override.
     * There are two hashes here, both will give a different `unique' hash.
     * Need to pick one and stick with it. Not sure which will reduce the chance
     * of overlap the most?
     */
    public int hashCode() {
        long fill = 13;
        fill = fill + 31 * this.get_name().hashCode() //string: ok
                + 37 * this.get_alias().hashCode() //string: ok
                + 41 * this.get_type().hashCode() //string: ok
                + 43 * this.get_py_name().hashCode() //string: ok
                + 47 * this.get_py_package().hashCode(); //string: ok

        return ((int) fill) ^ ((int) (fill >> 32));
    }

    /**
     * Create a copy of this object. When the user clicks the copy button we
     * copy the class. This contains the code to set all the variables in the
     * new class to the correct values
     *
     * Don't increment the version this will be done automatically
     *
     * @return
     */
    @Override
    public Object clone() {
        HLTComponent copy = new HLTComponent();
        copy.set_id(this.get_id());
        copy.set_alias(this.get_alias());
        copy.set_py_name(this.get_py_name());
        copy.set_py_package(this.get_py_package());
        copy.set_type(this.get_type());
        //copy.set_flag(get_flag());
        copy.set_name(this.get_name());
        copy.set_version(this.get_version());
        copy.set_algorithm_counter(get_algorithm_counter());
        copy.set_hash();
        return copy;
    }

    /**
     * Save this to the DB. Have to check all the parameters are the same too.
     * Contains the code that saves the record to the database. Have to check
     * that the component points parameters that already exist, else make the
     * links.
     *
     * @return
     * @throws SQLException Stop on SQL problems.
     */
    @Override
    public int save() throws SQLException {
        int matchingID = -1;
        //sorted vector of parameters to compare candidates to
        ArrayList<Integer> parameterSortedIds_vec = getParameterIDs();
        Collections.sort(parameterSortedIds_vec);

        //sorted vector of linked components to compare candidates to
        ArrayList<Integer> componentSortedIds_vec = new ArrayList<>();
        for (HLTComponent comp : getChildComponents()) {
            componentSortedIds_vec.add(comp.get_id());
        }
        Collections.sort(componentSortedIds_vec);

        //step 0: look for candidates with same name, alias, type, py_name and py_package
        ConnectionManager cm = ConnectionManager.getInstance();
        List<Integer> candidate_ids = cm.get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");

        //Now loop over the candidates
        for (Integer comp_id : candidate_ids) {

            //step 1: match parameters
            ArrayList<Integer> component_par_IDs = cm.get_ItemList("HLT_CP_TO_PA", "HCP2PA", "HCP2PA_COMPONENT", "HCP2PA_PARAMETER", comp_id);
            Collections.sort(component_par_IDs);
            if (!parameterSortedIds_vec.equals(component_par_IDs)) {
                continue;
            }

            //step 2: see if candidates have same linked components!
            //logger.info("Component candidate has same parameters so looking at linked comps");
            ArrayList<Integer> component_par_comp_IDs = cm.get_ItemList("HLT_CP_TO_CP", "HCP2CP", "HCP2CP_PARENT_COMP", "HCP2CP_CHILD_COMP", comp_id);
            Collections.sort(component_par_comp_IDs);
            if (!componentSortedIds_vec.equals(component_par_comp_IDs)) {
                continue;
            }

            //if we got to here, we have a match so stop
            matchingID = comp_id;
            break;
        }

        //save new one if required
        if (matchingID < 0) {

            //version must depend on name and alias!
            int version = 1 + ConnectionManager.getInstance().getMaxCompVersion(get_name(), get_alias());

            keyValue.put("VERSION", version);
            set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix, get_id(), keyValue));

            //save parameters
            ArrayList<TreeMap<String, Object>> table = new ArrayList<>();

            ArrayList<Integer> paramIDs = getParameterIDs();
            for (Integer paramID : paramIDs) {
                TreeMap<String, Object> link = new TreeMap<>();
                link.put("COMPONENT_ID", get_id());
                link.put("PARAMETER_ID", paramID);
                table.add(link);
                //ConnectionManager.getInstance().save("HLT_CP_TO_PA", "HCP2PA_", -1, link);
            }

            TreeMap<Integer, Integer> results = ConnectionManager.getInstance().saveBatch("HLT_CP_TO_PA", "HCP2PA_", 0, table);

            //save linked components
            table.clear();
            for (HLTComponent comp : getChildComponents()) {
                TreeMap<String, Object> link2 = new TreeMap<>();
                link2.put("PARENT_COMP_ID", get_id());
                link2.put("CHILD_COMP_ID", comp.get_id());
                table.add(link2);
                //ConnectionManager.getInstance().save("HLT_CP_TO_CP", "HCP2CP_", -1, link2);
            }
            TreeMap<Integer, Integer> results1 = ConnectionManager.getInstance().saveBatch("HLT_CP_TO_CP", "HCP2CP_", 0, table);

        } else {
            set_id(matchingID);
        }

        return get_id();

    }

    /**
     * Save this to the DB. Have to check all the parameters are the same too.
     * Contains the code that saves the record to the database. Have to check
     * that the component points parameters that already exist, else make the
     * links. This should be more compact in terms of trips to the DB as it
     * combines multiple queries into one.
     *
     * @return
     * @throws SQLException Stop on SQL problems.
     */
    public int compactsave() throws SQLException {
        int matchingID = -1;

        logger.log(Level.FINER, "Alias: {0}, type: {1} {2}", new Object[]{get_alias(), get_type(), get_py_package()});
        logger.log(Level.FINEST, "Num of linked cps = {0}", getChildComponents().size());
        logger.log(Level.FINEST, "Num of linked pas: {0}", getParameters().size());

        //sorted vector of parameters to compare candidates to
        ArrayList<Integer> parameterSortedIds_vec = getParameterIDs();
        Collections.sort(parameterSortedIds_vec);

        //sorted vector of linked components to compare candidates to
        ArrayList<Integer> componentSortedIds_vec = new ArrayList<>();
        for (HLTComponent comp : getChildComponents()) {
            componentSortedIds_vec.add(comp.get_id());
        }
        Collections.sort(componentSortedIds_vec);

        //step 0: look for candidates with same name, alias, type, py_name and py_package
        ConnectionManager cm = ConnectionManager.getInstance();
        //List<Integer> candidate_ids = cm.get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");
        ArrayList<Integer> candidate_ids = cm.get_hashIDs(getTableName(), tablePrefix, this.get_hashfull(), true);
        ArrayList<int[]> holderParams = null;
        ArrayList<int[]> holderComps = null;

        if (candidate_ids.size() > 0) {
            holderParams = cm.get_CompactItemList("HLT_CP_TO_PA", "HCP2PA", "HCP2PA_COMPONENT", "HCP2PA_PARAMETER", candidate_ids);
            holderComps = cm.get_CompactItemList("HLT_CP_TO_CP", "HCP2CP", "HCP2CP_PARENT_COMP", "HCP2CP_CHILD_COMP", candidate_ids);
        }

        for (Integer comp_id : candidate_ids) {
            //Check name, alias and type
            HLTComponent comp = new HLTComponent(comp_id);
            if (!comp.get_name().equals(this.get_name())) {
                continue;
            }
            if (!comp.get_alias().equals(this.get_alias())) {
                continue;
            }
            if (!comp.get_type().equals(this.get_type())) {
                continue;
            }

            ArrayList<Integer> component_par_IDs = new ArrayList<>();
            ArrayList<Integer> component_par_comp_IDs = new ArrayList<>();
            for (int[] i : holderParams) {
                if (i[0] == comp_id) {
                    component_par_IDs.add(i[1]);
                }
            }
            Collections.sort(component_par_IDs);
            if (!parameterSortedIds_vec.equals(component_par_IDs)) {
                continue;
            }
            for (int[] i : holderComps) {
                if (i[0] == comp_id) {
                    component_par_comp_IDs.add(i[1]);
                }
            }
            Collections.sort(component_par_comp_IDs);
            if (!componentSortedIds_vec.equals(component_par_comp_IDs)) {
                continue;
            }

            matchingID = comp_id;
            break;
        }

        //save new one if required
        if (matchingID < 0) {

            //version must depend on name and alias!
            int version = 1 + ConnectionManager.getInstance().getMaxCompVersion(get_name(), get_alias());

            keyValue.put("VERSION", version);
            set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix, get_id(), keyValue));

            //save parameters
            ArrayList<TreeMap<String, Object>> table = new ArrayList<>();

            //save parameters
            ArrayList<Integer> paramIDs = getParameterIDs();
            for (Integer paramID : paramIDs) {
                TreeMap<String, Object> link = new TreeMap<>();
                link.put("COMPONENT_ID", get_id());
                link.put("PARAMETER_ID", paramID);
                table.add(link);
                //ConnectionManager.getInstance().save("HLT_CP_TO_PA", "HCP2PA_", -1, link);
            }

            TreeMap<Integer, Integer> results = ConnectionManager.getInstance().saveBatch("HLT_CP_TO_PA", "HCP2PA_", 0, table);

            //save linked components
            table.clear();
            for (HLTComponent comp : getChildComponents()) {
                TreeMap<String, Object> link2 = new TreeMap<>();
                link2.put("PARENT_COMP_ID", get_id());
                link2.put("CHILD_COMP_ID", comp.get_id());
                //System.out.println("batch pid: " + get_id() + " cid " + comp.get_id());
                table.add(link2);
                //ConnectionManager.getInstance().save("HLT_CP_TO_CP", "HCP2CP_", -1, link2);
            }
            TreeMap<Integer, Integer> results1 = ConnectionManager.getInstance().saveBatch("HLT_CP_TO_CP", "HCP2CP_", 0, table);
            logger.log(Level.FINE, "[SAVE COMP :: new id = {0} ] {1}", new Object[]{get_id(), get_name()});
        } else {
            set_id(matchingID);
        }

        return get_id();

    }

    /**
     * Get the alias from this r
     *
     * @return ecord.
     */
    public String get_alias() {
        return (String) keyValue.get("ALIAS");
    }

    /**
     * Set the alias in this record.
     *
     * @param alias new alias.
     */
    public void set_alias(final String alias) {
        keyValue.put("ALIAS", alias);
    }

    /**
     * Get the alias from this record.
     *
     * @return
     */
    public String get_py_package() {
        return (String) keyValue.get("PY_PACKAGE");
    }

    /**
     * Set the python package.
     *
     * @param pyPackage the new package name.
     */
    public void set_py_package(final String pyPackage) {
        keyValue.put("PY_PACKAGE", pyPackage);
    }

    /**
     * Get the python name from this record.
     *
     * @return
     */
    public String get_py_name() {
        return (String) keyValue.get("PY_NAME");
    }

    /**
     * set the python name for this component.
     *
     * @param pyName the new name.
     */
    public void set_py_name(final String pyName) {
        keyValue.put("PY_NAME", pyName);
    }

    ///Set the algorithm counter (link table).

    /**
     *
     * @param alg_counter
     */
    public void set_algorithm_counter(int alg_counter) {
        algorithm_counter = alg_counter;
    }

    /*
     * Returns the algorithm counter
     */

    /**
     *
     * @return
     */

    public Integer get_algorithm_counter() {
        return algorithm_counter;
    }

    /**
     * Get the type (algo, tool, service, etc)
     *
     * @return The type as a string
     */
    public String get_type() {
        return (String) keyValue.get("TYPE");
    }

    /**
     *
     * @return
     */
    public boolean isChild() {
        return (boolean) isChild;
    }

    /**
     *
     * @param setIsChild
     */
    public void setIsChild(boolean setIsChild) {
        isChild = setIsChild;
    }

    /**
     * Set to list mode, which changes how some functions work. I forgot
     * which...
     *
     * @param b Set true or false for list mode.
     */
    public void set_list_mode(final boolean b) {
        list_mode = b;
    }

    public boolean get_list_mode(){
        return list_mode;
    }
    /**
     * Set the type
     *
     * @param inp Put this value as the new type value.
     */
    public void set_type(final String inp) {
        keyValue.put("TYPE", inp);
    }

    /**
     * Experimental, setting a hashcode representing the whole table Will be
     * used to attempt to speed up saving and recovery of DBs.
     *
     * @return The hashcode
     */
    public Integer get_hash() {
        return (Integer) keyValue.get("HASH");
    }
    ///Set this prescale for an L2 or EF chain.

    /**
     *
     */
    public void set_hash() {
        Integer inp = this.hashCode();
        keyValue.put("HASH", inp);
    }

    /**
     * Experimental, setting a hashcode representing the whole table Will be
     * used to attempt to speed up saving and recovery of DBs.
     *
     * @return The hashcode
     */
    public Integer get_hashfull() {
        return (Integer) keyValue.get("HASHFULL");
    }
    ///Set this prescale for an L2 or EF chain.

    /**
     *
     * @param hashes
     */
    public void set_hashfull(ArrayList<Integer> hashes) {
        long fill = 13;
        int j = 1;
        Collections.sort(hashes);
        for (Integer i : hashes) {
            fill += (31 + (j * (j + 5))) * i;
            j++;
        }
        Integer inp = ((int) fill) ^ ((int) (fill >> 32));
        keyValue.put("HASHFULL", inp);
    }

    /**
     *
     * @param hash
     */
    public void set_hashfull(Integer hash) {
        keyValue.put("HASHFULL", hash);
    }

    /**
     *
     * @return
     * @throws SQLException
     */
    public int check_hashfull() throws SQLException {
        int there = -1;
        String q = "select hcp_id from HLT_COMPONENT where hcp_hashfull = ?";
        q = ConnectionManager.getInstance().fix_schema_name(q);
        try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(q)) {
            ps.setInt(1, this.get_hashfull());
            ResultSet rset = ConnectionManager.executeSelect(ps);
            while (rset.next()) {
                if (rset.getInt("HCP_ID") > 0) {
                    there = rset.getInt("HCP_ID");
                }
            }
            rset.close();
            ps.close();
        }
        return there;
    }

    /**
     *
     * @param CompId
     * @param currentComps
     * @param currentParas
     * @return
     * @throws SQLException
     */
    public boolean doublecheck(int CompId, ArrayList<HLTComponent> currentComps, ArrayList<HLTParameter> currentParas) throws SQLException {
        ArrayList<HLTComponent> returnedComps = new ArrayList<>();
        ArrayList<HLTParameter> returnedParas = new ArrayList<>();

        HLTComponent nu = new HLTComponent(CompId);
        ArrayList<Integer> list = nu.getAllChildComponentIDs();
        list.add(CompId);
        for (Integer Comp : list) {
            returnedComps.addAll(this.checkComp(Comp));
        }
        for (HLTComponent comp : returnedComps) {
            returnedParas.addAll(this.checkPara(comp.get_id()));
        }
        ArrayList<HLTParameter> removeList = new ArrayList<>();
        ArrayList<HLTParameter> addList = new ArrayList<>();
        for (HLTParameter I : currentParas) {
            if (I.get_value().length() > 4000) {
                removeList.add(I);
                addList.addAll(I.split());
            }
        }
        if (removeList.size() > 0) {
            for (HLTParameter I : removeList) {
                currentParas.remove(I);
            }
            for (HLTParameter I : addList) {
                currentParas.add(I);
            }
        }

        //Make duplicate of each list an remove contents which match in the other list
        //If any contents remain then there is no overall match
        //Compare Parameters
        ArrayList<HLTParameter> ParaRef1 = new ArrayList<>(currentParas);
        ArrayList<HLTParameter> ParaRef2 = new ArrayList<>(returnedParas);
        //System.out.println("Pre Param Match " + ParaRef1.size() + " " + ParaRef2.size());
        //int store1 = ParaRef1.size();
        //int store2 = ParaRef2.size();

        ParaRef1.removeAll(returnedParas);
        ParaRef2.removeAll(currentParas);
        //System.out.println("Post Param Match " + ParaRef1.size() + " " + ParaRef2.size());
        if (!ParaRef1.isEmpty() || !ParaRef2.isEmpty()) {
            /*System.out.println("Pre Param Match " + store1 + " " + store2);

            System.out.println("Post Param Match " + ParaRef1.size() + " " + ParaRef2.size());
            for (HLTParameter para : ParaRef1) {
                System.out.println("P1 " + para.get_name());
            }

            for (HLTParameter para : ParaRef2) {
                System.out.println("P2 " + para.get_name());
            }*/

            return false;
        }
        //Compare Components
        ArrayList<HLTComponent> CompRef1 = new ArrayList<>(currentComps);
        ArrayList<HLTComponent> CompRef2 = new ArrayList<>(returnedComps);
        //System.out.println("Pre Comp Match " + CompRef1.size() + " " + CompRef2.size());
        //int store3 = CompRef1.size();
        //int store4 = CompRef2.size();

        CompRef1.removeAll(returnedComps);
        CompRef2.removeAll(currentComps);
        //System.out.println("Post Comp Match " + CompRef1.size() + " " + CompRef2.size());

        if (!CompRef1.isEmpty() || !CompRef2.isEmpty()) {
            /*System.out.println("Pre Comp Match " + store3 + " " + store4);

            System.out.println("Post Comp Match " + CompRef1.size() + " " + CompRef2.size());
            for (HLTComponent comp : CompRef1) {
                System.out.println("C1 " + comp.get_name() + " " + comp.get_alias() + " " + comp.get_py_name() + " " + comp.get_py_package() + " " + comp.get_type() + " " + comp.hashCode() + " " + comp.get_hash() + " " + comp.get_hashfull());
            }

            for (HLTComponent comp : CompRef2) {
                System.out.println("C2 " + comp.get_name() + " " + comp.get_alias() + " " + comp.get_py_name() + " " + comp.get_py_package() + " " + comp.get_type() + " " + comp.hashCode() + " " + comp.get_hash() + " " + comp.get_hashfull());
            }*/
            return false;
        }

        //Parent Components Identical
        return true;
    }

    private ArrayList<HLTParameter> checkPara(int CompId) throws SQLException {
        ArrayList<HLTParameter> returnedParas = new ArrayList<>();
        String q = "select "
                + "hpa_id, hpa_name,hpa_op,hpa_value,hpa_chain_user_version "
                + "from HLT_COMPONENT, HLT_PARAMETER, HLT_CP_TO_PA "
                + "where HLT_CP_TO_PA.hcp2pa_component_id = HLT_COMPONENT.hcp_id AND "
                + "hlt_cp_to_pa.hcp2pa_parameter_id = hlt_parameter.hpa_id AND "
                + "hcp_id = ?";
        q = ConnectionManager.getInstance().fix_schema_name(q);
        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(q);
        ps.setInt(1, CompId);
        ResultSet rset = ConnectionManager.executeSelect(ps);
        int i = 1;
        while (rset.next()) {
            HLTParameter nu = new HLTParameter(-1);
            nu.set_id(rset.getInt("HPA_ID"));
            nu.set_name(rset.getString("HPA_NAME"));
            nu.set_op(rset.getString("HPA_OP"));
            nu.set_value(rset.getString("HPA_VALUE"));
            nu.set_chain_user_version(rset.getBoolean("HPA_CHAIN_USER_VERSION"));
            returnedParas.add(nu);
        }
        rset.close();
        ps.close();
        return returnedParas;
    }

    public ArrayList<HLTParameter> checkParaPublic(int CompId) throws SQLException {
        return this.checkPara(CompId);
    }
    
    private ArrayList<HLTComponent> checkComp(int CompId) throws SQLException {
        ArrayList<HLTComponent> returnedComps = new ArrayList<>();
        String q = "select hcp_id, hcp_name, hcp_version, hcp_alias, hcp_type, hcp_py_name, hcp_py_package from HLT_COMPONENT"
                + " where hcp_id = ?";
        q = ConnectionManager.getInstance().fix_schema_name(q);
        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(q);
        ps.setInt(1, CompId);
        ResultSet rset = ConnectionManager.executeSelect(ps);
        while (rset.next()) {
            HLTComponent nu = new HLTComponent();
            nu.set_id(rset.getInt("HCP_ID"));
            nu.set_name(rset.getString("HCP_NAME"));
            nu.set_version(rset.getInt("HCP_VERSION"));
            nu.set_alias(rset.getString("HCP_ALIAS"));
            nu.set_type(rset.getString("HCP_TYPE"));
            nu.set_py_name(rset.getString("HCP_PY_NAME"));
            nu.set_py_package(rset.getString("HCP_PY_PACKAGE"));
            returnedComps.add(nu);
        }
        //logger.info("Component Parent? "+returnedComps.size());
        rset.close();
        ps.close();
        return returnedComps;
    }

    /**
     * Get all parameters that belong in this component. New method implemented
     * during the schema update. Loads all the parameters for the current
     * Component only if the vector of parameters is currently null. In general
     * this means we only read them once, unless we do a forceLoad. forceLoad
     * should set parameters = null.
     *
     * @return Vector of the parameters which belong to this component.
     * @throws java.sql.SQLException
     */
    public ArrayList<HLTParameter> getParameters() throws SQLException {
        if (parameters == null) {
            parameters = new ArrayList<>();

            if (get_id() > 0) {
                String query = (new HLTParameter(-1)).getQueryString();
                query = query.replace("FROM ", "FROM HLT_COMPONENT, HLT_CP_TO_PA, ");

                String and = " WHERE "
                        + "HCP_ID=? "
                        + "AND "
                        + "HCP2PA_COMPONENT_ID=HCP_ID "
                        + "AND "
                        + "HCP2PA_PARAMETER_ID=HPA_ID "
                        + "ORDER BY HPA_ID";

                query = ConnectionManager.getInstance().fix_schema_name(query + and);

                PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
                ps.setInt(1, get_id());
                ResultSet rset = ConnectionManager.executeSelect(ps);
                while (rset.next()) {
                    HLTParameter param = new HLTParameter(-1);
                    param.loadFromRset(rset);
                    parameters.add(param);
                }
                rset.close();
                ps.close();
            }
        }

        if (parameters != null && parameters.size() > 0) {
            joinParameters();
        }

        return parameters;
    }

    /**
     * Get all parameters IDs that belong in this component.
     *
     * @return Vector of the parameters IDs which belong to this component.
     * @throws java.sql.SQLException
     */
    public ArrayList<Integer> getParameterIDs() throws SQLException {
        ArrayList<Integer> output = new ArrayList<>();
        if (parameters == null) {

            if (get_id() > 0) {
                String query = " SELECT HCP2PA_PARAMETER_ID FROM HLT_CP_TO_PA ";
                String where = " WHERE  HCP2PA_COMPONENT_ID=?";

                query = ConnectionManager.getInstance().fix_schema_name(query + where);

                PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
                ps.setInt(1, get_id());
                ResultSet rset = ps.executeQuery();

                while (rset.next()) {
                    output.add(rset.getInt("HCP2PA_PARAMETER_ID"));
                }

                rset.close();
                ps.close();
            }
        } else {
            for (HLTParameter par : parameters) {
                if (par.get_id() > 0) {
                    output.add(par.get_id());
                } else { // most probably a joined parameter - should be splitted and the splitted ID's has to be found
                    ArrayList<HLTParameter> splitted_par = par.split();
                    for (HLTParameter parSplitted : splitted_par) {
                        output.add(parSplitted.save());
                    }
                }
            }
        }

        return output;
    }

    ///Join splitted parameters which belong to this component
    /**
     * Joins the splitted parameters - Identification __IPC__xx (xx is an
     * integer number) The rest of the parameters keeps as they are
     *
     */
    private void joinParameters() {
        if (parameters != null) {
            // Comparator of Parameter names for a sort function
            class ParameterNameComparator implements Comparator<HLTParameter> {

                @Override
                public int compare(HLTParameter par1, HLTParameter par2) {
                    String par1Name = par1.get_name();
                    String par2Name = par2.get_name();
                    return par1Name.compareTo(par2Name);
                }
            }
            Collections.sort(parameters, new ParameterNameComparator());

            ArrayList<HLTParameter> parToBeRemoved = new ArrayList<>();
            ArrayList<HLTParameter> parToBeJoined = new ArrayList<>();
            HLTParameter joinedPar;
            String joinedName;
            int maxpar = parameters.size();
            for (int ipar = 0; ipar < maxpar; ipar++) {
                HLTParameter par = parameters.get(ipar);
                // Splitted parameters found
                String parName = par.get_name();
                if (parName.contains("__IPC__")) {
                    int index = Integer.parseInt(parName.substring(parName.indexOf("__IPC__") + 7));
                    // On this place it is sure the name fullfills the formating requirements of the splitted parameter
                    parToBeJoined.add(par);
                    joinedName = parName.substring(0, parName.indexOf("__IPC__"));

                    // Test next entry if it is not going to be joined, add the joinedPar into Vector
                    boolean newJoinedParFlag = false;
                    if (ipar >= maxpar - 1) {
                        newJoinedParFlag = true;
                    } else {
                        String nextParJoinedName = ((HLTParameter) parameters.get(ipar + 1)).get_name();
                        if (nextParJoinedName.contains("__IPC__")) {
                            nextParJoinedName = nextParJoinedName.substring(0, nextParJoinedName.indexOf("__IPC__"));
                        }
                        if (!nextParJoinedName.equals(joinedName)) {
                            newJoinedParFlag = true;
                        }
                    }

                    if (newJoinedParFlag && parToBeJoined.size() > 1) {
                        //joinedPar = new HLTParameter(-16);
                        joinedPar = new HLTParameter(); // was -16 before
                        joinedPar.set_name(joinedName);
                        joinedPar.set_value("");
                        joinedPar.set_op(parToBeJoined.get(0).get_op());
                        // Go through existing parameters if a joined parameter doesn't exist already
                        // Safer would be a method addParameter and setParameter in order to avoid duplicates

                        // Go through the parameters which ha to be joined
                        for (HLTParameter candPar : parToBeJoined) {
                            joinedPar.set_value(joinedPar.get_value() + candPar.get_value());
                            parToBeRemoved.add(candPar);
                        }
                        joinedPar.set_hash();
                        parameters.add(joinedPar);
                        parToBeJoined.clear();
                    }
                } // end of test of the parameter name
            } // end of for loop over parameters

            if (parToBeRemoved.size() > 0) {
                parameters.removeAll(parToBeRemoved);
            }
        }
    }

    /**
     * Get direct child components (tools) that belong in this component. New
     * method implemented during the schema update. Loads all the child
     * components for the current component only if the vector of these is
     * currently null. In general this means we only read them once, unless we
     * do a forceLoad. forceLoad should set childcomponents = null.
     *
     * @return Vector of the parameters which belong to this component.
     * @throws java.sql.SQLException
     */
    public ArrayList<HLTComponent> getChildComponents() throws SQLException {
        if (childcomponents == null) {
            childcomponents = new ArrayList<>();
            ArrayList<HLTComponent> childcomp = new ArrayList<>();

            if (get_id() > 0) {
                HLTComponent child = new HLTComponent(-1);
                String query = child.getQueryString();
                query = query.replace("FROM ", "FROM HLT_CP_TO_CP, ");

                String where = " WHERE "
                        + "HCP2CP_PARENT_COMP_ID=? "
                        + "AND "
                        + "HCP2CP_CHILD_COMP_ID=HCP_ID";
                ArrayList<Object> filters = new ArrayList<>();
                filters.add(this.get_id());
                ConnectionManager mgr = ConnectionManager.getInstance();
                ArrayList<AbstractTable> aTables
                        = mgr.forceLoadVector(child, query, where, filters);
                for (AbstractTable aTable : aTables) {
                    childcomp.add((HLTComponent) aTable);
                }
                childcomponents.addAll(childcomp);
            }
        }
        return childcomponents;
    }

    /**
     * Get the ids of all child components.
     *
     * @return the ids of all child Components (recursive).
     * @throws java.sql.SQLException
     */
    public ArrayList<Integer> getAllChildComponentIDs() throws SQLException {

        //note - we still need a vector of objects, so no memory
        //saving, only the return is different to the method above
        if (allchildcomponentIDs == null) {
            allchildcomponentIDs = new ArrayList<>();

            //start with all children of this one
            for (HLTComponent child : getAllChildComponents()) {
                allchildcomponentIDs.add(child.get_id());
            }

        }
        return allchildcomponentIDs;
    }

    /**
     * Get all child components that belong in this component, inc. children of
     * children. New method implemented during the schema update. Loads all the
     * child components for the current component and the children of these and
     * so on down the tree.
     *
     * @return Vector of the components which belong to this component.
     * @throws java.sql.SQLException
     */
    public ArrayList<HLTComponent> getAllChildComponents() throws SQLException {

        if (allchildcomponents == null) {
            allchildcomponents = new ArrayList<>();

            //start with all children of this one
            ArrayList<HLTComponent> thesechildren = this.getChildComponents();
            allchildcomponents.addAll(thesechildren);

            //loop recursively
            for (HLTComponent child : thesechildren) {
                allchildcomponents.addAll(child.getAllChildComponents());
            }
        }
        return allchildcomponents;
    }

    /**
     * String representation of this object. So the tree view knows what
     * information to display in the tree.
     *
     * @return A string containing the id, name and version of the record.
     */
    @Override
    public String toString() {
        if (list_mode) {
            return get_alias() + " (" + get_name() + ")";
        }

        if (get_id() == -1) {
            return "HLT Component";
        }
        return "COMPONENT: " + get_name() + "/" + get_alias() + " (DBid=" + get_id() + "/V." + get_version() + ")";
    }

    /**
     * Minimum names for search results.
     *
     * @return a vector with the minimum names to search for.
     */
    @Override
    public ArrayList<String> get_min_names() {
        ArrayList<String> info = new ArrayList<>();
        info.add("ID");
        info.add("Alias");
        info.add("Version");
        info.add("Type");
        return info;
    }

    ///Minimum data for search results.
    @Override
    public ArrayList<Object> get_min_info() {
        ArrayList<Object> info = new ArrayList<>();
        info.add(get_id());
        info.add(get_alias());
        info.add(get_version());
        info.add(get_type());
        return info;
    }

    /**
     * Function finds all HLT setups that link to this component, including ones
     * that link to parents & grandparents of this component.
     *
     * @return - set of HLT setups.
     * @throws java.sql.SQLException
     */
    public Set<HLTSetup> findAllParentSetups() throws SQLException {
        Set<HLTSetup> setups = new HashSet<>(); // use a set to avoid duplicates
        setups.addAll(findParentSetups());

        /// loop over parents
        Set<HLTComponent> parents = findAllParentComponents();
        for (HLTComponent comp : parents) {
            setups.addAll(comp.findParentSetups());
        }

        return setups;
    }

    /**
     * Function to find HLT Setups that link to this component.
     *
     * @return - a vector of HLTSetups.
     * @throws java.sql.SQLException
     */
    public ArrayList<HLTSetup> findParentSetups() throws SQLException {
        ArrayList<HLTSetup> setups = new ArrayList<>();

        String query = (new HLTSetup(-1)).getQueryString();
        query = query.replace("FROM ", "FROM HLT_ST_TO_CP, ");

        String and = " WHERE "
                + "HST2CP_SETUP_ID=HST_ID "
                + "AND "
                + "HST2CP_COMPONENT_ID=? ";

        query = ConnectionManager.getInstance().fix_schema_name(query + and);
        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ps.setInt(1, get_id());
        ResultSet rset = ps.executeQuery();

        while (rset.next()) {
            HLTSetup setup = new HLTSetup(-1);
            setup.loadFromRset(rset);
            setups.add(setup);
        }
        rset.close();
        ps.close();

        return setups;
    }

    /**
     * Function to find all components that are parents, grandparents ... of
     * this component.
     *
     * @return - A set of all the components.
     * @throws java.sql.SQLException
     */
    public Set<HLTComponent> findAllParentComponents() throws SQLException {
        Set<HLTComponent> links = new HashSet<>(); //set to avoid duplicates
        ArrayList<HLTComponent> parents = findParentComponents();
        for (HLTComponent comp : parents) {
            links.add(comp);
            links.addAll(comp.findAllParentComponents());
        }
        return links;
    }

    /**
     * Function to find direct parents of this component
     *
     * @return
     * @throws java.sql.SQLException
     */
    public ArrayList<HLTComponent> findParentComponents() throws SQLException {
        ArrayList<HLTComponent> links = new ArrayList<>();

        String query = (new HLTComponent(-1)).getQueryString();
        query = query.replace("FROM ", "FROM HLT_CP_TO_CP, ");

        String and = " WHERE "
                + "HCP2CP_PARENT_COMP_ID=HCP_ID "
                + "AND "
                + "HCP2CP_CHILD_COMP_ID=?";

        query = ConnectionManager.getInstance().fix_schema_name(query + and);

        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ps.setInt(1, get_id());
        ResultSet rset = ps.executeQuery();

        while (rset.next()) {
            HLTComponent comp = new HLTComponent(-1);
            comp.loadFromRset(rset);
            links.add(comp);
        }
        rset.close();
        ps.close();

        return links;
    }

    
    public String findParentElementsQuery() throws SQLException {
        ArrayList<HLTTriggerElement> elements = new ArrayList<>();

        String query = (new HLTTriggerElement()).getQueryString();
        query = query.replace(" FROM ", ", HCP_ID FROM "); //11
        query = query.replace("FROM ", "FROM HLT_COMPONENT, HLT_TE_TO_CP, ");
        String and = " WHERE "
                + "HTE2CP_TRIGGER_ELEMENT_ID=HTE_ID "
                + "AND "
                + "HTE2CP_COMPONENT_ID=HCP_ID "
                + "AND "
                + "HCP_ID=?";

        query = ConnectionManager.getInstance().fix_schema_name(query + and);
        return query;
    }
    /**
     *
     * @return
     * @throws SQLException
     */
    public ArrayList<HLTTriggerElement> findParentElements() throws SQLException {
        ArrayList<HLTTriggerElement> elements = new ArrayList<>();

        String query = (new HLTTriggerElement()).getQueryString();
        query = query.replace(" FROM ", ", HCP_ID FROM "); //11
        query = query.replace("FROM ", "FROM HLT_COMPONENT, HLT_TE_TO_CP, ");
        String and = " WHERE "
                + "HTE2CP_TRIGGER_ELEMENT_ID=HTE_ID "
                + "AND "
                + "HTE2CP_COMPONENT_ID=HCP_ID "
                + "AND "
                + "HCP_ID=?";

        query = ConnectionManager.getInstance().fix_schema_name(query + and);

        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ps.setInt(1, get_id());
        ResultSet rset = ps.executeQuery();
        while (rset.next()) {
            HLTTriggerElement ele = new HLTTriggerElement();
            ele.loadFromRset(rset);
            elements.add(ele);
        }
        rset.close();
        ps.close();

        return elements;
    }

    // TODO 
    void setChildren(List<HLTComponent> children) {
        this.childcomponents = new ArrayList<>();
        this.childcomponents.addAll(children);
    }

    /**
     * Recursively load from DB all children from a list of hlt components.
     *
     * @param parents the list of "parent" hlt components.
     * @return the list with parents with the children linked to them.
     * @throws java.sql.SQLException
     */
    public static ArrayList<HLTComponent> loadAllChildrenComponents(final ArrayList<HLTComponent> parents) throws SQLException {
        ArrayList<HLTComponent> children = HLTComponent._loadChildrenComponents(parents);
        if (children.size() > 0) {
            loadAllChildrenComponents(children);
        }
        return parents;
    }

    /**
     * Retrieve from DB the list of children from a list of component.
     *
     * @param algorithms the list of parent HLT Component.
     * @return the list of children.
     * 
     * 
     */
    private static ArrayList<HLTComponent> _loadChildrenComponents(final ArrayList<HLTComponent> algorithms) throws SQLException {
        //
        // get parent ids 
        ArrayList<Integer> ids = ConnectionManager.get_IDs(algorithms);
        //
        // distribute the list of ids into 1000 block.
        ArrayList<List<Integer>> _ids = _divide(ids);
        //
        // A map with PARENT ID as key, a list of Child Components as value.
        HashMap<Integer, ArrayList<HLTComponent>> map0 = new HashMap<>(1000);
        //
        /// The Query
        String query = "SELECT DISTINCT HCP_ALIAS, HCP_HASH, HCP_HASHFULL, HCP_ID, "
                + "HCP_NAME, HCP_PY_NAME, HCP_PY_PACKAGE, HCP_TYPE, HCP_VERSION, " // 10
                + "HCP2CP_PARENT_COMP_ID "
                + "\n FROM HLT_COMPONENT"
                + "\n, HLT_CP_TO_CP "
                + "\n WHERE HCP2CP_CHILD_COMP_ID=HCP_ID "
                + "\n AND HCP2CP_PARENT_COMP_ID IN ";
        String orderBy = "\n ORDER BY HCP_ID ASC";
        ConnectionManager mgr = ConnectionManager.getInstance();
        for (List<Integer> parentIdList : _ids) {
            String in = "(" + parentIdList.get(0);
            for (int i = 1; i < parentIdList.size(); i++) {
                in += ", " + parentIdList.get(i);//", ?";
            }
            in += ")";
            String query1 = query + in + orderBy;
            query1 = mgr.fix_schema_name(query1);
            PreparedStatement ps = mgr.getConnection().prepareStatement(query1);
            ResultSet rset = ps.executeQuery();
            rset.setFetchSize(1000);
            //int j = 0;
            while (rset.next()) {
                //j++;
                int parId = rset.getInt("HCP2CP_PARENT_COMP_ID");
                HLTComponent child = new HLTComponent();
                child.loadFromRset(rset);

                // map of FatherId, ListOfChildren:map0
                ArrayList<HLTComponent> children = map0.get(parId);
                if (children == null) {
                    children = new ArrayList<>();
                }
                if (!children.contains(child)) {
                    children.add(child);
                    map0.put(parId, children);
                }

            }
            rset.close();
            ps.close();
        }
        //
        // DO THE MATCHING & RETURN CHILDREN's LIST
        return HLTComponent._match(algorithms, map0);
    }

    /**
     * Matches the map of children loaded form DB to the parents list. This will
     * create a copy of the objects if a child is assigned to more than one
     * parent, instead of reusing it.
     *
     * @param parents the list of parent components.
     * @param childrenMap0 the map of <parentId, children's List> to assign.
     * @return the list of all children.
     */
    private static ArrayList<HLTComponent> _match(final ArrayList<HLTComponent> parents,
            final Map<Integer, ArrayList<HLTComponent>> childrenMap0) {
        ArrayList<HLTComponent> _parents = new ArrayList<>(parents.size());
        _parents.addAll(parents);
        ArrayList<HLTComponent> _chilren = new ArrayList<>(_parents.size() * 2);

        //Removed to prevent violation of General contract.
        //When the components are re-writted this needs to be fixed.
        //Leaving out for now as the whole commponents DB entry needs
        //to be re-thought.
        //Collections.sort(_parents);
        int parIdprev = -1;
        for (HLTComponent parent : _parents) {
            int parId = parent.get_id();
            if (childrenMap0.containsKey(parId)) {
                ArrayList<HLTComponent> chs;
                if (parId != parIdprev) {
                    chs = childrenMap0.get(parId);
                } else {
                    ArrayList<HLTComponent> _l = childrenMap0.get(parId);
                    chs = new ArrayList<>(_l.size());
                    for (HLTComponent child : _l) {
                        HLTComponent c = (HLTComponent) child.clone();
                        c.set_id(child.get_id());
                        chs.add(c);
                    }

                }
                parent.setChildren(chs);
                _chilren.addAll(chs);
            } else {
                parent.setChildren(new ArrayList<HLTComponent>());
            }
            parIdprev = parId;
        }
        return _chilren;
    }

    /**
     * Divides a list of integers into blocks of size HLTComponent.MAXARGUMENTS.
     *
     * @param ids the list of integers.
     * @return a list with blocks of integers.
     */
    private static ArrayList<List<Integer>> _divide(final ArrayList<Integer> ids) {
        int MAXARGUMENTS = 1000;
        ArrayList<List<Integer>> _ids = new ArrayList<>();
        List<Integer> idsCp = ids;
        while (idsCp.size() > MAXARGUMENTS) {
            List<Integer> subIds = new ArrayList<>(MAXARGUMENTS);
            subIds.addAll(idsCp.subList(0, MAXARGUMENTS));
            _ids.add(subIds);
            idsCp = idsCp.subList(MAXARGUMENTS, idsCp.size());
        }
        _ids.add(idsCp);
        return _ids;
    }

    @Override
    public String getTableName() {
        return "HLT_COMPONENT";
    }

    //find all the chains of the selected algos of this supermaster table, and fill in the list table

    /**
     *
     * @param hlt_menu_id
     * @return
     * @throws SQLException
     */
    
    public String getChainsQuery() throws SQLException {
        ArrayList<HLTTriggerChain> chains_v = new ArrayList<>();

        String query = "SELECT "
                + "HTM2TC_TRIGGER_MENU_ID, HTM2TC_TRIGGER_CHAIN_ID, "
                + "HTC_ID, HTC_CHAIN_COUNTER "
                + "HTC2TS_TRIGGER_CHAIN_ID, HTC2TS_TRIGGER_SIGNATURE_ID, "
                + "HTS2TE_TRIGGER_SIGNATURE_ID, HTS2TE_TRIGGER_ELEMENT_ID, "
                + "HTE2CP_TRIGGER_ELEMENT_ID, HTE2CP_COMPONENT_ID "
                + "FROM "
                + "HLT_TM_TO_TC, HLT_TRIGGER_CHAIN, HLT_TC_TO_TS, HLT_TS_TO_TE, HLT_TE_TO_CP "
                + "WHERE "
                + "HTM2TC_TRIGGER_MENU_ID=? "
                + "AND "
                + "HTC_ID = HTM2TC_TRIGGER_CHAIN_ID "
                + "AND "
                + "HTM2TC_TRIGGER_CHAIN_ID=HTC2TS_TRIGGER_CHAIN_ID "
                + "AND "
                + "HTC2TS_TRIGGER_SIGNATURE_ID=HTS2TE_TRIGGER_SIGNATURE_ID "
                + "AND "
                + "HTS2TE_TRIGGER_ELEMENT_ID=HTE2CP_TRIGGER_ELEMENT_ID "
                + "AND "
                + "HTE2CP_COMPONENT_ID =? "
                + "ORDER BY HTC_CHAIN_COUNTER ASC";

        query = ConnectionManager.getInstance().fix_schema_name(query);
        return query;
        }
    public ArrayList<HLTTriggerChain> getChains(int hlt_menu_id) throws SQLException {
        ArrayList<HLTTriggerChain> chains_v = new ArrayList<>();

        String query = "SELECT "
                + "HTM2TC_TRIGGER_MENU_ID, HTM2TC_TRIGGER_CHAIN_ID, "
                + "HTC_ID, HTC_CHAIN_COUNTER "
                + "HTC2TS_TRIGGER_CHAIN_ID, HTC2TS_TRIGGER_SIGNATURE_ID, "
                + "HTS2TE_TRIGGER_SIGNATURE_ID, HTS2TE_TRIGGER_ELEMENT_ID, "
                + "HTE2CP_TRIGGER_ELEMENT_ID, HTE2CP_COMPONENT_ID "
                + "FROM "
                + "HLT_TM_TO_TC, HLT_TRIGGER_CHAIN, HLT_TC_TO_TS, HLT_TS_TO_TE, HLT_TE_TO_CP "
                + "WHERE "
                + "HTM2TC_TRIGGER_MENU_ID=? "
                + "AND "
                + "HTC_ID = HTM2TC_TRIGGER_CHAIN_ID "
                + "AND "
                + "HTM2TC_TRIGGER_CHAIN_ID=HTC2TS_TRIGGER_CHAIN_ID "
                + "AND "
                + "HTC2TS_TRIGGER_SIGNATURE_ID=HTS2TE_TRIGGER_SIGNATURE_ID "
                + "AND "
                + "HTS2TE_TRIGGER_ELEMENT_ID=HTE2CP_TRIGGER_ELEMENT_ID "
                + "AND "
                + "HTE2CP_COMPONENT_ID =? "
                + "ORDER BY HTC_CHAIN_COUNTER ASC";

        query = ConnectionManager.getInstance().fix_schema_name(query);

        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ps.setInt(1, hlt_menu_id);
        ps.setInt(2, get_id());
        ResultSet rset = ps.executeQuery();
        while (rset.next()) {
            HLTTriggerChain ch = new HLTTriggerChain();
            ch.set_id(rset.getInt("HTM2TC_TRIGGER_CHAIN_ID"));
            ch.forceLoad();
            chains_v.add(ch);
        }

        rset.close();
        ps.close();

        return chains_v;
    }

}
