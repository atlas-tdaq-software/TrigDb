package triggerdb.Entities.HLT;

import java.sql.SQLException;
import java.util.ArrayList;
import triggerdb.Entities.AbstractTable;
import triggerdb.Connections.ConnectionManager;

///Contains a row from HLT Parameter table.
/**
 * HLT Software Parameter object.
 *
 * @author Paul Bell
 * @author Simon Head
 */
public final class HLTParameter extends AbstractTable {

    /**
     * Default constructor.
     * 
     */
    public HLTParameter() {
        super("HPA_");
        DefineKeyValuePairs();
    }

    /**
     *
     * @param input_id
     * @throws SQLException
     */
    public HLTParameter(int input_id) throws SQLException {
        super(input_id, "HPA_");
        DefineKeyValuePairs();
        if (input_id > 0) {
            forceLoad();
        }
    }

    private void DefineKeyValuePairs() {
        keyValue.remove("VERSION");
        
        keyValue.putFirst("HASH", -1, "HashCode for the table");
        keyValue.putFirst("VALUE", "Value", "Value");
        keyValue.putFirst("OP", "Op", "Op");
        keyValue.putFirst("CHAIN_USER_VERSION", false, "CUV");
    }
    ///Create a copy of this parameter.
    /**
     * When the user clicks the copy button we copy the class. This contains the
     * code to set all the variables in the new class to the correct values
     * 
     * NB: parameter has no version
     * @return 
     */
    @Override
    public Object clone() {
        HLTParameter copy = new HLTParameter();
        copy.set_name(get_name());
        copy.set_op(get_op());
        copy.set_value(get_value());
        copy.set_chain_user_version(get_chain_user_version());
        //incase its a split parameter just get the hash from the old one.
        copy.set_hash(this.get_hash());
        return copy;
    }

    ///Save routine is pretty straight forward.
    /**
     * Contains the code that saves the record to the database.
     * 
     * @return 
     * @throws SQLException Stop on sql error.
     */
    @Override
    public int save() throws SQLException {
        if (get_value().length() > 4000) {
            ArrayList<HLTParameter> splittedPars = split();
            Integer firstID = -64;
            for (HLTParameter par : splittedPars) {
                if (firstID == -64) {
                    firstID = par.save();
                } else {
                    par.save();
                }
            }
            return (firstID > 0) ? -1 * firstID : -1;
        }

        this.set_hash();
        ArrayList<Integer> hpa_ids = ConnectionManager.getInstance().get_hashIDs(getTableName(), tablePrefix, this.get_hash(), false);

        if (hpa_ids.isEmpty()) {
            set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix, get_id(), keyValue));
        } else {
            set_id(hpa_ids.get(0));
        }

        return get_id();
    }

    ///Split routine
    /**
     * Contains the code which splits the parameter into more if the value is longer than 4000 char
     * 
     * @return Vector of new parameters - append __IPC__xxx to the original name
     */
    public ArrayList<HLTParameter> split() {
        String value = get_value();
        Integer hash = get_hash();
        ArrayList<HLTParameter> output = new ArrayList<>();
        if (value.length() <= 4000) {
            output.add(this);
            return output;
        }
        String appendString = "__IPC__";
        int nDigits = 1;
        nDigits += (int) Math.log10(value.length() / 4000 + 1);
        appendString += "%0" + nDigits + "d";

        for (int ipc = 0; ipc <= value.length() / 4000; ipc++) {
            HLTParameter splittedParameter = new HLTParameter();
            splittedParameter.set_name(get_name() + String.format(appendString, ipc + 1));
            String splittedValue = ((ipc + 1) * 4000 > value.length()) ? value.substring(ipc * 4000) : value.substring(ipc * 4000, (ipc + 1) * 4000);
            splittedParameter.set_value(splittedValue);
            splittedParameter.set_op(get_op());
            splittedParameter.set_hash(hash);
            output.add(splittedParameter);
        }
        
        return output;
    }

    ///Get op, not sure what it is, normally "SET"

    /**
     *
     * @return
     */
    public String get_op() {
        return (String) keyValue.get("OP");
    }
    ///Op is normally "SET"

    /**
     *
     * @param inp
     */
    public void set_op(String inp) {
        keyValue.put("OP", inp);
    }

    /** Get the value of this parameter.
     * @return  */
    public String get_value() {
        return (String) keyValue.get("VALUE");
    }

    /**
     * Set the value of this parameter.
     * 
     * @param inp the new value.
     */
    public void set_value(final String inp) {
        keyValue.put("VALUE", inp);
    }

    ///Get chain user version

    /**
     *
     * @return
     */
    public Boolean get_chain_user_version() {
        return (Boolean) keyValue.get("CHAIN_USER_VERSION");
    }
    ///Set chain user version

    /**
     *
     * @param inp
     */
    public void set_chain_user_version(Boolean inp) {
        keyValue.put("CHAIN_USER_VERSION", inp);
    }

     /**
     * Experimental, setting a hashcode representing the whole table
     * Will be used to attempt to speed up saving and recovery of DBs.
     * @return The hashcode
     */
    public Integer get_hash() {
        return (Integer) keyValue.get("HASH");
    }
    ///Set the hash for this prescale

    /**
     *
     */
    public void set_hash() {
        Integer inp = this.hashCode();
        keyValue.put("HASH", inp);
    }
    
    //Explicitly set the hashcode for split parameters

    /**
     *
     * @param hash
     */
    public void set_hash(Integer hash) {
        Integer inp = hash;
        keyValue.put("HASH", inp);
    }
    
    ///String representation of the object.
    @Override
    public String toString() {
        if (get_id() == -1) {
            return "HLT Parameter";
        }

        return "HLT Parameter: " + get_name() + " (DBid=" + get_id() + ")";
    }

    ///Minimum names to show in the search results.
    @Override
    public ArrayList<String> get_min_names() {
        ArrayList<String> info = new ArrayList<>();
        info.add("ID");
        info.add("Name");
        info.add("Value");
        return info;
    }

    ///Minimum data to show in the search results.
    @Override
    public ArrayList<Object> get_min_info() {
        ArrayList<Object> info = new ArrayList<>();
        info.add(get_id());
        info.add(get_name());
        info.add(get_value());
        return info;
    }

    @Override
    public String getTableName() {
        return "HLT_PARAMETER";
    }
    
    /*
     * Overriding the equals as we changed the hashcode. They should match logic.
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj instanceof HLTParameter) {
            final HLTParameter other = (HLTParameter) obj;
            return this.hashCode() == other.hashCode();
        } else return false;
    }
    
    /*
    * Counter conversions of null to ~
    */

    /**
     *
     * @param value
     * @return
     */

    protected String convertTilde(String value) {
        if(value.equals("~")) {
            return "";
        } else return value;
    }
    
    @Override
     /*
     * Hashcode override.
     * This will override the hashcode for the table. It should give a unique hash for the whole parameter.
     */
    public int hashCode () {
        long fill = 13;
        //hack to counter conversions of null to ~ before hash calc.
        String value = convertTilde(this.get_value());
        fill = fill+31*this.get_name().hashCode() //string: ok
               +37*this.get_op().hashCode() //string: ok
               +41*value.hashCode() //string: ok
               +43*this.get_chain_user_version().hashCode(); //boolean: ok
        
        return ((int) fill) ^ ((int) (fill >> 32));
    }
}
