package triggerdb.Entities.HLT;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import javax.swing.tree.DefaultMutableTreeNode;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;

/**
 * HLT Trigger Signature mapping class.
 *
 * @author Paul Bell
 * @author Simon Head
 */
public final class HLTTriggerSignature extends AbstractTable {

    /**Vector of all the trigger elements within this signature.*/
    private ArrayList<HLTTriggerElement> elements = new ArrayList<>();
    ///Hold information for the tree.
    private ArrayList<DefaultMutableTreeNode> tree_data = new ArrayList<>();
    ///Counter is held in link table, so not part of the map.
    private Integer signature_counter = -1;
    /** Select query */
    public static final String QUERY = "SELECT HTS_ID, HTS_LOGIC, "
            + "FROM HLT_TRIGGER_SIGNATURE";

    /**
     *
     */
    public HLTTriggerSignature() {
        super(-1, "HTS_");
        DefineKeyValuePairs();
    }
    
     /**
     * Default constructor.
     * 
     * @param input_id The id of the row to load.
     * @throws java.sql.SQLException
     */
    public HLTTriggerSignature(final int input_id) throws SQLException {
        super(input_id, "HTS_");
        DefineKeyValuePairs();

        if (input_id > 0) {
            forceLoad();
            elements = getElementsFromDb();
        }
    }

    private void DefineKeyValuePairs() {
        keyValue.putFirst("HASH", -1, "HashCode for the table");
        keyValue.remove("NAME");
        keyValue.remove("VERSION");
        keyValue.putFirst("LOGIC", -1, "Logic");
    }

    ///Maybe not a good idea, renumbers trigger elements.

    /**
     *
     * @throws SQLException
     */
    public void renumberElements() throws SQLException {
        if (elements != null) {
            int i = 0;
            for (HLTTriggerElement element : getElements()) {
                element.set_element_counter(i);
                ++i;
            }
        }
    }

    ///Force the data to be read from the database again.
    /**
     * After an edit we need to force the record to read from the database.
     * @throws java.sql.SQLException
     */
    @Override
    public void forceLoad() throws SQLException {
        tree_data = new ArrayList<>();
        elements = new ArrayList<>();
        ConnectionManager.getInstance().forceLoad(this);
    }

    /**
     *
     * @return
     */
    public ArrayList<HLTTriggerElement> getElements() {
        //Better way than this.....
        if (elements == null) {
            try {
                logger.info("Loading elelements");
                this.elements = getElementsFromDb();
            } catch (SQLException ex) {
                logger.severe("Could not load algorithms from db in HLT trigger element");
            }
        }
        return elements;
    }

    /**
     *
     * @param elements
     */
    public void setElements(ArrayList<HLTTriggerElement> elements) {
        this.elements = elements;
    }

        /** 
     * Load all the trigger elements (output te) for this signautre.
     * 
     * @return a vector with all elements linked to this trigger signature.
     * @throws java.sql.SQLException
     */
    private ArrayList<HLTTriggerElement> getElementsFromDb() throws SQLException {
        ArrayList<HLTTriggerElement> elementsList = new ArrayList<>();

        if (get_id() > 0) {
     
            ConnectionManager mgr = ConnectionManager.getInstance();
     
            String query = "SELECT HTS2TE_TRIGGER_ELEMENT_ID,HTS2TE_ELEMENT_COUNTER FROM HLT_TS_TO_TE WHERE HTS2TE_TRIGGER_SIGNATURE_ID=? ORDER BY HTS2TE_ELEMENT_COUNTER ASC";
            query = mgr.fix_schema_name(query);
 	    PreparedStatement ps = mgr.getConnection().prepareStatement(query);
 	    ps.setInt(1, get_id());
 	    ResultSet rset = ps.executeQuery();
 		           
 	    while (rset.next()) {
 		               
                HLTTriggerElement el = new HLTTriggerElement(rset.getInt("HTS2TE_TRIGGER_ELEMENT_ID"));
 		el.set_element_counter(rset.getInt("HTS2TE_ELEMENT_COUNTER"));
 		elementsList.add(el);
 		               
 	    }
 	    rset.close();
 	    ps.close();
        }
        return elementsList;
    }
    
    public ArrayList<HLTTriggerElement> getElementsFromDbPublic() throws SQLException {
        return getElementsFromDb();
    }

    ///Get the value of logic.

    /**
     *
     * @return
     */
    public Integer get_logic() {
        return (Integer) keyValue.get("LOGIC");
    }

    ///Set the value of logic.

    /**
     *
     * @param inp
     */
    public void set_logic(int inp) {
        keyValue.put("LOGIC", inp);
    }

    /* Returns the signature counter */

    /**
     *
     * @return
     */

    public Integer get_signature_counter() {
        return signature_counter;
    }

    ///Returns a string of names of all the output trigger elements.

    /**
     *
     * @return
     * @throws SQLException
     */
    public String get_trigger_element_names() throws SQLException {

        String te_names = "";

        for (HLTTriggerElement te : getElements()) {
            if (te_names.length() == 0) {
                te_names = te.get_name();
            } else {
                te_names = te_names + ", " + te.get_name();
            }
        }
        return te_names;
    }

    ///Add the children of signature to the tree.
    @Override
    public void addToTree(DefaultMutableTreeNode treeNode, int counter) throws SQLException {
        --counter;

        if (counter >= 0) {
            if (tree_data.isEmpty()) {
                for (HLTTriggerElement el : getElements()) {
                    DefaultMutableTreeNode anotherLayer = new DefaultMutableTreeNode(el);
                    treeNode.add(anotherLayer);
                    tree_data.add(anotherLayer);
                }
            } else {
                int i = 0;
                for (HLTTriggerElement el : getElements()) {
                    el.addToTree(tree_data.get(i), counter);
                    ++i;
                }
            }
        }
    }

    ///Create a copy of this trigger signature.
    /**
     * When the user clicks the copy button we copy the class. This contains the
     * code to set all the variables in the new class to the correct values.
     * 
     * Don't increment the version this will be done automatically.
     * @return 
     */
    @Override
    public Object clone() {
        HLTTriggerSignature copy = new HLTTriggerSignature();
        copy.set_logic(get_logic());
        copy.set_signature_counter(get_signature_counter());
        copy.set_id(get_id());

        for (HLTTriggerElement element : elements) {
            copy.getElements().add((HLTTriggerElement) element.clone());
        }

        return copy;
    }

    @Override
    public int doDiff(AbstractTable t, DefaultMutableTreeNode treeNode, java.util.Set<String> linkstoignore) throws SQLException {
        int ndiff = super.doDiff(t, treeNode, linkstoignore);

        if (t instanceof HLTTriggerSignature) {
            HLTTriggerSignature sig = (HLTTriggerSignature) t;
            /// Compare chains
            ndiff += triggerdb.Diff.CompareTables.compareTables(this.getElements(), sig.getElements(), treeNode);
        }

        return ndiff;
    }

    ///Save the signature to the database.
    /** a
     * Contains the code that saves the record to the database.
     * 
     * @return 
     * @throws SQLException Stop on SQL problems.
     */
    @Override
    public int save() throws SQLException {

        int matching_id = -1;

        ArrayList<Integer> teIds_vec = new ArrayList<>();
        ArrayList<Integer> counters_vec = new ArrayList<>();
        ArrayList<Integer> teIds2_vec = new ArrayList<>();

        logger.log(Level.FINE, "In save for signature with TEs {0} and logic {1}", new Object[]{elements, get_logic()});

        //look for candidates with same elements (only way to ID a signature)
        for (HLTTriggerElement element : elements) {
            if (element.get_element_counter() < 0 || element.get_element_counter() > 99) {
                teIds2_vec.add(element.get_id());
                logger.log(Level.FINE, "add to teids 2: {0}", element.get_id());
            } else {
                teIds_vec.add(element.get_id());
                counters_vec.add(element.get_element_counter());
                logger.log(Level.FINE, "add to teids 1: {0}", element.get_id());
            }
        }

        ArrayList<Integer> htsConnectionIDs = ConnectionManager.getInstance().matchingConnectionSignatureIDs(teIds_vec, counters_vec, teIds2_vec);

        //loop over candidate ids and check the logic
        //for (int ihts = 0; ihts < htsConnectionIDs.size() && get_id() < 0; ++ihts) {
        for (int ihts = 0; ihts < htsConnectionIDs.size(); ++ihts) {

            logger.log(Level.FINE, "Signature candidate id: {0}", htsConnectionIDs.get(ihts));

            //is the logic the same?
            HLTTriggerSignature matchLogic = new HLTTriggerSignature(htsConnectionIDs.get(ihts));

            logger.log(Level.FINE, "candidate has logic {0}", matchLogic.get_logic());
            logger.log(Level.FINE, "this sig has logic  {0}", get_logic());

            if (matchLogic.get_logic().equals(get_logic())) {
                //this really is identical now
                matching_id = htsConnectionIDs.get(ihts);
                logger.fine("Candidate has same logic");
                break;
            }
            //fields_values.clear();
            //fields_values.put("LOGIC", get_logic());
            //fields_values.put("ID", htsConnectionIDs.get(ihts));
            //Vector<Integer> potHtsIDs = ConnectionManager.getInstance().get_IDs(tableName, tablePrefix, fields_values, null, "ID");

            //Collections.sort(potHtsIDs);

            //if (potHtsIDs.size() > 0) {
            //  htsIDs.add(potHtsIDs.get(0));
            //}
        }

        //Does not already exist?
        if (matching_id < 0) {
            //Does not already exist
            //if (htsIDs.isEmpty()) {
            set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix, get_id(), keyValue));

                   ArrayList<TreeMap<String, Object>> table = new ArrayList<>();
                    for (HLTTriggerElement element : getElements()) {
                        TreeMap<String, Object> link = new TreeMap<>();
                        link.put("TRIGGER_SIGNATURE_ID", get_id());
                        link.put("TRIGGER_ELEMENT_ID", element.get_id());
                        link.put("ELEMENT_COUNTER", element.get_element_counter());
                        table.add(link);
                    }
                    //save parameters
                    TreeMap<Integer, Integer> results = ConnectionManager.getInstance().saveBatch("HLT_TS_TO_TE", "HTS2TE_", 0, table);
            logger.log(Level.FINE, "[SAVE SIGNATURE :: new id = {0} ] {1}", new Object[]{get_id(), get_name()});
        } else {
            //set_id(htsIDs.firstElement());
            set_id(matching_id);
            logger.log(Level.FINE, "[SAVE SIGNATURE :: returned id = {0} ] {1}", new Object[]{get_id(), get_name()});
        }

        return get_id();
    }

    ///Set the signature counter (link table).

    /**
     *
     * @param sig_counter
     */
    public void set_signature_counter(int sig_counter) {
        signature_counter = sig_counter;
    }

    ///Return a string version of the object.
    @Override
    public String toString() {
        String returnString = null;
        if (get_id() == -1) {
            returnString = "HLT Signature";
        }
        try {
            returnString = "SIGNATURE: Counter=" + get_signature_counter() + ", Output TE(s)=" + get_trigger_element_names() + " (DBid=" + get_id() + ")";
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "SQLException cought while trying to return toString().", ex);
        }

        return returnString;
    }

    ///signature has no name in the usual way.
    @Override
    public String get_name() {
        String name = null;
        try {
            name = "signature " + get_trigger_element_names();
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "SQLException cought while trying to get_name().", ex);
        }
        return name;
    }

    ///Minimum names for search results.
    @Override
    public ArrayList<String> get_min_names() {
        ArrayList<String> info = new ArrayList<>();
        info.add("ID");
        info.add("Counter");
        info.add("Output Trigger Elements");
        info.add("Logic");
        return info;
    }

    ///Minimum data for search results.
    @Override
    public ArrayList<Object> get_min_info() throws SQLException {
        ArrayList<Object> info = new ArrayList<>();
        info.add(get_id());
        info.add(get_signature_counter());
        info.add(get_trigger_element_names());
        info.add(get_logic().toString());
        return info;
    }

    /**
     * Used to make the class think it has already loaded the child records.
     */
    public void confuseLoader() {
        elements = new ArrayList<>();
    }

    /**
     *
     * @return
     * @throws SQLException
     */
    public Set<HLTTriggerChain> findParentChains() throws SQLException {
        Set<HLTTriggerChain> chains = new HashSet<>();

        String query = (new HLTTriggerChain()).getQueryString();

        query = query.replace("FROM ", ", HTC2TS_SIGNATURE_COUNTER FROM HLT_TRIGGER_SIGNATURE, HLT_TC_TO_TS, ");
        query += " WHERE HTS_ID=? AND HTC2TS_TRIGGER_CHAIN_ID=HTC_ID AND HTC2TS_TRIGGER_SIGNATURE_ID=HTS_ID ORDER BY HTC2TS_SIGNATURE_COUNTER";

        query = ConnectionManager.getInstance().fix_schema_name(query);

        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ps.setInt(1, get_id());
        ResultSet rset = ps.executeQuery();
        while (rset.next()) {
            HLTTriggerChain ch = new HLTTriggerChain();
            ch.loadFromRset(rset);
            logger.log(Level.INFO, "Found chain: {0}", ch.get_id());
            chains.add(ch);
        }
        rset.close();
        ps.close();

        return chains;
    }
    

    @Override
    public String getTableName() {
        return "HLT_TRIGGER_SIGNATURE";
    }
    
         /**
     * Experimental, setting a hashcode representing the whole table
     * Will be used to attempt to speed up saving and recovery of DBs.
     * @return The hashcode
     */
    public Integer get_hashfull() {
        return (Integer) keyValue.get("HASH");
    }
    ///Set this prescale for an L2 or EF chain.

    //I do not think this should exist? We are hashing a hash here I believe?

    /**
     *
     * @param hashes
     */
    public void set_hashfull(ArrayList<Integer> hashes) {
        long fill = 13;
        int j = 1;
        Collections.sort(hashes);
        for(Integer i:hashes){
            fill += (31+(j*(j+5)))*i;
            j++;
        }
        int inp = ((int) fill) ^ ((int) (fill >> 32));
	keyValue.put("HASH", inp);
    }
    
    /**
     *
     * @param hash
     */
    public void set_hashfull(Integer hash) {
        keyValue.put("HASH", hash);
    }
    
    /**
     *
     * @return
     * @throws SQLException
     */
    public int check_hash() throws SQLException {
        int there = -1;
        String q = "select hts_id from HLT_TRIGGER_SIGNATURE where hts_hash = ?";
        ConnectionManager cmgr = ConnectionManager.getInstance();
        q = cmgr.fix_schema_name(q);
        ResultSet rset;
        try (PreparedStatement ps = cmgr.getConnection().prepareStatement(q)) {
            ps.setInt(1, this.get_hashfull());
            rset = ConnectionManager.executeSelect(ps);
            while (rset.next()) {
                if(rset.getInt("HTS_ID") > 0) {
                    there = rset.getInt("HTS_ID");
                }
            }
            rset.close();
            ps.close();
        }
        return there;
    }
    
     /*
     * Ok, as we just loaded and double checked all the elements and algorithms, now we do the
     * same for the signature. I will just check that the elements match, no need
     * to go down to the algorithms/parameters?
     */

    /**
     *
     * @param SigId
     * @param currentElements
     * @return
     * @throws SQLException
     */

    public boolean doublecheck(int SigId, ArrayList<HLTTriggerElement> currentElements) throws SQLException {
        
        ArrayList<HLTTriggerElement> returnedElements = new ArrayList<>();    
        
        HLTTriggerSignature nu = new HLTTriggerSignature(SigId);
        returnedElements.addAll(nu.getElements());
        return currentElements.containsAll(returnedElements);
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (obj instanceof HLTTriggerSignature) {
            if (!this.get_logic().equals(((HLTTriggerSignature) obj).get_logic())) {
                //logger.info("Failed because alias "+this.get_alias() +" !- "+((HLTTriggerChain) obj).get_alias());
                return false;
            } else return AreTriggerElementsEqual(obj);
        } else {
            return false;
        }
    }

    private boolean AreTriggerElementsEqual(final Object obj) {
        for (HLTTriggerElement e : elements) {
            if (!e.equals((HLTTriggerSignature) obj)){
                return false;
            }
        }
        return true;
    }
    
    public boolean AreTriggerElementsEqualPublic(final Object obj) {
        return AreTriggerElementsEqual(obj);
    }

    @Override
    /*
     * Hashcode override.
     * Hash of just this table, the full hash saved to the DB here will represent the elements
     * and the componenets attached to this signature.
     */
    public int hashCode () {
        long fill = 13;
        fill += 31*this.get_logic();
        int j = 1;
        for (HLTTriggerElement e : elements) {
            fill += (37+(j*(j+5)))*e.hashCode();
            j++;
        }
        
        return ((int) fill) ^ ((int) (fill >> 32));
    }
}
