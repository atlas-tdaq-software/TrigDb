package triggerdb.Entities.HLT;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.tree.DefaultMutableTreeNode;

import triggerdb.Entities.AbstractTable;
import triggerdb.Connections.ConnectionManager;
import triggerdb.DiffMutableTreeNode;

///HLT Trigger Stream.
/**
 * HLT Trigger Stream object.
 *
 * @author Paul Bell
 * @author Simon Head
 */
public class HLTTriggerStream extends AbstractTable {

    ///Prescales belongs to the link, so has its own variable.
    private String stream_prescale = "";

    ///Default constructor.

    /**
     *
     */
    public HLTTriggerStream() {
        super("HTR_");
        setKeys();
    }

    ///Default constructor.

    /**
     *
     * @param input_id
     * @throws SQLException
     */
    public HLTTriggerStream(int input_id) throws SQLException {
        super(input_id, "HTR_");
        setKeys();

        if (input_id > 0) {
            forceLoad();
        }
    }

    private void setKeys() {
        keyValue.remove("VERSION");
        keyValue.putFirst("DESCRIPTION", "", "Description");
        keyValue.putFirst("TYPE", "", "Type");
        keyValue.putFirst("OBEYLB", 0, "Obey LB");
    }

    ///Create a copy of this stream.
    /**
     * When the user clicks the copy button we copy the class. This contains the
     * code to set all the variables in the new class to the correct values.
     *
     * Stream doesn't have a version.
     *
     * @return
     */
    @Override
    public Object clone() {
        HLTTriggerStream copy = new HLTTriggerStream();
        copy.set_name(get_name());
        copy.set_description(get_description());
        copy.set_type(get_type());
        copy.set_obeyLB(get_obeyLB());
        return copy;
    }

    //@Override
    //public boolean equals(Object object){
    //    return object instanceof HLTTriggerStream && ((HLTTriggerStream)object).get_id() == get_id();
    //}
    ///Simple Save called by HLT Trigger Chain.
    /**
     * Contains the code that saves the record to the database.
     *
     * @return
     * @throws SQLException Stop on an SQL problem.
     */
    @Override
    public int save() throws SQLException {

        ArrayList<Integer> htrIDs = ConnectionManager.getInstance().get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");

        if (htrIDs.isEmpty()) {
            set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix, get_id(), keyValue));
        } else {
            set_id(htrIDs.get(0));
        }

        return get_id();
    }

    @Override
    public boolean equals(Object object) {
        return object instanceof HLTTriggerStream && ((HLTTriggerStream) object).get_id() == get_id();
    }

    /*
    This is an old version which fails for new objcts as the description is empty but in the db is "~"
    @Override
    public boolean equals(Object o) {	
	if(!super.equals(o)) {
            return false;
        }

	if( o instanceof HLTTriggerStream) {
	    HLTTriggerStream stream = (HLTTriggerStream)o;
	    if(!this.get_stream_prescale().equals(stream.get_stream_prescale())) {
                return false;
            }
	} else {
            return false;
        }	

	return true;
    }*/
    @Override
    public int hashCode() {
        return super.hashCode() + this.get_stream_prescale().hashCode();
    } //hash for super is abstractmap, may not be safe - why is it implemented there anyway?

    /**
     * Function to look for differences between this and another table.
     *
     * @return number of differences.
     * @throws java.sql.SQLException
     */
    @Override
    public int doDiff(AbstractTable t, DefaultMutableTreeNode treeNode, java.util.Set<String> linkstoignore) throws SQLException {
        int ndiff = super.doDiff(t, treeNode, linkstoignore);

        if (t instanceof HLTTriggerStream) {
            HLTTriggerStream stream = (HLTTriggerStream) t;
            // need to check stream prescale
            if (!this.get_stream_prescale().equals(stream.get_stream_prescale())) {
                treeNode.add(new DiffMutableTreeNode("Stream Prescale", this.get_stream_prescale(), stream.get_stream_prescale()));
                ndiff++;
            }
        }//HLTTriggerStream	    

        return ndiff;
    }

    ///Get obey luminosity block value.

    /**
     *
     * @return
     */
    public Integer get_obeyLB() {
        return (Integer) keyValue.get("OBEYLB");
    }

    ///Set the value of obey luminosity block.

    /**
     *
     * @param inp
     */
    public void set_obeyLB(Integer inp) {
        keyValue.put("OBEYLB", inp);
    }

    ///Set the description.

    /**
     *
     * @return
     */
    public String get_description() {
        return (String) keyValue.get("DESCRIPTION");
    }

    ///Get the description.

    /**
     *
     * @param inp
     */
    public void set_description(String inp) {
        keyValue.put("DESCRIPTION", inp);
    }

    ///Get the type string.

    /**
     *
     * @return
     */
    public String get_type() {
        return (String) keyValue.get("TYPE");
    }

    ///Set the stream prescale (held in link table).

    /**
     *
     * @param inp
     */
    public void set_stream_prescale(String inp) {
        stream_prescale = inp;
    }

    ///Get the stream prescale held in the link table.

    /**
     *
     * @return
     */
    public String get_stream_prescale() {
        return stream_prescale;
    }

    ///Set the tyupe string.

    /**
     *
     * @param inp
     */
    public void set_type(String inp) {
        keyValue.put("TYPE", inp);
    }

    //get the express stream prescale
    ///Return string representation of this object.
    @Override
    public String toString() {
        if (get_id() == -1) {
            return "HLT Stream";
        }
        return "STREAM: " + get_name() + "_" + get_type() + " (DBid=" + get_id() + ")";
    }

    ///Get minimum names for search results, no version.
    @Override
    public ArrayList<String> get_min_names() {
        ArrayList<String> info = new ArrayList<>();
        info.add("ID");
        info.add("Name");
        return info;
    }

    ///Get minimum data for search results, no version.
    @Override
    public ArrayList<Object> get_min_info() {
        ArrayList<Object> info = new ArrayList<>();
        info.add(get_id());
        info.add(get_name());
        return info;
    }

    @Override
    public String getTableName() {
        return "HLT_TRIGGER_STREAM";
    }

    /**
     *
     * @param hlt_menu_id
     * @return
     * @throws SQLException
     */
    public ArrayList<HLTTriggerChain> getChains(int hlt_menu_id) throws SQLException {

        ArrayList<HLTTriggerChain> chains_v = new ArrayList<>();

        String query = "SELECT "
                + "HTC_ID, HTC_CHAIN_COUNTER, HTC2TR_TRIGGER_STREAM_ID, HTC2TR_TRIGGER_CHAIN_ID, HTC2TR_TRIGGER_STREAM_PRESCALE, "
                + "HTM2TC_TRIGGER_CHAIN_ID, HTM2TC_TRIGGER_MENU_ID, HTM_ID "
                + "FROM "
                + "HLT_TRIGGER_CHAIN, HLT_TC_TO_TR, HLT_TM_TO_TC, HLT_TRIGGER_MENU "
                + "WHERE "
                + "HTM_ID=? "
                + "AND "
                + "HTM2TC_TRIGGER_MENU_ID=HTM_ID "
                + "AND "
                + "HTM2TC_TRIGGER_CHAIN_ID=HTC2TR_TRIGGER_CHAIN_ID "
                + "AND "
                + "HTC2TR_TRIGGER_STREAM_ID=? "
                + "AND "
                + "HTC_ID=HTC2TR_TRIGGER_CHAIN_ID "
                + "ORDER BY HTC_CHAIN_COUNTER ASC";

        query = ConnectionManager.getInstance().fix_schema_name(query);

        //System.out.println("Chains query " + query);
        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ps.setInt(1, hlt_menu_id);
        ps.setInt(2, get_id());
        ResultSet rset = ps.executeQuery();
        while (rset.next()) {
            HLTTriggerChain ch = new HLTTriggerChain();
            ch.set_id(rset.getInt("HTC_ID"));
            ch.forceLoad();
            //ch.getStreamsFromDb();
            //Don't need to load signatures or groups, load elements one by one to improve performance
            chains_v.add(ch);
        }

        rset.close();
        ps.close();

        return chains_v;
    }
}
