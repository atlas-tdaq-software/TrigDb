package triggerdb.Entities.HLT;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.TreeMap;
import java.util.logging.Level;
import javax.swing.tree.DefaultMutableTreeNode;
import triggerdb.Entities.AbstractTable;
import triggerdb.Connections.ConnectionManager;

///HLT Setup file. Configuration requires an L2 and EF setup.
/**
 * HLT setup. Links to all components, which link to all
 * parameters.
 * 
 * @author Paul Bell
 * @author Simon Head
 */
public final class HLTSetup extends AbstractTable {

    /** Components contained within this setup.*/
    private ArrayList<HLTComponent> components = null;
    ///Tree.
    private ArrayList<DefaultMutableTreeNode> tree_data = new ArrayList<>();

    ///Default constructor.

    /**
     *
     */
    public HLTSetup() {
        super("HST_");
    }
    
    /**
     *
     * @param input_id
     * @throws SQLException
     */
    public HLTSetup(int input_id) throws SQLException {
        super(input_id, "HST_");
        if (input_id > 0) {
            forceLoad();
        }
    }

    ///Force the data to be read from the DB again.
    @Override
    public void forceLoad() throws SQLException {
        tree_data = new ArrayList<>();
        ConnectionManager.getInstance().forceLoad(this);
    }

    ///Add the components of this setup to the tree.
    /**
     * You need to implement this so the tree knows how to display the children.
     * @throws java.sql.SQLException
     */
    @Override
    public void addToTree(DefaultMutableTreeNode treeNode, int counter) throws SQLException {
        --counter;
        if (counter >= 0) {
            if (tree_data.isEmpty()) {
                for (HLTComponent comp : getInfraStructureComponents()) {
                    DefaultMutableTreeNode anotherLayer = new DefaultMutableTreeNode(comp);
                    treeNode.add(anotherLayer);
                    tree_data.add(anotherLayer);
                }
            } else {
                int i = 0;
                for (HLTComponent comp : getInfraStructureComponents()) {
                    comp.addToTree(tree_data.get(i), counter);
                    ++i;
                }
            }
        }
    }

    ///Make a copy of this object. Should be a deep copy?
    /**
     * When the user clicks the copy button we copy the class. This contains the
     * code to set all the variables in the new class to the correct values.
     *
     * Don't increment the version this will be done automagically.
     * @return 
     */
    @Override
    public Object clone() {
        HLTSetup copy = new HLTSetup();
        copy.set_name(get_name());
        copy.set_version(get_version());
        return copy;
    }

    @Override
    public int doDiff(AbstractTable t, DefaultMutableTreeNode treeNode, java.util.Set<String> linkstoignore) throws SQLException {
        int ndiff = super.doDiff(t,treeNode,linkstoignore);
        
        if(t instanceof HLTSetup) {
            HLTSetup setup = (HLTSetup)t;
            /// Compare components
            ndiff += triggerdb.Diff.CompareTables.compareTables(this.getInfraStructureComponents(),setup.getInfraStructureComponents(),treeNode);
        }
        
        return ndiff;
    }
    
    ///Save to DB, have to check this record and all the links.
    /**
     * Contains the code that saves the record to the database.
     *
     * @return 
     * @throws SQLException Stop on SQL Exception.
     */
    @Override
    public int save() throws SQLException {
        String name = get_name();
        
        ArrayList<Integer> componentIds_vec = new ArrayList<>();
        for (HLTComponent comp : getInfraStructureComponents()) {
            componentIds_vec.add(comp.get_id());
        }

        Collections.sort(componentIds_vec);

        //if name is only difference, don't want a new key, so temporarilty remove from map
        keyValue.remove("NAME"); 
        ArrayList<Integer> hst_ids = ConnectionManager.getInstance().get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");
        keyValue.put("NAME", name); //put the name back
        ArrayList<Integer> hstCompIDs = new ArrayList<>();

        for (Integer setup_id : hst_ids) {
            ArrayList<Integer> setup_parIDs = ConnectionManager.getInstance().get_ItemList("HLT_ST_TO_CP", "HST2CP", "HST2CP_SETUP", "HST2CP_COMPONENT", setup_id);
            Collections.sort(setup_parIDs);

            if (componentIds_vec.equals(setup_parIDs)) {
                hstCompIDs.add(setup_id);
                break;
            }
        }

        if (hstCompIDs.size() > 0) {
            set_id(hstCompIDs.get(0));

            if (hstCompIDs.size() > 1) {
                logger.log(Level.WARNING, "AMBIGUITY: Multiple Setups found: {0} HST_NAME: {1}", new Object[]{hstCompIDs.size(), get_name()});
            }
        }

        if (get_id() <= 0) {
            int version = ConnectionManager.getInstance().getMaxVersion(getTableName(), tablePrefix, get_name()) + 1;
            keyValue.put("VERSION", version);
            set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix, get_id(), keyValue));

            //save parameters
            ArrayList<TreeMap<String, Object>> table = new ArrayList<>();

            for (HLTComponent comp : getInfraStructureComponents()) {
                TreeMap<String, Object> link = new TreeMap<>();
                link.put("COMPONENT_ID", comp.get_id());
                link.put("SETUP_ID", get_id());
                table.add(link);
                //ConnectionManager.getInstance().save("HLT_ST_TO_CP", "HST2CP_", -1, link);
            }

             TreeMap<Integer, Integer> results = ConnectionManager.getInstance().saveBatch("HLT_ST_TO_CP", "HST2CP_", 0, table);
        }

        return get_id();
    }
    
    ///Save to DB, have to check this record and all the links.
    /**
     * Contains the code that saves the record to the database.
     *
     * @return 
     * @throws SQLException Stop on SQL Exception.
     */
    public int compactsave() throws SQLException {
        String name = get_name();
        
        ArrayList<Integer> componentIds_vec = new ArrayList<>();
        for (HLTComponent comp : getInfraStructureComponents()) {
            componentIds_vec.add(comp.get_id());
        }

        Collections.sort(componentIds_vec);

        //if name is only difference, don't want a new key, so temporarilty remove from map
        keyValue.remove("NAME"); 
        ArrayList<Integer> hst_ids = ConnectionManager.getInstance().get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");
        keyValue.put("NAME", name); //put the name back
        ArrayList<Integer> hstCompIDs = new ArrayList<>();
        
        //This reduces the number of queries to the DB incase there are multiple
        //candidate Ids.
        ArrayList<int[]> holderParams = null;
        if(hst_ids.size()>0){
            holderParams = ConnectionManager.getInstance().get_CompactItemList("HLT_ST_TO_CP", "HST2CP", "HST2CP_SETUP", "HST2CP_COMPONENT", hst_ids)
;
        }
        for (Integer setup_id : hst_ids) {
            ArrayList<Integer> setup_parIDs = new ArrayList<>();
            for(int[] i : holderParams){
                if(i[0] == setup_id) setup_parIDs.add(i[1]);
            }
            Collections.sort(setup_parIDs);

            if (componentIds_vec.equals(setup_parIDs)) {
                hstCompIDs.add(setup_id);
                break;
            }
        }

        if (hstCompIDs.size() > 0) {
            set_id(hstCompIDs.get(0));

            if (hstCompIDs.size() > 1) {
                logger.log(Level.WARNING, "AMBIGUITY: Multiple Setups found: {0} HST_NAME: {1}", new Object[]{hstCompIDs.size(), get_name()});
            }
        }

        if (get_id() <= 0) {
            int version = ConnectionManager.getInstance().getMaxVersion(getTableName(), tablePrefix, get_name()) + 1;
            keyValue.put("VERSION", version);
            set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix, get_id(), keyValue));

             //save parameters
            ArrayList<TreeMap<String, Object>> table = new ArrayList<>();
            
            for (HLTComponent comp : getInfraStructureComponents()) {
                TreeMap<String, Object> link = new TreeMap<>();
                link.put("COMPONENT_ID", comp.get_id());
                link.put("SETUP_ID", get_id());
                table.add(link);
                //ConnectionManager.getInstance().save("HLT_ST_TO_CP", "HST2CP_", -1, link);
            }

            TreeMap<Integer, Integer> results = ConnectionManager.getInstance().saveBatch("HLT_ST_TO_CP", "HST2CP_", 0, table);
        }

        return get_id();
    }

    ///String representation of this object.
    /**
     * So the tree view knows what information to display in the tree.
     *
     * @return A string containing the id, name and version of the record.
     */
    @Override
    public String toString() {
        if (get_id() == -1) {
            return "HLT I-Setup";
        }

        return "HLT I-SETUP: ID=" + get_id() + ", Name=" + get_name() + ", Version=" + get_version();
    }

    /**
     * Load all the components for this setup. 
     * 
     * @return 
     * @throws java.sql.SQLException 
     */
        public ArrayList<HLTComponent> getInfraStructureComponents() throws SQLException {
        if (components == null) {
            components = new ArrayList<>();

            if (get_id() > 0) {
                String query = (new HLTComponent(-1)).getQueryString();
                query = query.replace("FROM ", "FROM HLT_SETUP, HLT_ST_TO_CP, ");

                String and = " WHERE " +
                        "HST_ID=? " +
                        "AND " +
                        "HST2CP_SETUP_ID=HST_ID " +
                        "AND " +
                        "HST2CP_COMPONENT_ID=HCP_ID";

                query = ConnectionManager.getInstance().fix_schema_name(query + and);

                PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
                ps.setInt(1, get_id());
                ResultSet rset = ps.executeQuery();

                while (rset.next()) {
                    HLTComponent component = new HLTComponent(-1);
                    component.loadFromRset(rset);
                    components.add(component);
                }

                rset.close();
                ps.close();
            }
        }
        return components;
    }

    ///Minimum names for search results.
    @Override
    public ArrayList<String> get_min_names() {
        ArrayList<String> info = new ArrayList<>();
        info.add("ID");
        info.add("Name");
        info.add("Version");
        info.add("Linked Menus");
        return info;
    }

    ///Minimum data for search results.
    @Override
    public ArrayList<Object> get_min_info() {
        ArrayList<Object> info = new ArrayList<>();
        info.add(get_id());
        info.add(get_name());
        info.add(get_version());
        info.add("");
        return info;
    }

    @Override
    public String getTableName() {
         return "HLT_SETUP";
   }
}
