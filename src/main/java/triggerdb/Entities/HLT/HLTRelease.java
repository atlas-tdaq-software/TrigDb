package triggerdb.Entities.HLT;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.logging.Level;
import javax.swing.tree.DefaultMutableTreeNode;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;

/**
 * Holds information about the software release used by the HLT.  Unused 
 * in loader classes to my knowledge. Name is 12.0.6 for example. Version is
 * an integer.
 * 
 * @author Paul Bell
 * @author Simon Head
 */
public class HLTRelease extends AbstractTable {

    ///Supermasters to be used in conjunction
    private ArrayList<Integer> smkids_v = null;
    
    ///So the tree knows what it has loaded.
    private final ArrayList<DefaultMutableTreeNode> tree_data = new ArrayList<>();
    
    ///Create a new object with -1, or load a row from the DB.

    /**
     *
     */
    public HLTRelease() {
        super("HRE_");
        DefineKeyValuePairs();
    }

    /**
     *
     * @param input_id
     * @throws SQLException
     */
    public HLTRelease(int input_id) throws SQLException {
        super(input_id, "HRE_");
        DefineKeyValuePairs();
        if (input_id > 0) {
            forceLoad();
        }
    }

    private void DefineKeyValuePairs() {
        keyValue.remove("VERSION");
        keyValue.putFirst("BASE", "Base", "Base");
        keyValue.putFirst("PATCH_1", "Patch_1", "Patch_1");
        keyValue.putFirst("PATCH_2", "Patch_2", "Patch_2");
    }
    
    ///Copying this object is easy.
    /**
     * When the user clicks the copy button we copy the class. This contains the
     * code to set all the variables in the new class to the correct values.
     * 
     * Don't increment the version this will be done automatically on save.
     * @return 
     */
    @Override
    public Object clone() {
        HLTRelease copy = new HLTRelease();
        copy.set_name(get_name());
        return copy;
    }

    ///Very simple save routine.
    /**
     * Contains the code that saves the record to the database.
     * 
     * @return 
     * @throws SQLException Stop on SQL exception.
     */
    @Override
    public int save() throws SQLException {
        keyValue.remove("VERSION");
        
        ArrayList<Integer> ids = ConnectionManager.getInstance().get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");

        if (ids.isEmpty()) {
            set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix, get_id(), keyValue));
        } else {
            set_id(ids.get(0));
        }
        
        return get_id();
    }

    
    ///String representation of the object.
    @Override
    public String toString() {
        if (get_id() == -1) {
            return "HLT Release";
        }
        return "HLT RELEASE: " + get_name();
    }
    
    ///Load all the valid supermaster keys for this release

    /**
     *
     * @return
     * @throws SQLException
     */
    public ArrayList<Integer> getSMKIDs() throws SQLException {
        if (smkids_v == null) {
            smkids_v = new ArrayList<>();
        
            if (get_id() > 0) {
                String query = "SELECT SMT2RE_SUPER_MASTER_TABLE_ID FROM HLT_SMT_TO_HRE ";
                String and = "WHERE " +
                             "SMT2RE_RELEASE_ID=? " +
                             "ORDER BY SMT2RE_SUPER_MASTER_TABLE_ID";
                
                query = ConnectionManager.getInstance().fix_schema_name(query + and);

                PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
                ps.setInt(1, get_id());
                ResultSet rset = ps.executeQuery();

                while (rset.next()) {
                    smkids_v.add(rset.getInt("SMT2RE_SUPER_MASTER_TABLE_ID"));
                }

                rset.close();
                ps.close();
            }
        }
        return smkids_v;
    }
    
 
    @Override
    public String getTableName() {
        return "HLT_RELEASE";
    }
    
     /**
     * Minimum names for the search results.
     * @return 
     */
    @Override
    public ArrayList<String> get_min_names() {
        ArrayList<String> info = new ArrayList<>();
        info.add("ID");
        info.add("Name");
        return info;
    }
    
    /**
     * Minimum information for the search results.
     * @return 
     */
    @Override
    public ArrayList<Object> get_min_info() {
        ArrayList<Object> info = new ArrayList<>();
        info.add(get_id());
        info.add(get_name());
        return info;
    }
}