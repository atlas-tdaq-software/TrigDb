package triggerdb.Entities.HLT;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeMap;
import javax.swing.JOptionPane;
import javax.swing.tree.DefaultMutableTreeNode;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.HLTLinks.HLTTM_PS;

/**
 * The HLT Prescale Set record.
 *
 * @author Paul Bell
 * @author Simon Head
 */
public final class HLTPrescaleSet extends AbstractTable {

    /**
     * List of prescales in this set.
     */
    private final ArrayList<HLTPrescale> regularPrescales = new ArrayList<>();
    /**
     * List of stream prescales.
     */
    private final ArrayList<HLTPrescale> streamPrescales = new ArrayList<>();
    /**
     * List of all prescales.
     */
    private ArrayList<HLTPrescale> allPrescales = new ArrayList<>();
    /**
     * So the tree remembers what it has loaded.
     */
    private boolean needtoadd = true;

    /**
     *
     */
    public HLTPrescaleSet() {
        super("HPS_");
        DefineKeyValuePairs();
    }

    /**
     * Default constructor. If id>0 it will try to load the record from DB, else
     * it will create a new record with default values.
     *
     * @param input_id The database Id of this record.
     * @throws java.sql.SQLException
     */
    public HLTPrescaleSet(int input_id) throws SQLException {
        super(input_id, "HPS_");

        DefineKeyValuePairs();

        if (input_id > 0) {
            forceLoad();
            allPrescales = getPrescalesFromDb();
        }
    }

    private void DefineKeyValuePairs() {
        keyValue.putFirst("TYPE", "Physics", "Type");
        keyValue.putFirst("COMMENT", "", "Comment");
    }

    /**
     * Remove this row from the database.
     *
     * @throws java.sql.SQLException
     */
//    public void delete() throws SQLException {
//        String query = "DELETE FROM " + getTableName() + " WHERE " + tablePrefix + "ID=?";
//        query = ConnectionManager.getInstance().fix_schema_name(query);
//        try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query)) {
//            ps.setInt(1, get_id());
//            ps.executeUpdate();
//        }
//    }
    /**
     * Force this record to be read again from the database. After an edit we
     * need to force the record to read from the database.
     *
     * @throws java.sql.SQLException
     */
    @Override
    public void forceLoad() throws SQLException {
        regularPrescales.clear();
        allPrescales.clear();
        streamPrescales.clear();
        ConnectionManager.getInstance().forceLoad(this);
    }

    /**
     * get the id of the link to the menu.
     *
     * @param hlt_menu_id The id of the HLT Trigger Menu.
     * @return the Id of the Link to the menu hlt_menu_id.
     */
    public int get_TMPSid(int hlt_menu_id) throws SQLException {
        int linkid = HLTTM_PS.getTM2PS(hlt_menu_id, this.get_id());
        return linkid;
    }

    /**
     *
     * @return
     * @throws SQLException
     */
    public ArrayList<Integer> get_Menus() throws SQLException {
        ArrayList<Integer> menus = HLTTM_PS.getMenus(this.get_id());
        return menus;
    }

    public ArrayList<HLTPrescale> getPrescales() {
        return allPrescales;
    }

    /**
     *
     * @param list
     */
    public void setPrescales(ArrayList<HLTPrescale> list) {
        allPrescales = list;
    }

    /**
     *
     * @param list
     */
    public void addPrescales(ArrayList<HLTPrescale> list) {
        allPrescales.addAll(list);
    }

    /**
     *
     * @param prescale
     */
    public void addPrescale(HLTPrescale prescale) {
        allPrescales.add(prescale);
    }

    /**
     * Get ALL prescales (Regular and Stream) that are part of this prescale
     * set.
     *
     * @return a vector with all prescales ordered by L2/EF and Chain Counter.
     */
    ArrayList<HLTPrescale> getPrescalesFromDb() throws SQLException {
        ArrayList<HLTPrescale> list = new ArrayList<>();
        if (get_id() >= 0) {
            ConnectionManager mgr = ConnectionManager.getInstance();
            String whereClause = " WHERE HPR_PRESCALE_SET_ID=? ORDER BY HPR_CHAIN_COUNTER ASC";
            ArrayList<Object> filters = new ArrayList<>();
            filters.add(this.get_id());
            ArrayList<AbstractTable> aTabs = mgr.forceLoadVector(
                    new HLTPrescale(), whereClause, filters);
            for (AbstractTable tab : aTabs) {
                list.add((HLTPrescale) tab);
            }
        }

        return list;
    }

    /**
     *
     * @return a vector with all regular prescales belonging to this set.
     */
    public ArrayList<HLTPrescale> getRegularPrescales() {
        ArrayList<HLTPrescale> list = new ArrayList<>();
        for (HLTPrescale p : allPrescales) {
            if (p.get_type() == HLTPrescaleType.Prescale) {
                list.add(p);
            }
        }
        return list;
    }

    /**
     *
     * @return a vector with all non-rerun prescales belonging to this set.
     */
    public ArrayList<HLTPrescale> getNonReRunPrescales() {
        ArrayList<HLTPrescale> list = new ArrayList<>();

        for (HLTPrescale p : allPrescales) {
            if (p.get_type() != HLTPrescaleType.ReRun) {
                list.add(p);
            }
        }
        return list;
    }

    /**
     * Add the prescales to the prescale set in the tree view.
     *
     * @param treeNode
     * @param counter
     * @throws java.sql.SQLException
     */
    @Override
    public final void addToTree(DefaultMutableTreeNode treeNode, int counter) throws SQLException {
        --counter;

        if (counter >= 0) {
            if (needtoadd) {
                for (HLTPrescale ps : getPrescales()) {
                    DefaultMutableTreeNode anotherLayer = new DefaultMutableTreeNode(ps);
                    treeNode.add(anotherLayer);
                }
                needtoadd = false;
            }
        }
    }

    /**
     * Create a copy of this prescale set. Should be a deep copy? When the user
     * clicks the copy button we copy the class. This contains the code to set
     * all the variables in the new class to the correct values
     *
     * Don't increment the version this will be done automagically
     *
     * @return
     */
    @Override
    public Object clone() {
        HLTPrescaleSet copy = new HLTPrescaleSet();
        copy.set_name(get_name());
        copy.set_version(get_version());
        return copy;
    }

    @Override
    public int doDiff(AbstractTable t, DefaultMutableTreeNode treeNode, java.util.Set<String> linkstoignore) throws SQLException {
        int ndiff = super.doDiff(t, treeNode, linkstoignore);

        if (t instanceof HLTPrescaleSet) {
            HLTPrescaleSet thisset = new HLTPrescaleSet(this.get_id());
            HLTPrescaleSet set = new HLTPrescaleSet(t.get_id());

            /// Diff the prescales
            ndiff += triggerdb.Diff.CompareTables.compareTables(thisset.getRegularPrescales(), set.getRegularPrescales(), treeNode);
        }

        return ndiff;
    }

    private ArrayList<Integer> getMatchingPrescaleSetsFromDB(ConnectionManager mgr, String query, ArrayList<Integer> prescales, int numEntries) throws SQLException {
        ArrayList<Integer> ids2;
        //logger.fine(query);
        try (PreparedStatement ps = mgr.getConnection().prepareStatement(query)) {
            //endTime = System.currentTimeMillis();
            //System.out.println("Query Ready Total execution time: " + (endTime - startTime));
            int psCounter = 1;
            Iterator<Integer> psIter = prescales.iterator();
            while (psIter.hasNext()) {
                //int var = psIter.next();
                //System.out.println(psCounter + " " + var);
                ps.setInt(psCounter, psIter.next());
                psCounter++;
            }
            //System.out.println("test " + numEntries);
            ps.setInt(psCounter++, numEntries);
            ps.setInt(psCounter, numEntries);

            try (ResultSet rset = ps.executeQuery()) {
                //endTime = System.currentTimeMillis();
                //System.out.println("Query Compete Total execution time: " + (endTime - startTime));
                ids2 = new ArrayList<>();
                while (rset.next()) {
                    ids2.add(rset.getInt("HPR_PRESCALE_SET_ID"));
                    //System.out.println("match");
                }
                rset.close();
            }
            //endTime = System.currentTimeMillis();
            //System.out.println("ids2 ready Total execution time: " + (endTime - startTime));
            ps.close();
        }
        return ids2;
    }

    @Override
    public int save() throws SQLException{
	System.out.println("NOT IN USE");
	return -1;
    }
    public ArrayList<Integer> getMatchingPrescaleSetsFromDBPublic(ConnectionManager mgr, String query, ArrayList<Integer> prescales, int numEntries) throws SQLException{
        return getMatchingPrescaleSetsFromDB(mgr, query, prescales, numEntries);
    }
    /**
     * Save the set and all the prescales. Contains the code that saves the
     * record to the database.
     *
     * @param menuID
     * @return
     * @throws SQLException Stop on an SQL problem.
     */

    public int save(int menuID) throws SQLException{

        logger.finer(keyValue.toString());

        if(getPrescales().isEmpty()){
                 JOptionPane.showMessageDialog(null,
                    "Attempting to save a prescale set containing no prescales, aborting",
                    "Empty Prescale Set!",
                    JOptionPane.ERROR_MESSAGE);
                //throw new UploadException("Attemting to save a prescales et containing no prescales!");
                return -1; // put in proper exception - requires rewrite of abstract table member and propagation throughout code
        }
        
        ArrayList<Integer> ids2 = new ArrayList<>();
        int numEntries = getPrescales().size();

        if (numEntries > 0) {

	    java.lang.String query = "Select * from (SELECT HPR_PRESCALE_SET_ID,count(*) FROM HLT_PRESCALE where HPR_PRESCALE_SET_ID IN (select HTM2PS_PRESCALE_SET_ID from HLT_TM_TO_PS where HTM2PS_TRIGGER_MENU_ID = ?) AND HPR_HASH IN (select tempvar from TEMP) GROUP BY HPR_PRESCALE_SET_ID having count(*) = ?)  match join (select HPR_PRESCALE_SET_ID from (select * from (SELECT HPR_PRESCALE_SET_ID FROM HLT_PRESCALE) lhs join ( select HTM2PS_PRESCALE_SET_ID from HLT_TM_TO_PS where HTM2PS_TRIGGER_MENU_ID = ?) rhs on HPR_PRESCALE_SET_ID = HTM2PS_PRESCALE_SET_ID) group by HPR_PRESCALE_SET_ID having count(*) = ?) total on match.HPR_PRESCALE_SET_ID = total.HPR_PRESCALE_SET_ID";

            ConnectionManager mgr = ConnectionManager.getInstance();
            query = mgr.fix_schema_name(query);

	    //System.out.println(query);

            try (PreparedStatement ps = mgr.getConnection().prepareStatement(query)) {
                  //endTime = System.currentTimeMillis();
                  //System.out.println("Query Ready Total execution time: " + (endTime - startTime));
                 
                  mgr.getConnection().setAutoCommit(false);
                  
                  java.lang.String insert = "insert into TEMP (TEMPVAR) values(?)";
                  insert = mgr.fix_schema_name(insert);
		  PreparedStatement temp = mgr.getConnection().prepareStatement(insert);
                  for (HLTPrescale Ps : getPrescales()) {
                      temp.setInt(1, Ps.hashCode());
                      temp.addBatch();
                  }
                  temp.executeBatch();

		  ps.setInt(1, menuID);                  
		  ps.setInt(2, numEntries);
                  ps.setInt(3, menuID);                  
                  ps.setInt(4, numEntries);

                  try (ResultSet rset = ps.executeQuery()) {
                      //endTime = System.currentTimeMillis();
                      //System.out.println("Query Compete Total execution time: " + (endTime - startTime));
		      ids2 = new ArrayList<>();
                      while (rset.next()) {
                          ids2.add(rset.getInt("HPR_PRESCALE_SET_ID"));
                      }
                      rset.close();
                  }
		  mgr.getConnection().commit();
		  temp.close();
                  ps.close();
                  mgr.getConnection().setAutoCommit(true);
                  //endTime = System.currentTimeMillis();
                  //System.out.println("ids2 ready Total execution time: " + (endTime - startTime));
              }


        }

        if (ids2.isEmpty()) {

            int version = 1 + ConnectionManager.getInstance().getMaxVersion(getTableName(), tablePrefix, get_name());
            keyValue.put("VERSION", version);

            set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix, get_id(), keyValue));

            //Save the prescales
            ArrayList<TreeMap<String, Object>> table = new ArrayList<>();
            for (HLTPrescale link : getPrescales()) {
                link.set_id(-1);
                link.set_prescale_set_id(get_id());
                link.set_hash();
                table.add(link.getKey());
                //WPV batch
            }
            TreeMap<Integer, Integer> results = ConnectionManager.getInstance().saveBatch("HLT_PRESCALE", "HPR_", 0, table);
        } else {
            set_id(ids2.get(0));
        }
        return get_id();
    }

    /**
     * Save this Prescale Set and links it to the given L1Menu
     *
     * @param onMenu the Trigger Menu to link to.
     * @return The id of the newly saved L1PRescale.
     * @throws java.sql.SQLException
     */
    public int save(final HLTTriggerMenu onMenu) throws SQLException {

        // Save this prescale set
        int psSaveId = this.save(onMenu.get_id());

        // Add the saved prescale to the given menu, this will create a new
        // Link table if needed.
        onMenu.addPrescaleSet(this, true);

        // Get the id of the link table.
        return psSaveId;
    }

    /**
     * Get the comment about this configuration.
     *
     * @return The comment field of this record.
     */
    public String get_comment() {
        if (keyValue.get("COMMENT") == null) {
            return "null comment";
        } else {
            return (String) keyValue.get("COMMENT");
        }
    }

    /**
     * Set a comment about this PSS.
     *
     * @param comment
     */
    public void set_comment(final String comment) {
        keyValue.put("COMMENT", comment);
    }

    /**
     *
     * @return The name of this HLT Prescale Set formatted as: "HPS_ID :
     * HPS_NAME v HPS_VERSION".
     */
    @Override
    public String toString() {
        if (this.get_id() < 0) {
            return " *: " + this.get_name();
        }
        return get_id() + ": " + get_name() + " v" + get_version();
    }

    /**
     * Loads all HLTPrescaleSet form DB.
     *
     * @return a List with all HLTPresacleSet.
     * @throws java.sql.SQLException
     */
    public static ArrayList<HLTPrescaleSet> loadAll() throws SQLException {
        ArrayList<HLTPrescaleSet> hltSets = new ArrayList<>();
        ConnectionManager mgr = ConnectionManager.getInstance();
        String query = "SELECT HPS_ID "
                + "FROM "
                + "HLT_PRESCALE_SET "
                + "ORDER BY HPS_ID DESC";

        query = mgr.fix_schema_name(query);
        try (PreparedStatement ps = mgr.getConnection().prepareStatement(query); ResultSet rset = ps.executeQuery()) {
            while (rset.next()) {
                HLTPrescaleSet hltps = new HLTPrescaleSet(rset.getInt("HPS_ID"));
                hltSets.add(hltps);
            }
            rset.close();
            ps.close();
        }

        return hltSets;
    }

    /**
     *
     * @return
     * @throws SQLException
     */
    public static ArrayList<String> loadAvailableSetNames() throws SQLException {
        ArrayList<String> hltSets = new ArrayList<>();
        ConnectionManager mgr = ConnectionManager.getInstance();
        String query = "SELECT HPS_ID, HPS_NAME "
                + "FROM "
                + "HLT_PRESCALE_SET "
                + "ORDER BY HPS_ID DESC";

        query = mgr.fix_schema_name(query);
        try (PreparedStatement ps = mgr.getConnection().prepareStatement(query); ResultSet rset = ps.executeQuery()) {
            while (rset.next()) {
                hltSets.add(rset.getInt("HPS_ID") + ": " + rset.getString("HPS_NAME"));
            }
            rset.close();
            ps.close();
        }

        return hltSets;
    }

    /**
     *
     * @param hltMenuId
     * @return
     * @throws SQLException
     */
    public static ArrayList<String> loadAvailableSetNames(final int hltMenuId) throws SQLException {
        ArrayList<String> hltSets = new ArrayList<>();
        ConnectionManager mgr = ConnectionManager.getInstance();
        String query = "select * from (select  HTM2PS_PRESCALE_SET_ID, HTM2PS_TRIGGER_MENU_ID FROM HLT_TM_TO_PS "
                + "where  HTM2PS_TRIGGER_MENU_ID=?) lhs\n"
                + "join (select HPS_ID, HPS_NAME from HLT_PRESCALE_SET order by HPS_ID DESC) rhs\n"
                + "on HTM2PS_PRESCALE_SET_ID=HPS_ID";
        query = mgr.fix_schema_name(query);
        try (PreparedStatement ps = mgr.getConnection().prepareStatement(query)) {
            ps.setInt(1, hltMenuId);
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                hltSets.add(rset.getInt("HPS_ID") + ": " + rset.getString("HPS_NAME"));
            }
            rset.close();
            ps.close();
        }

        return hltSets;
    }

    /**
     * Loads all hlt prescale sets linked to the HLT Trigger Menu with id
     * hltMenuId.
     *
     * @param hltMenuId the id of the menu to query.
     * @return the list of the hlt prescale set linked to the HLT Trigger Menu
     * with id hltMenuId.
     * @throws java.sql.SQLException
     */
    public static ArrayList<HLTPrescaleSet> loadAll(final int hltMenuId) throws SQLException {
        ArrayList<HLTPrescaleSet> hltSets = new ArrayList<>();
        ConnectionManager mgr = ConnectionManager.getInstance();
        String query = "SELECT HPS_ID, HTM2PS_PRESCALE_SET_ID, "
                + "HTM2PS_TRIGGER_MENU_ID "
                + "FROM HLT_PRESCALE_SET, HLT_TM_TO_PS "
                + "WHERE "
                + "HTM2PS_TRIGGER_MENU_ID=? "
                + "AND "
                + "HTM2PS_PRESCALE_SET_ID=HPS_ID "
                + "ORDER BY HPS_ID DESC";
        query = mgr.fix_schema_name(query);
        try (PreparedStatement ps = mgr.getConnection().prepareStatement(query)) {
            ps.setInt(1, hltMenuId);
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                HLTPrescaleSet hltps = new HLTPrescaleSet(rset.getInt("HPS_ID"));
                hltSets.add(hltps);
            }
            rset.close();
            ps.close();
        }

        return hltSets;
    }
    /**
     * object to compare HLT prescales. First compares level, then chain
     * counter, then prescale, then pass through.
     */
    private static final Comparator<HLTPrescale> HLTPRESCALE_COMPARATOR = new Comparator<HLTPrescale>() {

        @Override
        public int compare(HLTPrescale ps1, HLTPrescale ps2) {
            if (ps1.get_type().equals(ps2.get_type())) { //same level
                // first sort by chain counter
                if (ps1.get_chain_counter() < ps2.get_chain_counter()) {
                    return -1;
                } else if (ps1.get_chain_counter() > ps2.get_chain_counter()) {
                    return +1;
                }
                // now by prescale
                int pscomp = ps1.get_value().compareTo(ps2.get_value());
                if (pscomp != 0) {
                    return pscomp;
                }
                // now by pass throuth
                int passcomp = ps1.get_condition().compareTo(ps2.get_condition());

                return passcomp;

            } else { //different level
                return -1 * ps1.get_type().compareTo(ps2.get_type());

            }

        }//compare
    };//HLTPRESCALE_COMPARATOR

    @Override
    public String getTableName() {
        return "HLT_PRESCALE_SET";
    }

    /**
     *
     * @param chainCounter
     * @return
     * @throws SQLException
     */
    public ArrayList<HLTPrescale> GetPrescalesForChain(int chainCounter) throws SQLException {
        ArrayList<HLTPrescale> list = new ArrayList<>();
        for (HLTPrescale p : allPrescales) {
            if (p.get_chain_counter() == chainCounter) {
                list.add(p);
            }
        }

        return list;
    }
}
