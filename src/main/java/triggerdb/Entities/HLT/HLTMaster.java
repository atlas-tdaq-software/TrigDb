package triggerdb.Entities.HLT;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;
import java.util.logging.Level;
import javax.swing.tree.DefaultMutableTreeNode;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;


/**
 * HLT Master Table.
 * Root of an HLT configuration.
 * 
 * @author Paul Bell
 * @author Simon Head
 */
public final class HLTMaster extends AbstractTable {

    ///Reference to the trigger menu table.
    private HLTTriggerMenu thisHLTTriggerMenu;
    ///Reference to the setup.
    private HLTSetup thisHLTSetup;
    private DefaultMutableTreeNode menuLayer;
    private DefaultMutableTreeNode setupLayer;

    ///Default constructor.

    /**
     *
     */
    public HLTMaster() {
        super("HMT_");
        DefineKeyValuePairs();
    }
    
    /**
     *
     * @param input_id
     * @throws SQLException
     */
    public HLTMaster(int input_id) throws SQLException {
        super(input_id, "HMT_");
        DefineKeyValuePairs();

        if (input_id > 0) {
            forceLoad();
        }
    }

    private void DefineKeyValuePairs() {
        keyValue.putFirst("COMMENT", "", "Comment");
        keyValue.putFirst("TRIGGER_MENU_ID", -1, "Trigger Menu ID");
        keyValue.putFirst("SETUP_ID", -1, "Setup ID");
        keyValue.putFirst("STATUS", -1, "Status");
    }

    
    /**
     * Force the record to be read from the database again.
     * After an edit we need to force the record to read from the database.
     * @throws java.sql.SQLException
     */
    @Override
    public void forceLoad() throws SQLException {
        thisHLTTriggerMenu = null;
        menuLayer = null;
        setupLayer = null;
        ConnectionManager.getInstance().forceLoad(this);
    }

    ///Get the ID for the setup record.

    /**
     *
     * @return
     */
    public Integer get_setup_id() {
        return (Integer) keyValue.get("SETUP_ID");
    }
    ///Set the ID for the setup record.

    /**
     *
     * @param inp
     */
    public void set_setup_id(int inp) {
        keyValue.put("SETUP_ID", inp);
    }

    ///get the HLT Menu ID.

    /**
     *
     * @return
     */
    public Integer get_menu_id() {
        return (Integer) keyValue.get("TRIGGER_MENU_ID");
    }
    ///Set the HLT menu ID.

    /**
     *
     * @param inp
     */
    public void set_menu_id(int inp) {
        keyValue.put("TRIGGER_MENU_ID", inp);
    }

    ///Get the trigger menu object.

    /**
     *
     * @return
     * @throws SQLException
     */
    public HLTTriggerMenu get_menu() throws SQLException {
        if (thisHLTTriggerMenu == null && new HLTTriggerMenu().isValid(get_menu_id())) {
            thisHLTTriggerMenu = new HLTTriggerMenu(get_menu_id());
        }

        return thisHLTTriggerMenu;
    }
    ///Set the trigger menu object.

    /**
     *
     * @param men
     */
    public void set_menu(HLTTriggerMenu men) {
        thisHLTTriggerMenu = men;
    }

    ///Get the L2 setup.

    /**
     *
     * @return
     * @throws SQLException
     */
    public HLTSetup get_setup() throws SQLException {
        if (thisHLTSetup == null && new HLTSetup().isValid(get_setup_id())) {
            thisHLTSetup = new HLTSetup(get_setup_id());
        }
        return thisHLTSetup;
    }
    ///Set the L2 setup.

    /**
     *
     * @param sup
     */
    public void set_setup(HLTSetup sup) {
        thisHLTSetup = sup;
    }

    ///Add this to the tree.
    /**
     * You need to implement this so the tree knows how to display the children.
     * @throws java.sql.SQLException
     */
    @Override
    public void addToTree(DefaultMutableTreeNode treeNode, int counter) throws SQLException {
        --counter;

        if (counter >= 0) {
        
            //menu
            if (menuLayer == null && get_menu() != null) {
                menuLayer = new DefaultMutableTreeNode(get_menu());
                treeNode.add(menuLayer);
            } else if (get_menu() != null) {
                get_menu().addToTree(menuLayer, counter);
            }

            //l2 setup
            if (setupLayer == null && get_setup() != null) {
                setupLayer = new DefaultMutableTreeNode(get_setup());
                treeNode.add(setupLayer);
            } else if (get_setup() != null) {
                get_setup().addToTree(setupLayer, counter);
            }
        }
    }

    ///Create a copy of this record.
    /**
     * When the user clicks the copy button we copy the class.  This contains the code to 
     * set all the variables in the new class to the correct values
     * 
     * Don't increment the version this will be done automagically
     * @return 
     */
    @Override
     public Object clone() {
         HLTMaster copy = new HLTMaster();
         copy.set_comment(get_comment());
         copy.set_name(get_name());
         copy.set_version(get_version());
         copy.set_menu_id(get_menu_id());
         copy.set_setup_id(get_setup_id());
         return copy;
    }

    @Override
    public int doDiff(AbstractTable t, DefaultMutableTreeNode treeNode, java.util.Set<String> linkstoignore) throws SQLException {
        if(linkstoignore==null) linkstoignore = new java.util.HashSet<>();
        linkstoignore.add("TRIGGER_MENU_ID");
        linkstoignore.add("EF_SETUP_ID");
        int ndiff = super.doDiff(t,treeNode,linkstoignore);
        
        if(t instanceof HLTMaster) {
            HLTMaster master = (HLTMaster)t;
            if(!Objects.equals(this.get_menu_id(), master.get_menu_id())) {
                DefaultMutableTreeNode menu = new DefaultMutableTreeNode(master.get_menu().getTableName());
                treeNode.add(menu);
                ndiff += this.get_menu().doDiff(master.get_menu(), menu, null);           
            }
            if(!Objects.equals(this.get_setup_id(), master.get_setup_id())) {
                DefaultMutableTreeNode setup = new DefaultMutableTreeNode("Setup");
                treeNode.add(setup);
                ndiff += this.get_setup().doDiff(master.get_setup(), setup, null);
            }
        }//HLTMaster
        
        return ndiff;
    }
     
    ///Save the master table. No links, so simple.
    /**
     * Contains the code that saves the record to the database.
     * 
     * @return 
     * @throws SQLException Stop on SQL problems.
     */
    @Override
    public int save() throws SQLException {
         boolean found = false;

        String query = "SELECT HMT_ID FROM HLT_MASTER_TABLE WHERE " +
                "HMT_TRIGGER_MENU_ID=? AND " +
                "HMT_SETUP_ID=? AND " +
                "HMT_STATUS=? AND " +
                "HMT_NAME=?";

        query = ConnectionManager.getInstance().fix_schema_name(query);
        try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query)) {
            logger.log(Level.FINER, "{0} {1} {2} {3} {4}", new Object[]{query, get_menu_id(), get_setup_id(), get_status(), get_name()});
            ps.setInt(1, get_menu_id());
            ps.setInt(2, get_setup_id());
            ps.setInt(3, get_status());
            ps.setString(4, get_name());
            
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                set_id(rset.getInt("HMT_ID"));
                found = true;
                break;
            }
            rset.close();
            ps.close();
        }

        if (!found) {
            int version = 1 + ConnectionManager.getInstance().getMaxVersion(getTableName(), tablePrefix, get_name());
            keyValue.put("VERSION", version);
            set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix, get_id(), keyValue));
        } else {
            forceLoad();
        }

        return get_id();
    }

    ///String representation of the object.
    @Override
    public String toString() {
        if (get_id() == -1) {
            return "HLT Master Table: Name=" + get_name();
        }

        return "HLT MASTER: ID=" + get_id() + ", Name=" + get_name() + ", Version=" + get_version();
    }

    ///Get the status of this configuration.

    /**
     *
     * @return
     */
    public Integer get_status() {
        return (Integer) keyValue.get("STATUS");
    }

    ///Set the status of this configuration.

    /**
     *
     * @param inp
     */
    public void set_status(int inp) {
        keyValue.put("STATUS", inp);
    }

    ///Set a comment about this configuration.

    /**
     *
     * @param inp
     */
    public void set_comment(String inp) {
        keyValue.put("COMMENT", inp);
    }

    ///Get the comment about this configuration.

    /**
     *
     * @return
     */
    public String get_comment() {
        return (String) keyValue.get("COMMENT");
    }

    @Override
    public String getTableName() {
        return "HLT_MASTER_TABLE";
    }

    /**
     *
     * @return
     */
    public int findDummy() {
        String query = "SELECT HMT_ID FROM HLT_MASTER_TABLE WHERE HMT_NAME ='dummyHLT'";
        query = ConnectionManager.getInstance().fix_schema_name(query);
        int id = -1;
        try {
            PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
            ResultSet rset = ConnectionManager.executeSelect(ps);
            while (rset.next()) {
               id=rset.getInt("HMT_ID");
            }
            rset.close();
            ps.close();
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return id;
    }
}
