package triggerdb.Entities.HLT;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.tree.DefaultMutableTreeNode;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Connections.DBTechnology;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.HLTLinks.HLTTM_PS;
import triggerdb.Entities.alias.PrescaleSetAlias;

/**
 * The HLT Trigger Menu record.
 *
 * @author Paul Bell
 * @author Simon Head
 * @author Tiago Perez
 */
public final class HLTTriggerMenu extends AbstractTable {
    // DEBUG constant
    //public static int MAXALGOS=10;

    /**
     * All valid prescale sets for this menu.
     */
    private ArrayList<HLTPrescaleSet> prescalesets = null;
    /**
     * All chains for this menu.
     */
    private ArrayList<HLTTriggerChain> chains = null;
    /**
     * All unseeded chains for this menu (since Num unseeded << Num total,
     * better use separate query)
     *
     */
    private ArrayList<HLTTriggerChain> unseeded_chains = null;
    /**
     * All algorithms assoicated with the menu.
     */
    private ArrayList<HLTComponent> all_algos = null;
    /**
     * So the tree knows what it has loaded.
     */
    private ArrayList<DefaultMutableTreeNode> tree_data = new ArrayList<>();
    /**
     * List of Prescale Alias linked to this menu.
     */
    private final ArrayList<PrescaleSetAlias> aliases = new ArrayList<>();
    
    
    /**
     * Default constructor, no db load
     */
    public HLTTriggerMenu() {
        super("HTM_");
        DefineKeyValuePairs();
    }

    /**
     * Default constructor.
     *
     * @param input_id the id of this record on db.
     * @throws java.sql.SQLException
     */
    public HLTTriggerMenu(int input_id) throws SQLException {
        super(input_id, "HTM_");
        DefineKeyValuePairs();
        if (input_id > 0) {
            forceLoad();
        }
    }

    private void DefineKeyValuePairs() {
        keyValue.putFirst("PHASE", "", "Phase");
        keyValue.putFirst("CONSISTENT", Boolean.FALSE, "Consistent");
    }
    
    /**
     * Initialize the menu by name. Uses a static function to query the DB
     * before calling this(id).
     *
     * @param menuName The name of this menu.
     * @throws java.sql.SQLException
     */
    public HLTTriggerMenu(final String menuName) throws SQLException {
        this(HLTTriggerMenu.getIdFromName(menuName));
    }

    /**
     * Loads all elements of this menu from DB to memory. This is much faster
     * than calling all individual getters for each table. Thus is useful before
     * writing XML files or before opening the menu editor panel.
     
     * @throws java.sql.SQLException
     * @see HLT_DBtoXML#write_menu
     * @see ConfigurationLoader
     *
     * TODO Fix loadStreams, Groups & Types.
     */
    public void loadMenu() throws SQLException {
        // Load Menu signatures, elements, algos before writing xml
        long t0 = System.currentTimeMillis();
        this.getChains();
        long t1 = System.currentTimeMillis();
        //this.loadSignatures();
        long t2 = System.currentTimeMillis();
//        this.loadStreams();
//        long t3 = System.currentTimeMillis();
//        this.loadGroups();
//        long t4 = System.currentTimeMillis();
//        this.loadTypes();
        long t5 = System.currentTimeMillis();
        //this.loadElements();
        long t6 = System.currentTimeMillis();
        this.loadTeTe();
        long t7 = System.currentTimeMillis();
        this.getAllAlgorithms();
        long t8 = System.currentTimeMillis();
        String tmsg = "\n Loading HLT Trigger Menu : " + this.get_id();
        tmsg += "\n Time to load chains         " + ((t1 - t0) / 1E3);
        tmsg += "\n Time to load signatures     " + ((t2 - t1) / 1E3);
        tmsg += "\n Time to load elements       " + ((t6 - t5) / 1E3);
        tmsg += "\n Time to load input elements " + ((t7 - t6) / 1E3);
        tmsg += "\n Time to load all algorithms " + ((t8 - t7) / 1E3);
        tmsg += "\n Total  time to load menu " + ((t8 - t0) / 1e3);
        logger.info(tmsg);
    }

    /**
     * After an edit we need to force the record to read from the database.
     *
     * @throws java.sql.SQLException
     */
    @Override
    public void forceLoad() throws SQLException {
        chains = null;
        unseeded_chains = null;
        tree_data = new ArrayList<>();
        ConnectionManager.getInstance().forceLoad(this);
    }

    /**
     * Make the chain think it has already loaded the children.
     * @throws java.sql.SQLException
     */
    public void confuseChains() throws SQLException {
        for (HLTTriggerChain chain : getChains()) {
            chain.confuseLoader();
        }
    }

    /**
     *
     * @return
     * @throws SQLException
     */
    public Set<Integer> getAllAlgoIds() throws SQLException {
        Set<Integer> algoids = new HashSet<>();

        String query = "SELECT HCP_ID FROM HLT_COMPONENT, ";
        query += "HLT_TRIGGER_MENU, "
                + "HLT_TM_TO_TC, "
                + "HLT_TC_TO_TS, "
                + "HLT_TS_TO_TE, "
                + "HLT_TE_TO_CP ";

        String and = " WHERE "
                + "HTM_ID=? "
                + "AND "
                + "HTM_ID=HTM2TC_TRIGGER_MENU_ID "
                + "AND "
                + "HTM2TC_TRIGGER_CHAIN_ID=HTC2TS_TRIGGER_CHAIN_ID "
                + "AND "
                + "HTC2TS_TRIGGER_SIGNATURE_ID=HTS2TE_TRIGGER_SIGNATURE_ID "
                + "AND "
                + "HTS2TE_TRIGGER_ELEMENT_ID=HTE2CP_TRIGGER_ELEMENT_ID "
                + "AND "
                + "HTE2CP_COMPONENT_ID=HCP_ID "
                + "ORDER BY HTE2CP_ALGORITHM_COUNTER ASC";

        query = ConnectionManager.getInstance().fix_schema_name(query + and);

        
        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ps.setInt(1, get_id());
        ResultSet rset = ps.executeQuery();

        while (rset.next()) {
            algoids.add(rset.getInt("HCP_ID"));
        }

        rset.close();
        ps.close();

        return algoids;
    }

    /**
     * Iteratively retrieve all algorithms and tools (children-of-algorithm). To
     * speed up things this will load all algorithms and children from DB before
     * "getting" them to avoid zillions of small DB connections.
     *
     * @return the list of all algorithm with their tool.
     * @throws java.sql.SQLException
     */
    public ArrayList<HLTComponent> getAllAlgorithms() throws SQLException {
        if (this.all_algos == null) {
            this.loadAlgorithms();
            ArrayList<HLTComponent> algorithms = this.getAlgorithms();
            ArrayList<HLTComponent> allAlgorithms = new ArrayList<>(algorithms.size() * 2);
            if (!algorithms.isEmpty()) {
                algorithms = HLTComponent.loadAllChildrenComponents(algorithms);
            }
            for (HLTComponent _alg : algorithms) {
                allAlgorithms.add(_alg);
                allAlgorithms.addAll(_alg.getAllChildComponents());
            }
            this.all_algos = new ArrayList<>(allAlgorithms);
        }
        return this.all_algos;
    }

    /**
     * Iterates over chains, signatures & elements and gets the list of
     * algorithms. You should call HLTTriggerMenu.loadSignatures and
     * loadElements before, otherwise this may be very slow.
     *
     * @return the list of algorithms belonging to this menu.
     *
     */
    private ArrayList<HLTComponent> getAlgorithms() throws SQLException {
        ArrayList<HLTComponent> algos = new ArrayList<>();
        ArrayList<HLTTriggerChain> _chains = this.getChains();
        for (HLTTriggerChain ch : _chains) {
            for (HLTTriggerSignature s : ch.getSignatures()) {
                for (HLTTriggerElement e : s.getElements()) {
                    for (HLTComponent a : e.getAlgorithms()) {
                        algos.add(a);
                    }
                }
            }
        }
        return algos;
    }

    /**
     * Load the algorithms linked to this menu in one query and sets them as
     * children to the elements of this menu. This method does not set any
     * variable.
     *
     * @throws java.sql.SQLException
     */
    public void loadAlgorithms() throws SQLException {
        // The list with all agorithm to return.        
        ArrayList<HLTComponent> algorithms;
        algorithms = new ArrayList<>(4000);
        // A map for internal use with all algos grouped by element Id.
        LinkedHashMap<Integer, ArrayList<HLTComponent>> algoMap =
                new LinkedHashMap<>(4000);
        // Selcts only different row from HLT_TE_TO_CP ...
        String query = "SELECT DISTINCT HCP_ALIAS,  HCP_HASH, HCP_HASHFULL, HCP_ID, "
                + "HCP_NAME, HCP_PY_NAME, HCP_PY_PACKAGE, HCP_TYPE, HCP_VERSION, " // 7
                + "HTE2CP_ALGORITHM_COUNTER, " // 8
                + "HTE2CP_TRIGGER_ELEMENT_ID, " // 9
                + "HTE2CP_ID" // 10
                + " FROM HLT_TM_TO_TC, "
                + " HLT_TC_TO_TS, "
                + " HLT_TS_TO_TE, "
                + " HLT_TE_TO_CP, "
                + " HLT_COMPONENT "
                + " WHERE HTM2TC_TRIGGER_MENU_ID=? "
                + "AND HTM2TC_TRIGGER_CHAIN_ID=HTC2TS_TRIGGER_CHAIN_ID "
                + "AND HTC2TS_TRIGGER_SIGNATURE_ID=HTS2TE_TRIGGER_SIGNATURE_ID "
                + "AND HTS2TE_TRIGGER_ELEMENT_ID=HTE2CP_TRIGGER_ELEMENT_ID "
                + "AND HTE2CP_COMPONENT_ID=HCP_ID "
                + "ORDER BY HTE2CP_TRIGGER_ELEMENT_ID ASC"
                + ", HTE2CP_ALGORITHM_COUNTER ASC";

        ConnectionManager mgr = ConnectionManager.getInstance();
        query = mgr.fix_schema_name(query);
        int numberOfRows = 0;
        PreparedStatement ps = mgr.getConnection().prepareStatement(query);
        ps.setInt(1, get_id());
        ResultSet rset = ps.executeQuery();
        rset.setFetchSize(500);
        while (rset.next()) {
            numberOfRows++;
            Integer elemId = rset.getInt("HTE2CP_TRIGGER_ELEMENT_ID");
            ArrayList<HLTComponent> algos = algoMap.get(elemId);
            if (algos == null) {
                algos = new ArrayList<>(5);
            }
            HLTComponent algorithm = new HLTComponent(-1);
            algorithm.loadFromRset(rset);
            algorithm.set_algorithm_counter(rset.getInt("HTE2CP_ALGORITHM_COUNTER"));
            algos.add(algorithm);
            algoMap.put(elemId, algos);
            //
            algorithms.add(algorithm);
        }
        rset.close();
        ps.close();
        // Get the map of all signatures of this menu.
        ArrayList<HLTTriggerElement> elements = this.getElements();
        ArrayList<HLTTriggerElement> elementsSorted = new ArrayList<>(elements);
        //Collections.sort(elementsSorted);
        // assign the DB algorithms to this menu elements.
        int prev = -1;
        for (HLTTriggerElement el : elementsSorted) {
            Integer elementId = el.get_id();
            ArrayList<HLTComponent> algos = algoMap.get(elementId);
            if (algos != null) {
                if (elementId != prev) {
                    el.setAlgorithms(algos);
                } else {
                    // Deep copy of the algos.
                    ArrayList<HLTComponent> v2 = new ArrayList<>(algos.size());
                    for (HLTComponent c : algos) {
                        v2.add((HLTComponent) c.clone());
                    }
                    el.setAlgorithms(v2);
                }
            } else {
                el.setAlgorithms(new ArrayList<HLTComponent>());
            }
            prev = elementId;
        }
    }

    /**
     * Load all the elements in this menu and assign them to this menu's
     * signatures.
     * @throws java.sql.SQLException
     */
    public void loadElements() throws SQLException {
        String query = "SELECT DISTINCT ";
                if(ConnectionManager.TopoStartFromColumnExists == 1){
                    //query+= "HTE_TOPO_START_FROM, "; //1?
                }
                query+= "HTE_HASH, " // 1/2
                + "HTE_ID, " // 2/3
                + "HTE_NAME, " // 3/4
                + "HTE_VERSION, " // 4/5
                + "HTS2TE_TRIGGER_SIGNATURE_ID, " // 5/6
                + "HTS2TE_ELEMENT_COUNTER, " // 6/7
                + "HTS2TE_ID  " // 7/8
                + "FROM HLT_TM_TO_TC, "
                + "HLT_TC_TO_TS, "
                + "HLT_TS_TO_TE, "
                + "HLT_TRIGGER_ELEMENT "
                + "WHERE HTM2TC_TRIGGER_MENU_ID=? "
                + "AND HTM2TC_TRIGGER_CHAIN_ID=HTC2TS_TRIGGER_CHAIN_ID "
                + "AND HTS2TE_TRIGGER_SIGNATURE_ID=HTC2TS_TRIGGER_SIGNATURE_ID "
                + "AND HTS2TE_TRIGGER_ELEMENT_ID=HTE_ID "
                + "ORDER BY HTS2TE_TRIGGER_SIGNATURE_ID ASC, "
                + "HTS2TE_ELEMENT_COUNTER ASC";
        ConnectionManager mgr = ConnectionManager.getInstance();
        query = mgr.fix_schema_name(query);

        LinkedHashMap<Integer, ArrayList<HLTTriggerElement>> elementsMap =
                new LinkedHashMap<>();
        PreparedStatement ps = mgr.getConnection().prepareStatement(query);
        ps.setInt(1, get_id());
        ResultSet rset = ps.executeQuery();

        while (rset.next()) {
            Integer sigId = rset.getInt("HTS2TE_TRIGGER_SIGNATURE_ID");
            ArrayList<HLTTriggerElement> elements = elementsMap.get(sigId);

            if (elements == null) {
                elements = new ArrayList<>();
            }

            HLTTriggerElement element = new HLTTriggerElement();
            element.loadFromRset(rset);
            element.set_element_counter(rset.getInt("HTS2TE_ELEMENT_COUNTER"));
            elements.add(element);
            elementsMap.put(sigId, elements);
        }
        rset.close();
        ps.close();

        // Get the list of all signatures of this menu.
        List<HLTTriggerSignature> signatures = this.getSignatures();
        Collections.sort(signatures);
        // assign the DB elements to this menu signatures.
        int prev = -1;
        for (HLTTriggerSignature sig : signatures) {
            Integer sigId = sig.get_id();
            ArrayList<HLTTriggerElement> elements = elementsMap.get(sigId);
            if (elements != null) {
                if (sigId != prev) {
                    sig.setElements(elements);
                } else {
                    // Dont reuse objects, create copies. So in case of save, 
                    // we only edit this one entry.
                    ArrayList<HLTTriggerElement> clone = new ArrayList<>();
                    for (HLTTriggerElement e : elements) {
                        HLTTriggerElement e2 = e.clone_simple();
                        clone.add(e2);
                    }
                    sig.setElements(clone);
                }
            } else {
                sig.setElements(new ArrayList<HLTTriggerElement>());
            }
            prev = sigId;
        }
    }

    /**
     * Load the list of chainCounters that have express streams.
     * @return 
     * @throws java.sql.SQLException 
     */
    public final ArrayList<Integer> loadExpressStreams() throws SQLException {
        int menuId = this.get_id();
        ArrayList<Integer> expressIds = new ArrayList<>();
        ArrayList<Integer> expressChains = new ArrayList<>();

        String query = (new HLTTriggerStream(-1)).getQueryString();

        ConnectionManager mgr = ConnectionManager.getInstance();
        Connection con = ConnectionManager.getInstance().getConnection();

        // Query express Streams        
        query += " WHERE HTR_TYPE=?";
        query = mgr.fix_schema_name(query);

        PreparedStatement ps = con.prepareStatement(query);
        ps.setString(1, "express");
        ResultSet rset = ps.executeQuery();
        while (rset.next()) {
            int exprStrId = rset.getInt("HTR_ID");
            expressIds.add(exprStrId);
        }
        rset.close();
        ps.close();

        if (expressIds.size() > 100) {
            logger.severe("too many express streams... breaking");
            return null;
        }

        query = "SELECT htc_id, HTC2TR_TRIGGER_STREAM_ID, htc_name, htc_chain_counter "
                + " FROM HLT_TM_TO_TC, HLT_TRIGGER_CHAIN, HLT_TC_TO_TR"
                + " WHERE HTM2TC_TRIGGER_MENU_ID= ? "
                + " AND HTM2TC_TRIGGER_CHAIN_ID=HTC_ID"
                + " AND HTC_ID=HTC2TR_TRIGGER_CHAIN_ID"
                + " AND HTC2TR_TRIGGER_STREAM_ID IN (";
        query += ConnectionManager.preparePlaceHolders(expressIds.size()) + ")";
        query = mgr.fix_schema_name(query);

        ps = con.prepareStatement(query);
        //ps.setInt(1, menuId);
        expressIds.add(0, menuId);
        ConnectionManager.setValues(ps, expressIds.toArray());
        rset = ps.executeQuery();

        while (rset.next()) {
            int count = rset.getInt("HTC_CHAIN_COUNTER");
            if (!expressChains.contains(count)) {
                expressChains.add(count);
            }
        }
        rset.close();
        ps.close();

        return expressChains;
    }

    /**
     * Load all the groups in one query. Requires chains to be loaded.
     * @throws java.sql.SQLException
     */
    public void loadGroups() throws SQLException {
        String query = (new HLTTriggerGroup()).getQueryString();
        query = query.replace("FROM ", ", HTM2TC_TRIGGER_CHAIN_ID FROM HLT_TRIGGER_MENU, HLT_TM_TO_TC, ");

        String and = " WHERE "
                + "HTM_ID=? "
                + "AND "
                + "HTM2TC_TRIGGER_MENU_ID=HTM_ID "
                + "AND "
                + "HTM2TC_TRIGGER_CHAIN_ID=HTG_TRIGGER_CHAIN_ID";

        query = ConnectionManager.getInstance().fix_schema_name(query + and);

        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ps.setInt(1, get_id());
        ResultSet rset = ps.executeQuery();

        while (rset.next()) {
            ArrayList<HLTTriggerChain> addtothese = new ArrayList<>();
            for (HLTTriggerChain chain : getChains()) {
                if (chain.get_id() == rset.getInt("HTM2TC_TRIGGER_CHAIN_ID")) {
                    addtothese.add(chain);
                }
            }

            for (HLTTriggerChain chain : addtothese) {
                HLTTriggerGroup group = new HLTTriggerGroup();
                group.loadFromRset(rset);
                chain.getTriggerGroups().add(group);
            }
        }

        rset.close();
        ps.close();
    }

    /**
     * Load all the signatures in one query. Note this method will replace the
     * signatures vector for a new fresh one form DB.
     * @throws java.sql.SQLException
     */
    public final void loadSignatures() throws SQLException {
        String LOADSIGNATURESQUERY = "SELECT HTS_HASH, HTS_ID, HTS_LOGIC, "
                + "HTM2TC_TRIGGER_CHAIN_ID, " //3
                + "HTC2TS_SIGNATURE_COUNTER " //4
                + "FROM HLT_TM_TO_TC, HLT_TC_TO_TS, HLT_TRIGGER_SIGNATURE"
                + " WHERE HTM2TC_TRIGGER_MENU_ID=? "
                + "AND HTM2TC_TRIGGER_CHAIN_ID=HTC2TS_TRIGGER_CHAIN_ID "
                + "AND HTS_ID=HTC2TS_TRIGGER_SIGNATURE_ID "
                + "ORDER BY HTM2TC_TRIGGER_CHAIN_ID ASC, HTC2TS_SIGNATURE_COUNTER ASC";

        ConnectionManager mgr = ConnectionManager.getInstance();
        String query = mgr.fix_schema_name(LOADSIGNATURESQUERY);

        // A map containig the vector of signatures for each chain.
        // K = chain Id.
        Map<Integer, ArrayList<HLTTriggerSignature>> sigMap =
                new LinkedHashMap<>();

        PreparedStatement ps = mgr.getConnection().prepareStatement(query);
        ps.setInt(1, this.get_id());
        ResultSet rset = ps.executeQuery();

        //logger.info(" PS rows " + ps.getFetchSize() + " RSET rows " + rset.getFetchSize());
        while (rset.next()) {
            Integer chainId = rset.getInt("HTM2TC_TRIGGER_CHAIN_ID");
            Integer sigCounter = rset.getInt("HTC2TS_SIGNATURE_COUNTER");
            HLTTriggerSignature signature = new HLTTriggerSignature();
            signature.loadFromRset(rset);
            signature.set_signature_counter(sigCounter);
            //sigs.add(signature);
            ArrayList<HLTTriggerSignature> signatures = sigMap.get(chainId);
            if (signatures == null) {
                signatures = new ArrayList<>();
            }

            signatures.add(signature);
            sigMap.put(chainId, signatures);
        }
        rset.close();
        ps.close();

        // Assign signature to chains.
        ArrayList<HLTTriggerChain> _chains = this.getChains();
        for (HLTTriggerChain ch : _chains) {
            Integer chainId = ch.get_id();
            if (sigMap.containsKey(chainId)) {
                ArrayList<HLTTriggerSignature> signatures = sigMap.get(chainId);
                ch.setSignatures(signatures);
            } else {
                // Set an empty vector to the chains without signatures.
                ch.setSignatures(new ArrayList<HLTTriggerSignature>());
            }
        }


    }

    /**
     * Load all the streams in one query. Requires chains already.
     * @throws java.sql.SQLException
     */
    public void loadStreams() throws SQLException {
        String query = (new HLTTriggerStream(-1)).getQueryString();
        query = query.replace("FROM ", ", HTM2TC_TRIGGER_CHAIN_ID, HTC2TR_TRIGGER_STREAM_PRESCALE FROM HLT_TRIGGER_MENU, HLT_TM_TO_TC, HLT_TC_TO_TR, ");

        String and = " WHERE "
                + "HTM_ID=? "
                + "AND "
                + "HTM2TC_TRIGGER_MENU_ID=HTM_ID "
                + "AND "
                + "HTM2TC_TRIGGER_CHAIN_ID=HTC2TR_TRIGGER_CHAIN_ID "
                + "AND "
                + "HTR_ID=HTC2TR_TRIGGER_STREAM_ID";

        query = ConnectionManager.getInstance().fix_schema_name(query + and);

        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ps.setInt(1, get_id());
        ResultSet rset = ps.executeQuery();

        while (rset.next()) {
            ArrayList<HLTTriggerChain> addtothese = new ArrayList<>();
            for (HLTTriggerChain chain : getChains()) {
                if (chain.get_id() == rset.getInt("HTM2TC_TRIGGER_CHAIN_ID")) {
                    addtothese.add(chain);
                }
            }

            for (HLTTriggerChain chain : addtothese) {
                HLTTriggerStream stream = new HLTTriggerStream();
                stream.loadFromRset(rset);
                stream.set_stream_prescale(rset.getString("HTC2TR_TRIGGER_STREAM_PRESCALE"));
                chain.getStreams().add(stream);
            }
        }

        rset.close();
        ps.close();
    }

    /**
     * Load all the input trigger elements in one query.
     * @throws java.sql.SQLException
     */
    public void loadTeTe() throws SQLException {
        String query = "SELECT DISTINCT HTE2TE_ID"
                + ", HTE2TE_TE_COUNTER, HTE2TE_TE_ID, HTE2TE_TE_INP_ID, HTE2TE_TE_INP_TYPE"
                + " FROM HLT_TM_TO_TC"
                + ", HLT_TC_TO_TS"
                + ", HLT_TS_TO_TE"
                + ", HLT_TE_TO_TE \n"
                + "WHERE HTM2TC_TRIGGER_MENU_ID=? \n"
                + "AND HTM2TC_TRIGGER_CHAIN_ID=HTC2TS_TRIGGER_CHAIN_ID \n"
                + "AND HTS2TE_TRIGGER_SIGNATURE_ID=HTC2TS_TRIGGER_SIGNATURE_ID \n"
                + "AND HTS2TE_TRIGGER_ELEMENT_ID=HTE2TE_TE_ID \n"
                + "ORDER BY HTE2TE_TE_ID ASC, "
                + "HTE2TE_TE_COUNTER ASC";
        ConnectionManager mgr = ConnectionManager.getInstance();
        query = mgr.fix_schema_name(query);
        //
        // Produce a map with all elements in menu. This map uses Elemenet_id as
        // key, and contains a list of all elements with that id.
        //
        //// 1. Get a list with all elements.
        ArrayList<HLTTriggerElement> elements = new ArrayList<>();
        for (HLTTriggerChain chain : this.getChains()) {
            for (HLTTriggerSignature sig : chain.getSignatures()) {
                for (HLTTriggerElement element : sig.getElements()) {               
                    elements.add(element);
                }
            }
        }
        
        //// Sort the elements list by id.
        //does nothing as element has no comparitor
        //Collections.sort(elements);
        //// Assign elements in list to map.
        HashMap<Integer, ArrayList<HLTTriggerElement>> eMap = new HashMap<>();
        for (HLTTriggerElement el : elements) {
            Integer id = el.get_id();
            ArrayList<HLTTriggerElement> eList = eMap.get(id);
            if (eList == null) {
                eList = new ArrayList<>();
            }
            eList.add(el);
            eMap.put(id, eList);
        }

        logger.log(Level.INFO, "We have {0}", eMap.size());
        PreparedStatement ps = mgr.getConnection().prepareStatement(query);
        ps.setInt(1, get_id());
        ResultSet rset = ps.executeQuery();
        // it's a long query, increase fetch size to speed up things.
        rset.setFetchSize(500);
        //       
        int j = 0;
        while (rset.next()) {
            j++;
            Integer elementId = rset.getInt("HTE2TE_TE_ID");
            
            HLTTE_TE te = new HLTTE_TE(rset.getInt("HTE2TE_ID"));
            te.loadFromRset(rset);
            te.set_element_counter(rset.getInt("HTE2TE_TE_COUNTER"));
            ArrayList<HLTTriggerElement> eList = eMap.get(elementId);
            for (HLTTriggerElement element : eList) {
                element.addTeTeLink(te);
                //element.getInputElements().add(te);
            }
        }
        //
        rset.close();
        ps.close();
    }

    /**
     * Load all the types for the entire menu in one query.
     * @throws java.sql.SQLException
     */
    public final void loadTypes() throws SQLException {
        String query = (new HLTTriggerType(-1)).getQueryString();
        query = query.replace("FROM ", ", HTM2TC_TRIGGER_CHAIN_ID FROM HLT_TRIGGER_MENU, HLT_TM_TO_TC, ");

        String and = " WHERE "
                + "HTM_ID=? "
                + "AND "
                + "HTM2TC_TRIGGER_MENU_ID=HTM_ID "
                + "AND "
                + "HTM2TC_TRIGGER_CHAIN_ID=HTT_TRIGGER_CHAIN_ID";

        query = ConnectionManager.getInstance().fix_schema_name(query + and);

        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ps.setInt(1, get_id());
        ResultSet rset = ps.executeQuery();

        while (rset.next()) {
            ArrayList<HLTTriggerChain> addtothese = new ArrayList<>();
            for (HLTTriggerChain chain : getChains()) {
                if (chain.get_id() == rset.getInt("HTM2TC_TRIGGER_CHAIN_ID")) {
                    addtothese.add(chain);
                }
            }

            for (HLTTriggerChain chain : addtothese) {
                HLTTriggerType type = new HLTTriggerType(-1);
                type.loadFromRset(rset);
                chain.getTriggerTypes().add(type);
            }
        }

        rset.close();
        ps.close();
    }

    /**
     * Get the phase fro
     * @return m the menu.
     */
    public String get_phase() {
        return (String) keyValue.get("PHASE");
    }

    /**
     * Set the phase in the menu.
     * @param inp
     */
    public void set_phase(String inp) {
        Object put = keyValue.put("PHASE", inp);
    }

    /**
     * Get the value of the consistent flag.
     * @return 
     */
    public Boolean get_consistent() {
        return (Boolean) keyValue.get("CONSISTENT");
    }

    /**
     * Get the list of PrescaleAlias linked to this menu.
     * @return the list of <code>PrescaleAlias</code> linked to this menu.
     * @throws java.sql.SQLException
     */
    public List<PrescaleSetAlias> getAliases() throws SQLException {
        if (this.aliases.isEmpty()) {
            this.loadAliases();
        }
        return this.aliases;
    }
    
    /**
     * Load all the valid prescale sets for this menu.
     * @return 
     * @throws java.sql.SQLException 
     */
    public ArrayList<HLTPrescaleSet> getPrescaleSets() throws SQLException {
        if (prescalesets == null) {
            prescalesets = new ArrayList<>();
        }
        if(this.prescalesets.isEmpty()){
            if (get_id() > 0) {
                String query = new HLTPrescaleSet().getQueryString();
                query = query.replace("FROM ", "FROM HLT_TM_TO_PS, ");
                String and = " WHERE "
                        + "HTM2PS_TRIGGER_MENU_ID=? "
                        + "AND HTM2PS_PRESCALE_SET_ID=HPS_ID "
                        + "ORDER BY HPS_ID DESC";

                ConnectionManager mgr = ConnectionManager.getInstance();
                query = mgr.fix_schema_name(query + and);
                logger.fine(query);
                PreparedStatement ps = mgr.getConnection().prepareStatement(query);
                ps.setInt(1, get_id());
                ResultSet rset = ps.executeQuery();

                while (rset.next()) {
                    HLTPrescaleSet psset = new HLTPrescaleSet();
                    psset.loadFromRset(rset);
                    prescalesets.add(psset);
                }

                rset.close();
                ps.close();
            }
        }
        return prescalesets;
    }

    /**
     * Link a new prescale set to this menu.
     *
     * @param hps the new prescale set to link.
     * @param forceVissible if prescale set already exist, ensure that it is
     * visible.
     * @return
     * <code>true</code> if prescale set already exist.
     * @throws java.sql.SQLException
     */
    public boolean addPrescaleSet(final HLTPrescaleSet hps, boolean forceVissible) throws SQLException {
        int menuId = this.get_id();
        int psId = hps.get_id();

        boolean exists = this.checkPrescaleExist(hps);
        if (!exists) {
            HLTTM_PS newlink = new HLTTM_PS(hps, this);
            newlink.save();
            this.prescalesets.clear();
            logger.log(Level.INFO, " Prescale \"{0}\" added to menu \"{1} \" ", new Object[]{hps, this});
        } else {
            HLTTM_PS.unhidePS(menuId, psId);
            logger.log(Level.INFO, " Prescale {0} already linked to HLT Trigger Menu {1}", new Object[]{hps, this});
        }        
        return exists;
    }

    /**
     * Check whether the given PrescaleSet is already linked to this menu.
     *
     * @param hps the HLT PrescaleSet to check.
     * @return
     * <code>true</code> if PrescaleSet already linked to this menu.
     */
    private boolean checkPrescaleExist(final HLTPrescaleSet hps) throws SQLException {
        boolean exists = false;
        int psId = hps.get_id();
        for (HLTPrescaleSet _ps : this.getPrescaleSets()) {
            if (_ps.get_id() == psId) {
                exists = true;
                break;
            }
        }
        return exists;
    }
    
    public boolean checkPrescaleExistPublic (final HLTPrescaleSet hps) throws SQLException {
        return checkPrescaleExist(hps);
    }

    /**
     * Mark this menu and children as consistent.
     * @param inp
     */
    public final void set_consistent(final Boolean inp) {
        keyValue.put("CONSISTENT", inp);
    }

    /**
     * Add children to the tree.
     * @throws java.sql.SQLException
     */
    @Override
    public final void addToTree(DefaultMutableTreeNode treeNode, int counter) throws SQLException {
        --counter;

        if (counter >= 0) {

            if (tree_data.isEmpty()) {
                for (HLTTriggerChain chain : getChains()) {
                    DefaultMutableTreeNode anotherLayer =
                            new DefaultMutableTreeNode(chain);
                    treeNode.add(anotherLayer);
                    tree_data.add(anotherLayer);
                }
            } else {
                int i = 0;
                for (HLTTriggerChain chain : getChains()) {
                    chain.addToTree(tree_data.get(i), counter);
                    ++i;
                }
            }
        }
    }

    /**
     * Make a copy of the trigger menu. When the user clicks the copy button we
     * copy the class. This contains the code to set all the variables in the
     * new class to the correct values
     *
     * Don't increment the version this will be done automagically
     * @return 
     */
    @Override
    public Object clone() {
        HLTTriggerMenu copy = new HLTTriggerMenu();
        copy.set_phase(get_phase());
        copy.set_consistent(get_consistent());
        copy.set_id(-1);
        copy.set_name(get_name());
        copy.set_version(get_version());
        
        try {
            for (HLTTriggerChain chain : getChains()) {
                copy.chains.add((HLTTriggerChain) chain.clone());
            }
        } catch (SQLException ex) {
            Logger.getLogger(HLTTriggerMenu.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return copy;
    }

    ///Save has to check the chains too.
    /**
     * Contains the code that saves the record to the database. Note that we
     * don't match prescale sets as they are not children in the same way as
     * chains - two menus can be the same but linked to different pssets
     *
     * @return 
     * @throws SQLException Stop on SQL problems.
     */
    @Override
    public int save() throws SQLException {

        /// Store the name
        String name = get_name();
        //if name is only difference, don't want a new key, so temporarilty remove from map
        keyValue.remove("NAME");

        //get the chains for comparison of candidates
        ArrayList<Integer> chainIds_sorted = new ArrayList<>();
        for (HLTTriggerChain chain : getChains()) {
            chainIds_sorted.add(chain.get_id());
        }
        Collections.sort(chainIds_sorted);

        ArrayList<Integer> htm_nameIDs = ConnectionManager.getInstance().get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");

        //loop over candidates and see if they have same chains
        ArrayList<Integer> htm_nameChainIDs = new ArrayList<>();
        for (Integer ihtmName : htm_nameIDs) {
            ArrayList<Integer> menu_chainsIDs = ConnectionManager.getInstance().get_ItemList("HLT_TM_TO_TC", "HTM2TC", "HTM2TC_TRIGGER_MENU", "HTM2TC_TRIGGER_CHAIN", ihtmName);
            Collections.sort(menu_chainsIDs);
            if (menu_chainsIDs.equals(chainIds_sorted)) {
                htm_nameChainIDs.add(ihtmName);
                break;
            }
        }

        //put these back now
        keyValue.put("NAME", name);

        //Does not already exist
        if (htm_nameChainIDs.isEmpty()) {

            set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix, get_id(), keyValue));

               //save parameters
            ArrayList<TreeMap<String, Object>> table = new ArrayList<>();
            
            //save chains
            for (HLTTriggerChain chain : chains) {
                TreeMap<String, Object> link = new TreeMap<>();
                link.put("TRIGGER_MENU_ID", get_id());
                link.put("TRIGGER_CHAIN_ID", chain.get_id());
                table.add(link);
                //ConnectionManager.getInstance().save("HLT_TM_TO_TC", "HTM2TC_", -1, link);
            }
            TreeMap<Integer, Integer> results = ConnectionManager.getInstance().saveBatch("HLT_TM_TO_TC", "HTM2TC_", 0, table);
            logger.log(Level.FINE, "[SAVE MENU :: new id = {0} ] {1}", new Object[]{get_id(), get_name()});
        } else {
            set_id(htm_nameChainIDs.get(0));
            logger.log(Level.FINE, "[SAVE MENU :: returned id = {0} ] {1}", new Object[]{get_id(), get_name()});
        }

        return get_id();
    }
    
        ///Save has to check the chains too.
    /**
     * Contains the code that saves the record to the database. Note that we
     * don't match prescale sets as they are not children in the same way as
     * chains - two menus can be the same but linked to different pssets
     *
     * @return 
     * @throws SQLException Stop on SQL problems.
     */
    public int compactsave() throws SQLException {

        /// Store the name
        String name = get_name();
        //if name is only difference, don't want a new key, so temporarilty remove from map
        keyValue.remove("NAME");

        //get the chains for comparison of candidates
        ArrayList<Integer> chainIds_sorted = new ArrayList<>();
        for (HLTTriggerChain chain : chains) {
            chainIds_sorted.add(chain.get_id());
        }
        Collections.sort(chainIds_sorted);

        ArrayList<Integer> htm_nameIDs = ConnectionManager.getInstance().get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");

        //loop over candidates and see if they have same chains
        ArrayList<Integer> htm_nameChainIDs = new ArrayList<>();
        for (Integer ihtmName : htm_nameIDs) {
            ArrayList<Integer> menu_chainsIDs = ConnectionManager.getInstance().get_ItemList("HLT_TM_TO_TC", "HTM2TC", "HTM2TC_TRIGGER_MENU", "HTM2TC_TRIGGER_CHAIN", ihtmName);
            Collections.sort(menu_chainsIDs);
            if (menu_chainsIDs.equals(chainIds_sorted)) {
                htm_nameChainIDs.add(ihtmName);
                break;
            }
        }
        
        //This reduces the number of queries to the DB incase there are multiple
        //candidate Ids.
        ArrayList<int[]> holderParams = null;
        if(htm_nameIDs.size() > 0){
            holderParams = ConnectionManager.getInstance().get_CompactItemList("HLT_TM_TO_TC", "HTM2TC", "HTM2TC_TRIGGER_MENU", "HTM2TC_TRIGGER_CHAIN", htm_nameIDs)
;
        }
        for (Integer ihtmName : htm_nameIDs) {
            ArrayList<Integer> menu_chainsIDs = new ArrayList<>();
            for(int[] i : holderParams){
                if(i[0] == ihtmName) menu_chainsIDs.add(i[1]);
            }
            Collections.sort(menu_chainsIDs);
            if (menu_chainsIDs.equals(chainIds_sorted)) {
                htm_nameChainIDs.add(ihtmName);
                break;
            }
        }

        //put these back now
        keyValue.put("NAME", name);

        //Does not already exist
        if (htm_nameChainIDs.isEmpty()) {

            set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix, get_id(), keyValue));

                    //save parameters
            ArrayList<TreeMap<String, Object>> table = new ArrayList<>();
            
            //save chains
            for (HLTTriggerChain chain : chains) {
                TreeMap<String, Object> link = new TreeMap<>();
                link.put("TRIGGER_MENU_ID", get_id());
                link.put("TRIGGER_CHAIN_ID", chain.get_id());
                table.add(link);
                //ConnectionManager.getInstance().save("HLT_TM_TO_TC", "HTM2TC_", -1, link);
            }
            TreeMap<Integer, Integer> results = ConnectionManager.getInstance().saveBatch("HLT_TM_TO_TC", "HTM2TC_", 0, table);
            logger.log(Level.FINE, "[SAVE MENU :: new id = {0} ] {1}", new Object[]{get_id(), get_name()});
        } else {
            set_id(htm_nameChainIDs.get(0));
            logger.log(Level.FINE, "[SAVE MENU :: returned id = {0} ] {1}", new Object[]{get_id(), get_name()});
        }

        return get_id();
    }

    ///String representation of this object.
    @Override
    public String toString() {
        if (get_id() == -1) {
            return "HLT Menu";
        }

        return "HLT MENU: ID=" + get_id() + ", Name=" + get_name() + ", Version=" + get_version();
    }

    @Override
    public int doDiff(AbstractTable t, DefaultMutableTreeNode treeNode, java.util.Set<String> linkstoignore) throws SQLException {
        int ndiff = super.doDiff(t, treeNode, linkstoignore);

        if (t instanceof HLTTriggerMenu) {
            /// Look for differences in the chains
            HLTTriggerMenu menu = (HLTTriggerMenu) t;
            ArrayList<HLTTriggerChain> chains1 = this.getChains();
            ArrayList<HLTTriggerChain> chains2 = menu.getChains();

            ndiff += triggerdb.Diff.CompareTables.compareTables(chains1, chains2, treeNode);

        }

        return ndiff;
    }

     /**
     * Load all the chains for this menu.
     *
     * @return a Vector with all the trigger chains of this menu sorted by L2/EF
     * and chain counter.
     * @throws java.sql.SQLException
     */
    public ArrayList<HLTTriggerChain> getChains() throws SQLException {
        if (chains == null) {
            chains = new ArrayList<>();
            if (get_id() > 0) {
                ArrayList<HLTTriggerChain> aTabs = getChainsFromMenu();
		/*		for (AbstractTable tab : aTabs) {
                        chains.add(new HLTTriggerChain(tab.get_id()));
			}*/
                chains.addAll(aTabs);
	    }
        }
        return chains;
    }

    private ArrayList<HLTTriggerChain> getChainsFromMenu() throws SQLException {
        String query = new HLTTriggerChain().getQueryString();
        query = query.replace("FROM ", "FROM HLT_TM_TO_TC, ");
        String and = " WHERE "
                + "HTM2TC_TRIGGER_MENU_ID=? "
                + "AND "
                + "HTM2TC_TRIGGER_CHAIN_ID=HTC_ID "
                + "ORDER BY HTC_CHAIN_COUNTER ASC";// +
        ConnectionManager mgr = ConnectionManager.getInstance();
        List<Object> filters = new ArrayList<>();
        filters.add(this.get_id());
        ArrayList<AbstractTable> aTabs = mgr.forceLoadVector(
                new HLTTriggerChain(), query, and, filters);
        return ((ArrayList<HLTTriggerChain>)((Object)aTabs));
    }

    public ArrayList<HLTTriggerChain> getChainsFromMenuPublic() throws SQLException {
        return getChainsFromMenu();
    }      
    /**
     *
     * @param chains
     */
    public void setChains(ArrayList<HLTTriggerChain> chains){
        this.chains = chains;
    }
    
    /**
     * Load all the chains for this menu.
     * @return 
     * @throws java.sql.SQLException 
     */
    public ArrayList<HLTTriggerChain> getUnseededChains() throws SQLException {
        if (unseeded_chains == null) {
            unseeded_chains = new ArrayList<>();
            if (get_id() > 0) {
                String query = (new HLTTriggerChain()).getQueryString();
                query = query.replace("FROM ", "FROM HLT_TRIGGER_MENU, HLT_TM_TO_TC, ");
                String and = " WHERE "
                        + "HTM_ID=? "
                        + "AND "
                        + "HTM2TC_TRIGGER_MENU_ID=HTM_ID "
                        + "AND "
                        + "HTM2TC_TRIGGER_CHAIN_ID=HTC_ID "
                        + "AND "
                        + "HTC_LOWER_CHAIN_NAME = ? "
                        + "ORDER BY HTC_CHAIN_COUNTER ASC";
                ConnectionManager mgr = ConnectionManager.getInstance();
                List<Object> filters = new ArrayList<>();
                filters.add(this.get_id());
                if (mgr.getInitInfo().getTechnology() == DBTechnology.ORACLE) {
                    filters.add("~");
                } else {
                    filters.add("");
                }

                ArrayList<AbstractTable> aTabs = mgr.forceLoadVector(
                        new HLTTriggerChain(), query, and, filters);
                for (AbstractTable tab : aTabs) {
                    unseeded_chains.add((HLTTriggerChain) tab);
                }
            }
        }
        return unseeded_chains;
    }

    /**
     * Queries the DB to find the id of the latest (higher version number) menu
     * with the name menuName
     *
     * @param menuName the name of hte menu to query.
     * @return the database id of the menu menuName.
     * @throws java.sql.SQLException
     */
    public static int getIdFromName(String menuName) throws SQLException {
        int menuId = -1;
        String query = "SELECT HTM_ID,HTM_NAME,HTM_VERSION FROM HLT_TRIGGER_MENU"
                + " WHERE HTM_NAME=? ORDER BY HTM_VERSION DESC";
        query = ConnectionManager.getInstance().fix_schema_name(query);

        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ps.setString(1, menuName);
        ResultSet rset = ps.executeQuery();
        if (rset.next()) {
            menuId = rset.getInt("HTM_ID");
            String name = rset.getString("HTM_NAME");
            int ver = rset.getInt("HTM_VERSION");
            logger.log(Level.INFO, "Retrieving HLT Menu id={0} name={1} ver={2}", new Object[]{menuId, name, ver});
        } else {
            logger.log(Level.SEVERE, " There is no HLT Trigger Menu with name \"{0}\" in this database.", menuName);
        }
        rset.close();
        ps.close();

        return menuId;
    }

    /**
     * Iterates over the chains and signatures of this menu and returns a list
     * with the elements.
     *
     * @return the map with the HLTTriggerElement of this menu.
     */
    private ArrayList<HLTTriggerElement> getElements() throws SQLException {
        ArrayList<HLTTriggerElement> elements = new ArrayList<>();
        //
        List<HLTTriggerSignature> signatures = this.getSignatures();
        for (HLTTriggerSignature sig : signatures) {
            for (HLTTriggerElement el : sig.getElements()) {
//                if (!elements.contains(el)) {
                elements.add(el);
//                }
            }
        }
        return elements;
    }

    /**
     * Iterates over a list of chains and returns a list with all the
     * signatures.
     *
     * @return the list of signatures of the givens chains.
     */
    private ArrayList<HLTTriggerSignature> getSignatures() throws SQLException {
        //final List<HLTTriggerChain> _chains}) {
        ArrayList<HLTTriggerSignature> signatures = new ArrayList<>();
        for (HLTTriggerChain chain : this.getChains()) {
            for (HLTTriggerSignature sig : chain.getSignatures()) {
                signatures.add(sig);
            }
        }
        return signatures;
    }

    @Override
    public String getTableName() {
        return "HLT_TRIGGER_MENU";
    }
        /**
     * Query the DB for the aliases linked to this menu and adds them to
     * <code>this.aliases</code>.
     */
    private void loadAliases() throws SQLException {
        this.aliases.clear();

        String query = "SELECT DISTINCT(HPSA_ALIAS_ID) "
                + "FROM HLT_TM_TO_PS ,HLT_PRESCALE_SET_ALIAS "
                + "WHERE HTM2PS_TRIGGER_MENU_ID=? "
                + "AND HTM2PS_ID=hpsa_htm2ps_id ";
        ConnectionManager mgr = ConnectionManager.getInstance();
        query = mgr.fix_schema_name(query);
        ArrayList<Integer> aliaseIds = new ArrayList<>();

        PreparedStatement ps = mgr.getConnection().prepareStatement(query);
            // Set the menu id
        ps.setInt(1, this.get_id());

        ResultSet rset = ps.executeQuery();
        while (rset.next()) {
            int aliasId = rset.getInt("HPSA_ALIAS_ID");
            aliaseIds.add(aliasId);
        }
        rset.close();
        ps.close();
        
        for (Integer aliasId : aliaseIds) {
            this.aliases.add(new PrescaleSetAlias(aliasId));
        }
    }

}
