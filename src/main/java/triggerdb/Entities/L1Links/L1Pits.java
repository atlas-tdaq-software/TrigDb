package triggerdb.Entities.L1Links;

import java.sql.PreparedStatement;
import java.sql.SQLException;
//import java.util.logging.Logger;

import triggerdb.Entities.AbstractTable;
import triggerdb.Connections.ConnectionManager;

///Hold L1 Pit info from CTP Menu Compiler
/** 
 * Class that conencts to TM to TT, that holds the information created by the
 * trigger menu compiler concerning the Pit Bus information.  One row is created
 * for every Pit that is assigned.
 * 
 * @author Simon Head
 */
public final class L1Pits extends AbstractTable {

    ///ID of the menu to threshold link that this pit row is attached to.
    private final Integer tm_to_tt_id = 0;
    ///Pit number, up to 160 are available.
    private final Integer pit_number = 0;
    ///Threshold bit.
    private final Integer threshold_bit = 0;

    ///Default constructor.
    /**
     * Constructor that reads the row from the database identified by some
     * ID.
     * 
     */
    public L1Pits() {
        super("L1PIT_");
        setKeys();
    }

    /**
     *
     * @param input_id
     * @throws SQLException
     */
    public L1Pits(int input_id) throws SQLException {
        super(input_id, "L1PIT_");
        setKeys();
        if (input_id > 0) {
            forceLoad();
        }
    }

    private void setKeys() {
        keyValue.putFirst("TM_TO_TT_ID", 0, "TM_TO_TT_ID");
        keyValue.putFirst("PIT_NUMBER", 0, "Pit Number");
        keyValue.putFirst("THRESHOLD_BIT", 0, "Threshold Bit");
        keyValue.remove("NAME");
        keyValue.remove("VERSION");
    }
    ///Delete this row from the database.
    /**
     * Removes the pit that the user has an instance of.  The interface to the
     * trigger menu compiler should always take care of this kind of thing.
     * 
     * @throws java.sql.SQLException When something goes wrong.
     */
    public void delete() throws SQLException {
        String query = "DELETE FROM L1_PITS WHERE L1PIT_ID=?";
        query = ConnectionManager.getInstance().fix_schema_name(query);
        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ps.setInt(1, get_id());
        ps.executeUpdate();
        ps.close();
    }

    ///Get the PIT.
    /**
     * Return the Pit number.
     * 
     * @return Pit number.
     */
    public Integer get_pit_number() {
        return (Integer) keyValue.get("PIT_NUMBER");
    }

    ///Set the PIT.
    /**
     * Set the pit number.
     * 
     * @param inp New pit number.
     */
    public void set_pit_number(int inp) {
        keyValue.put("PIT_NUMBER", inp);
    }

    ///Get the threshold bit.
    /**
     * Get the threshold bit for this row.
     * 
     * @return The threshold bit.
     */
    public Integer get_threshold_bit() {
        return (Integer) keyValue.get("THRESHOLD_BIT");
    }

    ///Set the threshold bit.
    /**
     * Set the threshold bit for this pit.
     * 
     * @param inp The threhsold bit.
     */
    public void set_threshold_bit(int inp) {
        keyValue.put("THRESHOLD_BIT", inp);
    }

    ///Get the TM to TT ID.
    /**
     * Get the ID of the tm to tt table that this is linked to.
     * 
     * @return The ID of the tm to tt table this is linked to.
     */
    public Integer get_tm_to_tt_id() {
        return (Integer) keyValue.get("TM_TO_TT_ID");
    }

    ///Set the TM to TT ID.
    /**
     * Set the ID of the tm to tt link.
     * 
     * @param inp Set the ID of the tm to tt link.
     */
    public void set_tm_to_tt_id(int inp) {
        keyValue.put("TM_TO_TT_ID", inp);
    }

    ///Very simple save.
    /**
     * Simple implementaion to save this record to the database. A new ID is
     * always assigned.
     * 
     * @return The ID where this record has been saved.
     * @throws java.sql.SQLException Stop when it all goes wrong.
     */
    @Override
    public int save() throws SQLException {
        set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix, get_id(), keyValue));
        return get_id();
    }

    ///Make a copy of this class but set the id to -1.
    @Override
    public Object clone() {
        L1Pits copy = new L1Pits();
        copy.set_threshold_bit(threshold_bit);
        copy.set_pit_number(pit_number);
        copy.set_tm_to_tt_id(tm_to_tt_id);
        return copy;
    }

    @Override
    public String getTableName() {
        return "L1_PITS";
    }
}