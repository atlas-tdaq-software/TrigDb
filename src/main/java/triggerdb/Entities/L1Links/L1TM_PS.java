package triggerdb.Entities.L1Links;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import triggerdb.Entities.AbstractTable;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.L1.L1Menu;
import triggerdb.Entities.L1.L1Prescale;

/**
 * Link between a menu and prescale set. This is needed since a menu is not
 * copied (no new id) if prescale sets are added or removed
 *
 * @author Paul Bell
 */
public final class L1TM_PS extends AbstractTable {

    /**
     * Get the id of the link to the given Menu and L1 Prescale Set
     *
     * @param l1menu The L1 Menu
     * @param l1ps The L1Prescale
     * @return the id of the link TM <-> PSset.
     * @throws java.sql.SQLException
     */
    public static int getTM2PS(final L1Menu l1menu, final L1Prescale l1ps) throws SQLException {
        int menuId = l1menu.get_id();
        int psId = l1ps.get_id();
        return L1TM_PS.getTM2PS(menuId, psId);
    }

    /**
     * Get the id of the link to the given Menu and L1 Prescale Set
     *
     * @param menuId
     * @param psId
     * @return the id of the link TM <-> PSset.
     * @throws java.sql.SQLException
     */
    public static int getTM2PS(final int menuId, final int psId) throws SQLException {

        int linkid = -1;

        String query = "SELECT L1TM2PS_ID, L1TM2PS_PRESCALE_SET_ID, L1TM2PS_TRIGGER_MENU_ID "
                + "FROM L1_TM_TO_PS "
                + "WHERE L1TM2PS_TRIGGER_MENU_ID=? "
                + "AND L1TM2PS_PRESCALE_SET_ID=? "
                + "ORDER BY L1TM2PS_ID DESC";

        ConnectionManager mgr = ConnectionManager.getInstance();
        query = mgr.fix_schema_name(query);
        PreparedStatement ps = mgr.getConnection().prepareStatement(query);
        ps.setInt(1, menuId);
        ps.setInt(2, psId);
        ResultSet rset = ps.executeQuery();
        while (rset.next()) {
            linkid = rset.getInt("L1TM2PS_ID");
        }
        if (rset.next()) {
            logger.log(Level.SEVERE, "More than one TMPS link found for L1 menu-pss {0} {1}", new Object[]{menuId, psId});
        }
        rset.close();
        ps.close();

        return linkid;
    }

    /**
     * Hide a given L1 Prescale in a L1 Menu.
     *
     * @param menuId the id of the l1 menu
     * @param psId the i of the prescale set.
     * @throws java.sql.SQLException
     */
    public static void hidePS(final int menuId, final int psId) throws SQLException {
        int linkId = L1TM_PS.getTM2PS(menuId, psId);

        //if (jButtonL1Hide.getText().equals("Hide")) {
        String query = "UPDATE L1_TM_TO_PS SET L1TM2PS_USED=1 WHERE L1TM2PS_ID=?";
        //} else {
        // query = "UPDATE L1_TM_TO_PS SET L1TM2PS_USED=0 WHERE L1TM2PS_ID=?";
        //}
        ConnectionManager mgr = ConnectionManager.getInstance();
        query = mgr.fix_schema_name(query);
        PreparedStatement ps = mgr.getConnection().prepareStatement(query);
        ps.setInt(1, linkId);
        ps.executeUpdate();
        ps.close();
    }

    /**
     * Unhide a given L1 Prescale in a L1 Menu.
     *
     * @param menuId the id of the l1 menu
     * @param psId the i of the prescale set.
     * @throws java.sql.SQLException
     */
    public static void unhidePS(final int menuId, final int psId) throws SQLException {
        int linkId = L1TM_PS.getTM2PS(menuId, psId);
        String query = "UPDATE L1_TM_TO_PS SET L1TM2PS_USED=0 WHERE L1TM2PS_ID=?";
        ConnectionManager mgr = ConnectionManager.getInstance();
        query = mgr.fix_schema_name(query);
        PreparedStatement ps = mgr.getConnection().prepareStatement(query);
        ps.setInt(1, linkId);
        ps.executeUpdate();
        ps.close();
    }

    /**
     * Empty constructor.
     */
    public L1TM_PS() {
        super(-1,"L1TM2PS_");
       _L1TM_PS();
    }

    /**
     * Load the record from DB if is > 0.
     * @param tm2psId
     * @throws java.sql.SQLException
     */
    public L1TM_PS(final int tm2psId) throws SQLException {
        super(tm2psId, "L1TM2PS_");
        _L1TM_PS();
        if (tm2psId > 0) {
            forceLoad();
        }
    }    

    /**
     * Set up the KeyValues
     */
    private void _L1TM_PS() {
        keyValue.putFirst("TRIGGER_MENU_ID", -1, "Trigger Menu ID");
        keyValue.putFirst("PRESCALE_SET_ID", -1, "Prescale Set ID");
        keyValue.putFirst("USED", false, "Used");
        
        keyValue.remove("NAME");
        keyValue.remove("VERSION");

    }

    /**Get the menu ID
     * @return .*/
    public Integer get_menu_id() {
        return (Integer) keyValue.get("TRIGGER_MENU_ID");
    }
    
    /**Set the menu ID
     * @param inp.*/
    public void set_menu_id(final Integer inp) {
        keyValue.put("TRIGGER_MENU_ID", inp);
    }

    /** Get the prescale set ID
     * @return .*/
    public Integer get_prescale_set_id() {
        return (Integer) keyValue.get("PRESCALE_SET_ID");
    }
    
    /** Set the menu ID
     * @param inp.*/
    public void set_prescale_set_id(final Integer inp) {
        keyValue.put("PRESCALE_SET_ID", inp);
    }

    
    /**
     * Save the menu to prescale set link
     * Check if this link already exists before saving. This is done at the
     * start using the map of db field and value.
     *
     * @return The new ID if one is assigned, or the ID of the existing record.
     * @throws java.sql.SQLException Stop if we have an SQL problem
     */
    @Override
    public int save() throws SQLException {
        ConnectionManager mgr = ConnectionManager.getInstance();

        ArrayList<Integer> ids = mgr.get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");

        if (ids.size() <= 0) {
            set_id(mgr.save(getTableName(), tablePrefix, get_id(), keyValue));
        } else {
            set_id(ids.get(0));
            forceLoad();
        }

        return get_id();
    }

    /**Clone is not supported here
     * @return .*/
    @Override
    public Object clone() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getTableName() {
        return "L1_TM_TO_PS";
    }
}
