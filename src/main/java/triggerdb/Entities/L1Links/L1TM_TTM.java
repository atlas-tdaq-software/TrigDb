package triggerdb.Entities.L1Links;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.swing.tree.DefaultMutableTreeNode;
import triggerdb.Entities.AbstractTable;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.L1.L1Threshold;

///CTP Monitor counters information.
/**
 * Table that holds information about the CTP monitor counters.  Some information
 * is filled by the user, the rest is populated by the trigger menu compiler.
 * 
 * @author Simon Head
 */
public class L1TM_TTM extends AbstractTable {
    //
    // XML LABELS
    /** COUNTER TAG. */
    public static final String XML_COUNTER_LIST_TAG="TriggerCounterList";
    /** COUNTER TAG. */
    public static final String XML_COUNTER_TAG="TriggerCounter";
    /** Trigger Threshold attribute.*/
    public static final String XML_THRESHOLD_ATTR="triggerthreshold";
    /** Name attribute.*/
    public static final String XML_NAME_ATTR="name";
    /** Multi attribute.*/
    public static final String XML_MULTI_ATTR="multi";
    /** Type attribute.*/
    public static final String XML_TYPE_ATTR="type";
    
    ///Link to threshold this monitor belongs to.
    private L1Threshold trigger_threshold = null;

    ///Default constructor.

    /**
     *
     * @param input_id
     */
    public L1TM_TTM(int input_id) {
        super(-1, "L1TM2TTM_");
        
        keyValue.putFirst("TRIGGER_THRESHOLD_ID", 0, "Trigger Threshold");
        keyValue.putFirst("NAME", new String(), "Name");
        keyValue.putFirst("BUNCH_GROUP_ID", 0, "Bunch Group");
        keyValue.putFirst("MULTIPLICITY", 0, "Multiplicity");
        keyValue.putFirst("INTERNAL_COUNTER", 0, "Internal Counter");
        keyValue.putFirst("COUNTER_TYPE", new String(), "Counter Type");
        keyValue.putFirst("TRIGGER_MENU_ID", 0, "Trigger Menu");
        
        keyValue.remove("VERSION");
        

        
        set_id(input_id);
    }

    ///Get the threshold.

    /**
     *
     * @return
     * @throws SQLException
     */
    public L1Threshold get_threshold() throws SQLException {
        if (trigger_threshold == null) {
            trigger_threshold = new L1Threshold(get_threshold_id());
        }
        return trigger_threshold;
    }
    
    
///Set the threshold.

    /**
     *
     * @param threshold
     */
    public void set_threshold(L1Threshold threshold) {
        trigger_threshold = threshold;
    }
    
    
    
    
    
    ///Get the threshold ID.

    /**
     *
     * @return
     */
    public Integer get_threshold_id() {
        return (Integer) keyValue.get("TRIGGER_THRESHOLD_ID");
    }

    ///Get the counter name.

    /**
     *
     * @return
     */
    public String get_counter_name() {
        return (String) keyValue.get("NAME");
    }

    ///Get the bunch group ID.

    /**
     *
     * @return
     */
    public Integer get_bunch_group_id() {
        return (Integer) keyValue.get("BUNCH_GROUP_ID");
    }

    ///Get the monitor multiplicity.

    /**
     *
     * @return
     */
    public Integer get_multiplicity() {
        return (Integer) keyValue.get("MULTIPLICITY");
    }

    ///Get the internal counter.

    /**
     *
     * @return
     */
    public Integer get_internal_id() {
        return (Integer) keyValue.get("INTERNAL_COUNTER");
    }

    ///Set the monitor type

    /**
     *
     * @return
     */
    public String get_type() {
        return (String) keyValue.get("COUNTER_TYPE");
    }

    ///Set the counter name.

    /**
     *
     * @param inp
     */
    public void set_counter_name(String inp) {
        keyValue.put("NAME", inp);
    }

    ///Set the bunch group ID.

    /**
     *
     * @param inp
     */
    public void set_bunch_group_id(int inp) {
        keyValue.put("BUNCH_GROUP_ID", inp);
    }

    ///Set the monitor internal ID. Set by trigger menu compiler?

    /**
     *
     * @param inp
     */
    public void set_internal_id(int inp) {
       keyValue.put("INTERNAL_COUNTER", inp);
    }

    ///Set the monitor multiplicity.

    /**
     *
     * @param inp
     */
    public void set_multiplicity(int inp) {
        keyValue.put("MULTIPLICITY", inp);
    }

    ///Set the monitor type. CTPMON or something?

    /**
     *
     * @param inp
     */
    public void set_type(String inp) {
       keyValue.put("COUNTER_TYPE", inp);
    }

    ///Set the menu ID.

    /**
     *
     * @param inp
     */
    public void set_menu_id(int inp) {
        keyValue.put("TRIGGER_MENU_ID", inp);
    }
    
    ///Get the menu ID.

    /**
     *
     * @return
     */
    public Integer get_menu_id() {
        return (Integer) keyValue.get("TRIGGER_MENU_ID");
    }

    ///Set the threshold ID.

    /**
     *
     * @param inp
     */
    public void set_threshold_id(int inp) {
        keyValue.put("TRIGGER_THRESHOLD_ID", inp);
    }

    ///Simple save routine.
    /**
     * No clever-ness.  Just save the record if the ID is -1, or update it if the
     * ID is that of an existing entry.
     * 
     * @return ID that was updated or saved.
     * @throws java.sql.SQLException Stop if we have an SQL problem.
     */
    @Override
    public int save() throws SQLException {
        set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix, get_id(), keyValue));
        return get_id();
    }

    ///Not yet supported for monitors.
    @Override
    public Object clone() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    @Override
    public int doDiff(AbstractTable t, DefaultMutableTreeNode treeNode, java.util.Set<String> linkstoignore) throws SQLException {
        if(linkstoignore==null) {
            linkstoignore = new java.util.HashSet<>();
            linkstoignore.add("TRIGGER_MENU_ID");
        }
        int ndiff = super.doDiff(t, treeNode, linkstoignore);

        return ndiff;
    }
    //Check if two monitors are the same.
    @Override
    public boolean equals(Object other) {
        if(other == null){
            return false;
        }
         if(!(other instanceof L1TM_TTM)){
            return false;
        }
        L1TM_TTM tm = (L1TM_TTM) other;

        return tm.get_counter_name().equals(get_counter_name()) &&
                tm.get_multiplicity().equals(get_multiplicity()) &&
                tm.get_type().equals(get_type()) &&
                tm.get_bunch_group_id().equals(get_bunch_group_id());
    }

    ///Required by equals.
    ///But needs to be looked into correctly setting to 7 is wrong!
    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }
    ///TM2TTM internal counters can be changed without changing the ID of this record!

    /**
     *
     * @throws SQLException
     */
    public void replace_TM2TTM_links() throws SQLException {
        String query = "UPDATE L1_TM_TO_TT_MON SET L1TM2TTM_INTERNAL_COUNTER=? WHERE L1TM2TTM_NAME=? AND L1TM2TTM_TRIGGER_MENU_ID=? AND L1TM2TTM_TRIGGER_THRESHOLD_ID=? AND L1TM2TTM_COUNTER_TYPE=? AND L1TM2TTM_BUNCH_GROUP_ID=?";
        query = ConnectionManager.getInstance().fix_schema_name(query);
        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ps.setInt(1, get_internal_id());
        ps.setString(2, get_counter_name());
        ps.setInt(3, get_menu_id());
        ps.setInt(4, get_threshold_id());
        ps.setString(5, get_type());
        ps.setInt(6, get_bunch_group_id());
        ps.executeUpdate();
        ps.close();
    }

    @Override
    public String getTableName() {
        return "L1_TM_TO_TT_MON";
    }
}
