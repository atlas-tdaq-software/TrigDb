package triggerdb.Entities.L1Links;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.tree.DefaultMutableTreeNode;

import triggerdb.Entities.AbstractTable;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.L1.L1Threshold;

///Link between a menu and threshold with cable info.
/**
 * This is the alternative lsink between trigger menu and a threshold.  The user
 * adds thresholds by adding them to items, or adding a forced threshold to the
 * menu.  After this is done the trigger tool, based on some logic, decides on
 * the cable positions for the thresholds and populates the menu with a row
 * in this table for each unique threshold.
 * 
 * @author Simon Head
 */
public class L1TM_TT extends AbstractTable {

    ///Threshold object that belongs to this menu.
    private L1Threshold trigger_threshold;
    ///Vector of L1 Pit tables containg the pit bus information.
    private ArrayList<L1Pits> pits = new ArrayList<>();

    ///Empty constructor.

    /**
     *
     */
    public L1TM_TT() {
        super(-1, "L1TM2TT_");

        keyValue.putFirst("TRIGGER_THRESHOLD_ID", 0, "Trigger Threshold");
        keyValue.putFirst("CABLE_CONNECTOR", new String(), "Cable Connector");
        keyValue.putFirst("CABLE_START", 0, "Cable Start");
        keyValue.putFirst("CABLE_END", 0, "Cable End");
        keyValue.putFirst("CABLE_CLOCK", 0, "Cable Clock");
        keyValue.putFirst("CABLE_NAME", new String(), "Cable Name");
        keyValue.putFirst("CABLE_CTPIN", new String(), "Cable CTP_IN");
        keyValue.putFirst("TRIGGER_MENU_ID", 0, "Trigger Menu");


        keyValue.putFirst("TRIGGER_THRESHOLD_ID", -1, "Trigger Threshold ID");
                
        keyValue.remove("NAME");
        keyValue.remove("VERSION");
    }

    // Helpful function to define full cable properties.

    /**
     *
     * @param name
     * @param type
     * @param slot
     * @param conn
     * @param start
     * @param end
     * @param clock
     * @param thres
     */
    public L1TM_TT(String name, String type, String slot, String conn, int start, int end, int clock, L1Threshold thres) {
        super(-1, "L1TM2TT_");
        keyValue.putFirst("TRIGGER_THRESHOLD_ID", 0, "Trigger Threshold");
        keyValue.putFirst("CABLE_CONNECTOR", new String(), "Cable Connector");
        keyValue.putFirst("CABLE_START", 0, "Cable Start");
        keyValue.putFirst("CABLE_END", 0, "Cable End");
        keyValue.putFirst("CABLE_NAME", new String(), "Cable Name");
        keyValue.putFirst("CABLE_CTPIN", new String(), "Cable CTP_IN");
        keyValue.putFirst("CABLE_CLOCK", new String(), "Cable CTP_IN");
        keyValue.putFirst("TRIGGER_MENU_ID", 0, "Trigger Menu");
        set_cable_name(type);
        set_cable_ctpin(slot);
        set_cable_connector(conn);
        set_cable_start(start);
        set_cable_end(end);
        set_cable_clock(clock);
        set_threshold_id(thres.get_id());
        set_threshold(thres);
        keyValue.remove("NAME");
        keyValue.remove("VERSION");
    }

    ///Get the threshold this cable is assigned to.

    /**
     *
     * @return
     */
    public L1Threshold get_threshold(){
        if (trigger_threshold == null) {
            try {
                trigger_threshold = new L1Threshold(get_threshold_id());
            } catch (SQLException ex) {
                Logger.getLogger(L1TM_TT.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return trigger_threshold;
    }

    ///Get the thrshold ID.

    /**
     *
     * @return
     */
    public Integer get_threshold_id() {
        return (Integer) keyValue.get("TRIGGER_THRESHOLD_ID");
    }

    ///Set the cable connector.

    /**
     *
     * @param inp
     */
    public void set_cable_connector(String inp) {
        keyValue.put("CABLE_CONNECTOR", inp);
    }

    ///Set the cable start bit.

    /**
     *
     * @param inp
     */
    public void set_cable_start(int inp) {
        keyValue.put("CABLE_START", inp);
    }

    ///Set the cable end bit.

    /**
     *
     * @param inp
     */
    public void set_cable_end(int inp) {
        keyValue.put("CABLE_END", inp);
    }

    // Set the cable clock.

    /**
     *
     * @param inp
     */
    public void set_cable_clock(int inp) {
        keyValue.put("CABLE_CLOCK", inp);
    }

    ///Set the cable name.

    /**
     *
     * @param inp
     */
    public void set_cable_name(String inp) {
        keyValue.put("CABLE_NAME", inp);
    }

    ///Set the cable ctp in.

    /**
     *
     * @param inp
     */
    public void set_cable_ctpin(String inp) {
        keyValue.put("CABLE_CTPIN", inp);
    }

    ///Set the menu ID.

    /**
     *
     * @param inp
     */
    public void set_menu_id(int inp) {
        keyValue.put("TRIGGER_MENU_ID", inp);
    }

    ///Set the threshold.

    /**
     *
     * @param threshold
     */
    public void set_threshold(L1Threshold threshold) {
        trigger_threshold = threshold;
    }

    ///Set the threshold ID.

    /**
     *
     * @param inp
     */
    public void set_threshold_id(int inp) {
        keyValue.put("TRIGGER_THRESHOLD_ID", inp);
    }

    ///Get the cable connector.

    /**
     *
     * @return
     */
    public String get_cable_connector() {
        return (String) keyValue.get("CABLE_CONNECTOR");
    }

    ///Get the cable start bit.

    /**
     *
     * @return
     */
    public Integer get_cable_start() {
        return (Integer) keyValue.get("CABLE_START");
    }

    ///Get the cable end bit.

    /**
     *
     * @return
     */
    public Integer get_cable_end() {
        return (Integer) keyValue.get("CABLE_END");
    }

    // Get the cable clock.

    /**
     *
     * @return
     */
    public Integer get_cable_clock() {
        return (Integer) keyValue.get("CABLE_CLOCK");
    }

    ///Get the cable name.

    /**
     *
     * @return
     */
    public String get_cable_name() {
        return (String) keyValue.get("CABLE_NAME");
    }

    ///Get the cable CTP in.

    /**
     *
     * @return
     */
    public String get_cable_ctpin() {
        return (String) keyValue.get("CABLE_CTPIN");
    }

    ///Get the menu ID.

    /**
     *
     * @return
     */
    public int get_menu_id() {
        return (Integer) keyValue.get("TRIGGER_MENU_ID");
    }

    ///Get the pits assigned to this threshold cable map.
    /**
     * A single threshold may have multiple pits assigned to it.  In addition to
     * this the pits are normally consecutive, although the trigger menu compiler
     * does not require this. So we should not count on it.  One query is
     * performed for every instance of TM to TT that wants to know which
     * pits are attached.
     * 
     * @return A vector of L1 Pit tables.
     * @throws java.sql.SQLException
     */
    public ArrayList<L1Pits> get_pits() throws SQLException {
        if (pits.size()==0) {
            String query = "SELECT L1PIT_ID FROM L1_PITS WHERE L1PIT_TM_TO_TT_ID=?";
            query = ConnectionManager.getInstance().fix_schema_name(query);
            try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query)) {
                ps.setInt(1, get_id());
                ResultSet rset = ps.executeQuery();
                while (rset.next()) {
                    pits.add(new L1Pits(rset.getInt("L1PIT_ID")));
                }
                rset.close();
                ps.close();
            }
        }
        return pits;
    }

    ///Save this menu to threshold link containing cable information.
    /**
     * Check if this link already exists before saving.  This is done at the 
     * start using the map of db field and value.  Currently the pit values aren't
     * checked, which could cause a problem in the future.
     * 
     * @return The new ID if one is assigned, or the ID of the existing record.
     * @throws java.sql.SQLException Stop if we have an SQL problem
     */
    @Override
    public int save() throws SQLException {
        
        ArrayList<Integer> ids = ConnectionManager.getInstance().get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");

        if (ids.size() <= 0) {
            set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix, get_id(), keyValue));
        } else {
            //    logger.info("Found this item, getting id and loading");
            set_id(ids.get(0));
            forceLoad();
        }

        return get_id();
    }

    ///Clone is not supported here.
    @Override
    public Object clone() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    ///Equals method used when checking cable maps between menus.
    @Override
    public boolean equals(Object other) {
        if(other == null){
            return false;
        }
        if(!(other instanceof L1TM_TT)){
            return false;
        }
        L1TM_TT tmtt = (L1TM_TT) other;

        return tmtt.get_cable_name().equals(get_cable_name()) &&
                tmtt.get_cable_connector().equals(get_cable_connector()) &&
                tmtt.get_cable_start().equals(get_cable_start()) &&
                tmtt.get_cable_end().equals(get_cable_end()) &&
                tmtt.get_cable_clock().equals(get_cable_clock()) &&
                tmtt.get_cable_ctpin().equals(get_cable_ctpin()) &&
                tmtt.get_threshold().get_name().equals(get_threshold().get_name());
    }

    @Override
    public int doDiff(AbstractTable t, DefaultMutableTreeNode treeNode, java.util.Set<String> linkstoignore) throws SQLException {
         int ndiff = super.doDiff(t, treeNode, linkstoignore);

         if(t instanceof L1TM_TT) {
             L1TM_TT other = (L1TM_TT)t;
             // check L1Pits
             ndiff += triggerdb.Diff.CompareTables.compareTables(this.get_pits(),other.get_pits(),treeNode);
         }


         return ndiff;
    }

    ///Required by the equals method.
    /**
     * We need to implement this if we implement the equals method. Some rules
     * exist on the internet about how this should be done. Use google.
     * 
     * @return Integer hashcode which is the same for identical values.
     */
    @Override
    public int hashCode() {
        int hash = 7;
//        hash = 11 * hash + (this.cable_name != null ? this.cable_name.hashCode() : 0);
//        hash = 11 * hash + (this.cable_ctpin != null ? this.cable_ctpin.hashCode() : 0);
//        hash = 11 * hash + (this.cable_connector != null ? this.cable_connector.hashCode() : 0);
//        hash = 11 * hash + (this.cable_start != null ? this.cable_start.hashCode() : 0);
//        hash = 11 * hash + (this.cable_end != null ? this.cable_end.hashCode() : 0);
        return hash;
    }

    @Override
    public String getTableName() {
        return "L1_TM_TO_TT";
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(1000);
        sb.append("[");
        sb.append(get_cable_name());
        sb.append(" ");
        sb.append(get_threshold());
        sb.append(" ");
        sb.append(get_cable_connector());
        sb.append(" ");
        sb.append(get_cable_ctpin());
        sb.append(" ");
        sb.append(get_cable_start());
        sb.append(" ");
        sb.append(get_cable_end());
        sb.append(" ");
        sb.append(get_cable_clock());
        sb.append("]");
        return sb.toString();
    }

}
