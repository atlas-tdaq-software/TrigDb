package triggerdb.Entities.alias;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;

/**
 * Table containing the aliases: "PHYSICS", "STAND_BY"...
 *
 * TODO (tperez): Need add DEFAULT, COMMENT TODO (tperez): Remove VERSION from
 * SQL
 *
 * @author Tiago Perez <tperez@cern.ch>
 * @version 2012-04-18
 */
public final class PrescaleSetAlias extends AbstractTable {

    private static final String TABLE_NAME = "PRESCALE_SET_ALIAS";
    /**
     * DB Columns.
     */
    private static final String NAME_FREE = "NAME_FREE";
    private static final String DEFAULT = "DEFAULT";
    private static final String COMMENT = "COMMENT";

    /**
     * Minimum Trigger DB schema with alias support.
     */
    //public static final int TRIGGER_DB_ALIAS_VER = 11;

    /**
     * Get the list of all aliases linked to the given SMK.
     *
     * @param smk the SMK
     * @return the list of unique alias id. (only aliases linked to both L1 and
     * HLT Menus!)
     * @throws java.sql.SQLException
     */
    public static List<Integer> getAliasIdList(final int smk) throws SQLException {
        ArrayList<Integer> _aliasIdSetL1 = new ArrayList<>();

        // Get ALias from L1
        String query = "SELECT DISTINCT(l1psa_alias_id) FROM SUPER_MASTER_TABLE,"
                + " L1_MASTER_TABLE,"
                + " L1_TM_TO_PS,"
                + " L1_PRESCALE_SET_ALIAS"
                + " WHERE L1MT_ID=SMT_L1_MASTER_TABLE_ID "
                + "AND L1TM2PS_TRIGGER_MENU_ID=L1MT_TRIGGER_MENU_ID "
                + "AND L1TM2PS_ID=l1psa_l1tm2ps_id "
                + "AND SMT_ID=?";

        ConnectionManager mgr = ConnectionManager.getInstance();
        query = mgr.fix_schema_name(query);

        PreparedStatement ps = mgr.getConnection().prepareStatement(query);
        ps.setInt(1, smk);
        ResultSet rset = ps.executeQuery();
        while (rset.next()) {
            _aliasIdSetL1.add(rset.getInt("l1psa_alias_id"));
        }
        rset.close();
        ps.close();

        ArrayList<Integer> _aliasIdSetHLT = new ArrayList<>();

        // Get Alias Ids from HLT
        query = "SELECT DISTINCT(hpsa_alias_id) FROM SUPER_MASTER_TABLE, "
                + "HLT_MASTER_TABLE, "
                + "HLT_TM_TO_PS, "
                + "HLT_PRESCALE_SET_ALIAS "
                + "WHERE HMT_ID = SMT_HLT_MASTER_TABLE_ID "
                + "AND HTM2PS_TRIGGER_MENU_ID = HMT_TRIGGER_MENU_ID "
                + "AND HTM2PS_ID = hpsa_htm2ps_id "
                + "AND SMT_ID=?";
        query = mgr.fix_schema_name(query);

        ps = mgr.getConnection().prepareStatement(query);
        ps.setInt(1, smk);
        rset = ps.executeQuery();
        while (rset.next()) {
            Integer _aliasId = rset.getInt("hpsa_alias_id");
            _aliasIdSetHLT.add(_aliasId);
        }
        rset.close();
        ps.close();

        _aliasIdSetL1.retainAll(_aliasIdSetHLT);
        Collections.sort(_aliasIdSetL1);
        return _aliasIdSetL1;
    }

    /**
     *
     */
    public PrescaleSetAlias() {
        super(-1, "PSA_");
        DefineKeyValuePairs();
    }

    /**
     *
     * @param input_id
     * @throws SQLException
     */
    public PrescaleSetAlias(final int input_id) throws SQLException {
        super(input_id, "PSA_");
        DefineKeyValuePairs();

        if (input_id > 0) {
            forceLoad();
        }
    }

    private void DefineKeyValuePairs() {
        keyValue.putFirst(NAME_FREE, "", "Alias free description");
        keyValue.putFirst(DEFAULT, Boolean.FALSE, "Default");
        keyValue.putFirst(COMMENT, "", "Alias comment");
        keyValue.putFirst("USERNAME", "nobody", "Username");
        keyValue.putFirst("MODIFIED_TIME", new Timestamp(System.currentTimeMillis()), "Modified");
        keyValue.remove("VERSION");// No Versioning for aliases.
        keyValue.remove("USED"); // No "USED", use "DEFAULT" instead
    }

    public int checkDup() throws SQLException {
        if (ConnectionManager.getInstance() != null) {
            ConnectionManager mgr = ConnectionManager.getInstance();

            List<Integer> ids = mgr.get_IDs(this.getTableName(),
                    this.tablePrefix, this.keyValue, null, "ID");

            if (!ids.isEmpty()) {
                return ids.get(0);
            }
        }
        return -1;
    }

    @Override
    public int save() throws SQLException {
        if (ConnectionManager.getInstance() != null) {
            ConnectionManager mgr = ConnectionManager.getInstance();

            List<Integer> ids = mgr.get_IDs(this.getTableName(),
                    this.tablePrefix, this.keyValue, null, "ID");

            if (ids.isEmpty()) {
                this.set_id(mgr.save(this.getTableName(), this.tablePrefix,
                        this.get_id(), this.keyValue));
            } else {
                this.set_id(ids.get(0));
            }
        }
        return this.get_id();
    }

    @Override
    public Object clone() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getTableName() {
        return PrescaleSetAlias.TABLE_NAME;
    }

    /**
     * @return <code>true</code> if this alias is default.
     */
    public Boolean isDdefault() {
        return this.get_default();
    }

    /**
     * @return <code>true</code> if this alias is default.
     */
    public Boolean get_default() {
        return (Boolean) keyValue.get(DEFAULT);
    }

    /**
     * Set this alias as default.
     *
     * @param def
     */
    public void set_default(final Boolean def) {
        keyValue.put(DEFAULT, def);
    }

    /**
     * Set this alias as default.
     */
    public void set_default() {
        keyValue.put(DEFAULT, Boolean.TRUE);
    }

    /**
     * Unset this alias as default.
     */
    public void unset_default() {
        keyValue.put(DEFAULT, Boolean.FALSE);
    }

    /**
     * Get the Long description of the alias: "HI v1", "1300 bunches_25ns"...
     *
     * @return the long description of the alias.
     */
    public String get_name_free() {
        return (String) keyValue.get(NAME_FREE);
    }

    /**
     * Set the long description of the alias: "HI v1", "1300 bunches_25ns"...
     *
     * @param nameFree the long description of the alias.
     */
    public void set_name_free(final String nameFree) {
        keyValue.put(NAME_FREE, nameFree);
    }

    /**
     * Get the comment.
     *
     * @return the comment.
     */
    public String get_comment() {
        return (String) keyValue.get(COMMENT);
    }

    /**
     * Set a comment.
     *
     * @param comment a comment.
     */
    public void set_comment(final String comment) {
        keyValue.put(COMMENT, comment);
    }

    /**
     * Get the alias name: "Physics", "Stand By", "Other"
     *
     * @return the alias name.
     */
    @Override
    public String get_name() {
        return (String) keyValue.get("NAME");
    }

    /**
     * @return the alias as NAME (NAME_FREE VER / ID)
     */
    @Override
    public String toString() {
        return get_name() + "(" + this.get_name_free()
                //+ " v " + get_version()
                + " / " + this.get_id() + ")";
    }

    /**
     * Compares whether 2 Prescale Set Alias tables are equal. Equal is defined
     * as same NAME, NAME_FREE and ID.
     *
     * @param other a Prescale Set Alias to compare with.
     * @return <code>true</code> if equal.
     */
    @Override
    public boolean equals(Object other) {
        boolean same = false;
        if (other instanceof PrescaleSetAlias) {
            PrescaleSetAlias others = (PrescaleSetAlias) other;

            if (this.get_name().equals(others.get_name())
                    && this.get_name_free().equals(others.get_name_free())
                    && this.get_id() == others.get_id()) {
                same = true;
            }
        }
        return same;
    }

    /**
     * Compare NAME (PHYSICS, STAND BY..), then NAME_FREE (1318b, 678b...).
     *
     * @param t the table to compare to.
     * @return
     */
    @Override
    public int compareTo(final AbstractTable t) {
        int comp = -1;

        if (!this.get_name().equalsIgnoreCase(t.get_name())) {
            comp = this.get_name().compareTo(t.get_name());
        } else {
            if (t instanceof PrescaleSetAlias) {
                String t_name_free = ((PrescaleSetAlias) t).get_name_free();
                comp = this.get_name_free().compareTo(t_name_free);
            }
        }
        return comp;
    }

    /**
     * Get from DB the list of L1PrescaleSetAliasLumi linked to this alias and
     * the given L1Menu.
     *
     * @param menuId the L1Menu id
     * @return the list of L1PrescaleSetAliasLumi
     * @throws java.sql.SQLException
     */
    public List<L1PrescaleSetAliasLumi> getL1PrescaleSetAliases(final int menuId) throws SQLException {
        return L1PrescaleSetAliasLumi.get_prescale_aliases(menuId, this.get_id());
    }

    /**
     * Get from DB the list of L1PrescaleSetAliasLumi linked to this alias and
     * the given L1Menu.
     *
     * @param menuId the L1Menu id
     * @return the list of L1PrescaleSetAliasLumi
     * @throws java.sql.SQLException
     */
    public List<HLTPrescaleSetAliasLumi> getHLTPrescaleSetAliases(final int menuId) throws SQLException {
        return HLTPrescaleSetAliasLumi.get_prescale_aliases(menuId, this.get_id());
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    /**
     *
     * @param _comment
     * @throws SQLException
     */
    public void update_comment(String _comment) throws SQLException {
        this.set_comment(_comment);
        String query = "UPDATE PRESCALE_SET_ALIAS SET PSA_COMMENT=? WHERE PSA_ID=?";
        ConnectionManager mgr = ConnectionManager.getInstance();
        query = mgr.fix_schema_name(query);
        PreparedStatement ps = mgr.getConnection().prepareStatement(query);
        ps.setString(1, _comment);
        ps.setInt(2, this.get_id());
        ps.executeUpdate();
        ps.close();
    }

    /**
     *
     * @return @throws SQLException
     */
    public ArrayList<Integer> get_matchingAliases() throws SQLException {
        ArrayList<Integer> psaIds = new ArrayList<>();
        String query = "SELECT PSA_ID FROM PRESCALE_SET_ALIAS WHERE PSA_NAME=? AND PSA_NAME_FREE=?";
        ConnectionManager mgr = ConnectionManager.getInstance();
        query = mgr.fix_schema_name(query);
        PreparedStatement ps = mgr.getConnection().prepareStatement(query);
        ps.setString(1, get_name());
        ps.setString(2, get_name_free());
        ResultSet rset = ps.executeQuery();
        while (rset.next()) {
            psaIds.add(rset.getInt("PSA_ID"));
        }
        rset.close();
        ps.close();
        return psaIds;
    }

    //Needs to clear the links to older prescale sets.
    /**
     *
     * @param l1_id
     * @param hlt_id
     * @throws SQLException
     */
    public void clearAliases(int l1_id, int hlt_id) throws SQLException {
        //Is the MenuId needeD?
        ConnectionManager mgr = ConnectionManager.getInstance();
        List<L1PrescaleSetAliasLumi> L1PSKs = getL1PrescaleSetAliases(l1_id);
        List<HLTPrescaleSetAliasLumi> HLTPSKs = getHLTPrescaleSetAliases(hlt_id);
        if (L1PSKs.size() > 0) {
            String queryL1 = "DELETE FROM L1_PRESCALE_SET_ALIAS WHERE L1PSA_ID IN (";
            for (L1PrescaleSetAliasLumi L1PSK : L1PSKs) {
                queryL1 += L1PSK.get_id() + ",";
            }
            if (queryL1.endsWith(",")) {
                queryL1 = queryL1.substring(0, queryL1.length() - 1);
            }
            queryL1 += ")";
            queryL1 = mgr.fix_schema_name(queryL1);
            PreparedStatement psL1 = mgr.getConnection().prepareStatement(queryL1);
            psL1.executeUpdate();
            psL1.close();
        }
        if (HLTPSKs.size() > 0) {
            String queryHLT = "DELETE FROM HLT_PRESCALE_SET_ALIAS WHERE HPSA_ID IN (";
            for (HLTPrescaleSetAliasLumi HLTPSK : HLTPSKs) {
                queryHLT += HLTPSK.get_id() + ",";
            }
            if (queryHLT.endsWith(",")) {
                queryHLT = queryHLT.substring(0, queryHLT.length() - 1);
            }
            queryHLT += ")";
            queryHLT = mgr.fix_schema_name(queryHLT);
            PreparedStatement psHLT = mgr.getConnection().prepareStatement(queryHLT);
            psHLT.executeUpdate();
            psHLT.close();
        }
    }

    public static ArrayList<String> getAliasesForSMK(int smk, String type) throws SQLException {
        ArrayList<String> results = new ArrayList<>();
        ConnectionManager mgr = ConnectionManager.getInstance();

        String query = "select SMT_ID,uber.PSA_ID,PSA_NAME,PSA_NAME_FREE,PSA_COMMENT from ( "
                + "select distinct SMT_ID,PSA_ID,PSA_NAME,PSA_NAME_FREE,PSA_COMMENT from (select SMT_ID,L1PSA_ALIAS_ID "
                + "from (select SMT_ID,L1TM2PS_ID from (select SMT_ID,L1MT_TRIGGER_MENU_ID from (select SMT_ID,SMT_L1_MASTER_TABLE_ID from "
                + "SUPER_MASTER_TABLE where SMT_ID = ?) lhs "
                + " join (select l1mt_id,l1mt_trigger_menu_id from L1_MASTER_TABLE) rhs"
                + " on lhs.smt_l1_master_table_id = rhs.l1mt_id) lhs1"
                + " join (select l1tm2ps_id,l1tm2ps_trigger_menu_id from L1_TM_TO_PS) rhs1"
                + " on lhs1.l1mt_trigger_menu_id = rhs1.l1tm2ps_trigger_menu_id) step1"
                + " join (select L1PSA_L1TM2PS_ID,L1PSA_ALIAS_ID from L1_PRESCALE_SET_ALIAS) step2"
                + " on step1.l1tm2ps_id = step2.L1PSA_L1TM2PS_ID) bigstep1"
                + " join (select * from PRESCALE_SET_ALIAS) bigstep2"
                + " on bigstep1.L1PSA_ALIAS_ID = bigstep2.PSA_ID where PSA_NAME = ?) uber"
                + " join "
                + " (select distinct PSA_ID from (select SMT_ID,HPSA_ALIAS_ID from (select SMT_ID,HTM2PS_ID from "
                + "(select SMT_ID,HMT_TRIGGER_MENU_ID from (select SMT_ID,SMT_HLT_MASTER_TABLE_ID from SUPER_MASTER_TABLE where SMT_ID = ?) lhs"
                + " join (select hmt_id,hmt_trigger_menu_id from HLT_MASTER_TABLE) rhs"
                + " on lhs.smt_hlt_master_table_id = rhs.hmt_id) lhs1"
                + " join (select htm2ps_id,htm2ps_trigger_menu_id from HLT_TM_TO_PS) rhs1"
                + " on lhs1.hmt_trigger_menu_id = rhs1.htm2ps_trigger_menu_id) step1"
                + " join (select HPSA_HTM2PS_ID,HPSA_ALIAS_ID from HLT_PRESCALE_SET_ALIAS) step2"
                + " on step1.htm2ps_id = step2.HPSA_HTM2PS_ID) bigstep1"
                + " join (select * from PRESCALE_SET_ALIAS) bigstep2"
                + " on bigstep1.HPSA_ALIAS_ID = bigstep2.PSA_ID where PSA_NAME = ?) uber1"
                + " on uber.PSA_ID = uber1.PSA_ID";
        
        query = mgr.fix_schema_name(query);

        PreparedStatement ps = mgr.getConnection().prepareStatement(query);
        ps.setInt(1, smk);
        ps.setString(2, type);
        ps.setInt(3, smk);
        ps.setString(4, type);
        ResultSet rset = ps.executeQuery();
        while (rset.next()) {
            results.add(rset.getInt(2) + ":      " + rset.getString(4) + " -      " + rset.getString(5));
        }
        rset.close();
        ps.close();
        return results;
    }
    
       public static ArrayList<String> getAliasesForSMK(int smk, String type, String name) throws SQLException {
        ArrayList<String> results = new ArrayList<>();
        ConnectionManager mgr = ConnectionManager.getInstance();

        String query = "select SMT_ID,uber.PSA_ID,PSA_NAME,PSA_NAME_FREE,PSA_COMMENT from ( "
                + "select distinct SMT_ID,PSA_ID,PSA_NAME,PSA_NAME_FREE,PSA_COMMENT from (select SMT_ID,L1PSA_ALIAS_ID "
                + "from (select SMT_ID,L1TM2PS_ID from (select SMT_ID,L1MT_TRIGGER_MENU_ID from (select SMT_ID,SMT_L1_MASTER_TABLE_ID from "
                + "SUPER_MASTER_TABLE where SMT_ID = ?) lhs "
                + " join (select l1mt_id,l1mt_trigger_menu_id from L1_MASTER_TABLE) rhs"
                + " on lhs.smt_l1_master_table_id = rhs.l1mt_id) lhs1"
                + " join (select l1tm2ps_id,l1tm2ps_trigger_menu_id from L1_TM_TO_PS) rhs1"
                + " on lhs1.l1mt_trigger_menu_id = rhs1.l1tm2ps_trigger_menu_id) step1"
                + " join (select L1PSA_L1TM2PS_ID,L1PSA_ALIAS_ID from L1_PRESCALE_SET_ALIAS) step2"
                + " on step1.l1tm2ps_id = step2.L1PSA_L1TM2PS_ID) bigstep1"
                + " join (select * from PRESCALE_SET_ALIAS) bigstep2"
                + " on bigstep1.L1PSA_ALIAS_ID = bigstep2.PSA_ID where PSA_NAME = ? and PSA_NAME_FREE = ?) uber"
                + " join "
                + " (select distinct PSA_ID from (select SMT_ID,HPSA_ALIAS_ID from (select SMT_ID,HTM2PS_ID from "
                + "(select SMT_ID,HMT_TRIGGER_MENU_ID from (select SMT_ID,SMT_HLT_MASTER_TABLE_ID from SUPER_MASTER_TABLE where SMT_ID = ?) lhs"
                + " join (select hmt_id,hmt_trigger_menu_id from HLT_MASTER_TABLE) rhs"
                + " on lhs.smt_hlt_master_table_id = rhs.hmt_id) lhs1"
                + " join (select htm2ps_id,htm2ps_trigger_menu_id from HLT_TM_TO_PS) rhs1"
                + " on lhs1.hmt_trigger_menu_id = rhs1.htm2ps_trigger_menu_id) step1"
                + " join (select HPSA_HTM2PS_ID,HPSA_ALIAS_ID from HLT_PRESCALE_SET_ALIAS) step2"
                + " on step1.htm2ps_id = step2.HPSA_HTM2PS_ID) bigstep1"
                + " join (select * from PRESCALE_SET_ALIAS) bigstep2"
                + " on bigstep1.HPSA_ALIAS_ID = bigstep2.PSA_ID where PSA_NAME = ? and PSA_NAME_FREE = ?) uber1"
                + " on uber.PSA_ID = uber1.PSA_ID";
        
        query = mgr.fix_schema_name(query);

        PreparedStatement ps = mgr.getConnection().prepareStatement(query);
        ps.setInt(1, smk);
        ps.setString(2, type);
        ps.setString(3, name);        
        ps.setInt(4, smk);
        ps.setString(5, type);
        ps.setString(6, name);
        ResultSet rset = ps.executeQuery();
        while (rset.next()) {
            results.add(rset.getInt(2) + ":      " + rset.getString(4) + " -      " + rset.getString(5));
        }
        rset.close();
        ps.close();
        return results;
    }

    public static ArrayList<String> getAliasesForSMK(int smk) throws SQLException {
        ArrayList<String> results = new ArrayList<>();
        ConnectionManager mgr = ConnectionManager.getInstance();

        String query = "select SMT_ID,uber.PSA_ID,PSA_NAME,PSA_NAME_FREE,PSA_COMMENT from ( "
                + "select distinct SMT_ID,PSA_ID,PSA_NAME,PSA_NAME_FREE,PSA_COMMENT from (select SMT_ID,L1PSA_ALIAS_ID "
                + "from (select SMT_ID,L1TM2PS_ID from (select SMT_ID,L1MT_TRIGGER_MENU_ID from (select SMT_ID,SMT_L1_MASTER_TABLE_ID "
                + "from SUPER_MASTER_TABLE where SMT_ID = ?) lhs "
                + " join (select l1mt_id,l1mt_trigger_menu_id from L1_MASTER_TABLE) rhs"
                + " on lhs.smt_l1_master_table_id = rhs.l1mt_id) lhs1"
                + " join (select l1tm2ps_id,l1tm2ps_trigger_menu_id from L1_TM_TO_PS) rhs1"
                + " on lhs1.l1mt_trigger_menu_id = rhs1.l1tm2ps_trigger_menu_id) step1"
                + " join (select L1PSA_L1TM2PS_ID,L1PSA_ALIAS_ID from L1_PRESCALE_SET_ALIAS) step2"
                + " on step1.l1tm2ps_id = step2.L1PSA_L1TM2PS_ID) bigstep1"
                + " join (select * from PRESCALE_SET_ALIAS) bigstep2"
                + " on bigstep1.L1PSA_ALIAS_ID = bigstep2.PSA_ID where PSA_NAME in ('COSMIC','STANDBY','EMITTANCE','OTHER')) uber"
                + " join "
                + " (select distinct PSA_ID from (select SMT_ID,HPSA_ALIAS_ID from (select SMT_ID,HTM2PS_ID from "
                + "(select SMT_ID,HMT_TRIGGER_MENU_ID from (select SMT_ID,SMT_HLT_MASTER_TABLE_ID from SUPER_MASTER_TABLE where SMT_ID = ?) lhs"
                + " join (select hmt_id,hmt_trigger_menu_id from HLT_MASTER_TABLE) rhs"
                + " on lhs.smt_hlt_master_table_id = rhs.hmt_id) lhs1"
                + " join (select htm2ps_id,htm2ps_trigger_menu_id from HLT_TM_TO_PS) rhs1"
                + " on lhs1.hmt_trigger_menu_id = rhs1.htm2ps_trigger_menu_id) step1"
                + " join (select HPSA_HTM2PS_ID,HPSA_ALIAS_ID from HLT_PRESCALE_SET_ALIAS) step2"
                + " on step1.htm2ps_id = step2.HPSA_HTM2PS_ID) bigstep1"
                + " join (select * from PRESCALE_SET_ALIAS) bigstep2"
                + " on bigstep1.HPSA_ALIAS_ID = bigstep2.PSA_ID where PSA_NAME in ('COSMIC','STANDBY','EMITTANCE','OTHER')) uber1"
                + " on uber.PSA_ID = uber1.PSA_ID";

        query = mgr.fix_schema_name(query);

        PreparedStatement ps = mgr.getConnection().prepareStatement(query);
        ps.setInt(1, smk);
        ps.setInt(2, smk);
        ResultSet rset = ps.executeQuery();
        while (rset.next()) {
            results.add(rset.getInt(2) + ": " + rset.getString(3) + " - " + rset.getString(4) + " -      " + rset.getString(5));
        }
        rset.close();
        ps.close();
        return results;
    }

}
