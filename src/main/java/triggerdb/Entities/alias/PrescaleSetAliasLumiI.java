/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Entities.alias;

import java.sql.SQLException;

/**
 *
 * @author Tiago Perez <tperez@cern.ch>
 */
public interface PrescaleSetAliasLumiI {

    /**
     * Get the id of the alias this record points to.
     * @return 
     * @throws java.sql.SQLException
     */
    public int get_alias_id() throws SQLException;

    /**
     * Set the new Alias Id
     * @param aliasId
     * @throws java.sql.SQLException
     */
    public void set_alias_id(final int aliasId) throws SQLException;

    /**
     * Get the alias this record points to.
     * @return 
     * @throws java.sql.SQLException
     */
    public PrescaleSetAlias get_alias() throws SQLException;

    /**
     * Set the alias.
     *
     * @param alias the new alias.
     * @throws java.sql.SQLException
     */
    public void set_alias(final PrescaleSetAlias alias) throws SQLException;

    /**
     * @return
     * <code>true</code> if this alias is default.
     * @throws java.sql.SQLException
     */
    public Boolean isDdefault() throws SQLException;

    /**
     * @return
     * <code>true</code> if this alias is default.
     * @throws java.sql.SQLException
     */
    public Boolean get_default() throws SQLException;

    /**
     * Set this alias as default.
     * @param def
     * @throws java.sql.SQLException
     */
    public void set_default(final Boolean def) throws SQLException;

    /**
     * Return Lum Max.
     * @return 
     * @throws java.sql.SQLException
     */
    public String get_lum_max() throws SQLException;

    /**
     * Set Lum Max.
     * @param lmax
     * @throws java.sql.SQLException
     */
    public void set_lum_max(String lmax) throws SQLException;

    /**
     * Return Lum Max.
     * @return 
     * @throws java.sql.SQLException
     */
    public String get_lum_min() throws SQLException;

    /**
     * Set Lum Max.
     * @param lmin
     * @throws java.sql.SQLException
     */
    public void set_lum_min(String lmin) throws SQLException;

    /**
     * @return the comment
     * @throws java.sql.SQLException
     */
    public String get_comment() throws SQLException;

    /**
     * Set the a comment
     * @param comment
     * @throws java.sql.SQLException
     */
    public void set_comment(String comment) throws SQLException;

    /**
     * Updates the row on db. It keeps the TM2PS and ALIAS and updates LMIN,
     * LMAX & COMMENT.
     * @throws java.sql.SQLException
     */
    public void update() throws SQLException;
    
    /**
     * Delete this record from DB.
     * @throws java.sql.SQLException
     */
    public void delete() throws SQLException;
    
//    /**
//     * Get the prescale set.
//     */
//    public int get_prescale_set_id();
//
//    /**
//     * Set the prescale set id. Setting this will clear TM2PS if not matching.
//     *
//     * @param the id new prescale set.
//     *
//     */
//    public void set_prescale_set_id(int psId);
}
