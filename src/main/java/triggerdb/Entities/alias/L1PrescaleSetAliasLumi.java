package triggerdb.Entities.alias;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.L1.L1Menu;
import triggerdb.Entities.L1.L1Prescale;
import triggerdb.Entities.L1Links.L1TM_PS;

/**
 * Class representation of the L1_Prescale_Set_ALIAS table.
 *
 *
 * @author Tiago Perez <tperez@cern.ch>
 * @version 2012-04-18
 */
public final class L1PrescaleSetAliasLumi
        extends BasicPrescaleSetAliasLumi
        implements PrescaleSetAliasI<L1Menu, L1Prescale, L1TM_PS> {

    /**
     * The name of the table.
     */
    private static final String TABLE_NAME = "L1_PRESCALE_SET_ALIAS";
    /**
     * Keys of the keyValue map.
     */
    private static final String TM2PS_ID = "L1TM2PS_ID";
    //
    private static final String LEVEL = "L1";
    //
    private L1TM_PS tm2ps;
    private L1Prescale psset;
    private L1Menu menu;
    //interface requires that menu object exist, but only store ID as creating menu object is very slow
    private int menuID;
    /**
     *
     */
    public L1PrescaleSetAliasLumi() {
        super("L1PSA_");
    }

    /**
     * Default constructor with L1PSA_ID as param.
     *
     * @param l1psa_id The db id of the L1PSA table to load. or -1 to create an
     * empty record.
     * @throws java.sql.SQLException
     */
    public L1PrescaleSetAliasLumi(final int l1psa_id) throws SQLException {
        super(l1psa_id,"L1PSA_");
        this._L1PrescaleSetAlias();
        if (l1psa_id > 0) {
            forceLoad();
        }
        
        menuID = this.get_tm2ps().get_menu_id();
        //menu = new L1Menu(this.get_tm2ps().get_menu_id());
    }
   
    /**
     * Get the ID of the L1 Prescale Set Alias record with the given parameters
     *
     * @param menuId The id of the l1 menu.
     * @param psId the id of the l1 prescale set.
     * @param aliasId the id of the alias.
     * @return The id of the matching L1PSA or -1 if no match is found.
     * @throws java.sql.SQLException
     */
    public static int getL1PSA_ID(final int menuId, final int psId, final int aliasId) throws SQLException {
        int l1psaId = -1;
        int l1tm2psId = L1TM_PS.getTM2PS(menuId, psId);

        String query = "SELECT L1PSA_ID FROM L1_PRESCALE_SET_ALIAS "
                + "WHERE L1PSA_L1TM2PS_ID=? "
                + "AND L1PSA_ALIAS_ID=? "
                + "ORDER BY L1PSA_ID DESC";
        ConnectionManager mgr = ConnectionManager.getInstance();
        query = mgr.fix_schema_name(query);
        PreparedStatement ps = mgr.getConnection().prepareStatement(query);
        ps.setInt(1, l1tm2psId);
        ps.setInt(2, aliasId);
        ResultSet rset = ps.executeQuery();
        while (rset.next()) {
            l1psaId = rset.getInt("L1PSA_ID");
        }
        if (rset.next()) {
            logger.log(Level.SEVERE, "More than one L1PSA found for L1 menu, pss, alias {0}, {1}, {2}", new Object[]{menuId, psId, aliasId});
        }
        rset.close();
        ps.close();

        return l1psaId;
    }

    /**
     * Get from DB the list of L1PrescaleAlias Ids linked to the given L1Menu
     * and PrescaleAlias.
     *
     * @param menuId the Id of the L1Menu to query
     * @param aliasId the id of the PrescaleAlias
     * @return List of L1PrescaleSetAliasLumi IDS linked to menuId and aliasId.
     * @throws java.sql.SQLException
     */
    public static ArrayList<Integer> get_prescale_aliases_ids(final int menuId, final int aliasId) throws SQLException{
        ArrayList<Integer> psaIds = new ArrayList<>();

        String query = "SELECT L1PSA_ID,L1PSA_L1TM2PS_ID, L1PSA_LMIN, "
                + "L1PSA_LMAX, L1TM2PS_PRESCALE_SET_ID, L1TM2PS_TRIGGER_MENU_ID "
                + "FROM L1_PRESCALE_SET_ALIAS, L1_TM_TO_PS "
                + "WHERE L1TM2PS_TRIGGER_MENU_ID=? "
                + "AND L1PSA_ALIAS_ID=? "
                + "AND l1psa_l1tm2ps_id=L1TM2PS_ID "
                + "ORDER BY l1psa_id DESC";

        ConnectionManager mgr = ConnectionManager.getInstance();
        query = mgr.fix_schema_name(query);
        PreparedStatement ps = mgr.getConnection().prepareStatement(query);
        ps.setInt(1, menuId);
        ps.setInt(2, aliasId);
        ResultSet rset = ps.executeQuery();
        while (rset.next()) {
            psaIds.add(rset.getInt("L1PSA_ID"));
        }
        rset.close();
        ps.close();

        return psaIds;
    }

    /**
     * Get from DB the list of L1PrescaleAlias linked to the given L1Menu and
     * PrescaleAlias.
     *
     * @param menuId the Id of the L1Menu to query
     * @param aliasId the id of the PrescaleAlias
     * @return List of L1PrescaleSetAliasLumi linked to menuId and aliasId.
     * @throws java.sql.SQLException
     */
    public static ArrayList<L1PrescaleSetAliasLumi> get_prescale_aliases(final int menuId, final int aliasId) throws SQLException {
        ArrayList<Integer> psaIds = L1PrescaleSetAliasLumi.get_prescale_aliases_ids(menuId, aliasId);
        ArrayList<L1PrescaleSetAliasLumi> prescaleSetAliases = new ArrayList<>();

        for (Integer psaId : psaIds) {
            prescaleSetAliases.add(new L1PrescaleSetAliasLumi(psaId));
        }
        return prescaleSetAliases;
    }

    /**
     * Load a L1PSA from db with the give parameters if a match is found, else
     * creates an empty record.
     *
     * @param menuId The id of the l1 menu.
     * @param aliasId the id of the alias.
     * @param psId the id of the l1 prescale set.
     * @throws java.sql.SQLException
     */
    public L1PrescaleSetAliasLumi(final int menuId, final int aliasId, final int psId) throws SQLException {
        super(-1,"L1PSA_");
        this._L1PrescaleSetAlias();
        //menu = new L1Menu(this.get_tm2ps().get_menu_id());
        menuID = menuId;
        int l1psaId = L1PrescaleSetAliasLumi.getL1PSA_ID(menuId, psId, aliasId);

        if (l1psaId > 0) {
            this.set_id(l1psaId);
            this.forceLoad();
        } else {
            L1TM_PS l1tmps = new L1TM_PS(L1TM_PS.getTM2PS(menuId, psId));
            if (l1tmps.get_id() > 0) {
                this.set_tm2ps(l1tmps);
            }
            this._loadMenuPSSAlias(menuId, psId, aliasId);
        }
    }

    /**
     * Initialize the table record.
     */
    private void _L1PrescaleSetAlias() {
        tablePrefix = "L1PSA_";
        keyValue.putFirst(TM2PS_ID, -1, "L1 TM to PS ID");
    }

    /**
     * Loads the given menu, pss and alias to this HLTPrescaleSetAlias if they
     * exist (i.e. their id not 0
     *
     * @param _menuId the id of the HLT Trigger Menu.
     * @param _psId the id of the HLT Prescale Set.
     * @param _psaId the id of hte alias table.
     */
    private void _loadMenuPSSAlias(final int _menuId, final int _psId, final int _psaId) throws SQLException {

        if (new L1Menu().exists(_menuId)) {
            this.set_menu(new L1Menu(_menuId));
        }
        if (new L1Prescale().exists(_psId)) {
            this.set_prescale_set(new L1Prescale(_psId));
        }
        if (new PrescaleSetAlias().exists(_psaId)) {
            this.set_alias(new PrescaleSetAlias(_psaId));
        }
    }

    /**
     * After an edit we need to force the record to read from the database
     * @throws java.sql.SQLException
     */
    @Override
    public void forceLoad() throws SQLException {
        this.set_alias(null);
        psset = null;
        tm2ps = null;
        ConnectionManager.getInstance().forceLoad(this);
        this.tm2ps = new L1TM_PS(this.get_tm2ps_id());
        this.psset = new L1Prescale(this.tm2ps.get_prescale_set_id());
        this.set_alias(new PrescaleSetAlias(this.get_alias_id()));
    }

    /**
     * {@inheritDoc}
     * @return 
     */
    @Override
    public int get_tm2ps_id() {
        return (Integer) keyValue.get(TM2PS_ID);
    }

    /**
     * {@inheritDoc}
     * @param tm2pId
     */
    @Override
    public void set_tm2ps_id(final int tm2pId) throws SQLException {
        this.set_tm2ps(new L1TM_PS(tm2pId));
    }

    /**
     * {@inheritDoc}
     * @return 
     */
    @Override
    public L1TM_PS get_tm2ps() throws SQLException {
        if (this.tm2ps == null) {
            this.tm2ps = new L1TM_PS(this.get_tm2ps_id());
        }
        return this.tm2ps;
    }

    /**
     * {@inheritDoc}
     * @param tm2ps
     */
    @Override
    public void set_tm2ps(final L1TM_PS tm2ps) {
        this.tm2ps = tm2ps;

        if (this.tm2ps != null) {
            if (menuID != this.tm2ps.get_menu_id()) {
                menuID = -1;
            }
            if (this.psset != null
                    && this.psset.get_id() != this.tm2ps.get_prescale_set_id()) {
                this.psset = null;
            }
        }
        keyValue.put(TM2PS_ID, this.tm2ps.get_id());
    }

    /**
     * {@inheritDoc}
     * @return 
     */
    @Override
    public L1Menu get_menu() {
        return this.menu;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void set_menu(final L1Menu menu) {
        menuID = menu.get_id();
        if (this.get_tm2ps_id() > 0
                && menuID != this.tm2ps.get_menu_id()) {
            this.tm2ps = null;
            keyValue.put(TM2PS_ID, -1);
        }
    }

    /**
     * {@inheritDoc}
     * @return 
     */
    @Override
    public int get_menu_id() {
        return menuID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void set_menu_id(final int menuId) {
        menuID = menuId;
    }

    /**
     * {@inheritDoc}
     * @return 
     */
    @Override
    public L1Prescale get_prescale_set() throws SQLException {
        if (this.psset == null) {
            this.psset = new L1Prescale(this.get_tm2ps().get_prescale_set_id());
        }
        return this.psset;
    }

    /**
     * {@inheritDoc}
     * @param ps
     */
    @Override
    public void set_prescale_set(final L1Prescale ps) {
        this.psset = ps;
        if (this.get_tm2ps_id() > 0
                && this.psset.get_id() != this.tm2ps.get_prescale_set_id()) {
            this.tm2ps = null;
            keyValue.put(TM2PS_ID, -1);
        }
    }

    /**
     * {@inheritDoc}
     * @return 
     */
    @Override
    public int get_prescale_set_id() throws SQLException {
        return this.get_prescale_set().get_id();
    }

    /**
     * {@inheritDoc}
     * @param psId
     */
    @Override
    public void set_prescale_set_id(final int psId) throws SQLException {
        this.set_prescale_set(new L1Prescale(psId));
    }

    @Override
    public int updateTM2PS() throws SQLException {
        int _menuId = this.get_menu_id();
        int _psId = this.get_prescale_set_id();
        //System.out.println("l1 menu + " + _menuId + " pss " + _psId);
        /// TM 2 PS
        //// If TM2PS ids consistent with menuId and psId Keep it, 
        //// Else save a new TM2PS 
        L1TM_PS _tm2ps = this.get_tm2ps();
        if (_menuId > 0 && _psId > 0
                && (_tm2ps.get_menu_id() != _menuId
                || _tm2ps.get_prescale_set_id() != _psId)) {
            _tm2ps.set_menu_id(_menuId);
            _tm2ps.set_prescale_set_id(_psId);
            _tm2ps.set_id(-1);
            set_tm2ps_id(_tm2ps.save());
        }
        return this.get_tm2ps_id();
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public Object clone() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Compare to another L1PrescaleSetAliasLumi. Comparison: <br> Compare
     * L1_MENU <br>Compare PRESCALE_ALIAS <br> Compare LMAX <br>Compare LMIN
     * <br>Compare L1PS
     * @param o
     * @return 
     */
    @Override
    public int compareTo(final AbstractTable o) {
        int compare = 1;
        if (o instanceof L1PrescaleSetAliasLumi) {
            L1PrescaleSetAliasLumi l1psa2 = (L1PrescaleSetAliasLumi) o;

            int menu1 = this.get_menu_id();
            int menu2 = l1psa2.get_menu_id();
            // Compare Menu
            if (menu1 != menu2) {
                compare = -1;
            } else if (!this.get_alias().equals(l1psa2.get_alias())) {
                // Compare Aliases
                compare = this.get_alias().compareTo(l1psa2.get_alias());
            } else if (!this.get_lum_min().equals(l1psa2.get_lum_min())) {
                // Compare L Min
                compare = compareLumis(this.get_lum_min(), l1psa2.get_lum_min());
            } else if (!this.get_lum_max().equals(l1psa2.get_lum_max())) {
                // Compare L Max                
                compare = compareLumis(this.get_lum_max(), l1psa2.get_lum_max());
            } else {
                try {
                    // Compate Prescale Set
                    compare = this.get_prescale_set().compareTo(
                            l1psa2.get_prescale_set());
                } catch (SQLException ex) {
                    Logger.getLogger(L1PrescaleSetAliasLumi.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return compare;
    }

    @Override
    public String get_level() {
        return L1PrescaleSetAliasLumi.LEVEL;
    }

}
