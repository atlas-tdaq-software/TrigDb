package triggerdb.Entities.alias;

import static java.lang.Math.round;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;

/**
 * Basic implementation of PrescaleSetAliasLumiI. this implements several
 * methods shared between L1PrescaleSetAliasLumi and HLTPrescaleSetAliasLumi.
 *
 * @author Tiago Perez <tperez@cern.ch>
 */
public abstract class BasicPrescaleSetAliasLumi
        extends AbstractTable
        implements PrescaleSetAliasLumiI {

    /**
     * Name of the columns in the DB table.
     */
    protected static final String ALIAS_ID = "ALIAS_ID";

    /**
     *
     */
    protected static final String LMIN = "LMIN";

    /**
     *
     */
    protected static final String LMAX = "LMAX";

    /**
     *
     */
    protected static final String DEFAULT = "DEFAULT";

    /**
     *
     */
    protected static final String COMMENT = "COMMENT";
    //private static final String TM2PS_ID = "L1TM2PS_ID";
    //    
    /**
     * Private variables
     */
    private PrescaleSetAlias psa;

    /**
     *
     * @param prefix
     */
    public BasicPrescaleSetAliasLumi(final String prefix) {
        super(-1,prefix);
        DefineKeyValuePairs();
    }
        
    /**
     *
     * @param id
     * @param prefix
     * @throws SQLException
     */
    public BasicPrescaleSetAliasLumi(final int id, final String prefix) throws SQLException {
        super(id,prefix);
        DefineKeyValuePairs();
       
        psa = new PrescaleSetAlias(this.get_alias_id());
    }

    private void DefineKeyValuePairs() {
        keyValue.putFirst(LMIN, "~", "Lumi Min");
        keyValue.putFirst(LMAX, "~", "Lumi Max");
        keyValue.putFirst(ALIAS_ID, -1, "Alias Id");
        keyValue.putFirst(DEFAULT, Boolean.FALSE, "Default");
        keyValue.putFirst(COMMENT, "~", "Comment");
        keyValue.remove("NAME");
        keyValue.remove("VERSION");
        keyValue.remove("USED");
    }

    @Override
    public int get_alias_id() {
        return (Integer) keyValue.get(ALIAS_ID);
    }

    @Override
    public void set_alias_id(int aliasId) {
        keyValue.put(ALIAS_ID, aliasId);
        if (this.get_alias().get_id() != this.get_alias_id()) {
            this.set_alias(null);
            this.get_alias();
        }
    }

    @Override
    public PrescaleSetAlias get_alias() {
        return psa;
    }

    @Override
    public void set_alias(final PrescaleSetAlias alias) {
        this.psa = alias;
        if (this.psa != null) {
            this.set_alias_id(alias.get_id());
        }
    }

    /**
     * @return
     * <code>true</code> if this alias is default.
     */
    @Override
    public Boolean isDdefault() {
        return this.get_default();
    }

    /**
     * @return
     * <code>true</code> if this alias is default.
     */
    @Override
    public Boolean get_default() {
        return (Boolean) keyValue.get(DEFAULT);
    }

    /**
     * Set this alias as default.
     */
    @Override
    public void set_default(final Boolean def) {
        keyValue.put(DEFAULT, def);
    }

    @Override
    public String get_lum_max() {
        if(((String)(keyValue.get(LMAX))).equals("~")){
            return (String)(keyValue.get(LMAX));
        }
        return Double.toString(round(Double.parseDouble((String)(keyValue.get(LMAX))) / 1e30));
    }

    @Override
    public void set_lum_max(final String lmax) {
        if(lmax.equals("~")){
            keyValue.put(LMIN, lmax);
            return;
        }
        keyValue.put(LMAX, Double.toString((Double.parseDouble(lmax) * 1e30)));
    }

    @Override
    public String get_lum_min() {
        if(((String)(keyValue.get(LMIN))).equals("~")){
            return (String)(keyValue.get(LMIN));
        }
        return Double.toString(round(Double.parseDouble((String)(keyValue.get(LMIN))) / 1e30));
    }

    public void set_lum_max_bulk(final String lmax) {
        keyValue.put(LMAX, lmax);
    }
    
    public void set_lum_min_bulk(final String lmin) {
        keyValue.put(LMIN, lmin);
    }
    
    @Override
    public void set_lum_min(final String lmin) {
        if(lmin.equals("~")){
            keyValue.put(LMIN, lmin);
            return;
        }
        keyValue.put(LMIN, Double.toString((Double.parseDouble(lmin) * 1e30)));
    }

    @Override
    public void set_comment(final String comment) {
        keyValue.put(COMMENT, comment);
    }

    @Override
    public String get_comment() {
        return (String) keyValue.get(COMMENT);
    }

    @Override
    public int save() throws SQLException {
        int _tm2psId = this.updateTM2PS();

        if (_tm2psId < 0) {
            this.set_id(-1);
            int _menuId = this.get_menu_id();
            int _psId = this.get_prescale_set_id();
            /*logger.log(Level.WARNING," I can''t save because the Menu or {0}" + " Prescale Set"
                    + " are invalid." + "\n Menu: {1}\n Prescale Set: {2}", new Object[]{this.get_level(), _menuId, _psId});*/
            return this.get_id();
        }

        int _aliasId = this.get_alias_id();
        if (_tm2psId > 0 && _aliasId > 0) {
            
            if (this.get_id() < 0) {
                // Save the HLTPrescaleSetAlias table
                this.saveSimple();
            } else {
                logger.log(Level.INFO, " Updating entry ID= {0}\t {1}", new Object[]{this.get_id(), this.toString()});
                this.update();
            }
        } else {
            logger.log(Level.WARNING," Trying to save incomplete HLT Prescale Set Alias."
                    + " Ingnoring. " + "\n ALIAS ID    : {0}\n {1} TM2PS ID : {2}\n{3}", new Object[]{_aliasId, this.get_level(), _tm2psId, this.toString()});
        }

        return this.get_id();
    }
    
    /**
     * Compares luminosity values encoded as string. This will compare the Float
     * value if present else compare the lumis as Strings.
     *
     * @param l1 First Lumi values.
     * @param l2 Second lumi value.
     * @return
     */
    public static int compareLumis(final String l1, final String l2) {
        int comp;
        try {
            java.lang.Float l1f = java.lang.Float.parseFloat(l1.trim());
            java.lang.Float l2f = java.lang.Float.parseFloat(l2.trim());
            comp = -l1f.compareTo(l2f);
        } catch (NumberFormatException e) {
            logger.log(Level.WARNING," Luminosities are not numbers." + "\n L1 = {0} \n L2 = {1}", new Object[]{l1, l2});
            comp = l1.trim().compareTo(l2.trim());
        }

        return comp;
    }

    /**
     * @return A string containing the ID, name and version of the record.
     */
    @Override
    public String toString() {
        String alias = this.get_alias().toString();
        alias += " : ";
        alias += this.get_lum_min() + " < L < " + this.get_lum_max();
        try {
            alias += " (PSK: " + this.get_prescale_set_id() + ")";
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Cannot convert to string.", ex);
        }
        return alias;
    }

    @Override
    public void update() throws SQLException {
        // Comapre TM2PS with Prescale Set Id and Menu Id.
        this.updateTM2PS();
        ConnectionManager mgr1 = ConnectionManager.getInstance();
        String pre = this.tablePrefix;
        String tableName = this.getTableName();
        Integer tableId = this.get_id();

        mgr1.Update(tableName, pre, tableId, this.keyValue);
    }

    /**
     * Compares the TM2PS id with the values stored in the Prescale Set and
     * Menu. Updates TM2PS if necessary.
     *
     * @return the updated TM2PS id.
     * @throws java.sql.SQLException
     */
    public abstract int updateTM2PS() throws SQLException;

    /**
     *
     * @return
     * @throws SQLException
     */
    public abstract int get_prescale_set_id() throws SQLException;

    /**
     *
     * @return
     * @throws SQLException
     */
    public abstract int get_menu_id() throws SQLException;

    /**
     * @return "L1" or "HLT".
     * @throws java.sql.SQLException
     */
    public abstract String get_level() throws SQLException;

    /**
     * Deletes this record from DB.
     * @throws java.sql.SQLException
     */
    @Override
    public void delete()  throws SQLException{
        if (this.get_id() > 0) {
            String pre = this.tablePrefix;
            String name = this.getTableName();
            String deleteQuery = " DELETE ";
            deleteQuery += " FROM " + name;
            deleteQuery += " WHERE " + pre + "ID=?";
            ConnectionManager mgr = ConnectionManager.getInstance();
            deleteQuery = mgr.fix_schema_name(deleteQuery);
            try (PreparedStatement ps = mgr.getConnection().prepareStatement(deleteQuery)) {
                ps.setInt(1, this.get_id());
                ps.executeUpdate();
                ps.close();
            }
        }
    }
}
