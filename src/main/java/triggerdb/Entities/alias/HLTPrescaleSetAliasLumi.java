package triggerdb.Entities.alias;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.HLT.HLTPrescaleSet;
import triggerdb.Entities.HLT.HLTTriggerMenu;
import triggerdb.Entities.HLTLinks.HLTTM_PS;

/**
 * Alias table for HLT Prescale. Holds the HLT Prescale Set Alias while we have
 * it in memory. This contains a link to the prescale that should be shown to
 * the shifters for selection.
 *
 * @author Tiago Perez <tperez@cern.ch>
 * @version 2012-04-24
 */
public final class HLTPrescaleSetAliasLumi
        extends BasicPrescaleSetAliasLumi
        implements PrescaleSetAliasI<HLTTriggerMenu, HLTPrescaleSet, HLTTM_PS> {

    /**
     * The name of the table.
     */
    private static final String TABLE_NAME = "HLT_PRESCALE_SET_ALIAS";
    /**
     * Name of the columns in the DB table.
     */
    private static final String TM2PS_ID = "HTM2PS_ID";
    /**
     * This is a HLT Prescale Set Alis record.
     */
    private static final String LEVEL = "HLT";
    /**
     * Private variables
     */
    private HLTTM_PS tm2ps;
    private HLTPrescaleSet psset;
    private HLTTriggerMenu menu;

    /**
     * Get the ID of the HLT Prescale Set Alias record with the given parameters
     *
     * @param menuId The id of the hlt menu.
     * @param psId the id of the hlt prescale set.
     * @param aliasId the id of the alias.
     * @return The id of the matching HPSA or -1 if no match is found.
     * @throws java.sql.SQLException
     */
    public static int getHPSA_ID(final int menuId, final int psId, final int aliasId) throws SQLException {
        int _psaId = -1;
        int _tm2psId = HLTTM_PS.getTM2PS(menuId, psId);

        String query = "SELECT HPSA_ID FROM HLT_PRESCALE_SET_ALIAS "
                + "WHERE HPSA_HTM2PS_ID=? "
                + "AND HPSA_ALIAS_ID=? "
                + "ORDER BY HPSA_ID DESC";
        ConnectionManager mgr = ConnectionManager.getInstance();
        query = mgr.fix_schema_name(query);

        try (PreparedStatement ps = mgr.getConnection().prepareStatement(query)) {
            ps.setInt(1, _tm2psId);
            ps.setInt(2, aliasId);
            try (ResultSet rset = ps.executeQuery()) {
                while (rset.next()) {
                    _psaId = rset.getInt("HPSA_ID");
                }
                if (rset.next()) {
                    logger.log(Level.SEVERE, "More than one HPSA found for HLT menu, pss, alias {0}, {1}, {2}", new Object[]{menuId, psId, aliasId});
                }
                rset.close();
                ps.close();
            }
        }

        return _psaId;
    }

    /**
     * Get from DB the list of HLTPrescaleAlias ids linked to the given HLTMenu
     * and PrescaleAlias.
     *
     * @param menuId the Id of the HLTMenu to query
     * @param aliasId the id of the PrescaleAlias
     * @return List of HLTPrescaleSetAliasLumi ids linked to menuId and aliasId.
     * @throws java.sql.SQLException
     */
    public static ArrayList<Integer> get_prescale_aliases_id(final int menuId, final int aliasId) throws SQLException {

        ArrayList<Integer> psaIds = new ArrayList<>();

        String query = "SELECT HPSA_ID,HPSA_HTM2PS_ID, HPSA_LMIN, "
                + "HPSA_LMAX, HTM2PS_PRESCALE_SET_ID, HTM2PS_TRIGGER_MENU_ID "
                + "FROM HLT_PRESCALE_SET_ALIAS, HLT_TM_TO_PS "
                + "WHERE HTM2PS_TRIGGER_MENU_ID=? "
                + "AND HPSA_ALIAS_ID=? "
                + "AND hpsa_htm2ps_id=HTM2PS_ID "
                + "ORDER BY hpsa_id DESC";

        ConnectionManager mgr = ConnectionManager.getInstance();
        query = mgr.fix_schema_name(query);
        PreparedStatement ps = mgr.getConnection().prepareStatement(query);
        ps.setInt(1, menuId);
        ps.setInt(2, aliasId);
        ResultSet rset = ps.executeQuery();
        while (rset.next()) {
            psaIds.add(rset.getInt("HPSA_ID"));
        }
        rset.close();
        ps.close();

        return psaIds;

    }

    /**
     * Get from DB the list of HLTPrescaleAlias linked to the given HLTMenu and
     * PrescaleAlias.
     *
     * @param menuId the Id of the HLTMenu to query
     * @param aliasId the id of the PrescaleAlias
     * @return List of HLTPrescaleSetAliasLumi linked to menuId and aliasId.
     * @throws java.sql.SQLException
     */
    public static ArrayList<HLTPrescaleSetAliasLumi> get_prescale_aliases(final int menuId, final int aliasId) throws SQLException {
        ArrayList<HLTPrescaleSetAliasLumi> prescaleAliases = new ArrayList<>();
        ArrayList<Integer> psaIds = HLTPrescaleSetAliasLumi.get_prescale_aliases_id(menuId, aliasId);
        for (Integer psaId : psaIds) {
            prescaleAliases.add(new HLTPrescaleSetAliasLumi(psaId));
        }
        return prescaleAliases;
    }

    /**
     *
     */
    public HLTPrescaleSetAliasLumi() {
        super("HPSA_");
    }
    
    /**
     * Default constructor with L1PSA_ID as param.
     *
     * @param hpsa_id The db id of the HLT Prescale Set Alias table to load or
     * -1 to create an empty record.
     * @throws java.sql.SQLException
     */
    public HLTPrescaleSetAliasLumi(final int hpsa_id) throws SQLException {
        super(hpsa_id,"HPSA_");
        this._HLTPrescaleSetAlias();
        if (hpsa_id > 0) {
            forceLoad();
        }
    }

    /**
     * Load a HLTPSA from db with the give parameters if a match is found, else
     * creates an empty record.
     *
     * @param menuId The id of the l1 menu.
     * @param aliasId the id of the alias.
     * @param psId the id of the l1 prescale set.
     * @throws java.sql.SQLException
     */
    public HLTPrescaleSetAliasLumi(final int menuId, final int aliasId, final int psId) throws SQLException {
        super(-1,"HPSA_");
        this._HLTPrescaleSetAlias();
        int hpsaId = HLTPrescaleSetAliasLumi.getHPSA_ID(menuId, psId, aliasId);

        if (hpsaId > 0) {
            this.set_id(hpsaId);
            this.psset = new HLTPrescaleSet(this.get_prescale_set_id());
            this.forceLoad();
        } else {
            // Record not present, load one by one
            int _htm2psId = HLTTM_PS.getTM2PS(menuId, psId);
            if (_htm2psId > 0) {
                this.set_tm2ps_id(_htm2psId);
            }
            this._loadMenuPSSAlias(menuId, psId, aliasId);
        }
    }

    /**
     * Construct the object. Pass input_id of -1 to create a new record,
     * otherwise the data is loaded from the database.
     *
     */
    public void _HLTPrescaleSetAlias() {
        tablePrefix = "HPSA_";
        keyValue.putFirst(TM2PS_ID, -1, "HLT TM to PS ID");
    }

    /**
     * Loads the given menu, pss and alias to this HLTPrescaleSetAliasLumi if
     * they exist (i.e. their id not 0
     *
     * @param _menuId the id of the HLT Trigger Menu.
     * @param _psId the id of the HLT Prescale Set.
     * @param _psaId the id of hte alias table.
     */
    private void _loadMenuPSSAlias(final int _menuId, final int _psId, final int _psaId) throws SQLException {

        if (new HLTTriggerMenu(-1).exists(_menuId)) {
            this.set_menu(new HLTTriggerMenu(_menuId));
        }
        if (new HLTPrescaleSet().exists(_psId)) {
            this.set_prescale_set(new HLTPrescaleSet(_psId));
        }
        if (new PrescaleSetAlias(-1).exists(_psaId)) {
            this.set_alias(new PrescaleSetAlias(_psaId));
        }
    }

    /**
     * Force this record to be read from the database again. After an edit we
     * need to force the record to read from the database.
     * @throws java.sql.SQLException
     */
    @Override
    public void forceLoad() throws SQLException {
        this.set_alias(null);
        psset = null;
        menu = null;
        tm2ps = null;
        ConnectionManager.getInstance().forceLoad(this);
        this.tm2ps = new HLTTM_PS(this.get_tm2ps_id());
        this.menu = new HLTTriggerMenu(this.get_menu().get_id());
        this.set_alias(new PrescaleSetAlias(this.get_alias_id()));
    }

    /**
     * {@inheritDoc}
     * @return 
     */
    @Override
    public int get_tm2ps_id() {
        return (Integer) keyValue.get(TM2PS_ID);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void set_tm2ps_id(final int tm2psId) {
        keyValue.put(TM2PS_ID, tm2psId);
    }

    /**
     * {@inheritDoc}
     * @return 
     */
    @Override
    public HLTTM_PS get_tm2ps() throws SQLException {
        if (this.tm2ps == null) {
            this.tm2ps = new HLTTM_PS(this.get_tm2ps_id());
        }
        return this.tm2ps;
    }

    /**
     * {@inheritDoc}
     * @param tm2ps
     */
    @Override
    public void set_tm2ps(final HLTTM_PS tm2ps) {
        this.tm2ps = tm2ps;
        if (this.menu.get_id() != this.tm2ps.get_menu_id()) {
            this.menu = null;
        }
        if (this.psset.get_id() != this.tm2ps.get_prescale_set_id()) {
            this.psset = null;
        }
        keyValue.put(TM2PS_ID, this.tm2ps.get_id());

    }

    /**
     * {@inheritDoc}
     * @return 
     */
    @Override
    public HLTTriggerMenu get_menu() throws SQLException {
        if (this.menu == null) {
            this.menu = new HLTTriggerMenu(this.get_tm2ps().get_menu_id());
        }
        return this.menu;
    }

    /**
     * {@inheritDoc}
     * @param hltMenu
     */
    @Override
    public void set_menu(final HLTTriggerMenu hltMenu) throws SQLException {
        this.menu = hltMenu;
        if (this.get_tm2ps_id() > 0
                && this.menu.get_id() != this.get_tm2ps().get_menu_id()) {
            this.tm2ps = null;
            keyValue.put(TM2PS_ID, -1);
        }
    }

    /**
     * {@inheritDoc}
     * @return 
     */
    @Override
    public int get_menu_id() throws SQLException {
        return this.get_menu().get_id();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void set_menu_id(final int menuId) throws SQLException {
        this.set_menu(new HLTTriggerMenu(menuId));
    }

    /**
     * {@inheritDoc}
     * @return 
     */
    @Override
    public HLTPrescaleSet get_prescale_set() throws SQLException{
        if (this.psset == null) {
            this.psset = new HLTPrescaleSet(this.get_tm2ps().get_prescale_set_id());
        }
        return this.psset;
    }

    /**
     * {@inheritDoc}
     * @param ps
     */
    @Override
    public void set_prescale_set(final HLTPrescaleSet ps) {
        this.psset = ps;
        if (this.get_tm2ps_id() > 0
                && this.psset.get_id() != this.tm2ps.get_prescale_set_id()) {
            this.tm2ps = null;
            keyValue.put(TM2PS_ID, -1);
        }
    }

    /**
     * {@inheritDoc}
     * @return 
     * @throws java.sql.SQLException 
     */
    @Override
    public int get_prescale_set_id() throws SQLException {
        return this.get_prescale_set().get_id();
    }

    /**
     * {@inheritDoc}
     * @param psId
     * @throws java.sql.SQLException
     */
    @Override
    public void set_prescale_set_id(final int psId) throws SQLException{
        this.set_prescale_set(new HLTPrescaleSet(psId));        
    }

    @Override
    public int updateTM2PS() throws SQLException {
        int _menuId = this.get_menu_id();
        int _psId = this.get_prescale_set_id();
        //int _aliasId = this.get_alias_id();
        //
        //// TM 2 PS
        //// If TM2PS ids consistent with menuId and psId Keep it, 
        //// Else save a new TM2PS 
        HLTTM_PS _htmps = this.get_tm2ps();
        if (_menuId > 0 && _psId > 0
                && (_htmps.get_menu_id() != _menuId
                || _htmps.get_prescale_set_id() != _psId)) {
            _htmps.set_menu_id(_menuId);
            _htmps.set_prescale_set_id(_psId);
            _htmps.set_id(-1);
            _htmps.save();
            this.set_tm2ps(_htmps);
        }
        return this.get_tm2ps_id();
    }

    @Override
    public String getTableName() {
        return HLTPrescaleSetAliasLumi.TABLE_NAME;
    }

    /**
     * {@inheritDoc}
     * @return 
     */
    @Override
    public Object clone() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Compare to another HLTPrescaleSetAliasLumi. Comparison: <br> Compare
     * L1_MENU <br>Compare PRESCALE_ALIAS <br> Compare LMAX <br>Compare LMIN
     * <br>Compare L1PS
     * @param o
     * @return 
     */
    @Override
    public int compareTo(final AbstractTable o) {
        int compare = 1;
        if (o instanceof HLTPrescaleSetAliasLumi) {
            try {
                HLTPrescaleSetAliasLumi hltpsa2 = (HLTPrescaleSetAliasLumi) o;

                HLTTriggerMenu menu1 = this.get_menu();
                HLTTriggerMenu menu2 = hltpsa2.get_menu();
                if (!menu1.equals(menu2)) {
                    compare = menu1.compareTo(menu2);
                } else if (!this.get_alias().equals(hltpsa2.get_alias())) {
                    compare = this.get_alias().compareTo(hltpsa2.get_alias());
                } else if (!this.get_lum_max().equals(hltpsa2.get_lum_max())) {

                    // Compare L Max                
                    compare = this.get_lum_max().compareTo(hltpsa2.get_lum_max());
                } else if (!this.get_lum_min().equals(hltpsa2.get_lum_min())) {

                    // Compare L Max                
                    compare = this.get_lum_min().compareTo(hltpsa2.get_lum_min());
                } else {
                    try {
                        // Compate Prescale Set
                        compare = this.get_prescale_set().compareTo(
                                hltpsa2.get_prescale_set());
                    } catch (SQLException ex) {
                        Logger.getLogger(HLTPrescaleSetAliasLumi.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } catch (SQLException ex) {
                Logger.getLogger(HLTPrescaleSetAliasLumi.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return compare;
    }

    @Override
    public String get_level() {
        return HLTPrescaleSetAliasLumi.LEVEL;
    }
}