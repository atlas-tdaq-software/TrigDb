package triggerdb.Entities.alias;

import java.sql.SQLException;
import triggerdb.Entities.HLT.HLTPrescaleSet;
import triggerdb.Entities.HLT.HLTTriggerMenu;
import triggerdb.Entities.L1.L1Menu;
import triggerdb.Entities.L1.L1Prescale;
import triggerdb.Entities.HLTLinks.HLTTM_PS;
import triggerdb.Entities.L1Links.L1TM_PS;

/**
 * Interface for L1PrescaleSet and HLTPrescaleSet.
 *
 * @author Tiago Perez <tperez@cern.ch>
 * @version 2012-04-25
 *
 * @param <M> The class of the trigger menu ({@link L1Menu} or {@link HLTTriggerMenu}).
 * @param <PS> The class of the prescale set object ({@link L1Prescale} or {@link HLTPrescaleSet}).
 * @param <TM2PS> The class of the TM to PS table ({@link L1TM_PS} or {@link HLTTM_PS}).
 */
public interface PrescaleSetAliasI<M, PS, TM2PS>
        extends PrescaleSetAliasLumiI {
//
//    /**
//     * Get the id of the alias this record points to.
//     */
//    public int get_alias_id();
//
//    /**
//     * Set the new Alias Id
//     */
//    public void set_alias_id(final int aliasId);
//
//    /**
//     * Get the alias this record points to.
//     */
//    public PrescaleSetAlias get_alias();
//
//    /**
//     * Set the alias.
//     *
//     * @param alias the new alias.
//     */
//    public void set_alias(final PrescaleSetAlias alias);
//
//    /**
//     * @return
//     * <code>true</code> if this alias is default.
//     */
//    public Boolean isDdefault();
//
//    /**
//     * @return
//     * <code>true</code> if this alias is default.
//     */
//    public Boolean get_default();
//
//    /**
//     * Set this alias as default.
//     */
//    public void set_default(final Boolean def);
//
//    /**
//     * Return Lum Max.
//     */
//    public String get_lum_max();
//
//    /**
//     * Set Lum Max.
//     */
//    public void set_lum_max(String lmax);
//
//    /**
//     * Return Lum Max.
//     */
//    public String get_lum_min();
//
//    /**
//     * Set Lum Max.
//     */
//    public void set_lum_min(String lmin);
//
//    /**
//     * @return the comment
//     */
//    public String get_comment();
//
//    /**
//     * Set the a comment
//     */
//    public void set_comment(String comment);

    /**
     * Get the TM 2 PS ID.
     * @return 
     */
    public int get_tm2ps_id();

    /**
     * Set the TM 2 PS ID. Setting this will clear PRESCALE_SET and MENU if they
     * are not compatible.
     *
     * @param tm2psId the Id of the TM 2 PS table.
     * @throws java.sql.SQLException
     */
    public void set_tm2ps_id(final int tm2psId) throws SQLException;

    /**
     * Get the TM 2 PS.
     * @return 
     * @throws java.sql.SQLException 
     */
    public TM2PS get_tm2ps() throws SQLException;

    /**
     * Set the TM 2 PS. Setting this will clear PRESCALE_SET and MENU if they
     * are not compatible.
     *
     * @param tm2psId
     */
    public void set_tm2ps(final TM2PS tm2psId);

    /**
     * Get the trigger menu.
     * @return 
     * @throws java.sql.SQLException 
     */
    public M get_menu() throws SQLException;

    /**
     * Link this alias to a new trigger menu. Setting this will clear TM2PS if
     * not matching.
     *
     * @param menu the new trigger menu.
     * @throws java.sql.SQLException
     */
    public void set_menu(M menu) throws SQLException ;

    /**
     * Get the id of the linked trigger menu.
     * @return 
     * @throws java.sql.SQLException 
     */
    public int get_menu_id() throws SQLException;

    /**
     * Links this alias to a new trigger menu. Setting this will clear TM2PS if
     * not matching.
     *
     * @param menuId the id of the new trigger menu.
     * @throws java.sql.SQLException
     */
    public void set_menu_id(int menuId) throws SQLException;

    /**
     * Get the prescale set.
     * @return 
     * @throws java.sql.SQLException 
     */
    public PS get_prescale_set() throws SQLException;

    /**
     * Set the prescale set. Setting this will clear TM2PS if not matching.
     *
     * @param ps
     *
     */
    public void set_prescale_set(PS ps);

    /**
     * Get the prescale set.
     * @return 
     * @throws java.sql.SQLException 
     */
    public int get_prescale_set_id() throws SQLException;

    /**
     * Set the prescale set id. Setting this will clear TM2PS if not matching.
     *
     * @param psId
     * @throws java.sql.SQLException
     *
     */
    public void set_prescale_set_id(int psId) throws SQLException;

//    /**
//     * Updates the row on db. It keeps the TM2PS and ALIAS and updates LMIN,
//     * LMAX & COMMENT.
//     */
//    public void update();
}
