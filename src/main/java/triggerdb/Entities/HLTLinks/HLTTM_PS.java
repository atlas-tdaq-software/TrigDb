package triggerdb.Entities.HLTLinks;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import triggerdb.Entities.AbstractTable;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.HLT.HLTPrescaleSet;
import triggerdb.Entities.HLT.HLTTriggerMenu;

/**
 * Link between a menu and prescale set This is needed since a menu is not
 * copied (no new id) if prescale sets are added or removed
 *
 * @author Paul Bell
 */
public final class HLTTM_PS extends AbstractTable {

    /**
     * Get the id of the link to the given Menu and Prescale Set
     *
     * @param menuId The Menu Id
     * @param psId The PrescaleSet Id
     * @return the id of the link TM <-> PSset.
     * @throws java.sql.SQLException
     */
    public static int getTM2PS(final int menuId, final int psId) throws SQLException {
        int linkid = -1;
        String query = "SELECT HTM2PS_ID, "
                + "HTM2PS_PRESCALE_SET_ID, "
                + "HTM2PS_TRIGGER_MENU_ID "
                + "FROM HLT_TM_TO_PS "
                + "WHERE HTM2PS_TRIGGER_MENU_ID=? "
                + "AND HTM2PS_PRESCALE_SET_ID=? "
                + "ORDER BY HTM2PS_ID DESC";

        ConnectionManager mgr = ConnectionManager.getInstance();
        query = mgr.fix_schema_name(query);

        PreparedStatement ps = mgr.getConnection().prepareStatement(query);
        ps.setInt(1, menuId);
        ps.setInt(2, psId);
        ResultSet rset = ps.executeQuery();
        if(rset.next()){
            linkid = rset.getInt("HTM2PS_ID");
        }
        if (rset.next()) {
            logger.log(Level.SEVERE, "More than one TMPS link found for HLT Menu-PSS {0} {1}", new Object[]{menuId, psId});
        }
        rset.close();
        ps.close();

        return linkid;
    }

    //Return all HLT menus associated with a prescale set

    /**
     *
     * @param psId
     * @return
     * @throws SQLException
     */
    public static ArrayList<Integer> getMenus(final int psId) throws SQLException {
        ArrayList<Integer> menus = new ArrayList<>();
        String query = "SELECT HTM2PS_TRIGGER_MENU_ID "
                + "FROM HLT_TM_TO_PS "
                + "WHERE HTM2PS_PRESCALE_SET_ID=? ";

        ConnectionManager mgr = ConnectionManager.getInstance();
        query = mgr.fix_schema_name(query);

        PreparedStatement ps = mgr.getConnection().prepareStatement(query);
        ps.setInt(1, psId);
        ResultSet rset = ps.executeQuery();
        while (rset.next()) {
            menus.add(rset.getInt("HTM2PS_TRIGGER_MENU_ID"));
        }

        rset.close();
        ps.close();

        return menus;
    }

    /**
     * Unhide a given HLT Prescale Set in a HLT Trigger Menu.
     *
     * @param menuId the id of the menu
     * @param psId the id of the prescale set.
     * @throws java.sql.SQLException
     */
    public static void unhidePS(int menuId, int psId) throws SQLException {
        int linkId = HLTTM_PS.getTM2PS(menuId, psId);
        String query = "UPDATE HLT_TM_TO_PS SET HTM2PS_USED=0 WHERE HTM2PS_ID=?";
        ConnectionManager mgr = ConnectionManager.getInstance();
        query = mgr.fix_schema_name(query);

        PreparedStatement ps = mgr.getConnection().prepareStatement(query);
        ps.setInt(1, linkId);
        ps.executeUpdate();
        ps.close();
    }

    /**
     * Empty constructor.
     */
    public HLTTM_PS() {
        super("HTM2PS_");
        _HLTTM_PS();
    }

    /**
     * Load the record from DB if is > 0.
     *
     * @param tm2psId
     * @throws java.sql.SQLException
     */
    public HLTTM_PS(final int tm2psId) throws SQLException {
        super(tm2psId, "HTM2PS_");
        _HLTTM_PS();
        if (tm2psId > 0) {
            forceLoad();
        }
    }

    /**
     * Load from DB the link table between the given Menu and PrescaleSet if
     * exist. Else, it creates a new entry with the given parameters.
     *
     * @param hps the HLTPrescaleSet.
     * @param htm the HLT Trigger Menu.
     * @throws java.sql.SQLException
     */
    public HLTTM_PS(final HLTPrescaleSet hps, final HLTTriggerMenu htm) throws SQLException {
        super("HTM2PS_");
        _HLTTM_PS();
        int _id = HLTTM_PS.getTM2PS(htm.get_id(), hps.get_id());
        if (_id > 0) {
            this.set_id(_id);
            this.forceLoad();
        } else {
            this.set_menu_id(htm.get_id());
            this.set_prescale_set_id(hps.get_id());
        }
    }

    /**
     * Set up the KeyValues
     */
    public void _HLTTM_PS() {

        keyValue.putFirst("TRIGGER_MENU_ID", -1, "Trigger Menu ID");
        keyValue.putFirst("PRESCALE_SET_ID", -1, "Prescale Set ID");
        keyValue.putFirst("USED", false, "Used");

        keyValue.remove("NAME");
        keyValue.remove("VERSION");
    }

    ///Get the menu ID.

    /**
     *
     * @return
     */
    public Integer get_menu_id() {
        return (Integer) keyValue.get("TRIGGER_MENU_ID");
    }
    ///Set the menu ID.

    /**
     *
     * @param inp
     */
    public void set_menu_id(Integer inp) {
        keyValue.put("TRIGGER_MENU_ID", inp);
    }

    ///Get the menu ID.

    /**
     *
     * @return
     */
    public Integer get_prescale_set_id() {
        return (Integer) keyValue.get("PRESCALE_SET_ID");
    }
    ///Set the menu ID.

    /**
     *
     * @param inp
     */
    public void set_prescale_set_id(Integer inp) {
        keyValue.put("PRESCALE_SET_ID", inp);
    }

    //Set visible - make use the used property, so links never removed
    //but can choose not to see certain sets

    /**
     *
     * @return
     */
    public Boolean get_hidden() {
        return (Boolean) keyValue.get("USED");
    }

    /**
     *
     * @param inp
     */
    public void set_hidden(Boolean inp) {
        keyValue.put("USED", inp);
    }

    ///Save the menu to prescale set link
    /**
     * Check if this link already exists before saving. This is done at the
     * start using the map of db field and value.
     *
     * @return The new ID if one is assigned, or the ID of the existing record.
     * @throws java.sql.SQLException Stop if we have an SQL problem
     */
    @Override
    public int save() throws SQLException {

        ArrayList<Integer> ids = ConnectionManager.getInstance().get_IDs(getTableName(), tablePrefix, keyValue, null, "ID");
        if (ids.size() <= 0) {
            set_id(ConnectionManager.getInstance().save(getTableName(), tablePrefix, get_id(), keyValue));
        } else {
            set_id(ids.get(0));
            forceLoad();
        }

        return get_id();
    }

    ///Clone is not supported here.
    @Override
    public Object clone() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getTableName() {
        return "HLT_TM_TO_PS";
    }
}
