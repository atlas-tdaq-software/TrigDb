
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb;

import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeMap;

/**
 *
 * @author Simon Head
 * @param <K>
 * @param <V>
 */
public class TriggerMap<K, V> extends TreeMap<K, V> {

    private final HashMap<K, String> sqlHuman = new HashMap<>();

    /**
     *
     * @param sql
     * @param obj
     * @param human
     */
    public void putFirst(K sql, V obj, String human) {
        put(sql, obj);
        sqlHuman.put(sql, human);
    }
    
    /**
     *
     * @return
     */
    public HashMap<K,String> getSearchData() {
        return sqlHuman;
    }

    /**
     *
     * @return
     */
    public TreeMap<String, Object> getRowData() {
        TreeMap<String, Object> humanValue = new TreeMap<>();

        for (Iterator<K> it = sqlHuman.keySet().iterator(); it.hasNext();) {
            K key1 = it.next();
            for (Iterator<K> it2 = keySet().iterator(); it2.hasNext();) {
                K key2 = it2.next();

                if (key1.equals(key2)) {
                    humanValue.put(sqlHuman.get(key1), get(key2));
                }
            }
        }

        return humanValue;
    }

    
    public void setRowData(TreeMap<String,Object> map, String key, String str){
        map.put(key, str);
        
    }
    /**
     *
     * @param key
     * @return
     */
    public String getHuman(K key) {
        return sqlHuman.get(key);
    }
    
    public void setHuman(K key, String str){
        sqlHuman.put(key, str);
    }
}
