
package triggerdb;

import javax.swing.tree.*;

/**
 * Class for a node in the diff tree.
 * This class represents a node holding the differences between two tables
 * and so stores the name of the key and the two values.
 * @author markowen
 */
public class DiffMutableTreeNode extends DefaultMutableTreeNode {

    /// The key or name othe thing that is different.
    private String key=null;
    /// The value in the first table
    private String val1=null;
    /// The value in the second table
    private String val2=null;

    /**
     * Constructor using a string to make this node.
     *
     * Just calls DefaultMutableTreeNode constructor & allows the node to function as a
     * DefaultMutableTreeNode object.
     * @param string
     */
    public DiffMutableTreeNode(String string) {
        super(string);
    }

    /**
     * Constructor that takes the key & two values (as strings).
     *
     * It builds up the display string in the form:
     * key: inval1,     inval2
     * and sends this to the DetaultMutableTreeNode constructor.
     * @param inkey
     * @param inval1
     * @param inval2
     */
    public DiffMutableTreeNode(String inkey, String inval1, String inval2) {
        super(inkey + ": " + inval1 + ",\t" + inval2);
        key = inkey;
        val1 = inval1;
        val2 = inval2;
    }

    /**
     *
     * @return
     */
    public String getKey() {
        return key;
    }

    /**
     *
     * @return
     */
    public String getVal1() {
        return val1;
    }

    /**
     *
     * @return
     */
    public String getVal2() {
        return val2;
    }


}//class DiffMutableTreeNode
