package triggerdb;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import triggerdb.Connections.ConnectionManager;
import triggerdb.Entities.L1.L1Item;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Michele
 */
public class StreamsQueries {

    /**
     *
     * @param matchedchainid
     * @param matcheditems_v
     * @return
     * @throws SQLException
     */
    public static List<Integer> GetIds(int matchedchainid, List<L1Item> matcheditems_v) throws SQLException {
     ////System.out.println("Clicked the chains table row " + row + " for chain id " + matchedchainid);
        List<Integer> supermatcheditems_v = new ArrayList<>();
        for(L1Item item : matcheditems_v){

            int L1typeitem = Integer.parseInt(item.get_trigger_type(), 2);

            //get the algo types for this chain
            String  query = "SELECT HCP_ID, HCP_NAME, HTE2CP_ALGORITHM_COUNTER, HTE2CP_TRIGGER_ELEMENT_ID, HTE2CP_ID " +
                    "FROM " +
                    "HLT_TC_TO_TS, " +
                    "HLT_TS_TO_TE, " +
                    "HLT_TE_TO_CP, " +
                    "HLT_COMPONENT " +
                    "WHERE " +
                    "HCP_NAME LIKE 'L1InfoHypo' " +
                    "AND " +
                    "HTC2TS_TRIGGER_CHAIN_ID=? " +
                    "AND " +
                    "HTC2TS_TRIGGER_SIGNATURE_ID=HTS2TE_TRIGGER_SIGNATURE_ID " +
                    "AND " +
                    "HTS2TE_TRIGGER_ELEMENT_ID=HTE2CP_TRIGGER_ELEMENT_ID " +
                    "AND " +
                    "HTE2CP_COMPONENT_ID=HCP_ID " +
                    "ORDER BY HTE2CP_ALGORITHM_COUNTER ASC";

            query = ConnectionManager.getInstance().fix_schema_name(query );

            //System.out.println("PJB this is the load algo query in the L1TT streams viewer: " + query);

            PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
            ps.setInt(1, matchedchainid);
            ResultSet rset = ps.executeQuery();

            while (rset.next()) {

                int compid = rset.getInt("HCP_ID");

                int L1typebitalgo           = Integer.parseInt(getTypeBit(compid));
                int L1typebitmaskalgo       = Integer.parseInt(getTypeBitMask(compid));
                String L1invertbitmaskalgo  = getInvertMask(compid);

                int finalbit;
                if(L1invertbitmaskalgo.equals("True")){
                    finalbit = L1typebitalgo & ~L1typebitmaskalgo;
                }else{
                    finalbit = L1typebitalgo & L1typebitmaskalgo;
                }

                if(L1typeitem == finalbit){
                    if(!supermatcheditems_v.contains(item.get_id())){
                        supermatcheditems_v.add(item.get_id());
                    }
                }
            }
            rset.close();
            ps.close();
        }
        return supermatcheditems_v;
    }
        ///get the typebit for a special algo

    /**
     *
     * @param compid
     * @return
     * @throws SQLException
     */
    public static String getTypeBit(int compid) throws SQLException {            
        String TypeBit = "";
        String query = "SELECT HPA_NAME, HPA_VALUE, HPA_ID, HCP2PA_PARAMETER_ID, HCP2PA_COMPONENT_ID " +
                       "FROM HLT_CP_TO_PA, HLT_PARAMETER " +
                       " WHERE " +
                       " HCP2PA_COMPONENT_ID = ? " +
                       " AND " +
                       " HCP2PA_PARAMETER_ID=HPA_ID " +
                       " AND " +
                       " HPA_NAME = 'TriggerTypeBit'"
                       ;

        query = ConnectionManager.getInstance().fix_schema_name(query);

        ////System.out.println("Typebit query" + query);

        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ps.setInt(1, compid);
        ResultSet rset = ps.executeQuery();

        if (rset.next()) {
            TypeBit = rset.getString("HPA_VALUE");
        }
        rset.close();
        ps.close();

        //System.out.println("Returning " + TypeBit);
        return TypeBit;
    }


    ///get the typebitmark for a special algo

    /**
     *
     * @param compid
     * @return
     * @throws SQLException
     */
    public static String getTypeBitMask(int compid) throws SQLException {
        String TypeBitMask = "";
        String query = "SELECT HPA_NAME, HPA_VALUE, HPA_ID, HCP2PA_PARAMETER_ID, HCP2PA_COMPONENT_ID " +
                       "FROM HLT_CP_TO_PA, HLT_PARAMETER " +
                       " WHERE " +
                       " HCP2PA_COMPONENT_ID = ? " +
                       " AND " +
                       " HCP2PA_PARAMETER_ID=HPA_ID " +
                       " AND " +
                       " HPA_NAME = 'TriggerTypeBitMask'"
                       ;

        query = ConnectionManager.getInstance().fix_schema_name(query);

        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ps.setInt(1, compid);
        ResultSet rset = ps.executeQuery();

        if (rset.next()) {
            TypeBitMask = rset.getString("HPA_VALUE");
        }
        rset.close();
        ps.close();

        return TypeBitMask;
    }
    
    //invert mask

    /**
     *
     * @param compid
     * @return
     * @throws SQLException
     */
    public static String getInvertMask(int compid) throws SQLException {     
        String InvertMask = "";
         
        String query = "SELECT HPA_NAME, HPA_VALUE, HPA_ID, HCP2PA_PARAMETER_ID, HCP2PA_COMPONENT_ID " +
                               "FROM HLT_CP_TO_PA, HLT_PARAMETER " +
                               " WHERE " +
                               " HCP2PA_COMPONENT_ID = ? " +
                               " AND " +
                               " HCP2PA_PARAMETER_ID=HPA_ID " +
                               " AND " +
                               " HPA_NAME = 'InvertBitMaskSelection'"
                               ;
                               
        query = ConnectionManager.getInstance().fix_schema_name(query);

        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ps.setInt(1, compid);
        ResultSet rset = ps.executeQuery();

        if (rset.next()) {
            InvertMask = rset.getString("HPA_VALUE");
        }
        rset.close();
        ps.close();

        return InvertMask;
    } 
        
    /**
     *
     * @param chainid
     * @return
     * @throws SQLException
     */
    public static List<Integer> GetAlgoIds(Integer chainid) throws SQLException {
        List<Integer> ids = new ArrayList<>();
        //use a query to get the algos
        String query = "SELECT HCP_ID, HCP_NAME, HTE2CP_ALGORITHM_COUNTER, HTE2CP_TRIGGER_ELEMENT_ID, HTE2CP_ID " +
                "FROM " +
                "HLT_TC_TO_TS, " +
                "HLT_TS_TO_TE, " +
                "HLT_TE_TO_CP, " +
                "HLT_COMPONENT " +
                "WHERE " +
                "HCP_NAME LIKE 'L1InfoHypo' " +
                "AND " +
                "HTC2TS_TRIGGER_CHAIN_ID=? " +
                "AND " +
                "HTC2TS_TRIGGER_SIGNATURE_ID=HTS2TE_TRIGGER_SIGNATURE_ID " +
                "AND " +
                "HTS2TE_TRIGGER_ELEMENT_ID=HTE2CP_TRIGGER_ELEMENT_ID " +
                "AND " +
                "HTE2CP_COMPONENT_ID=HCP_ID " +
                "ORDER BY HTE2CP_ALGORITHM_COUNTER ASC";
        
        query = ConnectionManager.getInstance().fix_schema_name(query );
        
        //System.out.println("PJB this is the load algo query in the L1TT streams viewer: " + query);
        
        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ps.setInt(1, chainid);
        ResultSet rset = ps.executeQuery();
        
        while (rset.next()) {
            int compid = rset.getInt("HCP_ID");
            ids.add(compid);
        }
        rset.close();
        ps.close();
        
        return ids;
    }

    /**
     *
     * @param l1_menu_id
     * @return
     * @throws SQLException
     */
    public static List<L1Item> GetL1TriggerItems(int l1_menu_id) throws SQLException {
        //at this point, we already have all the chains in chains_v
        //so need to get all L1 items, and see if the type matches the type looked for in the algo of the chain
        List<L1Item> items = new ArrayList<>();
        String query = "SELECT " +
                "L1TI_ID, L1TM2TI_TRIGGER_ITEM_ID, L1TM2TI_TRIGGER_MENU_ID " +
                "FROM " +
                "L1_TRIGGER_ITEM, L1_TM_TO_TI " +
                "WHERE " +
                "L1TM2TI_TRIGGER_MENU_ID =? " +
                "AND " +
                "L1TM2TI_TRIGGER_ITEM_ID = L1TI_ID " +
                "ORDER BY L1TI_ID ASC";
        
        query = ConnectionManager.getInstance().fix_schema_name(query);
        
        ////System.out.println("Items query " + query);
        
        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ps.setInt(1, l1_menu_id);
        ResultSet rset = ps.executeQuery();
        while (rset.next()) {
            L1Item item = new L1Item(rset.getInt("L1TI_ID"));
            items.add(item);
        }
        rset.close();
        ps.close();
        
        return items;
    }
}
