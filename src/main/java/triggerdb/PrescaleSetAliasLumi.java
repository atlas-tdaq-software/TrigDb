package triggerdb;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import triggerdb.Entities.HLT.HLTPrescaleSet;
import triggerdb.Entities.L1.L1Prescale;
import triggerdb.Entities.SMT.SuperMasterTable;
import triggerdb.Entities.alias.HLTPrescaleSetAliasLumi;
import triggerdb.Entities.alias.L1PrescaleSetAliasLumi;
import triggerdb.Entities.alias.PrescaleSetAlias;
import triggerdb.Entities.alias.PrescaleSetAliasLumiI;

/**
 * /*
 * High level class that holds and manages a pair of L1PrescaleSetAliasLumi and
 * HLTPrescaleSetAliasLumi. This simplifies the handling of L1 and HLT Prescale
 * Aliases. 
 * 
 * @date 2012-07-19
 * @author Tiago Perez <tperez@cern.ch>
 */
 public final class PrescaleSetAliasLumi
        implements PrescaleSetAliasLumiI, Comparable<PrescaleSetAliasLumi> {


    private final L1PrescaleSetAliasLumi l1psa;
    private final HLTPrescaleSetAliasLumi hpsa;

    /**
     * Empty constructor.
     */
    public PrescaleSetAliasLumi()  {
        this.l1psa = new L1PrescaleSetAliasLumi();
        this.hpsa = new HLTPrescaleSetAliasLumi();
    }

    /**
     * Initializes a PrescaleSetAliasLumi from the pair L1 Prescale Set Alias
     * and HLT Prescale Set Alias.
     * @param l1psaId the L1PrescaleSetAlias Id
     * @param hpsaId  the HLTPrescaleSetAlias Id
     * @throws java.sql.SQLException
     */
    public PrescaleSetAliasLumi(final int l1psaId, final int hpsaId) throws SQLException {
        this.l1psa = new L1PrescaleSetAliasLumi(l1psaId);
        this.hpsa = new HLTPrescaleSetAliasLumi(hpsaId);
    }

    /**
     *
     * @param _l1psa
     * @param _hpsa
     * @throws SQLException
     */
    public PrescaleSetAliasLumi(final L1PrescaleSetAliasLumi _l1psa,
            final HLTPrescaleSetAliasLumi _hpsa) throws SQLException {
        if (_l1psa != null) {
            this.l1psa = _l1psa;
        } else {
            this.l1psa = new L1PrescaleSetAliasLumi(-1);
        }
        if (_hpsa != null) {
            this.hpsa = _hpsa;
        } else {
            this.hpsa = new HLTPrescaleSetAliasLumi(-1);
        }
        if (this.l1psa.get_id() < 0 && this.hpsa.get_id() > 0) {
            this.set_lum_min(this.hpsa.get_lum_min());
            this.set_lum_max(this.hpsa.get_lum_max());
            this.set_comment(this.hpsa.get_comment());
            this.set_alias(this.hpsa.get_alias());
        } else if (this.l1psa.get_id() > 0 && this.hpsa.get_id() < 0) {
            this.set_lum_min(this.l1psa.get_lum_min());
            this.set_lum_max(this.l1psa.get_lum_max());
            this.set_comment(this.l1psa.get_comment());
            this.set_alias(this.l1psa.get_alias());
        }
    }

    /**
     * 
     * @param smt The super master table
     * @param aliasId the alias id.
     * @param l1Psk the L1 Prescale Key.
     * @param hltPsk the HLT Prescale Key.
     * @throws java.sql.SQLException
     */
    public PrescaleSetAliasLumi(final SuperMasterTable smt, final int aliasId,
            final int l1Psk, final int hltPsk) throws SQLException {
        int _l1MenuId = smt.get_l1_master_table().get_menu_id();
        int _hltMenuId = smt.get_hlt_master_table().get_menu_id();
        this.l1psa = new L1PrescaleSetAliasLumi(_l1MenuId, aliasId, l1Psk);
        this.hpsa = new HLTPrescaleSetAliasLumi(_hltMenuId, aliasId, hltPsk);
    }

    /**
     *
     * @return
     */
    public L1PrescaleSetAliasLumi get_l1_prescale_set_alias() {
        return this.l1psa;
    }

    /**
     *
     * @return
     */
    public HLTPrescaleSetAliasLumi get_hlt_prescale_set_alias() {
        return this.hpsa;
    }

    @Override
    public int get_alias_id() {
        return this.l1psa.get_alias_id();
    }

    @Override
    public void set_alias_id(int aliasId) {
        this.l1psa.set_alias_id(aliasId);
        this.hpsa.set_alias_id(aliasId);
    }

    @Override
    public PrescaleSetAlias get_alias() {
        return this.l1psa.get_alias();
    }

    /**
     *
     * @param alias
     */
    public void set_alias(PrescaleSetAliasComponent alias) {
        this.l1psa.set_alias(alias.get_alias());
        this.hpsa.set_alias(alias.get_alias());
    }

    @Override
    public void set_alias(PrescaleSetAlias alias) {
        this.l1psa.set_alias(alias);
        this.hpsa.set_alias(alias);
    }

    @Override
    public Boolean isDdefault() {
        return this.l1psa.isDdefault() && this.hpsa.isDdefault();
    }

    @Override
    public Boolean get_default() {
        return this.isDdefault();
    }

    @Override
    public void set_default(final Boolean def) {
        this.l1psa.set_default(def);
        this.hpsa.set_default(def);
    }

    @Override
    public String get_lum_max() {
        return this.l1psa.get_lum_max();
    }

    @Override
    public void set_lum_max(final String lmax) {
        this.l1psa.set_lum_max(lmax);
        this.hpsa.set_lum_max(lmax);
    }

    public void set_lum_max_bulk(final String lmax) {
        this.l1psa.set_lum_max_bulk(lmax);
        this.hpsa.set_lum_max_bulk(lmax);
    }

    @Override
    public String get_lum_min() {
        return this.l1psa.get_lum_min();
    }

    @Override
    public void set_lum_min(final String lmin) {
        this.l1psa.set_lum_min(lmin);
        this.hpsa.set_lum_min(lmin);
    }

    public void set_lum_min_bulk(final String lmin) {
        this.l1psa.set_lum_min_bulk(lmin);
        this.hpsa.set_lum_min_bulk(lmin);
    }

    
    /**
     * @return the comment.
     */
    @Override
    public String get_comment() {
        String theComment = "";
        String _l1Comment = this.l1psa.get_comment();
        String _hComment = this.hpsa.get_comment();
        if (_l1Comment != null && _hComment != null) {

            if (!_l1Comment.equalsIgnoreCase(_hComment)) {
                theComment = _l1Comment + "\n --------------\n" + _hComment;
            } else {
                theComment = _l1Comment;
            }
        }
        return theComment;
    }

    @Override
    public void set_comment(final String comment) {
        this.l1psa.set_comment(comment);
        this.hpsa.set_comment(comment);
    }

//    public void update() {
//        this.l1psa.update();
//        this.hpsa.update();
//    }
    /**
     * Saved the L1PrescaleSetAliasLumi and HLTPrescaleSetAliasLumi belonging to
     * this object.
     *
     * @return
     * @throws java.sql.SQLException
     */
    public int save() throws SQLException{
        int s1 = this.get_l1_prescale_set_alias().save();
        int s2 = this.get_hlt_prescale_set_alias().save();
        if(s1 == -1 || s2 == -1){
            return -1;
        }
        return s1 * s2;
    }

    /**
     * Compare to the L1 Prescale Set Alias. this is for simplicity, L1 or HLT
     * return the same result.
     * @param psal
     * @return 
     */
    @Override
    public int compareTo(final PrescaleSetAliasLumi psal) {
        int comp = this.l1psa.compareTo(psal.get_l1_prescale_set_alias());
        if (comp == 0) {
            comp = this.get_hlt_prescale_set_alias().compareTo(psal.get_hlt_prescale_set_alias());
        }
        return comp;
    }

    /**
     * @return A string containing the ID, name and version of the record.
     */
    @Override
    public String toString() {
        String lumiInfo = this.get_alias().toString();
        lumiInfo += " : ";
        lumiInfo += this.get_lum_min() + " < L < " + this.get_lum_max();
        try {
            lumiInfo += " (L1PSK: " + this.get_l1_prescale_set_alias().get_prescale_set_id() + " / HPSK: " + this.get_hlt_prescale_set_alias().get_prescale_set_id() + ")";
        } catch (SQLException ex) {
            Logger.getLogger(PrescaleSetAliasLumi.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lumiInfo;
    }

    /**
     * Set the L1 Prescale
     *
     * @param l1ps the new L1Prescale.
     */
    public void set_l1_prescale_set(final L1Prescale l1ps) {
        this.get_l1_prescale_set_alias().set_prescale_set(l1ps);
    }
    
    public L1Prescale get_l1_prescale_set() throws SQLException {
        return this.get_l1_prescale_set_alias().get_prescale_set();
    }

    /**
     * Set the HLT Prescale Set
     *
     * @param ps the new HLT prescale set.
     */
    public void set_hlt_prescale_set(final HLTPrescaleSet ps) {
        this.get_hlt_prescale_set_alias().set_prescale_set(ps);
    }

    @Override
    public void update() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void delete() throws SQLException {
        this.get_l1_prescale_set_alias().delete();
        this.get_hlt_prescale_set_alias().delete();
    }
}
