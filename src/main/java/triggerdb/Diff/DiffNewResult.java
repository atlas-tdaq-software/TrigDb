/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package triggerdb.Diff;

import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.HLT.HLTComponent;

/**
 * Helper class to store 'new' tables from the diffing.
 * @author markowen
 */
public class DiffNewResult {
    /// The ID of the new item
    private int id = -1;
    /// The name of the new item
    private String name = null;
    /// The table name of the item
    private String tableName = null;
    /// Parent of the diffing
    private AbstractTable parent = null;

    /// Make a new object from an abstract table

    /**
     *
     * @param table
     * @param theparent
     */
    public DiffNewResult(AbstractTable table, AbstractTable theparent) {
        id = table.get_id();
        tableName = table.getTableName();
        if (tableName.equals("HLT_COMPONENT")) {
            name = table.get_name() + " " + ((HLTComponent) table).get_alias();
        } else {
            name = table.get_name();
        }
        parent = theparent;
    }

    /**
     *
     * @return
     */
    public int getID() {
        return id;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @return
     */
    public String getTableName() {
        return tableName;
    }

    @Override
    public String toString() {
        if (name != null) {
            if (parent == null) {
                return "New " + tableName + ": " + name;
            } else {
                return "New " + tableName + " in " + parent.getTableName() + " " + parent.get_id() + ": " + name;
            }
        } else {
            //in case table does not have name field, display the ID
            if (parent == null) {
                return "New " + tableName + ": " + getID();
            } else {
                return "New " + tableName + " in " + parent.getTableName() + " " + parent.get_id() + ": " + getID();
            }
        }
    }
    
}
