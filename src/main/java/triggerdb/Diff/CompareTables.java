/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package triggerdb.Diff;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.tree.DefaultMutableTreeNode;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.HLT.HLTComponent;
import triggerdb.Entities.HLT.HLTParameter;
import triggerdb.Entities.HLT.HLTPrescale;
import triggerdb.Entities.HLT.HLTTriggerGroup;
import triggerdb.Entities.HLT.HLTTriggerType;
//import triggertool.Diff.TopNode;

/**
 *
 * @author Michele
 */
public class CompareTables {
    /**
     * Message Log.
     */
    protected static final Logger logger = Logger.getLogger("TriggerDb");
    /**
     * Function to compare two Collections of Tables. The tables to be compared
     * must inherit from AbstractTable.
     * The code checks for any items not in both lists and then looks to see if
     * they are new items, or just an updated item (version change only).
     * Results are added to the provided tree node.
     *
     * @param items1 - Collection 1 to be compared
     * @param items2 - Collection 2 to be compared
     * @param treeNode - Tree node to put the results in.
     * @return - number of differences.
     * @throws java.sql.SQLException
     */
    public static int compareTables(Collection<? extends AbstractTable> items1, Collection<? extends AbstractTable> items2, DefaultMutableTreeNode treeNode) throws SQLException {
        List<AbstractTable> newitems1 = new ArrayList<>();
        List<AbstractTable> newitems2 = new ArrayList<>();
        Map<AbstractTable, AbstractTable> changes = new HashMap<>();
        int ndiff = compareTables(items1, items2, newitems1, newitems2, changes);
        AbstractTable parent1 = null;
        AbstractTable parent2 = null;
        if (((DefaultMutableTreeNode) treeNode.getRoot()).getUserObject() instanceof TopNode) {
            TopNode thetopnode = (TopNode) ((DefaultMutableTreeNode) treeNode.getRoot()).getUserObject();
            parent1 = thetopnode.table1;
            parent2 = thetopnode.table2;
        }
        for (AbstractTable item : newitems1) {
            treeNode.add(new DefaultMutableTreeNode(new DiffNewResult(item, parent1)));
        }
        for (AbstractTable item : newitems2) {
            treeNode.add(new DefaultMutableTreeNode(new DiffNewResult(item, parent2)));
        }
        addVersionChanges(changes, treeNode);
        return ndiff;
    }

    private static List<Integer> idsFromTableList(Collection<? extends AbstractTable> items) {
        List<Integer> ids = new ArrayList<>();
        for (AbstractTable entry : items) {
            ids.add(entry.get_id());
        }
        return ids;
    }

    /**
     * Function to compare two Collections of Tables. The tables to be compared
     * must inherit from AbstractTable.
     * The code checks for any items not in both lists and then looks to see if
     * they are new items, or just an updated item (version change only).
     * The results are put into the appropriate vectors.
     *
     * @param items1 - Collection of tables
     * @param items2 - Collection of tables
     * @param newitems1 - Vector that will hold the items in collection 1, but not 2 after the fn has been called.
     * @param newitems2 - Vector that will hold the items in collection 2, but not 1 after the fn has been called.
     * @param versionchanges - Map that will hold items that have changed between the 2 collections after the fn has been called.
     * @return number of differences found.
     */
    private static int compareTables(Collection<? extends AbstractTable> items1,
                                     Collection<? extends AbstractTable> items2,
                                     List<AbstractTable> newitems1,
                                     List<AbstractTable> newitems2,
                                     java.util.Map<AbstractTable, AbstractTable> versionchanges)
    {
        int ndiff = 0;
        if (items1 == null || items2 == null) {
            logger.severe("Collection passed to compareTables is null, exiting function");
            return ndiff;
        }
        List<AbstractTable> items_in1_not2 = new ArrayList<>();
        List<AbstractTable> items_in2_not1 = new ArrayList<>();

        List<Integer> ids1 = idsFromTableList(items1);
        List<Integer> ids2 = idsFromTableList(items2);

        for (AbstractTable entry : items1) {
            if (entry == null) {
                logger.finer("Null pointer encountered in compareTables, will Ignore");
                continue;
            }
            Integer id = entry.get_id();
            if ( ids2.contains(id)) {
                continue;
            }
            ndiff++;
            items_in1_not2.add(entry);
            logger.log(Level.FINE, "{0} in menu 1, but not 2", entry.get_name());
        }

        for (AbstractTable entry : items2) {
            if (entry == null) {
                logger.info("Null pointer encountered in compareTables, will Ignore");
                continue;
            }
            Integer id = entry.get_id();
            if (!ids1.contains(id)) {
                ndiff++;
                items_in2_not1.add(entry);
                logger.log(Level.FINE, "{0} in menu 2, but not 1", entry.get_name());
            }
        }

        for (AbstractTable test : items_in1_not2) {
            boolean found = false;
            if (test.get_name() != null) {
                for (AbstractTable item2 : items2) {
                    if (test.get_name().equals(item2.get_name())) {
                        if (test instanceof HLTComponent) {
                            if (!((HLTComponent) test).get_alias().equals(((HLTComponent) item2).get_alias())) {
                                continue;
                            }
                        }
                        try {
                            String vtest = test.get_version().toString();
                            versionchanges.put(test, item2);
                        } catch (NullPointerException ex) {
                            logger.fine("This type does not have a valid version");
                            versionchanges.put(test, item2);
                        } finally {
                            found = true;
                            items_in2_not1.remove(item2);
                            ndiff++;
                        }
                    }
                }
            } else {
                if (test instanceof HLTPrescale) {
                    HLTPrescale ps1 = (HLTPrescale) test;
                    for (AbstractTable item2 : items2) {
                        if (item2 instanceof HLTPrescale) {
                            HLTPrescale ps2 = (HLTPrescale) item2;
                            if (ps1.get_chain_counter().equals(ps2.get_chain_counter()) && ps1.get_type().equals(ps2.get_type())) {
                                versionchanges.put(test, item2);
                                found = true;
                                items_in2_not1.remove(item2);
                                ndiff++;
                                break; //stop searching
                            }
                        }
                    }
                }
            }
            if (!found) {
                newitems1.add(test);
                ndiff++;
            }
        }
        for (AbstractTable test : items_in2_not1) {
            boolean found = false;
            if (test.get_name() != null) {
                for (AbstractTable item1 : items1) {
                    if (test.get_name().equals(item1.get_name())) {
                        if (test instanceof HLTComponent) {
                            if (!((HLTComponent) test).get_alias().equals(((HLTComponent) item1).get_alias())) {
                                continue;
                            }
                        }
                        try {
                            String vtest = test.get_version().toString();
                            versionchanges.put(test, item1);
                        } catch (NullPointerException ex) {
                            logger.fine("This type does not have a valid version");
                            versionchanges.put(test, item1);
                        } finally {
                            found = true;
                            ndiff++;
                        }
                    }
                }
            } else {
                if (test instanceof HLTPrescale) {
                    HLTPrescale ps1 = (HLTPrescale) test;
                    for (AbstractTable item1 : items1) {
                        if (item1 instanceof HLTPrescale) {
                            HLTPrescale ps2 = (HLTPrescale) item1;
                            if (ps1.get_chain_counter().equals(ps2.get_chain_counter()) && ps1.get_type().equals(ps2.get_type())) {
                                versionchanges.put(test, item1);
                                found = true;
                                break; //stop searching
                            }
                        }
                    }
                }
            }
            if (!found) {
                ndiff++;
                newitems2.add(test);
            }
        }
        logger.log(Level.FINE, "N changes = {0}", versionchanges.size());
        logger.log(Level.FINE, "N new in 1 = {0}", newitems1.size());
        logger.log(Level.FINE, "N new in 2 = {0}", newitems2.size());
        return ndiff;
    }

    /**
     *
     * @param changes
     * @param treeNode
     * @throws SQLException
     */
    public static void addVersionChanges(Map<AbstractTable, AbstractTable> changes, DefaultMutableTreeNode treeNode) throws SQLException {
        for (AbstractTable key : changes.keySet()) {
            AbstractTable val = changes.get(key);
            String node = val.getTableName() + " " + val.get_name();
            if (val.getTableName().equals("HLT_COMPONENT")) {
                node = val.getTableName() + " " + val.get_name() + " " + ((HLTComponent) val).get_alias();
            }
            if (key instanceof HLTPrescale) {
                HLTPrescale ps = (HLTPrescale) key;
                node = val.getTableName() + " " + ps.get_type() + ", chain counter = " + ps.get_chain_counter();
            }
            DefaultMutableTreeNode chain = new DefaultMutableTreeNode(node);
            treeNode.add(chain);
            int ndiff = key.doDiff(val, chain, null);
            boolean add = true;
            if (ndiff == 1) {
                if (key instanceof HLTTriggerType && val instanceof HLTTriggerType) {
                    HLTTriggerType t1 = (HLTTriggerType) key;
                    HLTTriggerType t2 = (HLTTriggerType) val;
                    if (!(t1.get_trigger_chain_id()).equals(t2.get_trigger_chain_id())) {
                        add = false;
                    }
                }
                if (key instanceof HLTTriggerGroup && val instanceof HLTTriggerGroup) {
                    HLTTriggerGroup t1 = (HLTTriggerGroup) key;
                    HLTTriggerGroup t2 = (HLTTriggerGroup) val;
                    if (!(t1.get_trigger_chain_id()).equals(t2.get_trigger_chain_id())) {
                        add = false;
                    }
                }
            }
            if (ndiff == 0) {
                if (key instanceof HLTPrescale) {
                    add = false;
                }
                if (key.get_id() < 0 && key instanceof HLTParameter) {
                    if (!key.get_modified().equals(val.get_modified())) {
                        logger.log(Level.INFO, "Two HLTParameters ({0}) differ by modified time & have ndiff==0 - assuming no real difference", key.get_name());
                        add = false;
                    }
                }
                if (add) {
                    logger.log(Level.WARNING, "WARNING: version change for {0} and {1} has ndiff==0, please contact TriggerTool developers", new Object[]{key.get_name(), val.get_name()});
                    logger.log(Level.WARNING, "Size of TriggerMap = {0}, {1}", new Object[]{key.getTriggerMap().keySet().size(), val.getTriggerMap().keySet().size()});
                    for (String thekey : key.getTriggerMap().keySet()) {
                        Object value1 = key.getTriggerMap().get(thekey);
                        Object value2 = val.getTriggerMap().get(thekey);
                        logger.log(Level.WARNING, "Key {0} has vals = {1}, {2}", new Object[]{thekey, value1.toString(), value2.toString()});
                    }
                }
            }
            if (!add) {
                treeNode.remove(chain);
            }
        }
    }
    
}
