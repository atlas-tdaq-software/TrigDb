/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package triggerdb.Diff;

import triggerdb.Entities.AbstractTable;

/**
 * Helper class to store the top node of the diffing
 * @author markowen
 */
public class TopNode {
    /// Table 1

    /**
     *
     */
    public AbstractTable table1 = null;
    /// Table 2

    /**
     *
     */
    public AbstractTable table2 = null;

    /**
     *
     * @param thetable1
     * @param thetable2
     */
    public TopNode(AbstractTable thetable1, AbstractTable thetable2) {
        this.table1 = thetable1;
        this.table2 = thetable2;
    }

    @Override
    public String toString() {
        return table1.getTableName() + " ID1=" + table1.get_id() + " ID2=" + table2.get_id();
    }
    
}
