package triggerdb.Diff;

import javax.swing.table.AbstractTableModel;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

/**
 *  Table model for diff results.
 *
 * @author markowen
 */
public class DiffResultsTableModel extends AbstractTableModel {
    /** 
     *  3 columns
     *  for Parameter, val in smt1, val in smt2
     */
    static private List<String> columnNames = null;
              

    /**
     * Vector of the rows.
     */
    private final List< DiffResultsRow > data = new ArrayList<  >();
    
    /// help description
    private final StringBuffer helpDescription;
    
     /** 
      * Constructor to set up column names
     */
    public DiffResultsTableModel() {
        if(columnNames==null) {
            columnNames = new ArrayList<>(5);
            setDefaultColumnNames();
        }
        helpDescription = new StringBuffer("This table shows all differences relevant for this panel\n\n");
    }
      
    /**
     * Get The help string
     * @return the help
     */
    public String getHelp() {
        return helpDescription.toString();
    }
    
    /**
     *
     * @param add
     */
    public void addToHelp(String add) {
        helpDescription.append(add);
        helpDescription.append("\n");
    }
    
    /**
     *
     * @param id1
     * @param id2
     */
    public static void setSMKIDs(int id1, int id2) {
        if(columnNames==null) {
            columnNames = new ArrayList<>(5);
            setDefaultColumnNames();
        }
        columnNames.add(1, "Value in SMK " + String.valueOf(id1));
        columnNames.add(2, "Value in SMK " + String.valueOf(id2));
        columnNames.add(3, "ID in SMK " + String.valueOf(id1));
        columnNames.add(4, "ID in SMK " + String.valueOf(id2));
    }
    
    public List<String> getColumnNames(){
        return columnNames;
    }
    
    /**
     *
     */
    public static void setDefaultColumnNames() {
        if(columnNames==null) columnNames = new ArrayList<>(5);
        columnNames.clear();
        columnNames.add("Parameter");
        columnNames.add("Value in key 1");
        columnNames.add("Value in key 2");
        columnNames.add("ID in key 1");
        columnNames.add("ID in key 2");
    }
    
    /**
     * Function to change the column Names
     * @param col1 - Column 1 name
     * @param col2 - Column 2 name
     * @param col3 - Column 3 name
     * @param col4
     * @param col5
     */
    public void setColumnNames(String col1, String col2, String col3, String col4, String col5) {
        columnNames.clear();
        columnNames.add(col1);
        columnNames.add(col2);
        columnNames.add(col3);
        columnNames.add(col4);
        columnNames.add(col5);
    }
    
    public void setColumnNamesNull(){
        columnNames=null;
    }
    /** 
     * method to add a row to the end of the table
     * @param inrow
     */
    public void addRow( List<Object> inrow) {
        insertRow(getRowCount(),inrow);
    }

    /**
     *
     * @param inrow
     * @param lis
     */
    public void addRow( List<Object> inrow, MouseListener lis) {
        insertRow(getRowCount(),inrow,lis);
    }
    
    /** method to add row at position pos
     * 
     * @param pos       position of row to be inserted
     * @param inrow     the row to insert
     */
    public void insertRow( int pos, List<Object> inrow) {
        //check inrow has correct number of coumns
        if(inrow.size() != columnNames.size()) {
            //System.out.println("MARK ERROR: wrong size of row");
            //need to figure out exceptions
            //throw new DataFormatException("Attempting to insert row with incorrect number of columns");
        }
        else {
            data.add(pos, new DiffResultsRow(inrow));
            fireTableRowsInserted(pos,pos);
        }
    }
    
    /** 
     * method to add row at position pos
     * 
     * @param pos       position of row to be inserted
     * @param inrow     the row to insert
     * @param lis
     */
    public void insertRow( int pos, List<Object> inrow, MouseListener lis) {
        //check inrow has correct number of coumns
        if(inrow.size() != columnNames.size()) {
            //System.out.println("MARK ERROR: wrong size of row");
            //need to figure out exceptions
            //throw new DataFormatException("Attempting to insert row with incorrect number of columns");
        }
        else {
            data.add(pos, new DiffResultsRow(inrow,lis));
            fireTableRowsInserted(pos,pos);
        }
    }
     
    /**
     *
     * @param row
     */
    public void removeRow(DiffResultsRow row) {
        if(data.contains(row)) data.remove(row);
    }
    
    /** Reset the table contents (keeps the headers)
     * 
     */
    public void resetContents() {
        data.clear();
    }
    
    @Override
    public int getColumnCount() {
        return columnNames.size();
    }

    @Override
    public int getRowCount() {
        return data.size();
    }
    
    /**
     *
     * @param i
     * @return
     */
    public DiffResultsRow getRow(int i) {
        return data.get(i);
    }

    /**
     *
     * @return
     */
    public List<DiffResultsRow> getRows() {
        return data;
    }
    
    @Override
    public String getColumnName(int col) {
        return columnNames.get(col);
    }

    @Override
    public Object getValueAt(int row, int col) {
        return (data.get(row)).getRowContents().get(col);
    }
    

    
}
