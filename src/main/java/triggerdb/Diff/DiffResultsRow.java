package triggerdb.Diff;

import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.util.List;

/**
 * Class to hold a single row of diff results.
 * 
 * @author markowen
 */
public class DiffResultsRow  {
        
        /// the row contents
        private List<Object> rowContents = null;
        
        /// the listener for when the row is clicked on
        private MouseListener listener = null;
        
    /**
     *
     * @param therow
     */
    public DiffResultsRow(List<Object> therow) {
            rowContents = therow;
        }
        
    /**
     *
     * @param therow
     * @param lis
     */
    public DiffResultsRow(List<Object> therow, MouseListener lis) {
            rowContents = therow;
            listener = lis;
        }
        
    /**
     *
     * @param e
     */
    public void DoubleClick(MouseEvent e) {
            if(listener!=null) listener.mouseClicked(e);
        }
        
    /**
     *
     * @return
     */
    public List<Object> getRowContents() {
            return rowContents;
        }
        
    /**
     *
     * @param lis
     */
    public void AddListener(MouseListener lis) {
            listener = lis;
        }
        
    /**
     *
     * @return
     */
    public final MouseListener getListener() {
            return listener;
        }
        
        /**
         * Function to see if two rows are the same.
         * @param obj
         * @return
         */
        @Override
        public boolean equals(Object obj) {
            if(this==obj) return true;
            if(obj==null || this==null) return false;
            
            if( obj.getClass() == this.getClass() ) {
                DiffResultsRow row = (DiffResultsRow)obj;
                if(!this.getRowContents().equals(row.getRowContents())) return false;
                return this.getListener().equals(row.getListener());
            }
            else return false;
        }
        
        @Override
        public int hashCode() {
            int hash =7;
            hash = 31*hash + (null == rowContents ? 0 : rowContents.hashCode());
            hash += (null == listener ? 0 : listener.hashCode());
            return hash;
        }
        
}
