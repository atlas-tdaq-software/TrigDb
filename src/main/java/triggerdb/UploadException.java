/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb;

/**
 *
 * @author stelzer
 */
@SuppressWarnings("serial")
public class UploadException extends Exception {

    /**
     *
     * @param what
     */
    public UploadException(String what) {
        super(what);
    }
    
}
