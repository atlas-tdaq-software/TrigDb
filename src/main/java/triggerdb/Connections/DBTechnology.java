/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerdb.Connections;

/**
 *
 * @author stelzer
 */
public enum DBTechnology {

    /**
     *
     */
    ORACLE("oracle"),

    /**
     *
     */
    DBLOOKUP("dblookup"),

    /**
     *
     */
    UNKNOWN("unknown");

    private final String rep;
    
    private DBTechnology(final String rep) {
        this.rep = rep;
    }

    @Override public String toString(){
      return rep;
    }
    
}
