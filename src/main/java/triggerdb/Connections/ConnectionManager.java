package triggerdb.Connections;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import oracle.jdbc.OracleConnection;
import triggerdb.Entities.AbstractTable;
import triggerdb.Entities.InternalWriteLock;
import triggerdb.TableNames;

/**
 * Handles the connection to the Trigger Database. The new, reduced
 * ConnectionManager class. Deals with the database connection. Now Oracle only.
 * Also takes care of the ATONR connection, and the differences between dealing
 * with that and a normal connection. e.g. user access restrictions.
 *
 * @author Simon Head
 */
public class ConnectionManager {

    /**
     * Total time to prepare statements.
     */
    public static long T0;
    /**
     * Total time to execute queries.
     */
    public static long T1;
    /**
     * Total time to loop over Results sets and load.
     */
    public static long T2;
    /**
     * Total number of calls to forceLoad().
     */
    public static int HITS;
    /**
     * Schema that this version of the TriggerTool requires. Held in
     * trigger_schema table, as the id TODO externalize this.
     */
    public static final int SCHEMA_VERSION = 8;
    /**
     * Limit of children to load in one go (to avoid mem problems).
     */
    private static final int MAX_CHILDREN = 9000000;
    /**
     * db schema version.
     */
    private int db_schema_version = 0;
    /**
     * Message Log.
     */
    protected static final Logger logger = Logger.getLogger("TriggerDb");
    /**
     * Singleton pointer.
     */
    private static ConnectionManager connectionManager_ = null;
    /**
     * Singleton. Holds connection to main db.
     */
    private static ConnectionManager connectionManagerMainDB = null;
    /**
     * Singleton. Holds connection to copy db.
     */
    private static ConnectionManager connectionManagerCopyDB = null;
    /**
     * We need to keep a copy of the InitInfo.
     */
    private InitInfo savedInitInfo;
    /**
     * Path for logging and TMC files.
     */
    private String log_path;
    /**
     * Connection is set up when the user logs in. The connection is reused in
     * any successive SQL query.
     */
    protected Connection connection_ = null;

    private static final int returnValueForUnsupportedMET = -1;

    private static final int returnValueForUnsupportedPrescaleAlias = -2;

    //Flag to check if added columns exist in the DB you are looking at.
    /**
     *
     */
    public static int L1ItemMonitorColumnExists = 0;

    /**
     *
     */
    public static int TopoStartFromColumnExists = 0;

    static {
        logger.log(Level.INFO, "Default TimeZone is: {0}", TimeZone.getDefault());
        if (!TimeZone.getDefault().getID().equals("Europe/Zurich")) {
            TimeZone.setDefault(TimeZone.getTimeZone("Europe/Zurich"));
            logger.log(Level.INFO, "Default TimeZone set to: {0}", TimeZone.getDefault());
        }
    }

    /**
     * Loads the Schema version from a connection without
     * "fixing_the_schema_name".
     *
     * @param con the connection.
     * @return the schema version.
     */
    public static int loadSchemaVersionStatic(Connection con) {
        int schema_version = 0;
        String query = "SELECT MAX(TS_ID) FROM TRIGGER_SCHEMA";
        try (PreparedStatement ps = con.prepareStatement(query); ResultSet rset = executeSelect(ps)) {
            while (rset.next()) {
                schema_version = rset.getInt(1);
            }
            rset.close();
            ps.close();
        } catch (SQLException ex) {
            String msg = "Caught error during schema version check: " + ex.getMessage();
            JOptionPane.showMessageDialog(null, msg, "Error", JOptionPane.ERROR_MESSAGE);
            logger.severe(msg);
            System.exit(1);
        }
        return schema_version;
    }

    /**
     * This registers the additional JAR packages we need to talk to the
     * database(s) using Oracle.
     *
     * @param logger
     */
    public ConnectionManager(Logger logger) {
        if (logger != null) {
            if (logger.getHandlers().length == 1) {
                ConnectionManager.logger.addHandler(logger.getHandlers()[0]);
            }

            try {
                DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
            } catch (SQLException e) {
                logger.log(Level.SEVERE, "Error loading Oracle driver\n", e);
            }

            log_path = System.getenv("TRIGGERTOOL_LOGDIRECTORY");

            if (log_path == null) {
                log_path = "";
            } else if (log_path.lastIndexOf("/") != log_path.length() - 1) {
                log_path += "/";
            }
        }
    }

    /**
     * @return true if the connection is open
     */
    public boolean isConnectionOpen() {
        return connection_ != null;
    }

    /**
     * Form a connection to the relevant database. At the moment this can
     * connect to Oracle only. It also stores a copy of the initialisation
     * information in savedInitInfo
     *
     * Does a lot of tricky things on atonr, including looking up a user's
     * access level and signing in as either the read only or write account
     * based on its findings.
     *
     * @param initInfo Object that contains the start-up information
     * @return true if the connection was made ok
     * @throws SQLException if something goes wrong connecting to DB.
     */
    public boolean connect(final InitInfo initInfo)
            throws SQLException {

        savedInitInfo = initInfo;
        String url = initInfo.getURL();
        String username = initInfo.getUserName();
        String password = initInfo.getPassWord();
        String msg = " Connecting to " + url;
        msg += "\n System user " + savedInitInfo.getSystemUsername();
        msg += "\n DB username " + username;
        msg += "\n Role        " + savedInitInfo.getUserMode();
        boolean online = savedInitInfo.getOnline();
        boolean onlineDB = savedInitInfo.getOnlineDB();
        msg += "\n online      " + (online ? "Yes" : "No");
        msg += "\n onlineDB    " + (onlineDB ? "Yes" : "No");

        logger.info(msg);

        // oracle
        String r_username = "atlas_conf_trigger_v2_r"; //$NON-NLS-1$
        // Checking user credentials on ATONR
        if (initInfo.getdbServer().contains("atonr")) {

            if (initInfo.getUserName().toLowerCase().contains("atlas_conf_trigger_v2_w")) {

                if (initInfo.isOnlineExpert() && initInfo.IsCommandLine()) { // no authentication required
                    getInitInfo().setUserMode(UserMode.EXPERT);
                    logger.log(Level.INFO, "Recognized user ''{0}'' as online or menu expert. In this case in commandline mode no authentication required at P1. Role switch to ''{1}''", new Object[]{initInfo.getSystemUsername(), getInitInfo().getUserMode()});
                } else if (initInfo.getSystemUsername().equals("AutoPrescaleCTP")) { // no authentication required
                    getInitInfo().setUserMode(UserMode.SHIFTER);
                    logger.info("Recognized AutoPrescaler.");
                } else {
                    String atonrR_password = initInfo.getAtonrRPassword();
                    // log on first as reader to check if privilege ok to log
                    // on as writer!
                    logger.fine("Attempting to log on to atlas_conf_trigger_v2_r@atonr to get the user credentials");
                    connection_ = DriverManager.getConnection(url, r_username, atonrR_password);

                    logger.log(Level.INFO, "User level requested: {0}", initInfo.getUserMode());
                    boolean pw_ok = checkPassword(getInitInfo().getSystemUsername(),
                            getInitInfo().encryptAtonrPassword());
                    if (!pw_ok) {
                        throw new SQLException("Incorrect password for " + getInitInfo().getUserMode() + " "
                                + getInitInfo().getSystemUsername(), "Error");
                    }
                }
            }
        } else if (initInfo.getdbServer().toLowerCase().contains("atlr")) { // || //$NON-NLS-1$
            if (initInfo.getUserName().toLowerCase().contains("atlas_conf_trigger_v2_r")) {
                // Reader account on atlr
                username = r_username;
                getInitInfo().setUserName(username);
                // password is set in the box - users must know it!
                getInitInfo().setSchemaName("atlas_conf_trigger_v2"); //$NON-NLS-1$
            }
        }

        logger.fine(url);
        logger.fine(username);
        connection_ = DriverManager.getConnection(url, username, password);

        if (initInfo.getTechnology() == DBTechnology.ORACLE) { //$NON-NLS-1$
            ((OracleConnection) connection_).setStatementCacheSize(100);
            ((OracleConnection) connection_).setImplicitCachingEnabled(true);
        }

        // check the schema version matches the trigger tool
        Boolean ok = (checkDBVersionCompatible() != 0);
        L1ItemMonitorColumnExists = check_monitor_column();
        TopoStartFromColumnExists = check_topo_start_from_column();

        // Create Oracle DatabaseMetaData object
        DatabaseMetaData meta = connection_.getMetaData();

        // gets driver info:
        System.out.println("JDBC driver version is " + meta.getDriverVersion());

        return ok;
    }

    /**
     * Closes the current connection, if any, and tries to remove any existing
     * write lock.
     *
     * @throws SQLException if problems with the db communication.
     */
    public void disconnect() throws SQLException {
        if (connection_ != null) {
            String msg = " Trying to remove write lock.";
            logger.info(msg);
            InternalWriteLock writeLock = InternalWriteLock.getInstance();
            if (writeLock.isLocked()) {
                logger.info("Removing write lock");
                writeLock.removeLock();
            }
            logger.info(" Closing db connection.");
            connection_.close();
        }
        connection_ = null;
    }

    /**
     *
     * @param user
     * @param pwd
     * @param mode
     */
    public void changeUser(String user, String pwd, UserMode mode) {
        boolean good = false;

        if (mode == UserMode.USER) {
            good = true;
        } else {
            try {
                good = checkPassword(user, getInitInfo().encrypt(pwd));
            } catch (SQLException ex) {
                logger.log(Level.SEVERE, ex.getMessage(), ex);
            }
        }

        if (good) {
            try {
                connection_.close();

                if (mode == UserMode.USER) {
                    getInitInfo().setUserName("atlas_conf_trigger_v2_r"); //$NON-NLS-1$
                    getInitInfo().setUserMode(mode);
                } else {
                    getInitInfo().setUserName("atlas_conf_trigger_v2_w"); //$NON-NLS-1$
                    getInitInfo().setSystemUsername(user);
                    getInitInfo().setPassWord(getInitInfo().getAtonrWPassword());
                    getInitInfo().setOnlineUserPassword(pwd);
                    getInitInfo().setUserMode(mode);
                }

                connect(getInitInfo());
            } catch (SQLException ex) {
                logger.log(Level.SEVERE, ex.getMessage(), ex);
            }
        }
    }
    
    private boolean checkPassword(String user, String pwd) throws SQLException {
        boolean pw_ok = false;
        String query = "SELECT TT_LEVEL, TT_PASSWORD FROM ATLAS_CONF_TRIGGER_V2.TT_USERS WHERE TT_USER=?"; //$NON-NLS-1$
        try (PreparedStatement ps = getConnection().prepareStatement(query)) {
            ps.setString(1, user);
            try (ResultSet rset = executeSelect(ps)) {

                while (rset.next()) {

                    // ugly
                    UserMode allowedmode = UserMode.UNKNOWN;
                    String userLevel = rset.getString("TT_LEVEL");

                    // in db TriggerMenuMeister = Expert and TriggerMeister = DBA
                    switch (userLevel) {
                        case "Shifter":
                            allowedmode = UserMode.SHIFTER;
                            break;
                        case "TriggerMenuMeister":
                        case "Expert":
                            allowedmode = UserMode.EXPERT;
                            break;
                        case "TriggerMeister":
                        case "DBA":
                            allowedmode = UserMode.DBA;
                    }
                    pw_ok = rset.getString("TT_PASSWORD").equals(pwd) && allowedmode.ordinal() >= getInitInfo().getUserMode().ordinal();
                }
                rset.close();
            }
            ps.close();
        }
        getConnection().close();

        return pw_ok;
    }

    public boolean getPasswordOK() throws SQLException{
       boolean pw_ok = this.checkPassword("Shifter", "TT_PASSWORD");
       return pw_ok;
    }
    
    /**
     * Reload a row from the database. This is OK for a small number of rows,
     * but is far too slow to be used in general.
     *
     * @param aThis The record that we want reloading.
     * @throws java.sql.SQLException
     */
    public void forceLoad(AbstractTable aThis) throws SQLException {
        //logger.config(" ForceLoad " + aThis.tableName + " ID " + aThis.get_id());
        HITS++;
        try {
            long t0 = System.currentTimeMillis();
            String query = aThis.getQueryString();
            query += " WHERE " + aThis.tablePrefix + "ID=?";
            query = fix_schema_name(query);
            long t1 = System.currentTimeMillis();
            long t2;
            long t3;
            try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query)) {
                ps.setInt(1, aThis.get_id());
                try (ResultSet rset = executeSelect(ps)) {
                    t2 = System.currentTimeMillis();
                    while (rset.next()) {
                        aThis.loadFromRset(rset);
                    }
                    t3 = System.currentTimeMillis();
                    rset.close();
                }
                ps.close();
            }
            T0 += (t1 - t0);
            T1 += (t2 - t1);
            T2 += (t3 - t2);
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
            throw ex;
        }
    }

    /**
     * Check whether there is an entry in DB with the given id.
     *
     * @param table the Table class
     * @param id the id
     * @return <code>true</code> if entry exist.
     */
    public final boolean exists(final AbstractTable table, final int id) {
        int nEntries = returnValueForUnsupportedMET;
        String query = AbstractTable.QUERY_COUNT + table.get_name() + " WHERE " + table.tablePrefix + "ID=?";

        query = fix_schema_name(query);

        try {
            PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
            ps.setInt(1, id);
            ResultSet rset = executeSelect(ps);

            while (rset.next()) {
                nEntries = rset.getInt(1);
            }
            
            rset.close();
            ps.close();

        } catch (SQLException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        }
        return nEntries > 0;
    }

    /**
     * Load all tables of type child fulfilling the WEHRECLAUSE condition. This
     * will take care that the memory doesn't overflow and it will divide the
     * query into small steps if the original query were too long, check the
     * value of static var MAX_CHILDREN.
     *
     * @param child. a table of the type of the children we want to query, this
     * is needed to get the original query from.
     * @param whereClause the 'WHERE' condition to query.
     * @param filters a list to parameters to set on the query.
     * @return
     * @throws java.sql.SQLException
     */
    public ArrayList<AbstractTable> forceLoadVector(
            final AbstractTable child,
            final String whereClause,
            final List filters) throws SQLException {
        String query = child.getQueryString();
        return this.forceLoadVector(child, query, whereClause, filters);
    }

    /**
     * Load all tables of type child fulfilling the condition
     * QUERYCLAUSE+WEHRECLAUSE condition. This method takes care that the memory
     * doesn't overflow and it will divide the query into small steps if the
     * original query were too long, check the value of static var MAX_CHILDREN.
     *
     * @param child. a table of the type of the result we want.
     * @param query
     * @param where
     * @param filters a list to parameters to set on the query.
     * @return
     * @throws java.sql.SQLException
     *
     */
    public ArrayList<AbstractTable> forceLoadVector(
            final AbstractTable child,
            final String query,
            final String where,
            final List filters) throws SQLException {
        return this.forceLoadVector(child, query, where, filters, returnValueForUnsupportedMET, null);
    }

    /**
     * Load all tables of type child fulfilling the condition
     * QUERYCLAUSE+WEHRECLAUSE condition. This method takes care that the memory
     * doesn't overflow and it will divide the query into small steps if the
     * original query were too long, check the value of static var MAX_CHILDREN.
     *
     * @param child. a table of the type of the result we want.
     * @param queryClause The query to launch.
     * @param whereClause the 'WHERE' condition to query.
     * @param filters a list to parameters to set on the query.
     * @param extraColumnIdx the index of a col we want to get as is.
     * @param extraColumn the values of the extra column.
     * @return
     * @throws java.sql.SQLException
     *
     */
    public ArrayList<AbstractTable> forceLoadVector(
            final AbstractTable child,
            final String queryClause,
            final String whereClause,
            final List filters,
            final int extraColumnIdx,
            List<Integer> extraColumn) throws SQLException {
        // Maximum number of rows for each query
        final int LIMIT = ConnectionManager.MAX_CHILDREN;
        int OFFSET = 0;
        // The return vector
        ArrayList<AbstractTable> children = new ArrayList<>();
        // The basic SELECT Statement.
        String query = queryClause;
        // Check that the number of "?" in the where clause and the number of
        // parameter in the filter list match.
        int nParamsInWhere = whereClause.replaceAll("[^?]", "").length();
        if (nParamsInWhere != filters.size()) {
            logger.log(Level.SEVERE, " Wrong number of parameter given. "
                    + "\n QUERY = " + "{0}\n PARAMS = {1}", new Object[]{query, filters.size()});
            return null;
        }
        // The connection
        ConnectionManager mgr = ConnectionManager.getInstance();

        // DB Tech
        String dbTech = mgr.getInitInfo().getTechnology().toString();

        int orderByIdx = whereClause.indexOf("ORDER BY");
        String rowNumber = ", ROWNUM AS r";
        if (orderByIdx > returnValueForUnsupportedMET) {
            String ordr = whereClause.substring(orderByIdx);
            rowNumber = ", ROW_NUMBER() OVER ("
                    + ordr + ") AS r";

        }
        query = query.replace(" FROM", rowNumber + " FROM");
        query = "SELECT * FROM (\n" + query + "\n";
        query += whereClause;
        query += "\n) foo\n";
        query += "WHERE r <= ? AND r > ?";

        query = fix_schema_name(query);
        //System.out.println(query);
        int nResults = LIMIT;
        while (nResults >= LIMIT) {
            Connection con = mgr.getConnection();
            try (PreparedStatement ps = con.prepareStatement(query)) {
                int parIdx = 1;
                // Iterate over the list of filters and set the paramenters for the
                // query.
                for (Object par : filters) {
                    if (par instanceof Integer) {
                        ps.setInt(parIdx, ((Integer) par));
                    } else if (par instanceof String) {
                        ps.setString(parIdx, par.toString());
                    }
                    //System.out.println("PAR" + par);
                    parIdx++;
                }
                // set the Max number of results
                // For oracle use MAX
                ps.setInt(parIdx, LIMIT + OFFSET);
                //System.out.println("LIMIT" + LIMIT);

                ps.setInt(parIdx + 1, OFFSET);
                //System.out.println("OFFSET" + OFFSET);
                ResultSet rset = executeSelect(ps);
                nResults = 0;
                while (rset.next()) {
                    AbstractTable aTable = (AbstractTable) child.clone();
                    aTable.loadFromRset(rset);
                    /*if(child instanceof HLTPrescale){
                        HLTPrescale test = (HLTPrescale) aTable;
                        if(test.get_chain_counter() == 883){
                            System.out.println("DB " + test.get_type() + " nres " + nResults);
                        }
                    }*/

                    children.add(aTable);
                    if (extraColumnIdx > returnValueForUnsupportedMET) {
                        extraColumn.add(rset.getInt(extraColumnIdx));
                    }
                    nResults++;
                }
                rset.close();
                ps.close();
            }
            OFFSET += LIMIT;
            //System.out.println("nresults " + nResults);
        }
        return children;
    }

    /**
     *
     * @param classType
     * @param queryClause
     * @param whereClause
     * @param filters
     * @param extraColumnIdx
     * @param extraColumn
     * @return
     * @throws SQLException
     * @throws NoSuchMethodException
     * @throws InstantiationException
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    public ArrayList<AbstractTable> LoadVectorOf(
            final Class<? extends AbstractTable> classType,
            final String queryClause,
            final String whereClause,
            final List filters,
            final int extraColumnIdx,
            List<Integer> extraColumn) throws SQLException, InstantiationException, InvocationTargetException,
            IllegalAccessException, NoSuchMethodException {
        // Maximum number of rows for each query
        final int LIMIT = ConnectionManager.MAX_CHILDREN;
        int OFFSET = 0;
        // The return vector
        ArrayList<AbstractTable> children = new ArrayList<>();
        // The basic SELECT Statement.
        String query = queryClause;
        // Check that the number of "?" in the where clause and the number of
        // parameter in the filter list match.
        int nParamsInWhere = whereClause.replaceAll("[^?]", "").length();
        if (nParamsInWhere != filters.size()) {
            logger.log(Level.SEVERE, " Wrong number of parameter given. "
                    + "\n QUERY = " + "{0}\n PARAMS = {1}", new Object[]{query, filters.size()});
            return null;
        }
        // The connection
        ConnectionManager mgr = ConnectionManager.getInstance();

        // DB Tech
        int orderByIdx = whereClause.indexOf("ORDER BY");
        String rowNumber = ", ROWNUM AS r";
        if (orderByIdx > returnValueForUnsupportedMET) {
            String ordr = whereClause.substring(orderByIdx);
            rowNumber = ", ROW_NUMBER() OVER ("
                    + ordr + ") AS r";
        }

        query = query.replace(" FROM", rowNumber + " FROM");
        query = "SELECT * FROM (\n" + query + "\n";
        query += whereClause;
        query += "\n) foo\n";
        query += "WHERE r <= ? AND r > ?";

        query = fix_schema_name(query);

        int nResults = LIMIT;
        while (nResults >= LIMIT) {
            Connection con = mgr.getConnection();
            PreparedStatement ps = con.prepareStatement(query);

            int parIdx = 1;
            // Iterate over the list of filters and set the paramenters for the
            // query.
            for (Object par : filters) {
                if (par instanceof Integer) {
                    ps.setInt(parIdx, ((Integer) par));
                } else if (par instanceof String) {
                    ps.setString(parIdx, par.toString());
                }
                parIdx++;
            }
            // set the Max number of results
            // For oracle use MAX
            ps.setInt(parIdx, LIMIT + OFFSET);

            ps.setInt(parIdx + 1, OFFSET);
            ResultSet rset = executeSelect(ps);
            nResults = 0;
            while (rset.next()) {
                String prefix = classType.newInstance().tablePrefix;
                AbstractTable aTable = classType.getConstructor(int.class).newInstance(rset.getString(prefix + "_ID"));
                aTable.loadFromRset(rset);
                children.add(aTable);
                if (extraColumnIdx > returnValueForUnsupportedMET) {
                    extraColumn.add(rset.getInt(extraColumnIdx));
                }
                nResults++;
            }
            rset.close();
            ps.close();
            OFFSET += LIMIT;
        }
        return children;
    }

    /**
     *
     * @param length
     * @return
     */
    public static String preparePlaceHolders(int length) {
        StringBuilder builder = new StringBuilder(1000);
        for (int i = 0; i < length;) {
            builder.append("?");
            if (++i < length) {
                builder.append(",");
            }
        }
        return builder.toString();
    }

    /**
     *
     * @param preparedStatement
     * @param values
     * @throws SQLException
     */
    public static void setValues(PreparedStatement preparedStatement, Object... values) throws SQLException {
        for (int i = 0; i < values.length; i++) {
            preparedStatement.setObject(i + 1, values[i]);
        }
    }

    /**
     * Get the init info, which contains important information about the user
     * and the database connection parameters.
     *
     * @return The Init Info object.
     */
    public InitInfo getInitInfo() {
        return this.savedInitInfo;
    }

    /**
     * Set the initInfo, if not already set Used for testing
     *
     * @param initInfo
     */
    public void setInitInfo(final InitInfo initInfo) {
        if (savedInitInfo == null) {
            savedInitInfo = initInfo;
        }
    }

    /**
     * Use this method when referring to the connection, so that we can swap
     * between connection to read from and the conncetion to write to.
     *
     * @return The connection. If doDBCopy return the connection to write to.
     */
    public Connection getConnection() {
        if (connection_ == null) {
            logger.severe("No DB connection");
        }
        return connection_;
    }

    /**
     * Check whether this instance of ConnectionManager is connected to a copy
     * DB.
     *
     * @return <code>true</code> is connected to copy DB.
     */
    public boolean isDBCopy() {
        boolean isConnectedToCopyDB;
        isConnectedToCopyDB = ConnectionManager.connectionManagerCopyDB != null
                && this == ConnectionManager.connectionManagerCopyDB;
        return isConnectedToCopyDB;
    }

    /**
     * Swap back to the main DB connection and close & flush the copy DB
     * connection.
     *
     * @throws java.sql.SQLException
     */
    public static void disconnectFromCopyDB() throws SQLException {
        // Flush copy db connection.
        if (ConnectionManager.connectionManagerCopyDB != null) {
            ConnectionManager.connectionManagerCopyDB.getConnection().close();
        }
        ConnectionManager.connectionManagerCopyDB = null;
        // set back copy db connection.
        if (ConnectionManager.getInstance().isDBCopy()) {
            ConnectionManager.connectionManager_ = ConnectionManager.connectionManagerMainDB;
        }
    }

    /**
     * Function to connect to write db, uses connect(..) function - see below.
     *
     * @param initInfo - the InitInfo object
     * @return true if successful
     * @throws java.sql.SQLException
     */
    public static boolean connectToCopyDB(final InitInfo initInfo)
            throws SQLException {
        if (!ConnectionManager.getInstance().isDBCopy()) {
            // Back up main connection.
            ConnectionManager.connectionManagerMainDB = ConnectionManager.getInstance();
            // Create a new ConnectionManager instance to hold the copy connection.
            ConnectionManager.connectionManagerCopyDB = new ConnectionManager(logger);
            // make copy connection default
            ConnectionManager.connectionManager_ = ConnectionManager.connectionManagerCopyDB;
        }
        ConnectionManager mgr = ConnectionManager.getInstance();
        // Connect to copy DB.
        return mgr.connect(initInfo);
    }

    /**
     * Switch to the copy db connection. Main db connection stays open in
     * background.
     *
     * @return <code>true</code> if connection to copy db is open.
     */
    public static boolean switchToCopyDB() {
        boolean copyDBopen;
        if (ConnectionManager.getInstance().isDBCopy()) {
            copyDBopen = ConnectionManager.getInstance().isConnectionOpen();
        } else {
            if (ConnectionManager.connectionManagerCopyDB != null) {
                ConnectionManager.connectionManager_ = ConnectionManager.connectionManagerCopyDB;
                copyDBopen = ConnectionManager.getInstance().isConnectionOpen();
            } else {
                copyDBopen = false;
            }
        }
        return copyDBopen;
    }

    /**
     * Switch to the main db connection. This method keeps the copy db
     * connection open in background. To flush the copy db connection use
     * <code>disconnectFromCoyDB()</code>.
     *
     * @see disconnectFromCopyDB()
     * @return <code>true</code> if connection to main db is open.
     */
    public static boolean switchToMainDB() {
        boolean mainDBopen;
        if (!ConnectionManager.getInstance().isDBCopy()) {
            mainDBopen = ConnectionManager.getInstance().isConnectionOpen();
        } else {
            ConnectionManager mgr = ConnectionManager.connectionManagerMainDB;
            if (mgr != null) {
                ConnectionManager.connectionManager_ = mgr;
                mainDBopen = ConnectionManager.getInstance().isConnectionOpen();
            } else {
                mainDBopen = false;
            }
        }
        return mainDBopen;
    }

    /**
     * Save a clob to the database, using a string as input. So far only
     * implemented for oracle.
     *
     * @param tableName Name of the table to insert the clob into
     * @param fieldName Field for the clob
     * @param text Text to insert
     * @param tablePrefix Used to concatenate a string prefix_id
     * @param id integer id of the record we want to replace (insert not yet
     * supported)
     */
    public void set_clob_from_str(String tableName, String fieldName,
            String text, String tablePrefix, int id) {
        logger.log(Level.INFO, "Ready to insert clob of size {0} into field {1}", new Object[]{text.length(), fieldName}); //$NON-NLS-1$

        try {
            if (getInitInfo().getTechnology() == DBTechnology.ORACLE
                    && getInitInfo().getSchemaName().length() > 2) {
                tableName = getInitInfo().getSchemaName() + "." + tableName; //$NON-NLS-1$
            }

            String query = "UPDATE " + tableName + " SET " + fieldName //$NON-NLS-1$ //$NON-NLS-2$
                    + "=? WHERE " + tablePrefix + "ID=" + id; //$NON-NLS-1$ //$NON-NLS-2$

            query = fix_schema_name(query);
            logger.log(Level.INFO, "Query: {0}", query); //$NON-NLS-1$

            try (PreparedStatement pstmt = getConnection().prepareStatement(query)) {

                switch (getInitInfo().getTechnology()) {
                    case ORACLE:
                        oracle.sql.CLOB newClob = oracle.sql.CLOB.createTemporary(connection_, false, oracle.sql.CLOB.DURATION_SESSION);
                        newClob.setString(1, text);
                        pstmt.setClob(1, newClob);
                        break;
                    case UNKNOWN:
                        logger.log(Level.SEVERE, "Unknown database technnology");
                        throw new RuntimeException("Unknown database technnology");
                }

                pstmt.executeUpdate();
                pstmt.close();
            }
            logger.info("Clob insert ok");

        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "CLOB SQL ERROR! ", ex);
        }

    }

    /**
     * Save a clob to the database
     *
     * @param tableName Name of the table to insert the clob into
     * @param fieldName Field for the clob
     * @param filename Text file to insert
     * @param tablePrefix Used to concatenate a string prefix_id
     * @param id integer id of the record we want to replace (insert not yet
     * supported)
     */
    public void set_clob(String tableName, String fieldName, String filename,
            String tablePrefix, int id) {
        logger.fine("Ready to insert clob"); //$NON-NLS-1$

        try {
            if (getInitInfo().getTechnology() == DBTechnology.ORACLE
                    && getInitInfo().getSchemaName().length() > 2) {
                tableName = getInitInfo().getSchemaName() + "." + tableName;
            }

            String query = "UPDATE " + tableName + " SET " + fieldName //$NON-NLS-1$ //$NON-NLS-2$
                    + "=? WHERE " + tablePrefix + "ID=" + id; //$NON-NLS-1$ //$NON-NLS-2$

            query = fix_schema_name(query);
            logger.log(Level.FINE, "Query: {0}", query); //$NON-NLS-1$
            PreparedStatement pstmt = getConnection().prepareStatement(query);

            logger.log(Level.FINE, "Opening {0}", filename); //$NON-NLS-1$

            File fFile = new File(filename);
            FileInputStream fis = new FileInputStream(fFile);

            if (getInitInfo().getTechnology() == DBTechnology.ORACLE) { //$NON-NLS-1$
                InputStreamReader irdr = new InputStreamReader(fis);
                BufferedReader brdr = new BufferedReader(irdr, 8192);

                String str;
                String line;
                int nlines = 0;
                StringBuilder buffer = new StringBuilder(1000);
                logger.fine("going to read file"); //$NON-NLS-1$
                try {
                    while ((line = brdr.readLine()) != null) {
                        nlines++;
                        buffer.append(line).append("\n"); //$NON-NLS-1$
                    }
                    logger.log(Level.FINE, "read lines:  {0}", nlines); //$NON-NLS-1$
                    brdr.close();
                } catch (IOException e) {
                    logger.warning("File reading failed"); //$NON-NLS-1$
                    logger.warning("Sometimes this is ok"); //$NON-NLS-1$
                }
                logger.fine("going to save clob:"); //$NON-NLS-1$
                str = buffer.toString();
                oracle.sql.CLOB newClob = oracle.sql.CLOB.createTemporary(connection_,
                        false, oracle.sql.CLOB.DURATION_CALL);

                newClob.setString(1, str);
                pstmt.setClob(1, newClob);
            } else {
                pstmt.setAsciiStream(1, fis, (int) fFile.length());
            }
            pstmt.executeUpdate();

            pstmt.close();
            logger.finer("Clob insert ok"); //$NON-NLS-1$
        } catch (FileNotFoundException ex) {
            logger.log(Level.SEVERE, "CLOB FILE ERROR ", ex);
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "CLOB SQL ERROR! ", ex);
        }
    }

    /**
     * Get a clob from the database
     *
     * @param tableName
     * @param tablePrefix
     * @param id
     * @param field
     * @return String representation of the clob
     */
    public String get_clob(String tableName, String tablePrefix, int id,
            String field) {
        String outstr = ""; //$NON-NLS-1$
        String query = "SELECT " + tablePrefix + field + " FROM " + tableName //$NON-NLS-1$ //$NON-NLS-2$
                + " WHERE " + tablePrefix + "ID=?"; //$NON-NLS-1$ //$NON-NLS-2$
        query = fix_schema_name(query);

        try (PreparedStatement ps = getConnection().prepareStatement(query)) {
            ps.setInt(1, id);
            try (ResultSet rset = executeSelect(ps)) {
                while (rset.next()) {

                    StringBuilder strOut = new StringBuilder(1000);
                    Clob out = rset.getClob(tablePrefix + field);
                    if (out != null) {

                        String aux;
                        BufferedReader br = new BufferedReader(out.getCharacterStream());

                        try {
                            while ((aux = br.readLine()) != null) {
                                strOut.append(aux).append("\n"); //$NON-NLS-1$
                            }
                        } catch (IOException ex) {
                            logger.log(Level.SEVERE, "problem converting clob to string", ex);
                        }
                        outstr = strOut.toString();
                    }

                }
                rset.close();
                ps.close();
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error reading clob " + field + "\n", ex);
        }

        return outstr;
    }

    /**
     * Get the log path.
     *
     * @return The log path.
     */
    public String get_log_path() {
        return log_path;
    }

    /**
     * This needs to be a singleton so we can manage the connection to the
     * database properly.
     *
     * @return Instance of the singleton.
     */
    public static synchronized ConnectionManager getInstance() {
        if (connectionManager_ == null) {
            connectionManager_ = new ConnectionManager(logger);
        }
        return connectionManager_;
    }

    /**
     *
     * Set the jdbc connection for tests NOT TO BE USED IN PRODUCTION CODE. FOR
     * TESTING ONLY.
     *
     * @param connection
     */
    public void setTestingConnection(Connection connection) {
        connection_ = connection;
    }

    public void unsetTestingConnection() {
        connection_ = null;
    }

    /**
     * Converts a java Boolean type to a string (as used in the database schema)
     *
     * @param used True or false
     * @return 0 or 1 as a string
     */
    private String format_bool(Boolean used) {
        String used_str = "0"; //$NON-NLS-1$
        if (used) {
            used_str = "1";
        } else {
        }
        return used_str;
    }

    /**
     *
     * @param tableName
     * @return
     * @throws SQLException
     */
    public int executeDeleteTable(final String tableName) throws SQLException {
        String fullTableName = fix_schema_name(tableName);
        logger.log(Level.INFO, "executing DELETE FROM {0}", fullTableName);
        String query = "DELETE FROM FAUCCI." + fullTableName;
        PreparedStatement stmt = this.getConnection().prepareStatement(query);
        int nUpdates;
        nUpdates = stmt.executeUpdate();
        stmt.close();
        return nUpdates;
    }

    /**
     * When doing a SQL statement that doesn't have a return for example
     * removing a record use this
     *
     * @param query The SQL code to be executed
     * @return
     * @throws SQLException
     */
    public int executeUpdateStatement(final String query) throws SQLException {
        String _query = fix_schema_name(query);
        logger.log(Level.INFO, "executing{0}", _query);
        int nUpdates = 0;
        try (PreparedStatement stmt = this.getConnection().prepareStatement(_query)) {
            try {
                nUpdates = stmt.executeUpdate();
            } catch (SQLException ex) {
                String exceptionMessage = "DB Error "; //$NON-NLS-1$
                exceptionMessage += ex.getMessage();
                exceptionMessage += "Query:\t" + _query; //$NON-NLS-1$
                throw new SQLException(exceptionMessage, ex.getSQLState());
            }

            try {
                stmt.executeUpdate("COMMIT");
            } catch (SQLException ex) {
                String exceptionMessage = "DB Error ";
                exceptionMessage += ex.getMessage();
                exceptionMessage += "Query:\t" + _query;
                throw new SQLException(exceptionMessage, ex.getSQLState());
            }
            stmt.close();
        }
        return nUpdates;
    }

    /**
     * @brief find maximum id of table
     * @param tableName
     * @param tablePrefix
     * @return max table id, 0 for empty tables
     * @throws SQLException
     */
    private int getMaxId(final String tableName, final String tablePrefix) throws SQLException {
        int id = 0;

        // this query returns 0 if the table is empty!
        String query = "SELECT MAX(" + tablePrefix + "ID) FROM " + tableName;
        query = fix_schema_name(query);

        Connection con = this.getConnection();
        try (PreparedStatement ps = con.prepareStatement(query); ResultSet rset = executeInsert(ps)) {
            while (rset.next()) {
                id = rset.getInt(1);
            }
            rset.close();
            ps.close();
        }

        return id;
    }

    /**
     *
     * @param tableName
     * @param tablePrefix
     * @param oldId
     * @param fields_values
     * @return
     * @throws SQLException
     */
    public int save(final String tableName, final String tablePrefix, final Integer oldId,
            TreeMap<String, Object> fields_values) throws SQLException {
        if (oldId > 0) {
            return oldId;
        }

        // versioning for comp depends on name and alias
        if (!tableName.equals("HLT_COMPONENT")) {
            if (fields_values.containsKey("VERSION")) {
                fields_values.put("VERSION", 1 + getMaxVersion(
                        tableName, tablePrefix, (String) fields_values.get("NAME")));
            }
        } else {
            if (fields_values.containsKey("VERSION")) {
                fields_values.put("VERSION", 1 + getMaxCompVersion(
                        (String) fields_values.get("NAME"),
                        (String) fields_values.get("ALIAS")));
            }
        }

        // the new ID under which to store the entry
        int newId = 1 + getMaxId(tableName, tablePrefix);

        //Start online/offline keys at 2000
        if (newId == 1 && tablePrefix.equals("SMT_")) {
            if (getInitInfo().getUserName().equalsIgnoreCase("atlas_trig_stelzer")) {
                newId = 100; // to test
            } else if (getInitInfo().getUserName().equalsIgnoreCase("atlas_conf_trigger_run2_w")) {
                newId = 2000; // new online db
            } else if (getInitInfo().getUserName().equalsIgnoreCase("ATLAS_CONF_TRIGGER_RUN2_MC_W")) {
                newId = 2000; // new mc db
            }
        }
        //Start online/offline BGS keys at 1000
        if (newId == 1 && tablePrefix.equals("L1BGS_")) {
            if (getInitInfo().getUserName().equalsIgnoreCase("martyniu")) {
                newId = 100; // to test
            } else if (getInitInfo().getUserName().equalsIgnoreCase("atlas_conf_trigger_run2_w")) {
                newId = 1000; // new online db
            } else if (getInitInfo().getUserName().equalsIgnoreCase("atlas_conf_trigger_mc_run2_w")) {
                newId = 1000; // new mc db
            }
        }

        logger.log(Level.FINE, "Saving {0} id {1} to table {2}", new Object[]{fields_values.get("NAME"), newId, tableName});

        // code to save a new record
        //Switch to separate tables with username/modified time from the rest
        final Set<String> extentedTables = new HashSet<>(Arrays.asList("SMT_", "L1MT_", "HMT_", "TW_", "PSA_", "L1BGS_"));
        boolean useUserAndModtime = extentedTables.contains(tablePrefix);

        StringBuilder fieldNames = new StringBuilder(1000); // Part 1 of the INSERT command
        StringBuilder fieldValues = new StringBuilder(1000); // Part 2 of the INSERT command

        fieldNames.append(tablePrefix).append("ID,");
        fieldValues.append("?,");

        if (useUserAndModtime) {
            DBTechnology technology = getInitInfo().getTechnology();
            fieldNames.append(tablePrefix).append("MODIFIED_TIME,")
                    .append(tablePrefix).append("USERNAME,");
            fieldValues.append("sysdate,?,");
        }

        for (String key : fields_values.keySet()) {
            if (key.equals("MODIFIED_TIME") || key.equals("USERNAME") || key.equals("ID")) {
                continue;
            }
            fieldNames.append(tablePrefix).append(key).append(",");
            fieldValues.append("?,");
        }

        // remove last ','
        fieldNames.deleteCharAt(fieldNames.length() - 1);
        fieldValues.deleteCharAt(fieldValues.length() - 1);

        // build the command
        StringBuilder insertCommand = new StringBuilder(1000);
        insertCommand.append("INSERT INTO ")
                .append(tableName)
                .append(" (")
                .append(fieldNames)
                .append(") VALUES (")
                .append(fieldValues)
                .append(")");

        //logger.info("Query: " +fieldNames);
        Connection con = this.getConnection();
        PreparedStatement ps = con.prepareStatement(fix_schema_name(insertCommand.toString()));

        int i = 1;
        ps.setInt(i++, newId);
        if (useUserAndModtime) {
            String user = getInitInfo().getSystemUsername();
            if (user.isEmpty()) {
                user = getInitInfo().getUserName();
            }
            ps.setString(i++, user);
        }

        for (String key : fields_values.keySet()) {

            if (key.equals("MODIFIED_TIME") || key.equals("USERNAME") || key.equals("ID")) {
                continue;
            }
            Object obj = fields_values.get(key);
            //System.out.println("KEY : " + key);
            if (obj instanceof String) {
                String str = (String) obj;
                if (str.isEmpty()) {
                    str = "~";
                }
                //System.out.println("STRING " + str);
                ps.setString(i, str);
            } else if (obj instanceof Integer) {
                ps.setInt(i, (Integer) obj);
            } else if (obj instanceof Long) {
                ps.setLong(i, (Long) obj);
            } else if (obj instanceof Boolean) {
                ps.setString(i, format_bool((Boolean) obj));
            } else if (obj instanceof Double) {
                ps.setDouble(i, (Double) obj);
            } else if (obj instanceof Float) {
                ps.setDouble(i, (Float) obj);
            } else {
                logger.log(Level.SEVERE, " Object class not recognised {0}", obj.getClass());
            }
            ++i;
        }

        try {
            ps.executeUpdate();
        } catch (SQLException ex) {
            String errMsg = "Caught SQL error: " + ex.getMessage();
            errMsg += "\nquery: " + fieldNames.toString() + "\n";
            logger.log(Level.SEVERE, errMsg, ex);
            printSaveStatement(tableName, tablePrefix, fields_values);
            throw ex;
        }
        ps.close();
        return newId;
    }

    /**
     *
     * @param tableName
     * @param tablePrefix
     * @param id
     * @param table
     * @return
     * @throws SQLException
     */
    public TreeMap<Integer, Integer> saveBatch(String tableName, String tablePrefix, Integer id,
            ArrayList<TreeMap<String, Object>> table) throws SQLException {

        //logger.config("Saving " + tableName + " id " + id);
        TreeMap<Integer, Integer> commitList = new TreeMap<>();

        if (id > 0) {
            commitList.put(0, id);
            return commitList;
        }

        if (table.isEmpty()) {
            commitList.put(0, 0);
            return commitList;
        }

        int batchResults[];

        String query = "SELECT MAX(" + tablePrefix + "ID) FROM " + tableName;
        query = fix_schema_name(query);

        //logger.finer("Query: " +query);
        Connection con = this.getConnection();
        PreparedStatement ps = con.prepareStatement(query);
        ResultSet rset = executeSelect(ps);

        while (rset.next()) {
            id = rset.getInt(1) + 1;
        }

        rset.close();
        ps.close();

        con.setAutoCommit(false);
        StringBuilder sb = new StringBuilder(1000); // Part 1 of the INSERT command
        StringBuffer sb2 = new StringBuffer(1000); // Part 2 of the INSERT command

        // code to save a new record
        //Switch to separate tables with username/used/modified time from the rest
        boolean junkEntries = false;

        if (tablePrefix.equals("SMT_") || tablePrefix.equals("L1MT_") || tablePrefix.equals("HMT_")
                || tablePrefix.equals("TW_") || tablePrefix.equals("PSA_")) {
            junkEntries = true;
        }

        sb.append("INSERT INTO "); //$NON-NLS-1$
        sb.append(tableName);
        if (junkEntries) {
            sb.append(" (").append(tablePrefix).append("ID,").append(tablePrefix).append("MODIFIED_TIME,").append(tablePrefix).append("USERNAME,");
        } else {
            sb.append(" (").append(tablePrefix).append("ID,");
        }

        for (String key : table.get(0).keySet()) {

            if (key.equals("MODIFIED_TIME") || key.equals("USERNAME")
                    || key.equals("ID")) {
                continue;
            }

            sb.append(tablePrefix);
            sb.append(key);
            sb.append(","); //$NON-NLS-1$
            sb2.append("?,"); //$NON-NLS-1$
        }

        // remove last ','
        sb.deleteCharAt(sb.length() - 1);
        sb2.deleteCharAt(sb2.length() - 1);

        sb.append(") VALUES ("); //$NON-NLS-1$
        //Switch to separate tables with username/used/modified time from the rest
        if (junkEntries) {
            DBTechnology technology = getInitInfo().getTechnology();
            sb.append("?,sysdate,?,"); //$NON-NLS-1$
        } else {
            sb.append("?,"); //$NON-NLS-1$
        }

        sb.append(sb2);
        sb.append(")"); //$NON-NLS-1$
        //logger.info(fieldNames.toString());
        //logger.info("Query: " +fieldNames);
        ps = con.prepareStatement(fix_schema_name(sb.toString()));

        int current = 0;
        for (TreeMap<String, Object> fields_values : table) {

            // versioning for comp depends on name and alias
            if (!tableName.equals("HLT_COMPONENT")) {
                if (fields_values.containsKey("VERSION")) {
                    fields_values.put("VERSION", 1 + getMaxVersion(
                            tableName, tablePrefix, (String) fields_values.get("NAME")));
                }
            } else {
                if (fields_values.containsKey("VERSION")) {
                    fields_values.put("VERSION", 1 + getMaxCompVersion(
                            (String) fields_values.get("NAME"),
                            (String) fields_values.get("ALIAS")));
                }
            }

            ps.setInt(1, id);
            ps.setString(2, getInitInfo().getSystemUsername());

            int i;
            if (junkEntries) {
                i = 3;
            } else {
                i = 2;
            }

            for (String key : fields_values.keySet()) {

                //logger.info("Key: " +i +" is called " +key +" out of " +fields_values.keySet().size());
                if (key.equals("MODIFIED_TIME") || key.equals("USERNAME") //$NON-NLS-1$ //$NON-NLS-2$
                        || key.equals("ID")) { //$NON-NLS-1$
                    continue;
                }

                Object obj = fields_values.get(key);
                if (obj instanceof String) {
                    String str = (String) obj;
                    if (str.isEmpty()) { //$NON-NLS-1$
                        str = "~"; //$NON-NLS-1$
                    }

                    ps.setString(i, str);
                } else if (obj instanceof Integer) {
                    ps.setInt(i, (Integer) obj);
                } else if (obj instanceof Long) {
                    ps.setLong(i, (Long) obj);
                } else if (obj instanceof Boolean) {
                    ps.setString(i, format_bool((Boolean) obj));
                } else if (obj instanceof Double) {
                    ps.setDouble(i, (Double) obj);
                } else if (obj instanceof Float) {
                    ps.setDouble(i, (Float) obj);
                } else {
                    logger.log(Level.SEVERE, " Object class not recognised {0}", obj.getClass());
                }

                ++i;
                //This needs re-thinking, currently hacked to output the id/input position
                switch (key) {
                    case "BUNCH_NUMBER":
                        commitList.put((Integer) obj, id);
                        break;
                    case "NAME":
                        commitList.put(current, id);
                        break;
                    case "ALGO_NAME":
                        commitList.put(current, id);
                        break;
                    case "GENERIC_ID":
                    case "INPUT_ID":
                    case "OUTPUT_ID":
                    case "PARAMETER_ID":
                        commitList.put(current, id);
                        break;
                }
            }
            try {
                ps.addBatch();
            } catch (SQLException ex) {
                String errMsg = "Caught SQL error: " + ex.getMessage();
                errMsg += "\nquery: " + sb.toString() + "\n";
                logger.log(Level.SEVERE, errMsg, ex);
                printSaveStatement(tableName, tablePrefix, fields_values);
                throw ex;
            }
            current++;
            id++; //Prepare ID for next row in table
        }
        try {
            batchResults = ps.executeBatch();
        } catch (SQLException ex) {
            String errMsg = "Caught SQL error: " + ex.getMessage();
            errMsg += "\nBatch Submission\n";
            logger.log(Level.SEVERE, errMsg, ex);
            printSaveStatement(tableName, tablePrefix, table.get(0));
            throw ex;
        }

        try {
            con.commit();
        } catch (SQLException ex) {
            String errMsg = "Caught SQL error: " + ex.getMessage();
            errMsg += "\nCommit Error\n";
            logger.log(Level.SEVERE, errMsg, ex);
            printSaveStatement(tableName, tablePrefix, table.get(0));
            throw ex;
        }

        ps.close();
        con.setAutoCommit(true);
        return commitList;
    }

    /**
     * Updates the given record on DB.
     *
     * @param tableName the table name.
     * @param tablePrefix the table prefix.
     * @param id the id of the record to update.
     * @param tableValues the TreeMap.
     * @throws SQLException
     */
    public void Update(final String tableName, final String tablePrefix,
            final Integer id, final TreeMap<String, Object> tableValues)
            throws SQLException {
        //
        String pre = tablePrefix;
        String tName = tableName;
        TreeMap<String, Object> treeMap = tableValues;

        // code to save a new record
        //Switch to separate tables with username/used/modified time from the rest
        boolean junkEntries = false;
        if (tablePrefix.equals("SMT_") || tablePrefix.equals("L1MT_") || tablePrefix.equals("HMT_")
                || tablePrefix.equals("TW_") || tablePrefix.equals("PSA_")) {
            junkEntries = true;
        }
        //
        String updateQuery = "UPDATE " + tName;
        updateQuery += " SET ";

        for (String key : treeMap.keySet()) {
            if (key.equals("MODIFIED_TIME")
                    || key.equals("USERNAME")
                    || key.equals("ID")) {
                continue;
            }
            updateQuery += "\n " + pre + key + "=?,";
        }
        ConnectionManager mgr = ConnectionManager.getInstance();

        // TIME STAMP
        if (mgr.getInitInfo().getTechnology() == DBTechnology.ORACLE && junkEntries) {
            updateQuery += "\n " + pre + "MODIFIED_TIME=sysdate, ";
        }
        // USER NAME
        if (junkEntries) {
            updateQuery += "\n " + pre + "USERNAME=? ";
        } else {
            // remove last ','
            updateQuery = updateQuery.substring(0, updateQuery.length() - 1);
        }
        updateQuery += "\n WHERE " + pre + "ID=?";
        updateQuery = mgr.fix_schema_name(updateQuery);

        try (PreparedStatement ps = mgr.getConnection().prepareStatement(updateQuery)) {
            int qIdx = 0;
            for (String key : treeMap.keySet()) {
                if (key.equals("MODIFIED_TIME")
                        || key.equals("USERNAME")
                        || key.equals("ID")) {
                    continue;
                }
                qIdx++;
                Object obj = treeMap.get(key);
                //logger.info(qIdx + "  Setting KEY: " + key + "   VALUE: " + obj);
                mgr.setSetter(treeMap.get(key), qIdx, ps);
            }
            if (junkEntries) {
                qIdx++;
                ps.setString(qIdx, mgr.getInitInfo().getSystemUsername());
            }
            qIdx++;
            ps.setInt(qIdx, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Problems updating ALIAS record: "
                    + e.getMessage(), e);
        }
    }

    /**
     *
     * @param obj
     * @param i
     * @param ps
     * @throws SQLException
     */
    public void setSetter(final Object obj, final int i, PreparedStatement ps)
            throws SQLException {
        if (obj instanceof String) {
            String str = (String) obj;
            if (str.isEmpty()) {
                str = "~";
            }
            ps.setString(i, str);
        } else if (obj instanceof Integer) {
            ps.setInt(i, (Integer) obj);
        } else if (obj instanceof Long) {
            ps.setLong(i, (Long) obj);
        } else if (obj instanceof Boolean) {
            ps.setString(i, format_bool((Boolean) obj));
        } else if (obj instanceof Double) {
            ps.setDouble(i, (Double) obj);
        } else if (obj instanceof Float) {
            ps.setDouble(i, (Float) obj);
        } else {
            logger.log(Level.SEVERE, " Object class not recogniced {0}", obj.getClass());
        }
    }

    /**
     *
     * @param tableName
     * @param tablePrefix
     * @param fields_values
     */
    public void printSaveStatement(final String tableName,
            final String tablePrefix,
            final TreeMap<String, Object> fields_values) {

        // versioning for comp depends on name and alias
        if (!tableName.equals("HLT_COMPONENT")) { //$NON-NLS-1$
            if (fields_values.containsKey("VERSION")) { //$NON-NLS-1$
                fields_values.put("VERSION", 1 + getMaxVersion( //$NON-NLS-1$
                        tableName, tablePrefix, (String) fields_values.get("NAME"))); //$NON-NLS-1$
            }
        } else {
            if (fields_values.containsKey("VERSION")) { //$NON-NLS-1$
                fields_values.put("VERSION", 1 + getMaxCompVersion( //$NON-NLS-1$
                        (String) fields_values.get("NAME"), //$NON-NLS-1$
                        (String) fields_values.get("ALIAS"))); //$NON-NLS-1$
            }
        }
        String msg = "Inserting into " + tableName;
        for (String key : fields_values.keySet()) {
            Object obj = fields_values.get(key);
            msg += key + ": " + obj.toString();
        }
        logger.severe(msg);
    }

    /**
     * @brief Get the version.
     *
     * When we do a copy we need to increment the version. This returns the max
     * version that exists in the database. Also used in the XML reading in to
     * database.
     *
     * @param tableName Name of the table we're searching.
     * @param tablePrefix Prefix for the table.
     * @param name The name of the row.
     *
     * @return the max version, if you're writing a new record add one to this
     */
    public int getMaxVersion(String tableName, String tablePrefix, String name) {
        StringBuilder query = new StringBuilder(1000);

        if (tableName.equals("HLTTriggerSignature") //$NON-NLS-1$
                || tableName.equals("L1_Random") //$NON-NLS-1$ 
                || tableName.equals("HLTRelease")) { //$NON-NLS-1$
            query.append("SELECT MAX("); //$NON-NLS-1$
            query.append(tableName);
            query.append(") FROM "); //$NON-NLS-1$
            query.append(tableName);
            query.append(" WHERE "); //$NON-NLS-1$
            query.append(tableName);
            query.append("=?"); //$NON-NLS-1$
        } else {
            query.append("SELECT MAX("); //$NON-NLS-1$
            query.append(tablePrefix);
            query.append("VERSION) FROM "); //$NON-NLS-1$
            query.append(tableName);
            query.append(" WHERE "); //$NON-NLS-1$
            query.append(tablePrefix);
            query.append("NAME=?"); //$NON-NLS-1$
        }

        int max = 0;
        try {
            String q = fix_schema_name(query.toString());
            try (PreparedStatement ps = getConnection().prepareStatement(q)) {
                ps.setString(1, name);
                try (ResultSet rset = executeSelect(ps)) {
                    while (rset.next()) {
                        max = rset.getInt(1);
                    }
                    rset.close();
                    ps.close();
                }
            }
        } catch (SQLException ex) {
            String errMsg = ex.getMessage();
            errMsg += "\n TABLE NAME: " + tableName;
            errMsg += "\n COL NAME: " + name;
            logger.log(Level.SEVERE, errMsg, ex);
        }

        //System.out.println("JOERG query for max version " + query + " with name " + name +" returns " + max);
        return max;
    }

    /**
     * Special adaptation of getMaxVersion for compt, which needs name and alias
     *
     * @param name
     * @param alias
     * @return
     */
    public int getMaxCompVersion(String name, String alias) {
        //logger.config("getMaxCompVersion");
        String query = "SELECT MAX(HCP_VERSION) FROM HLT_COMPONENT WHERE HCP_NAME=? AND HCP_ALIAS=?"; //$NON-NLS-1$

        int max = 0;
        try {
            query = fix_schema_name(query);
            try (PreparedStatement ps = getConnection().prepareStatement(query)) {
                ps.setString(1, name);
                ps.setString(2, alias);
                try (ResultSet rset = executeSelect(ps)) {
                    while (rset.next()) {
                        max = rset.getInt(1);
                    }
                    rset.close();
                    ps.close();
                }
            }
        } catch (SQLException e) {
            logger.severe(e.getMessage());
        }
        return max;
    }

    /**
     * For HLT XML upload. Need to link elements to a component Never USED!
     *
     * @param setupID id for the setup record
     * @param alias
     * @return id of the hlt component
     */
    public int findComponent(int setupID, String alias) {
        int hcp_id = returnValueForUnsupportedMET;

        StringBuilder buffer = new StringBuilder(1000);
        buffer.append("SELECT HCP_ID FROM HLT_COMPONENT, HLT_ST_TO_CP");
        buffer.append(" WHERE HLT_COMPONENT.HCP_ALIAS=\'?\'");
        buffer.append(" AND HLT_ST_TO_CP.HST2CP_COMPONENT_ID=HLT_COMPONENT.HCP_ID");
        buffer.append(" AND HLT_ST_TO_CP.HST2CP_SETUP_ID=?");

        String query = fix_schema_name(buffer.toString());

        try {
            logger.log(Level.FINER, "findcom {0}", query);
            try (PreparedStatement ps = getConnection().prepareStatement(query)) {
                ps.setString(1, alias);
                ps.setInt(1, setupID);
                try (ResultSet rset = executeSelect(ps)) {
                    while (rset.next()) {
                        hcp_id = rset.getInt("HCP_ID");
                    }
                    rset.close();
                    ps.close();
                }
            }
        } catch (SQLException e) {
            logger.severe(e.getMessage());
        }

        return hcp_id;
    }

    /**
     * Get the list of DBID's from a list of tables.
     *
     * @param tableList the list of table objects.
     * @return the list of DBID of these tables, one entry per id and sorted.
     */
    public static ArrayList<Integer> get_IDs(final ArrayList tableList) {
        ArrayList<Integer> ids = new ArrayList<>(tableList.size());
        for (Object o : tableList) {
            AbstractTable t = (AbstractTable) o;
            if (!ids.contains(t.get_id())) {
                ids.add(t.get_id());
            }
        }
        Collections.sort(ids);
        return ids;
    }

    /**
     *
     * @param table_name
     * @param table_prefix
     * @param fields_values
     * @param order
     * @param ignore
     * @return
     * @throws SQLException
     */
    public ArrayList<Integer> get_IDs(String table_name, String table_prefix,
            TreeMap<String, Object> fields_values, String order, String ignore)
            throws SQLException {
        ArrayList<String> ignoreList = new ArrayList<>();
        ignoreList.add(ignore);
        return get_IDs(table_name, table_prefix, fields_values, order,
                ignoreList);
    }

    /**
     * Get the rows from the database that have columns matching those in
     * fields_values. Note you can pass keyValues for a class and specify those
     * to be ignore. By default USERNAME, MODIFIED_TIME, USED and VERSION are
     * ignore by the seach. ID is not, since you might not always want to!
     *
     * @param table_name Database name of table.
     * @param table_prefix Database prefix including underscore, SMT_
     * @param fields_values Criteria to match in search.
     * @param order NULL?
     * @param ignoreList Vector of fields to ignore in the match.
     * @return Vector of all the matching IDs in ascending order.
     * @throws java.sql.SQLException
     */
    public ArrayList<Integer> get_IDs(String table_name, String table_prefix,
            TreeMap<String, Object> fields_values, String order,
            ArrayList<String> ignoreList) throws SQLException {
        if (ignoreList == null || ignoreList.isEmpty()) {
            ignoreList = new ArrayList<>();
        }

        ignoreList.add("USERNAME");
        ignoreList.add("MODIFIED_TIME");
        ignoreList.add("VERSION");
        ignoreList.add("USED");
        ignoreList.add("HASH");

        ArrayList<Integer> ids_vec = new ArrayList<>();
        String prefix = this.getInitInfo().getSchemaName().toUpperCase() + ".";
        StringBuilder sb = new StringBuilder(1000);
        sb.append("SELECT ");
        sb.append(table_prefix);
        sb.append("ID FROM ");
        sb.append(prefix);
        sb.append(table_name);
        sb.append(" WHERE ");

        int i = 1;
        StringBuilder sb3 = new StringBuilder(1000);

        try {
            for (String key : fields_values.keySet()) {
                boolean skipField = false;
                for (String ignore : ignoreList) {
                    if (key.equals(ignore)) {
                        skipField = true;
                    }
                }

                if (skipField) {
                    continue;
                }
                sb.append(table_prefix);
                sb.append(key);
                sb.append("=? AND ");
            }

            sb.delete(sb.length() - 5, sb.length());
            sb.append(" ORDER BY ").append(table_prefix).append("ID ASC");

            String q = sb.toString();
            try (PreparedStatement ps = getConnection().prepareStatement(q)) {
                for (String key : fields_values.keySet()) {
                    boolean skipField = false;
                    for (String ignore : ignoreList) {
                        if (key.equals(ignore)) {
                            skipField = true;
                        }
                    }

                    if (skipField) {
                        continue;
                    }

                    Object obj = fields_values.get(key);
                    if (obj instanceof String) {
                        String str = (String) obj;
                        if (str.isEmpty()) { //$NON-NLS-1$
                            str = "~"; //$NON-NLS-1$
                        }
                        ps.setString(i, str);
                        sb3.append(str);
                        sb3.append(" "); //$NON-NLS-1$
                    } else if (obj instanceof Integer) {
                        ps.setInt(i, (Integer) obj);
                        sb3.append((Integer) obj);
                        sb3.append(" "); //$NON-NLS-1$
                    } else if (obj instanceof Long) {
                        ps.setLong(i, (Long) obj);
                        sb3.append((Long) obj);
                        sb3.append(" "); //$NON-NLS-1$
                    } else if (obj instanceof Boolean) {
                        ps.setString(i, format_bool((Boolean) obj));
                        sb3.append(format_bool((Boolean) obj));
                        sb3.append(" "); //$NON-NLS-1$
                    } else if (obj instanceof Double) {
                        ps.setDouble(i, (Double) obj);
                        sb3.append((Double) obj);
                        sb3.append(" "); //$NON-NLS-1$
                    } else if (obj instanceof Float) {
                        ps.setDouble(i, (Float) obj);
                        sb3.append((Float) obj);
                        sb3.append(" "); //$NON-NLS-1$
                    } else {
                        logger.log(Level.SEVERE, " Object not recogniced. {0}", obj.getClass()); //$NON-NLS-1$
                    }

                    ++i;
                }

                try (ResultSet rset = executeSelect(ps)) {
                    while (rset.next()) {
                        ids_vec.add(rset.getInt(1));
                    }
                    rset.close();
                    ps.close();
                }
            }

        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "SQL error with following query {0}", ex.getMessage()); //$NON-NLS-1$
            logger.log(Level.SEVERE, "query:  {0}", sb.toString()); //$NON-NLS-1$
            logger.log(Level.SEVERE, "warning: {0}", sb3.toString()); //$NON-NLS-1$
            throw ex;
        }
        return ids_vec;
    }

    /**
     * Get the rows from the database that have columns matching those in
     * fields_values. Note you can pass keyValues for a class and specify those
     * to be ignore. By default USERNAME, MODIFIED_TIME, USED and VERSION are
     * ignore by the search. ID is not, since you might not always want to!
     *
     * @param table_name Database name of table.
     * @param table_prefix Database prefix including underscore, SMT_
     * @param hash
     * @param full
     * @return Vector of all the matching IDs in ascending order.
     * @throws java.sql.SQLException
     */
    public ArrayList<Integer> get_hashIDs(String table_name, String table_prefix,
            Integer hash, boolean full) throws SQLException {

        ArrayList<Integer> ids_vec = new ArrayList<>();
        String prefix = this.getInitInfo().getSchemaName().toUpperCase() + ".";
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(table_prefix);
        sb.append("ID FROM ");
        sb.append(prefix);
        sb.append(table_name);
        sb.append(" WHERE ");

        sb.append(table_prefix);
        if (!full) {
            sb.append("HASH");
        } else {
            sb.append("HASHFULL");
        }
        sb.append("=?");
        sb.append(" ORDER BY ").append(table_prefix).append("ID ASC");

        //String q = fix_schema_name(fieldNames.toString());
        String q = sb.toString();
        try (PreparedStatement ps = getConnection().prepareStatement(q)) {
            ps.setInt(1, hash);
            try (ResultSet rset = executeSelect(ps)) {
                while (rset.next()) {
                    ids_vec.add(rset.getInt(1));
                }
                rset.close();
                ps.close();
            }
        }
        return ids_vec;
    }

    // /Get all ID's from the table.
    /**
     * Note you can pass keyValues for a class and specify those to be ignore.
     * By default USERNAME, MODIFIED_TIME, USED and VERSION are ignore by the
     * search. ID is not, since you might not always want to!
     *
     * @param table_name Database name of table.
     * @param table_prefix Database prefix including underscore, SMT_
     * @return Vector of all the matching IDs in ascending order.
     * @throws java.sql.SQLException
     */
    public ArrayList<Integer> get_allIDs(String table_name, String table_prefix)
            throws SQLException {
        String query = "SELECT "; //$NON-NLS-1$
        query += table_prefix + "ID "; //$NON-NLS-1$
        query += "FROM " + table_name; //$NON-NLS-1$
        query = fix_schema_name(query);

        ArrayList<Integer> ids_vec = new ArrayList<>();
        try (PreparedStatement s = getConnection().prepareStatement(query); ResultSet rset = s.executeQuery()) {
            while (rset.next()) {
                ids_vec.add(rset.getInt(1));
            }
            rset.close();
            s.close();
        }

        return ids_vec;
    }

    /**
     * For HLT XML upload. Need to link elements to a component Never USED!!
     *
     * @param setupID id for the setup record
     * @param alias
     * @param name
     * @return id of the hlt component
     */
    public int findComponent(int setupID, String alias, String name) {
        int hcp_id = returnValueForUnsupportedMET;

        String query = "SELECT HCP_ID FROM HLT_COMPONENT, HLT_ST_TO_CP "; //$NON-NLS-1$
        query += "WHERE HLT_COMPONENT.HCP_ALIAS=? "; //$NON-NLS-1$
        if (name != null) {
            query += " AND HLT_ST_TO_CP.HST2CP_COMPONENT_ID=HLT_COMPONENT.HCP_ID AND HLT_ST_TO_CP.HST2CP_SETUP_ID=?"; //$NON-NLS-1$
        }

        try {
            logger.log(Level.FINER, "findcom {0}", query); //$NON-NLS-1$
            try (PreparedStatement ps = getConnection().prepareStatement(query)) {
                ps.setString(1, alias);
                if (name != null) {
                    ps.setInt(2, setupID);
                }
                try (ResultSet rset = executeSelect(ps)) {
                    while (rset.next()) {
                        hcp_id = rset.getInt("HCP_ID");
                    }
                    rset.close();
                    ps.close();
                }
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        }
        logger.log(Level.FINER, "-------------- found comp {0}", hcp_id); //$NON-NLS-1$
        return hcp_id;
    }

    /**
     * For HLT XML upload. Need to link elements to a component Never Used!!
     *
     * @param setupID id for the setup record
     * @param aliases Aliases of the HLT_COMPONENT
     * @param names Names of the HLT_COMPONENT
     * @return id of the hlt component
     * @throws java.sql.SQLException
     */
    public ArrayList<Integer> findComponents(int setupID, ArrayList<String> aliases,
            ArrayList<String> names) throws SQLException {
        ArrayList<Integer> hcp_ids = new ArrayList<>();

        int nAliases = aliases.size();
        int nNames = names.size();
        int nMaxNA = (nNames > nAliases) ? nNames : nAliases;
        // Test the input variables
        // Mirek: make simpler condition
        if (!(nNames == nAliases || nNames == 0 || nAliases == 0)) {
            return hcp_ids;
        }
        // Go through components one by one to have the name and aliases id's
        // sorted
        for (int iComponent = 0; iComponent < nMaxNA; iComponent++) {
            String query = "SELECT DISTINCT HLT_COMPONENT.HCP_ID FROM HLT_SETUP "; //$NON-NLS-1$
            query += "JOIN HLT_ST_TO_CP ON (HLT_ST_TO_CP.HST2CP_SETUP_ID = HLT_SETUP.HST_ID) "; //$NON-NLS-1$
            query += "JOIN HLT_COMPONENT ON (HLT_ST_TO_CP.HST2CP_COMPONENT_ID = HLT_COMPONENT.HCP_ID) "; //$NON-NLS-1$
            query += "WHERE HLT_SETUP.HST_ID=? "; //$NON-NLS-1$
            query += " AND "; //$NON-NLS-1$

            if (iComponent < nAliases) {
                query += " HLT_COMPONENT.HCP_ALIAS=? "; //$NON-NLS-1$
            }

            if (iComponent < nAliases && iComponent < nNames) {
                query += " AND "; //$NON-NLS-1$
            }
            if (iComponent < nNames) {
                query += " HLT_COMPONENT.HCP_NAME=? "; //$NON-NLS-1$
            }

            int hcp_id = returnValueForUnsupportedMET;
            int nhcp_ids;

            logger.log(Level.FINER, "findComponents {0}", query); //$NON-NLS-1$
            query = fix_schema_name(query);
            try (PreparedStatement ps = getConnection().prepareStatement(query)) {
                ps.setInt(1, setupID);
                int count = 2;

                if (iComponent < nAliases) {
                    ps.setString(count, (String) aliases.get(iComponent));
                    ++count;
                }

                if (iComponent < nNames) {
                    ps.setString(count, (String) names.get(iComponent));
                }

                try (ResultSet rset = executeSelect(ps)) {
                    nhcp_ids = 0;
                    while (rset.next()) {
                        hcp_id = rset.getInt(1);
                        nhcp_ids++;
                    }
                    rset.close();
                    ps.close();
                }
            }

            if (nhcp_ids != 1) {
                if (nhcp_ids == 0) {
                    // No Component found
                    logger.finer("No Matching Component found"); //$NON-NLS-1$
                    hcp_ids.add(returnValueForUnsupportedMET);
                } else {
                    // Multiple Components found - that should not happen
                    logger.log(Level.FINER, "Multiple Components found:\t{0}", nhcp_ids); //$NON-NLS-1$
                    hcp_ids.add(returnValueForUnsupportedPrescaleAlias);
                }
            } else {
                // everything OK
                hcp_ids.add(hcp_id);
            }
        }

        if (hcp_ids.size() != nMaxNA) {
            logger.log(Level.FINER, "Output not matching the input:\t" + "Size of Input: {0}\tSize of Output: {1}", new Object[]{nMaxNA, hcp_ids.size()}); //$NON-NLS-1$
        }

        return hcp_ids;
    }

    /**
     *
     * @param table_name
     * @param id
     * @param where_term
     * @param element1
     * @param element2
     * @return
     */
    public TreeMap<Integer, Integer> getConstrainedItemList(String element1, String element2, String table_name, String where_term, int id) {
        String query = "Select " + element1 + "," + element2 + " from " + table_name + " where " + where_term + " = ?";
        query = ConnectionManager.getInstance().fix_schema_name(query);

        TreeMap<Integer, Integer> results = new TreeMap<>();
        try {
            try (PreparedStatement ps = getConnection().prepareStatement(query)) {
                ps.setInt(1, id);
                try (ResultSet rset = executeSelect(ps)) {
                    while (rset.next()) {
                        results.put(rset.getInt(element1), rset.getInt(element2));
                    }
                    rset.close();
                    ps.close();
                }
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);

        }

        return results;
    }

    /**
     * Get all Items used
     *
     * @param table_name
     * @param table_prefix
     * @param base_name
     * @param reference_name
     * @param baseID
     * @return Vector of Item's ID's
     */
    public ArrayList<Integer> get_ItemList(String table_name, String table_prefix,
            String base_name, String reference_name, int baseID) {
        // Don't use distinct there can be multiple relations

        Connection connection = this.connection_;

        StringBuilder query = new StringBuilder(1000);
        query.append("SELECT "); //$NON-NLS-1$
        query.append(reference_name);
        query.append("_ID FROM "); //$NON-NLS-1$
        query.append(table_name);
        query.append(" WHERE "); //$NON-NLS-1$
        query.append(base_name);
        query.append("_ID =?"); //$NON-NLS-1$
        query.append(" ORDER BY "); //$NON-NLS-1$
        query.append(reference_name).append("_ID ASC"); //$NON-NLS-1$

        ArrayList<Integer> ids_vec = new ArrayList<>();

        String query2 = fix_schema_name(query.toString());
        //logger.config(query2 + "\n Values BaseId=" + baseID);
        try (PreparedStatement ps = connection.prepareStatement(query2)) {
            ps.setInt(1, baseID);
            try (ResultSet rset = executeSelect(ps)) {
                while (rset.next()) {
                    ids_vec.add(rset.getInt(1));
                }
                rset.close();
                ps.close();
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return ids_vec;
    }

    /**
     * Get all Items used
     *
     * @param table_name
     * @param table_prefix
     * @param base_name
     * @param reference_name
     * @param BaseIds
     * @return Vector of Item's ID's
     */
    public ArrayList<int[]> get_CompactItemList(String table_name, String table_prefix,
            String base_name, String reference_name, ArrayList<Integer> BaseIds) {
        // Don't use distinct there can be multiple relations

        Connection connection = this.connection_;

        StringBuilder query = new StringBuilder();

        int count = 1;
        for (Integer i : BaseIds) {
            query.append("SELECT "); //$NON-NLS-1$
            query.append(reference_name);
            query.append("_ID, ");
            query.append("\'").append(i).append("\' AS INT");
            query.append(" FROM "); //$NON-NLS-1$
            query.append(table_name);
            query.append(" WHERE "); //$NON-NLS-1$
            query.append(base_name);
            query.append("_ID = ").append(i); //$NON-NLS-1$
            if (count != BaseIds.size()) {
                query.append(" UNION ALL ");
            }
            count++;
        }
        ArrayList<int[]> holder = new ArrayList<>();

        String queryString = fix_schema_name(query.toString());
        try {
            PreparedStatement ps = connection.prepareStatement(queryString);
            ResultSet rset = executeSelect(ps);

            while (rset.next()) {
                int[] entry = new int[2];
                entry[0] = rset.getInt(2);
                entry[1] = rset.getInt(1);
                holder.add(entry);
            }

            rset.close();
            ps.close();
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return holder;
    }

    /**
     *
     * @param message
     */
    public void dblog(String message) {
        String values;
        DBTechnology technology = getInitInfo().getTechnology();
        values = "?,?,sysdate"; //$NON-NLS-1$

        String query = "INSERT INTO TRIGGER_LOG (TLOG_USERNAME, TLOG_MESSAGE, TLOG_MODIFIED_TIME) VALUES (" //$NON-NLS-1$
                + values + ")"; //$NON-NLS-1$
        query = fix_schema_name(query);
        try (PreparedStatement ps = getConnection().prepareStatement(query)) {
            ps.setString(1, getInitInfo().getSystemUsername());
            ps.setString(2, message);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    /**
     * Here we look into a connection table for an existing mapping for
     * instance: If there is a trigger_menu with exactly the chains (a,b,c) and
     * we want to add a trigger_menu with exactly those three chains (meaning
     * exactly the same chain_IDs), then we don't have to add a new trigger_menu
     * Since in the 'connection tables' the mapping (e.g.
     *
     * @param id_vec
     * @param counters_vec
     * @param id2_vec
     * @return Vector of matching ID's of the base name, Remember there can be
     * more things referring to same Items with different own parameters
     * @throws java.sql.SQLException
     */
    public ArrayList<Integer> matchingConnectionSignatureIDs(
            ArrayList<Integer> id_vec, ArrayList<Integer> counters_vec,
            ArrayList<Integer> id2_vec) throws SQLException {

        Collections.sort(id2_vec);

        String table_name = "HLT_TS_TO_TE"; //$NON-NLS-1$
        String base_name = "HTS2TE_TRIGGER_SIGNATURE"; //$NON-NLS-1$
        String reference_name = "HTS2TE_TRIGGER_ELEMENT"; //$NON-NLS-1$

        ArrayList<Integer> baseIDs = new ArrayList<>(); // Output vector with
        // base ID with
        // matching
        // references

        if (id_vec == null || id_vec.isEmpty()) {
            return baseIDs;
        }
        ArrayList<Integer> idV = new ArrayList<>();
        ArrayList<Integer> ecV = new ArrayList<>();

        Integer ec = 0;
        for (Integer id : id_vec) {
            idV.add(id);
            ecV.add(ec++);
        }
        for (Integer id : id2_vec) {
            idV.add(id);
            ecV.add(returnValueForUnsupportedMET);
        }

        StringBuffer buffer = new StringBuffer(1000);
        buffer.append("SELECT DISTINCT HTS2TE_TRIGGER_SIGNATURE_ID FROM HLT_TS_TO_TE WHERE "); //$NON-NLS-1$
        Integer nRef = idV.size();
        for (int i = 0; i < nRef; i++) {
            buffer.append("(HTS2TE_TRIGGER_ELEMENT_ID=? AND HTS2TE_ELEMENT_COUNTER=?)"); //$NON-NLS-1$
            if (i < nRef - 1) {
                buffer.append(" OR "); //$NON-NLS-1$
            }
        }

        String query = fix_schema_name(buffer.toString());

        logger.log(Level.FINER, "MCL: {0}", query); //$NON-NLS-1$

        ArrayList<Integer> base_ids = new ArrayList<>();
        try {
            try (PreparedStatement ps = getConnection().prepareStatement(query)) {
                for (int i = 0; i < nRef; i++) {
                    ps.setInt(2 * i + 1, idV.get(i));
                    ps.setInt(2 * i + 2, ecV.get(i));
                }
                try (ResultSet rset = executeSelect(ps)) {
                    while (rset.next()) {
                        base_ids.add(rset.getInt(1));
                    }
                    rset.close();
                    ps.close();
                }
            }
        } catch (SQLException ex) {
            //logger.warning("Error finding base ID's"); //$NON-NLS-1$
            logger.log(Level.SEVERE, "Error finding base ID's", ex);
        }

        int nbase_ids = base_ids.size();
        if (nbase_ids == 0) {
            // Base ID's is still empty and there was no result searching
            // reference ID's
            return baseIDs;
        }
        // Now loop over found base ID's and compare found references with the
        // sorted input sorted_ref_vec
        Collections.sort(base_ids);

        // Do not use DISTINCT we need all the ID some of them can be doubled
        buffer = new StringBuffer(300);
        buffer.append("SELECT HTS2TE_TRIGGER_ELEMENT_ID, HTS2TE_ELEMENT_COUNTER FROM "); //$NON-NLS-1$
        buffer.append(table_name);
        buffer.append(" WHERE "); //$NON-NLS-1$
        buffer.append(base_name);
        buffer.append("_ID=?"); //$NON-NLS-1$
        // buffer.append(base_ids.get(ibase_id));
        buffer.append(" ORDER BY HTS2TE_ELEMENT_COUNTER, HTS2TE_TRIGGER_ELEMENT_ID"); //$NON-NLS-1$

        query = fix_schema_name(buffer.toString());

        logger.log(Level.FINER, "   MCL 2: {0}", query); //$NON-NLS-1$        
        try (PreparedStatement ps = getConnection().prepareStatement(query)) {
            for (int ibase_id = 0; ibase_id < nbase_ids; ibase_id++) {

                ArrayList<Integer> refCounters = new ArrayList<>();
                ArrayList<Integer> refIDs = new ArrayList<>();
                ArrayList<Integer> refIDsNoEC = new ArrayList<>();
                // Address the database

                ps.setInt(1, base_ids.get(ibase_id));
                try (ResultSet rset = executeSelect(ps)) {
                    while (rset.next()) {
                        if (rset.getInt("HTS2TE_ELEMENT_COUNTER") < 0 || rset.getInt("HTS2TE_ELEMENT_COUNTER") > 99) {
                            refIDsNoEC.add(rset.getInt("HTS2TE_TRIGGER_ELEMENT_ID")); // the floating TE's

                        } else {
                            refIDs.add(rset.getInt("HTS2TE_TRIGGER_ELEMENT_ID")); // the TE's that are listed
                            // in the signature
                            refCounters.add(rset.getInt("HTS2TE_ELEMENT_COUNTER"));
                        }
                    }
                    rset.close();

                    // In case of matching references add base id into the output
                    // list
                    // if (refIDs.equals(id_vec) && refIDsNoEC.equals(id2_vec)) {
                    if (refIDs.equals(id_vec) && refIDsNoEC.equals(id2_vec)
                            && refCounters.equals(counters_vec)) {
                        baseIDs.add(base_ids.get(ibase_id));
                    }
                }
      
            }
            ps.close();
        } catch (SQLException ex) {
            logger.log(Level.WARNING, ex.getMessage(), ex);
        }

        return baseIDs;
    }

    /**
     * Here we look into a connection table for an existing mapping for
     * instance: If there is a trigger_menu with exactly the chains (a,b,c) and
     * we want to add a trigger_menu with exactly those three chains (meaning
     * exactly the same chain_IDs), then we don't have to add a new trigger_menu
     * Since in the 'connection tables' the mapping (e.g.
     *
     * @param componentIDs Vector of component IDs sorted accordingly to the
     * algorithm counter
     * @return Vector of matching ID's of the base name, Remember there can be
     * more things referring to same Items with different own parameters
     */
    public ArrayList<Integer> matchingElementIDs(ArrayList<Integer> componentIDs) {

        String table_name = "HLT_TE_TO_CP"; //$NON-NLS-1$
        String base_name = "HTE2CP_TRIGGER_ELEMENT"; //$NON-NLS-1$
        String reference_name = "HTE2CP_COMPONENT"; //$NON-NLS-1$

        ArrayList<Integer> baseIDs = new ArrayList<>(); // Output vector with
        // base ID with
        // matching
        // references

        if (componentIDs == null || componentIDs.isEmpty()) {
            return baseIDs;
        }

        StringBuilder queryList = new StringBuilder(1000);

        int nReferences = componentIDs.size();

        for (int i = 0; i < nReferences; i++) {
            queryList.append("(HTE2CP_COMPONENT_ID=").append(componentIDs.get(i)).append(" AND HTE2CP_ALGORITHM_COUNTER=").append(i).append(")"); //$NON-NLS-1$ //$NON-NLS-2$
            if (i < nReferences - 1) {
                queryList.append(" OR "); //$NON-NLS-1$
            }
        }

        String query = "SELECT DISTINCT " + base_name + "_ID FROM " //$NON-NLS-1$ //$NON-NLS-2$
                + table_name + " WHERE " + queryList.toString(); //$NON-NLS-1$

        ArrayList<Integer> base_ids = new ArrayList<>();

        try {
            try (Statement stmt2 = getConnection().createStatement(); ResultSet rset = stmt2.executeQuery(query)) {

                while (rset.next()) {
                    base_ids.add(rset.getInt(1));
                }
                rset.close();
                stmt2.close();
            }
        } catch (SQLException ex) {
            //logger.finer("Error finding base ID's\n" + logException(e));
            logger.log(Level.FINER, "Error finding base ID's", ex);
        }

        int nbase_ids = base_ids.size();
        if (nbase_ids <= 0) {
            // Base ID's is still empty and there was no result searching
            // reference ID's
            return baseIDs;
        }
        Collections.sort(base_ids);
        // Do not use DISTINCT we need all the ID some of them can be doubled
        query = "SELECT " + reference_name + "_ID FROM " + table_name; //$NON-NLS-1$ //$NON-NLS-2$
        query += " WHERE " + base_name //$NON-NLS-1$
                + "_ID=? ORDER BY HTE2CP_ALGORITHM_COUNTER"; //$NON-NLS-1$
        try (PreparedStatement ps = getConnection().prepareStatement(query)) {
            // Now loop over found base ID's and compare found references
            for (int ibase_id = 0; ibase_id < nbase_ids; ibase_id++) {

                // Address the database
                ps.setInt(1, base_ids.get(ibase_id));
                try (ResultSet rset = executeSelect(ps)) {
                    ArrayList<Integer> refIDs = new ArrayList<>();
                    while (rset.next()) {
                        refIDs.add(rset.getInt(1));
                    }
                    rset.close();
                    // In case of matching references add base id into the output
                    // list
                    if (refIDs.equals(componentIDs)) {
                        baseIDs.add(base_ids.get(ibase_id));
                    }
                }
            }
            ps.close();
        } catch (SQLException ex) {
            //logger.finer("Error finding children\n" + logException(e));
            logger.log(Level.FINER, "Error finding children", ex);
        }

        return baseIDs;
    }

    /**
     * Add the schema name in front of the table name in for ORACLE connections.
     * Fix query for the standard connection.
     *
     * @param query the original SQL query.
     * @return the query with fixed schema name.
     */
    public String fix_schema_name(final String query) {

        if (this.getInitInfo().getSchemaName().length() == 0) {
            return query;
        }

        final String[] tableNames = TableNames.allTableName;

        String prefix = this.getInitInfo().getSchemaName().toUpperCase() + ".";

        String fixedQuery = query;
        for (int i = 0; i < tableNames.length; ++i) {
            fixedQuery = fixedQuery.replace(" " + tableNames[i], " " + prefix
                    + tableNames[i]);
        }

        return fixedQuery;
    }

    /**
     *
     * @param connection
     * @return
     */
    public int loadSchemaVersion(Connection connection) {

        int schema_version = 0;

        String schemaPrefix = this.getInitInfo().getSchemaName().toUpperCase() + ".";
        String run3query = "SELECT TS_TAG FROM " + schemaPrefix + "TRIGGER_SCHEMA";
        try (PreparedStatement ps = connection.prepareStatement(run3query)) {
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                schema_version = 1;//actually returns string so should compare to Trigger-Run3-Schema-v
                return schema_version;
            }
        } catch (SQLException ex) {
            String msg = "Will proceed assuming Run2 DB as caught error during Run3 schema version check: "
                    + "query was '" + run3query + "' gave error: "+ ex.getMessage() ;
            logger.warning(msg);
        }

        String query = "SELECT MAX(TS_ID) FROM TRIGGER_SCHEMA";

        query = fix_schema_name(query);

        try {
            try (PreparedStatement ps = connection.prepareStatement(query);
                    ResultSet rset = executeSelect(ps)) {
                while (rset.next()) {
                    schema_version = rset.getInt(1);
                }
                rset.close();
                ps.close();
            }

        } catch (SQLException ex) {
            String msg = "Caught error during schema version check: " + ex.getMessage() + "\n"
                    + "query was '" + query + "'";

            if (!savedInitInfo.IsCommandLine()) {
                JOptionPane.showMessageDialog(null, msg, "Error", JOptionPane.ERROR_MESSAGE);
            }
            logger.severe(msg);
            System.exit(1);
        }

        return schema_version;
    }

    /**
     * Compatibility test between db version & trigger tool version
     *
     * @return - 1 for compatible, 0 for complete incompatibility, <0 for
     * warning: -1 = MET significance triggers not supported
     */
    public int checkDBVersionCompatible() {
        // first time, read schema version from database
        if (db_schema_version == 0) {
            db_schema_version = loadSchemaVersion(getConnection());
        }

        // the test
        return isDBVersionCompatible(db_schema_version, "");
    }
    
    public void setDBSchemaversion(Integer input){
        savedInitInfo.setDbMinimunSchema(input);
        db_schema_version = input;
    }

    /**
     * Compatibility test between db version & trigger tool version.
     *
     * @param dbversion - version of the db we are connected to
     * @param dbdesc
     * @return - 1 for compatible, 0 for complete incompatibility, <0 for
     * warning: -1 = MET significance triggers not supported
     */
    public int isDBVersionCompatible(int dbversion, String dbdesc) {
        boolean isCompatible = (dbversion >= savedInitInfo.getDbMinimunSchema());

        if (!isCompatible) {
            String msg = dbdesc + "TriggerDB schema has version " + db_schema_version
                    + ". TriggerTool works only for version >= " + savedInitInfo.getDbMinimunSchema();
            JOptionPane.showMessageDialog(null, msg, "Error", JOptionPane.ERROR_MESSAGE);
            logger.severe(msg);
            return 0;
        }

        logger.log(Level.INFO, "TriggerDB version ({0}) combatibility with TT: {1}", new Object[]{db_schema_version, isCompatible});

        //if (dbversion >= triggerdb.Entities.L1.L1CaloInfo.DBVER_WITH_XS) {
//            logger.info("TriggerDB supports L1 MET significance parameters");
//            if (dbversion >= PrescaleSetAlias.TRIGGER_DB_ALIAS_VER) {
//                logger.info("TriggerDB supports Prescale Set Alias");
//            }// else {
//                logger.warning("TriggerDB does not support Prescale Set Alias");
//                return TriggerTool.TRIGDB_NO_ALIAS_SUPPORT;
//            }
//        } else {
//            logger.warning("TriggerDB does not support L1 MET significance parameters");
//            return returnValueForUnsupportedMET;
//        }
        return 1;
    }

    /**
     * Function to return the appropriate warning message id the Db version is
     * not compatible with the TT
     *
     * @param type - return value from isDBVersionCompatible
     * @return the warning msg
     */
    public String getDBVersionWarningMsg(int type) {

        switch (type) {
            case 1:
                return "";
            case 0:
                return "Trigger DB is incompatible with the TriggerTool";
            case returnValueForUnsupportedMET:
                return "TriggerDB does not support L1 MET significance parameters";
            case returnValueForUnsupportedPrescaleAlias:
                return "TriggerDB does not support Prescale Set Alias";
            default:
                return "Unknown warning - contact trigger tool authors";
        }
    }

    /**
     * Converts a java.sqlCLOB object into a String.
     *
     * @param clob the input CLOB object
     * @return the String representing exactly the clob.
     */
    public static String clobToString(final java.sql.Clob clob) {
        String clobStr = "";
        try {

            if (clob != null) {
                String aux;
                java.io.Reader clobReader = clob.getCharacterStream();
                BufferedReader br = new BufferedReader(clobReader);
                StringBuilder strOut = new StringBuilder();
                try {
                    while ((aux = br.readLine()) != null) {
                        strOut.append(aux).append("\n");
                    }
                } catch (IOException ex) {
//                    logger.warning("problem converting clob to string\n"
//                            + LoggerFormatter.logException(e));
                    logger.log(Level.WARNING, "problem converting clob to string", ex);
                }
                clobStr = strOut.toString();
            }
        } catch (SQLException e) {
            logger.severe(e.getMessage());
        }
        return clobStr;

    }

    /**
     *
     * @param Objects
     * @param Names
     * @param tablePrefix
     * @param tableName
     * @return
     */
    public ArrayList<AbstractMap.SimpleEntry<Integer, Integer>> checkTopoExisting(ArrayList<String> Objects, String Names, String tablePrefix, String tableName) {
//        TreeMap<Integer, Integer> out = new TreeMap<Integer, Integer>();
//        TreeMap<Integer, ArrayList<Integer>> test = new TreeMap();
//        test = checkTopoExistingMulti(Objects, Names, tablePrefix, tableName);
//        for(int key:test.keySet()){
//            ArrayList<Integer> grab = test.get(key);
//            if(grab.size() == 1){
//                logger.info("We are filling with " +key+ " "+grab.get(0));
//                out.put(key, grab.get(0));
//            }
//        }
//        return out;
        if (Objects.size() > 0 && !Names.isEmpty()) {
            ArrayList<AbstractMap.SimpleEntry<Integer, Integer>> out = new ArrayList<>();
            //select count(*),tai_id from topo_algo_input where tai_name||'.'||tai_value||'.'||tai_position in () group by tai_id having count(*) = ?;
            //Lets start making a query to the DB see if we have anything in there that matches.
            StringBuilder sb = new StringBuilder(); // Part 1 of the INSERT command
            StringBuilder whole = new StringBuilder();

            //This make the sub query....
            sb.append("select ");
            sb.append(tablePrefix);
            //This ? is the identifier for the entry in the input object
            //Can use it to match which input object matched the ID in the DB
            sb.append("ID,? as I from ");
            sb.append(tableName);
            sb.append(" where ");
            //Loop over the table columns
            sb.append(Names);

            //This ? is for the table information
            sb.append(" = ?");

            ArrayList<String> toCheck = new ArrayList<>();
            for (int i = 0; i < Objects.size() / 2; i++) {
                whole.append(sb);
                //System.out.println(sb);
                whole.append(" union ");
            }
            whole.delete(whole.length() - 7, whole.length());
            final String query = fix_schema_name(whole.toString());
            //System.out.println("EXISTING " + query);
            try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query)) {
                for (int i = 1; i < Objects.size() + 1; i++) {
                    ps.setString(i, Objects.get(i - 1));
                    //System.out.println(i + " " + Objects.get(i - 1));
                }
                try (ResultSet rset = ConnectionManager.executeSelect(ps)) {
                    while (rset.next()) {

                        final String current_id = tablePrefix + "ID";
                        out.add(new AbstractMap.SimpleEntry<Integer, Integer>(rset.getInt("I"), rset.getInt(current_id)));
                        //System.out.println("key " + rset.getInt("I") + " value " + rset.getInt(current_id));
                    }
                    rset.close();
                    ps.close();
                }
            } catch (SQLException ex) {
                logger.log(Level.SEVERE, ex.getMessage(), ex);
            }
            return out;
        } else {
            logger.log(Level.SEVERE, "Tried to make a query with no inputs");
            return null;
        }
    }

    /**
     * Currently works one by one. Could be made more generic?
     *
     * @param Objects
     * @param Names
     * @param tablePrefix
     * @param tableName
     * @return
     */
    public TreeMap<Integer, ArrayList<Integer>> checkTopoExistingMulti(ArrayList<String> Objects, String Names, String tablePrefix, String tableName) {
        if (Objects.size() > 0 && !Names.isEmpty()) {
            //TreeMap<Integer, Integer> out = new TreeMap<>();
            TreeMap<Integer, ArrayList<Integer>> test = new TreeMap<>();
            //select count(*),tai_id from topo_algo_input where tai_name||'.'||tai_value||'.'||tai_position in () group by tai_id having count(*) = ?;
            //Lets start making a query to the DB see if we have anything in there that matches.
            StringBuilder sb = new StringBuilder(1000); // Part 1 of the INSERT command
            StringBuilder whole = new StringBuilder(1000);

            //This make the sub query....
            sb.append("select ");
            sb.append(tablePrefix);
            //This ? is the identifier for the entry in the input object
            //Can use it to match which input object matched the ID in the DB
            sb.append("ID,? as I from ");
            sb.append(tableName);
            sb.append(" where ");
            //Loop over the table columns
            sb.append(Names);

            //This ? is for the table information
            sb.append(" = ?");

            //ArrayList<String> toCheck = new ArrayList<>();
            for (int i = 0; i < Objects.size() / 2; i++) {
                whole.append(sb);
                whole.append(" union ");
            }
            whole.delete(whole.length() - 7, whole.length());
            final String query = fix_schema_name(whole.toString());
            try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query)) {
                for (int i = 1; i < Objects.size() + 1; i++) {
                    ps.setString(i, Objects.get(i - 1));
                }
                try (ResultSet rset = ConnectionManager.executeSelect(ps)) {
                    while (rset.next()) {
                        ArrayList<Integer> current = new ArrayList<>();
                        int counter = rset.getInt("I");
                        if (test.size() > 0 && test.containsKey(counter)) {
                            current = test.get(counter);
                        }
                        String current_id = tablePrefix + "ID";
                        current.add(rset.getInt(current_id));
                        test.put(counter, current);
                    }
                    rset.close();
                    ps.close();
                }
            } catch (SQLException ex) {
                logger.log(Level.SEVERE, ex.getMessage(), ex);
            }
            return test;
        } else {
            logger.log(Level.SEVERE, "Tried to make a query with no inputs");
            return null;
        }
    }

    /**
     * Currently works one by one. Could be made more generic?
     *
     * @param Objects
     * @param ID
     * @param tableName
     * @param list_id
     * @return
     */
    public ArrayList<Integer> checkLinkExisting(ArrayList<String> Objects, String ID, String tableName, String list_id) {
        ArrayList<Integer> id = new ArrayList<>();
        if (!Objects.isEmpty()) {
            //select count(*),tai_id from topo_algo_input where tai_name||'.'||tai_value||'.'||tai_position in () group by tai_id having count(*) = ?;
            //Lets start making a query to the DB see if we have anything in there that matches.
            StringBuilder sb = new StringBuilder(); // Part 1 of the INSERT command

            //This make the sub query....
            sb.append("select total.");
            sb.append(ID);
            sb.append(" from (select count(*),");
            sb.append(ID);
            //This ? is the identifier for the entry in the input object
            //Can use it to match which input object matched the ID in the DB
            sb.append(" from ");
            sb.append(tableName);
            sb.append(" where ");
            //Loop over the table columns
            sb.append(list_id);

            //This ? is for the table information
            sb.append(" in (");

            for (String in : Objects) {
                sb.append(in);
                sb.append(",");
            }
            sb.deleteCharAt(sb.length() - 1);
            sb.append(") group by ");
            sb.append(ID);
            sb.append(" having count(*) = ");
            sb.append(Objects.size());
            sb.append(") match join (select count(*), ");
            sb.append(ID);
            sb.append(" from ");
            sb.append(tableName);
            sb.append(" group by ");
            sb.append(ID);
            sb.append(" having count(*) = ");
            sb.append(Objects.size());
            sb.append(") total on match.");
            sb.append(ID);
            sb.append(" = total.");
            sb.append(ID);
            final String query = fix_schema_name(sb.toString());
            //System.out.println(query);
            try {
                PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
                ResultSet rset = ConnectionManager.executeSelect(ps);
                while (rset.next()) {
                    id.add(rset.getInt(ID));
                }
                rset.close();
                ps.close();
            } catch (SQLException ex) {
                logger.log(Level.SEVERE, ex.getMessage(), ex);
            }
            return id;
        }
        return id;
    }

    /**
     * Currently works one by one. Could be made more generic?
     *
     * @param Objects
     * @param linkTableName
     * @param tableName
     * @param linkUpID
     * @param linkDownID
     * @param targetID
     * @param Ordering
     * @return
     */
    public ArrayList<Integer> getLinkedTable(ArrayList<String> Objects, String linkTableName, String tableName,
            String linkUpID, String linkDownID, String targetID, String Ordering) {
        ArrayList<Integer> id = new ArrayList<>();
        //select count(*),tai_id from topo_algo_input where tai_name||'.'||tai_value||'.'||tai_position in () group by tai_id having count(*) = ?;
        //Lets start making a query to the DB see if we have anything in there that matches.
        StringBuilder sb = new StringBuilder(); // Part 1 of the INSERT command

        //This make query
        //In the end we want to grab the contents of the tables we are linking to
        sb.append("select total.*");
        sb.append(" from (select count(*),");
        sb.append(linkDownID);
        //This ? is the identifier for the entry in the input object
        //Can use it to match which input object matched the ID in the DB
        sb.append(" from ");
        sb.append(linkTableName);
        sb.append(" where ");
        //Loop over the table columns
        sb.append(linkUpID);

        //This ? is for the table information
        sb.append(" in (");

        for (String in : Objects) {
            sb.append(in);
            sb.append(",");
        }
        sb.deleteCharAt(sb.length() - 1);
        sb.append(") group by ");
        sb.append(linkDownID);
        sb.append(" having count(*) = ");
        sb.append(Objects.size());
        sb.append(") match join ( select * from ");
        sb.append(tableName);
        sb.append(") total on match. ");
        sb.append(linkDownID);
        sb.append(" = total.");
        sb.append(targetID);
        sb.append(" order by total.");
        sb.append(Ordering);
        try {
            String query = fix_schema_name(sb.toString());
            try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
                    ResultSet rset = ConnectionManager.executeSelect(ps)) {
                while (rset.next()) {
                    id.add(rset.getInt(targetID));
                }
                rset.close();
                ps.close();
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return id;
    }

    /**
     *
     * @param ps
     * @return
     * @throws SQLException
     */
    public static ResultSet executeSelect(PreparedStatement ps) throws SQLException {
        ResultSet rset = ps.executeQuery();
        return rset;
    }

    /**
     *
     * @param ps
     * @return
     * @throws SQLException
     */
    public static ResultSet executeSelectCheck(PreparedStatement ps) throws SQLException {
        ResultSet rset = ps.executeQuery();
        return rset;
    }

    /**
     *
     * @param ps
     * @return
     * @throws SQLException
     */
    public static ResultSet executeInsert(PreparedStatement ps) throws SQLException {
        ResultSet rset = ps.executeQuery();
        return rset;
    }

    private int check_topo_start_from_column() throws SQLException {
        String query = "SELECT COUNT(*) FROM ALL_TAB_COLUMNS WHERE TABLE_NAME = 'HLT_TRIGGER_ELEMENT' AND COLUMN_NAME = 'HTE_TOPO_START_FROM' AND OWNER = ?";
        int check = 0;

        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ps.setString(1, this.getInitInfo().getSchemaName().toUpperCase());
        ResultSet rset = ps.executeQuery();

        while (rset.next()) {
            check = rset.getInt("COUNT(*)");
        }

        rset.close();
        ps.close();

        return check;
    }

    private int check_monitor_column() throws SQLException {
        String query = "SELECT COUNT(*) FROM ALL_TAB_COLUMNS WHERE TABLE_NAME = 'L1_TRIGGER_ITEM' AND COLUMN_NAME = 'L1TI_MONITOR' AND OWNER = ?";
        int check = 0;

        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ps.setString(1, this.getInitInfo().getSchemaName().toUpperCase());
        ResultSet rset = ps.executeQuery();

        while (rset.next()) {
            check = rset.getInt("COUNT(*)");
        }

        rset.close();
        ps.close();

        return check;
    }

}
