/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package triggerdb.Connections;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Michele
 */
public class TriggerSchema {

    /**
     *
     * @return
     * @throws SQLException
     */
    public static int GetVersion() throws SQLException {
        int ver = 0;
        String query = "SELECT TS_ID FROM TRIGGER_SCHEMA";
        query = ConnectionManager.getInstance().fix_schema_name(query);
        PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query);
        ResultSet rset = ps.executeQuery();
        while (rset.next()) {
            ver = rset.getInt("TS_ID");
        }
        rset.close();
        ps.close();
        return ver;
    }  
}
