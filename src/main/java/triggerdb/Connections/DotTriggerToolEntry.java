package triggerdb.Connections;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JMenuItem;

/**
 * Holds the connection information from a dot TriggerTool file entry or an 
 * entry in the DBLookup file.
 * @author tiago perez <tperez@cern.ch>
 * @date 2011-05-27
 */
@SuppressWarnings("serial")
public final class DotTriggerToolEntry extends JMenuItem implements Comparable<DotTriggerToolEntry> {

    /** DB Technology constants */

    /** Account name. E.g.: atlas_trig_stelzer */
    private final String dbaccount;

    /** Short host: ATONR, INTR, INT8R, DEVDB10/11
     *  Also used for db alias
     */
    private final String dbhost;

    /** db account password */
    private String dbpasswd;

    /** DB schema for DB's with multiple accounts. */

    private final String dbschema;

    
    /** db in readonly mode */
    private boolean readonly;

    /** Oracle, MySQL, DBLOOKUP */
    private final DBTechnology technology;

    /**
     * Initializes a dotTriggerTool entry from the line in the dotTriggerTool file:
     * FORMAT Example: Oracle,devdb10,atlas_trigger_tperez,atlas_trigger_tperez, PWD
     * @param dotTriggerToolLine a line of a dotTriggerTool file.
     */
    public DotTriggerToolEntry(final String dotTriggerToolLine) {
        super();

        String[] params = dotTriggerToolLine.split(",");
        
        switch (params[0].toLowerCase()) {
            case "oracle":
                this.technology = DBTechnology.ORACLE;
                break;
            default:
                this.technology = DBTechnology.UNKNOWN;
        }
        
        this.dbhost = params[1].toUpperCase();
        
        this.dbaccount = params[2].toUpperCase();
        
        if (params.length > 3) {
            this.dbschema = params[3];
        } else {
            this.dbschema = params[2].toUpperCase();
        }

        if (params.length > 4) {
            this.dbpasswd = params[4];
        } else {
            this.dbpasswd = "";
           
        }


        this.readonly = true;
    }

    /**
     *
     * @param _tech
     * @param _dbhost
     * @param _dbaccount
     * @param _dbschema
     * @param _dbpasswd
     */
    public DotTriggerToolEntry(final DBTechnology _tech, final String _dbhost,
                               final String _dbaccount, final String _dbschema,
                               final String _dbpasswd) {
        super();
        this.technology = _tech;
        this.dbhost = _dbhost.toUpperCase();
        this.dbaccount = _dbaccount.toUpperCase();
        this.dbschema = _dbschema.toUpperCase();
        this.dbpasswd = _dbpasswd;
        this.readonly = true;
        this.setText(this.toString());
    }

    /**
     *
     * @param readonly
     */
    public void setReadOnly(boolean readonly) {
        this.readonly = readonly;
        this.setText(this.toString());
    }
    
    public boolean getReadOnly() {
        return this.readonly;
    }

    /**
     * Creates and returns an InitInfo object ready to connect to db.
     * @return the InitInfo with the information of this DotTriggerTool entry.
     * @throws java.lang.Exception
     */
    public InitInfo getInitInfo() throws Exception {
        InitInfo ii = new InitInfo();
        if (technology != DBTechnology.DBLOOKUP) {
            ii.setTechnology(this.getTechnology());
            ii.setdbHost(this.dbhost); 
            ii.setUserName(this.dbaccount);
            ii.setSchemaName(this.dbschema);
            ii.setPassWord(this.dbpasswd);
            ii.setUserMode(UserMode.EXPERT);
        } else {
            ii = getInitInfoFromDBLookUp();
            this.dbpasswd = ii.getPassWord();
        }
        return ii;
    }
    

    /**
     * Prints the connection as "ATLAS_TRIGGER_TPEREZ@DEV10;*"
     * @return the entry as string.
     */
    @Override
    public String toString() {
        if (null != this.technology) switch (this.technology) {
            case DBLOOKUP:
                return dbhost + "  " + (this.readonly ? "r " : "rw")
                        + "  [" + this.dbaccount.toLowerCase() + "] *";
            case ORACLE:
                return dbaccount + " @ " + dbhost + (this.dbpasswd.length()==0 ? "" : " *");
            default:
                return dbhost;
        }
        return dbhost;
    }

    @Override
    public int compareTo(DotTriggerToolEntry e) {
        if (this.technology != e.getTechnology()) {
            return this.technology.ordinal() - e.getTechnology().ordinal();
        }

        if (!this.dbhost.equals(e.getDbhost())) {
            return this.dbhost.compareTo(e.getDbhost());
        }

        return this.dbaccount.compareTo(e.getDbaccount());
    }

    /**
     *
     * @return
     */
    public String getDbaccount() {
        return this.dbaccount;
    }

    /**
     *
     * @return
     */
    public String getDbhost() {
        return this.dbhost;
    }

    /**
     *
     * @return
     */
    public String getDbpasswd() {
        return this.dbpasswd;
    }

    /**
     *
     * @return
     */
    public String getDbschema() {
        return this.dbschema;
    }

    /**
     *
     * @return
     */
    public String getAlias() {
        return this.dbhost;
    }
    
    /**
     *
     * @return
     */
    public boolean isReadonly() {
        return this.readonly;
    }

    /**
     *
     * @return
     */
    public DBTechnology getTechnology() {
        return this.technology;
    }

    /**
     * parse connection string "oracle://intr/ATLAS_CONF_DEV2", get auth file 
     * and read username and pwd from it
     * @return the init info object ready to connect.
     */
    private InitInfo getInitInfoFromDBLookUp() throws Exception {
        InitInfo ii = new InitInfo();
        String host = "";
        String schema = "";
        String fName = null;

        String svcName = this.dbaccount;

        String techno = svcName.split(":")[0];

        String[] parts = svcName.split(";");
        Pattern p = Pattern.compile("(\\w+)://(\\w+)/(\\w+)");
        Matcher m = p.matcher(parts[0]);
        if (m.find()) {
            techno = m.group(1);
            host = m.group(2);
            schema = m.group(3);
        }

        String[] user = DBConnections.getConnUserPwFromSvcName(svcName);
        String username = user[0];
        String password = user[1];

        ii.setTechnologyFromString(techno); // always set technology before setting host!
        ii.setdbHost(host);
        ii.setSchemaName(schema);
        ii.setUserName(username);
        ii.setPassWord(password);
        return ii;
    }
    
}
