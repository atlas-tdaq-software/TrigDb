package triggerdb.Connections;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Helper class which holds connection data to several DB. This class also
 * provides several methods to set up connections to standard DB's.
 */
public final class DBConnections {

    final static class AliasEntry {
        private String service = "";
        private boolean readonly = false;
        private String user = "";
        private String password = "";
        public AliasEntry(final String service, boolean readonly) {
            this.service = service;
            this.readonly = readonly;
        }
        public String getServiceName() { return service; }
        public boolean isReadonly() { return readonly; }
        public String getUser() { return user; }
        public void setUser(String user) { this.user = user; }
        public String getPassword() { return password; }
        public void setPassword(String password) { this.password = password; }
    }

    /**
     * Online (P1) or offline environment
     */
    private static boolean isOnline = false;
    private static final String ATLR = "atlr";
    private static final String ATONR = "atonr";
    /**
     * Message Log.
     */
    private static final Logger logger = Logger.getLogger("TriggerDb");

    /**
     * True if dblookup has been read
     */
    static boolean isDblookupRead = false;

    /**
     * True if dbauth has been read
     */
    static boolean isDbAuthRead = false;

    /**
     * Static aliases to databases.
     */
    public static final HashMap<String, AliasEntry> DBLookup = new HashMap<>();

    /**
     * Static aliases to databases.
     */
    public static final HashMap<String, String> DBALIASES = new HashMap<>();

    /**
     * True if the .triggertool have been read
     */
    private static String atonrW_password;
    private static String atonrR_password;
    private static String atlrR_password;
    /**
     * Lines from .triggertool
     */
    private static ArrayList<String> lines = null;

    static {
        // Read connections from tnsnames.ora, if not available use explicit connection
        String tnsadmin = System.getenv("TNS_ADMIN");
        if (tnsadmin != null) {
            DBALIASES.put("int8r", "jdbc:oracle:thin:@int8r_lb");
            DBALIASES.put("atonr", "jdbc:oracle:thin:@atonr");
            DBALIASES.put("atlr", "jdbc:oracle:thin:@atlr");
            DBALIASES.put("atonr_adg", "jdbc:oracle:thin:@atonr_adg");
        } else {
            tnsadmin = "/eos/project/o/oracle/public/admin";
            DBALIASES.put("int8r", "jdbc:oracle:thin:@int8r1-s.cern.ch:10121:int8r1");
            DBALIASES.put("atonr", "jdbc:oracle:thin:@atonr-atcn-s.cern.ch:10121:atonr1");
            DBALIASES.put("atlr", "jdbc:oracle:thin:@atlr-s.cern.ch:10121:atlr1");
            DBALIASES.put("atonr_adg", "jdbc:oracle:thin:@atonr_adg.cern.ch:10121:atonr1");
        }
        System.setProperty("oracle.net.tns_admin", tnsadmin);
        logger.log(Level.INFO, "Using TNS_ADMIN directory {0}/", tnsadmin);
        // Alternative alias names
        DBALIASES.put("atonr_conf", DBALIASES.get("atonr"));    // same as atonr
        DBALIASES.put("atonr_mubr", DBALIASES.get("atonr"));    // same as atonr
        DBALIASES.put("atlas_config", DBALIASES.get("atlr"));

        // Local connections
        DBALIASES.put("localhost-tunnel-devdb10", "jdbc:oracle:thin:@localhost:10520:D10");
        DBALIASES.put("localhost-tunnel-devdb11", "jdbc:oracle:thin:@localhost:10121:DEVDB11");
        DBALIASES.put("localhosttunneldevdb11", DBALIASES.get("localhost-tunnel-devdb11"));
        DBALIASES.put("localhost-tunnel-atlr", "jdbc:oracle:thin:@localhost:10121:atlr1");
        DBALIASES.put("localhost-tunnel-intr", "jdbc:oracle:thin:@localhost:10121:intr1");
        DBALIASES.put("localhosttunnelintr", DBALIASES.get("localhost-tunnel-intr"));
        DBALIASES.put("localrhul", "jdbc:oracle:thin:@localhost:1521:xe");
        DBALIASES.put("rhulfarm", "jdbc:oracle:thin:@dbserver.pp.rhul.ac.uk:1521:xe");
        DBALIASES.put("local", "jdbc:oracle:thin:@localhost:1521:xe");
        DBALIASES.put("local1", "jdbc:oracle:thin:@localhost:10001:xe");
        DBALIASES.put("remote1", "jdbc:oracle:thin:@localhost:10001:atlr1");
    }

//    static {
//        OFFLINE_CONF_FILES.add(System.getProperty("user.home") + "/.triggertool");
//        OFFLINE_CONF_FILES.add("/afs/cern.ch/user/a/attrgcnf/TriggerTool/.triggertool");
//    }

    /**
     * Set up a reader connection to ATLR. ,
     *
     * @param online
     */
    public static void setOnline(boolean online) {
        isOnline = online;
    }

    public boolean getOnline(){
        return isOnline;
    }
    /**
     * Check whether a user is a trigger expert. The role info is retrieved from
     * LDAP via lfinger.
     *
     * @param user the user to check.
     * @return
     * <code>true</code> if use is a trigger expert.
     */
    public static boolean isOnlineExpert(final String user) {
        List<String> roles = _LFINGER_RUNCOMMAND(user);
        boolean isExpert = roles.contains("TRG:expert");
        logger.log(Level.INFO, " Is {0} an Online Trigger Expert ? {1}", new Object[]{user, isExpert});
        return isExpert;
    }


    // ///////////////////
    // OLD
    // ///////////////////
    /**
     * Creates and returns a valid InitInfo object. 1) parse DBALIAS to check
     * whether ORACLE, ALIAS
     *
     * @param _db
     * @param offline
     * @return
     */
    public InitInfo setUpConnection(final String _db, final Boolean offline) {

        // Looks for the db conn string in dottriggertool files.
        InitInfo ii = this.setUpConnDotTriggerTool(_db, offline);

        // If not found, look in auth.xml, dblookup.xml
        //      if(ii==null){
        //          ii = this.readAuthXml(connString);
        //      }
        return ii;
    }

    /**
     * Returns the full db connection string from the alias or short name.
     *
     * @param alias the DB alias as defined in DBALIASES.
     * @return The full connection string.
     */
    public static String getDBstring(final String alias) {
        String dbAlias = alias.toLowerCase();
        if (!DBALIASES.containsKey(dbAlias)) {
            return null;
        }
        return DBALIASES.get(dbAlias);
    }
    
    public void setDBAliases(String param1, String param2) {
        DBALIASES.put(param1, param2);
    }

    public void setDBLookup(String param1, AliasEntry param2) {
        DBLookup.put(param1, param2);
    }

    /**
     * Set up a reader connection to ATLR.
     *
     * @return a new configured InitInfo object ready to connect to ATLR.
     */
    public static InitInfo createAtlrReader() {
        InitInfo ii = new InitInfo();
        DBConnections.setAtlrReader(ii);
        return ii;
    }

    /**
     * Set up a reader connection to ATLR.
     *
     * @param ii the InitInfo data.
     */
    public static void setAtlrReader(final InitInfo ii) {
        ii.setTechnology(DBTechnology.ORACLE);
        ii.setUserMode(UserMode.EXPERT);
        ii.setUserName("ATLAS_CONF_TRIGGER_RUN2_R");
        ii.setdbHost("atlr");
        ii.setSchemaName("ATLAS_CONF_TRIGGER_RUN2");
        ii.setOnline(false);
        ii.setOnlineDB(false);
        setPasswords(ii);
        ii.setPassWord(atlrR_password);
    }
    
    public InitInfo getAtlrReader(){
        InitInfo reader = new InitInfo();
        this.setAtlrReader(reader);
        return reader;
    }

    /**
     * Creates an InitInfo object ready to connect to the online DB at P1.
     *
     * @param ii the original initInfo.
     */
    public static void setAtonrWriter(final InitInfo ii) {
        ii.setTechnology(DBTechnology.ORACLE);
        ii.setUserMode(UserMode.EXPERT);
        ii.setUserName("ATLAS_CONF_TRIGGER_RUN2_W");
        ii.setdbHost("atonr");
        ii.setSchemaName("ATLAS_CONF_TRIGGER_RUN2");
        ii.setOnline(true);
        ii.setOnlineDB(true);
        ii.setgoodInfo(true);
        setPasswords(ii);
        ii.setPassWord(ii.getAtonrWPassword());
    }

    /**
     * Set up a reader connection to ATONR.
     *
     * @param ii the InitInfo data.
     * @return the configured InitInfo.
     */
    public static InitInfo setAtonrReader(final InitInfo ii) {
        ii.setTechnology(DBTechnology.ORACLE);
        ii.setUserMode(UserMode.EXPERT);
        ii.setUserName("ATLAS_CONF_TRIGGER_RUN2_R");
        ii.setdbHost("atonr");
        ii.setSchemaName("ATLAS_CONF_TRIGGER_RUN2");
        ii.setOnline(true);
        ii.setOnlineDB(true);
        ii.setgoodInfo(true);
        setPasswords(ii);
        ii.setPassWord(ii.getAtonrRPassword());
        return ii;
    }

    /**
     * Set up a reader connection to ATLR.
     *
     * @param ii the InitInfo data.
     */
    public static void setRHULReader(final InitInfo ii) {
        ii.setTechnology(DBTechnology.ORACLE);
        ii.setUserMode(UserMode.EXPERT);
        ii.setUserName("giannell");
        ii.setdbHost("rhulfarm");
        ii.setSchemaName("GIANNELL");
        ii.setOnline(false);
        ii.setOnlineDB(false);
        setPasswords(ii);
        ii.setPassWord("atlas");
    }

    /**
     * Set up a reader connection to ATLR.
     *
     * @param ii the InitInfo data.
     */
    public static void setLocalReader(final InitInfo ii) {
        ii.setTechnology(DBTechnology.ORACLE);
        ii.setUserMode(UserMode.EXPERT);
        ii.setUserName("faucci");
        ii.setdbHost("localhost");
        ii.setSchemaName("GIANNELL");
        ii.setOnline(false);
        ii.setOnlineDB(false);
        setPasswords(ii);
        ii.setPassWord("michele");
    }
    
    /**
     * Creates an InitInfo object ready to connect to the online DB at P1.
     *
     * @return the object containing the connection settings for ATONR.
     */
    public static InitInfo createAtonrWriter() {
        InitInfo ii = new InitInfo();
        DBConnections.setAtonrWriter(ii);
        return ii;
    }


    /**
     * Set up a reader connection to ATONR.
     *
     * @return the configured InitInfo for readonly ATONR access
     */
    public static InitInfo createAtonrReader() {
        final InitInfo ii = new InitInfo();
        return setAtonrReader(ii);
    }

    /**
     * Set up a reader connection to ATONR.
     *
     * @param ii the InitInfo data.
     * @return the configured InitInfo.
     */
    private static void setPasswords(InitInfo ii) {
        readLinesFromDotTriggerTool();
        ii.setAtlrRPassword(atlrR_password);
        ii.setAtonrRPassword(atonrR_password);
        ii.setAtonrWPassword(atonrW_password);

        if( ! ii.getOnline() && ii.getdbHost().contains("atlr") ) {
            if(atlrR_password==null || atlrR_password.isEmpty()) {
                try {
                    String[] userAndPW = getConnUserPwFromAlias("TRIGGERDB");
                    atlrR_password = userAndPW[1];
                    ii.setAtlrRPassword(userAndPW[1]);
                } catch (Exception ex) {
                    Logger.getLogger(DBConnections.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
   
    


    /**
     * Tries to set up a InitInfo object from the db connection string and the
     * dotTriggerTool files.
     *
     * @param connString the connection string
     * @param offline
     * @return
     */
    private InitInfo setUpConnDotTriggerTool(final String connString, final boolean offline) {

        // The InitInfo to return.
        InitInfo retIi = null;

        // The expanded Connection String.
        String _connString = connString;

        if (DBALIASES.containsKey(_connString)) {
            _connString = DBALIASES.get(_connString);
        }

        /**
         * atnor writer password is read from file.
         */
        if (_connString.equalsIgnoreCase(DBALIASES.get(ATONR))) {
//          this.setAtonr(retIi);
        } else if (_connString.equalsIgnoreCase(DBALIASES.get(ATLR))) {
            DBConnections.setAtlrReader(retIi);
        } else {
            retIi = this.setUpConnection(_connString, true);
        }

        return retIi;
    }


    private static void readDBLookup() throws Exception {
        
        if(isDblookupRead) { return; }
        isDblookupRead = true;

        String coral_dblookup_path = DBConnections.getDBLookupPath();
        logger.log(Level.INFO, "Using CORAL authentication path {0}", coral_dblookup_path);

        File f = new File(coral_dblookup_path + "/dblookup.xml");
        if (!f.exists()) {
            return;
        }

        // first build a xml file parser
        DocumentBuilder parser;
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setIgnoringComments(true);
            factory.setCoalescing(true);
            factory.setNamespaceAware(false);
            factory.setValidating(false);
            parser = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            logger.log(Level.SEVERE, "Can not configure XML parser to read DBLookup file: {0}", e.getMessage());
            throw new Exception();
        }

        try {

            // first get the xml document from the file
            Document document = parser.parse(f);
            // then get all the locical services
            NodeList lservices = document.getElementsByTagName("logicalservice");
            // and find ours
            for (int i = 0; i < lservices.getLength(); i++) {
                String alias = ((Element) lservices.item(i)).getAttribute("name");
                if (!alias.startsWith("TRIGGERDB")) {
                    continue;
                }
                Element lservice = (Element) lservices.item(i);
                Element lookedUp = (Element) lservice.getElementsByTagName("service").item(0);
                String servicename = lookedUp.getAttribute("name");
                boolean isReadonly = lookedUp.getAttribute("accessMode").equalsIgnoreCase("read");
                DBLookup.put(alias, new AliasEntry(servicename, isReadonly));
            }
        } catch (SAXException e) {
            logger.log(Level.SEVERE, "Problems in file {0}/{1}: {2}", new Object[]{f.getPath(), f.getName(), e.getMessage()});
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Problems in file {0}/{1}: {2}", new Object[]{f.getPath(), f.getName(), e.getMessage()});
        }

        readAuthentication();
    }

    private static void readAuthentication() throws Exception {
        
        if(isDbAuthRead) { return; }
        isDbAuthRead = true;

        DocumentBuilder parser = null;
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setIgnoringComments(true);
            factory.setCoalescing(true);
            factory.setNamespaceAware(false);
            factory.setValidating(false);
            parser = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            logger.log(Level.SEVERE, "Can not configure XML parser to read Authentication file: {0}", e.getMessage());
            throw new Exception();
        }

        String username = null;
        String password = null;

        logger.log(Level.FINE, "TriggerTool is using authentication to find user names and password");
        String coral_auth_path = DBConnections.getDBAuthPath();
        ArrayList<File> dbAuthfiles = new ArrayList<>();
        File f = new File(coral_auth_path + "/authentication.xml");
        if (f.exists()) {
            dbAuthfiles.add(f);
        }
        if (dbAuthfiles.isEmpty()) {
            logger.log(Level.SEVERE, "No authentication file, neither locally nor in ''{0}''", coral_auth_path);
            throw new Exception();
        }


        // look in each file (one or two) to find the first
        // occurance of the alias and take the first instance of
        // service (this is also the behavior of CORAL, so we do
        // the same here)

        HashMap<String, String> accessMap = new HashMap<>();

        //Element authConn = null;
        Iterator<File> fIt = dbAuthfiles.iterator();
        while (fIt.hasNext()) {
            f = fIt.next();
            try {
                // first get the xml document from the file
                Document document = parser.parse(f);
                // then get all the connections
                NodeList connections = document.getElementsByTagName("connection");
                // and find ours
                for (int i = 0; i < connections.getLength(); i++) {
                    String authSrvName = ((Element) connections.item(i)).getAttribute("name");
                    logger.log(Level.FINE, "Found connection for {0}", authSrvName);
                    Element authConn = (Element) connections.item(i);
                    NodeList params = authConn.getElementsByTagName("parameter");
                    for (int j = 0; j < params.getLength(); j++) {
                        Element param = (Element) params.item(j);
                        if (param.getAttribute("name").equals("user")) {
                            username = param.getAttribute("value");
                        }
                        if (param.getAttribute("name").equals("password")) {
                            password = param.getAttribute("value");
                        }
                    }
                    if (username == null || password == null) {
                        logger.log(Level.SEVERE, "In authentication file {0}/{1} connection {2} has no user or no password", new Object[]{f.getPath(), f.getName(), authSrvName});
                        throw new Exception();
                    }
                    accessMap.put(authSrvName, username + "#" + password);
                }
            } catch (Exception e) {
                logger.log(Level.SEVERE, "Problems in file {0}/{1}: {2}", new Object[]{f.getPath(), f.getName(), e.getMessage()});
            }

        }
        
        for( AliasEntry value : DBLookup.values()) {

            String userpw = accessMap.get(value.getServiceName());
            if(userpw!=null) {
                String[] UserAndPW = userpw.split("#");
                value.setUser(UserAndPW[0]);
                value.setPassword(UserAndPW[1]);
            } else {
                logger.log(Level.WARNING, "Connection {0} is not listed in authentication file {1}", new Object[]{value.getServiceName(), f.getPath()});
            }
        }

    }

    /**
     * Read the lines from the .triggertool configuration file and sets the 3
     * passwords if available
     *
     * TODO move this to ConnectionManager or even better create a new class
     * "triggertool.Connections.ConnectionSetter"
     *
     */
    public static void readLinesFromDotTriggerTool() {

        if (lines != null) {
            return; // already read
        }
        ArrayList<String> filenames = new ArrayList<>();
        ArrayList<File> files = new ArrayList<>();

        // offline we use first the user file $HOME/.triggertool
        if (!isOnline) {
            String fn = System.getProperty("user.home") + "/.triggertool";
            File f = new File(fn);
            if (f.exists()) {
                files.add(f);
                logger.log(Level.INFO, "Found user .triggertool file: {0}", fn);
            }
        }
        if (isOnline) {
            filenames.add("/det/tdaq/scripts/.triggertool");
        } else {
            filenames.add("/afs/cern.ch/user/a/attrgcnf/TriggerTool/.triggertool");
        }
        for (String fn : filenames) {
            File f = new File(fn);
            if (!f.exists()) {
                continue;
            }
            files.add(f);
            break;
        }

        lines = new ArrayList<>();
        for (File f : files) {
            try (BufferedReader in = new BufferedReader(new FileReader(f))) {
                String line;
                while ((line = in.readLine()) != null) {
                    lines.add(line);
                }
            } catch (IOException e) {
                logger.log(Level.WARNING, "Cannot open .triggertool: {0}", e.getMessage());
            }
        }

        // setting passwords
        for (String line : lines) {
            if (line.startsWith("atonr_W_Password_run2=")) {
                atonrW_password = line.replace("atonr_W_Password_run2=", "").trim();
            } else if (line.startsWith("atonr_R_Password_run2=")) {
                atonrR_password = line.replace("atonr_R_Password_run2=", "").trim();
            } else if (line.startsWith("atlr_R_Password_run2=")) {
                atlrR_password = line.replace("atlr_R_Password_run2=", "").trim();
            }
        }

    }

    /**
     *
     * @param aliases
     */
    public static void createDropDownEntriesFromDBLookup(ArrayList<DotTriggerToolEntry> aliases) {
        try {
            readDBLookup();
        } catch (Exception ex) {
            Logger.getLogger(DBConnections.class.getName()).log(Level.SEVERE, null, ex);
        }

        for( Map.Entry e : DBLookup.entrySet()) {
            String alias = (String) e.getKey();
            AliasEntry value = (AliasEntry) e.getValue();
            DotTriggerToolEntry dtte = new DotTriggerToolEntry(DBTechnology.DBLOOKUP, alias, value.getServiceName(), "", "");
            dtte.setReadOnly(value.isReadonly());
            aliases.add(dtte);
        }
                
        Collections.sort(aliases);
    }
   
    /**
     * Read the .triggertool configuration file.
     *
     * TODO move this to ConnectionManager or even better create a new class
     * "triggertool.Connections.ConnectionSetter"
     *
     * @param oracle
     * @param mysql
     * @param sqlite
     */
    public static void createDropDownEntriesFromDotTriggerTool(ArrayList<DotTriggerToolEntry> oracle) {
        readLinesFromDotTriggerTool();
        for (String line : lines) {
            String[] params = line.split(",");
            String technology = params[0].toLowerCase(); 
            switch (technology) {
                case "oracle":
                    oracle.add(new DotTriggerToolEntry(DBTechnology.ORACLE, params[1], params[2],
                            (params.length > 3) ? params[3] : params[2],
                            (params.length > 4) ? params[4] : ""));
                    break;
            }
        }
        Collections.sort(oracle);
    }


    /**
     * Get the username & password from a dblookup alias.
     * @param alias
     * @return 
     * @throws java.lang.Exception 
     */
    public static String[] getConnUserPwFromAlias(final String alias) throws Exception {

        readDBLookup();

        AliasEntry aliasEntry = DBLookup.get(alias);

        if(aliasEntry == null) {
            logger.log(Level.SEVERE, "Could not find DB alias '" + alias +"' in dblookup.xml");
            return new String[]{ "", "" };
        }
        
        return new String[]{ aliasEntry.getUser(), aliasEntry.getPassword() };

    }


    /**
     * utility function to get DBConnection from DBAlias.
     * @param alias
     * @return 
     * @throws java.lang.Exception
     */
    public static String getSvcNameFromDbConn(final String alias) throws Exception {

        readDBLookup();

        AliasEntry aliasEntry = DBLookup.get(alias);

        if(aliasEntry == null) {
            logger.log(Level.SEVERE, "Could not find DB alias '" + alias +"' in dblookup.xml");
            return "";
        }

        return aliasEntry.getServiceName();
    }



    /**
     * Get the username & password form a dblookup entry.
     *
     * @param svcName the dblookup "service name" e.g.
     * "oracle://intr/ATLAS_CONF_DEV2"
     * @return username,password
     * @throws Exception
     */
    public static String[] getConnUserPwFromSvcName(final String svcName) throws Exception {

        readAuthentication();

        for( AliasEntry aliasEntry : DBLookup.values()) {

            if( ! aliasEntry.getServiceName().equals(svcName)) {
                continue;
            }

            return new String[]{ aliasEntry.getUser(), aliasEntry.getPassword() };

        }

        return new String[]{ "", "" };

    }



    /**
     * Check the system and java start environment for the DB LOOK UP path.
     *
     * @return the path to the dblookup.xml file.
     */
    public static String getDBLookupPath() {
        String coral_dblookup_path = System.getProperty("CORAL_DBLOOKUP_PATH");
        if (coral_dblookup_path == null) {
            coral_dblookup_path = System.getenv("TRIGGER_EXP_CORAL_PATH");
        }
        if (coral_dblookup_path == null) {
            coral_dblookup_path = System.getenv("CORAL_DBLOOKUP_PATH");
        }
        return coral_dblookup_path;
    }

    /**
     * Check the system and java start environment for the AUTHENTICATUN.xml
     * path.
     *
     * @return the path to the authentication.xml file.
     */
    public static String getDBAuthPath() {
        String coral_auth_path = System.getProperty("CORAL_AUTH_PATH");
        if (coral_auth_path == null) {
            coral_auth_path = System.getenv("TRIGGER_EXP_CORAL_PATH");
        }
        if (coral_auth_path == null) {
            coral_auth_path = System.getenv("CORAL_AUTH_PATH");
        }
        return coral_auth_path;
    }

    /**
     * Run lfinger for the given user, parse the text output and return the
     * roles "Assigned and Enabled"
     *
     * @param user the user to check roles for.
     * @return the list of enabled roles.
     */
    private static List<String> _LFINGER_RUNCOMMAND(final String user) {

        Runtime rt = Runtime.getRuntime();
        Process process = null;
        try {
            process = rt.exec("lfinger " + user);
        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
        } 
        if (process == null) {
            return null;
        }
                
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(process.getInputStream()) );


        List<String> roles = new ArrayList<>();
        Boolean rolesEnabled = false;
        try {
            // Read first line
            String line = bufferedReader.readLine().trim();
            // Read through buffer line by line, filter enabled roles
            while (line != null) {
                if (rolesEnabled) {
                    roles.add(line);
                } else if (line.contains("ROLES ASSIGNED AND ENABLED")) {
                    rolesEnabled = true;
                }
                line = bufferedReader.readLine();
            }
        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
        return roles;
    }

}
