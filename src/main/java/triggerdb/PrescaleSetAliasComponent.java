package triggerdb;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import triggerdb.Entities.HLT.HLTTriggerMenu;
import triggerdb.Entities.L1.L1Menu;
import triggerdb.Entities.SMT.SuperMasterTable;
import triggerdb.Entities.alias.HLTPrescaleSetAliasLumi;
import triggerdb.Entities.alias.L1PrescaleSetAliasLumi;
import triggerdb.Entities.alias.PrescaleSetAlias;

/**
 *
 * Class that handles PrescaleSetAlias in a more advance and simple fashion.
 * This provide methods to do complex operations and queries on the aliases.
 * @date 2012-07-19
 *
 * @author Tiago Perez <tperez@cern.ch>
 */
public final class PrescaleSetAliasComponent
        implements Comparable<PrescaleSetAliasComponent> {

    private final PrescaleSetAlias psa;
    private SuperMasterTable smt;
    private final List<PrescaleSetAliasLumi> lumis = new ArrayList<>();

    /**
     * Get the list of Prescale Set Aliases from a given SMK
     *
     * @param smk the Super Master Key.
     * @return the list of aliases (PrescaleSetAliasComponent) linked to the
     * given super master table.
     * @throws java.sql.SQLException
     */
    public static List<PrescaleSetAliasComponent> getAliasList(final int smk) throws SQLException {
        SuperMasterTable _smt = new SuperMasterTable(smk);
        List<PrescaleSetAliasComponent> psacList = new ArrayList<>();
        List<Integer> aliasIdList = PrescaleSetAlias.getAliasIdList(smk);
        for (Integer psaId : aliasIdList) {
            psacList.add(new PrescaleSetAliasComponent(psaId, _smt));
        }
        return psacList;
    }

    /**
     *
     * @param smt
     * @param alias
     * @return
     * @throws SQLException
     */
    public static List<L1PrescaleSetAliasLumi> get_l1_aliases(final SuperMasterTable smt, final PrescaleSetAlias alias) throws SQLException {
        int menuId = smt.get_l1_master_table().get_menu_id();
        return alias.getL1PrescaleSetAliases(menuId);
    }

    /**
     *
     * @param smt
     * @param alias
     * @return
     * @throws SQLException
     */
    public static List<HLTPrescaleSetAliasLumi> get_hlt_aliases(final SuperMasterTable smt, final PrescaleSetAlias alias) throws SQLException {
        int menuId = smt.get_hlt_master_table().get_menu_id();
        return alias.getHLTPrescaleSetAliases(menuId);
    }

    /**
     *
     * @param smt
     * @param alias
     * @return
     * @throws SQLException
     */
    public static List<PrescaleSetAliasLumi> _get_lumis_from_DB(final SuperMasterTable smt, final PrescaleSetAlias alias) throws SQLException {
        List<PrescaleSetAliasLumi> _lumis = new ArrayList<>();
        List<L1PrescaleSetAliasLumi> _l1psaList = get_l1_aliases(smt, alias);
        List<HLTPrescaleSetAliasLumi> _hpsaList = get_hlt_aliases(smt, alias);
        Collections.sort(_l1psaList);
        Collections.sort(_hpsaList);


        //String msg = " L1 Size : " + _l1psaList.size() + "\t HLT Size : " + _hpsaList.size();

        for (Iterator<L1PrescaleSetAliasLumi> l1Iter = _l1psaList.iterator();
                l1Iter.hasNext();) {
            L1PrescaleSetAliasLumi _l1psa = l1Iter.next();
            String l1Min = _l1psa.get_lum_min();
            String l1Max = _l1psa.get_lum_max();
            HLTPrescaleSetAliasLumi temp = null;
            //for (HLTPrescaleSetAliasLumi _hpsa : _hpsaList) {

            for (Iterator<HLTPrescaleSetAliasLumi> hIter = _hpsaList.iterator(); hIter.hasNext();) {
                HLTPrescaleSetAliasLumi _hpsa = hIter.next();

                //msg += "\n \t" + _l1psa.get_id() + "\t " + _hpsa.get_id();

                String hMin = _hpsa.get_lum_min();
                if (l1Min.equalsIgnoreCase(hMin)) {
                    String hMax = _hpsa.get_lum_max();
                    if (l1Max.equalsIgnoreCase(hMax)) {
                        temp = _hpsa;
                    }
                }
                if (temp != null) {
                    _lumis.add(new PrescaleSetAliasLumi(_l1psa, temp));
                    //msg += "\n \t\t Macth " + _l1psa.get_id() + "\t " + _hpsa.get_id() + "\n";
                    l1Iter.remove();
                    hIter.remove();
                    break;
                }
            }
        }
        //logger.info(msg + "\n L1 Size : " + _l1psaList.size() + "\t HLT Size : " + _hpsaList.size());

        L1Menu l1Menu = smt.get_l1_master_table().get_menu();
        HLTTriggerMenu hMenu = smt.get_hlt_master_table().get_menu();
        for (L1PrescaleSetAliasLumi _l1psa : _l1psaList) {
            PrescaleSetAliasLumi lum = new PrescaleSetAliasLumi(_l1psa, null);
            lum.get_hlt_prescale_set_alias().set_menu(hMenu);
            _lumis.add(lum);

        }
        for (HLTPrescaleSetAliasLumi _hpsa : _hpsaList) {
            PrescaleSetAliasLumi lum = new PrescaleSetAliasLumi(null, _hpsa);
            lum.get_l1_prescale_set_alias().set_menu(l1Menu);
            _lumis.add(lum);
        }
        Collections.sort(_lumis);
        return _lumis;
    }

    /**
     *
     */
    public PrescaleSetAliasComponent() {
        this.psa = new PrescaleSetAlias();
    }

    /**
     *
     * @param psaId
     * @param _smt
     * @throws SQLException
     */
    public PrescaleSetAliasComponent(final int psaId, final SuperMasterTable _smt) throws SQLException {
        this.psa = new PrescaleSetAlias(psaId);
        this.smt = _smt;
    }

    /**
     * Query the DB for the list of PrescaleSetAliasLumi linked to this alias.
     *
     * @return the list of Lumis linked to this alias.
     * @throws java.sql.SQLException
     */
    public List<PrescaleSetAliasLumi> get_lumis() throws SQLException {
        if (lumis.isEmpty()) {
            lumis.addAll(_get_lumis_from_DB(this.smt, this.psa));
        }
        return lumis;
    }

    public void reset_lumis() throws SQLException {
        lumis.clear();;
        lumis.addAll(_get_lumis_from_DB(this.smt, this.psa));
    }
    
    /**
     *
     * @param l
     * @throws SQLException
     */
    public void add_lumi(final PrescaleSetAliasLumi l) throws SQLException{
        this.get_lumis().add(l);
    }

    /**
     *
     * @return
     */
    public String get_name() {
        return this.psa.get_name();
    }

    /**
     *
     * @param name
     */
    public void set_name(final String name) {
        this.psa.set_name(name);
    }

    /**
     * Get the Long description of the alias: "HI v1", "1300 bunches_25ns"...
     *
     * @return the long description of the alias.
     */
    public String get_name_free() {
        return this.psa.get_name_free();
    }

    /**
     * Set the long description of the alias: "HI v1", "1300 bunches_25ns"...
     *
     * @param nameFree the long description of the alias.
     */
    public void set_name_free(final String nameFree) {
        this.psa.set_name_free(nameFree);
    }

    /**
     * Get the comment.
     *
     * @return the comment.
     */
    public String get_comment() {
        return this.psa.get_comment();
    }

    /**
     * Set a comment.
     *
     * @param comment a comment.
     */
    public void set_comment(final String comment) {
        this.psa.set_comment(comment);
    }

    /**
     * Save the alias to DB.
     *
     * @return the id of the saved alias or -1 if fail.
     * @throws java.sql.SQLException
     */
    public int save() throws SQLException {        
        return this.psa.save();
    }
    
    /**
     *
     * @param _comment
     * @throws SQLException
     */
    public void update_comment(final String _comment) throws SQLException {
        this.psa.update_comment(_comment);
    }

    /**
     * @return the alias as NAME (NAME_FREE VER / ID)
     */
    @Override
    public String toString() {
        return this.psa.toString();
    }

    /**
     * {@inheritDoc }
     * @param psac
     * @return 
     */
    @Override
    public int compareTo(final PrescaleSetAliasComponent psac) {
        return this.psa.compareTo(psac.get_alias());
    }

    /**
     * Get the SuperMasterTable.
     * @return 
     */
    public SuperMasterTable get_super_master_table() {
        return this.smt;
    }

    /**
     *
     * @return
     */
    public int get_alias_id(){
        return get_alias().get_id();
    }
    
    /**
     * Get the alias table.
     * @return 
     */
    public PrescaleSetAlias get_alias() {
        return this.psa;
    }
}
