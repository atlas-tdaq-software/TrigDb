function(get_test_classname result filename)
  get_filename_component(name_we ${filename} NAME_WE) # isolate base class name
  string(REPLACE "/" ";" dirs ${filename}) # split path into list

  # Check for a valid test name
  string(REGEX MATCH ".*Test$" exists ${name_we})
  if(NOT exists)
    set(${result} NOTFOUND PARENT_SCOPE)
    return()
  endif()
  
  list(LENGTH dirs len)
  math(EXPR limit "${len}-1")
  list(FIND dirs ${TDAQ_PACKAGE_NAME} idx) # find the package folder
  math(EXPR idx "${idx}+4") # start in 'src/test/java'

  set(output)
  while(idx LESS ${limit})
    list(GET dirs ${idx} dir)
    list(APPEND output ${dir})
    math(EXPR idx "${idx}+1")
  endwhile()
  list(APPEND output ${name_we}) # finally add the base class
  
  string(REPLACE ";" "." output "${output}") # turn list into period-separated classname
  set(${result} ${output} PARENT_SCOPE)

endfunction()

function(find_external_jar result)
  if(TDAQ_HAVE_TDAQExtJars)
     set(${result} ${CMAKE_BINARY_DIR}/TDAQExtJars/external.jar PARENT_SCOPE)
  else()
     set(${result} ${TDAQ_INST_PATH}/share/lib/external.jar PARENT_SCOPE)
  endif()
endfunction()

