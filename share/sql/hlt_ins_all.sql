--
-- Dumping data for table hlt_component
-- id,name,version,alias,topalg,flag,username,modtime,used

INSERT INTO hlt_component VALUES ( 1, "PESA::dummyAlgo",          1, "Egamma_L2",                  0, "flag", "atltrig",NULL,0);
INSERT INTO hlt_component VALUES ( 2, "PESA::dummyAlgo",          1, "Egamma2_L2",                 0, "flag", "atltrig",NULL,0);
INSERT INTO hlt_component VALUES ( 3, "PESA::dummyAlgo",          1, "EgammaAdv3",                 0, "flag", "atltrig",NULL,0);
INSERT INTO hlt_component VALUES ( 4, "PESA::dummyAlgo",          1, "EgammaAdv_L2",               0, "flag", "atltrig",NULL,0);
INSERT INTO hlt_component VALUES ( 5, "PESA::dummyAlgo",          1, "Em25",                       0, "flag", "atltrig",NULL,0);
INSERT INTO hlt_component VALUES ( 6, "PESA::dummyAlgo",          1, "EgammaAdv_g10",              0, "flag", "atltrig",NULL,0);
INSERT INTO hlt_component VALUES ( 7, "PESA::newDummyAlgo2To1",   1, "HtoZZ",                      0, "flag", "atltrig",NULL,0);
INSERT INTO hlt_component VALUES ( 8, "PESA::dummyAlgo",          1, "Jet_L2",                     0, "flag", "atltrig",NULL,0);
INSERT INTO hlt_component VALUES ( 9, "PESA::dummyAlgo",          1, "JetAdv_L2",                  0, "flag", "atltrig",NULL,0);
INSERT INTO hlt_component VALUES (10, "PESA::newDummyAlgoAllTEs", 1, "MissETRefiner_L2",           0, "flag", "atltrig",NULL,0);
INSERT INTO hlt_component VALUES (11, "PESA::dummyAlgo",          1, "Muon PESA::dummyAlgo/Muon2", 0, "flag", "atltrig",NULL,0);
INSERT INTO hlt_component VALUES (12, "PESA::dummyAlgo",          1, "MuonAdv",                    0, "flag", "atltrig",NULL,0);
INSERT INTO hlt_component VALUES (13, "PESA::dummyAlgo",          1, "Muon",                       0, "flag", "atltrig",NULL,0);
INSERT INTO hlt_component VALUES (14, "PESA::dummyAlgo",          1, "Tau_L2",                     0, "flag", "atltrig",NULL,0);
INSERT INTO hlt_component VALUES (15, "PESA::dummyAlgo",          1, "TauAdv_L2",                  0, "flag", "atltrig",NULL,0);
INSERT INTO hlt_component VALUES (16, "PESA::dummyAlgo",          1, "MissingEt_L2",               0, "flag", "atltrig",NULL,0);
INSERT INTO hlt_component VALUES (17, "PESA::newDummyAlgo2To1",   1, "ZFinder_L2",                 0, "flag", "atltrig",NULL,0);
INSERT INTO hlt_component VALUES (18, "PESA::dummyAlgo",          1, "Muon2",                      0, "flag", "atltrig",NULL,0);
INSERT INTO hlt_component VALUES (19, "PESA::dummyAlgo",          1, "electron_EF",                0, "flag", "atltrig",NULL,0);
INSERT INTO hlt_component VALUES (20, "PESA::dummyAlgo",          1, "electronAdv_EF",             0, "flag", "atltrig",NULL,0);
INSERT INTO hlt_component VALUES (21, "PESA::dummyAlgo",          1, "gamma_EF",                   0, "flag", "atltrig",NULL,0);
INSERT INTO hlt_component VALUES (22, "PESA::dummyAlgo",          1, "gammaAdv_EF",                0, "flag", "atltrig",NULL,0);
INSERT INTO hlt_component VALUES (23, "PESA::newDummyAlgo2To1",   2, "HtoZZ",                      0, "flag", "atltrig",NULL,0);
INSERT INTO hlt_component VALUES (24, "PESA::dummyAlgo",          1, "JetKt",                      0, "flag", "atltrig",NULL,0);
INSERT INTO hlt_component VALUES (25, "PESA::dummyAlgo",          1, "JetAdvKt",                   0, "flag", "atltrig",NULL,0);
INSERT INTO hlt_component VALUES (26, "PESA::dummyAlgo",          1, "MissingEt",                  0, "flag", "atltrig",NULL,0);
INSERT INTO hlt_component VALUES (27, "PESA::dummyAlgo",          1, "MuonMoore_EF",               0, "flag", "atltrig",NULL,0);
INSERT INTO hlt_component VALUES (28, "PESA::dummyAlgo",          1, "xuxMuon_EF",                 0, "flag", "atltrig",NULL,0);
INSERT INTO hlt_component VALUES (29, "PESA::dummyAlgo",          1, "MuonAdvEF1",                 0, "flag", "atltrig",NULL,0);
INSERT INTO hlt_component VALUES (30, "PESA::dummyAlgo",          1, "TauClu",                     0, "flag", "atltrig",NULL,0);
INSERT INTO hlt_component VALUES (31, "PESA::dummyAlgo",          1, "TauAdvEF",                   0, "flag", "atltrig",NULL,0);
INSERT INTO hlt_component VALUES (32, "PESA::newDummyAlgo2To1",   1, "ZFinder_EF",                 0, "flag", "atltrig",NULL,0);
INSERT INTO hlt_component VALUES (33, "PESA::newDummyAlgo2To1",   1, "HtoZZ_EF",                   0, "flag", "atltrig",NULL,0);

-- 
-- Dumping data for table `hlt_trigger_signature`
-- 

INSERT INTO hlt_trigger_signature VALUES ( 1, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES ( 2, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES ( 3, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES ( 4, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES ( 5, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES ( 6, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES ( 7, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES ( 8, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES ( 9, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (10, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (11, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (12, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (13, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (14, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (15, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (16, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (17, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (18, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (19, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (20, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (21, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (22, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (23, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (24, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (25, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (26, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (27, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (28, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (29, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (30, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (31, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (32, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (33, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (34, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (35, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (36, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (37, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (38, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (39, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (40, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (41, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (42, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (43, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (44, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (45, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (46, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (47, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (48, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_signature VALUES (49, 1, "atltrig", NULL, 0);

-- 
-- Dumping data for table `hlt_trigger_stream`
-- 

INSERT INTO hlt_trigger_stream VALUES (1, "electrons",  "", "physics",     1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_stream VALUES (2, "muons",     "", "physics",     1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_stream VALUES (3, "jet",       "", "physics",     1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_stream VALUES (4, "jet",       "", "calibration", 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_stream VALUES (5, "tau",       "", "physics",     1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_stream VALUES (6, "missingET", "", "physics",     1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_stream VALUES (7, "zee",       "", "physics",     1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_stream VALUES (8, "zee",       "", "calibration", 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_stream VALUES (9, "higgs",     "", "physics",     1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_stream VALUES (10, "higgs",     "", "express",     1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_stream VALUES (11, "emuons",    "", "physics",     1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_stream VALUES (12, "jetss",    "", "physics",     1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_stream VALUES (13, "Bphys",    "", "physics",     1, "atltrig", NULL, 0);

-- 
-- Dumping data for table `hlt_trigger_menu`
-- 

INSERT INTO hlt_trigger_menu VALUES (1, "MyMenu", 1, "<phase>", 0, "atltrig",NULL,0);

-- 
-- Dumping data for table `hlt_setup`
-- 

INSERT INTO hlt_setup VALUES (1, "pureSteering_L2", 1, "atltrig",NULL,0);
INSERT INTO hlt_setup VALUES (2, "pureSteering_EF", 1, "atltrig",NULL,0);


-- 
-- Dumping data for table `hlt_trigger_chain`
-- 

INSERT INTO hlt_trigger_chain VALUES (1, "em25i_L2",       1,  1, "EM25i",         "L2", 1, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_chain VALUES (2, "mu20_L2",        1,  2, "MU20",          "L2", 0, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_chain VALUES (3, "2em15i_L2",      1,  3, "2EM15i",        "L2", 0, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_chain VALUES (4, "2mu6_L2",        1,  4, "2MU06",         "L2", 0, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_chain VALUES (5, "j200_L2",        1,  5, "J200",          "L2", 0, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_chain VALUES (6, "3j90_L2",        1,  6, "3J90",          "L2", 0, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_chain VALUES (7, "tau25_xE30_L2",  1,  7, "TAU25+XE30",    "L2", 0, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_chain VALUES (8, "j50_xE60_L2",    1,  8, "J50+XE60",      "L2", 0, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_chain VALUES (9, "Z_L2",           1,  9, "EM25i",         "L2", 0, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_chain VALUES (10, "H_L2",          1, 10, "EM25i",         "L2", 0, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_chain VALUES (11, "MissingET_L2",  1, 12, "EM01",          "L2", 0, 1, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_chain VALUES (12, "e25i_EF",       1,  1, "em25i_L2",      "EF", 0, 2, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_chain VALUES (13, "mu20_EF",       1,  2, "mu20_L2",       "EF", 0, 2, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_chain VALUES (14, "2e15i_EF",      1,  3, "2em15i_L2",     "EF", 0, 2, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_chain VALUES (15, "2mu6_EF",       1,  4, "2mu6_L2",       "EF", 0, 2, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_chain VALUES (16, "g25_EF",        1,  5, "em25i_L2",      "EF", 0, 2, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_chain VALUES (17, "j200_EF",       1,  6, "j200_L2",       "EF", 0, 2, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_chain VALUES (18, "3j90_EF",       1,  7, "3j90_L2",       "EF", 0, 2, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_chain VALUES (19, "tau25_xE30_EF", 1,  8, "tau25_xE30_L2", "EF", 0, 2, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_chain VALUES (20, "j50_xE60_EF",   1,  9, "j50_xE60_L2",   "EF", 0, 2, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_chain VALUES (21, "Z_EF",          1, 10, "Z_L2",          "EF", 0, 2, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_chain VALUES (22, "H_EF",          1, 11, "H_L2",          "EF", 0, 2, "atltrig", NULL, 0);

-- 
-- Dumping data for table `hlt_trigger_group`
-- 

INSERT INTO hlt_trigger_group VALUES ( 1,  1,     "EM", "atltrig", NULL, 0);
INSERT INTO hlt_trigger_group VALUES ( 2,  2,       "", "atltrig", NULL, 0);
INSERT INTO hlt_trigger_group VALUES ( 3,  3,       "", "atltrig", NULL, 0);
INSERT INTO hlt_trigger_group VALUES ( 4,  4,       "", "atltrig", NULL, 0);
INSERT INTO hlt_trigger_group VALUES ( 5,  5,       "", "atltrig", NULL, 0);
INSERT INTO hlt_trigger_group VALUES ( 6,  6,     "EM", "atltrig", NULL, 0);
INSERT INTO hlt_trigger_group VALUES ( 7,  7,       "", "atltrig", NULL, 0);
INSERT INTO hlt_trigger_group VALUES ( 8,  8,       "", "atltrig", NULL, 0);
INSERT INTO hlt_trigger_group VALUES ( 9,  9,       "", "atltrig", NULL, 0);
INSERT INTO hlt_trigger_group VALUES (10, 10,     "EM", "atltrig", NULL, 0);
INSERT INTO hlt_trigger_group VALUES (11, 10,  "calib", "atltrig", NULL, 0);
INSERT INTO hlt_trigger_group VALUES (12, 11,       "", "atltrig", NULL, 0);
INSERT INTO hlt_trigger_group VALUES (13, 12,       "", "atltrig", NULL, 0);
INSERT INTO hlt_trigger_group VALUES (14, 13,       "", "atltrig", NULL, 0);
INSERT INTO hlt_trigger_group VALUES (15, 14,       "", "atltrig", NULL, 0);
INSERT INTO hlt_trigger_group VALUES (16, 15,       "", "atltrig", NULL, 0);
INSERT INTO hlt_trigger_group VALUES (17, 16,       "", "atltrig", NULL, 0);
INSERT INTO hlt_trigger_group VALUES (18, 17,       "", "atltrig", NULL, 0);
INSERT INTO hlt_trigger_group VALUES (19, 18,       "", "atltrig", NULL, 0);
INSERT INTO hlt_trigger_group VALUES (20, 19,       "", "atltrig", NULL, 0);
INSERT INTO hlt_trigger_group VALUES (21, 20,       "", "atltrig", NULL, 0);
INSERT INTO hlt_trigger_group VALUES (22, 21,"express", "atltrig", NULL, 0);
INSERT INTO hlt_trigger_group VALUES (23, 21,       "", "atltrig", NULL, 0);
INSERT INTO hlt_trigger_group VALUES (24, 22,     "EM", "atltrig", NULL, 0);
INSERT INTO hlt_trigger_group VALUES (25, 22,  "fresh", "atltrig", NULL, 0);

-- 
-- Dumping data for table `hlt_trigger_type`
-- 

INSERT INTO hlt_trigger_type VALUES ( 1,  1,  4, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_type VALUES ( 2,  2, 10, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_type VALUES ( 3,  3,  3, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_type VALUES ( 4,  4,  9, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_type VALUES ( 5,  5, 24, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_type VALUES ( 6,  6, 28, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_type VALUES ( 7,  7, 48, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_type VALUES ( 8,  8, 31, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_type VALUES ( 9,  9,  4, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_type VALUES (10, 10,  4, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_type VALUES (11, 10, 67, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_type VALUES (12, 11, 36, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_type VALUES (13, 12,  4, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_type VALUES (14, 13, 12, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_type VALUES (15, 14,  5, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_type VALUES (16, 15, 11, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_type VALUES (17, 16,  5, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_type VALUES (18, 17, 45, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_type VALUES (19, 18, 46, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_type VALUES (20, 19, 36, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_type VALUES (21, 20, 37, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_type VALUES (22, 21,  4, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_type VALUES (23, 21, 67, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_type VALUES (24, 22,  4, "atltrig", NULL, 0);
INSERT INTO hlt_trigger_type VALUES (25, 22, 67, "atltrig", NULL, 0);

-- 
-- Dumping data for table hlt_prescale_set
-- 

INSERT INTO hlt_prescale_set VALUES (1, "My_prescale_set", 1, 1, "atltrig",NULL,0);

-- 
-- Dumping data for table hlt_prescale
-- 

INSERT INTO hlt_prescale VALUES ( 1, 1,  1, "L2", 1, "atltrig", NULL, 0);
INSERT INTO hlt_prescale VALUES ( 2, 1,  2, "L2", 1, "atltrig", NULL, 0);
INSERT INTO hlt_prescale VALUES ( 3, 1,  3, "L2", 1, "atltrig", NULL, 0);
INSERT INTO hlt_prescale VALUES ( 4, 1,  4, "L2", 1, "atltrig", NULL, 0);
INSERT INTO hlt_prescale VALUES ( 5, 1,  5, "L2", 1, "atltrig", NULL, 0);
INSERT INTO hlt_prescale VALUES ( 6, 1,  6, "L2", 1, "atltrig", NULL, 0);
INSERT INTO hlt_prescale VALUES ( 7, 1,  7, "L2", 1, "atltrig", NULL, 0);
INSERT INTO hlt_prescale VALUES ( 8, 1,  8, "L2", 1, "atltrig", NULL, 0);
INSERT INTO hlt_prescale VALUES ( 9, 1,  9, "L2", 1, "atltrig", NULL, 0);
INSERT INTO hlt_prescale VALUES (10, 1, 10, "L2", 1, "atltrig", NULL, 0);
INSERT INTO hlt_prescale VALUES (11, 1, 12, "L2", 1, "atltrig", NULL, 0);
INSERT INTO hlt_prescale VALUES (12, 1,  1, "EF", 1, "atltrig", NULL, 0);
INSERT INTO hlt_prescale VALUES (13, 1,  2, "EF", 1, "atltrig", NULL, 0);
INSERT INTO hlt_prescale VALUES (14, 1,  3, "EF", 1, "atltrig", NULL, 0);
INSERT INTO hlt_prescale VALUES (15, 1,  4, "EF", 1, "atltrig", NULL, 0);
INSERT INTO hlt_prescale VALUES (16, 1,  5, "EF", 1, "atltrig", NULL, 0);
INSERT INTO hlt_prescale VALUES (17, 1,  6, "EF", 1, "atltrig", NULL, 0);
INSERT INTO hlt_prescale VALUES (18, 1,  7, "EF", 1, "atltrig", NULL, 0);
INSERT INTO hlt_prescale VALUES (19, 1,  8, "EF", 1, "atltrig", NULL, 0);
INSERT INTO hlt_prescale VALUES (20, 1,  9, "EF", 1, "atltrig", NULL, 0);
INSERT INTO hlt_prescale VALUES (21, 1, 10, "EF", 1, "atltrig", NULL, 0);
INSERT INTO hlt_prescale VALUES (22, 1, 11, "EF", 1, "atltrig", NULL, 0);

-- 
-- Dumping data for table `hlt_release`
-- 

INSERT INTO hlt_release VALUES (1, "LST_test", "12.0.3", "atltrig",NULL,0);

-- 
-- Dumping data for table `hlt_trigger_element`
-- 

INSERT INTO hlt_trigger_element VALUES (1, "em25i", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (2, "em25i", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (3, "mu20", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (4, "mu20", 1,"atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (5, "em15i", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (6, "em15i", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (7, "mu6", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (8, "mu6", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (9, "j200", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (10, "j200", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (11, "j90", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (12, "j90", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (13, "tau25", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (14, "xE30", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (15, "tau25", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (16, "xE30", 1,"atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (17, "j50", 1,"atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (18, "xE60", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (19, "j50", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (20, "xE60", 1,"atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (21, "Z", 1,"atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (22, "Higgs", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (23, "MET_L2", 1,"atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (24, "e25i", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (25, "e25i", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (26, "muon20", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (27, "muon20", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (28, "e15i", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (29, "e15i", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (30, "muon6", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (31, "muon6", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (32, "g25", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (33, "g25", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (34, "jet200", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (35, "jet200", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (36, "jet90", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (37, "jet90", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (38, "Tau25", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (39, "missingE30", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (40, "jet50", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (41, "missingE30", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (42, "jet50", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (43, "missingE60", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (44, "jet50", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (45, "missingE60", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (46, "Z", 1, "atltrig",NULL,0);
INSERT INTO hlt_trigger_element VALUES (47, "Higgs", 1, "atltrig",NULL,0);

-- 
-- Dumping data for table hlt_master_table
-- id,name,ver,prescaleid,triggermenuid,forcedsetupid,relid,username,date,used

INSERT INTO hlt_master_table VALUES (1, "ExampleMast", 1, 1, 1, 0, 1, "atltrig",NULL,0,0);

-- 
-- Dumping data for table `hlt_st_to_cp`
-- 

INSERT INTO hlt_st_to_cp VALUES ( 1, 1,  1, "atltrig",NULL,0);
INSERT INTO hlt_st_to_cp VALUES ( 2, 1,  2, "atltrig",NULL,0);
INSERT INTO hlt_st_to_cp VALUES ( 3, 1,  3, "atltrig",NULL,0);
INSERT INTO hlt_st_to_cp VALUES ( 4, 1,  4, "atltrig",NULL,0);
INSERT INTO hlt_st_to_cp VALUES ( 5, 1,  5, "atltrig",NULL,0);
INSERT INTO hlt_st_to_cp VALUES ( 6, 1,  6, "atltrig",NULL,0);
INSERT INTO hlt_st_to_cp VALUES ( 7, 1,  7, "atltrig",NULL,0);
INSERT INTO hlt_st_to_cp VALUES ( 8, 1,  8, "atltrig",NULL,0);
INSERT INTO hlt_st_to_cp VALUES ( 9, 1,  9, "atltrig",NULL,0);
INSERT INTO hlt_st_to_cp VALUES (10, 1, 10, "atltrig",NULL,0);
INSERT INTO hlt_st_to_cp VALUES (11, 1, 11, "atltrig",NULL,0);
INSERT INTO hlt_st_to_cp VALUES (12, 1, 12, "atltrig",NULL,0);
INSERT INTO hlt_st_to_cp VALUES (13, 1, 13, "atltrig",NULL,0);
INSERT INTO hlt_st_to_cp VALUES (14, 1, 14, "atltrig",NULL,0);
INSERT INTO hlt_st_to_cp VALUES (15, 1, 15, "atltrig",NULL,0);
INSERT INTO hlt_st_to_cp VALUES (16, 1, 16, "atltrig",NULL,0);
INSERT INTO hlt_st_to_cp VALUES (17, 1, 17, "atltrig",NULL,0);
INSERT INTO hlt_st_to_cp VALUES (18, 1, 18, "atltrig",NULL,0);
INSERT INTO hlt_st_to_cp VALUES (19, 2, 19, "atltrig",NULL,0);
INSERT INTO hlt_st_to_cp VALUES (20, 2, 20, "atltrig",NULL,0);
INSERT INTO hlt_st_to_cp VALUES (21, 2, 21, "atltrig",NULL,0);
INSERT INTO hlt_st_to_cp VALUES (22, 2, 22, "atltrig",NULL,0);
INSERT INTO hlt_st_to_cp VALUES (23, 2, 23, "atltrig",NULL,0);
INSERT INTO hlt_st_to_cp VALUES (24, 2, 24, "atltrig",NULL,0);
INSERT INTO hlt_st_to_cp VALUES (25, 2, 25, "atltrig",NULL,0);
INSERT INTO hlt_st_to_cp VALUES (26, 2, 26, "atltrig",NULL,0);
INSERT INTO hlt_st_to_cp VALUES (27, 2, 27, "atltrig",NULL,0);
INSERT INTO hlt_st_to_cp VALUES (28, 2, 28, "atltrig",NULL,0);
INSERT INTO hlt_st_to_cp VALUES (29, 2, 29, "atltrig",NULL,0);
INSERT INTO hlt_st_to_cp VALUES (30, 2, 30, "atltrig",NULL,0);
INSERT INTO hlt_st_to_cp VALUES (31, 2, 31, "atltrig",NULL,0);
INSERT INTO hlt_st_to_cp VALUES (32, 2, 32, "atltrig",NULL,0);
INSERT INTO hlt_st_to_cp VALUES (33, 2, 33, "atltrig",NULL,0);

-- 
-- Dumping data for table `hlt_tc_to_tr`
-- 

INSERT INTO hlt_tc_to_tr VALUES ( 1,  1,  1, 1, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_tr VALUES ( 2,  2,  2, 1, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_tr VALUES ( 3,  3,  1, 1, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_tr VALUES ( 4,  4,  2, 1, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_tr VALUES ( 5,  5,  3, 1, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_tr VALUES ( 6,  6,  3, 1, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_tr VALUES ( 7,  7,  4, 1, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_tr VALUES ( 8,  8,  5, 1, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_tr VALUES ( 9,  9,  1, 1, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_tr VALUES (10,  9,  6, 1, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_tr VALUES (11, 10,  7, 1, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_tr VALUES (12, 10,  8, 1, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_tr VALUES (13, 11,  5, 1, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_tr VALUES (14, 12,  1, 1, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_tr VALUES (15, 13,  2, 1, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_tr VALUES (16, 14,  1, 1, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_tr VALUES (17, 15,  9, 1, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_tr VALUES (18, 16,  1, 1, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_tr VALUES (19, 17, 10, 1, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_tr VALUES (20, 18, 10, 1, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_tr VALUES (21, 19, 11, 1, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_tr VALUES (22, 20, 11, 1, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_tr VALUES (23, 21,  7, 1, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_tr VALUES (24, 21,  6, 1, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_tr VALUES (25, 21,  8, 1, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_tr VALUES (26, 22,  7, 1, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_tr VALUES (27, 22,  8, 1, "atltrig",NULL,0);

-- 
-- Dumping data for table `hlt_tc_to_ts`
-- 

INSERT INTO hlt_tc_to_ts VALUES (1, 1, 1, 1,  "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (2, 1, 2, 2,  "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (3, 2, 3, 1,  "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (4, 2, 4, 2,  "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (5, 3, 5, 1,  "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (6, 3, 6, 2,  "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (7, 4, 7, 1,  "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (8, 4, 8, 2,  "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (9, 5, 9, 1,  "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (10, 5, 10, 2,  "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (11, 6, 11, 1,  "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (12, 6, 12, 2,  "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (13, 7, 13, 1,  "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (14, 7, 14, 2,  "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (15, 8, 15, 1,  "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (16, 8, 16, 2,  "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (17, 9, 17, 1,  "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (18, 9, 18, 2, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (19, 9, 19, 3, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (20, 10, 20, 1,  "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (21, 10, 21, 2,  "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (22, 10, 22, 3, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (23, 10, 23, 4, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (24, 11, 24, 1,  "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (25, 12, 25, 1,  "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (26, 12, 26, 2,  "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (27, 13, 27, 1,  "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (28, 13, 28, 2,  "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (29, 14, 29, 1, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (30, 14, 30, 2,  "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (31, 15, 31, 1,  "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (32, 15, 32, 2,  "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (33, 16, 33, 1, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (34, 16, 34, 2,  "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (35, 17, 35, 1,  "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (36, 17, 36, 2,  "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (37, 18, 37, 1,  "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (38, 18, 38, 2,  "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (39, 19, 39, 1, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (40, 19, 40, 2, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (41, 20, 41, 1, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (42, 20, 42, 2, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (43, 21, 43, 1,  "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (44, 21, 44, 2,  "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (45, 21, 45, 3, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (46, 22, 46, 1, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (47, 22, 47, 2, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (48, 22, 48, 3, "atltrig",NULL,0);
INSERT INTO hlt_tc_to_ts VALUES (49, 22, 49, 4, "atltrig",NULL,0);

-- 
-- Dumping data for table `hlt_te_to_cp`
-- 

INSERT INTO hlt_te_to_cp VALUES (1, 1, 5, 0, "atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (2, 2, 6, 0, "atltrig",NULL,0);
--INSERT INTO hlt_te_to_cp VALUES (3, 3, -1, 0, "atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (4, 4, 12, 0, "atltrig",NULL,0);
--INSERT INTO hlt_te_to_cp VALUES (5, 5, -1, 0,"atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (6, 6, 4, 0, "atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (7, 7, 13, 0,"atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (8, 8, 12, 0,"atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (9, 9, 8, 0,"atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (10, 10, 9, 0,"atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (11, 11, 8, 0,"atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (12, 12, 9, 0,"atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (13, 13, 14, 0, "atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (14, 14, 16, 0, "atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (15, 15, 15, 0,"atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (16, 16, 16, 0, "atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (17, 17, 8, 0, "atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (18, 18, 16, 0, "atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (19, 19, 9, 0, "atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (20, 20, 16, 0, "atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (21, 21, 17, 0, "atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (22, 22, 7, 0, "atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (23, 23, 10, 0, "atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (24, 24, 19, 0, "atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (25, 25, 20, 0, "atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (26, 26, 27, 0,"atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (27, 27, 28, 0, "atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (28, 28, 19, 0, "atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (29, 29, 20, 0,"atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (30, 30, 27, 0,"atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (31, 31, 29, 0, "atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (32, 32, 21, 0, "atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (33, 33, 22, 0, "atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (34, 34, 24, 0,"atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (35, 35, 25, 0,"atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (36, 36, 24, 0,"atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (37, 37, 25, 0, "atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (38, 38, 30, 0, "atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (39, 39, 26, 0, "atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (40, 40, 31, 0, "atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (41, 41, 26, 0, "atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (42, 42, 24, 0, "atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (43, 43, 26, 0, "atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (44, 44, 25, 0, "atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (45, 45, 26, 0, "atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (46, 46, 32, 0, "atltrig",NULL,0);
INSERT INTO hlt_te_to_cp VALUES (47, 47, 33, 0, "atltrig",NULL,0);

-- 
-- Dumping data for table `hlt_te_to_te`
-- 

INSERT INTO hlt_te_to_te VALUES ( 1,  1, "EM25i",       "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES ( 2,  2, "em25i",       "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES ( 3,  3, "MU20",        "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES ( 4,  4, "mu20",        "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES ( 5,  5, "EM15i",       "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES ( 6,  6, "em15i",       "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES ( 7,  7, "MU06",        "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES ( 8,  8, "mu6",         "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES ( 9,  9, "J200",        "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (10, 10, "j200",        "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (11, 11, "J90",         "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (12, 12, "j90",         "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (13, 13, "TAU25",       "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (14, 14, "XE30",        "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (15, 15, "tau25",       "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (16, 16, "xE30",        "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (17, 17, "J50",         "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (18, 18, "XE60",        "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (19, 19, "j50",         "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (20, 20, "xE60",        "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (21, 21, "em25i",       "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (22, 21, "em25i",       "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (23, 22, "Z",           "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (24, 22, "Z",           "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (25, 23, "MU20",        "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (26, 23, "EM25i",       "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (27, 24, "em25i\'",      "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (28, 25, "e25i",        "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (29, 26, "mu20\'",       "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (30, 27, "muon20",      "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (31, 28, "em15i\'",      "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (32, 29, "e15i",        "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (33, 30, "mu6\'",        "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (34, 31, "muon6",       "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (35, 32, "em25i\'",      "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (36, 33, "g25",         "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (37, 34, "j200\'",       "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (38, 35, "jet200",      "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (39, 36, "j90\'",        "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (40, 37, "jet90",       "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (41, 38, "tau25\'",      "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (42, 39, "xE30\'",       "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (43, 40, "Tau25",       "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (44, 41, "missingE30",  "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (45, 42, "j50\'",        "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (46, 43, "xE60\'",       "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (47, 44, "jet50",       "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (48, 45, "missingE60",  "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (49, 46, "e25i",        "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (50, 46, "e25i",        "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (51, 47, "Z\'",          "input", 1, "atltrig", NULL,0);
INSERT INTO hlt_te_to_te VALUES (52, 47, "Z\'",          "input", 1, "atltrig", NULL,0);

-- 
-- Dumping data for table `hlt_tm_to_tc`
-- 

INSERT INTO hlt_tm_to_tc VALUES ( 1, 1,  1, "atltrig",NULL,0);
INSERT INTO hlt_tm_to_tc VALUES ( 2, 1,  2, "atltrig",NULL,0);
INSERT INTO hlt_tm_to_tc VALUES ( 3, 1,  3, "atltrig",NULL,0);
INSERT INTO hlt_tm_to_tc VALUES ( 4, 1,  4, "atltrig",NULL,0);
INSERT INTO hlt_tm_to_tc VALUES ( 5, 1,  5, "atltrig",NULL,0);
INSERT INTO hlt_tm_to_tc VALUES ( 6, 1,  6, "atltrig",NULL,0);
INSERT INTO hlt_tm_to_tc VALUES ( 7, 1,  7, "atltrig",NULL,0);
INSERT INTO hlt_tm_to_tc VALUES ( 8, 1,  8, "atltrig",NULL,0);
INSERT INTO hlt_tm_to_tc VALUES ( 9, 1,  9, "atltrig",NULL,0);
INSERT INTO hlt_tm_to_tc VALUES (10, 1, 10, "atltrig",NULL,0);
INSERT INTO hlt_tm_to_tc VALUES (11, 1, 11, "atltrig",NULL,0);
INSERT INTO hlt_tm_to_tc VALUES (12, 1, 12, "atltrig",NULL,0);
INSERT INTO hlt_tm_to_tc VALUES (13, 1, 13, "atltrig",NULL,0);
INSERT INTO hlt_tm_to_tc VALUES (14, 1, 14, "atltrig",NULL,0);
INSERT INTO hlt_tm_to_tc VALUES (15, 1, 15, "atltrig",NULL,0);
INSERT INTO hlt_tm_to_tc VALUES (16, 1, 16, "atltrig",NULL,0);
INSERT INTO hlt_tm_to_tc VALUES (17, 1, 17, "atltrig",NULL,0);
INSERT INTO hlt_tm_to_tc VALUES (18, 1, 18, "atltrig",NULL,0);
INSERT INTO hlt_tm_to_tc VALUES (19, 1, 19, "atltrig",NULL,0);
INSERT INTO hlt_tm_to_tc VALUES (20, 1, 20, "atltrig",NULL,0);
INSERT INTO hlt_tm_to_tc VALUES (21, 1, 21, "atltrig",NULL,0);
INSERT INTO hlt_tm_to_tc VALUES (22, 1, 22, "atltrig",NULL,0);


-- 
-- Dumping data for table `hlt_ts_to_te`
-- 

INSERT INTO hlt_ts_to_te VALUES ( 1,  1,  1, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES ( 2,  2,  2, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES ( 3,  3,  3, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES ( 4,  4,  4, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES ( 5,  5,  5, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES ( 6,  5,  5, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES ( 7,  6,  6, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES ( 8,  6,  6, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES ( 9,  7,  7, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (10,  8,  8, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (11,  9,  9, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (12, 10, 10, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (13, 11, 11, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (14, 11, 11, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (15, 11, 11, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (16, 12, 12, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (17, 12, 12, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (18, 12, 12, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (19, 13, 13, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (20, 13, 14, 1, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (21, 14, 15, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (22, 14, 16, 1, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (23, 15, 17, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (24, 15, 18, 1, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (25, 16, 19, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (26, 16, 20, 1, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (27, 17,  1, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (28, 17,  1, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (29, 18,  2, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (30, 18,  2, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (31, 19, 21, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (32, 20,  1, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (33, 20,  1, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (34, 20,  1, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (35, 20,  1, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (36, 21,  2, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (37, 21,  2, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (38, 21,  2, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (39, 21,  2, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (40, 22, 21, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (41, 22, 21, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (42, 23, 22, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (43, 24, 23, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (44, 25, 24, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (45, 26, 25, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (46, 27, 26, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (47, 28, 27, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (48, 29, 28, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (49, 29, 28, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (50, 30, 29, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (51, 30, 29, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (52, 31, 30, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (53, 31, 30, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (54, 32, 31, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (55, 32, 31, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (56, 33, 32, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (57, 34, 33, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (58, 35, 34, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (59, 36, 35, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (60, 37, 36, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (61, 37, 36, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (62, 37, 36, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (63, 38, 37, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (64, 38, 37, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (65, 38, 37, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (66, 39, 38, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (67, 39, 39, 1, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (68, 40, 40, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (69, 40, 41, 1, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (70, 41, 42, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (71, 41, 43, 1, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (72, 42, 44, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (73, 42, 45, 1, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (74, 43, 24, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (75, 43, 24, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (76, 44, 25, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (77, 44, 25, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (78, 45, 46, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (79, 46, 24, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (80, 46, 24, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (81, 46, 24, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (82, 46, 24, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (83, 47, 25, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (84, 47, 25, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (85, 47, 25, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (86, 47, 25, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (87, 48, 46, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (88, 48, 46, 0, "atltrig",NULL,0);
INSERT INTO hlt_ts_to_te VALUES (89, 49, 47, 0, "atltrig",NULL,0);

commit;
