-- Remove obsolete columns from BGS table
ALTER TABLE l1_bunch_group_set DROP (l1bgs_username, l1bgs_modified_time, l1bgs_used) CASCADE CONSTRAINTS;

-- Add and populate BG size column in BG table
alter table l1_bunch_group
add l1bg_size number(4,0);

update l1_bunch_group
set l1bg_size = (select count(*) FROM L1_BG_TO_B where l1bg_id = L1_BG_TO_B.L1BG2B_BUNCH_GROUP_ID);
commit;