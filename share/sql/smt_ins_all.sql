INSERT INTO super_master_table VALUES(1,"smt1",1,1,1,"atltrig",NULL,0,0);
INSERT INTO super_master_table VALUES(2,"smt2_low",2,1,1,"atltrig",NULL,0,0);
INSERT INTO super_master_table VALUES(3,"Cosmics",2,2,1,"atltrig",NULL,0,0);

INSERT INTO trigger_alias VALUES(1,1,"High Luminosity","0","atltrig",NULL,0);
INSERT INTO trigger_alias VALUES(2,2,"Low Luminosity","0","atltrig",NULL,0);
INSERT INTO trigger_alias VALUES(3,3,"Cosmics","1","atltrig",NULL,0);

INSERT INTO l1_prescale_set_alias VALUES(1,2,"M3","1","atltrig",NULL,0);
