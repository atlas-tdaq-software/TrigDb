
// Oracle SQL classes
import oracle.sql.CLOB;
// Java SQL classes
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
// Oracle JDBC driver class
import oracle.jdbc.OracleDriver;
import oracle.jdbc.OracleResultSet;
// Java IO classes
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.Writer;
import java.io.Reader;
//Java Util classes
import java.util.Properties; 

/**
 * Standalone java program to insert an Lvl1 SMX file into the trigger DB
 * @author T. Wengler
*/
public class InsertSMX {  

    /* Database Connection object */  
    private Connection conn = null;  
    /* Variables to hold database details */  
    private String url      = null;  
    private String user     = null;  
    private String password = null;  
    // Create a property object to hold the username, password.  
    private Properties props = new Properties();  
    /* String to hold file name */  
    private String smxFile = null; 
    private String id = null;
    private String name = null;
    private String version = null;

    /**  
     * Default Constructor to instantiate and get a handle to class methods  
     * and variables.  
     * @param String smxFile - input SMX File
     * @param String id - the id the entry will have in the DB
     * @param String name - the name the entry will have in the DB
     */  
    public InsertSMX(String smxFile, String id, String name, String version) {
    	this.smxFile = smxFile;
    	this.id = id;
    	this.name = name;
    	this.version = version;
    }  

    /**   
     * Main program. Gets input parameters from the command line and 
     * does the DB insertion   
     */  
    public static void main(String[] args) throws SQLException {
    	
    	// Check input parameters 
    	if (args.length<4) {
    		System.out.println("Wrong number of input parameters!");  
    		System.out.println("Usage:"); 
    		System.out.println("  > java InsertSMX <smxFile> <ID> <name> <version>");  
    		return;
    	}
    	
    	// Instantiate the insertion class
    	InsertSMX insSMX =                            
    		new InsertSMX(args[0], args[1], args[2], args[3]);    
    	
    	// Load the Oracle JDBC driver class.    
    	DriverManager.registerDriver(new OracleDriver());    
    	
    	// Load the database details into the variables.    
    	insSMX.url      = "jdbc:oracle:thin:@oradev10.cern.ch:10520:D10";    
    	insSMX.user     = "atlas_trig_wengler";    
    	insSMX.password = "menu2005";    
    	
    	// Populate the property object to hold the username, password and    
    	// the new property 'SetBigStringTryClob' which is set to true. Setting    
    	// this property allows inserting of large data using the existing    
    	// setString() method, to a CLOB column in the database.    
    	insSMX.props.put("user", insSMX.user );    
    	insSMX.props.put("password", insSMX.password);    
    	insSMX.props.put("SetBigStringTryClob", "true");    
    	// Check if the table 'CLOB_TAB' is present in the database.    
    	insSMX.checkTables();    
    	// Call the methods to insert the file in the DB
    	insSMX.insertClob();    
    	// Check the result
    	insSMX.selectClob();  
    } 
    
    /**  
     *  Insert the data from the SMX file into a CLOB column in the database.  
     *  Oracle JDBC 9i has enhanced the existing PreparedStatement.setString()  
     *  method for setting the data more than 32765 bytes. So, using setString(),  
     *  it is now easy to insert CLOB data into the database directly.  
     */  
    private void insertClob() throws SQLException {    
    	// Create a PreparedStatement object.    
    	Statement stmt = null;    
    	try {      
    		// Create the database connection, if it is closed.      
    		if ((conn==null)||conn.isClosed()){        
    			System.out.println("connecting ...");
    			conn = DriverManager.getConnection( this.url, this.props );
    		}      
    		// for the insert of CLOB we have to disable AutoCommit!
    		// if we need Auto_commit for other parts, reenable it afterwards.
    		conn.setAutoCommit(false); 
    		
    		stmt = conn.createStatement();
    		
    		// create the entry and the locator
    		String cmd="INSERT INTO l1_ctp_smx VALUES (" + this.id + ",'" +
    		this.name + "'," + this.version + 
    		", empty_clob(), NULL, NULL, NULL, NULL, NULL, NULL, 'smxins', NULL, 0)";
    		System.out.println("Creating entry: " + cmd);
    		stmt.execute(cmd);
    		
    		// get the locators from the table
    		cmd = "SELECT * FROM l1_ctp_smx WHERE L1SMX_ID=" + this.id + " FOR UPDATE";
    		System.out.println("Get the locator back: " + cmd);  
    		ResultSet rset = stmt.executeQuery(cmd);
    		if (!rset.next()) {        
    			// Table does not exist, create it 
    			System.out.println("Table does not exist, create it ");  
    		}    
    		String name = ((OracleResultSet)rset).getString(2);
    		CLOB smx_clob = ((OracleResultSet)rset).getCLOB(4);
    		
    		System.out.println("Got locator for name: " + name);
    		System.out.println("smx_length: " + smx_clob.length());
    		
    		String smx_str = this.readFile(this.smxFile);
    		System.out.println("smx_input string: "); 
    		System.out.println(smx_str);
    		
    		Writer smx_outstream = ((CLOB)smx_clob).getCharacterOutputStream();
    		
    		smx_outstream.write(smx_str);
    		smx_outstream.flush();
    		System.out.println("smx_length: " + smx_clob.length());
    		smx_outstream.close();    		
    		    		
    	} catch (SQLException sqlex) {        
    		// Catch Exceptions and display messages accordingly.        
    		System.out.println("SQLException while connecting and inserting into " +
    				"the database table: " + sqlex.toString());
    	} catch (Exception ex) {  
    		System.out.println("Exception while connecting and inserting into the" +
    				" database table: " + ex.toString());
    	} finally { 
    		// Close the Statement and the connection objects.        
    		if (stmt!=null) stmt.close();        
    		if (conn!=null)   conn.close();    
    	}  
    } 
    /**  
     * Read the CLOB data back from the database by using getString()  
     * method.  
     */  
    private void selectClob() throws SQLException {    
    	// Create a PreparedStatement object    
    	Statement stmt = null;    
    	// Create a ResultSet to hold the records retrieved.    
    	ResultSet rset = null;    
    	try {      
    		// Create the database connection, if it is closed.      
    		if ((conn==null)||conn.isClosed()){ 
    			// Connect to the database.        
    			conn = DriverManager.getConnection( this.url, this.props );
    		} 
    		stmt = conn.createStatement();
    		rset = stmt.executeQuery("SELECT * FROM l1_ctp_smx WHERE L1SMX_ID="+this.id);
    		
    		if (!rset.next()) {        
    			// Table does not exist, create it 
    			System.out.println("Table does not exist, create it ");  
    		}    	    
    		String name = ((OracleResultSet)rset).getString(2);
    		CLOB smx_clob = ((OracleResultSet)rset).getCLOB(4);
    		System.out.println("output name: " + name);      
    		
    		// Print out the lenght of the file and dump the file itself
    		System.out.println("output smx_length: " + smx_clob.length());      
			String smxOutFile = name + "out.dat";
			try {

		    	Reader smx_clob_reader = ((OracleResultSet)rset).getCharacterStream(4);
		    	BufferedReader br = new BufferedReader(smx_clob_reader);      
		    	String nextLine = ""; 
		    	StringBuffer sb = new StringBuffer();
		    	while ((nextLine = br.readLine()) != null) {
		    	    sb.append(nextLine);     
		    	    sb.append(System.getProperty("line.separator"));
		    	}     
		    	// get rid of the last line separator
		    	int lastLS = sb.lastIndexOf(System.getProperty("line.separator"));
		    	sb.deleteCharAt(lastLS);
		    	String clobData = sb.toString();
				FileOutputStream file = new FileOutputStream(smxOutFile, false);
    			PrintStream smxps = new PrintStream(file);
    			smxps.println(clobData);        		
        		smxps.close();
			
			} catch (Exception e) {
    			System.out.println("Cannot open file : " + smxOutFile);
    			e.printStackTrace();
    		}
 
    		
    	} catch (SQLException sqlex) {
    		// Catch Exceptions and display messages accordingly.        
    		System.out.println("SQLException while connecting and querying the " +
    				"database table: " + sqlex.toString());    
    	} catch (Exception ex) {        
    		System.out.println("Exception while connecting and querying the " +
    				"database table: " + ex.toString());    
    	} finally { 
    		// Close the resultset, statement and the connection objects.
    		if (rset !=null) rset.close();
    		if (stmt!=null) stmt.close(); 
    		if (conn!=null)  conn.close();    
    	}  
    }  

    /**   
     * Method to check if the table ('CLOB_TAB') exists in the database; if not   
     * then it is created.   
     *   
     * Table Name: CLOB_TAB   
     *   Column Name               Type   
     *   -----------------------------------   
     *   col_col                  CLOB   
     */  
    private void checkTables()   { 
    	Statement stmt = null;
    	ResultSet rset = null;
    	try {      
    		// Create the database connection, if it is closed.      
    		if ((conn==null)||conn.isClosed()){
    			// Connect to the database.        
    			conn = DriverManager.getConnection( this.url, this.props );      
    		}      
    		// Create Statement object      
    		stmt = conn.createStatement();      
    		// Check if the table is present      
    		System.out.println("Check if the CLOB_TAB table is present");      
    		rset = stmt.executeQuery(" SELECT table_name FROM user_tables "+        
    		" WHERE table_name = 'CLOB_TAB' ");      
    		// If the table is not present, then create the table.      
    		if (!rset.next()) {        
    			// Table does not exist, create it 
    			System.out.println("CLOB_TAB table does not exist, creating it ... ");  
    			stmt.executeUpdate(" CREATE TABLE clob_tab(clob_col CLOB)");      
    		} else {
    			System.out.println("CLOB_TAB table does already exist.");
    		}
    	} catch (SQLException sqlEx) {
    		System.out.println("Could not create table CLOB_TAB : "      
    				+sqlEx.toString()); 
    	} finally  { 
    		try {   
    			if( rset != null ) rset.close(); 
    			if( stmt != null ) stmt.close();
    			if (conn!=null)  conn.close();
    		} catch(SQLException ex) {
    			System.out.println("Could not close objects in checkTables method : " 
    					+ex.toString());  
    		} 
    	}
    	
    } 
    
    /**  
     * This method reads the specified text file and, returns the content  
     * as a string. 
     * @param  String fileName - the name of the file to be read
     */  
    private String readFile( String fileName)    
    throws FileNotFoundException, IOException{      
    	
    	// Read the file whose content has to be passed as String      
    	System.out.println("Reading the file: " +fileName);
    	BufferedReader br = new BufferedReader(new FileReader(fileName));      
    	String nextLine = ""; 
    	StringBuffer sb = new StringBuffer();
    	while ((nextLine = br.readLine()) != null) {
    		//	    System.out.println(nextLine);
    		sb.append(nextLine);     
    		sb.append(System.getProperty("line.separator"));
    	}      
    	// Convert the content into to a string 
    	String clobData = sb.toString(); 
    	// Return the data.  
    	return clobData;  
    }
}
