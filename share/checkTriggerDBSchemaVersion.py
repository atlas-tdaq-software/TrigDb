#!/usr/bin/env python

from TrigConfigSvc.TrigConfigSvcUtils import getTriggerDBCursor

if __name__=="__main__":

    connections = [
        "TRIGGERDB",
        "TRIGGERDBV1",
        "TRIGGERDBMC",
        "TRIGGERDBDEV1",
        "TRIGGERDBDEV2",
        "TRIGGERDBRTT",
        "TRIGGERDBRTT2",
        "TRIGGERDBATN",
        ]


    for conn in connections:

        try:
            cursor, schemaname = getTriggerDBCursor(conn)
        except:
            print "problem with",conn
            continue
        schemaname = schemaname.rstrip('.')
        
        query = 'select max(TS_ID) from %s.trigger_schema' % schemaname

        cursor.execute(str(query))

        res = cursor.fetchall()

        for r in res:
            print "DB '%s' has version %s" % (schemaname,r[0])
